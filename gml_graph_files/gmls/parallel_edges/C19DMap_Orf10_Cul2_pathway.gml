# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOB"
      map_id "M119_103"
      name "ELOB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:Q15370"
    ]
    graphics [
      x 1139.3361614325988
      y 455.2858891524388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_51"
      name "deg_EloB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1287.270349414418
      y 342.51179761993797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_133"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 1372.6391058987942
      y 237.25545432392187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_23"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 1628.391103906865
      y 784.7617166425412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_75"
      name "diss_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1681.7633763915044
      y 863.582468526063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2"
      hgnc "HGNC_SYMBOL:UBE2"
      map_id "M119_126"
      name "E2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 1819.20240100806
      y 932.766820196646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:ZYG11B"
      map_id "M119_20"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 1621.1609639425672
      y 701.5726995950283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1"
      map_id "M119_109"
      name "NAE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa176"
      uniprot "UNIPROT:Q13564"
    ]
    graphics [
      x 1404.3270641372167
      y 1322.3773257397352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_12"
      name "bind_NAE1_UBA3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE1_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1376.380950514403
      y 1057.9118527131618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:UBA3"
      map_id "M119_110"
      name "UBA3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa177"
      uniprot "UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1242.9519846233247
      y 1042.2325740888673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3"
      map_id "M119_40"
      name "NAE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      x 1372.7256499321256
      y 808.5180353052282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_26"
      name "Cul2_space_ubiquitin_space_ligase:N8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    ]
    graphics [
      x 871.3839457634926
      y 1207.8626875553136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_78"
      name "diss_E3:NEDD8_E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_NEDD8_E3"
      uniprot "NA"
    ]
    graphics [
      x 612.1938069271519
      y 899.8319033718097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_27"
      name "CSN5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "NA"
    ]
    graphics [
      x 338.87105078606487
      y 948.9128232842082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "M119_39"
      name "Cul2_space_ubiquitin_space_ligase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      x 623.2576384741258
      y 577.9236909279307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "M119_34"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 2135.0692998732393
      y 445.34873372151014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_200"
      name "ubiquit_subs:E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 2276.401516060614
      y 412.30429676525046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "M119_33"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    ]
    graphics [
      x 2249.7577946838514
      y 529.3808814455209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_64"
      name "deg_UBA3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1093.108506319836
      y 1028.3340554897177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_120"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1000.4154622040962
      y 931.8627668318454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1"
      map_id "M119_107"
      name "NAE1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa174"
      uniprot "UNIPROT:Q13564"
    ]
    graphics [
      x 1410.2753126781527
      y 1795.1919136362662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_56"
      name "deg_NAE1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1301.6640727484255
      y 1881.3446076234313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_117"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 1179.2392337253189
      y 1913.1112746522158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M"
      map_id "M119_93"
      name "UBE2M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa102"
      uniprot "UNIPROT:P61081"
    ]
    graphics [
      x 1020.4631354576369
      y 300.6147488994163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_66"
      name "deg_Ubc12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1033.468483233606
      y 233.9264648203001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_131"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 971.0883096665037
      y 345.9808898724326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2"
      hgnc "HGNC_SYMBOL:UBE2"
      map_id "M119_156"
      name "E2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa42"
      uniprot "NA"
    ]
    graphics [
      x 2020.9864442702046
      y 732.767551158498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re99"
      uniprot "NA"
    ]
    graphics [
      x 2139.316416468009
      y 924.6583569636791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_140"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 2146.9590717146357
      y 1105.3036127312607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "M119_29"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:0E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    ]
    graphics [
      x 1879.1086265486101
      y 602.5629544451608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_74"
      name "diss_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1864.7139402988307
      y 701.9035118551706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_35"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      x 1730.0027793581926
      y 527.8672961745497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "M119_125"
      name "E1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1765.4319344262035
      y 872.5473535312169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_47"
      name "deg_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1"
      uniprot "NA"
    ]
    graphics [
      x 1957.574039017835
      y 835.1140694940059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_150"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 2106.5777838282247
      y 833.9363438926589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_3"
      name "bind_E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1733.5489927264216
      y 782.4284260655766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_102"
      name "ubit_underscore_traget"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1257.5608311320009
      y 1433.7985227862046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_72"
      name "deubiquit_subs:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deubiquit_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1225.3236881991866
      y 1312.1089137586437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:DUB"
      hgnc "HGNC_SYMBOL:DUB"
      map_id "M119_127"
      name "DUB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 1199.3284543255127
      y 1423.530411462952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_105"
      name "ubit_underscore_traget"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 1113.7635469866104
      y 1223.7426652453955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC"
      map_id "M119_113"
      name "ELOC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q15369"
    ]
    graphics [
      x 846.5317619084531
      y 788.8979338459534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_53"
      name "deg_EloC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC"
      uniprot "NA"
    ]
    graphics [
      x 831.4697008294484
      y 959.4588591379654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_141"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 833.2274185104818
      y 1120.1922157838007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:ZYG11B"
      map_id "M119_36"
      name "Cul2_space_ubiquitin_space_ligase:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 764.9678853698966
      y 536.8566877080558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_81"
      name "diss_E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 523.8925576246925
      y 638.6665869769538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "M119_37"
      name "Cul2_space_ubiquitin_space_ligase:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 422.89026899904263
      y 778.6836613795605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_94"
      name "ubit_underscore_traget"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa105"
      uniprot "NA"
    ]
    graphics [
      x 597.6168132390771
      y 761.9217587427463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB"
      map_id "M119_16"
      name "ELOB:ELOC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 732.3989283602132
      y 375.6987758044726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_86"
      name "diss_EloB:EloC"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 935.0445509594235
      y 568.3676122845609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "M119_99"
      name "E1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 1050.321904818754
      y 631.1544599445472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_48"
      name "deg_E1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 879.1967001377752
      y 600.0303695778698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_149"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 766.0794777380892
      y 618.9461601039284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_32"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
    ]
    graphics [
      x 979.5861754502823
      y 1524.0506569515942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_82"
      name "diss_E3:Orf10:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 937.0072603494015
      y 1396.9280107992977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_30"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 682.7760819949935
      y 1248.4722322256805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_2"
      name "bind_E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1789.0556085487685
      y 683.478525584545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:NEDD8"
      map_id "M119_96"
      name "NEDD8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa118"
      uniprot "UNIPROT:Q15843"
    ]
    graphics [
      x 1064.5369364484816
      y 466.2164316806851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_57"
      name "deg_NEDD8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1108.2045488669646
      y 238.38023347571902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_116"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa182"
      uniprot "NA"
    ]
    graphics [
      x 1114.045908598928
      y 74.19721541109095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_197"
      name "ubiquit:E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 2009.114862047179
      y 553.1758809799223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:DUB"
      hgnc "HGNC_SYMBOL:DUB"
      map_id "M119_101"
      name "DUB"
      node_subtype "RNA"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1176.0103388290797
      y 969.645088196603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_46"
      name "deg_DUB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1215.8602216241359
      y 813.1338319884014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_152"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa39"
      uniprot "NA"
    ]
    graphics [
      x 1249.6225096730634
      y 666.8848890386744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_87"
      name "diss_NAE"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1370.749080911138
      y 1112.133014374547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_11"
      name "bind_NAE_Pevonedistat"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1508.461234899
      y 683.9729762141615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "NA"
      map_id "M119_162"
      name "Pevonedistat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 1549.9422129326224
      y 539.6497441064932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:obo.chebi:CHEBI%3A145535;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3"
      map_id "M119_18"
      name "NAE:Pevonedistat"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      x 1606.5666635164341
      y 598.0917339061975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "M119_139"
      name "Orf10_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa278"
      uniprot "NA"
    ]
    graphics [
      x 140.16211723837614
      y 823.7637294781242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_60"
      name "deg_Orf10_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 132.0079990096317
      y 998.6505212664861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_135"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 202.62096000937095
      y 1086.993895747422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ZYG11B"
      map_id "M119_121"
      name "ZYG11B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:Q9C0D3"
    ]
    graphics [
      x 414.79300486949853
      y 320.2825762480859
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_68"
      name "deg_Zyg11B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 554.9775367803998
      y 187.5166493024701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_144"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 756.8572905405817
      y 210.05126135704916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:NEDD8"
      map_id "M119_111"
      name "NEDD8"
      node_subtype "GENE"
      node_type "species"
      org_id "sa178"
      uniprot "UNIPROT:Q15843"
    ]
    graphics [
      x 578.009018629041
      y 1171.800338178665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_177"
      name "transcr_NEDD8"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 656.8356195822239
      y 1063.3600190448492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:NEDD8"
      map_id "M119_106"
      name "NEDD8"
      node_subtype "RNA"
      node_type "species"
      org_id "sa173"
      uniprot "UNIPROT:Q15843"
    ]
    graphics [
      x 740.3850082478443
      y 877.3410678885873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ZYG11B"
      map_id "M119_95"
      name "ZYG11B"
      node_subtype "RNA"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:Q9C0D3"
    ]
    graphics [
      x 307.3916163038974
      y 812.1513050256093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_194"
      name "transl_Zyg11B"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 318.4630001374902
      y 545.6002133560183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_184"
      name "transl_E1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E1"
      uniprot "NA"
    ]
    graphics [
      x 1401.9085232314462
      y 734.6963868006397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "M119_164"
      name "E1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 705.889167472298
      y 464.8882715315947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_172"
      name "transcr_E1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E1"
      uniprot "NA"
    ]
    graphics [
      x 837.7848482933584
      y 521.5979109163726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "M119_21"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    ]
    graphics [
      x 1772.8024143482803
      y 983.0543149954087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_71"
      name "deubiquit_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1818.7471817438998
      y 773.5062823599756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:CUL2"
      map_id "M119_98"
      name "CUL2"
      node_subtype "RNA"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:Q13617"
    ]
    graphics [
      x 1908.7466936482738
      y 1306.0439011954486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_44"
      name "deg_Cul2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 2007.612644466116
      y 1450.3873976624413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_147"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 2077.7338726970183
      y 1555.436474051428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M"
      map_id "M119_129"
      name "UBE2M"
      node_subtype "RNA"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:P61081"
    ]
    graphics [
      x 1198.2631010175808
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_193"
      name "transl_Ubc12"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1034.9049521692746
      y 101.19376010227211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_45"
      name "deg_DUB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1176.2528950668882
      y 1599.7194949469247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_154"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 1127.9966252419783
      y 1713.2569550171347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:CUL2"
      map_id "M119_124"
      name "CUL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:Q13617"
    ]
    graphics [
      x 1437.7259208512623
      y 852.5828345571435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_43"
      name "deg_Cul2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 1316.2388197915268
      y 1024.171775039417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_148"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 1271.3365893606633
      y 1143.2000189975925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2"
      hgnc "HGNC_SYMBOL:UBE2"
      map_id "M119_165"
      name "E2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa7"
      uniprot "NA"
    ]
    graphics [
      x 1035.6906691399438
      y 1472.967447769964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_173"
      name "transcr_E2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E2"
      uniprot "NA"
    ]
    graphics [
      x 1118.3230203650778
      y 1381.4687984490881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2"
      hgnc "HGNC_SYMBOL:UBE2"
      map_id "M119_100"
      name "E2"
      node_subtype "RNA"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 1314.3069973350728
      y 1257.591692088938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_10"
      name "bind_EloB_EloC"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 921.8971561442561
      y 504.09237813710763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1"
      map_id "M119_112"
      name "NAE1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa179"
      uniprot "UNIPROT:Q13564"
    ]
    graphics [
      x 1278.770779706613
      y 1998.8411005002254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_176"
      name "transcr_NAE1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 1397.1680933301325
      y 1945.402752896541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_79"
      name "diss_E3:Orf10"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 431.90848664318287
      y 652.611665662851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "M119_163"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 478.817439952899
      y 824.525531935008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_8"
      name "bind_Elo_Zyg11B"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 553.7715770299859
      y 342.93026583535095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ZYG11B"
      map_id "M119_24"
      name "Zyg11B:EloBC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 684.6006111863499
      y 306.1500669886792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOB"
      map_id "M119_167"
      name "ELOB"
      node_subtype "RNA"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:Q15370"
    ]
    graphics [
      x 1651.7169912895858
      y 378.6218738622561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_52"
      name "deg_EloB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1826.94182117345
      y 503.10729602935646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_128"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 1952.9103974191812
      y 595.7889928415525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_70"
      name "deubiquit_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1961.6948914574334
      y 505.6455608775761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_196"
      name "ubiquit_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E2"
      uniprot "NA"
    ]
    graphics [
      x 1941.2578577554668
      y 921.0686319436521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "M119_155"
      name "E1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1923.4710420973697
      y 1108.156367458093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "M119_22"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    ]
    graphics [
      x 1737.9426146112257
      y 1387.6818219439251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_76"
      name "diss_E2:E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1676.957295168665
      y 1270.793910703061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "M119_25"
      name "Cul2_space_ubiquitin_space_ligase:N8:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    ]
    graphics [
      x 1432.8116518988472
      y 1420.1275725550627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_59"
      name "deg_Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 701.4486790784599
      y 944.4090242682986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_134"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 873.05445313621
      y 1026.988227686294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_183"
      name "transl_DUB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1179.909544675333
      y 1179.426845555372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:CUL2"
      map_id "M119_161"
      name "CUL2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:Q13617"
    ]
    graphics [
      x 1994.945149279999
      y 1104.9524277691128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_170"
      name "transcr_Cul2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 2070.6048667947935
      y 1267.6627078783179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOB"
      map_id "M119_91"
      name "ELOB"
      node_subtype "GENE"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:Q15370"
    ]
    graphics [
      x 1625.4370188678502
      y 225.26381937918256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_174"
      name "transcr_EloB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1721.051413737493
      y 213.16761565559636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_85"
      name "diss_Elo:Zyg11B"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 564.4706526302872
      y 270.00306932885417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_49"
      name "deg_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2"
      uniprot "NA"
    ]
    graphics [
      x 2019.1005449468407
      y 923.2745821640711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "M119_97"
      name "RBX1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 1926.2073204846167
      y 293.10331672951736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_191"
      name "transl_Rbx1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 1737.4438922061418
      y 363.30589312507755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "M119_123"
      name "RBX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 1507.262176745357
      y 437.4241462611617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_67"
      name "deg_Ubc12_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1281.0091756075535
      y 111.55902662673509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_132"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa258"
      uniprot "NA"
    ]
    graphics [
      x 1186.6197157235001
      y 227.9715698735314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_9"
      name "bind_Elo:Zyg11B_Cul2:Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 793.8124562037307
      y 427.31038029421563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1"
      map_id "M119_31"
      name "Rbx1:Cul2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 1079.2404226009487
      y 545.8410711208619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC"
      map_id "M119_92"
      name "ELOC"
      node_subtype "RNA"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q15369"
    ]
    graphics [
      x 575.6383884411458
      y 1321.4243391336054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_54"
      name "deg_EloC_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 572.2261476736772
      y 1460.7841113833942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_138"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 680.3471176133874
      y 1439.2205143229912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0000502"
      hgnc "NA"
      map_id "M119_28"
      name "26S_minus_proteasom"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "NA"
    ]
    graphics [
      x 1605.9149092575965
      y 1192.2160246395067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_41"
      name "deg_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1766.3277093945412
      y 1197.0786003402516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_136"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa268"
      uniprot "NA"
    ]
    graphics [
      x 1770.3392517132102
      y 1319.8596277436882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_199"
      name "ubiquit_subs:E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3"
      uniprot "NA"
    ]
    graphics [
      x 1721.9195299786675
      y 1214.8583481820515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_69"
      name "deg_Zyg11B_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 452.1935042357916
      y 941.2483089413158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_142"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 620.789949255443
      y 1010.6704883703434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_1"
      name "bind_Cul2_Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 1328.968135264389
      y 629.8180875920234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_187"
      name "transl_EloC"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloC"
      uniprot "NA"
    ]
    graphics [
      x 725.1446155659229
      y 1082.596228369311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "M119_38"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 876.2856917547048
      y 663.8817689396162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_84"
      name "diss_E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 686.2161385519879
      y 661.6782250033414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_182"
      name "transl_Cul2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 1676.3256543028815
      y 1093.1684201666799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3;HGNC_SYMBOL:NEDD8"
      map_id "M119_17"
      name "NAE:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1214.0038803572345
      y 303.35269463933685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_15"
      name "bind_Ubc12_NAE:NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Ubc12_NAE_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1080.5259636919259
      y 178.44677094743406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2M;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M;HGNC_SYMBOL:NEDD8"
      map_id "M119_19"
      name "UBE2M:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:Q15843;UNIPROT:P61081"
    ]
    graphics [
      x 1102.9737291455917
      y 348.40599910858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_58"
      name "deg_NEDD8_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 571.3019046478986
      y 960.6005051085269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_115"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa181"
      uniprot "NA"
    ]
    graphics [
      x 512.6907743342115
      y 1079.1072013174467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_168"
      name "syn_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1628.3701456484096
      y 1317.3235394192993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:UBA3"
      map_id "M119_114"
      name "UBA3"
      node_subtype "GENE"
      node_type "species"
      org_id "sa180"
      uniprot "UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1547.8180430665757
      y 1368.9122939698686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_179"
      name "transcr_UBA3"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1530.2074527357772
      y 1235.0768939536292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:UBA3"
      map_id "M119_108"
      name "UBA3"
      node_subtype "RNA"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1522.9967493825802
      y 1062.0425875074177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_88"
      name "diss_NAE:Pevonedistat"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1482.1807309359112
      y 619.5344941348637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_198"
      name "ubiquit:E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1854.262459361096
      y 819.5231775023584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_13"
      name "bind_NEDD8_NAE"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NEDD8_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1289.1063751152394
      y 515.8989110579726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_5"
      name "bind_E3:Orf10:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1198.0516990342571
      y 421.96140940168203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_185"
      name "transl_E2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E2"
      uniprot "NA"
    ]
    graphics [
      x 1579.402554222042
      y 1107.013090744022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_14"
      name "bind_Orf10_E3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Orf10_E3"
      uniprot "NA"
    ]
    graphics [
      x 509.0726547207985
      y 713.9039947962281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_77"
      name "diss_E3"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3"
      uniprot "NA"
    ]
    graphics [
      x 846.472112684899
      y 448.31415592049945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_188"
      name "transl_NAE1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 1421.6509313531078
      y 1584.9970507278645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_137"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa269"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 840.7438948413417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_169"
      name "syn_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 199.4839532871249
      y 874.6317921310356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M"
      map_id "M119_130"
      name "UBE2M"
      node_subtype "GENE"
      node_type "species"
      org_id "sa256"
      uniprot "UNIPROT:P61081"
    ]
    graphics [
      x 1259.9332013155808
      y 449.2716607437257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_180"
      name "transcr_Ubc12"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1278.7365527408847
      y 243.6972671322343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 2019.4732248153641
      y 1231.5917245161374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_83"
      name "diss_E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_sub_ub"
      uniprot "NA"
    ]
    graphics [
      x 1151.6134169793568
      y 1453.9926990571007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:DUB"
      hgnc "HGNC_SYMBOL:DUB"
      map_id "M119_166"
      name "DUB"
      node_subtype "GENE"
      node_type "species"
      org_id "sa8"
      uniprot "NA"
    ]
    graphics [
      x 1058.6090950421133
      y 909.2625004161315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_171"
      name "transcr_DUB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1132.8037796616186
      y 852.3813944257182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_4"
      name "bind_E3:Orf10_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 582.3341483066861
      y 643.2798977990548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_7"
      name "bind_E3:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1168.379168500561
      y 541.6794604683014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_80"
      name "diss_E3:Orf10:NEDD8_E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_Orf10_NEDD8_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 450.57466948764807
      y 1041.779570212958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_50"
      name "deg_E2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1137.4846701734562
      y 1272.022822149525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_151"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa37"
      uniprot "NA"
    ]
    graphics [
      x 1048.7352909159154
      y 1145.488461247195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "M119_153"
      name "RBX1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 2102.8998760326317
      y 538.3887520533615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_178"
      name "transcr_Rbx1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 2045.1642890278147
      y 397.2622367601366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ZYG11B"
      map_id "M119_143"
      name "ZYG11B"
      node_subtype "GENE"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q9C0D3"
    ]
    graphics [
      x 166.28606947088645
      y 1128.1703871645068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_181"
      name "transcr_Zyg11B"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 230.04389226498427
      y 984.1338029599848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC"
      map_id "M119_122"
      name "ELOC"
      node_subtype "GENE"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:Q15369"
    ]
    graphics [
      x 509.1486025681563
      y 1607.4174215035164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_175"
      name "transcr_EloC"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloC"
      uniprot "NA"
    ]
    graphics [
      x 479.22339662538036
      y 1472.3955362229422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_186"
      name "transl_EloB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1406.0053645791688
      y 390.584227485246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_73"
      name "diss_Cul2:Rbx1"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 1363.4054799961866
      y 581.9630580673879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_63"
      name "deg_substrate:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_substrate_ub"
      uniprot "NA"
    ]
    graphics [
      x 1431.5188179246938
      y 1245.6789148418457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_104"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1454.586316590298
      y 1047.3881169785689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_42"
      name "deg_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 150.32389377012032
      y 927.745352710554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_189"
      name "transl_NEDD8"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 931.9967361408371
      y 704.0899413924809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_62"
      name "deg_Rbx1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1879.9062936396863
      y 181.83064563429366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_145"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1767.5185180050487
      y 289.0269286193412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_6"
      name "bind_E3_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 717.1252568079035
      y 718.2738672857297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_55"
      name "deg_NAE1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 1306.3654558075245
      y 1372.4803613606753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_118"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 1176.9314279578805
      y 1356.3723238586626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_192"
      name "transl_UBA3"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1390.357822547855
      y 981.0415316954288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_61"
      name "deg_Rbx1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 1529.2893428486507
      y 265.4582394732183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_146"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1548.679629136242
      y 132.0998404080591
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_65"
      name "deg_UBA3_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1545.1660480205328
      y 929.1577039809723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_119"
      name "degradation"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa185"
      uniprot "NA"
    ]
    graphics [
      x 1508.984245445686
      y 803.6635821363576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_190"
      name "transl_Orf10"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 277.9498302353759
      y 753.8061603793748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_195"
      name "ubiquit_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E1"
      uniprot "NA"
    ]
    graphics [
      x 1901.662279152515
      y 1031.920127887621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_157"
      name "Ub"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa43"
      uniprot "NA"
    ]
    graphics [
      x 1791.779753336388
      y 1082.3141059210343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422"
      hgnc "NA"
      map_id "M119_158"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa44"
      uniprot "NA"
    ]
    graphics [
      x 1947.2891446542853
      y 1170.6517264557842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456215"
      hgnc "NA"
      map_id "M119_159"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 1868.509913263935
      y 1164.2370919208304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A35782"
      hgnc "NA"
      map_id "M119_160"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 2012.6071966253885
      y 1142.8902160631226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 201
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_103"
      target_id "M119_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_51"
      target_id "M119_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_23"
      target_id "M119_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "M119_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "M119_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_109"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_110"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_12"
      target_id "M119_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_26"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "M119_27"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 13
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_78"
      target_id "M119_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_34"
      target_id "M119_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_200"
      target_id "M119_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 10
    target 19
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_110"
      target_id "M119_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_64"
      target_id "M119_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_107"
      target_id "M119_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_56"
      target_id "M119_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_93"
      target_id "M119_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_66"
      target_id "M119_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_156"
      target_id "M119_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_90"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_29"
      target_id "M119_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "M119_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 31
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "M119_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_125"
      target_id "M119_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_47"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 7
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_20"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 6
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_126"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 36
    target 4
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_3"
      target_id "M119_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_102"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "M119_127"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_72"
      target_id "M119_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_113"
      target_id "M119_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_53"
      target_id "M119_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_36"
      target_id "M119_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "M119_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "M119_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_16"
      target_id "M119_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 49
    target 1
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "M119_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 49
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "M119_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_99"
      target_id "M119_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_48"
      target_id "M119_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_32"
      target_id "M119_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "M119_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 54
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "M119_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 32
    target 56
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_35"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 6
    target 56
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_126"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 56
    target 30
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_2"
      target_id "M119_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_96"
      target_id "M119_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_57"
      target_id "M119_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 32
    target 60
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_35"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 27
    target 60
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_156"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 60
    target 16
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_197"
      target_id "M119_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_101"
      target_id "M119_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_46"
      target_id "M119_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 11
    target 64
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_40"
      target_id "M119_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 64
    target 8
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "M119_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 64
    target 10
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "M119_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 11
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_40"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_162"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 65
    target 67
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_11"
      target_id "M119_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_139"
      target_id "M119_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_60"
      target_id "M119_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_121"
      target_id "M119_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_68"
      target_id "M119_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_111"
      target_id "M119_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_177"
      target_id "M119_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_95"
      target_id "M119_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 78
    target 71
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_194"
      target_id "M119_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 50
    target 79
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_99"
      target_id "M119_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 79
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_184"
      target_id "M119_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_164"
      target_id "M119_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 81
    target 50
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_172"
      target_id "M119_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_21"
      target_id "M119_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 83
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "M119_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 83
    target 27
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "M119_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_98"
      target_id "M119_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_44"
      target_id "M119_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_129"
      target_id "M119_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 88
    target 24
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_193"
      target_id "M119_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 39
    target 89
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_127"
      target_id "M119_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_45"
      target_id "M119_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_124"
      target_id "M119_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_43"
      target_id "M119_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_165"
      target_id "M119_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_173"
      target_id "M119_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 1
    target 97
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_103"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 41
    target 97
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_113"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 97
    target 48
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_10"
      target_id "M119_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_112"
      target_id "M119_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 99
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_176"
      target_id "M119_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 46
    target 100
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_37"
      target_id "M119_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "M119_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 100
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "M119_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 48
    target 102
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_16"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 71
    target 102
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_121"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_8"
      target_id "M119_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_167"
      target_id "M119_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_52"
      target_id "M119_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 16
    target 107
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_34"
      target_id "M119_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 107
    target 32
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "M119_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 107
    target 27
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "M119_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 6
    target 108
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_126"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_155"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 108
    target 27
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_196"
      target_id "M119_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 108
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_196"
      target_id "M119_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_22"
      target_id "M119_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "M119_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 111
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "M119_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 101
    target 113
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_163"
      target_id "M119_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_59"
      target_id "M119_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 61
    target 115
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_101"
      target_id "M119_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 115
    target 39
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_183"
      target_id "M119_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_161"
      target_id "M119_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 117
    target 84
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_170"
      target_id "M119_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_91"
      target_id "M119_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 119
    target 104
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_174"
      target_id "M119_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 103
    target 120
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_24"
      target_id "M119_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 120
    target 48
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "M119_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 120
    target 71
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "M119_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 6
    target 121
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_126"
      target_id "M119_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 121
    target 35
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_49"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_97"
      target_id "M119_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_191"
      target_id "M119_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 87
    target 125
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_129"
      target_id "M119_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_67"
      target_id "M119_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 103
    target 127
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_24"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_31"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 127
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_9"
      target_id "M119_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_92"
      target_id "M119_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_54"
      target_id "M119_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_28"
      target_id "M119_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_41"
      target_id "M119_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 82
    target 135
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_21"
      target_id "M119_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 135
    target 110
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_199"
      target_id "M119_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 77
    target 136
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_95"
      target_id "M119_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_69"
      target_id "M119_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 124
    target 138
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_123"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 91
    target 138
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_124"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 138
    target 128
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_1"
      target_id "M119_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 129
    target 139
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_92"
      target_id "M119_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 139
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_187"
      target_id "M119_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 140
    target 141
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_38"
      target_id "M119_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 141
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "M119_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 141
    target 47
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "M119_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 84
    target 142
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_98"
      target_id "M119_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 142
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_182"
      target_id "M119_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 143
    target 144
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_17"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 24
    target 144
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_93"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_15"
      target_id "M119_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 76
    target 146
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_106"
      target_id "M119_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_58"
      target_id "M119_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 134
    target 148
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_136"
      target_id "M119_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 148
    target 132
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_168"
      target_id "M119_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_114"
      target_id "M119_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_179"
      target_id "M119_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 67
    target 152
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_18"
      target_id "M119_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 152
    target 66
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_88"
      target_id "M119_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 152
    target 11
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_88"
      target_id "M119_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 7
    target 153
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_20"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 27
    target 153
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_156"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 153
    target 82
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_198"
      target_id "M119_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 57
    target 154
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_96"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 11
    target 154
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_40"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 154
    target 143
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_13"
      target_id "M119_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 44
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_36"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 145
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_19"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 155
    target 32
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "M119_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 155
    target 24
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "M119_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 96
    target 156
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_100"
      target_id "M119_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 156
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_185"
      target_id "M119_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 15
    target 157
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_39"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 101
    target 157
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_163"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 157
    target 46
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_14"
      target_id "M119_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 15
    target 158
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_39"
      target_id "M119_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 158
    target 128
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "M119_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 158
    target 103
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "M119_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 21
    target 159
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_107"
      target_id "M119_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 159
    target 8
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_188"
      target_id "M119_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 160
    target 161
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_137"
      target_id "M119_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 161
    target 14
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_169"
      target_id "M119_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_130"
      target_id "M119_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 163
    target 87
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_180"
      target_id "M119_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 109
    target 164
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_155"
      target_id "M119_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 164
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_89"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 112
    target 165
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_25"
      target_id "M119_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 165
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "M119_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 165
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "M119_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_166"
      target_id "M119_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 167
    target 61
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_171"
      target_id "M119_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 46
    target 168
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_37"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 47
    target 168
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_94"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 168
    target 44
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_4"
      target_id "M119_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 145
    target 169
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_19"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 140
    target 169
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_38"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 169
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "M119_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 169
    target 24
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "M119_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 55
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_30"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 14
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "M119_27"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 170
    target 46
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_80"
      target_id "M119_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 96
    target 171
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_100"
      target_id "M119_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 171
    target 172
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_50"
      target_id "M119_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 173
    target 174
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_153"
      target_id "M119_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 174
    target 122
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_178"
      target_id "M119_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_143"
      target_id "M119_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 176
    target 77
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_181"
      target_id "M119_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_122"
      target_id "M119_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 178
    target 129
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_175"
      target_id "M119_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 104
    target 179
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_167"
      target_id "M119_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 179
    target 1
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_186"
      target_id "M119_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 128
    target 180
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_31"
      target_id "M119_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 180
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "M119_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 180
    target 124
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "M119_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 37
    target 181
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_102"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 132
    target 181
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "M119_28"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 181
    target 182
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_63"
      target_id "M119_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 14
    target 183
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_27"
      target_id "M119_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 183
    target 160
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_42"
      target_id "M119_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 76
    target 184
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_106"
      target_id "M119_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 184
    target 57
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_189"
      target_id "M119_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 122
    target 185
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_97"
      target_id "M119_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_62"
      target_id "M119_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 47
    target 187
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_94"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 15
    target 187
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_39"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 187
    target 140
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_6"
      target_id "M119_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 8
    target 188
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_109"
      target_id "M119_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_55"
      target_id "M119_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 151
    target 190
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_108"
      target_id "M119_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 190
    target 10
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_192"
      target_id "M119_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 124
    target 191
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_123"
      target_id "M119_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 191
    target 192
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_61"
      target_id "M119_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 151
    target 193
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_108"
      target_id "M119_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 193
    target 194
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_65"
      target_id "M119_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 68
    target 195
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_139"
      target_id "M119_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 195
    target 101
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_190"
      target_id "M119_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 33
    target 196
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_125"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_157"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 198
    target 196
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_158"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 196
    target 109
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "M119_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 196
    target 199
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "M119_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 196
    target 200
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "M119_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
