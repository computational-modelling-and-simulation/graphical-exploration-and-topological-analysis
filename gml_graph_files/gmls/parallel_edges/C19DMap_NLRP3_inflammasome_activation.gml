# generated with VANTED V2.8.2 at Fri Mar 04 09:57:01 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_8"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1198.7082955169637
      y 348.86541533455716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 1118.6008371885482
      y 461.79149198203856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_58"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa103"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1025.749975307565
      y 713.5999872440484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_113"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa99"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1290.4533280532955
      y 328.4519573508321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M117_57"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa102"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1027.0560987822682
      y 306.2172752777291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_109"
      name "proIL_minus_18"
      node_subtype "RNA"
      node_type "species"
      org_id "sa93"
      uniprot "NA"
    ]
    graphics [
      x 211.25055574278906
      y 567.0722755539393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 121.82334079899636
      y 684.2124651842403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_62"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 119.25896469430904
      y 832.5047271141024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_108"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1426.357511651547
      y 1312.6663781490338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_46"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1448.2196198213337
      y 1141.5517344730758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "M117_101"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1571.8299122888498
      y 888.287405332124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:TXNIP"
      map_id "M117_11"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1270.812806588409
      y 1294.9777179160478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_9"
      name "ASC:MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1041.4902879527704
      y 452.93940164599644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1187.5382799322201
      y 426.1673830010412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS"
      map_id "M117_10"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1238.2337879018094
      y 527.5849931795574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1289.1080185048156
      y 157.29040290703256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_84"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1176.2719071757695
      y 145.1245356633641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_83"
      name "s878"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1178.9078851558818
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_68"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1585.796857005047
      y 436.68779356990103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1748.3327391225566
      y 458.7980448871361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_69"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1648.0750733069272
      y 476.02754568228585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_12"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 1834.1994361351628
      y 608.079111329133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXN;HGNC_SYMBOL:TXNIP"
      map_id "M117_1"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1708.245067683768
      y 609.4189944960195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_17"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1723.1678293998552
      y 722.4692392847417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_91"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1876.2688226017722
      y 683.9000246957771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 198.1054304338678
      y 990.1571455019928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_14"
      name "Caspase_minus_1_space_Tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 371.7069238825957
      y 960.1810220340888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_79"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 62.5
      y 1029.226619257667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_92"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 185.08999364487795
      y 1107.98436712227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_81"
      name "s876"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 890.6654085356439
      y 1683.2532184024703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1060.1807939235796
      y 1750.379883638787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M117_78"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158"
      uniprot "NA"
    ]
    graphics [
      x 1300.122584675065
      y 1805.330478083822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_54"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 986.5411090780148
      y 1623.1072699879842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_27"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1623.877675243785
      y 733.0934366242388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_100"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1732.6391541893493
      y 791.7561518007863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_71"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1984.9426228948093
      y 893.1649115786453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_49"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1929.4999412995048
      y 786.40078125845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_104"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1809.3253846287535
      y 923.8559261673121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_70"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 371.8502522936585
      y 599.471136773714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 420.99265358443006
      y 756.0062653418642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_87"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 557.492543750057
      y 630.8282766017503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_90"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 538.3892960638368
      y 791.3008234895699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_64"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 471.4805833430445
      y 1288.233825538468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 576.3637871123223
      y 1419.7894414251198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_63"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 533.4302037556149
      y 1321.0417366070583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_65"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 764.0406596774523
      y 1406.3361659814773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_33"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1626.2296983388756
      y 934.1362955534693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_103"
      name "p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1431.3073785577094
      y 841.1714001586521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M117_72"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 1603.6148616271807
      y 1686.5435983019415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1547.986062330559
      y 1850.9897613839303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M117_74"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa154"
      uniprot "NA"
    ]
    graphics [
      x 1431.8648916157927
      y 1811.8016058709968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M117_73"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa153"
      uniprot "NA"
    ]
    graphics [
      x 1643.9232910186772
      y 1748.5829353292575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M117_80"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa161"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1504.0461923944445
      y 1696.6939029331854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M117_76"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 1455.4986376283318
      y 1906.5710329755493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M117_77"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 1665.9084332237842
      y 1810.6897377435532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M117_75"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155"
      uniprot "NA"
    ]
    graphics [
      x 1559.0493469554808
      y 1728.521682331762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M117_82"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 1471.4920634833686
      y 1755.5733810352224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_55"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1166.744444353947
      y 964.1171269076352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_26"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1067.1555100590895
      y 1006.8697939173783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "M117_3"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 967.7445158185872
      y 1081.0439682292554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3"
      map_id "M117_2"
      name "NLRP3_space_oligomer:ASC:proCaspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1082.3709190174766
      y 1120.4160068023593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_59"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa104"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 830.5083911352348
      y 460.4504464028176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 932.4123170824798
      y 425.73141267883466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1015.7026013807157
      y 1241.6328717885508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M117_96"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 1131.6551770920464
      y 1198.5881008377637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_85"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 882.9299483369919
      y 1315.0136758028034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_88"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa170"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1052.6273327834006
      y 1341.8311516916865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_89"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1128.6622452068973
      y 1284.8166225584105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_86"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 866.6933367429615
      y 1235.8936083694439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_97"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1411.1772183646922
      y 632.8525477757648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_32"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1405.547515752638
      y 473.0046280553506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_7"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 1344.5758603610366
      y 552.4200541878336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_25"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 130.26802541788027
      y 940.4702622103055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_94"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 235.08328833998007
      y 889.7518685252138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1230.0622850140498
      y 741.7765190561754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653"
      hgnc "HGNC_SYMBOL:NFKB2"
      map_id "M117_105"
      name "p65"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q00653"
    ]
    graphics [
      x 1277.2413689139762
      y 845.5975770046931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_5"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 1016.7886648941152
      y 643.3558897076152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1334.1433238987934
      y 1464.6182567959356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "M117_13"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1199.675804814759
      y 1581.8290936522287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:SUGT1;HGNC_SYMBOL:NLRP3"
      map_id "M117_15"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1203.875639992983
      y 1459.759946787768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_16"
      name "CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 606.4785348272167
      y 1257.4694269564493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 484.5124942083307
      y 1129.4331398591473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1853.404806417273
      y 790.5245180933375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_93"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1765.253721004894
      y 857.1477429494224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 785.8534922978737
      y 574.8416848632271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_4"
      name "NF_minus_KB_space_COMPLEX:IKBA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P19838;UNIPROT:Q00653"
    ]
    graphics [
      x 859.0093641672672
      y 685.0876498522734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_6"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 562.6145354514537
      y 457.63671986378426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_111"
      name "proIL_minus_18"
      node_subtype "GENE"
      node_type "species"
      org_id "sa95"
      uniprot "NA"
    ]
    graphics [
      x 461.73554202292746
      y 508.98781884228003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_29"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 356.01556986603987
      y 499.39273486152547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_41"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1308.443341004614
      y 431.07182656198233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_20"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 749.2864580138355
      y 1302.4978286468559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1062.239079264455
      y 152.3694247792796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_110"
      name "proIL_minus_1B"
      node_subtype "RNA"
      node_type "species"
      org_id "sa94"
      uniprot "NA"
    ]
    graphics [
      x 393.3436921912039
      y 291.4569632788191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_43"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 366.00252086243995
      y 422.0199263003137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_66"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa144"
      uniprot "NA"
    ]
    graphics [
      x 685.2356675815424
      y 1105.5804236086865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 690.0217267444358
      y 1237.8879104102189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_67"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 605.4062636547783
      y 1128.1883203668785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_102"
      name "IKBA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 889.4603242750459
      y 843.79813939306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 974.7605169484995
      y 775.0067591965748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_23"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 673.0611403177144
      y 495.9025444281773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_95"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 750.3054506894101
      y 380.8139851979438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "M117_98"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 1087.5611253014135
      y 1868.819328861826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:29712950;PUBMED:17435760"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1154.5652225933886
      y 1758.9746921644378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "M117_99"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1183.0259265485083
      y 1881.0569591392034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 963.9225398029312
      y 955.4678483468081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_56"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 977.7085566875971
      y 1187.389193371973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:25770182;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_37"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1056.3380831590855
      y 1439.3913626081294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_107"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa91"
      uniprot "NA"
    ]
    graphics [
      x 1004.1504933780609
      y 1541.1421449100962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_106"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 1087.1484514864949
      y 1550.2781033620597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "M117_60"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 937.8249006281426
      y 1428.101462535294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "M117_61"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 936.7578717472644
      y 1505.006367753125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_112"
      name "proIL_minus_1B"
      node_subtype "GENE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 585.4508100447226
      y 203.4467797807207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_28"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 515.5208309882826
      y 303.28217136092485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 114
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_8"
      target_id "M117_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_109"
      target_id "M117_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_44"
      target_id "M117_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_46"
      target_id "M117_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_9"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 4
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_39"
      target_id "M117_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 4
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_53"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_68"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_69"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_48"
      target_id "M117_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_1"
      target_id "M117_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 24
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 8
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_62"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 26
    target 29
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_81"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_78"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_51"
      target_id "M117_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 11
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 34
    target 23
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_27"
      target_id "M117_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_71"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 22
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_12"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_49"
      target_id "M117_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_70"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 27
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_64"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_63"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_45"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 38
    target 47
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_104"
      target_id "M117_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_33"
      target_id "M117_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_72"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_74"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 52
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_73"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 53
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_80"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 50
    target 54
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 50
    target 55
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 50
    target 56
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 50
    target 32
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 50
    target 57
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_55"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_3"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_26"
      target_id "M117_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_59"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 5
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 63
    target 13
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_38"
      target_id "M117_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 61
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_2"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_96"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 64
    target 66
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 64
    target 67
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 64
    target 68
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 64
    target 69
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 64
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_97"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 4
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_32"
      target_id "M117_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 28
    target 73
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_79"
      target_id "M117_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_25"
      target_id "M117_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 48
    target 75
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_103"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_105"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_31"
      target_id "M117_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 9
    target 78
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_13"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 78
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_42"
      target_id "M117_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_16"
      target_id "M117_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 82
    target 27
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_21"
      target_id "M117_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 35
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_93"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 83
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_19"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 77
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_4"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_36"
      target_id "M117_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_111"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 87
    target 89
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 89
    target 6
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_29"
      target_id "M117_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 15
    target 90
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_10"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 72
    target 90
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_7"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 90
    target 1
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_41"
      target_id "M117_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 66
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_85"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 69
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_86"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 91
    target 81
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_20"
      target_id "M117_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 5
    target 92
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 17
    target 92
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 92
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_52"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_110"
      target_id "M117_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 94
    target 39
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_43"
      target_id "M117_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_66"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_67"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 96
    target 46
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_47"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_102"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 77
    target 99
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 99
    target 86
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_35"
      target_id "M117_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 41
    target 100
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_87"
      target_id "M117_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_23"
      target_id "M117_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_98"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_99"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 103
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_30"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 3
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_58"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_56"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 105
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_34"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 80
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_15"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_107"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_106"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_60"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 111
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_61"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 33
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_54"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 12
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_11"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 46
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_65"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_107"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_106"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_60"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 111
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_61"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 33
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_54"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 12
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_11"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 46
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_65"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 107
    target 106
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 107
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_112"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 87
    target 113
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 113
    target 93
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_28"
      target_id "M117_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
