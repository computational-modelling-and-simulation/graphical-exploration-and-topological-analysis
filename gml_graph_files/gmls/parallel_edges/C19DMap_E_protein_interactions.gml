# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_38"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 1068.3216365288067
      y 846.4980461198073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 954.2184009693967
      y 858.4920148368965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_39"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1011.4228494439581
      y 768.1193671842784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_42"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 908.1267506444561
      y 726.9977029865923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29101"
      hgnc "NA"
      map_id "M17_68"
      name "Na_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa68"
      uniprot "NA"
    ]
    graphics [
      x 331.70966738744403
      y 1128.97481712961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 255.92807609688282
      y 1224.9148505347166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481;urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476"
      hgnc "HGNC_SYMBOL:ATP1B1;HGNC_SYMBOL:ATP1A1"
      map_id "M17_2"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P05026;UNIPROT:P05023"
    ]
    graphics [
      x 359.1320952403978
      y 1357.6331717218932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29101"
      hgnc "NA"
      map_id "M17_67"
      name "Na_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa67"
      uniprot "NA"
    ]
    graphics [
      x 207.96880500649155
      y 1106.3461933837466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:ASIC1"
      map_id "M17_53"
      name "ASIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27"
      uniprot "UNIPROT:P78348"
    ]
    graphics [
      x 589.410062426696
      y 1481.390966332715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_16"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 607.0901950969594
      y 1624.3119642982483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M17_52"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 659.9635689546518
      y 1515.3200952275297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:ASIC1"
      map_id "M17_51"
      name "ASIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P78348"
    ]
    graphics [
      x 698.7705727472152
      y 1704.4712831915372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "M17_55"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 273.13778454198746
      y 507.24824729768295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 320.2831027819597
      y 616.6027318984375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "M17_61"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 394.2415963982441
      y 741.6842421966988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:PALS1;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5;HGNC_SYMBOL:PALS1"
      map_id "M17_4"
      name "E_minus_PALS1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      x 1109.0733544588618
      y 1457.610895142615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_27"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1069.1922652215621
      y 1310.3500926025217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_71"
      name "Maintenance_space_of_space_tight_space_junction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa74"
      uniprot "NA"
    ]
    graphics [
      x 961.5200861632791
      y 1186.3379206789195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CDK9;HGNC_SYMBOL:CCNT1"
      map_id "M17_8"
      name "P_minus_TEFb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      x 413.42093677729866
      y 824.1178825176131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_22"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 306.7619271321014
      y 929.7440932690688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_72"
      name "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 375.2933764623132
      y 1010.7263498245927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_44"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1506.6407135734585
      y 787.2335514847298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1433.500757436561
      y 692.7433101566818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_48"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1287.2753352632553
      y 666.112760442543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_43"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1496.059827967631
      y 599.5623650807768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_59"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa32"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1023.6301989539411
      y 1572.5827919935364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_10"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 954.650617128644
      y 1475.9885909986638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "M17_57"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 769.4678059188477
      y 1367.1852568114628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_28"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 516.3249050663386
      y 1330.5389488394922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_70"
      name "Activity_space_of_space_sodium_space_channels"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa73"
      uniprot "NA"
    ]
    graphics [
      x 685.357904966494
      y 1308.5866067174657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_36"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 755.1153957471147
      y 916.66720431721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 770.7911757380841
      y 792.6074251005766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_41"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 628.2182004394642
      y 801.5729053578929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 735.2576346482188
      y 1574.0289775841964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033"
      hgnc "HGNC_SYMBOL:STOML3"
      map_id "M17_50"
      name "STOML3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:Q8TAV4"
    ]
    graphics [
      x 853.6754400258473
      y 1497.6020919695898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_49"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa22"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 536.3599641504808
      y 1546.745082417133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:STOML3;HGNC_SYMBOL:ASIC1"
      map_id "M17_5"
      name "ASIC1_space_trimer:H_plus_:STOML3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q8TAV4;UNIPROT:P78348"
    ]
    graphics [
      x 807.9668094764403
      y 1435.908529506952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_37"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 849.8864984532345
      y 653.1124803087109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 754.0428607597808
      y 639.7391280124972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_40"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 618.4811286319793
      y 626.836423792492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 657.0480437517016
      y 1210.913862519447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "M17_62"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa35"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 554.5128351832419
      y 1071.7925223013222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14"
      hgnc "HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14"
      map_id "M17_76"
      name "H4C14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 950.5692198619381
      y 288.8068485492006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 1017.1137478688745
      y 147.12480745940502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H4C1"
      map_id "M17_75"
      name "H4_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 962.1329255457601
      y 220.0940750562936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C1"
      map_id "M17_46"
      name "H4C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1058.980804788279
      y 256.67693901202506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464"
      hgnc "HGNC_SYMBOL:H3C15"
      map_id "M17_56"
      name "H3C15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q71DI3"
    ]
    graphics [
      x 894.1553468752849
      y 231.17686590857852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1"
      hgnc "HGNC_SYMBOL:H3C1"
      map_id "M17_35"
      name "H3C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P68431"
    ]
    graphics [
      x 878.3621374471845
      y 112.00673908884869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1"
      map_id "M17_63"
      name "H4C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 930.7264237538745
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349"
      hgnc "HGNC_SYMBOL:H2BC21"
      map_id "M17_69"
      name "H2BC21"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q16778"
    ]
    graphics [
      x 1123.1407471535713
      y 166.2214946382145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:interpro:IPR002119"
      hgnc "NA"
      map_id "M17_64"
      name "H2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 866.2402119669496
      y 174.19997635962034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4-16"
      map_id "M17_3"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      x 1063.7329715431665
      y 404.1023125649424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 480.794338051822
      y 913.5439766415543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "M17_60"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 310.82727550901757
      y 838.4762322033131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9;urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:PATJ;HGNC_SYMBOL:MPP5;HGNC_SYMBOL:CRB3"
      map_id "M17_7"
      name "CRB3:PALS1:PATJ_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      x 648.9501334101444
      y 999.9939673535522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 813.896286327554
      y 1088.1059234549036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "M17_54"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 62.5
      y 808.9705141135979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 176.20479287981374
      y 789.7513079303585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_47"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1260.3350326731124
      y 440.00644447919996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1270.3161261538664
      y 547.5124699301073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_45"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1135.4645407140697
      y 535.8768853822148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:pubchem.compound:46907787"
      hgnc "NA"
      map_id "M17_74"
      name "JQ_minus_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 1152.5450523268692
      y 609.5240529359492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1102.2564962613299
      y 666.3158524675183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119"
      hgnc "HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H4C14"
      map_id "M17_1"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      x 1214.8143272750117
      y 809.4879850471845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:18406326"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_24"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1299.3332133665726
      y 933.8136755934717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_73"
      name "Chromatin_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 1350.595363419227
      y 1047.5792600753525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_21"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 523.5308336497121
      y 719.7563750358981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476;urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481"
      hgnc "HGNC_SYMBOL:ATP1A1;HGNC_SYMBOL:ATP1B1"
      map_id "M17_6"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P05023;UNIPROT:P05026"
    ]
    graphics [
      x 299.06443341099305
      y 1332.4354812042884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 394.8973127295598
      y 1459.6640836224674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "M17_65"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa65"
      uniprot "NA"
    ]
    graphics [
      x 206.71303187731644
      y 1543.4109161606566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 211.70298554337745
      y 1431.4784529744256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "M17_66"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 92.97352618168668
      y 1455.4583885268949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 846.125802936959
      y 1364.720068053075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_30"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 803.6036506406195
      y 1290.9789392151565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1174.4065203346354
      y 1587.23280700584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_58"
      name "csa6_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1112.113991380099
      y 1700.3054345461644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 77
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_38"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_39"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_31"
      target_id "M17_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_68"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "M17_2"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_20"
      target_id "M17_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_53"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_52"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_16"
      target_id "M17_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_55"
      target_id "M17_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_13"
      target_id "M17_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_4"
      target_id "M17_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_27"
      target_id "M17_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_8"
      target_id "M17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_22"
      target_id "M17_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_44"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_34"
      target_id "M17_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_59"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_57"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 27
    target 16
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_10"
      target_id "M17_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 7
    target 29
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_2"
      target_id "M17_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_28"
      target_id "M17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_36"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 4
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_9"
      target_id "M17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 12
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_51"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_50"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 36
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_49"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 34
    target 37
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_17"
      target_id "M17_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_37"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 4
    target 39
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_19"
      target_id "M17_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 28
    target 41
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_57"
      target_id "M17_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_12"
      target_id "M17_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_76"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_75"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 46
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_46"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 47
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_56"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 48
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_35"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 49
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_63"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 50
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_69"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 51
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_64"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 44
    target 52
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_32"
      target_id "M17_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 42
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_62"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 15
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_61"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_60"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_25"
      target_id "M17_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_7"
      target_id "M17_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 56
    target 18
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_26"
      target_id "M17_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_54"
      target_id "M17_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 58
    target 54
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_14"
      target_id "M17_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_47"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_45"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_74"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 60
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_33"
      target_id "M17_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 52
    target 63
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_3"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 4
    target 63
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 24
    target 63
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_23"
      target_id "M17_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_1"
      target_id "M17_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_24"
      target_id "M17_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 33
    target 67
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_41"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 40
    target 67
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_40"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 67
    target 19
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_21"
      target_id "M17_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_6"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 36
    target 69
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_49"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 69
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_15"
      target_id "M17_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_65"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 7
    target 71
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "M17_2"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_18"
      target_id "M17_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 35
    target 73
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_50"
      target_id "M17_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 73
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_29"
      target_id "M17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 37
    target 74
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_5"
      target_id "M17_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 74
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_30"
      target_id "M17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 16
    target 75
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_4"
      target_id "M17_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_11"
      target_id "M17_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
