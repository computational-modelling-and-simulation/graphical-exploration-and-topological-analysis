# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M122_230"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa375"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1508.0012340160742
      y 1533.8186828174278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re164"
      uniprot "NA"
    ]
    graphics [
      x 1175.5263036669744
      y 1613.4932671848567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O15553;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553"
      hgnc "HGNC_SYMBOL:MEFV"
      map_id "M122_33"
      name "Pyrin_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa85"
      uniprot "UNIPROT:O15553"
    ]
    graphics [
      x 962.6750920606416
      y 1496.1886380138585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:O15553;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MEFV;HGNC_SYMBOL:PYCARD"
      map_id "M122_34"
      name "Pyrin_space_trimer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa88"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:O15553"
    ]
    graphics [
      x 1150.0546471132259
      y 1737.824402539047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_237"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa384"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2140.02240128496
      y 735.693440716607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_66"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 2257.5298245245194
      y 832.3518594006206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_236"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa383"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2125.9013917787506
      y 813.2138398147117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_36"
      name "CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2164.0581855518317
      y 1026.8735543860917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:SUGT1"
      map_id "M122_26"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa72"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1197.1965307520884
      y 1518.019078052912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1391.3906056894953
      y 1563.4995279294744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_223"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1736.29815652477
      y 1637.364351355177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "M122_27"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa73"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1105.1849282118958
      y 1556.6080354238316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15430"
      hgnc "NA"
      map_id "M122_269"
      name "PRIN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 1231.0082172637187
      y 856.8306858198438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1065.0388236537574
      y 1011.2282752490064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_270"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 1007.1647743578335
      y 927.4981985293742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:ec-code:4.99.1.1;urn:miriam:refseq:NM_000140;urn:miriam:uniprot:P22830;urn:miriam:hgnc.symbol:FECH;urn:miriam:hgnc.symbol:FECH;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_268"
      name "FECH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa53"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1174.4029476769458
      y 896.7433490656085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27889"
      hgnc "NA"
      map_id "M122_271"
      name "Pb2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa56"
      uniprot "NA"
    ]
    graphics [
      x 1101.1306103113304
      y 911.668367975973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_202"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 888.3292129795111
      y 1349.2476736437675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_272"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa57"
      uniprot "NA"
    ]
    graphics [
      x 1054.4474597298672
      y 869.8360077117842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 1925.1718667446294
      y 1131.6461782211104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_35"
      name "Caspase_minus_1_space_Tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1650.261129955591
      y 1213.5149807466366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57292"
      hgnc "NA"
      map_id "M122_274"
      name "SUCC_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa65"
      uniprot "NA"
    ]
    graphics [
      x 721.3773962271466
      y 1805.7275854568525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:25446301"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 558.1071463231777
      y 1732.48690037425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57305"
      hgnc "NA"
      map_id "M122_276"
      name "Gly"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa68"
      uniprot "NA"
    ]
    graphics [
      x 537.759047064909
      y 1871.056074474574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_275"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa67"
      uniprot "NA"
    ]
    graphics [
      x 404.20031061345355
      y 1751.905465646739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P22557;urn:miriam:uniprot:P13196;urn:miriam:hgnc.symbol:ALAS1;urn:miriam:hgnc.symbol:ALAS1;urn:miriam:ec-code:2.3.1.37;urn:miriam:ncbigene:211;urn:miriam:ensembl:ENSG00000023330;urn:miriam:ncbigene:211;urn:miriam:refseq:NM_000688;urn:miriam:uniprot:P13196;urn:miriam:uniprot:P13196;urn:miriam:hgnc:396;urn:miriam:hgnc.symbol:ALAS2;urn:miriam:hgnc.symbol:ALAS2;urn:miriam:ec-code:2.3.1.37;urn:miriam:ensembl:ENSG00000158578;urn:miriam:uniprot:P22557;urn:miriam:uniprot:P22557;urn:miriam:refseq:NM_000032;urn:miriam:ncbigene:212;urn:miriam:ncbigene:212;urn:miriam:hgnc:397"
      hgnc "HGNC_SYMBOL:ALAS1;HGNC_SYMBOL:ALAS2"
      map_id "M122_22"
      name "ALAS1:ALAS2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P22557;UNIPROT:P13196"
    ]
    graphics [
      x 705.0754222163155
      y 1705.4082677661581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A50385"
      hgnc "NA"
      map_id "M122_189"
      name "Panhematin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 431.60285488631894
      y 1808.2869618156728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A356416"
      hgnc "NA"
      map_id "M122_277"
      name "dALA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 366.1563861500183
      y 1897.2643859940863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15346"
      hgnc "NA"
      map_id "M122_279"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa71"
      uniprot "NA"
    ]
    graphics [
      x 584.6100613270283
      y 1623.3428376278816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M122_278"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 476.31037510491296
      y 1729.6654350805625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_172"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 620.1444561425592
      y 980.7945583952934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 522.2235326771464
      y 1091.0672666788319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000110911;urn:miriam:hgnc.symbol:SLC11A2;urn:miriam:hgnc.symbol:SLC11A2;urn:miriam:refseq:NM_000617;urn:miriam:uniprot:P49281;urn:miriam:hgnc:10908;urn:miriam:ncbigene:4891;urn:miriam:ncbigene:4891"
      hgnc "HGNC_SYMBOL:SLC11A2"
      map_id "M122_173"
      name "SLC11A2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa164"
      uniprot "UNIPROT:P49281"
    ]
    graphics [
      x 414.2439964949359
      y 975.3139574565042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_213"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 615.0754754437971
      y 1271.6331441790635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_209"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa249"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1422.6073885746428
      y 175.81505970230728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 1309.4051498560227
      y 89.47424612363011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_182"
      name "NRF2"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 1193.782478433096
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:interpro:IPR000608"
      hgnc "NA"
      map_id "M122_282"
      name "E2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89"
      uniprot "NA"
    ]
    graphics [
      x 1696.461502132011
      y 1142.5312781703776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_122"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 1727.5247840096156
      y 979.4567974075337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_11"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:Q15843"
    ]
    graphics [
      x 1631.3546742890903
      y 985.8675319016347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:interpro:IPR000608;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:CUL3;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:KEAP1"
      map_id "M122_30"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:Q15843"
    ]
    graphics [
      x 1777.6051270510513
      y 697.2704731417123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "NA"
      map_id "M122_218"
      name "Bilirubin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 918.6617184561296
      y 868.965409691941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_64"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 834.5755436195195
      y 563.4772665181142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ensembl:ENSG00000103222;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ncbigene:213;urn:miriam:ncbigene:4363;urn:miriam:ncbigene:4363;urn:miriam:uniprot:P33527;urn:miriam:uniprot:P33527;urn:miriam:ec-code:7.6.2.2;urn:miriam:ec-code:7.6.2.3;urn:miriam:refseq:NM_004996;urn:miriam:hgnc:51;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ABCC1;HGNC_SYMBOL:ALB"
      map_id "M122_239"
      name "ABCC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P02768;UNIPROT:P33527"
    ]
    graphics [
      x 850.4465122455044
      y 360.8618922119025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "NA"
      map_id "M122_233"
      name "Bilirubin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa38"
      uniprot "NA"
    ]
    graphics [
      x 715.2858400555192
      y 442.86734344666775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57845"
      hgnc "NA"
      map_id "M122_130"
      name "HMBL"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa105"
      uniprot "NA"
    ]
    graphics [
      x 176.0529962919578
      y 1090.6505373192822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 262.28617463064404
      y 790.7777456438725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:refseq:NM_000375;urn:miriam:hgnc.symbol:UROS;urn:miriam:hgnc.symbol:UROS;urn:miriam:ensembl:ENSG00000188690;urn:miriam:ncbigene:7390;urn:miriam:ncbigene:7390;urn:miriam:uniprot:P10746;urn:miriam:hgnc:12592;urn:miriam:ec-code:4.2.1.75"
      hgnc "HGNC_SYMBOL:UROS"
      map_id "M122_134"
      name "UROS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa112"
      uniprot "UNIPROT:P10746"
    ]
    graphics [
      x 359.2379650728741
      y 750.7673048185738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15437"
      hgnc "NA"
      map_id "M122_133"
      name "URO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa111"
      uniprot "NA"
    ]
    graphics [
      x 507.33847517167874
      y 577.3393379566108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_179"
      name "ROS"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 272.5956868869541
      y 1225.9069391760365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 357.4347972456993
      y 1312.9448983832656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_174"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa165"
      uniprot "NA"
    ]
    graphics [
      x 200.67975382145585
      y 1442.3866779597545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:25847972;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re177"
      uniprot "NA"
    ]
    graphics [
      x 2019.992939531036
      y 1549.799337178574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:NLRP3"
      map_id "M122_28"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa74"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1928.8536403623789
      y 1719.000724481532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M122_254"
      name "SARS_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 2064.360637147217
      y 1426.7219330581302
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M122_267"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa528"
      uniprot "NA"
    ]
    graphics [
      x 1993.7349683410828
      y 1642.9213186232867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M122_266"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 2150.4985453844515
      y 1460.4492992512069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "NA"
      map_id "M122_253"
      name "SARS_space_Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1921.5934872733378
      y 1579.0634403658391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_263"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa524"
      uniprot "NA"
    ]
    graphics [
      x 2074.2916169170994
      y 1368.149996719205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "M122_258"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 2125.310121938745
      y 1592.2358538877772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "M122_259"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa495"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 2174.866198565812
      y 1532.7495471700167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_252"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa468"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1874.5097079947363
      y 1409.5893754722572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167996;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTH1"
      map_id "M122_159"
      name "FTH1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P02794"
    ]
    graphics [
      x 803.1780130085901
      y 747.4240187235503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_101"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 661.9673018557494
      y 898.3162955992285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02794;urn:miriam:ncbigene:2512;urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:hgnc.symbol:FTL;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999;urn:miriam:ensembl:ENSG00000167996;urn:miriam:ec-code:1.16.3.1;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTL;HGNC_SYMBOL:FTH1"
      map_id "M122_3"
      name "Ferritin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P02794;UNIPROT:P02792"
    ]
    graphics [
      x 584.7244165957776
      y 1123.6770306859557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 1808.245740384743
      y 437.3511746933756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:interpro:IPR000608;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:CUL3;HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:RBX1"
      map_id "M122_1"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:P0CG48;UNIPROT:Q15843"
    ]
    graphics [
      x 1761.122313474304
      y 232.2709423949325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_262"
      name "s782"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa522"
      uniprot "NA"
    ]
    graphics [
      x 2025.7787602150456
      y 1157.832084247268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 1909.8818388506318
      y 1290.669179687647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M122_217"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1593.1745385967304
      y 1346.247711148537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_194"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 1392.1792973512834
      y 1034.1213287324163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 1209.0291011432585
      y 1383.6462623794298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_204"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 1311.8879238917955
      y 1445.593721226874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M122_210"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 1357.1881539374317
      y 1413.107709978521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_177"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1352.7384587369118
      y 1634.1902521008296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M122_200"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1076.9848719297804
      y 1327.2695836880841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_211"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 1376.8415063078314
      y 1360.6144617301388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M122_212"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 1278.454457028813
      y 1245.7580644850837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_23"
      name "Neddylated_space_CUL3:RBX1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P62877;UNIPROT:Q13618;UNIPROT:Q15843"
    ]
    graphics [
      x 1713.4393190692936
      y 899.9656748759601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:16449638;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_121"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 1576.1076011259825
      y 884.9594301475279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1"
      map_id "M122_25"
      name "NRF2:KEAP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q16236;UNIPROT:Q14145"
    ]
    graphics [
      x 1777.6547236314432
      y 785.2140620757323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:CAND1;urn:miriam:hgnc.symbol:CAND1;urn:miriam:uniprot:Q86VP6;urn:miriam:refseq:NM_018448;urn:miriam:ncbigene:55832;urn:miriam:ncbigene:55832;urn:miriam:hgnc:30688;urn:miriam:ensembl:ENSG00000111530"
      hgnc "HGNC_SYMBOL:CAND1"
      map_id "M122_183"
      name "CAND1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa189"
      uniprot "UNIPROT:Q86VP6"
    ]
    graphics [
      x 1433.6646251482146
      y 865.6574416974642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_164"
      name "BLVRB"
      node_subtype "GENE"
      node_type "species"
      org_id "sa149"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 1480.2121509777935
      y 1362.4348183887287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_107"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1353.4465129371706
      y 1288.2996396549775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_241"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1194.4321090059009
      y 966.474243578197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_166"
      name "BLVRB"
      node_subtype "RNA"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 1401.555341456804
      y 1495.7429814629143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15439"
      hgnc "NA"
      map_id "M122_137"
      name "COPRO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa115"
      uniprot "NA"
    ]
    graphics [
      x 1471.9394420786366
      y 244.23814159268738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1640.836435193912
      y 249.28401845152075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_141"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa121"
      uniprot "NA"
    ]
    graphics [
      x 1765.028919196907
      y 320.1432825516398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000080819;urn:miriam:hgnc:2321;urn:miriam:refseq:NM_000097;urn:miriam:hgnc.symbol:CPOX;urn:miriam:hgnc.symbol:CPOX;urn:miriam:ec-code:1.3.3.3;urn:miriam:ncbigene:1371;urn:miriam:ncbigene:1371;urn:miriam:uniprot:P36551"
      hgnc "HGNC_SYMBOL:CPOX"
      map_id "M122_140"
      name "CPOX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa118"
      uniprot "UNIPROT:P36551"
    ]
    graphics [
      x 1682.867267610039
      y 406.1048927638369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15435"
      hgnc "NA"
      map_id "M122_138"
      name "PPGEN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa116"
      uniprot "NA"
    ]
    graphics [
      x 1540.9047507282658
      y 362.1723246686282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M122_142"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa122"
      uniprot "NA"
    ]
    graphics [
      x 1609.8525544752183
      y 443.49080094224155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M122_143"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa123"
      uniprot "NA"
    ]
    graphics [
      x 1658.1190153336868
      y 335.2626792489003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 457.0460496120088
      y 1305.7424219913141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:8031;urn:miriam:refseq:NM_005437;urn:miriam:ncbigene:8031;urn:miriam:uniprot:Q13772;urn:miriam:ensembl:ENSG00000266412;urn:miriam:hgnc:7671;urn:miriam:hgnc.symbol:NCOA4;urn:miriam:hgnc.symbol:NCOA4"
      hgnc "HGNC_SYMBOL:NCOA4"
      map_id "M122_156"
      name "NCOA4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:Q13772"
    ]
    graphics [
      x 347.1938406754026
      y 1399.2814161469032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_171"
      name "Ferritin"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 433.48125609852355
      y 1478.191326312022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_246"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa462"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1906.120097734432
      y 1026.2488846483996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re175"
      uniprot "NA"
    ]
    graphics [
      x 1914.293716839301
      y 1226.6173968901046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_243"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa457"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1833.3761552739988
      y 1145.462613870067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_248"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa464"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 2002.3081258468426
      y 1450.7328177015836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_244"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa460"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1798.0593622719616
      y 1226.5411283681342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M122_153"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 602.8414307273429
      y 1067.3708618379014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 812.2142301745374
      y 1518.1103482755798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TF"
      map_id "M122_198"
      name "Transferrin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa214"
      uniprot "UNIPROT:P02787"
    ]
    graphics [
      x 896.0035391190127
      y 1782.729225912448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TF"
      map_id "M122_14"
      name "holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:P02787"
    ]
    graphics [
      x 958.2393053917245
      y 1704.289643972303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_9"
      name "CUL3:RBX1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:P62877;UNIPROT:Q13618"
    ]
    graphics [
      x 1976.1845255626113
      y 1013.5507624450299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 1835.8917744432147
      y 970.6630141529635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 360.18543824060123
      y 1140.399559570298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NP59;urn:miriam:obo.chebi:CHEBI%3A29036;urn:miriam:uniprot:P00450;urn:miriam:ncbigene:30061;urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:obo.chebi:CHEBI%3A29036;urn:miriam:hgnc:2295;urn:miriam:ensembl:ENSG00000047457;urn:miriam:ec-code:1.16.3.1;urn:miriam:hgnc.symbol:CP;urn:miriam:hgnc.symbol:CP;urn:miriam:refseq:NM_000096;urn:miriam:uniprot:P00450;urn:miriam:uniprot:P00450;urn:miriam:ncbigene:1356;urn:miriam:ncbigene:1356"
      hgnc "HGNC_SYMBOL:SLC40A1;HGNC_SYMBOL:CP"
      map_id "M122_4"
      name "SLC40A1:CP:Cu2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q9NP59;UNIPROT:P00450"
    ]
    graphics [
      x 251.5443252297339
      y 1068.9149270601652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_150"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 325.04580416506974
      y 927.8395310100595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 1009.2660868219128
      y 1196.5271537497276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M122_219"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 1006.7855774774176
      y 1308.510620021391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:ec-code:1.5.1.30;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ec-code:1.3.1.24;urn:miriam:ncbigene:645;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_221"
      name "BLVRB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 1090.9700844763277
      y 1427.880183851221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29805;urn:miriam:uniprot:P53004;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ncbigene:644;urn:miriam:ncbigene:644;urn:miriam:ec-code:1.3.1.24;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_12"
      name "BLVRA:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 1202.680960420283
      y 1236.21972983408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M122_220"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 946.9486689100729
      y 1300.9119718045588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_71"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 620.7510145736933
      y 502.6076479220259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:ncbigene:213;urn:miriam:ensembl:ENSG00000163631;urn:miriam:ncbigene:213;urn:miriam:hgnc:399;urn:miriam:hgnc.symbol:ALB;urn:miriam:refseq:NM_000477;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ALB"
      map_id "M122_240"
      name "ALB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:P02768"
    ]
    graphics [
      x 681.0988055353785
      y 682.9908453237732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:obo.chebi:CHEBI%3A16990;urn:miriam:uniprot:P02768;urn:miriam:ncbigene:213;urn:miriam:ensembl:ENSG00000163631;urn:miriam:ncbigene:213;urn:miriam:hgnc:399;urn:miriam:hgnc.symbol:ALB;urn:miriam:refseq:NM_000477;urn:miriam:hgnc.symbol:ALB;urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "HGNC_SYMBOL:ALB"
      map_id "M122_17"
      name "ALB_slash_BIL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P02768"
    ]
    graphics [
      x 548.3476519011201
      y 392.0054614645678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re156"
      uniprot "NA"
    ]
    graphics [
      x 1271.7792328971766
      y 1568.3173113587814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_264"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa525"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1522.0482161569284
      y 1650.3151307209857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167996;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTH1"
      map_id "M122_157"
      name "FTH1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa142"
      uniprot "UNIPROT:P02794"
    ]
    graphics [
      x 1012.5404286122796
      y 618.7585512204937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_99"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1020.2630789713551
      y 745.8884167181748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TFRC;HGNC_SYMBOL:TF"
      map_id "M122_18"
      name "TFRC:TF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1028.821208483561
      y 2195.9438160546124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1104.0305743209167
      y 2038.117671482054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TFRC;HGNC_SYMBOL:TF"
      map_id "M122_19"
      name "TFRC:TF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1044.2559575077075
      y 2120.879444506757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ensembl:ENSG00000103222;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ncbigene:213;urn:miriam:ncbigene:4363;urn:miriam:ncbigene:4363;urn:miriam:uniprot:P33527;urn:miriam:uniprot:P33527;urn:miriam:ec-code:7.6.2.2;urn:miriam:ec-code:7.6.2.3;urn:miriam:refseq:NM_004996;urn:miriam:hgnc:51;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ABCC1;HGNC_SYMBOL:ALB"
      map_id "M122_181"
      name "ABCC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P02768;UNIPROT:P33527"
    ]
    graphics [
      x 813.9551507021357
      y 240.45619214115482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 916.0669989745638
      y 227.77280316012366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M122_180"
      name "ORF9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 1028.6485258313614
      y 241.76322075736778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_91"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 250.44714777134016
      y 2017.682402514486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A356416"
      hgnc "NA"
      map_id "M122_280"
      name "dALA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa72"
      uniprot "NA"
    ]
    graphics [
      x 358.41387896211177
      y 2000.275693069469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_60"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re162"
      uniprot "NA"
    ]
    graphics [
      x 1763.155974439478
      y 1405.7848985958226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:obo.chebi:CHEBI%3A36080;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "M122_31"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa81"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 1989.0124001764348
      y 1211.4359170845846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "M122_224"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1693.1744053980237
      y 1977.9405511320465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_56"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re150"
      uniprot "NA"
    ]
    graphics [
      x 1592.2834449436505
      y 1930.5063278897774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M122_225"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1532.7101550934326
      y 1770.4421243316522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:TXN"
      map_id "M122_24"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa69"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1760.5994337422899
      y 2008.3485645195187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:refseq:NM_000140;urn:miriam:hgnc.symbol:FECH;urn:miriam:uniprot:P22830;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_162"
      name "FECH"
      node_subtype "RNA"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1303.7269131842463
      y 766.263650167278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_104"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1311.259097701599
      y 871.3358575835632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_61"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 2010.7345640286208
      y 1302.2137667097734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_231"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa376"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1930.533834138033
      y 1410.7399954040527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:CASP1"
      map_id "M122_29"
      name "NLRP3_space_oligomer:ASC:Caspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa78"
      uniprot "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 2109.112138665271
      y 1162.7002752782807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:29717933;PUBMED:31827672"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_76"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1212.515035031068
      y 1064.8564240981289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573"
      hgnc "HGNC_SYMBOL:MAF"
      map_id "M122_260"
      name "MAF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:O75444"
    ]
    graphics [
      x 1335.496585649389
      y 1021.4996801914729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:MAF"
      map_id "M122_10"
      name "Nrf2_slash_Maf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q16236;UNIPROT:O75444"
    ]
    graphics [
      x 1077.3085514922118
      y 1127.7345466743454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_69"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re176"
      uniprot "NA"
    ]
    graphics [
      x 2077.915153085083
      y 1635.7090748386975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_250"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 2064.7302283066892
      y 1764.228329901379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TFRC"
      map_id "M122_199"
      name "TFRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa217"
      uniprot "UNIPROT:P02786"
    ]
    graphics [
      x 1036.5785076721259
      y 1968.9408403086918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1084.2043121849938
      y 1870.6994421903144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TF;HGNC_SYMBOL:TFRC"
      map_id "M122_15"
      name "TFRC:holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1191.5264354727974
      y 1992.3491955993818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "M122_229"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa374"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 775.4567826456728
      y 1430.0785212800513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_57"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 884.673561125491
      y 1482.2793276805032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "M122_228"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa373"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 753.1612285055352
      y 1503.0593430443437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_203"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 697.4492014256288
      y 1769.8255003171073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_45"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 668.5866808342205
      y 1980.4325345419675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9GZU1;urn:miriam:hgnc:13356;urn:miriam:refseq:NM_020533;urn:miriam:ncbigene:57192;urn:miriam:ncbigene:57192;urn:miriam:hgnc.symbol:MCOLN1;urn:miriam:ensembl:ENSG00000090674;urn:miriam:hgnc.symbol:MCOLN1"
      hgnc "HGNC_SYMBOL:MCOLN1"
      map_id "M122_206"
      name "MCOLN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa241"
      uniprot "UNIPROT:Q9GZU1"
    ]
    graphics [
      x 582.8048373198709
      y 1959.4208723486431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_205"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa240"
      uniprot "NA"
    ]
    graphics [
      x 697.0176108072569
      y 1853.656910494783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_273"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 2082.363136309308
      y 842.7429401375966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:12198130;PUBMED:31692987"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 1971.3050171609825
      y 889.7766998820766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:PKC;urn:miriam:pubmed:12198130;urn:miriam:interpro:IPR012233"
      hgnc "NA"
      map_id "M122_184"
      name "PKC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa190"
      uniprot "UNIPROT:PKC"
    ]
    graphics [
      x 1932.056861359996
      y 801.761392184329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_265"
      name "CK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 2137.722438357218
      y 886.8013392697756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_215"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1764.0527145460342
      y 859.6005727961656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M122_201"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa229"
      uniprot "NA"
    ]
    graphics [
      x 785.5907915708505
      y 2157.809270455705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 745.8547297811656
      y 1970.4813431140394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "M122_207"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa242"
      uniprot "NA"
    ]
    graphics [
      x 839.235445173646
      y 1888.0641711357796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:refseq:NM_018234;urn:miriam:uniprot:Q658P3;urn:miriam:ensembl:ENSG00000115107;urn:miriam:ec-code:1.16.1.-;urn:miriam:hgnc.symbol:STEAP3;urn:miriam:hgnc.symbol:STEAP3;urn:miriam:ncbigene:55240;urn:miriam:ncbigene:55240;urn:miriam:hgnc:24592"
      hgnc "HGNC_SYMBOL:STEAP3"
      map_id "M122_208"
      name "STEAP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa243"
      uniprot "UNIPROT:Q658P3"
    ]
    graphics [
      x 634.6108126872305
      y 1781.8562853243948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 2108.3301494122916
      y 985.3757687208745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M122_234"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa381"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 2038.4016588556763
      y 867.5023420494315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_238"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2222.976070186013
      y 942.3304252044916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_235"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa382"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1926.281884165549
      y 1056.2488846483996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:15282312;PUBMED:32132672;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_82"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1979.9321215655216
      y 720.0006202373602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:KEAP1"
      map_id "M122_281"
      name "KEAP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q14145"
    ]
    graphics [
      x 1904.3400675455928
      y 660.4966703834887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A76004"
      hgnc "NA"
      map_id "M122_146"
      name "Dimethly_space_fumarate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa126"
      uniprot "NA"
    ]
    graphics [
      x 2078.588540120012
      y 724.5734387466712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1482.7028327178218
      y 560.7758378631602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_144"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 1354.055390532488
      y 581.7942726268235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P50336;urn:miriam:obo.chebi:CHEBI%3A16238;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "NA"
      map_id "M122_8"
      name "PPO:FAD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P50336"
    ]
    graphics [
      x 1317.124752976465
      y 473.405398880612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15430"
      hgnc "NA"
      map_id "M122_139"
      name "PRIN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa117"
      uniprot "NA"
    ]
    graphics [
      x 1422.3526270611892
      y 664.847329322328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M122_145"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa125"
      uniprot "NA"
    ]
    graphics [
      x 1307.914759927888
      y 539.8945259246981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273"
      hgnc "HGNC_SYMBOL:BACH1"
      map_id "M122_256"
      name "BACH1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa49"
      uniprot "UNIPROT:O14867"
    ]
    graphics [
      x 1902.1092993175612
      y 560.1358894615735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:21982894;PUBMED:28082120"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_78"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1769.0013931506164
      y 604.283961606868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:406947"
      hgnc "NA"
      map_id "M122_255"
      name "miRNA_minus_155"
      node_subtype "RNA"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 1847.3881110722264
      y 515.9918778695632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273;urn:miriam:ncbigene:571"
      hgnc "HGNC_SYMBOL:BACH1"
      map_id "M122_242"
      name "BACH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:O14867"
    ]
    graphics [
      x 1640.8183174165606
      y 781.361924757158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:644;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_163"
      name "BLVRA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 1512.198752713489
      y 1215.6936907608474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_106"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1391.054303410384
      y 1099.3395722153211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:644;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_165"
      name "BLVRA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa150"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 1507.9674085582913
      y 1130.1985737037535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_247"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa463"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1356.0006086214719
      y 1188.6101182283292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 1432.7092774742353
      y 1300.9807595231887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_249"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa465"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1346.9501543365116
      y 1561.688457409432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_245"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa461"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1302.2025911018336
      y 1297.7622247826555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999"
      hgnc "HGNC_SYMBOL:FTL"
      map_id "M122_160"
      name "FTL"
      node_subtype "RNA"
      node_type "species"
      org_id "sa145"
      uniprot "UNIPROT:P02792"
    ]
    graphics [
      x 746.9170957382853
      y 814.73056276601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_102"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 592.578660543251
      y 912.7611394132257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_222"
      name "NLRP3"
      node_subtype "GENE"
      node_type "species"
      org_id "sa361"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 2249.9605052407805
      y 1792.7455440122992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_52"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 2136.1763961665592
      y 1863.4052005974902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:7795;urn:miriam:refseq:NM_001077494;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:ensembl:ENSG00000077150;urn:miriam:uniprot:Q00653;urn:miriam:uniprot:Q00653;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:RELA"
      map_id "M122_2"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 2188.3552091689567
      y 1730.8044285529222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_227"
      name "NLRP3"
      node_subtype "RNA"
      node_type "species"
      org_id "sa367"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1941.0637280189703
      y 1825.7672266839718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 1152.3443697327466
      y 2111.047627392869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TF;HGNC_SYMBOL:TFRC"
      map_id "M122_16"
      name "TFRC:holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 985.9398058343029
      y 2116.645546573645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1"
      hgnc "HGNC_SYMBOL:SLC40A1"
      map_id "M122_154"
      name "SLC40A1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:Q9NP59"
    ]
    graphics [
      x 891.9628759362893
      y 947.1582484555139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_98"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 925.1564438012163
      y 1026.62581173933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1"
      hgnc "HGNC_SYMBOL:SLC40A1"
      map_id "M122_155"
      name "SLC40A1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:Q9NP59"
    ]
    graphics [
      x 690.6841739903359
      y 1027.8634232539846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_124"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 1539.262066978889
      y 963.5006434054978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:FLVCR1;urn:miriam:uniprot:Q9Y5Y0;urn:miriam:ncbigene:28982"
      hgnc "HGNC_SYMBOL:FLVCR1"
      map_id "M122_186"
      name "FLVCR1_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa196"
      uniprot "UNIPROT:Q9Y5Y0"
    ]
    graphics [
      x 1615.9282429697023
      y 1065.1065881208187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_187"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 1553.1477888492996
      y 783.6694367174748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_108"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1233.3919650718035
      y 1599.6876525996358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_73"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 1327.9060602827278
      y 1783.8336443619003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_251"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa467"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1319.7836340385843
      y 1933.8127490773054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 335.70579910941876
      y 1026.8889844040516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_168"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 469.2766444012908
      y 1141.3882167576783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_169"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 483.99240108846243
      y 1001.4317787226462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_170"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158"
      uniprot "NA"
    ]
    graphics [
      x 221.43358997020744
      y 969.9897492294203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58126"
      hgnc "NA"
      map_id "M122_127"
      name "PBG"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa101"
      uniprot "NA"
    ]
    graphics [
      x 316.98745749987165
      y 1627.110700500848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 249.05728314364183
      y 1388.4417519268102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_131"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa106"
      uniprot "NA"
    ]
    graphics [
      x 397.0767253139561
      y 1436.5184575703342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08397;urn:miriam:obo.chebi:CHEBI%3A36319;urn:miriam:hgnc:4982;urn:miriam:uniprot:P08397;urn:miriam:uniprot:P08397;urn:miriam:ec-code:2.5.1.61;urn:miriam:hgnc.symbol:HMBS;urn:miriam:refseq:NM_000190;urn:miriam:ensembl:ENSG00000256269;urn:miriam:hgnc.symbol:HMBS;urn:miriam:ncbigene:3145;urn:miriam:ncbigene:3145"
      hgnc "HGNC_SYMBOL:HMBS"
      map_id "M122_7"
      name "HMBS:DIPY"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P08397"
    ]
    graphics [
      x 250.83391250278225
      y 1293.7289264933024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M122_132"
      name "NH4_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa107"
      uniprot "NA"
    ]
    graphics [
      x 458.54223296041596
      y 1435.692090908048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_93"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 1367.7567785527494
      y 757.207895944283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A61051"
      hgnc "NA"
      map_id "M122_149"
      name "Lipid_space_Peroxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa130"
      uniprot "NA"
    ]
    graphics [
      x 302.55371341721536
      y 1771.571246049859
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_117"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 137.86476998569685
      y 1790.7427922282245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0097707"
      hgnc "NA"
      map_id "M122_147"
      name "Ferroptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa127"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1686.8737190250454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 652.8978101154396
      y 1811.8562853243948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167468;urn:miriam:ncbigene:2879;urn:miriam:ncbigene:2879;urn:miriam:hgnc:4556;urn:miriam:hgnc.symbol:GPX4;urn:miriam:hgnc.symbol:GPX4;urn:miriam:refseq:NM_002085;urn:miriam:uniprot:P36969;urn:miriam:ec-code:1.11.1.12"
      hgnc "HGNC_SYMBOL:GPX4"
      map_id "M122_176"
      name "GPX4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P36969"
    ]
    graphics [
      x 827.5294930927662
      y 1736.9374562017142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24026"
      hgnc "NA"
      map_id "M122_175"
      name "Lipid_space_alcohol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 771.1143662215175
      y 1704.6835553517872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:refseq:NM_000140;urn:miriam:hgnc.symbol:FECH;urn:miriam:uniprot:P22830;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_161"
      name "FECH"
      node_subtype "GENE"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1223.9996997273133
      y 684.6947607279295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:23766848;PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_103"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1190.1031750675206
      y 787.7318019712561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 807.1378688087198
      y 449.3187369087376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P06132;urn:miriam:refseq:NM_000374;urn:miriam:ensembl:ENSG00000126088;urn:miriam:hgnc.symbol:UROD;urn:miriam:hgnc.symbol:UROD;urn:miriam:ncbigene:7389;urn:miriam:ncbigene:7389;urn:miriam:ec-code:4.1.1.37;urn:miriam:hgnc:12591"
      hgnc "HGNC_SYMBOL:UROD"
      map_id "M122_136"
      name "UROD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa114"
      uniprot "UNIPROT:P06132"
    ]
    graphics [
      x 934.6542091897355
      y 444.9400368854938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15439"
      hgnc "NA"
      map_id "M122_135"
      name "COPRO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa113"
      uniprot "NA"
    ]
    graphics [
      x 1105.2777359055187
      y 403.50468634305844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:28082120"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_77"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1450.1135624005842
      y 933.4438720206672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273;urn:miriam:ncbigene:571"
      hgnc "HGNC_SYMBOL:MAF;HGNC_SYMBOL:BACH1"
      map_id "M122_21"
      name "BACH1_slash_Maf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:O75444;UNIPROT:O14867"
    ]
    graphics [
      x 1285.0021224413213
      y 987.5672556437707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999"
      hgnc "HGNC_SYMBOL:FTL"
      map_id "M122_158"
      name "FTL"
      node_subtype "GENE"
      node_type "species"
      org_id "sa143"
      uniprot "UNIPROT:P02792"
    ]
    graphics [
      x 848.1908531916406
      y 889.418494436243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_100"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 971.0767203571312
      y 827.4798912865067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_261"
      name "HMOX1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1157.7610830881224
      y 1317.9864418319926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_105"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1249.844187347859
      y 1500.7466970958958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_214"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1311.0060971188004
      y 1701.248273290216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 1409.9531237303727
      y 1807.8944458218834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M122_178"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1439.8118309091024
      y 1936.1865253940427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_48"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 934.0039801824305
      y 1997.0904721202744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_59"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1799.7454941703115
      y 1822.1282334248112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 1616.7967204109975
      y 103.73133945278516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:interpro:IPR000608;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:CUL3"
      map_id "M122_20"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:P0CG48;UNIPROT:Q15843"
    ]
    graphics [
      x 1517.0465694957607
      y 152.81509068662922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_126"
      name "HMOX1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1036.023133392067
      y 1075.8398490652357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:29717933;PUBMED:31827672;PUBMED:10473555"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_79"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1161.5853203949207
      y 1120.1728969901264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_50"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1163.5171773301302
      y 1197.3057063157833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:FLVCR1;urn:miriam:uniprot:Q9Y5Y0;urn:miriam:ncbigene:28982"
      hgnc "HGNC_SYMBOL:FLVCR1"
      map_id "M122_216"
      name "FLVCR1_minus_2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q9Y5Y0"
    ]
    graphics [
      x 1250.1709280263638
      y 1328.9988651631238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 687.0452013591525
      y 1562.1376362984129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 729.484634498172
      y 1165.433952620463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_151"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 779.1231275934681
      y 1257.8221183431824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_152"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 880.9566364561936
      y 1200.143760801773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A78619"
      hgnc "NA"
      map_id "M122_167"
      name "Fe(3_plus_)O(OH)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 827.6406940568581
      y 1126.673213722226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re101"
      uniprot "NA"
    ]
    graphics [
      x 483.25568034024195
      y 793.7406476567921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_196"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 390.5161981038967
      y 681.8156423814595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_197"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa213"
      uniprot "NA"
    ]
    graphics [
      x 469.6883574533804
      y 672.2833843772762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NP59;urn:miriam:obo.chebi:CHEBI%3A28694;urn:miriam:uniprot:Q9BQS7;urn:miriam:ncbigene:30061;urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:ec-code:1.-.-.-;urn:miriam:ensembl:ENSG00000089472;urn:miriam:uniprot:Q9BQS7;urn:miriam:uniprot:Q9BQS7;urn:miriam:hgnc:4866;urn:miriam:hgnc.symbol:HEPH;urn:miriam:hgnc.symbol:HEPH;urn:miriam:ncbigene:9843;urn:miriam:refseq:NM_138737;urn:miriam:ncbigene:9843;urn:miriam:obo.chebi:CHEBI%3A29036"
      hgnc "HGNC_SYMBOL:SLC40A1;HGNC_SYMBOL:HEPH"
      map_id "M122_13"
      name "SLC40A1:HEPH:Cu2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:Q9NP59;UNIPROT:Q9BQS7"
    ]
    graphics [
      x 474.87244351744846
      y 913.1766073876688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_195"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa211"
      uniprot "NA"
    ]
    graphics [
      x 695.0698925780742
      y 652.9908453237732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_42"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 904.334376705659
      y 2252.0751733882953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 470.5912005387171
      y 1846.6557802348937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:P13716;urn:miriam:refseq:NM_001003945;urn:miriam:ec-code:4.2.1.24;urn:miriam:ensembl:ENSG00000148218;urn:miriam:hgnc:395;urn:miriam:ncbigene:210;urn:miriam:ncbigene:210;urn:miriam:hgnc.symbol:ALAD;urn:miriam:uniprot:P13716;urn:miriam:uniprot:P13716;urn:miriam:hgnc.symbol:ALAD;urn:miriam:obo.chebi:CHEBI%3A29105"
      hgnc "HGNC_SYMBOL:ALAD"
      map_id "M122_6"
      name "ALAD:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P13716"
    ]
    graphics [
      x 624.031110579815
      y 1692.6823927001783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_129"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa104"
      uniprot "NA"
    ]
    graphics [
      x 439.08260907007343
      y 1694.848379061969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_128"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa103"
      uniprot "NA"
    ]
    graphics [
      x 464.8071495821239
      y 1985.4395347253567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_63"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re165"
      uniprot "NA"
    ]
    graphics [
      x 748.4238735544548
      y 1336.2292481317186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:ensembl:ENSG00000140368;urn:miriam:hgnc:9580;urn:miriam:ncbigene:9051;urn:miriam:ncbigene:9051;urn:miriam:refseq:NM_003978;urn:miriam:uniprot:O43586"
      hgnc "HGNC_SYMBOL:PSTPIP1"
      map_id "M122_232"
      name "PSTPIP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa377"
      uniprot "UNIPROT:O43586"
    ]
    graphics [
      x 670.3912725950297
      y 1190.0016719595417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O15553;urn:miriam:uniprot:O43586;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:ensembl:ENSG00000140368;urn:miriam:hgnc:9580;urn:miriam:ncbigene:9051;urn:miriam:ncbigene:9051;urn:miriam:refseq:NM_003978;urn:miriam:uniprot:O43586"
      hgnc "HGNC_SYMBOL:MEFV;HGNC_SYMBOL:PSTPIP1"
      map_id "M122_32"
      name "PSTPIP1_space_trimer:Pyrin_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "UNIPROT:O15553;UNIPROT:O43586"
    ]
    graphics [
      x 622.3226147543232
      y 1354.43132078556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_110"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 438.01071105428366
      y 1035.4902425919886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 732.3983904512074
      y 897.5590643801858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "M122_185"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 852.8418960571935
      y 813.9211124592244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q53TN4;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:ec-code:1.-.-.-;urn:miriam:hgnc.symbol:CYBRD1;urn:miriam:ncbigene:79901;urn:miriam:hgnc.symbol:CYBRD1;urn:miriam:ncbigene:79901;urn:miriam:ensembl:ENSG00000071967;urn:miriam:hgnc:20797;urn:miriam:uniprot:Q53TN4;urn:miriam:uniprot:Q53TN4;urn:miriam:refseq:NM_024843"
      hgnc "HGNC_SYMBOL:CYBRD1"
      map_id "M122_5"
      name "CYBRD1:Heme"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q53TN4"
    ]
    graphics [
      x 877.9985580919956
      y 759.771737038765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_92"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 1307.6818730860703
      y 308.7277752125451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26208"
      hgnc "NA"
      map_id "M122_148"
      name "Poly_minus_unsaturated_space_fatty_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 66.78500005238766
      y 1577.0844175109974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 180.153156406755
      y 1615.1620552959507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re149"
      uniprot "NA"
    ]
    graphics [
      x 1635.403519720017
      y 1879.7065822261395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_257"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa493"
      uniprot "NA"
    ]
    graphics [
      x 1704.7539611880347
      y 2046.034307534281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M122_226"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa366"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1609.7379406038579
      y 2029.7749094346884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_53"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re148"
      uniprot "NA"
    ]
    graphics [
      x 1735.4906121776694
      y 1753.3842640694588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_125"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 1515.27179801197
      y 659.6049778712547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_193"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa206"
      uniprot "NA"
    ]
    graphics [
      x 1452.1674023887226
      y 527.8907175389451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M122_190"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1416.2670432053596
      y 594.0531502502163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000118777;urn:miriam:uniprot:Q9UNQ0;urn:miriam:hgnc.symbol:ABCG2;urn:miriam:hgnc.symbol:ABCG2;urn:miriam:hgnc:74;urn:miriam:ncbigene:9429;urn:miriam:ncbigene:9429;urn:miriam:refseq:NM_004827;urn:miriam:ec-code:7.6.2.2"
      hgnc "HGNC_SYMBOL:ABCG2"
      map_id "M122_188"
      name "ABCG2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa198"
      uniprot "UNIPROT:Q9UNQ0"
    ]
    graphics [
      x 1384.9695902906733
      y 513.3272832160001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M122_192"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 1518.0742216241415
      y 508.1613063492214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M122_191"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa204"
      uniprot "NA"
    ]
    graphics [
      x 1473.8477161341161
      y 472.53084857329736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_109"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1406.912217767886
      y 1205.3633928882439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 423.77291497840747
      y 1091.5558405191814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      annotation "PUBMED:12198130"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 1497.6195460068693
      y 879.9274802670519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_51"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re146"
      uniprot "NA"
    ]
    graphics [
      x 1726.3841444193013
      y 2145.3539199275624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 283
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_230"
      target_id "M122_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_33"
      target_id "M122_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_62"
      target_id "M122_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_237"
      target_id "M122_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_236"
      target_id "M122_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_66"
      target_id "M122_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_26"
      target_id "M122_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_75"
      target_id "M122_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_75"
      target_id "M122_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_269"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_270"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_268"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 17
    target 14
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_271"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 14
    target 18
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_80"
      target_id "M122_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 14
    target 19
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_80"
      target_id "M122_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 8
    target 20
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_36"
      target_id "M122_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_67"
      target_id "M122_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_274"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_276"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 25
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_275"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 26
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_22"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 18
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_202"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 27
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_189"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 23
    target 28
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 23
    target 29
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 23
    target 30
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_172"
      target_id "M122_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_173"
      target_id "M122_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 32
    target 34
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_113"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_209"
      target_id "M122_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_120"
      target_id "M122_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_282"
      target_id "M122_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_11"
      target_id "M122_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 39
    target 41
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_122"
      target_id "M122_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_218"
      target_id "M122_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_239"
      target_id "M122_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 43
    target 45
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_64"
      target_id "M122_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_130"
      target_id "M122_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_134"
      target_id "M122_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 47
    target 49
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_86"
      target_id "M122_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_179"
      target_id "M122_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 34
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_213"
      target_id "M122_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_116"
      target_id "M122_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 11
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_223"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_28"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_254"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 56
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_267"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 57
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_266"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 58
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_253"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 59
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_263"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 60
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_258"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 61
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_259"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_254"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 56
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_267"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 57
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_266"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 58
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_253"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 59
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_263"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 60
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_258"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 61
    target 53
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_259"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 53
    target 62
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_70"
      target_id "M122_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_159"
      target_id "M122_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_101"
      target_id "M122_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 41
    target 66
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_30"
      target_id "M122_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_83"
      target_id "M122_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_262"
      target_id "M122_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_217"
      target_id "M122_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 69
    target 59
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_74"
      target_id "M122_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_204"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 74
    target 72
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_210"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 75
    target 72
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_177"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 72
    target 76
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 72
    target 77
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 72
    target 34
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 72
    target 78
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 72
    target 70
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_23"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_25"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_183"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 80
    target 40
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_121"
      target_id "M122_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 83
    target 84
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_164"
      target_id "M122_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 85
    target 84
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 84
    target 86
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_107"
      target_id "M122_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_137"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_141"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_140"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 88
    target 91
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 88
    target 92
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 88
    target 93
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 65
    target 94
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_3"
      target_id "M122_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_156"
      target_id "M122_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_111"
      target_id "M122_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_246"
      target_id "M122_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 99
    target 98
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_243"
      target_id "M122_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 98
    target 100
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_68"
      target_id "M122_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 98
    target 101
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_68"
      target_id "M122_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_153"
      target_id "M122_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_198"
      target_id "M122_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_39"
      target_id "M122_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_9"
      target_id "M122_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 107
    target 79
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_119"
      target_id "M122_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 34
    target 108
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_4"
      target_id "M122_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 108
    target 110
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_96"
      target_id "M122_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 76
    target 111
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_200"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_219"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 113
    target 111
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_221"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 114
    target 111
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_12"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 111
    target 42
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_55"
      target_id "M122_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 111
    target 115
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_55"
      target_id "M122_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 45
    target 116
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_233"
      target_id "M122_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 117
    target 116
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_240"
      target_id "M122_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 116
    target 118
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_71"
      target_id "M122_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 12
    target 119
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_27"
      target_id "M122_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 120
    target 119
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_264"
      target_id "M122_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 119
    target 9
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_58"
      target_id "M122_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_157"
      target_id "M122_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 85
    target 122
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 122
    target 63
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_99"
      target_id "M122_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_18"
      target_id "M122_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_47"
      target_id "M122_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_181"
      target_id "M122_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "MODULATION"
      source_id "M122_180"
      target_id "M122_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 127
    target 44
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_118"
      target_id "M122_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 28
    target 129
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_277"
      target_id "M122_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_91"
      target_id "M122_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 62
    target 131
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_252"
      target_id "M122_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 1
    target 131
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_230"
      target_id "M122_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_60"
      target_id "M122_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_224"
      target_id "M122_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 135
    target 134
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_225"
      target_id "M122_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 134
    target 136
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_56"
      target_id "M122_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_162"
      target_id "M122_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 138
    target 16
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_104"
      target_id "M122_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 132
    target 139
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_31"
      target_id "M122_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_231"
      target_id "M122_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 139
    target 141
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_61"
      target_id "M122_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 85
    target 142
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_241"
      target_id "M122_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_260"
      target_id "M122_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 142
    target 144
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_76"
      target_id "M122_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 100
    target 145
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_248"
      target_id "M122_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 145
    target 146
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_69"
      target_id "M122_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_199"
      target_id "M122_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 105
    target 148
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_14"
      target_id "M122_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_40"
      target_id "M122_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_229"
      target_id "M122_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_228"
      target_id "M122_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 151
    target 12
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_57"
      target_id "M122_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_203"
      target_id "M122_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_206"
      target_id "M122_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 154
    target 156
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_45"
      target_id "M122_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_273"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 159
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_184"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 160
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_265"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 158
    target 161
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_123"
      target_id "M122_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_201"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 164
    target 163
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_207"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 165
    target 163
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_208"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 163
    target 153
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_43"
      target_id "M122_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 141
    target 166
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_29"
      target_id "M122_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 167
    target 166
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_234"
      target_id "M122_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 166
    target 5
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 166
    target 168
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 166
    target 169
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 166
    target 7
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 166
    target 132
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 157
    target 170
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_273"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_281"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 172
    target 170
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_146"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 170
    target 81
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_82"
      target_id "M122_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 91
    target 173
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_138"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_144"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 175
    target 173
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_8"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 173
    target 176
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_89"
      target_id "M122_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 173
    target 177
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_89"
      target_id "M122_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 178
    target 179
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_256"
      target_id "M122_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 180
    target 179
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_255"
      target_id "M122_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 179
    target 181
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_78"
      target_id "M122_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 182
    target 183
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_163"
      target_id "M122_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 85
    target 183
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_106"
      target_id "M122_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_247"
      target_id "M122_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 21
    target 186
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_35"
      target_id "M122_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 186
    target 187
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_72"
      target_id "M122_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 186
    target 188
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_72"
      target_id "M122_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 189
    target 190
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_160"
      target_id "M122_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 190
    target 65
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_102"
      target_id "M122_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 191
    target 192
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_222"
      target_id "M122_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 193
    target 192
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_2"
      target_id "M122_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 192
    target 194
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_52"
      target_id "M122_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 149
    target 195
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_15"
      target_id "M122_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_41"
      target_id "M122_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_154"
      target_id "M122_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 85
    target 198
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 198
    target 199
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_98"
      target_id "M122_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 71
    target 200
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 201
    target 200
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_186"
      target_id "M122_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 200
    target 202
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_124"
      target_id "M122_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 86
    target 203
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_166"
      target_id "M122_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 203
    target 113
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_108"
      target_id "M122_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 187
    target 204
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_249"
      target_id "M122_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_73"
      target_id "M122_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 110
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_150"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 207
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_168"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 208
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_169"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 109
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_4"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 206
    target 102
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_97"
      target_id "M122_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 206
    target 209
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_97"
      target_id "M122_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 210
    target 211
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_127"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 212
    target 211
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_131"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 213
    target 211
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_7"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 211
    target 46
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_85"
      target_id "M122_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 211
    target 214
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_85"
      target_id "M122_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 176
    target 215
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_139"
      target_id "M122_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 215
    target 13
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_93"
      target_id "M122_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_149"
      target_id "M122_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 217
    target 218
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_117"
      target_id "M122_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 216
    target 219
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_149"
      target_id "M122_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 220
    target 219
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_176"
      target_id "M122_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 219
    target 221
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_114"
      target_id "M122_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_161"
      target_id "M122_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 85
    target 223
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 223
    target 137
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_103"
      target_id "M122_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 49
    target 224
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_133"
      target_id "M122_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 225
    target 224
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_136"
      target_id "M122_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 224
    target 226
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_87"
      target_id "M122_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 143
    target 227
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_260"
      target_id "M122_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 181
    target 227
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_242"
      target_id "M122_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 227
    target 228
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_77"
      target_id "M122_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 229
    target 230
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_158"
      target_id "M122_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 85
    target 230
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 230
    target 189
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_100"
      target_id "M122_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_261"
      target_id "M122_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 232
    target 233
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_105"
      target_id "M122_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_214"
      target_id "M122_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 235
    target 234
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "MODULATION"
      source_id "M122_178"
      target_id "M122_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 234
    target 75
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_115"
      target_id "M122_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 125
    target 236
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_19"
      target_id "M122_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 236
    target 147
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_48"
      target_id "M122_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 236
    target 104
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_48"
      target_id "M122_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 11
    target 237
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_223"
      target_id "M122_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 133
    target 237
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_224"
      target_id "M122_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 237
    target 54
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_59"
      target_id "M122_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 67
    target 238
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_1"
      target_id "M122_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_49"
      target_id "M122_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 238
    target 35
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_49"
      target_id "M122_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 240
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_126"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 144
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_10"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 228
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_21"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 241
    target 231
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_79"
      target_id "M122_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 18
    target 242
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_202"
      target_id "M122_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 243
    target 242
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_216"
      target_id "M122_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 242
    target 71
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_50"
      target_id "M122_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 156
    target 244
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_205"
      target_id "M122_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 244
    target 34
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_46"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 34
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 246
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_151"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 247
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_152"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 65
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_3"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 245
    target 248
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_95"
      target_id "M122_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 110
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_150"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 250
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_196"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 251
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_197"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 252
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_13"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 249
    target 102
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_38"
      target_id "M122_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 249
    target 253
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_38"
      target_id "M122_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 196
    target 254
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_16"
      target_id "M122_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 254
    target 162
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_42"
      target_id "M122_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 254
    target 123
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_42"
      target_id "M122_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 130
    target 255
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_280"
      target_id "M122_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 256
    target 255
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_6"
      target_id "M122_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 255
    target 210
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 255
    target 257
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 255
    target 258
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 3
    target 259
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_33"
      target_id "M122_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 260
    target 259
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_232"
      target_id "M122_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 259
    target 261
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_63"
      target_id "M122_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 199
    target 262
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_155"
      target_id "M122_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 262
    target 109
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_110"
      target_id "M122_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 102
    target 263
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_153"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 264
    target 263
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_185"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 265
    target 263
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_5"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 263
    target 31
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_112"
      target_id "M122_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 226
    target 266
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_135"
      target_id "M122_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 266
    target 87
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_92"
      target_id "M122_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 267
    target 268
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_148"
      target_id "M122_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 52
    target 268
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_174"
      target_id "M122_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 268
    target 216
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_94"
      target_id "M122_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 135
    target 269
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_225"
      target_id "M122_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 270
    target 269
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_257"
      target_id "M122_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 269
    target 271
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_54"
      target_id "M122_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 194
    target 272
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_227"
      target_id "M122_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 272
    target 120
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_53"
      target_id "M122_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 71
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 274
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_193"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 275
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_190"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 276
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_188"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 273
    target 202
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 273
    target 277
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 273
    target 278
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 184
    target 279
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_165"
      target_id "M122_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 279
    target 114
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_109"
      target_id "M122_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 34
    target 280
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 252
    target 280
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_13"
      target_id "M122_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 280
    target 110
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_37"
      target_id "M122_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 161
    target 281
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_215"
      target_id "M122_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 281
    target 85
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_90"
      target_id "M122_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 136
    target 282
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_24"
      target_id "M122_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 270
    target 282
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_257"
      target_id "M122_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 282
    target 133
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_51"
      target_id "M122_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 282
    target 271
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_51"
      target_id "M122_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
