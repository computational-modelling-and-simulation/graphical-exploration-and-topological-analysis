# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:55922"
      hgnc "NA"
      map_id "W19_15"
      name "NKRF"
      node_subtype "GENE"
      node_type "species"
      org_id "ea9bc"
      uniprot "NA"
    ]
    graphics [
      x 309.05415272218744
      y 525.4322754809133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "acca6"
      uniprot "NA"
    ]
    graphics [
      x 222.39069462007933
      y 420.4599143284952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3122;urn:miriam:ncbigene:3123;urn:miriam:ncbigene:3126;urn:miriam:ncbigene:3127"
      hgnc "NA"
      map_id "W19_5"
      name "b533f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b533f"
      uniprot "NA"
    ]
    graphics [
      x 97.55652908665061
      y 455.0840307374763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_9"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c049f"
      uniprot "NA"
    ]
    graphics [
      x 179.3720738803044
      y 487.1906895459954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:7535;urn:miriam:ncbigene:3932;urn:miriam:ncbigene:2534"
      hgnc "NA"
      map_id "W19_13"
      name "dbed0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dbed0"
      uniprot "NA"
    ]
    graphics [
      x 97.63925628313518
      y 382.3695406302461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "cb292"
      uniprot "NA"
    ]
    graphics [
      x 255.18279578866952
      y 662.6285668789222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:919;urn:miriam:ncbigene:28639;urn:miriam:ncbigene:28755;urn:miriam:ncbigene:916;urn:miriam:ncbigene:917"
      hgnc "NA"
      map_id "W19_12"
      name "d2540"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2540"
      uniprot "NA"
    ]
    graphics [
      x 192.97708814785966
      y 767.504627609126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aff81"
      uniprot "NA"
    ]
    graphics [
      x 423.71044604196925
      y 451.89139565005405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3569"
      hgnc "NA"
      map_id "W19_8"
      name "bebbf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bebbf"
      uniprot "NA"
    ]
    graphics [
      x 524.6044118796424
      y 394.0824975090368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2a0ea132"
      uniprot "NA"
    ]
    graphics [
      x 175.27686707660928
      y 596.6491952635108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:920;urn:miriam:ncbigene:926;urn:miriam:ncbigene:925;urn:miriam:ncbigene:914"
      hgnc "NA"
      map_id "W19_11"
      name "cb65d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cb65d"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 645.1756023834638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikipathways:WP4846"
      hgnc "NA"
      map_id "W19_14"
      name "SARS_minus_CoV_minus_2_space_ORFs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e9691"
      uniprot "NA"
    ]
    graphics [
      x 180.50099469559757
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_19"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "idea6a7587"
      uniprot "NA"
    ]
    graphics [
      x 292.4194233288002
      y 96.70470561686506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikidata:Q87917572;urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32838362"
      hgnc "NA"
      map_id "W19_6"
      name "b7705"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7705"
      uniprot "NA"
    ]
    graphics [
      x 324.731522459846
      y 219.74012719605662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd72cdda3"
      uniprot "NA"
    ]
    graphics [
      x 440.0494266158078
      y 571.3814023921926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:5725029;urn:miriam:ncbigene:3929;urn:miriam:ncbigene:10394;urn:miriam:ncbigene:1401;urn:miriam:ncbigene:64386"
      hgnc "NA"
      map_id "W19_4"
      name "b120d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b120d"
      uniprot "NA"
    ]
    graphics [
      x 541.1284028570462
      y 629.2716224512087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8e00894d"
      uniprot "NA"
    ]
    graphics [
      x 327.4219645517809
      y 360.9440341266146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_1"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "aa453"
      uniprot "NA"
    ]
    graphics [
      x 363.14148588062153
      y 657.7587825179342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3497;urn:miriam:ncbigene:3500;urn:miriam:ncbigene:3501;urn:miriam:ncbigene:3543;urn:miriam:ncbigene:3538;urn:miriam:ncbigene:3503"
      hgnc "NA"
      map_id "W19_7"
      name "b7f53"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7f53"
      uniprot "NA"
    ]
    graphics [
      x 383.13335013234945
      y 775.3219527020201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 20
    source 1
    target 2
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 2
    target 3
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_2"
      target_id "W19_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 4
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 4
    target 5
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_9"
      target_id "W19_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 1
    target 6
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 6
    target 7
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_10"
      target_id "W19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 1
    target 8
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 8
    target 9
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_3"
      target_id "W19_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 1
    target 10
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 10
    target 11
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_16"
      target_id "W19_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 12
    target 13
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_14"
      target_id "W19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 13
    target 14
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_19"
      target_id "W19_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 1
    target 15
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 15
    target 16
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_18"
      target_id "W19_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 14
    target 17
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_6"
      target_id "W19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 17
    target 1
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_17"
      target_id "W19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 1
    target 18
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "W19_15"
      target_id "W19_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 18
    target 19
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_1"
      target_id "W19_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
