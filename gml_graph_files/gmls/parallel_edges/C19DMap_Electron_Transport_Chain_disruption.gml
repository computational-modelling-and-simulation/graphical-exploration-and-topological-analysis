# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_162"
      name "s1132"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa664"
      uniprot "NA"
    ]
    graphics [
      x 1237.428215723903
      y 1181.9055401242576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 1132.7196124973889
      y 1140.445246462755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:AIA62288"
      hgnc "NA"
      map_id "M13_147"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498"
      uniprot "NA"
    ]
    graphics [
      x 1116.2195129884171
      y 1003.3334142266777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:4715;urn:miriam:ncbigene:4715;urn:miriam:refseq:NM_005005;urn:miriam:ensembl:ENSG00000147684;urn:miriam:hgnc:7704;urn:miriam:hgnc.symbol:NDUFB9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:hgnc.symbol:NDUFB9"
      hgnc "HGNC_SYMBOL:NDUFB9"
      map_id "M13_160"
      name "NDUFB9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa661"
      uniprot "UNIPROT:Q9Y6M9"
    ]
    graphics [
      x 1019.4067410627912
      y 1096.9368511854295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_169"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa672"
      uniprot "NA"
    ]
    graphics [
      x 1118.2650802805101
      y 914.2364293018121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re156"
      uniprot "NA"
    ]
    graphics [
      x 1263.313539491382
      y 868.5381838150115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:28976;urn:miriam:ncbigene:28976;urn:miriam:refseq:NM_014049;urn:miriam:ec-code:1.3.8.-;urn:miriam:ensembl:ENSG00000177646;urn:miriam:hgnc:21497;urn:miriam:hgnc.symbol:ACAD9;urn:miriam:uniprot:Q9H845;urn:miriam:uniprot:Q9H845;urn:miriam:hgnc.symbol:ACAD9"
      hgnc "HGNC_SYMBOL:ACAD9"
      map_id "M13_165"
      name "ACAD9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa668"
      uniprot "UNIPROT:Q9H845"
    ]
    graphics [
      x 1403.7821122987525
      y 687.5371322839724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18421;urn:miriam:obo.chebi:CHEBI%3A1842"
      hgnc "NA"
      map_id "M13_110"
      name "superoxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa360"
      uniprot "NA"
    ]
    graphics [
      x 561.4288625390133
      y 709.5559420859454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 648.8613083931268
      y 608.726659607229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_121"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa371"
      uniprot "NA"
    ]
    graphics [
      x 555.5811585517504
      y 494.852278543401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000112096;urn:miriam:hgnc:11180;urn:miriam:ncbigene:6648;urn:miriam:ncbigene:6648;urn:miriam:uniprot:P04179;urn:miriam:refseq:NM_000636;urn:miriam:hgnc.symbol:SOD2;urn:miriam:hgnc.symbol:SOD2;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD2"
      map_id "M13_108"
      name "SOD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358"
      uniprot "UNIPROT:P04179"
    ]
    graphics [
      x 387.7647072314278
      y 588.8653041965623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M13_105"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 1007.3192784781039
      y 667.2751919924956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M13_100"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 1427.5968881761148
      y 453.25299854202865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_49"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1344.5649358028704
      y 527.7934721525636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M13_101"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1262.6740948475212
      y 422.1703225934642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005753;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc:7414;urn:miriam:ncbigene:4508;urn:miriam:ncbigene:4508;urn:miriam:ensembl:ENSG00000198899;urn:miriam:refseq:YP_003024031;urn:miriam:uniprot:P00846;urn:miriam:uniprot:P00846;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:ensembl:ENSG00000130770;urn:miriam:ncbigene:93974;urn:miriam:ncbigene:93974;urn:miriam:refseq:NM_016311;urn:miriam:hgnc:871;urn:miriam:uniprot:Q9UII2;urn:miriam:uniprot:Q9UII2"
      hgnc "HGNC_SYMBOL:MT-ATP6;HGNC_SYMBOL:ATP5IF1"
      map_id "M13_8"
      name "ATP_space_Synthase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      x 1275.3015660865633
      y 748.293273308394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_103"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa335"
      uniprot "NA"
    ]
    graphics [
      x 1226.7653996233917
      y 489.18839851357416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422"
      hgnc "NA"
      map_id "M13_102"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 1316.473797814077
      y 409.0906897705245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_161"
      name "s1131"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa663"
      uniprot "NA"
    ]
    graphics [
      x 945.3698175843614
      y 1341.1651483132332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 1034.4375948255858
      y 1205.995781805981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_004541;urn:miriam:ensembl:ENSG00000125356;urn:miriam:uniprot:O15239;urn:miriam:uniprot:O15239;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:ncbigene:4694;urn:miriam:ncbigene:4694;urn:miriam:hgnc:7683"
      hgnc "HGNC_SYMBOL:NDUFA1"
      map_id "M13_163"
      name "NDUFA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa665"
      uniprot "UNIPROT:O15239"
    ]
    graphics [
      x 927.1551616748857
      y 1108.7443821098723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_182"
      name "s1195"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa714"
      uniprot "NA"
    ]
    graphics [
      x 1558.8604430461487
      y 464.24733943467675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 1456.569995200151
      y 554.5935055807857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:Q9BQ95;urn:miriam:uniprot:Q9BQ95;urn:miriam:ensembl:ENSG00000130159;urn:miriam:hgnc:29548;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:refseq:NM_016581;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:ncbigene:51295;urn:miriam:ncbigene:51295"
      hgnc "HGNC_SYMBOL:ECSIT"
      map_id "M13_164"
      name "ECSIT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa667"
      uniprot "UNIPROT:Q9BQ95"
    ]
    graphics [
      x 1319.551247503517
      y 651.7983265733144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000003509;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:ec-code:2.1.1.320;urn:miriam:hgnc:28816;urn:miriam:ncbigene:55471;urn:miriam:uniprot:Q7L592;urn:miriam:uniprot:Q7L592;urn:miriam:ncbigene:55471;urn:miriam:refseq:NM_144736"
      hgnc "HGNC_SYMBOL:NDUFAF7"
      map_id "M13_166"
      name "NDUFAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa669"
      uniprot "UNIPROT:Q7L592"
    ]
    graphics [
      x 1415.7897510558696
      y 286.34341433953637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_7"
      name "OXPHOS_space_factors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 1347.001092023645
      y 817.3453118904688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1217.1157624405548
      y 1093.5770946515904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16856"
      hgnc "NA"
      map_id "M13_131"
      name "glutathione"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa381"
      uniprot "NA"
    ]
    graphics [
      x 1140.5040274085993
      y 1363.1345559620745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000167468;urn:miriam:ncbigene:2879;urn:miriam:ncbigene:2879;urn:miriam:hgnc:4556;urn:miriam:hgnc.symbol:GPX4;urn:miriam:hgnc.symbol:GPX4;urn:miriam:refseq:NM_002085;urn:miriam:uniprot:P36969;urn:miriam:ec-code:1.11.1.12"
      hgnc "HGNC_SYMBOL:GPX4"
      map_id "M13_129"
      name "GPX4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa379"
      uniprot "UNIPROT:P36969"
    ]
    graphics [
      x 1267.4286425593464
      y 1236.8068145850382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:2876;urn:miriam:ncbigene:2876;urn:miriam:hgnc:4553;urn:miriam:ensembl:ENSG00000233276;urn:miriam:uniprot:P07203;urn:miriam:hgnc.symbol:GPX1;urn:miriam:hgnc.symbol:GPX1;urn:miriam:refseq:NM_000581;urn:miriam:ec-code:1.11.1.9"
      hgnc "HGNC_SYMBOL:GPX1"
      map_id "M13_128"
      name "GPX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa378"
      uniprot "UNIPROT:P07203"
    ]
    graphics [
      x 1182.2077767907201
      y 1219.4402669545855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M13_106"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa356"
      uniprot "NA"
    ]
    graphics [
      x 1341.8811331092918
      y 934.5567796267038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58297"
      hgnc "NA"
      map_id "M13_130"
      name "glutathione_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa380"
      uniprot "NA"
    ]
    graphics [
      x 1206.7581918934536
      y 1358.4001533712435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_32"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 1174.3515875806747
      y 623.0351053681204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS25;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS27"
      map_id "M13_21"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
    ]
    graphics [
      x 1319.5256391203618
      y 1664.6459559085788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 1203.5226391302836
      y 1651.1314904842586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742615"
      hgnc "NA"
      map_id "M13_146"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 1291.0620217402502
      y 1753.9966916290655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS27;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS25"
      map_id "M13_20"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa90"
      uniprot "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
    ]
    graphics [
      x 1048.041224419319
      y 1528.2888236654958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:doi:10.1042/EBC20170103;urn:miriam:obo.go:GO%3A0006264;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:refseq:NM_007215;urn:miriam:uniprot:Q9UHN1;urn:miriam:uniprot:Q9UHN1;urn:miriam:hgnc:9180;urn:miriam:ensembl:ENSG00000256525;urn:miriam:refseq:NM_018109;urn:miriam:ensembl:ENSG00000107951;urn:miriam:ncbigene:55149;urn:miriam:uniprot:Q9NVV4;urn:miriam:uniprot:Q9NVV4;urn:miriam:ncbigene:55149;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:ec-code:2.7.7.19;urn:miriam:hgnc:25532;urn:miriam:hgnc:29666;urn:miriam:ensembl:ENSG00000103707;urn:miriam:ncbigene:123263;urn:miriam:ncbigene:123263;urn:miriam:uniprot:Q96DP5;urn:miriam:uniprot:Q96DP5;urn:miriam:ec-code:2.1.2.9;urn:miriam:refseq:NM_139242;urn:miriam:hgnc.symbol:MTFMT;urn:miriam:hgnc.symbol:MTFMT"
      hgnc "HGNC_SYMBOL:POLG;HGNC_SYMBOL:POLG2;HGNC_SYMBOL:MTPAP;HGNC_SYMBOL:MTFMT"
      map_id "M13_16"
      name "Mt_space_replication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
    ]
    graphics [
      x 253.81435442216832
      y 694.1331972354369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_82"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 316.3821148814666
      y 838.0109571489116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_145"
      name "mt_space_DNA_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa463"
      uniprot "NA"
    ]
    graphics [
      x 506.679802237935
      y 1016.8144537787817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_180"
      name "s1190"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa712"
      uniprot "NA"
    ]
    graphics [
      x 784.3901467677553
      y 976.0347290746855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re164"
      uniprot "NA"
    ]
    graphics [
      x 957.6187856575222
      y 910.8086538246057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M13_179"
      name "ATP5MG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa710"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 794.4112577003702
      y 913.7390525701414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613"
      hgnc "NA"
      map_id "M13_181"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713"
      uniprot "NA"
    ]
    graphics [
      x 850.1745241150644
      y 1009.1737663071086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 754.2552337907565
      y 404.0761670661037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16234"
      hgnc "NA"
      map_id "M13_114"
      name "hydroxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa364"
      uniprot "NA"
    ]
    graphics [
      x 925.0954610916774
      y 319.0043827469407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18421;urn:miriam:obo.chebi:CHEBI%3A1842"
      hgnc "NA"
      map_id "M13_104"
      name "superoxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa354"
      uniprot "NA"
    ]
    graphics [
      x 608.8089782431409
      y 555.9455488686758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_120"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa370"
      uniprot "NA"
    ]
    graphics [
      x 682.2088352770172
      y 272.74216378043354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M13_119"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa369"
      uniprot "NA"
    ]
    graphics [
      x 629.3657582705479
      y 337.5493912307362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0000262"
      hgnc "NA"
      map_id "M13_156"
      name "mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa652"
      uniprot "NA"
    ]
    graphics [
      x 721.069683557541
      y 1183.1858639222135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_84"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 802.7819255106948
      y 792.460831074541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:refseq:NM_002047;urn:miriam:ensembl:ENSG00000106105;urn:miriam:uniprot:P41250;urn:miriam:uniprot:P41250;urn:miriam:hgnc.symbol:GARS1;urn:miriam:hgnc.symbol:GARS1;urn:miriam:ec-code:2.7.7.-;urn:miriam:ncbigene:2617;urn:miriam:ncbigene:2617;urn:miriam:hgnc:4162;urn:miriam:ec-code:6.1.1.14;urn:miriam:hgnc:25538;urn:miriam:hgnc.symbol:DARS2;urn:miriam:hgnc.symbol:DARS2;urn:miriam:ncbigene:55157;urn:miriam:refseq:NM_018122;urn:miriam:ncbigene:55157;urn:miriam:ensembl:ENSG00000117593;urn:miriam:ec-code:6.1.1.12;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q5JTZ9;urn:miriam:uniprot:Q5JTZ9;urn:miriam:ensembl:ENSG00000124608;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc:21022;urn:miriam:ncbigene:57505;urn:miriam:ncbigene:57505;urn:miriam:refseq:NM_020745;urn:miriam:ec-code:6.1.1.7;urn:miriam:ncbigene:3735;urn:miriam:ncbigene:3735;urn:miriam:ensembl:ENSG00000065427;urn:miriam:hgnc:6215;urn:miriam:ec-code:2.7.7.-;urn:miriam:uniprot:Q15046;urn:miriam:uniprot:Q15046;urn:miriam:refseq:NM_005548;urn:miriam:ec-code:6.1.1.6;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc:21406;urn:miriam:hgnc.symbol:RARS2;urn:miriam:hgnc.symbol:RARS2;urn:miriam:ncbigene:57038;urn:miriam:refseq:NM_020320;urn:miriam:ncbigene:57038;urn:miriam:ec-code:6.1.1.19;urn:miriam:uniprot:Q5T160;urn:miriam:uniprot:Q5T160;urn:miriam:ensembl:ENSG00000146282"
      hgnc "HGNC_SYMBOL:GARS1;HGNC_SYMBOL:DARS2;HGNC_SYMBOL:AARS2;HGNC_SYMBOL:KARS1;HGNC_SYMBOL:RARS2"
      map_id "M13_12"
      name "Mt_minus_tRNA_space_synthetase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
    ]
    graphics [
      x 721.7183248035001
      y 680.4095093429003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:28752201;urn:miriam:refseq:NM_017722;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:ensembl:ENSG00000104907;urn:miriam:uniprot:Q9NXH9;urn:miriam:uniprot:Q9NXH9;urn:miriam:ec-code:2.1.1.216;urn:miriam:hgnc:25980"
      hgnc "HGNC_SYMBOL:TRMT1"
      map_id "M13_173"
      name "TRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa692"
      uniprot "UNIPROT:Q9NXH9"
    ]
    graphics [
      x 945.6569670856516
      y 485.59672928558757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ensembl:ENSG00000210174;urn:miriam:ncbigene:4573;urn:miriam:hgnc:7496;urn:miriam:hgnc.symbol:MT-TR;urn:miriam:hgnc:7489;urn:miriam:hgnc.symbol:MT-TK;urn:miriam:ensembl:ENSG00000210156;urn:miriam:ncbigene:4566;urn:miriam:hgnc:7501;urn:miriam:hgnc.symbol:MT-TW;urn:miriam:ensembl:ENSG00000210117;urn:miriam:ncbigene:4578;urn:miriam:ensembl:ENSG00000210077;urn:miriam:hgnc:7500;urn:miriam:hgnc.symbol:MT-TV;urn:miriam:ncbigene:4577;urn:miriam:ensembl:ENSG00000210100;urn:miriam:hgnc:7488;urn:miriam:hgnc.symbol:MT-TI;urn:miriam:ncbigene:4565;urn:miriam:hgnc.symbol:MT-TP;urn:miriam:ensembl:ENSG00000210196;urn:miriam:ncbigene:4571;urn:miriam:hgnc:7494;urn:miriam:ensembl:ENSG00000210151;urn:miriam:hgnc.symbol:MT-TS1;urn:miriam:ncbigene:4574;urn:miriam:hgnc:7497;urn:miriam:ncbigene:4549;urn:miriam:uniprot:A0A0C5B5G6;urn:miriam:hgnc:7470;urn:miriam:ensembl:ENSG00000211459;urn:miriam:hgnc.symbol:MT-RNR1;urn:miriam:hgnc:7479;urn:miriam:ensembl:ENSG00000210194;urn:miriam:hgnc.symbol:MT-TE;urn:miriam:ncbigene:4556;urn:miriam:hgnc.symbol:MT-TL2;urn:miriam:ensembl:ENSG00000210191;urn:miriam:hgnc:7491;urn:miriam:ncbigene:4568;urn:miriam:hgnc:7499;urn:miriam:ensembl:ENSG00000210195;urn:miriam:hgnc.symbol:MT-TT;urn:miriam:ncbigene:4576;urn:miriam:hgnc:7498;urn:miriam:ensembl:ENSG00000210184;urn:miriam:hgnc.symbol:MT-TS2;urn:miriam:ncbigene:4575;urn:miriam:ncbigene:4558;urn:miriam:hgnc:7481;urn:miriam:hgnc.symbol:MT-TF;urn:miriam:ensembl:ENSG00000210049;urn:miriam:hgnc.symbol:MT-TL1;urn:miriam:hgnc:7490;urn:miriam:ensembl:ENSG00000209082;urn:miriam:ncbigene:4567;urn:miriam:hgnc.symbol:MT-TN;urn:miriam:ncbigene:4570;urn:miriam:hgnc:7493;urn:miriam:ensembl:ENSG00000210135;urn:miriam:ensembl:ENSG00000210176;urn:miriam:hgnc:7487;urn:miriam:ncbigene:4564;urn:miriam:hgnc.symbol:MT-TH;urn:miriam:ensembl:ENSG00000210107;urn:miriam:ncbigene:4572;urn:miriam:hgnc.symbol:MT-TQ;urn:miriam:hgnc:7495;urn:miriam:hgnc:7477;urn:miriam:ensembl:ENSG00000210140;urn:miriam:hgnc.symbol:MT-TC;urn:miriam:ncbigene:4511"
      hgnc "HGNC_SYMBOL:MT-TR;HGNC_SYMBOL:MT-TK;HGNC_SYMBOL:MT-TW;HGNC_SYMBOL:MT-TV;HGNC_SYMBOL:MT-TI;HGNC_SYMBOL:MT-TP;HGNC_SYMBOL:MT-TS1;HGNC_SYMBOL:MT-RNR1;HGNC_SYMBOL:MT-TE;HGNC_SYMBOL:MT-TL2;HGNC_SYMBOL:MT-TT;HGNC_SYMBOL:MT-TS2;HGNC_SYMBOL:MT-TF;HGNC_SYMBOL:MT-TL1;HGNC_SYMBOL:MT-TN;HGNC_SYMBOL:MT-TH;HGNC_SYMBOL:MT-TQ;HGNC_SYMBOL:MT-TC"
      map_id "M13_14"
      name "MT_space_tRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa55"
      uniprot "UNIPROT:A0A0C5B5G6"
    ]
    graphics [
      x 796.4569391701133
      y 650.2570429715148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0047485"
      hgnc "NA"
      map_id "M13_170"
      name "precursor_space_protein_space_N_minus_terminus_space_binding"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa688"
      uniprot "NA"
    ]
    graphics [
      x 1364.5967653000398
      y 1488.6057038001684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_48"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1464.326993632028
      y 1527.7378300424778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005742;urn:miriam:uniprot:TOMM37;urn:miriam:refseq:NM_014820;urn:miriam:uniprot:O94826;urn:miriam:uniprot:O94826;urn:miriam:hgnc:11985;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:ncbigene:9868;urn:miriam:ncbigene:9868;urn:miriam:ensembl:ENSG00000154174;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7;urn:miriam:refseq:NM_020243;urn:miriam:uniprot:Q9NS69;urn:miriam:uniprot:Q9NS69;urn:miriam:ncbigene:56993;urn:miriam:ncbigene:56993;urn:miriam:hgnc:18002;urn:miriam:ensembl:ENSG00000100216;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:hgnc.symbol:TOMM22"
      hgnc "HGNC_SYMBOL:TOMM70;HGNC_SYMBOL:TIMM9;HGNC_SYMBOL:TOMM22"
      map_id "M13_22"
      name "TOM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:TOMM37;UNIPROT:O94826;UNIPROT:Q9Y5J7;UNIPROT:Q9NS69"
    ]
    graphics [
      x 1444.2782772968021
      y 1352.514025129465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0042721;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643;urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337"
      hgnc "HGNC_SYMBOL:SDHC;HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM22"
      map_id "M13_23"
      name "TIM22_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
    ]
    graphics [
      x 1452.043982511769
      y 1715.3647867026903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:APO40587"
      hgnc "NA"
      map_id "M13_172"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa691"
      uniprot "NA"
    ]
    graphics [
      x 1350.9678094481565
      y 1422.7330884842825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005744;urn:miriam:ncbigene:10440;urn:miriam:uniprot:Q99595;urn:miriam:uniprot:Q99595;urn:miriam:ncbigene:10440;urn:miriam:hgnc:17315;urn:miriam:refseq:NM_006335;urn:miriam:ensembl:ENSG00000134375;urn:miriam:hgnc.symbol:TIMM17A;urn:miriam:hgnc.symbol:TIMM17A;urn:miriam:hgnc:17310;urn:miriam:hgnc.symbol:TIMM17B;urn:miriam:hgnc.symbol:TIMM17B;urn:miriam:ensembl:ENSG00000126768;urn:miriam:ncbigene:10245;urn:miriam:ncbigene:10245;urn:miriam:uniprot:O60830;urn:miriam:uniprot:O60830;urn:miriam:refseq:NM_005834;urn:miriam:hgnc:17312;urn:miriam:hgnc.symbol:TIMM23;urn:miriam:hgnc.symbol:TIMM23;urn:miriam:ensembl:ENSG00000265354;urn:miriam:uniprot:O14925;urn:miriam:uniprot:O14925;urn:miriam:refseq:NM_006327.2;urn:miriam:ncbigene:100287932;urn:miriam:ncbigene:100287932"
      hgnc "HGNC_SYMBOL:TIMM17A;HGNC_SYMBOL:TIMM17B;HGNC_SYMBOL:TIMM23"
      map_id "M13_24"
      name "TIM23_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:Q99595;UNIPROT:O60830;UNIPROT:O14925"
    ]
    graphics [
      x 1532.7373815927895
      y 1439.2655142913682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0042719;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M13_25"
      name "TIM9_minus_TIM10_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa95"
      uniprot "UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 1553.3712041801555
      y 1637.293974094851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0047485"
      hgnc "NA"
      map_id "M13_171"
      name "precursor_space_protein_space_N_minus_terminus_space_binding"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa690"
      uniprot "NA"
    ]
    graphics [
      x 1409.5215873418135
      y 1399.1700700604606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1464.7637933976412
      y 774.9329632300803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_012473;urn:miriam:hgnc:17772;urn:miriam:uniprot:Q99757;urn:miriam:ncbigene:25828;urn:miriam:ncbigene:25828;urn:miriam:hgnc.symbol:TXN2;urn:miriam:hgnc.symbol:TXN2;urn:miriam:ensembl:ENSG00000100348"
      hgnc "HGNC_SYMBOL:TXN2"
      map_id "M13_137"
      name "TXN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa392"
      uniprot "UNIPROT:Q99757"
    ]
    graphics [
      x 1688.0283837927677
      y 740.6889586335221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:26067716;urn:miriam:uniprot:P30044;urn:miriam:uniprot:P30044;urn:miriam:refseq:NM_181651;urn:miriam:ensembl:ENSG00000126432;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc:9355;urn:miriam:ncbigene:25824;urn:miriam:ncbigene:25824;urn:miriam:ec-code:3.1.1.4;urn:miriam:hgnc:16753;urn:miriam:ec-code:2.3.1.23;urn:miriam:refseq:NM_004905;urn:miriam:ensembl:ENSG00000117592;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:ncbigene:9588;urn:miriam:ncbigene:9588;urn:miriam:uniprot:P30041;urn:miriam:uniprot:P30041;urn:miriam:ec-code:1.11.1.27;urn:miriam:uniprot:Q06830;urn:miriam:uniprot:Q06830;urn:miriam:refseq:NM_181697;urn:miriam:ncbigene:5052;urn:miriam:ncbigene:5052;urn:miriam:hgnc:9352;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000117450;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:ncbigene:7001;urn:miriam:ncbigene:7001;urn:miriam:refseq:NM_005809;urn:miriam:uniprot:P32119;urn:miriam:uniprot:P32119;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000167815;urn:miriam:hgnc:9353;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:ensembl:ENSG00000165672;urn:miriam:ncbigene:10935;urn:miriam:ncbigene:10935;urn:miriam:uniprot:P30048;urn:miriam:uniprot:P30048;urn:miriam:refseq:NM_006793;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc:9354"
      hgnc "HGNC_SYMBOL:PRDX5;HGNC_SYMBOL:PRDX6;HGNC_SYMBOL:PRDX1;HGNC_SYMBOL:PRDX2;HGNC_SYMBOL:PRDX3"
      map_id "M13_27"
      name "PRDX"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
    ]
    graphics [
      x 1517.7518130566436
      y 911.847110149775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_012473;urn:miriam:hgnc:17772;urn:miriam:uniprot:Q99757;urn:miriam:ncbigene:25828;urn:miriam:ncbigene:25828;urn:miriam:hgnc.symbol:TXN2;urn:miriam:hgnc.symbol:TXN2;urn:miriam:ensembl:ENSG00000100348"
      hgnc "HGNC_SYMBOL:TXN2"
      map_id "M13_136"
      name "TXN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:Q99757"
    ]
    graphics [
      x 1663.264861965159
      y 811.604175135899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_152"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa645"
      uniprot "NA"
    ]
    graphics [
      x 278.6543458501253
      y 1376.0296066942162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_37"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 441.7876370449228
      y 1416.5042467043152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "M13_93"
      name "Q"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 359.2734349144115
      y 1222.834193299785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_154"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa650"
      uniprot "NA"
    ]
    graphics [
      x 648.9166811247749
      y 1580.9521460019298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_81"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 687.7113085434069
      y 1416.674861122938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_003201;urn:miriam:hgnc:11741;urn:miriam:hgnc.symbol:TFAM;urn:miriam:hgnc.symbol:TFAM;urn:miriam:uniprot:Q00059;urn:miriam:ensembl:ENSG00000108064;urn:miriam:ncbigene:7019;urn:miriam:ncbigene:7019"
      hgnc "HGNC_SYMBOL:TFAM"
      map_id "M13_143"
      name "TFAM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa460"
      uniprot "UNIPROT:Q00059"
    ]
    graphics [
      x 551.3363943716231
      y 1374.0201059908218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:18391175;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:ncbigene:51106;urn:miriam:ncbigene:51106;urn:miriam:hgnc:17037;urn:miriam:ensembl:ENSG00000029639;urn:miriam:uniprot:Q8WVM0;urn:miriam:uniprot:Q8WVM0;urn:miriam:ec-code:2.1.1.-;urn:miriam:refseq:NM_001350501;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:uniprot:Q9H5Q4;urn:miriam:uniprot:Q9H5Q4;urn:miriam:hgnc:18559;urn:miriam:refseq:NM_022366;urn:miriam:ec-code:2.1.1.-;urn:miriam:ensembl:ENSG00000162851;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:uniprot:O00411;urn:miriam:uniprot:O00411;urn:miriam:refseq:NM_005035;urn:miriam:ncbigene:5442;urn:miriam:ncbigene:5442;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9200;urn:miriam:ensembl:ENSG00000099821"
      hgnc "HGNC_SYMBOL:TFB1M;HGNC_SYMBOL:TFB2M;HGNC_SYMBOL:POLRMT"
      map_id "M13_15"
      name "MT_space_transcription"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
    ]
    graphics [
      x 749.0598183735784
      y 1546.688877392116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_157"
      name "damaged_space_mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa653"
      uniprot "NA"
    ]
    graphics [
      x 778.1360757396712
      y 1448.3054777525194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33699"
      hgnc "NA"
      map_id "M13_142"
      name "mt_space_mRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa459"
      uniprot "NA"
    ]
    graphics [
      x 698.7612754646864
      y 1513.3434581512809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_159"
      name "s1127"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa659"
      uniprot "NA"
    ]
    graphics [
      x 882.9193297829952
      y 899.6057508855673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 971.9753460929384
      y 990.7071423361641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_28"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa98"
      uniprot "NA"
    ]
    graphics [
      x 980.3145770582813
      y 862.3368825388827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045271;urn:miriam:doi:10.1016/j.bbabio.2011.08.010;urn:miriam:pubmed:19355884;urn:miriam:ensembl:ENSG00000023228;urn:miriam:refseq:NM_005006;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc:7707;urn:miriam:uniprot:P28331;urn:miriam:uniprot:P28331;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4723;urn:miriam:ncbigene:4723;urn:miriam:uniprot:P49821;urn:miriam:uniprot:P49821;urn:miriam:refseq:NM_007103;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc:7716;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000167792;urn:miriam:ncbigene:4537;urn:miriam:ncbigene:4537;urn:miriam:uniprot:P03897;urn:miriam:uniprot:P03897;urn:miriam:hgnc:7458;urn:miriam:refseq:YP_003024033;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ensembl:ENSG00000198840;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ec-code:7.1.1.2;urn:miriam:uniprot:P03923;urn:miriam:uniprot:P03923;urn:miriam:refseq:YP_003024037;urn:miriam:ncbigene:4541;urn:miriam:ncbigene:4541;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7462;urn:miriam:ensembl:ENSG00000198695;urn:miriam:ensembl:ENSG00000178127;urn:miriam:refseq:NM_021074;urn:miriam:ncbigene:4729;urn:miriam:ncbigene:4729;urn:miriam:uniprot:P19404;urn:miriam:uniprot:P19404;urn:miriam:hgnc:7717;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:ensembl:ENSG00000110717;urn:miriam:refseq:NM_002496;urn:miriam:ncbigene:4728;urn:miriam:ncbigene:4728;urn:miriam:uniprot:O00217;urn:miriam:uniprot:O00217;urn:miriam:hgnc:7715;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4539;urn:miriam:uniprot:P03901;urn:miriam:uniprot:P03901;urn:miriam:ncbigene:4539;urn:miriam:ensembl:ENSG00000212907;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc:7460;urn:miriam:refseq:YP_003024034;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7456;urn:miriam:ncbigene:4536;urn:miriam:ncbigene:4536;urn:miriam:refseq:YP_003024027;urn:miriam:uniprot:P03891;urn:miriam:uniprot:P03891;urn:miriam:ensembl:ENSG00000198763;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc:7710;urn:miriam:uniprot:O75489;urn:miriam:uniprot:O75489;urn:miriam:ensembl:ENSG00000213619;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:refseq:NM_004551;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:ncbigene:4722;urn:miriam:ncbigene:4722;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:YP_003024036;urn:miriam:ensembl:ENSG00000198786;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7461;urn:miriam:uniprot:P03915;urn:miriam:uniprot:P03915;urn:miriam:ncbigene:4538;urn:miriam:ncbigene:4538;urn:miriam:hgnc:7459;urn:miriam:refseq:YP_003024035;urn:miriam:ensembl:ENSG00000198886;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:uniprot:P03905;urn:miriam:uniprot:P03905;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000115286;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc:7714;urn:miriam:uniprot:O75251;urn:miriam:uniprot:O75251;urn:miriam:ncbigene:374291;urn:miriam:ncbigene:374291;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:NM_024407;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:ensembl:ENSG00000158864;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:refseq:NM_004550;urn:miriam:uniprot:O75306;urn:miriam:uniprot:O75306;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7708;urn:miriam:ncbigene:4720;urn:miriam:ncbigene:4720;urn:miriam:ensembl:ENSG00000160194;urn:miriam:refseq:NM_001001503;urn:miriam:uniprot:P56181;urn:miriam:uniprot:P56181;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc:7719;urn:miriam:ncbigene:4731;urn:miriam:ncbigene:4731;urn:miriam:hgnc:7455;urn:miriam:refseq:YP_003024026;urn:miriam:uniprot:P03886;urn:miriam:uniprot:P03886;urn:miriam:ensembl:ENSG00000198888;urn:miriam:ncbigene:4535;urn:miriam:ncbigene:4535;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND1;urn:miriam:hgnc.symbol:MT-ND1"
      hgnc "HGNC_SYMBOL:NDUFS1;HGNC_SYMBOL:NDUFV1;HGNC_SYMBOL:MT-ND3;HGNC_SYMBOL:MT-ND6;HGNC_SYMBOL:NDUFV2;HGNC_SYMBOL:NDUFS8;HGNC_SYMBOL:MT-ND4L;HGNC_SYMBOL:MT-ND2;HGNC_SYMBOL:NDUFS3;HGNC_SYMBOL:MT-ND5;HGNC_SYMBOL:MT-ND4;HGNC_SYMBOL:NDUFS7;HGNC_SYMBOL:NDUFS2;HGNC_SYMBOL:NDUFV3;HGNC_SYMBOL:MT-ND1"
      map_id "M13_2"
      name "Complex_space_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
    ]
    graphics [
      x 624.091783926619
      y 1114.855433878387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1024.576037176018
      y 371.22594504384153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M13_112"
      name "ROS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa362"
      uniprot "NA"
    ]
    graphics [
      x 976.4181947437298
      y 588.7196914231122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000112096;urn:miriam:hgnc:11180;urn:miriam:ncbigene:6648;urn:miriam:ncbigene:6648;urn:miriam:uniprot:P04179;urn:miriam:refseq:NM_000636;urn:miriam:hgnc.symbol:SOD2;urn:miriam:hgnc.symbol:SOD2;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD2"
      map_id "M13_107"
      name "SOD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa357"
      uniprot "UNIPROT:P04179"
    ]
    graphics [
      x 163.47796283199546
      y 842.1270419082429
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 182.03361348105227
      y 698.008897719179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:23410;urn:miriam:ncbigene:23410;urn:miriam:uniprot:Q9NTG7;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc:14931;urn:miriam:refseq:NM_001017524;urn:miriam:ec-code:2.3.1.286;urn:miriam:ensembl:ENSG00000142082"
      hgnc "HGNC_SYMBOL:SIRT3"
      map_id "M13_109"
      name "SIRT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa359"
      uniprot "UNIPROT:Q9NTG7"
    ]
    graphics [
      x 62.5
      y 697.962112154759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_88"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa18"
      uniprot "NA"
    ]
    graphics [
      x 1064.581319729804
      y 1058.3675863686826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_35"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 1238.950672338696
      y 961.858227556594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_168"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa671"
      uniprot "NA"
    ]
    graphics [
      x 1191.6722288040014
      y 926.2592007094836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1178.568956054478
      y 817.95430360303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_183"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa715"
      uniprot "NA"
    ]
    graphics [
      x 969.9957802840165
      y 1153.1325241992367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 838.5697419857802
      y 1125.301854871268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_79"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 470.9052658859567
      y 1212.320510599297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1091.5432258366272
      y 443.60762295546715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_117"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa367"
      uniprot "NA"
    ]
    graphics [
      x 1194.3161669979513
      y 561.3421612351677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A49648"
      hgnc "NA"
      map_id "M13_113"
      name "HO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 1007.1313450868374
      y 493.89754704842335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_118"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa368"
      uniprot "NA"
    ]
    graphics [
      x 1086.1482390956173
      y 601.0792590305495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_178"
      name "s1185"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa706"
      uniprot "NA"
    ]
    graphics [
      x 1387.912041697342
      y 1989.0187129441374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1496.0209005856786
      y 1909.9239372205284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580"
      hgnc "HGNC_SYMBOL:TIMM29"
      map_id "M13_177"
      name "TIMM29"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa700"
      uniprot "UNIPROT:Q9BSF4"
    ]
    graphics [
      x 1382.0736412992544
      y 1916.0228831540226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611"
      hgnc "NA"
      map_id "M13_176"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa695"
      uniprot "NA"
    ]
    graphics [
      x 1616.1317013841476
      y 1900.8752184488462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643"
      hgnc "HGNC_SYMBOL:SDHD;HGNC_SYMBOL:SDHC"
      map_id "M13_4"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      x 627.4028064272911
      y 867.4615778137734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:31082116"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 424.8437591683172
      y 740.0176773559424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_149"
      name "TCA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa500"
      uniprot "NA"
    ]
    graphics [
      x 320.43844242865373
      y 626.7663314284332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683"
      hgnc "HGNC_SYMBOL:SDHC;HGNC_SYMBOL:SDHD"
      map_id "M13_19"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:Q99643;UNIPROT:O14521"
    ]
    graphics [
      x 282.5091521619172
      y 889.1657883053996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_86"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 845.2886377402791
      y 1414.5416557626118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:hgnc.symbol:TWNK;urn:miriam:hgnc.symbol:TWNK;urn:miriam:ncbigene:56652;urn:miriam:ncbigene:56652;urn:miriam:ec-code:3.6.4.12;urn:miriam:refseq:NM_021830;urn:miriam:ensembl:ENSG00000107815;urn:miriam:hgnc:1160;urn:miriam:uniprot:Q96RR1;urn:miriam:uniprot:Q96RR1;urn:miriam:ensembl:ENSG00000123297;urn:miriam:uniprot:P43897;urn:miriam:uniprot:P43897;urn:miriam:hgnc:12367;urn:miriam:ncbigene:10102;urn:miriam:refseq:NM_005726;urn:miriam:ncbigene:10102;urn:miriam:hgnc.symbol:TSFM;urn:miriam:hgnc.symbol:TSFM;urn:miriam:ncbigene:7284;urn:miriam:ncbigene:7284;urn:miriam:ensembl:ENSG00000178952;urn:miriam:refseq:NM_003321;urn:miriam:hgnc:12420;urn:miriam:uniprot:P49411;urn:miriam:uniprot:P49411;urn:miriam:hgnc.symbol:TUFM;urn:miriam:hgnc.symbol:TUFM;urn:miriam:uniprot:Q96RP9;urn:miriam:uniprot:Q96RP9;urn:miriam:ncbigene:85476;urn:miriam:ncbigene:85476;urn:miriam:hgnc.symbol:GFM1;urn:miriam:hgnc.symbol:GFM1;urn:miriam:refseq:NM_024996;urn:miriam:ensembl:ENSG00000168827;urn:miriam:hgnc:13780"
      hgnc "HGNC_SYMBOL:TWNK;HGNC_SYMBOL:TSFM;HGNC_SYMBOL:TUFM;HGNC_SYMBOL:GFM1"
      map_id "M13_17"
      name "Mt_space_translation"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
    ]
    graphics [
      x 800.9835836039443
      y 1320.816983985196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ensembl:ENSG00000175110;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ncbigene:56945;urn:miriam:ncbigene:56945;urn:miriam:hgnc:14508;urn:miriam:uniprot:P82650;urn:miriam:uniprot:P82650;urn:miriam:refseq:NM_020191;urn:miriam:refseq:NM_007208;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc:10379;urn:miriam:ensembl:ENSG00000114686;urn:miriam:uniprot:P09001;urn:miriam:uniprot:P09001;urn:miriam:ensembl:ENSG00000182180;urn:miriam:hgnc:14048;urn:miriam:ncbigene:51021;urn:miriam:ncbigene:51021;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:refseq:NM_016065;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:uniprot:Q9Y3D3;urn:miriam:uniprot:Q9Y3D3"
      hgnc "HGNC_SYMBOL:MRPS22;HGNC_SYMBOL:MRPL3;HGNC_SYMBOL:MRPS16"
      map_id "M13_10"
      name "Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
    ]
    graphics [
      x 946.792921811076
      y 1302.197959639937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_18"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "NA"
    ]
    graphics [
      x 872.2950081349123
      y 1276.7519247129171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 884.7579770239414
      y 471.21139639052774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_115"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa365"
      uniprot "NA"
    ]
    graphics [
      x 748.6086872545022
      y 474.530000500544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_116"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa366"
      uniprot "NA"
    ]
    graphics [
      x 984.1079139652087
      y 416.92364191815386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1136.9100866297804
      y 1569.611197208225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M13_132"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382"
      uniprot "NA"
    ]
    graphics [
      x 1161.6356351193995
      y 1710.6264534825852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000104687;urn:miriam:ec-code:1.8.1.7;urn:miriam:hgnc:4623;urn:miriam:ncbigene:2936;urn:miriam:ncbigene:2936;urn:miriam:refseq:NM_000637;urn:miriam:hgnc.symbol:GSR;urn:miriam:hgnc.symbol:GSR;urn:miriam:uniprot:P00390"
      hgnc "HGNC_SYMBOL:GSR"
      map_id "M13_134"
      name "GSR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa384"
      uniprot "UNIPROT:P00390"
    ]
    graphics [
      x 1041.556915418962
      y 1665.3815785572392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M13_133"
      name "NADP(_plus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa383"
      uniprot "NA"
    ]
    graphics [
      x 1093.412570377208
      y 1711.0637854842712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 404.60879645131627
      y 1007.6924696862671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "M13_94"
      name "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 267.2248373377844
      y 1072.4336356333492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_91"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 1546.6439076073725
      y 1158.1063685278384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 1439.746917173662
      y 951.3978828385835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 798.3720555671421
      y 562.2626322286326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905;urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_9"
      name "paraquat_space_dication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "NA"
    ]
    graphics [
      x 384.7636992312597
      y 823.2472055590604
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:18039652;PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 500.5607703137493
      y 920.3557888374214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905"
      hgnc "NA"
      map_id "M13_141"
      name "paraquat"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa397"
      uniprot "NA"
    ]
    graphics [
      x 376.969196850622
      y 899.1863857269375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0042719;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M13_26"
      name "TIM9_minus_TIM10_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa96"
      uniprot "UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 1679.030236869241
      y 1678.7447610399022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re162"
      uniprot "NA"
    ]
    graphics [
      x 1616.999301354343
      y 1771.2598814104463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re120"
      uniprot "NA"
    ]
    graphics [
      x 1234.4864219585374
      y 786.2510796389194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_001752;urn:miriam:ncbigene:847;urn:miriam:ncbigene:847;urn:miriam:ec-code:1.11.1.6;urn:miriam:ensembl:ENSG00000121691;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc:1516;urn:miriam:uniprot:P04040"
      hgnc "HGNC_SYMBOL:CAT"
      map_id "M13_135"
      name "CAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P04040"
    ]
    graphics [
      x 1367.2006021078482
      y 749.7546602542014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_167"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa670"
      uniprot "NA"
    ]
    graphics [
      x 1128.3213813840175
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re158"
      uniprot "NA"
    ]
    graphics [
      x 1267.4248546698432
      y 100.39360701796522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M13_148"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa499"
      uniprot "NA"
    ]
    graphics [
      x 1347.8830793656762
      y 106.56823308123205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_38"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 936.8225672525788
      y 1607.183393193102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M13_97"
      name "Cyt_space_C"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 1279.680055358344
      y 1513.4318297557093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_155"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa651"
      uniprot "NA"
    ]
    graphics [
      x 802.7936295590627
      y 1626.1697957015128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 638.7255004508722
      y 1333.2750301023618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_144"
      name "mt_space_DNA_space_damage"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 570.7603554400989
      y 1210.1154691695472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 442.9172439059954
      y 858.2314421198205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_126"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa376"
      uniprot "NA"
    ]
    graphics [
      x 358.8122560830957
      y 1023.4798789776777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_124"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa374"
      uniprot "NA"
    ]
    graphics [
      x 363.96644367395663
      y 732.4556068389479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_127"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa377"
      uniprot "NA"
    ]
    graphics [
      x 314.7477032063075
      y 983.8432588293655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_125"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa375"
      uniprot "NA"
    ]
    graphics [
      x 446.922095308002
      y 1020.4093175583113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 464.6168241738068
      y 605.0228067079277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643"
      hgnc "HGNC_SYMBOL:SDHD;HGNC_SYMBOL:SDHC"
      map_id "M13_3"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      x 1130.9272249653513
      y 756.0821268101729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1003.3973342961409
      y 787.5969647003856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:23410;urn:miriam:ncbigene:23410;urn:miriam:uniprot:Q9NTG7;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc:14931;urn:miriam:refseq:NM_001017524;urn:miriam:ec-code:2.3.1.286;urn:miriam:ensembl:ENSG00000142082"
      hgnc "HGNC_SYMBOL:SIRT3"
      map_id "M13_92"
      name "SIRT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205"
      uniprot "UNIPROT:Q9NTG7"
    ]
    graphics [
      x 1113.8701858118015
      y 856.2017820674969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:12032145"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1859.4276667078116
      y 796.3988925266174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M13_140"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 1945.5595556842063
      y 706.6720621610941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ec-code:1.8.1.9;urn:miriam:ensembl:ENSG00000184470;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:refseq:NM_006440;urn:miriam:hgnc:18155;urn:miriam:ncbigene:10587;urn:miriam:ncbigene:10587;urn:miriam:uniprot:Q9NNW7"
      hgnc "HGNC_SYMBOL:TXNRD2"
      map_id "M13_138"
      name "TXNRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa393"
      uniprot "UNIPROT:Q9NNW7"
    ]
    graphics [
      x 1945.5574649613798
      y 892.4958130073136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M13_139"
      name "NADP(_plus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa394"
      uniprot "NA"
    ]
    graphics [
      x 1842.8149346896464
      y 694.4168436821259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908"
      hgnc "NA"
      map_id "M13_90"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 428.8295154712748
      y 1478.8515725514053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:19355884"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 390.5650534326421
      y 1336.392445867887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "M13_99"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 356.45247634850307
      y 1480.3729610016694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_153"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa649"
      uniprot "NA"
    ]
    graphics [
      x 298.7801052588984
      y 1442.4048674997116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_151"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa644"
      uniprot "NA"
    ]
    graphics [
      x 1530.5385048272028
      y 1024.4248927863491
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_34"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 1416.2967050273016
      y 878.8256157506526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_185"
      name "s1197"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa717"
      uniprot "NA"
    ]
    graphics [
      x 1708.877689681407
      y 640.5558175992969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1589.7207394503882
      y 712.4313600155273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_1"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "NA"
    ]
    graphics [
      x 1731.3931534413396
      y 812.6618085656534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045277;urn:miriam:uniprot:P00414;urn:miriam:uniprot:P00414;urn:miriam:ncbigene:4514;urn:miriam:ncbigene:4514;urn:miriam:hgnc:7422;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ensembl:ENSG00000198938;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ec-code:7.1.1.9;urn:miriam:refseq:YP_003024032;urn:miriam:uniprot:P10176;urn:miriam:uniprot:P10176;urn:miriam:ncbigene:1351;urn:miriam:ncbigene:1351;urn:miriam:refseq:NM_004074;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc:2294;urn:miriam:ensembl:ENSG00000176340;urn:miriam:uniprot:P00403;urn:miriam:uniprot:P00403;urn:miriam:refseq:YP_003024029;urn:miriam:hgnc:7421;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:ec-code:7.1.1.9;urn:miriam:ensembl:ENSG00000198712;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:1337;urn:miriam:ncbigene:1337;urn:miriam:hgnc:2277;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:refseq:NM_004373;urn:miriam:ensembl:ENSG00000111775;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P14854;urn:miriam:uniprot:P14854;urn:miriam:ensembl:ENSG00000126267;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:ncbigene:1340;urn:miriam:ncbigene:1340;urn:miriam:hgnc:2280;urn:miriam:refseq:NM_001863;urn:miriam:refseq:NM_001862;urn:miriam:ncbigene:1329;urn:miriam:ncbigene:1329;urn:miriam:hgnc:2269;urn:miriam:hgnc.symbol:COX5B;urn:miriam:uniprot:P10606;urn:miriam:uniprot:P10606;urn:miriam:hgnc.symbol:COX5B;urn:miriam:ensembl:ENSG00000135940;urn:miriam:uniprot:P15954;urn:miriam:uniprot:P15954;urn:miriam:ncbigene:1350;urn:miriam:refseq:NM_001867;urn:miriam:ncbigene:1350;urn:miriam:hgnc.symbol:COX7C;urn:miriam:hgnc.symbol:COX7C;urn:miriam:ensembl:ENSG00000127184;urn:miriam:hgnc:2292;urn:miriam:refseq:NM_005205;urn:miriam:uniprot:Q02221;urn:miriam:uniprot:Q02221;urn:miriam:ncbigene:1339;urn:miriam:ncbigene:1339;urn:miriam:hgnc:2279;urn:miriam:ensembl:ENSG00000156885;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:ncbigene:1349;urn:miriam:ncbigene:1349;urn:miriam:uniprot:P24311;urn:miriam:uniprot:P24311;urn:miriam:ensembl:ENSG00000131174;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc:2291;urn:miriam:refseq:NM_001866;urn:miriam:hgnc:24381;urn:miriam:ensembl:ENSG00000170516;urn:miriam:refseq:NM_130902;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:ncbigene:170712;urn:miriam:ncbigene:170712;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q6YFQ2;urn:miriam:uniprot:Q6YFQ2;urn:miriam:ensembl:ENSG00000160471;urn:miriam:hgnc:24380;urn:miriam:refseq:NM_144613;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:ncbigene:125965;urn:miriam:ncbigene:125965;urn:miriam:hgnc:2265;urn:miriam:ncbigene:1327;urn:miriam:refseq:NM_001861;urn:miriam:ncbigene:1327;urn:miriam:ensembl:ENSG00000131143;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:uniprot:P13073;urn:miriam:uniprot:P13073;urn:miriam:ncbigene:1346;urn:miriam:ncbigene:1346;urn:miriam:hgnc:2287;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:uniprot:P24310;urn:miriam:uniprot:P24310;urn:miriam:refseq:NM_001864;urn:miriam:ensembl:ENSG00000161281;urn:miriam:uniprot:P14406;urn:miriam:uniprot:P14406;urn:miriam:ncbigene:1347;urn:miriam:ncbigene:1347;urn:miriam:hgnc:2288;urn:miriam:ensembl:ENSG00000112695;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:refseq:NM_001865;urn:miriam:ensembl:ENSG00000178741;urn:miriam:ncbigene:9377;urn:miriam:ncbigene:9377;urn:miriam:refseq:NM_004255;urn:miriam:hgnc:2267;urn:miriam:uniprot:P20674;urn:miriam:uniprot:P20674;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc:16232;urn:miriam:ensembl:ENSG00000131055;urn:miriam:uniprot:Q96KJ9;urn:miriam:uniprot:Q96KJ9;urn:miriam:ncbigene:84701;urn:miriam:ncbigene:84701;urn:miriam:refseq:NM_032609;urn:miriam:hgnc:24382;urn:miriam:ensembl:ENSG00000187581;urn:miriam:uniprot:Q7Z4L0;urn:miriam:uniprot:Q7Z4L0;urn:miriam:refseq:NM_182971;urn:miriam:ncbigene:341947;urn:miriam:ncbigene:341947;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc:2285;urn:miriam:ensembl:ENSG00000164919;urn:miriam:uniprot:P09669;urn:miriam:uniprot:P09669;urn:miriam:refseq:NM_004374;urn:miriam:hgnc.symbol:COX6C;urn:miriam:hgnc.symbol:COX6C;urn:miriam:ncbigene:1345;urn:miriam:ncbigene:1345;urn:miriam:ec-code:7.1.1.9;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:ensembl:ENSG00000198804;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:refseq:YP_003024028;urn:miriam:uniprot:P00395;urn:miriam:uniprot:P00395;urn:miriam:hgnc:7419;urn:miriam:ncbigene:4512;urn:miriam:ncbigene:4512"
      hgnc "HGNC_SYMBOL:MT-CO3;HGNC_SYMBOL:COX8A;HGNC_SYMBOL:MT-CO2;HGNC_SYMBOL:COX6A1;HGNC_SYMBOL:COX6B1;HGNC_SYMBOL:COX5B;HGNC_SYMBOL:COX7C;HGNC_SYMBOL:COX6A2;HGNC_SYMBOL:COX7B;HGNC_SYMBOL:COX7B2;HGNC_SYMBOL:COX6B2;HGNC_SYMBOL:COX4I1;HGNC_SYMBOL:COX7A1;HGNC_SYMBOL:COX7A2;HGNC_SYMBOL:COX5A;HGNC_SYMBOL:COX4I2;HGNC_SYMBOL:COX8C;HGNC_SYMBOL:COX6C;HGNC_SYMBOL:MT-CO1"
      map_id "M13_6"
      name "complex_space_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
    ]
    graphics [
      x 1646.513722454984
      y 590.4612806123846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_111"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa361"
      uniprot "NA"
    ]
    graphics [
      x 621.9444013756491
      y 805.4880006813975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 647.2941406592031
      y 711.001710107509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045275;urn:miriam:ensembl:ENSG00000140740;urn:miriam:refseq:NM_003366;urn:miriam:uniprot:P22695;urn:miriam:uniprot:P22695;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:ncbigene:7385;urn:miriam:ncbigene:7385;urn:miriam:hgnc:12586;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc:30862;urn:miriam:ensembl:ENSG00000127540;urn:miriam:refseq:NM_006830;urn:miriam:ncbigene:10975;urn:miriam:ncbigene:10975;urn:miriam:uniprot:O14957;urn:miriam:uniprot:O14957;urn:miriam:refseq:NM_014402;urn:miriam:ncbigene:27089;urn:miriam:ncbigene:27089;urn:miriam:uniprot:O14949;urn:miriam:uniprot:O14949;urn:miriam:ensembl:ENSG00000164405;urn:miriam:hgnc:29594;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:refseq:NM_003365;urn:miriam:uniprot:P31930;urn:miriam:uniprot:P31930;urn:miriam:ncbigene:7384;urn:miriam:ensembl:ENSG00000010256;urn:miriam:ncbigene:7384;urn:miriam:hgnc:12585;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc:1020;urn:miriam:uniprot:Q9Y276;urn:miriam:uniprot:Q9Y276;urn:miriam:ensembl:ENSG00000074582;urn:miriam:ncbigene:617;urn:miriam:ncbigene:617;urn:miriam:refseq:NM_004328;urn:miriam:ncbigene:100128525;urn:miriam:hgnc:12588;urn:miriam:uniprot:P0C7P4;urn:miriam:uniprot:P0C7P4;urn:miriam:refseq:NG_009458;urn:miriam:ensembl:ENSG00000226085;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:ncbigene:7381;urn:miriam:ncbigene:7381;urn:miriam:ensembl:ENSG00000156467;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:refseq:NM_006294;urn:miriam:uniprot:P14927;urn:miriam:uniprot:P14927;urn:miriam:hgnc:12582;urn:miriam:hgnc:12590;urn:miriam:uniprot:P07919;urn:miriam:uniprot:P07919;urn:miriam:refseq:NM_006004;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:ncbigene:7388;urn:miriam:ncbigene:7388;urn:miriam:ensembl:ENSG00000173660;urn:miriam:refseq:NM_013387;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc:30863;urn:miriam:ncbigene:29796;urn:miriam:ncbigene:29796;urn:miriam:uniprot:Q9UDW1;urn:miriam:uniprot:Q9UDW1;urn:miriam:ensembl:ENSG00000184076"
      hgnc "HGNC_SYMBOL:UQCRC2;HGNC_SYMBOL:UQCR11;HGNC_SYMBOL:UQCRQ;HGNC_SYMBOL:UQCRC1;HGNC_SYMBOL:BCS1L;HGNC_SYMBOL:UQCRFS1P1;HGNC_SYMBOL:UQCRB;HGNC_SYMBOL:UQCRH;HGNC_SYMBOL:UQCR10"
      map_id "M13_5"
      name "complex_space_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
    ]
    graphics [
      x 1729.364924245524
      y 1156.5320562600955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_33"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 1637.222524368935
      y 1119.889224742265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ec-code:6.2.1.5;urn:miriam:hgnc:11448;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:refseq:NM_003850;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:ensembl:ENSG00000136143;urn:miriam:uniprot:Q9P2R7;urn:miriam:uniprot:Q9P2R7;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477;urn:miriam:ec-code:6.2.1.5;urn:miriam:ensembl:ENSG00000163541;urn:miriam:hgnc:11449;urn:miriam:ec-code:6.2.1.4;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:refseq:NM_003849;urn:miriam:uniprot:P53597;urn:miriam:uniprot:P53597;urn:miriam:ncbigene:8802;urn:miriam:ncbigene:8802;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:ncbigene:1716;urn:miriam:ncbigene:1716;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:refseq:NM_001318859;urn:miriam:uniprot:Q16854;urn:miriam:uniprot:Q16854;urn:miriam:hgnc:2858;urn:miriam:ensembl:ENSG00000114956;urn:miriam:hgnc.symbol:TK2;urn:miriam:hgnc.symbol:TK2;urn:miriam:ncbigene:7084;urn:miriam:ncbigene:7084;urn:miriam:hgnc:11831;urn:miriam:ensembl:ENSG00000166548;urn:miriam:ec-code:2.7.1.21;urn:miriam:refseq:NM_001172643;urn:miriam:uniprot:O00142;urn:miriam:uniprot:O00142"
      hgnc "HGNC_SYMBOL:SUCLA2;HGNC_SYMBOL:RRM2B;HGNC_SYMBOL:SUCLG1;HGNC_SYMBOL:DGUOK;HGNC_SYMBOL:TK2"
      map_id "M13_11"
      name "Mt_minus_dNTP_space_pool"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa52"
      uniprot "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
    ]
    graphics [
      x 628.3651439535984
      y 1244.1998824959367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_85"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 567.6323128463134
      y 1098.3413381206456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1101.8885853485795
      y 547.1216125040114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 812.3142621461361
      y 461.216283933349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_123"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 754.8801940132784
      y 326.52170316435047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_000454;urn:miriam:uniprot:P00441;urn:miriam:ensembl:ENSG00000142168;urn:miriam:hgnc:11179;urn:miriam:hgnc.symbol:SOD1;urn:miriam:hgnc.symbol:SOD1;urn:miriam:ncbigene:6647;urn:miriam:ncbigene:6647;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD1"
      map_id "M13_122"
      name "SOD1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa372"
      uniprot "UNIPROT:P00441"
    ]
    graphics [
      x 827.3616190740493
      y 314.82892097245747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 790.8329499918766
      y 699.2375324424903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 488.0793561914648
      y 784.435662895057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_96"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 1766.5582065547646
      y 1335.110328958442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1630.9695983968463
      y 1368.0981130065043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M13_87"
      name "Cyt_space_C"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 1719.5781366940046
      y 1272.6847813166792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_89"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa19"
      uniprot "NA"
    ]
    graphics [
      x 1624.7779714088915
      y 1268.8810037091189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "M13_98"
      name "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa255"
      uniprot "NA"
    ]
    graphics [
      x 1738.8575032582887
      y 1459.7927825842821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29356"
      hgnc "NA"
      map_id "M13_95"
      name "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 1775.4177060480488
      y 1400.2617988370457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "M13_150"
      name "Q"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1669.9449302510238
      y 1489.3989443136006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_184"
      name "s1196"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa716"
      uniprot "NA"
    ]
    graphics [
      x 1771.2388770468674
      y 973.5913552858441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1643.8826819409308
      y 947.9494841191555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 1152.735610835616
      y 314.06868077946365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742612"
      hgnc "NA"
      map_id "M13_174"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa693"
      uniprot "NA"
    ]
    graphics [
      x 1303.741129542663
      y 285.4434796170759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_175"
      name "s1167"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa694"
      uniprot "NA"
    ]
    graphics [
      x 1321.984297130689
      y 349.3132965059607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_158"
      name "s1119"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa655"
      uniprot "NA"
    ]
    graphics [
      x 616.9511741101885
      y 984.1249833140959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 673.2324717044744
      y 1073.793715490825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 712.5927016974308
      y 1346.9362934810786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:ncbigene:1161;urn:miriam:ncbigene:1161;urn:miriam:uniprot:Q13216;urn:miriam:uniprot:Q13216;urn:miriam:hgnc:3439;urn:miriam:refseq:NM_000082;urn:miriam:ensembl:ENSG00000049167;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:ensembl:ENSG00000225830;urn:miriam:refseq:NM_000124;urn:miriam:ncbigene:2074;urn:miriam:ncbigene:2074;urn:miriam:hgnc:3438;urn:miriam:uniprot:P0DP91;urn:miriam:uniprot:P0DP91;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:ec-code:3.6.4.-"
      hgnc "HGNC_SYMBOL:ERCC8;HGNC_SYMBOL:POLG;HGNC_SYMBOL:ERCC6"
      map_id "M13_13"
      name "Mt_minus_DNA_space_repair"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
    ]
    graphics [
      x 882.9230436024883
      y 1424.6576265168073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 186
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_162"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_42"
      target_id "M13_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_169"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_45"
      target_id "M13_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_110"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_121"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_108"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_64"
      target_id "M13_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_100"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_101"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_8"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 17
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M13_103"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 14
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_49"
      target_id "M13_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_161"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 3
    target 20
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_43"
      target_id "M13_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_182"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 7
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_165"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_164"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 25
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_166"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_44"
      target_id "M13_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 12
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_131"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 29
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_129"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 30
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_128"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 27
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 27
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "M13_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 16
    target 33
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_8"
      target_id "M13_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 33
    target 17
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_32"
      target_id "M13_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_21"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_146"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_41"
      target_id "M13_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_16"
      target_id "M13_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_82"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_180"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_179"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 44
    target 42
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_181"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 42
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_53"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 12
    target 45
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 45
    target 48
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 45
    target 49
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_12"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 53
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_173"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 51
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_84"
      target_id "M13_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_170"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_22"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 58
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_23"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 59
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_172"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 60
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_24"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 61
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_25"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 56
    target 62
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_48"
      target_id "M13_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 12
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_137"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 65
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_27"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 63
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 63
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "M13_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_152"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_93"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_37"
      target_id "M13_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 50
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_143"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 73
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_15"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 74
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_157"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 71
    target 75
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_81"
      target_id "M13_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_159"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 21
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_163"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 4
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_160"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_28"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 26
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 77
    target 79
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_29"
      target_id "M13_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 46
    target 80
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_69"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_107"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_109"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 83
    target 11
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_63"
      target_id "M13_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_88"
      target_id "M13_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 86
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_35"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_168"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 3
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 88
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_46"
      target_id "M13_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_183"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 79
    target 90
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 90
    target 85
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_57"
      target_id "M13_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 72
    target 91
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_143"
      target_id "M13_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 91
    target 40
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_79"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 46
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_117"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 92
    target 94
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "M13_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 92
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "M13_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_178"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_177"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 99
    target 97
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_176"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 97
    target 58
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_52"
      target_id "M13_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_4"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_149"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_55"
      target_id "M13_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 75
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_142"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_17"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 106
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_10"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 37
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_20"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 104
    target 107
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_86"
      target_id "M13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 12
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_115"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 108
    target 94
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 108
    target 110
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 108
    target 46
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 32
    target 111
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_130"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_132"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 113
    target 111
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_134"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 111
    target 28
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "M13_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 111
    target 114
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "M13_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 103
    target 115
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_19"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 69
    target 115
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_93"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 79
    target 115
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 115
    target 100
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "M13_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "M13_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_91"
      target_id "M13_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 118
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_31"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 47
    target 119
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 119
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_67"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_9"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 8
    target 121
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_110"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 79
    target 121
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_78"
      target_id "M13_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_26"
      target_id "M13_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 99
    target 124
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_176"
      target_id "M13_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 124
    target 61
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_51"
      target_id "M13_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 12
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_135"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 125
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_30"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_167"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 129
    target 128
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_148"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 128
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_47"
      target_id "M13_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 70
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_154"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_97"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 130
    target 132
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_38"
      target_id "M13_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 50
    target 133
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_144"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 133
    target 74
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_39"
      target_id "M13_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 47
    target 135
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_126"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 135
    target 137
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 135
    target 138
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 135
    target 139
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 137
    target 140
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_124"
      target_id "M13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 140
    target 47
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_73"
      target_id "M13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_3"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_92"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 26
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 142
    target 100
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_54"
      target_id "M13_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 66
    target 144
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_136"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 145
    target 144
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_140"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 146
    target 144
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_138"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 144
    target 64
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "M13_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 144
    target 147
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "M13_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_90"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 79
    target 149
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 149
    target 151
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 149
    target 67
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_151"
      target_id "M13_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 153
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_34"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 154
    target 155
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_185"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 156
    target 155
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_1"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 26
    target 155
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 155
    target 157
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_59"
      target_id "M13_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_111"
      target_id "M13_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 159
    target 8
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_65"
      target_id "M13_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 160
    target 161
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_5"
      target_id "M13_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 161
    target 152
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_33"
      target_id "M13_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_11"
      target_id "M13_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 163
    target 40
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_85"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 12
    target 164
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 164
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_71"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 47
    target 165
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 166
    target 165
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_123"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 167
    target 165
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_122"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 165
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_60"
      target_id "M13_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 8
    target 168
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_110"
      target_id "M13_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 168
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_66"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 122
    target 169
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_141"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 158
    target 169
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_111"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 169
    target 120
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "M13_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 169
    target 8
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "M13_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_96"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 172
    target 171
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_87"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 173
    target 171
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_89"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 174
    target 171
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_98"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 160
    target 171
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_5"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 171
    target 175
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 171
    target 131
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 171
    target 176
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 171
    target 117
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_184"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 156
    target 178
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_1"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 26
    target 178
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 178
    target 160
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_58"
      target_id "M13_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 53
    target 179
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_173"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 180
    target 179
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_174"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 179
    target 181
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_50"
      target_id "M13_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 182
    target 183
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_158"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 134
    target 183
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_144"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 40
    target 183
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_145"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 183
    target 50
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_80"
      target_id "M13_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 74
    target 184
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_157"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 72
    target 184
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_143"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 185
    target 184
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_13"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 162
    target 184
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_11"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 184
    target 50
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_40"
      target_id "M13_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
