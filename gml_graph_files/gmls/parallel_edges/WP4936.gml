# generated with VANTED V2.8.2 at Fri Mar 04 09:57:01 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000204673"
      hgnc "NA"
      map_id "W6_29"
      name "AKT1S1"
      node_subtype "GENE"
      node_type "species"
      org_id "c9d11"
      uniprot "NA"
    ]
    graphics [
      x 191.65736903054153
      y 902.9440811231127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id63264543"
      uniprot "NA"
    ]
    graphics [
      x 289.64170917891454
      y 788.820453955408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:2475;urn:miriam:ncbiprotein:64223;urn:miriam:ncbiprotein:57521;urn:miriam:ncbiprotein:64798"
      hgnc "NA"
      map_id "W6_27"
      name "c5e75"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c5e75"
      uniprot "NA"
    ]
    graphics [
      x 424.66468012398605
      y 668.5911800800649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6009"
      hgnc "NA"
      map_id "W6_34"
      name "RHEB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d10c6"
      uniprot "NA"
    ]
    graphics [
      x 667.3928856045491
      y 627.6667431100134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35a4115e"
      uniprot "NA"
    ]
    graphics [
      x 531.7195926945348
      y 616.7881863569676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "W6_46"
      name "GSK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ec6c1"
      uniprot "NA"
    ]
    graphics [
      x 671.3825214944131
      y 952.829528541661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id788d6f1c"
      uniprot "NA"
    ]
    graphics [
      x 631.3563146450224
      y 1117.4648042129802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "W6_48"
      name "TSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed654"
      uniprot "NA"
    ]
    graphics [
      x 614.8507476309289
      y 1264.2085759974143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_47"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "ec84a"
      uniprot "NA"
    ]
    graphics [
      x 1501.0372217140903
      y 1131.3266522309118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_3"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a2165"
      uniprot "NA"
    ]
    graphics [
      x 1596.677962180664
      y 1039.495019116843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:27072;urn:miriam:ncbiprotein:55823;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ensembl:ENSG00000104142;urn:miriam:ncbiprotein:64601;urn:miriam:ensembl:ENSG00000139719"
      hgnc "NA"
      map_id "W6_51"
      name "HOPS_space_COMPLEX_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f7bb6"
      uniprot "NA"
    ]
    graphics [
      x 1648.703420782204
      y 896.9039913053873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5315"
      hgnc "NA"
      map_id "W6_7"
      name "PKM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a628b"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 897.4825203200803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_57"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id28c533ea"
      uniprot "NA"
    ]
    graphics [
      x 100.43813871258374
      y 999.758313595899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:27072"
      hgnc "NA"
      map_id "W6_15"
      name "VPS41"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ba29d"
      uniprot "NA"
    ]
    graphics [
      x 1132.6011430342373
      y 900.8617633177788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_72"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd35c42c0"
      uniprot "NA"
    ]
    graphics [
      x 1020.3577188674349
      y 872.5456245741373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:7879"
      hgnc "NA"
      map_id "W6_14"
      name "RAB7A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b9f16"
      uniprot "NA"
    ]
    graphics [
      x 889.5959977782471
      y 855.058446024767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5563"
      hgnc "NA"
      map_id "W6_5"
      name "AMPK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a5318"
      uniprot "NA"
    ]
    graphics [
      x 298.8242596363458
      y 463.1221312898291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_60"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3a40ef1e"
      uniprot "NA"
    ]
    graphics [
      x 343.85441495538015
      y 563.5060454641346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302;urn:miriam:pubmed:33845483"
      hgnc "NA"
      map_id "W6_12"
      name "NSP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6a2a"
      uniprot "NA"
    ]
    graphics [
      x 608.1869906831471
      y 192.94588620105407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_50"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f725b"
      uniprot "NA"
    ]
    graphics [
      x 729.5035932378106
      y 108.55430267333088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000158528"
      hgnc "NA"
      map_id "W6_10"
      name "PPP1R9A"
      node_subtype "GENE"
      node_type "species"
      org_id "ad6e9"
      uniprot "NA"
    ]
    graphics [
      x 885.3168077676147
      y 125.22702084269065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000166887"
      hgnc "NA"
      map_id "W6_45"
      name "VPS39"
      node_subtype "GENE"
      node_type "species"
      org_id "e92b4"
      uniprot "NA"
    ]
    graphics [
      x 649.8823158237707
      y 755.2620276666946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_70"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idc3daa4b8"
      uniprot "NA"
    ]
    graphics [
      x 732.2416447715664
      y 832.9966008188329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000108443"
      hgnc "NA"
      map_id "W6_21"
      name "P70S6K"
      node_subtype "GENE"
      node_type "species"
      org_id "bfbce"
      uniprot "NA"
    ]
    graphics [
      x 801.8827790138731
      y 789.8539635922032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8825c1d0"
      uniprot "NA"
    ]
    graphics [
      x 910.4756762383653
      y 618.8762458657998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6194"
      hgnc "NA"
      map_id "W6_24"
      name "RPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1a5f"
      uniprot "NA"
    ]
    graphics [
      x 1005.1063418973823
      y 422.0271596862814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "W6_2"
      name "Phagophore"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a1420"
      uniprot "NA"
    ]
    graphics [
      x 987.6493057377143
      y 112.20220337551359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id949da026"
      uniprot "NA"
    ]
    graphics [
      x 1078.0839469338016
      y 210.68439078582827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000116209"
      hgnc "NA"
      map_id "W6_39"
      name "TMEM59"
      node_subtype "GENE"
      node_type "species"
      org_id "e10fa"
      uniprot "NA"
    ]
    graphics [
      x 1197.4969157034284
      y 239.05848386650013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000101844"
      hgnc "NA"
      map_id "W6_18"
      name "ATG4A"
      node_subtype "GENE"
      node_type "species"
      org_id "bc0e6"
      uniprot "NA"
    ]
    graphics [
      x 1046.401115192328
      y 88.84602601972699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:9140;urn:miriam:ncbiprotein:55054;urn:miriam:ncbiprotein:9474"
      hgnc "NA"
      map_id "W6_37"
      name "ddc93"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ddc93"
      uniprot "NA"
    ]
    graphics [
      x 1072.860353320985
      y 324.62730070803957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10533"
      hgnc "NA"
      map_id "W6_13"
      name "ATG7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8149"
      uniprot "NA"
    ]
    graphics [
      x 982.5083954135205
      y 272.87590481395523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:64422"
      hgnc "NA"
      map_id "W6_22"
      name "ATG3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bfd6f"
      uniprot "NA"
    ]
    graphics [
      x 1103.3667085002226
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10133"
      hgnc "NA"
      map_id "W6_11"
      name "OPTN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b3adb"
      uniprot "NA"
    ]
    graphics [
      x 1189.698667747648
      y 164.50127261161117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000066739"
      hgnc "NA"
      map_id "W6_4"
      name "ATG2B"
      node_subtype "GENE"
      node_type "species"
      org_id "a3222"
      uniprot "NA"
    ]
    graphics [
      x 964.1100724741815
      y 188.815048702044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000023287;urn:miriam:ncbiprotein:9776;urn:miriam:ncbiprotein:8408"
      hgnc "NA"
      map_id "W6_31"
      name "cf768"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cf768"
      uniprot "NA"
    ]
    graphics [
      x 833.4243950997524
      y 330.99445798778777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8878"
      hgnc "NA"
      map_id "W6_9"
      name "SQSTM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acae9"
      uniprot "NA"
    ]
    graphics [
      x 1154.455028298133
      y 104.3202324643704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10241"
      hgnc "NA"
      map_id "W6_16"
      name "CALCOCO2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bbbe9"
      uniprot "NA"
    ]
    graphics [
      x 1154.9248694041785
      y 307.79718836969795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_8"
      name "Autophagosome_slash__br_Late_space_endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa8e9"
      uniprot "NA"
    ]
    graphics [
      x 1301.2566139144626
      y 308.463158108602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:11345"
      hgnc "NA"
      map_id "W6_17"
      name "GABARAPL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc090"
      uniprot "NA"
    ]
    graphics [
      x 1277.161334235816
      y 119.14500509955201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_30"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "cbf03"
      uniprot "NA"
    ]
    graphics [
      x 680.01716169525
      y 432.8801556310808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_69"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id9545f48f"
      uniprot "NA"
    ]
    graphics [
      x 747.4113391895414
      y 524.3132125934413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_73"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide513164c"
      uniprot "NA"
    ]
    graphics [
      x 580.3427204872835
      y 770.0274252798587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7a84a2"
      uniprot "NA"
    ]
    graphics [
      x 407.9595059781993
      y 811.5587448312283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000155506"
      hgnc "NA"
      map_id "W6_1"
      name "LARP1"
      node_subtype "GENE"
      node_type "species"
      org_id "a13c2"
      uniprot "NA"
    ]
    graphics [
      x 443.30654865968165
      y 934.7596298664891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_71"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idd277ed15"
      uniprot "NA"
    ]
    graphics [
      x 603.8444451436394
      y 487.58469258727825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id20e40bd3"
      uniprot "NA"
    ]
    graphics [
      x 1486.4215548214256
      y 403.2892566671032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_52"
      name "Lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8183"
      uniprot "NA"
    ]
    graphics [
      x 1405.5382979459632
      y 476.3954674602238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000118640"
      hgnc "NA"
      map_id "W6_40"
      name "VAMP8"
      node_subtype "GENE"
      node_type "species"
      org_id "e1e54"
      uniprot "NA"
    ]
    graphics [
      x 1597.389402207103
      y 566.6027327007359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_33"
      name "Autophago_br_lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d106b"
      uniprot "NA"
    ]
    graphics [
      x 1513.6733438912315
      y 508.9524105671105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id180e5540"
      uniprot "NA"
    ]
    graphics [
      x 1650.5682013796886
      y 731.0578460455271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:64798"
      hgnc "NA"
      map_id "W6_23"
      name "DEPTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c152e"
      uniprot "NA"
    ]
    graphics [
      x 894.4735152952368
      y 1153.3261173162014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_59"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id396f48b2"
      uniprot "NA"
    ]
    graphics [
      x 952.0484753307807
      y 1049.4371738537104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:2475"
      hgnc "NA"
      map_id "W6_19"
      name "MTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc95b"
      uniprot "NA"
    ]
    graphics [
      x 1058.4466742819382
      y 1035.2440480617574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fbe32"
      uniprot "NA"
    ]
    graphics [
      x 704.1971634096185
      y 315.9292762276172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:79065"
      hgnc "NA"
      map_id "W6_43"
      name "ATG9A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6873"
      uniprot "NA"
    ]
    graphics [
      x 832.3238948889647
      y 432.2358951402364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id67a2f315"
      uniprot "NA"
    ]
    graphics [
      x 726.5204355955352
      y 760.7357547117196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e048e"
      uniprot "NA"
    ]
    graphics [
      x 468.40647143049887
      y 189.46964024187895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000188554"
      hgnc "NA"
      map_id "W6_32"
      name "NBR1"
      node_subtype "GENE"
      node_type "species"
      org_id "d05df"
      uniprot "NA"
    ]
    graphics [
      x 351.1485480114618
      y 177.24000677635706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:uniprot:P31749"
      hgnc "NA"
      map_id "W6_26"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c34e3"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 751.3444464547098
      y 1427.998088382898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide9784478"
      uniprot "NA"
    ]
    graphics [
      x 636.1789955664367
      y 1394.130480275846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_25"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "c28d5"
      uniprot "NA"
    ]
    graphics [
      x 1573.8475324969072
      y 104.48850804721644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "db1e2"
      uniprot "NA"
    ]
    graphics [
      x 1433.1038823130707
      y 81.03037592483577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000139719"
      hgnc "NA"
      map_id "W6_42"
      name "VPS33A"
      node_subtype "GENE"
      node_type "species"
      org_id "e65f9"
      uniprot "NA"
    ]
    graphics [
      x 1371.3883340555712
      y 835.2723578290038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_35"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "da259"
      uniprot "NA"
    ]
    graphics [
      x 1471.5194151214719
      y 790.7714600787458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_49"
      name "SNAREs"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f13f3"
      uniprot "NA"
    ]
    graphics [
      x 1505.8028650609947
      y 896.0628432470362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf121"
      uniprot "NA"
    ]
    graphics [
      x 1680.5973685335766
      y 184.05434868323033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8887"
      hgnc "NA"
      map_id "W6_6"
      name "TAX1BP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a579d"
      uniprot "NA"
    ]
    graphics [
      x 1685.9066100235548
      y 312.0025973563772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id259f64e"
      uniprot "NA"
    ]
    graphics [
      x 818.110492660275
      y 939.2014787167691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000063046"
      hgnc "NA"
      map_id "W6_28"
      name "EIF4B"
      node_subtype "GENE"
      node_type "species"
      org_id "c8253"
      uniprot "NA"
    ]
    graphics [
      x 855.2534922501534
      y 1055.5839465475815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_61"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id47e9ee53"
      uniprot "NA"
    ]
    graphics [
      x 950.2729697267616
      y 520.5356190112285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "W6_41"
      name "e44d3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e44d3"
      uniprot "NA"
    ]
    graphics [
      x 1052.1941629235944
      y 596.9178245493982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000149357"
      hgnc "NA"
      map_id "W6_44"
      name "LAMTOR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e6980"
      uniprot "NA"
    ]
    graphics [
      x 157.15690335451973
      y 639.5724017458783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id853c2e26"
      uniprot "NA"
    ]
    graphics [
      x 276.3155807111192
      y 659.306177561588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 75
    source 1
    target 2
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_29"
      target_id "W6_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 2
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_62"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 4
    target 5
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_34"
      target_id "W6_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 5
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_58"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 6
    target 7
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_46"
      target_id "W6_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 7
    target 8
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_64"
      target_id "W6_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 9
    target 10
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_47"
      target_id "W6_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 10
    target 11
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_3"
      target_id "W6_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 12
    target 13
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_7"
      target_id "W6_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 13
    target 1
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_57"
      target_id "W6_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 14
    target 15
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_15"
      target_id "W6_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 15
    target 16
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_72"
      target_id "W6_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 17
    target 18
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_5"
      target_id "W6_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 18
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_60"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 19
    target 20
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 20
    target 21
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_50"
      target_id "W6_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 22
    target 23
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_45"
      target_id "W6_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 23
    target 16
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_70"
      target_id "W6_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 24
    target 25
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_21"
      target_id "W6_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 25
    target 26
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_67"
      target_id "W6_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 27
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_2"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 29
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_39"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 30
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_18"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 31
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_37"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 32
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_13"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 33
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_22"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 34
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_11"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 35
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_4"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 36
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_31"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 37
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_9"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 26
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "INHIBITION"
      source_id "W6_24"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 21
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_10"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 38
    target 28
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_16"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 28
    target 39
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "W6_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 28
    target 40
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "W6_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 41
    target 42
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_30"
      target_id "W6_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 42
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_69"
      target_id "W6_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 3
    target 43
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 43
    target 24
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_73"
      target_id "W6_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 3
    target 44
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 44
    target 45
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_65"
      target_id "W6_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 3
    target 46
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 46
    target 36
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_71"
      target_id "W6_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 39
    target 47
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_8"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 48
    target 47
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_52"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 49
    target 47
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_40"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 47
    target 50
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_55"
      target_id "W6_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 11
    target 51
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_51"
      target_id "W6_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 51
    target 49
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_54"
      target_id "W6_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 52
    target 53
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_23"
      target_id "W6_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 53
    target 54
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_59"
      target_id "W6_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 19
    target 55
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 55
    target 56
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_53"
      target_id "W6_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 6
    target 57
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_46"
      target_id "W6_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 57
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_63"
      target_id "W6_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 19
    target 58
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 58
    target 59
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_38"
      target_id "W6_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 60
    target 61
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_26"
      target_id "W6_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 61
    target 8
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_74"
      target_id "W6_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 62
    target 63
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_25"
      target_id "W6_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 63
    target 40
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_36"
      target_id "W6_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 64
    target 65
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_42"
      target_id "W6_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 65
    target 66
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_35"
      target_id "W6_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 62
    target 67
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_25"
      target_id "W6_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 67
    target 68
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_20"
      target_id "W6_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 24
    target 69
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_21"
      target_id "W6_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 69
    target 70
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_56"
      target_id "W6_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 56
    target 71
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_43"
      target_id "W6_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 71
    target 72
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_61"
      target_id "W6_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 73
    target 74
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_44"
      target_id "W6_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 74
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_66"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
