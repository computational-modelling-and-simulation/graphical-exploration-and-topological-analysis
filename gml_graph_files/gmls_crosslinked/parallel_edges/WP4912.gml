# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; WP5038; WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "Endocytosis"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d77e4; c78a2; cde94"
      uniprot "NA"
    ]
    graphics [
      x 1262.7503385949221
      y 814.8217523982565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Endocytosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4912; WP4880"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "TICAM1"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "cea88; f9104; b7b77"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 449.92787059830994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TICAM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 254.09744828873642
      y 409.8376219254228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; WP4912; WP4868; WP4880; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "MAVS"
      name "MAVS"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e240d; f0e60; fa763; a978e; ae650"
      uniprot "NA"
    ]
    graphics [
      x 719.4574929997583
      y 174.75624455890323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAVS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4912"
      full_annotation "urn:miriam:hgnc.symbol:IFIH1; urn:miriam:ensembl:ENSG00000115267"
      hgnc "HGNC_SYMBOL:IFIH1; NA"
      map_id "IFIH1"
      name "IFIH1"
      node_subtype "GENE"
      node_type "species"
      org_id "c1e37; e1fcd"
      uniprot "NA"
    ]
    graphics [
      x 1218.0577267342787
      y 1033.140401142816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFIH1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4883; WP4969; WP4912; WP5039; WP4853"
      full_annotation "urn:miriam:ncbigene:59272; urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272; urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "ACE2"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "bf1a9; aa820; dc981; c2d8e; a9be1; f1b6b; f3245"
      uniprot "NA"
    ]
    graphics [
      x 817.2595793500736
      y 629.0322213207085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000162434;urn:miriam:ensembl:ENSG00000105397;urn:miriam:ncbigene:3454"
      hgnc "NA"
      map_id "cd056"
      name "cd056"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cd056"
      uniprot "NA"
    ]
    graphics [
      x 720.3724582913942
      y 1318.9843158343806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cd056"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_29"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id683cbfa3"
      uniprot "NA"
    ]
    graphics [
      x 833.5313791698973
      y 1273.8197834178313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "f278d"
      name "f278d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f278d"
      uniprot "NA"
    ]
    graphics [
      x 803.4098345463001
      y 1144.7088636600852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f278d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4912"
      full_annotation "urn:miriam:uniprot:694009; urn:miriam:uniprot:694009;urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "UNIPROT:694009"
      name "S; a5c10"
      node_subtype "GENE; COMPLEX"
      node_type "species"
      org_id "c8d7f; a5c10; db168"
      uniprot "UNIPROT:694009"
    ]
    graphics [
      x 1048.3850663055368
      y 653.9223549288071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:694009"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida37bfa65"
      uniprot "NA"
    ]
    graphics [
      x 1160.4817300312122
      y 730.1609960434571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2489aa99"
      uniprot "NA"
    ]
    graphics [
      x 683.0505848679763
      y 1030.6773651495268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000213928"
      hgnc "NA"
      map_id "IRF9"
      name "IRF9"
      node_subtype "GENE"
      node_type "species"
      org_id "cffa4"
      uniprot "NA"
    ]
    graphics [
      x 728.0225654136759
      y 919.0487508187581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000213928;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "ed2d7"
      name "ed2d7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ed2d7"
      uniprot "NA"
    ]
    graphics [
      x 563.2749001382872
      y 971.3150420601211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ed2d7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb755ca84"
      uniprot "NA"
    ]
    graphics [
      x 920.0101200453751
      y 696.2137431906041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "ea539"
      name "ea539"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea539"
      uniprot "NA"
    ]
    graphics [
      x 275.4489050707554
      y 200.72175948533715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ea539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:15681410"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_36"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided8e62a3"
      uniprot "NA"
    ]
    graphics [
      x 305.05124555084046
      y 85.01725928226773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "IFN_minus_beta_br_Response_space_element"
      name "IFN_minus_beta_br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "c7d27"
      uniprot "NA"
    ]
    graphics [
      x 417.46276355940927
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_beta_br_Response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_26"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id27cf2cc3"
      uniprot "NA"
    ]
    graphics [
      x 1277.500534444458
      y 937.3308191111873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000120242;urn:miriam:ensembl:ENSG00000228083;urn:miriam:ensembl:ENSG00000186803;urn:miriam:ensembl:ENSG00000234829;urn:miriam:ensembl:ENSG00000233816;urn:miriam:ensembl:ENSG00000147885;urn:miriam:ensembl:ENSG00000197919;urn:miriam:ensembl:ENSG00000120235;urn:miriam:ensembl:ENSG00000236637;urn:miriam:ensembl:ENSG00000188379;urn:miriam:ensembl:ENSG00000147873;urn:miriam:ensembl:ENSG00000137080;urn:miriam:ensembl:ENSG00000214042"
      hgnc "NA"
      map_id "IFN_minus_alpha_space_group"
      name "IFN_minus_alpha_space_group"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b12b5"
      uniprot "NA"
    ]
    graphics [
      x 767.227302894871
      y 1076.088455239498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_alpha_space_group"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide4fe418d"
      uniprot "NA"
    ]
    graphics [
      x 711.6882945674008
      y 1190.9525075506563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_37"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf68781f"
      uniprot "NA"
    ]
    graphics [
      x 1387.3469414470042
      y 810.7200095204553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4912; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "DDX58"
      name "DDX58"
      node_subtype "GENE"
      node_type "species"
      org_id "f1eea; a397d"
      uniprot "NA"
    ]
    graphics [
      x 1395.961263936388
      y 699.8588061796765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DDX58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8d8be581"
      uniprot "NA"
    ]
    graphics [
      x 1061.2865405534267
      y 525.8238113442951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_6"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c5c37"
      uniprot "NA"
    ]
    graphics [
      x 977.035607781055
      y 443.5755111564645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115267;urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "b9f09"
      name "b9f09"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9f09"
      uniprot "NA"
    ]
    graphics [
      x 927.6527902435719
      y 243.91275246728026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b9f09"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3573c07"
      uniprot "NA"
    ]
    graphics [
      x 848.0070112696884
      y 144.40550725381246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_16"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ecbcf"
      uniprot "NA"
    ]
    graphics [
      x 967.1521865952923
      y 142.35478855159226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_39"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf9e7dc0d"
      uniprot "NA"
    ]
    graphics [
      x 444.245266252159
      y 941.3087248757427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "ISRE_space__br_Response_space_element"
      name "ISRE_space__br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "a74fc"
      uniprot "NA"
    ]
    graphics [
      x 340.0473408403251
      y 898.8456799181134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ISRE_space__br_Response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "IFN_minus_beta"
      name "IFN_minus_beta"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5fa0"
      uniprot "NA"
    ]
    graphics [
      x 492.93506780163466
      y 1262.8915848382858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_beta"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_28"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3e37c065"
      uniprot "NA"
    ]
    graphics [
      x 598.1382402157305
      y 1306.3561957276825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_24"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id134a11f0"
      uniprot "NA"
    ]
    graphics [
      x 931.1811859910242
      y 594.4909049505598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf94c3c70"
      uniprot "NA"
    ]
    graphics [
      x 597.158786364221
      y 229.17244343657518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "d1b60"
      name "d1b60"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b60"
      uniprot "NA"
    ]
    graphics [
      x 472.11806061020036
      y 281.77882231896024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d1b60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbcd773be"
      uniprot "NA"
    ]
    graphics [
      x 338.3926935980297
      y 310.4168536139944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id95166c5e"
      uniprot "NA"
    ]
    graphics [
      x 164.79686707028736
      y 493.1703116690255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 38
    source 7
    target 8
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "cd056"
      target_id "W15_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 8
    target 9
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_29"
      target_id "f278d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 10
    target 11
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:694009"
      target_id "W15_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 11
    target 1
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_32"
      target_id "Endocytosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 9
    target 12
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "f278d"
      target_id "W15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 13
    target 12
    cd19dm [
      diagram "WP4912"
      edge_type "MODULATION"
      source_id "IRF9"
      target_id "W15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 12
    target 14
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_25"
      target_id "ed2d7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 6
    target 15
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "ACE2"
      target_id "W15_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 15
    target 10
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_33"
      target_id "UNIPROT:694009"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 16
    target 17
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "ea539"
      target_id "W15_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 17
    target 18
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_36"
      target_id "IFN_minus_beta_br_Response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 1
    target 19
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "Endocytosis"
      target_id "W15_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 19
    target 5
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_26"
      target_id "IFIH1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 20
    target 21
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_alpha_space_group"
      target_id "W15_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 21
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_35"
      target_id "cd056"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 1
    target 22
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "Endocytosis"
      target_id "W15_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 22
    target 23
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_37"
      target_id "DDX58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 10
    target 24
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:694009"
      target_id "W15_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 24
    target 25
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_30"
      target_id "W15_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 26
    target 27
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "b9f09"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 28
    target 27
    cd19dm [
      diagram "WP4912"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W15_16"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 27
    target 4
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_27"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 14
    target 29
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "ed2d7"
      target_id "W15_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 29
    target 30
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_39"
      target_id "ISRE_space__br_Response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 31
    target 32
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_beta"
      target_id "W15_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 32
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_28"
      target_id "cd056"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 10
    target 33
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:694009"
      target_id "W15_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 33
    target 6
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_24"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 4
    target 34
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "MAVS"
      target_id "W15_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 34
    target 35
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_38"
      target_id "d1b60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 3
    target 36
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 35
    target 36
    cd19dm [
      diagram "WP4912"
      edge_type "CATALYSIS"
      source_id "d1b60"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 36
    target 16
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_34"
      target_id "ea539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 2
    target 37
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "TICAM1"
      target_id "W15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 37
    target 3
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_31"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
