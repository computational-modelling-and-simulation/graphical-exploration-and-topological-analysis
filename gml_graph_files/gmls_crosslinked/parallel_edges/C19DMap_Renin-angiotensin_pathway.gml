# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 982.6611635450445
      y 535.8007962067722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 285.33925017237334
      y 737.2100688967822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4799; C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P50052; urn:miriam:ensembl:ENSG00000180772;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc:338;urn:miriam:refseq:NM_000686;urn:miriam:uniprot:P50052;urn:miriam:uniprot:P50052;urn:miriam:ncbigene:186;urn:miriam:ncbigene:186"
      hgnc "NA; HGNC_SYMBOL:AGTR2"
      map_id "UNIPROT:P50052"
      name "AT2R; AGTR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee6b1; sa79; sa25"
      uniprot "UNIPROT:P50052"
    ]
    graphics [
      x 927.0942322307404
      y 1252.2187092743845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50052"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 11
      diagram "WP4799; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P30556; urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "NA; HGNC_SYMBOL:AGTR1"
      map_id "UNIPROT:P30556"
      name "AT1R; AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab2a6; sa102; sa167; sa139; sa140; sa26; sa204; sa500; sa484; sa519; sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 654.2136263071072
      y 1130.980942474535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:obo.chebi:CHEBI%3A48432; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_II"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb; sa23; sa95; sa194"
      uniprot "NA"
    ]
    graphics [
      x 955.9385514233454
      y 737.0244685847659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_I"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a; sa21; sa195"
      uniprot "NA"
    ]
    graphics [
      x 824.0354084416437
      y 430.25792880266806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 7
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P12821; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "NA; HGNC_SYMBOL:ACE"
      map_id "UNIPROT:P12821"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e0c12; sa29; sa146; sa28; sa100; sa199; sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1357.8909590287508
      y 501.8552540831674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12821"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_198923;urn:miriam:ncbigene:116512;urn:miriam:ncbigene:116512;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:uniprot:Q8TDS7;urn:miriam:uniprot:Q8TDS7;urn:miriam:hgnc:29626;urn:miriam:ensembl:ENSG00000172938"
      hgnc "HGNC_SYMBOL:MRGPRD"
      map_id "UNIPROT:Q8TDS7"
      name "MRGPRD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94; sa50"
      uniprot "UNIPROT:Q8TDS7"
    ]
    graphics [
      x 1120.9837726622404
      y 1294.7995909748074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDS7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1221.6667800952466
      y 1146.8100617890839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:44192273"
      hgnc "NA"
      map_id "alamandine"
      name "alamandine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1210.5021054090723
      y 952.0276459893707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "alamandine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_7"
      name "angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24; sa96"
      uniprot "NA"
    ]
    graphics [
      x 1167.4350384585234
      y 777.3290626989924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1304.5808848761092
      y 869.2004795859643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831;PUBMED:19864379;PUBMED:32432918"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_38"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re153"
      uniprot "NA"
    ]
    graphics [
      x 1067.2238709285239
      y 579.4992690457827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16469"
      hgnc "NA"
      map_id "estradiol"
      name "estradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa169; sa200"
      uniprot "NA"
    ]
    graphics [
      x 1280.5251469782652
      y 562.2241969364226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "estradiol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38; sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 794.3585718677085
      y 697.811874325034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17823"
      hgnc "NA"
      map_id "Calcitriol"
      name "Calcitriol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa201; sa37"
      uniprot "NA"
    ]
    graphics [
      x 1007.0533011668291
      y 370.326851243997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Calcitriol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 1180.5186456993038
      y 1050.60343172693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000138792;urn:miriam:ncbigene:2028;urn:miriam:ncbigene:2028;urn:miriam:hgnc:3355;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:refseq:NM_001379611;urn:miriam:ec-code:3.4.11.7;urn:miriam:uniprot:Q07075;urn:miriam:uniprot:Q07075"
      hgnc "HGNC_SYMBOL:ENPEP"
      map_id "UNIPROT:Q07075"
      name "ENPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162; sa64"
      uniprot "UNIPROT:Q07075"
    ]
    graphics [
      x 1588.0356566602509
      y 464.6143819986931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07075"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re191"
      uniprot "NA"
    ]
    graphics [
      x 1619.7825808548973
      y 340.5143463489894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D008297"
      hgnc "NA"
      map_id "sex,_space_male"
      name "sex,_space_male"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa193; sa44"
      uniprot "NA"
    ]
    graphics [
      x 1480.8862921548962
      y 261.30668618909783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "sex,_space_male"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1384.0156439576322
      y 673.7358520021671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A89666"
      hgnc "NA"
      map_id "angiotensin_space_III"
      name "angiotensin_space_III"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa62"
      uniprot "NA"
    ]
    graphics [
      x 1485.1080074434371
      y 901.8292684306799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_III"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_188"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa99"
      uniprot "NA"
    ]
    graphics [
      x 1068.6101253411193
      y 826.0399063437768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097;PUBMED:19375596"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 918.7091514146933
      y 809.0544525593909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P04201"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa77; sa141; sa496; sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1075.0184693706801
      y 1413.827123766962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_42"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 1012.8897089850243
      y 1545.4590778556278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D013927"
      hgnc "NA"
      map_id "thrombosis"
      name "thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa172"
      uniprot "NA"
    ]
    graphics [
      x 851.6095563926266
      y 1472.8271307924813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "thrombosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:91691124;urn:miriam:kegg.compound:C20970"
      hgnc "NA"
      map_id "angiotensin_space_A"
      name "angiotensin_space_A"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 925.4067130288103
      y 896.4953622644418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1025.3864087430907
      y 790.9538651267741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:32432657"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_56"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re189"
      uniprot "NA"
    ]
    graphics [
      x 1184.3256072488375
      y 464.9053634028561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0007568"
      hgnc "NA"
      map_id "aging"
      name "aging"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 1338.8256401830313
      y 381.7633394031276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aging"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "UNIPROT:P01019"
      name "AGT"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "sa34; sa174; sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 407.17296497478594
      y 684.1004132251543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01019"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:6172448"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re129"
      uniprot "NA"
    ]
    graphics [
      x 528.1139738733809
      y 527.4470346389357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_001909;urn:miriam:ensembl:ENSG00000117984;urn:miriam:ec-code:3.4.23.5;urn:miriam:uniprot:P07339;urn:miriam:uniprot:P07339;urn:miriam:ncbigene:1509;urn:miriam:ncbigene:1509;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc:2529"
      hgnc "HGNC_SYMBOL:CTSD"
      map_id "UNIPROT:P07339"
      name "CTSD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:P07339"
    ]
    graphics [
      x 470.1218685590999
      y 667.5022576098181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:1511;urn:miriam:uniprot:P08311;urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448; urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "UNIPROT:P08311"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137; sa26; sa381"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 399.76656084142064
      y 522.8277645883575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:15767466"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 1066.4719410508878
      y 1039.594693648788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 516.5132973484489
      y 1073.7157562806474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D011654"
      hgnc "NA"
      map_id "pulmonary_space_edema"
      name "pulmonary_space_edema"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa208"
      uniprot "NA"
    ]
    graphics [
      x 438.2005045184536
      y 984.3743104651116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pulmonary_space_edema"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_31"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re146"
      uniprot "NA"
    ]
    graphics [
      x 990.3204875573854
      y 1483.3391606874322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014661"
      hgnc "NA"
      map_id "vasoconstriction"
      name "vasoconstriction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa56"
      uniprot "NA"
    ]
    graphics [
      x 813.2395863611429
      y 1632.2121868470317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vasoconstriction"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770;PUBMED:25124854"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re195"
      uniprot "NA"
    ]
    graphics [
      x 813.8511841087881
      y 1206.867707294912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003071"
      hgnc "NA"
      map_id "cognition"
      name "cognition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 976.9884850913631
      y 1326.357004624172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cognition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:2550696"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 1539.007856095149
      y 553.648314095261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43755"
      hgnc "NA"
      map_id "Lisinopril"
      name "Lisinopril"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1670.964442583378
      y 572.1890114687803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lisinopril"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:6555043"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 819.8601388526733
      y 586.7522279670529
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:6357;urn:miriam:ec-code:3.4.21.35;urn:miriam:refseq:NM_002257;urn:miriam:uniprot:P06870;urn:miriam:uniprot:P06870;urn:miriam:ncbigene:3816;urn:miriam:ncbigene:3816;urn:miriam:hgnc.symbol:KLK1;urn:miriam:hgnc.symbol:KLK1;urn:miriam:ensembl:ENSG00000167748"
      hgnc "HGNC_SYMBOL:KLK1"
      map_id "UNIPROT:P06870"
      name "KLK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P06870"
    ]
    graphics [
      x 694.2253757598551
      y 623.8862212341592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06870"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re193"
      uniprot "NA"
    ]
    graphics [
      x 1225.6408590975298
      y 726.2535763864305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "aldosterone"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195; sa509; sa503"
      uniprot "NA"
    ]
    graphics [
      x 1027.824566519057
      y 950.2264508119208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aldosterone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_57"
      name "PMID:22536270"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1092.8794173544609
      y 713.5190223117196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000137509;urn:miriam:hgnc.symbol:PRCP;urn:miriam:hgnc.symbol:PRCP;urn:miriam:ec-code:3.4.16.2;urn:miriam:uniprot:P42785;urn:miriam:uniprot:P42785;urn:miriam:hgnc:9344;urn:miriam:ncbigene:5547;urn:miriam:refseq:NM_005040;urn:miriam:ncbigene:5547"
      hgnc "HGNC_SYMBOL:PRCP"
      map_id "UNIPROT:P42785"
      name "PRCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa47"
      uniprot "UNIPROT:P42785"
    ]
    graphics [
      x 1134.5945670115238
      y 843.0142991304665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42785"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:27465904"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_12"
      name "angiotensin_space_1_minus_12"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa121"
      uniprot "NA"
    ]
    graphics [
      x 1049.5772123743159
      y 228.71455049659585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:22180785"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 914.7196555371784
      y 405.6971496149122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2097;urn:miriam:ncbigene:1215;urn:miriam:ncbigene:1215;urn:miriam:hgnc.symbol:CMA1;urn:miriam:hgnc.symbol:CMA1;urn:miriam:ensembl:ENSG00000092009;urn:miriam:refseq:NM_001836;urn:miriam:uniprot:P23946;urn:miriam:uniprot:P23946;urn:miriam:ec-code:3.4.21.39"
      hgnc "HGNC_SYMBOL:CMA1"
      map_id "UNIPROT:P23946"
      name "CMA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa122"
      uniprot "UNIPROT:P23946"
    ]
    graphics [
      x 759.3160261513576
      y 366.1941432641013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23946"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_85"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 901.3042209483625
      y 1433.7244982158843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006954"
      hgnc "NA"
      map_id "inflammatory_space_response"
      name "inflammatory_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa61"
      uniprot "NA"
    ]
    graphics [
      x 739.5496566492978
      y 1445.9171818369882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "inflammatory_space_response"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1406.5163981785975
      y 771.8439802521357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80129"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_5"
      name "angiotensin_space_1_minus_5"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1413.129820491636
      y 1024.724122103728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:27965422;PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re190"
      uniprot "NA"
    ]
    graphics [
      x 1157.1334639179745
      y 337.3420347052768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D006973"
      hgnc "NA"
      map_id "hypertension"
      name "hypertension"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 1150.3377413083795
      y 196.57852133125357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hypertension"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_35"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re150"
      uniprot "NA"
    ]
    graphics [
      x 498.1872062794423
      y 742.4623808771506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A135632"
      hgnc "NA"
      map_id "Camostat_space_mesilate"
      name "Camostat_space_mesilate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 407.64362935945564
      y 815.0163122271016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Camostat_space_mesilate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:32275855;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:ACE2;HGNC_SYMBOL:S"
      map_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
      name "ACE2_minus_Spike_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 606.031108530467
      y 760.4125577639892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:19834109"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re203"
      uniprot "NA"
    ]
    graphics [
      x 492.0701538492798
      y 1243.0490586102712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070527; urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_aggregation"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa209; sa409"
      uniprot "NA"
    ]
    graphics [
      x 362.78982706914064
      y 1298.826311849538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_aggregation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_46"
      name "PMID:10969042"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 914.1820122555167
      y 498.91612190866874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_9"
      name "angiotensin_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1019.4159360003916
      y 695.5182755112448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:20066004;PUBMED:32343152;PUBMED:23937567;PUBMED:24803075"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re188"
      uniprot "NA"
    ]
    graphics [
      x 1435.4579296655172
      y 361.2306575394622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D000017"
      hgnc "NA"
      map_id "ABO_space_blood_space_group_space_system"
      name "ABO_space_blood_space_group_space_system"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 1397.1698924289824
      y 246.2450629743206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ABO_space_blood_space_group_space_system"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re186"
      uniprot "NA"
    ]
    graphics [
      x 1491.4244577068207
      y 401.34088209850256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "UNIPROT:P00797"
      name "REN; Prorenin"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "sa36; sa35; sa71; sa415; sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 622.996526670996
      y 264.4882814506625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00797"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:12122115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_72"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 804.6398823017361
      y 255.85451992813591
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:27660028"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1291.8279564175173
      y 1248.4215757220545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ec-code:3.4.11.3;urn:miriam:refseq:NM_005575;urn:miriam:ensembl:ENSG00000113441;urn:miriam:hgnc:6656;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:ncbigene:4012;urn:miriam:uniprot:Q9UIQ6;urn:miriam:uniprot:Q9UIQ6;urn:miriam:ncbigene:4012"
      hgnc "HGNC_SYMBOL:LNPEP"
      map_id "UNIPROT:Q9UIQ6"
      name "LNPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa156; sa152"
      uniprot "UNIPROT:Q9UIQ6"
    ]
    graphics [
      x 1159.9770129020535
      y 1652.3955677971971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UIQ6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:9493859"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_30"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 991.4492758069487
      y 1733.8877932494377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:25014541"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 902.4511197695381
      y 1523.6402289766093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A147302"
      hgnc "NA"
      map_id "CGP42112A"
      name "CGP42112A"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 872.1159317816188
      y 1681.9640941491202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CGP42112A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 1328.1574878899737
      y 962.2143944899194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:30934934"
      hgnc "NA"
      map_id "angiotensin_space_3_minus_7"
      name "angiotensin_space_3_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 1331.1416301136853
      y 1167.6413339464837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_3_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:24227843;PUBMED:28512108;PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 747.2042468158772
      y 793.5072398433014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "UNIPROT:P78536"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43; path_1_sa174; path_1_sa171"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 648.4867290604153
      y 705.8845884593402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78536"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071;PUBMED:25666589"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_52"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re184"
      uniprot "NA"
    ]
    graphics [
      x 708.9945670510101
      y 1242.3412557300244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006979"
      hgnc "NA"
      map_id "oxidative_space_stress"
      name "oxidative_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 865.0597154893442
      y 1306.64590236001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "oxidative_space_stress"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000196549;urn:miriam:ncbigene:4311;urn:miriam:ncbigene:4311;urn:miriam:ec-code:3.4.24.11;urn:miriam:hgnc.symbol:MME;urn:miriam:hgnc.symbol:MME;urn:miriam:refseq:NM_000902;urn:miriam:uniprot:P08473;urn:miriam:uniprot:P08473;urn:miriam:hgnc:7154"
      hgnc "HGNC_SYMBOL:MME"
      map_id "UNIPROT:P08473"
      name "MME"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39; sa194"
      uniprot "UNIPROT:P08473"
    ]
    graphics [
      x 1206.4845520178048
      y 269.44561260417026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08473"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 1359.316335793167
      y 164.85994880988233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 1139.686149568604
      y 95.92911159513903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:22490446"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_4"
      name "angiotensin_space_1_minus_4"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 1253.1237205454372
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_10"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 784.188161205595
      y 956.3026455569136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:15809376;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P30556;UNIPROT:P04201"
      name "MAS1:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P30556;UNIPROT:P04201"
    ]
    graphics [
      x 754.7241314868704
      y 1126.3297463490953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556;UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 575.0969675586803
      y 439.6715575951534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:29928987"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 882.4634178254257
      y 951.1191226270075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_4"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 760.8000868567283
      y 622.5744512560485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_65"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re197"
      uniprot "NA"
    ]
    graphics [
      x 1247.9398679500887
      y 432.58478068146303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_96"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa113"
      uniprot "NA"
    ]
    graphics [
      x 1277.2801166851202
      y 1007.5223677479495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1205.4310080318592
      y 854.2330758965793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:C000657245; urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_infection"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa165; sa207; sa499; sa480; sa481"
      uniprot "NA"
    ]
    graphics [
      x 1320.354076140265
      y 785.7831708451979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_37"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 674.9948009112754
      y 913.2727542419844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014779"
      hgnc "NA"
      map_id "viral_space_replication_space_cycle"
      name "viral_space_replication_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 770.5090241797685
      y 1036.8307679482523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_replication_space_cycle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 1080.8504192500407
      y 355.15488041077003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:28512108"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 1087.4857428068321
      y 651.7595259346797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1452.0033564949638
      y 1131.1936243451562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:290;urn:miriam:ncbigene:290;urn:miriam:hgnc:500;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:refseq:NM_001150;urn:miriam:uniprot:P15144;urn:miriam:uniprot:P15144;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:ensembl:ENSG00000166825;urn:miriam:ec-code:3.4.11.2"
      hgnc "HGNC_SYMBOL:ANPEP"
      map_id "UNIPROT:P15144"
      name "ANPEP_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P15144"
    ]
    graphics [
      x 1576.0369277684388
      y 1165.4125530043893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80127"
      hgnc "NA"
      map_id "angiotensin_space_IV"
      name "angiotensin_space_IV"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa63"
      uniprot "NA"
    ]
    graphics [
      x 1217.3760569541846
      y 1263.5394026514384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_IV"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:10485450"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_7"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 132.7974915344496
      y 761.6718335210961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A50113"
      hgnc "NA"
      map_id "androgen"
      name "androgen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 864.7243280717964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "androgen"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 920.7945845395353
      y 1006.2158581362719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:30048754"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 990.0422648623577
      y 1001.1875381931968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_49"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 622.9451948788461
      y 1298.330670622242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_39"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 838.7513625674246
      y 1553.5675712140232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D005355"
      hgnc "NA"
      map_id "fibrosis"
      name "fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 655.2048116398379
      y 1525.7020941965975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fibrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:25225202"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 804.6809515128305
      y 837.2550395020901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:25225202;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P30556;UNIPROT:Q9BYF1"
      name "ACE2:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P30556;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 673.5987060840782
      y 828.5636210243359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re201"
      uniprot "NA"
    ]
    graphics [
      x 1304.7074076132105
      y 293.05837651272407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re123"
      uniprot "NA"
    ]
    graphics [
      x 1140.7149106505478
      y 421.6417356816046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:26010093;PUBMED:26171856"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 1247.5084734660713
      y 363.06499487801034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1072.3879354552978
      y 458.7717841094221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:24463937"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re165"
      uniprot "NA"
    ]
    graphics [
      x 789.5829169142938
      y 1409.0944399666396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:29287092"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re127"
      uniprot "NA"
    ]
    graphics [
      x 1252.518434453733
      y 1500.302306701593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:146025955"
      hgnc "NA"
      map_id "AR234960"
      name "AR234960"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 1380.5157520895677
      y 1479.8416690947597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AR234960"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:10234025;PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_64"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re196"
      uniprot "NA"
    ]
    graphics [
      x 1102.4732084019531
      y 1508.0663044459466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re151"
      uniprot "NA"
    ]
    graphics [
      x 1292.4393163875668
      y 1427.9599999909403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:12754187"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_50"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 571.6240545069568
      y 1358.4040650999034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_126"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 1313.2454086013686
      y 727.5023009885883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32408336"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re168"
      uniprot "NA"
    ]
    graphics [
      x 1215.559198185116
      y 642.9182110524647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:22710644"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re128"
      uniprot "NA"
    ]
    graphics [
      x 1641.5957790654143
      y 270.5347699878589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:24337978"
      hgnc "NA"
      map_id "QGC001"
      name "QGC001"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 1554.6070082683946
      y 148.95646528447526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "QGC001"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 797.9881358658911
      y 1480.8634376588939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_82"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 515.4286011759137
      y 214.51369400394003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:24041943"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 997.2102319790221
      y 604.5951605505595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:7064;urn:miriam:ncbigene:7064;urn:miriam:hgnc.symbol:THOP1;urn:miriam:ensembl:ENSG00000172009;urn:miriam:hgnc.symbol:THOP1;urn:miriam:uniprot:P52888;urn:miriam:uniprot:P52888;urn:miriam:hgnc:11793;urn:miriam:ec-code:3.4.24.15;urn:miriam:refseq:NM_003249"
      hgnc "HGNC_SYMBOL:THOP1"
      map_id "UNIPROT:P52888"
      name "THOP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:P52888"
    ]
    graphics [
      x 1126.266912236039
      y 563.4065552012551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52888"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_40"
      name "PMID:190881"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1051.9099823792285
      y 514.2935244537573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:2266130"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re131"
      uniprot "NA"
    ]
    graphics [
      x 761.3712386061969
      y 515.9823492734688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_51"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 664.9239512574404
      y 1414.8465646071047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_140"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 603.7369353920456
      y 923.8170897048271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:1338730"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re198"
      uniprot "NA"
    ]
    graphics [
      x 725.0305990149908
      y 999.1463985253812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:24530803"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 467.4305100151116
      y 1147.2973689456733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A6541"
      hgnc "NA"
      map_id "Losartan"
      name "Losartan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 324.4302009684844
      y 1142.2224953029672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Losartan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 1210.9411653739173
      y 547.6505841343521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:12829792"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_28"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 999.830377235545
      y 1620.4276436844511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_9"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 809.3439901821138
      y 1273.959151403314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "PUBMED:19034303;PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_67"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re200"
      uniprot "NA"
    ]
    graphics [
      x 983.1006072508211
      y 277.21231849616356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003924"
      hgnc "NA"
      map_id "_space_Diabetes_space_mellitus,_space_type_space_II"
      name "_space_Diabetes_space_mellitus,_space_type_space_II"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 924.379395315455
      y 173.5897686751448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_space_Diabetes_space_mellitus,_space_type_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_84"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 941.2356952222932
      y 1386.912758611991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 853.1113438633445
      y 778.7559775386912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:1310484"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 926.7736888177114
      y 611.7730428664041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:9358;urn:miriam:hgnc.symbol:PREP;urn:miriam:hgnc.symbol:PREP;urn:miriam:ncbigene:5550;urn:miriam:ncbigene:5550;urn:miriam:refseq:NM_002726;urn:miriam:ensembl:ENSG00000085377;urn:miriam:uniprot:P48147;urn:miriam:uniprot:P48147;urn:miriam:ec-code:3.4.21.26"
      hgnc "HGNC_SYMBOL:PREP"
      map_id "UNIPROT:P48147"
      name "PREP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "UNIPROT:P48147"
    ]
    graphics [
      x 847.5738073733492
      y 518.5630313261386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:26497614;PUBMED:17138938;PUBMED:32333398;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 914.2683774848739
      y 1069.7438796533106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:11707427"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re122"
      uniprot "NA"
    ]
    graphics [
      x 1192.9342175158567
      y 1488.0343869334015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_53"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re185"
      uniprot "NA"
    ]
    graphics [
      x 1232.4003534374278
      y 1800.0977195713813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D009410"
      hgnc "NA"
      map_id "neurodegeneration"
      name "neurodegeneration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa190"
      uniprot "NA"
    ]
    graphics [
      x 1284.6278929211994
      y 1911.9158866575408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "neurodegeneration"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:27038740"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 931.1786781267899
      y 1610.51238062533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re149"
      uniprot "NA"
    ]
    graphics [
      x 1347.7940900326817
      y 1318.2921792927827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 1031.1794210136825
      y 1338.5479713345683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:8351287"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re167"
      uniprot "NA"
    ]
    graphics [
      x 355.9538696536631
      y 978.4807644331012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A4903"
      hgnc "NA"
      map_id "ethynylestradiol"
      name "ethynylestradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa134"
      uniprot "NA"
    ]
    graphics [
      x 385.82795859658927
      y 1192.5134445893536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ethynylestradiol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re148"
      uniprot "NA"
    ]
    graphics [
      x 1141.495782455366
      y 961.9431346986979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:23884911"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_47"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re177"
      uniprot "NA"
    ]
    graphics [
      x 711.3103309444334
      y 1328.1014840021044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 1132.5733914159591
      y 1116.650038762954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_132"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 973.4044932787514
      y 1165.1466498522896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re194"
      uniprot "NA"
    ]
    graphics [
      x 1072.229453997348
      y 1198.4324246848264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 160
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "alamandine"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_88"
      target_id "UNIPROT:Q8TDS7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 12
    target 10
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_76"
      target_id "alamandine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 1
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "estradiol"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 16
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Calcitriol"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 13
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_38"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 8
    target 17
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 11
    target 17
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 17
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_27"
      target_id "UNIPROT:Q8TDS7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07075"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "sex,_space_male"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_59"
      target_id "UNIPROT:Q07075"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 5
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 18
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q07075"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_79"
      target_id "angiotensin_space_III"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_188"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 15
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 24
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_91"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_42"
      target_id "thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_A"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 1
    target 29
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 29
    target 10
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_78"
      target_id "alamandine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 1
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aging"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 30
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_56"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07339"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08311"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 33
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_17"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 3
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 11
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 36
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_24"
      target_id "UNIPROT:P50052"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 4
    target 37
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_69"
      target_id "pulmonary_space_edema"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 8
    target 39
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_31"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 4
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_63"
      target_id "cognition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 7
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Lisinopril"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 43
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_11"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 6
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P06870"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 45
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_19"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 7
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aldosterone"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 47
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_61"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 5
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 1
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P42785"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 49
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_57"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23946"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 52
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_6"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 25
    target 54
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_85"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 11
    target 56
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 7
    target 56
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_71"
      target_id "angiotensin_space_1_minus_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 1
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 31
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aging"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "hypertension"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 58
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_58"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 15
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Camostat_space_mesilate"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 2
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O15393"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 60
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_35"
      target_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 4
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_70"
      target_id "platelet_space_aggregation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 6
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 1
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_46"
      target_id "angiotensin_space_1_minus_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 7
    target 67
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "ABO_space_blood_space_group_space_system"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 67
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_55"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 7
    target 69
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 31
    target 69
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "aging"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 20
    target 69
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "sex,_space_male"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 69
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_54"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 16
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Calcitriol"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_72"
      target_id "UNIPROT:P00797"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 25
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 57
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_5"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 72
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_75"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 74
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_30"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 3
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "CGP42112A"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 75
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_81"
      target_id "UNIPROT:P50052"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 11
    target 77
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_32"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 1
    target 79
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P78536"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 4
    target 79
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P30556"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 79
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_83"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 4
    target 81
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_52"
      target_id "oxidative_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 83
    target 84
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08473"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 20
    target 84
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "sex,_space_male"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_60"
      target_id "UNIPROT:P08473"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 51
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_8"
      target_id "angiotensin_space_1_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 4
    target 87
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P30556;UNIPROT:P04201"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 5
    target 87
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 87
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_10"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 32
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 70
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00797"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 89
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_73"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 4
    target 90
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 11
    target 90
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 90
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_23"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 1
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 15
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 91
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_4"
      target_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 7
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 14
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "estradiol"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 16
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Calcitriol"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 92
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_65"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_96"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 94
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_5"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 62
    target 96
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_37"
      target_id "viral_space_replication_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 51
    target 98
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 7
    target 98
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 98
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_12"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 5
    target 99
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 1
    target 99
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 99
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_89"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 22
    target 100
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_III"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15144"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 100
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_80"
      target_id "angiotensin_space_IV"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 2
    target 103
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15393"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "androgen"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 103
    target 2
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_7"
      target_id "UNIPROT:O15393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 3
    target 105
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 5
    target 105
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 28
    target 105
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_A"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 105
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_87"
      target_id "UNIPROT:P50052"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 3
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 66
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_9"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 106
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_25"
      target_id "UNIPROT:P50052"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 4
    target 107
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 107
    target 55
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_49"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 25
    target 108
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_39"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 4
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 1
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 5
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_II"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_93"
      target_id "UNIPROT:P30556;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 7
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 59
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "hypertension"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 112
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_68"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 51
    target 113
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 83
    target 113
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 113
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_14"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 1
    target 114
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 20
    target 114
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "sex,_space_male"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 114
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_90"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 6
    target 115
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 83
    target 115
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 115
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_74"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 3
    target 116
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 116
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_43"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 25
    target 117
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 118
    target 117
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "AR234960"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 117
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_15"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 73
    target 119
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 119
    target 42
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_64"
      target_id "cognition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 73
    target 120
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 78
    target 120
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_3_minus_7"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 120
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_36"
      target_id "UNIPROT:Q9UIQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 4
    target 121
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 121
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_50"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_126"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 95
    target 123
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 123
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_45"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 18
    target 124
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07075"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "QGC001"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 124
    target 18
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_16"
      target_id "UNIPROT:Q07075"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 3
    target 126
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 126
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_86"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 70
    target 127
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M12_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 127
    target 70
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_82"
      target_id "UNIPROT:P00797"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 6
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 129
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P52888"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 128
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_20"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 6
    target 130
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 7
    target 130
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 130
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_40"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 6
    target 131
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 53
    target 131
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23946"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 131
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_18"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 4
    target 132
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 132
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_51"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_140"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 4
    target 134
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P30556"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 134
    target 48
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_66"
      target_id "aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 4
    target 135
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Losartan"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 135
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_22"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 66
    target 137
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_9"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 7
    target 137
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 83
    target 137
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 137
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_48"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 25
    target 138
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 138
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_28"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 4
    target 139
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 25
    target 139
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 139
    target 88
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_9"
      target_id "UNIPROT:P30556;UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 1
    target 140
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 59
    target 140
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "hypertension"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 141
    target 140
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "_space_Diabetes_space_mellitus,_space_type_space_II"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 140
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_67"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 8
    target 142
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 142
    target 55
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_84"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 5
    target 143
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 143
    target 28
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_77"
      target_id "angiotensin_space_A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 6
    target 144
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 145
    target 144
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48147"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 144
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_21"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 4
    target 146
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 5
    target 146
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 102
    target 146
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_IV"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 28
    target 146
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_A"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 146
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_92"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 73
    target 147
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 102
    target 147
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_IV"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 147
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_13"
      target_id "UNIPROT:Q9UIQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 73
    target 148
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_53"
      target_id "neurodegeneration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 73
    target 150
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 150
    target 55
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_29"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 102
    target 151
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_IV"
      target_id "M12_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 151
    target 78
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_34"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 25
    target 152
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 152
    target 82
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_41"
      target_id "oxidative_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 32
    target 153
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "ethynylestradiol"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 153
    target 32
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_44"
      target_id "UNIPROT:P01019"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 5
    target 155
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 155
    target 78
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_33"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 4
    target 156
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 156
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_47"
      target_id "thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 25
    target 157
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 11
    target 157
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 157
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_26"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_132"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 48
    target 159
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "aldosterone"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 159
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_62"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
