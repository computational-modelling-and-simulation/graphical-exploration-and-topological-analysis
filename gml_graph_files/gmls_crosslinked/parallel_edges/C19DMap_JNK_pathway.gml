# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; C19DMap:JNK pathway; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9682868; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683594;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:reactome:R-COV-9683676;urn:miriam:pubmed:15367599;urn:miriam:uniprot:P59594; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683608;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683638;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9683768; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683716;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S; urn:miriam:ncbigene:1489668;urn:miriam:pubmed:32275855;urn:miriam:pubmed:32075877;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32225176;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P59594"
      name "nascent_space_Spike; N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike; complex_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_unfolded_space_N_minus_glycan_space_Spike; S"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_227; layout_273; layout_300; layout_302; layout_287; layout_293; layout_317; layout_279; sa78; sa76"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 435.4920728022003
      y 265.26295260208127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 674.4872923006553
      y 521.5529652722564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:reactome:R-COV-9686193; urn:miriam:uniprot:P59635; urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "UNIPROT:P59635"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_614; c0237; sa84; sa76"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 710.1948570895161
      y 602.9847163048647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 849.3941595277731
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:JNK pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P10415; urn:miriam:hgnc.symbol:BCL2;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "NA; HGNC_SYMBOL:BCL2"
      map_id "UNIPROT:P10415"
      name "BCL2"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "c7da2; sa53; sa12; path_0_sa55; path_0_sa54; path_0_sa152"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 636.6602833041252
      y 184.62415776019293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10415"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4877; WP4880; C19DMap:JNK pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416; urn:miriam:uniprot:P59633; urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "UNIPROT:P59633"
      name "ee4e9; 3b; Orf3b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "ee4e9; e6060; sa75; sa356; sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 176.52700401285313
      y 508.2962260568431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4877; WP5038; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:wikipathways:WP4936; urn:miriam:obo.go:GO%3A0006914"
      hgnc "NA"
      map_id "Autophagy"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3; d0d85; sa18"
      uniprot "NA"
    ]
    graphics [
      x 848.1788164320319
      y 262.6554389201649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:obo.go:GO%3A0045087"
      hgnc "NA"
      map_id "Innate_space_Immunity"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c1ec8; sa16"
      uniprot "NA"
    ]
    graphics [
      x 259.35451320911227
      y 932.9790316405534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Innate_space_Immunity"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_001880;urn:miriam:uniprot:P15336;urn:miriam:uniprot:P15336;urn:miriam:ncbigene:1386;urn:miriam:hgnc:784;urn:miriam:hgnc.symbol:ATF2;urn:miriam:hgnc.symbol:ATF2;urn:miriam:ensembl:ENSG00000115966"
      hgnc "HGNC_SYMBOL:ATF2"
      map_id "UNIPROT:P15336"
      name "ATF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa71; sa33"
      uniprot "UNIPROT:P15336"
    ]
    graphics [
      x 249.00291832549544
      y 316.5769829110708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:7824938"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 333.9360472399077
      y 400.77365750647914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000109339;urn:miriam:hgnc:6872;urn:miriam:uniprot:P53779;urn:miriam:uniprot:P53779;urn:miriam:refseq:NM_001318067;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:ncbigene:5602;urn:miriam:ncbigene:5602;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK10;HGNC_SYMBOL:MAPK9"
      map_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
    ]
    graphics [
      x 441.09894684531633
      y 495.4561512893433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:JNK pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7; urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "UNIPROT:O14733"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44; sa5; sa372; sa453"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 727.0944994734497
      y 712.835994143216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14733"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 834.3569380022245
      y 814.2342120275999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_001284230;urn:miriam:hgnc:6861;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:ensembl:ENSG00000006432;urn:miriam:ncbigene:4293;urn:miriam:uniprot:P80192;urn:miriam:uniprot:P80192;urn:miriam:ncbigene:4293;urn:miriam:ec-code:2.7.11.25;urn:miriam:ncbigene:4294;urn:miriam:ncbigene:4294;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc:6849;urn:miriam:uniprot:Q02779;urn:miriam:uniprot:Q02779;urn:miriam:refseq:NM_002446;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ec-code:2.7.11.25;urn:miriam:hgnc:6850;urn:miriam:ncbigene:4296;urn:miriam:ensembl:ENSG00000173327;urn:miriam:ncbigene:4296;urn:miriam:uniprot:Q16584;urn:miriam:uniprot:Q16584;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:refseq:NM_002419;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K9;HGNC_SYMBOL:MAP3K10;HGNC_SYMBOL:MAP3K11"
      map_id "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
      name "MLK1_slash_2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
    ]
    graphics [
      x 962.3415899569899
      y 833.1401193950442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674;urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
      name "SARS_minus_CoV_minus_1_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
    ]
    graphics [
      x 784.2842182416109
      y 948.7525939100149
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:10567572"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 553.7489859129147
      y 329.5243749519227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000141510;urn:miriam:hgnc:11998;urn:miriam:ncbigene:7157;urn:miriam:hgnc.symbol:TP53;urn:miriam:hgnc.symbol:TP53;urn:miriam:uniprot:P04637;urn:miriam:uniprot:P04637;urn:miriam:refseq:NM_000546"
      hgnc "HGNC_SYMBOL:TP53"
      map_id "UNIPROT:P04637"
      name "TP53"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70; sa34"
      uniprot "UNIPROT:P04637"
    ]
    graphics [
      x 294.65394794057966
      y 726.5178234703712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:9724739"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 384.83453005289647
      y 625.2555687750523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 721.4048162142728
      y 69.84011609034548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:6206;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:JUND;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P17535;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:JUND;HGNC_SYMBOL:JUN"
      map_id "UNIPROT:P17535;UNIPROT:P05412"
      name "AP_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4; csa9"
      uniprot "UNIPROT:P17535;UNIPROT:P05412"
    ]
    graphics [
      x 219.81036157804817
      y 668.65919661494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17535;UNIPROT:P05412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_12"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 240.22339874520338
      y 816.4346146952558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:9724739"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_16"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 154.8068597657092
      y 767.6975471741665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0072331"
      hgnc "NA"
      map_id "TP53_space_signalling"
      name "TP53_space_signalling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 837.4950009108518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TP53_space_signalling"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:JNK pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416; urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "UNIPROT:P45985"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69; sa4; sa371; sa452"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 604.8421886615232
      y 765.4000244829801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45985"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 657.7331675864559
      y 897.5024056486463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_005921;urn:miriam:ensembl:ENSG00000095015;urn:miriam:uniprot:Q13233;urn:miriam:uniprot:Q13233;urn:miriam:hgnc:6848;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:ncbigene:4214;urn:miriam:ncbigene:4214;urn:miriam:ec-code:2.7.11.25;urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K1;HGNC_SYMBOL:MAP3K4"
      map_id "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
      name "MEKK1_slash_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3; csa11"
      uniprot "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 593.3842318031766
      y 998.242946051145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 704.5089040030618
      y 1053.6060909741996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_10"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 758.6640501009103
      y 193.33927032011405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:21561061"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 287.9863146125284
      y 545.3392837650154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044;urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000109339;urn:miriam:hgnc:6872;urn:miriam:uniprot:P53779;urn:miriam:uniprot:P53779;urn:miriam:refseq:NM_001318067;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:ncbigene:5602;urn:miriam:ncbigene:5602"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK9;HGNC_SYMBOL:MAPK10"
      map_id "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
    ]
    graphics [
      x 557.9882088104013
      y 482.4048572536452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:17267381"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 469.89230974589856
      y 381.7996708323019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:hgnc:6850;urn:miriam:ncbigene:4296;urn:miriam:ensembl:ENSG00000173327;urn:miriam:ncbigene:4296;urn:miriam:uniprot:Q16584;urn:miriam:uniprot:Q16584;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:refseq:NM_002419;urn:miriam:ec-code:2.7.11.25;urn:miriam:ncbigene:4294;urn:miriam:ncbigene:4294;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc:6849;urn:miriam:uniprot:Q02779;urn:miriam:uniprot:Q02779;urn:miriam:refseq:NM_002446;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ec-code:2.7.11.25;urn:miriam:refseq:NM_001284230;urn:miriam:hgnc:6861;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:ensembl:ENSG00000006432;urn:miriam:ncbigene:4293;urn:miriam:uniprot:P80192;urn:miriam:uniprot:P80192;urn:miriam:ncbigene:4293;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K11;HGNC_SYMBOL:MAP3K10;HGNC_SYMBOL:MAP3K9"
      map_id "UNIPROT:Q16584;UNIPROT:Q02779;UNIPROT:P80192"
      name "MLK1_slash_2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q16584;UNIPROT:Q02779;UNIPROT:P80192"
    ]
    graphics [
      x 924.5679378539573
      y 1046.4003159413182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16584;UNIPROT:Q02779;UNIPROT:P80192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 909.5017087266491
      y 936.9476820898351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:17141229"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 590.1429993498335
      y 615.2990398268425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 35
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P15336"
      target_id "M110_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
      target_id "M110_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_14"
      target_id "UNIPROT:P15336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14733"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_11"
      target_id "UNIPROT:O14733"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 5
    target 16
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10415"
      target_id "M110_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 11
    target 16
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
      target_id "M110_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 16
    target 5
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_17"
      target_id "UNIPROT:P10415"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04637"
      target_id "M110_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 11
    target 18
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
      target_id "M110_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_15"
      target_id "UNIPROT:P04637"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 5
    target 19
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10415"
      target_id "M110_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 19
    target 4
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_23"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17535;UNIPROT:P05412"
      target_id "M110_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 21
    target 8
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_12"
      target_id "Innate_space_Immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 17
    target 22
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04637"
      target_id "M110_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_16"
      target_id "TP53_space_signalling"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45985"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 15
    target 25
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_20"
      target_id "UNIPROT:P45985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
      target_id "M110_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 15
    target 27
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
      target_id "M110_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_19"
      target_id "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 5
    target 28
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10415"
      target_id "M110_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 28
    target 7
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_10"
      target_id "Autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 20
    target 29
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17535;UNIPROT:P05412"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 11
    target 29
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 6
    target 29
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59633"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 29
    target 20
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_18"
      target_id "UNIPROT:P17535;UNIPROT:P05412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
      target_id "M110_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 1
    target 31
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59594"
      target_id "M110_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 31
    target 11
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_22"
      target_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16584;UNIPROT:Q02779;UNIPROT:P80192"
      target_id "M110_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 15
    target 33
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
      target_id "M110_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 33
    target 14
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_13"
      target_id "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 30
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 24
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45985"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 12
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O14733"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 3
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 2
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 34
    target 11
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_21"
      target_id "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
