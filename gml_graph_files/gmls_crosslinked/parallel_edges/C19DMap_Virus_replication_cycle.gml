# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1830.5299140980503
      y 1581.4735176768927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1819.947199565905
      y 1318.8287014266366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717; urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767; urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "NA; HGNC_SYMBOL:CTSL"
      map_id "UNIPROT:P07711"
      name "Cathepsin_space_L1; CTSL:CTSL_space_inhibitors; CTSL"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_837; layout_1038; layout_2492; layout_2493; sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 1205.9509910761758
      y 1506.3990774095905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07711"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694300;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683625; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729324; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9684230;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729277;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9729264;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9684199;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694612; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729335;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694659; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729308;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC9; urn:miriam:uniprot:P0DTC9; urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9; urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P0DTC9"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; N_space_tetramer; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand); N; p_minus_S188,206_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S,2T_minus_N; p_minus_11S,2T_minus_N; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S_minus_N; ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; p_minus_11S,2T_minus_metR95,177_minus_N; nucleocapsid_br_protein; Nucleocapsid; Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_3639; layout_2352; layout_2496; layout_3571; layout_2364; layout_2341; layout_3558; layout_2343; layout_2465; layout_3542; layout_3528; layout_2353; layout_2467; layout_3574; layout_2361; layout_2362; layout_3616; layout_3529; layout_2960; ff3a1; b0f05; b6d08; b9df2; sa1685; sa1887; csa365; csa369; csa374; csa398; sa1667; csa353; csa357; csa391; csa389; csa387; csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1297.0969872497626
      y 988.6816514370888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP5038; WP5039; C19DMap:Virus replication cycle; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5; urn:miriam:uniprot:P0DTC5; urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "UNIPROT:P0DTC5"
      name "nascent_space_M; M; N_minus_glycan_space_M; M_space_lattice; membrane_br_glycoprotein; Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2338; layout_2416; layout_2419; layout_2463; layout_2469; d614c; d1b58; cedc7; sa2061; sa2116; sa2024; sa2066; sa1686; sa1891; sa1857; sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1360.555461922238
      y 1111.1109521155433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 17
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3"
      name "O_minus_glycosyl_space_3a_space_tetramer; O_minus_glycosyl_space_3a; 3a; 3a:membranous_space_structure; GalNAc_minus_O_minus_3a; ORF3a; Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2458; layout_2461; layout_2399; layout_2403; layout_2332; layout_2392; layout_2459; layout_2455; layout_2395; cb0cc; ac4ba; ed8aa; sa1873; sa2247; sa1; sa169; sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1746.2801743356042
      y 1297.8952636972174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1713.824660135293
      y 1567.4097060197014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 15
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681986; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681994; urn:miriam:reactome:R-COV-9684311;urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694778; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681991; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681992; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681990; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681995; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681983; urn:miriam:uniprot:P0DTC1;urn:miriam:pubmed:32169673; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1"
      name "pp1a_minus_nsp6_minus_11; pp1a; pp1a_space_dimer; pp1a_minus_3CL; pp1a_minus_nsp1_minus_4; pp1a_minus_nsp9; pp1a_minus_nsp8; pp1a_minus_nsp11; pp1a_minus_nsp7; pp1a_minus_nsp6; pp1a_minus_nsp10; orf1a; pp1a_space_Nsp6_minus_11; pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2172; layout_2719; layout_2168; layout_2547; layout_2208; layout_2546; layout_2542; layout_2543; layout_2544; layout_2184; layout_2545; eb8ab; sa2224; sa1789; sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 810.6860848345502
      y 788.4567900031242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9694516; WP5038; WP5039; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2; urn:miriam:uniprot:P0DTD2; urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "UNIPROT:P0DTD2"
      name "9b_space_homodimer; ORF9b; PLpro; Orf9b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_3481; e62df; bca7b; f4882; sa139; sa1878; sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1020.8597137102015
      y 1043.9386411435316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-CFA-2159856;urn:miriam:reactome:R-DRE-2159856;urn:miriam:reactome:R-HSA-2159856;urn:miriam:reactome:R-XTR-2159856;urn:miriam:reactome:R-GGA-2159856;urn:miriam:pubmed:1629222;urn:miriam:uniprot:P09958;urn:miriam:reactome:R-MMU-2159856;urn:miriam:reactome:R-SSC-2159856;urn:miriam:reactome:R-RNO-2159856; urn:miriam:uniprot:P09958;urn:miriam:pubmed:32376634; urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "NA; HGNC_SYMBOL:FURIN"
      map_id "UNIPROT:P09958"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3309; ee829; sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 1786.3728407825406
      y 1233.711267652026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09958"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP4861; WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC4; urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P0DTC4"
      name "nascent_space_E; N_minus_glycan_space_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; 3xPalmC_minus_E; envelope_br_protein; SARS_space_E; Envelope_space_Protein_space_E; E"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_2335; layout_2405; layout_2410; layout_2412; layout_2407; layout_2414; aa466; fce54; c7c21; sa2062; sa2115; sa2023; sa2065; sa1687; sa1892; sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1657.2284616072654
      y 1019.9613322591398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 664.8136182563276
      y 803.9608108479431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9694750;urn:miriam:uniprot:P0DTC7; urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "UNIPROT:P0DTC7"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2477; sa1875; sa2060; sa2114; sa2025; sa2067; sa2245; sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1568.322650738889
      y 938.6171081049949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
      name "recognition_space_complex; ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f1172; csa368; csa431; csa430"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1605.723367309652
      y 1555.0453297643373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "cholesterol"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a1c2c; b41ce; sa2372"
      uniprot "NA"
    ]
    graphics [
      x 1537.8344573440484
      y 1271.753065633493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cholesterol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O14786;urn:miriam:pubmed:33082293; urn:miriam:interpro:IPR002551;urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1; urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "NA; HGNC_SYMBOL:NRP1"
      map_id "UNIPROT:O14786"
      name "NRP1; S1:NRP1_space_complex"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "ea33c; csa441; sa2371"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 1504.2079599723338
      y 1393.5488793308223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:wikidata:Q106020256; urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "S1"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a19f1; bcd14; sa1539; sa1516"
      uniprot "NA"
    ]
    graphics [
      x 1539.0069345140212
      y 1512.682882999457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC6; urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "UNIPROT:P0DTC6"
      name "orf6; Orf6"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e9623; sa1874; sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1786.907106530553
      y 854.5909856129656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:wikidata:Q106020384; urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "S2"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b27b6; sa2188; sa2052; sa1601; sa2063"
      uniprot "NA"
    ]
    graphics [
      x 1583.9953353031824
      y 1350.2333753645296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742615; urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "Nsp8"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa494; sa2202; sa2362"
      uniprot "NA"
    ]
    graphics [
      x 631.3547028677582
      y 508.50364770172064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1; urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "Nsp6"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713; sa48; sa307; sa304; sa2204; sa2367"
      uniprot "NA"
    ]
    graphics [
      x 832.8444813677037
      y 542.2727224733467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611; urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "Nsp4"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa695; sa4; sa217; sa2228"
      uniprot "NA"
    ]
    graphics [
      x 970.7481326511142
      y 640.15791162113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "Nsp7"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa499; sa2203; sa2363"
      uniprot "NA"
    ]
    graphics [
      x 766.0669374008701
      y 582.6474579437946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742612; urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "Nsp5"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa693; sa251; sa2241; sa2366"
      uniprot "NA"
    ]
    graphics [
      x 724.6832555101612
      y 543.4669553921706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3; urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "UNIPROT:Nsp3"
      name "Nsp3; Nsp3:Nsp4:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa162; csa39; sa123; sa169; sa2222; sa357; sa361; sa349; sa354; sa363"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1000.8685785503415
      y 698.5279347709902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:1802476818"
      hgnc "NA"
      map_id "Nsp15"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa129; sa227; sa316; sa2205; sa415; sa416"
      uniprot "NA"
    ]
    graphics [
      x 666.7010911260396
      y 864.8964287929351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725297; urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "Nsp1"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132; sa2216; sa2356"
      uniprot "NA"
    ]
    graphics [
      x 309.85868513427454
      y 803.1260661925874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "Nsp10"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa317; sa2200; sa2360"
      uniprot "NA"
    ]
    graphics [
      x 563.9617169378887
      y 531.3458256377801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "Nsp14"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318; sa2206; sa35; sa120; sa410"
      uniprot "NA"
    ]
    graphics [
      x 541.4861572569484
      y 674.0298367263936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "Nsp16"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa319; sa2199"
      uniprot "NA"
    ]
    graphics [
      x 587.7523646028128
      y 767.9489422824206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re981"
      uniprot "NA"
    ]
    graphics [
      x 1679.423333703373
      y 1452.6397948232632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7a_space_(_plus_)ss_space_sgmRNA"
      name "Orf7a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2105"
      uniprot "NA"
    ]
    graphics [
      x 1096.372851380534
      y 572.4597858217878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7a_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1025"
      uniprot "NA"
    ]
    graphics [
      x 1340.99844866002
      y 746.9993259470264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "Host_space_translation_space_complex"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa428; csa429; csa427"
      uniprot "NA"
    ]
    graphics [
      x 1368.0058446973405
      y 928.0776339291843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Host_space_translation_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re932"
      uniprot "NA"
    ]
    graphics [
      x 1391.320573081389
      y 1048.9671757820008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7b_space_(_plus_)ss_space_sgmRNA"
      name "Orf7b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2106"
      uniprot "NA"
    ]
    graphics [
      x 1165.5930254332904
      y 1359.306079723117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7b_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1031"
      uniprot "NA"
    ]
    graphics [
      x 1427.6985564896293
      y 1236.4552878527982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "UNIPROT:P0DTD8"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1876; sa2251"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 1665.1752042365663
      y 1280.3636306562194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re979"
      uniprot "NA"
    ]
    graphics [
      x 1509.0234618153868
      y 1088.5685713902012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209;PUBMED:30632963"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1112"
      uniprot "NA"
    ]
    graphics [
      x 699.0266528293178
      y 730.6154898453325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725308"
      hgnc "NA"
      map_id "Nsp13"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2198"
      uniprot "NA"
    ]
    graphics [
      x 593.9389359158026
      y 708.4322822947167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725307"
      hgnc "NA"
      map_id "Nsp12"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2197"
      uniprot "NA"
    ]
    graphics [
      x 518.8364498837957
      y 728.674242532851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "Nsp9"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2201; sa2361"
      uniprot "NA"
    ]
    graphics [
      x 665.9188185537713
      y 556.6707726092335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "Nsp2"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2217; sa2357"
      uniprot "NA"
    ]
    graphics [
      x 574.4196923473244
      y 923.650536796841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Replication_space_transcription_space_complex"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa410; csa440"
      uniprot "NA"
    ]
    graphics [
      x 695.3161188609339
      y 937.2994570464236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Replication_space_transcription_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651;PUBMED:32944968;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_109"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re855"
      uniprot "NA"
    ]
    graphics [
      x 1543.692840394173
      y 1151.065526123514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf8_space_ds_space_sgmRNA"
      name "Orf8_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2349"
      uniprot "NA"
    ]
    graphics [
      x 916.343166366883
      y 328.8327332765668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf8_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1095"
      uniprot "NA"
    ]
    graphics [
      x 1063.4289023388174
      y 286.03061743276953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf8_space_(_minus_)ss_space_sgmRNA"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2299; sa2321"
      uniprot "NA"
    ]
    graphics [
      x 887.4071994268485
      y 415.7187002297349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf8_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf8_space_(_plus_)ss_space_sgmRNA"
      name "Orf8_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2107"
      uniprot "NA"
    ]
    graphics [
      x 1325.2569649299944
      y 399.52291777332704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf8_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "Nsp3_minus_4"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2243; sa2242; sa2359; sa2365"
      uniprot "NA"
    ]
    graphics [
      x 1017.0354510709911
      y 593.8602380730775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp3_minus_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1108"
      uniprot "NA"
    ]
    graphics [
      x 1142.2563997430505
      y 640.9916441592067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf9b_space_(_minus_)ss_space_sgmRNA"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2320; sa2300"
      uniprot "NA"
    ]
    graphics [
      x 431.20055919156573
      y 653.6868418967849
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9b_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1084"
      uniprot "NA"
    ]
    graphics [
      x 426.81864186831535
      y 777.712255980423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf9b_space_ds_space_sgmRNA"
      name "Orf9b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2350"
      uniprot "NA"
    ]
    graphics [
      x 350.6634884486806
      y 613.0557834846616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9b_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1120"
      uniprot "NA"
    ]
    graphics [
      x 995.538310993239
      y 947.3256482640792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1061"
      uniprot "NA"
    ]
    graphics [
      x 1430.7796621103107
      y 902.9732952194568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082294;PUBMED:33082293"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1124"
      uniprot "NA"
    ]
    graphics [
      x 1456.71124058527
      y 1569.0710740829995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "E_space_(_minus_)ss_space_sgmRNA"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2326; sa2294"
      uniprot "NA"
    ]
    graphics [
      x 880.0781367751445
      y 1152.4139219704139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1078"
      uniprot "NA"
    ]
    graphics [
      x 770.5865493100878
      y 1205.1548503958315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "E_space_ds_space_sgmRNA"
      name "E_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2344"
      uniprot "NA"
    ]
    graphics [
      x 933.7777101782366
      y 1325.2024537105356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1018"
      uniprot "NA"
    ]
    graphics [
      x 987.6931210546378
      y 1199.1227656200574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf3a_space_(_minus_)ss_space_sgmRNA"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2322; sa2298"
      uniprot "NA"
    ]
    graphics [
      x 962.6540878712549
      y 1134.2223374143705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf3a_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1082"
      uniprot "NA"
    ]
    graphics [
      x 820.3550736492865
      y 1155.7130580935063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf3a_space_ds_space_sgmRNA"
      name "Orf3a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2348"
      uniprot "NA"
    ]
    graphics [
      x 1014.0291296699945
      y 1267.651851477339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf3a_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M_space_(_minus_)ss_space_sgmRNA"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2327; sa2292"
      uniprot "NA"
    ]
    graphics [
      x 379.5200784280163
      y 911.8696893937436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1077"
      uniprot "NA"
    ]
    graphics [
      x 443.67631460972177
      y 1025.441094532092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M_space_ds_space_sgmRNA"
      name "M_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2343"
      uniprot "NA"
    ]
    graphics [
      x 337.3346277541742
      y 983.6011627675938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:28484023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1111"
      uniprot "NA"
    ]
    graphics [
      x 667.9511840451336
      y 1033.2490004625502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0071360"
      hgnc "NA"
      map_id "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      name "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2291"
      uniprot "NA"
    ]
    graphics [
      x 690.6998054624455
      y 1172.0330850772184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cellular_space_response_space_to_space_exogenous_space_dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1006"
      uniprot "NA"
    ]
    graphics [
      x 836.4991911300679
      y 722.3517304310654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "S_space_(_minus_)ss_space_sgmRNA"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2295; sa2325"
      uniprot "NA"
    ]
    graphics [
      x 1043.0295110241555
      y 981.8078066398847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1067"
      uniprot "NA"
    ]
    graphics [
      x 1205.9498799846344
      y 957.2524062801208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_248"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2332"
      uniprot "NA"
    ]
    graphics [
      x 1361.221563446117
      y 987.3489251904684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "N_space__space_(_minus_)ss_space_sgmRNA"
      name "N_space__space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2353"
      uniprot "NA"
    ]
    graphics [
      x 681.6947505068217
      y 678.3871355006346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_space__space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1099"
      uniprot "NA"
    ]
    graphics [
      x 821.4273368077555
      y 633.6498492630883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "N_space_ds_space_sgmRNA"
      name "N_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2354"
      uniprot "NA"
    ]
    graphics [
      x 1061.0314364486603
      y 390.4436196804176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1013"
      uniprot "NA"
    ]
    graphics [
      x 1627.8334198920174
      y 1693.747150933435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1090"
      uniprot "NA"
    ]
    graphics [
      x 1098.3190002955923
      y 1234.099795737723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "E_space_(_plus_)ss_space_sgmRNA"
      name "E_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2111"
      uniprot "NA"
    ]
    graphics [
      x 1325.8743184554298
      y 1172.1788171671833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf6_space_(_minus_)ss_space_sgmRNA"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2323; sa2297"
      uniprot "NA"
    ]
    graphics [
      x 987.2607742432838
      y 514.5566778358508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf6_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1081"
      uniprot "NA"
    ]
    graphics [
      x 926.1291069133492
      y 714.0136745688102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf6_space_ds_space_sgmRNA"
      name "Orf6_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2347"
      uniprot "NA"
    ]
    graphics [
      x 1056.9506147739899
      y 523.457868867019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf6_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1102"
      uniprot "NA"
    ]
    graphics [
      x 442.8351357273817
      y 893.0559347235941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "N_space_(_plus_)ss_space_sgmRNA"
      name "N_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1962"
      uniprot "NA"
    ]
    graphics [
      x 1422.9498041782203
      y 492.74942841996585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re940"
      uniprot "NA"
    ]
    graphics [
      x 1413.734745108708
      y 744.597202952403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0005783"
      hgnc "NA"
      map_id "Endoplasmic_space_reticulum"
      name "Endoplasmic_space_reticulum"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa439"
      uniprot "NA"
    ]
    graphics [
      x 1182.0752739331588
      y 370.4664143117314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Endoplasmic_space_reticulum"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re999"
      uniprot "NA"
    ]
    graphics [
      x 1111.8567174610191
      y 495.74643679916164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0039718"
      hgnc "NA"
      map_id "Double_minus_membrane_space_vesicle"
      name "Double_minus_membrane_space_vesicle"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa438"
      uniprot "NA"
    ]
    graphics [
      x 1234.4566956898493
      y 427.5348568923133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Double_minus_membrane_space_vesicle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1089"
      uniprot "NA"
    ]
    graphics [
      x 515.0619584701783
      y 943.8669102850114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M_space_(_plus_)ss_space_sgmRNA"
      name "M_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2113"
      uniprot "NA"
    ]
    graphics [
      x 842.0577123511976
      y 969.4568255081547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1094"
      uniprot "NA"
    ]
    graphics [
      x 1194.205731179487
      y 1202.8602396887652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf3a_space_(_plus_)ss_space_sgmRNA"
      name "Orf3a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2104"
      uniprot "NA"
    ]
    graphics [
      x 1447.7018588218432
      y 1164.7298489092107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf3a_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1119"
      uniprot "NA"
    ]
    graphics [
      x 573.7198101026778
      y 1008.6226472626049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "(_minus_)ss_space_gRNA"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2328; sa2314"
      uniprot "NA"
    ]
    graphics [
      x 598.2941255045317
      y 1071.8544750068077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_minus_)ss_space_gRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1076"
      uniprot "NA"
    ]
    graphics [
      x 493.3504189967715
      y 1122.4359991144802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "ds_space_gRNA"
      name "ds_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2342"
      uniprot "NA"
    ]
    graphics [
      x 433.0592769702547
      y 1249.0190019499214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ds_space_gRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1100"
      uniprot "NA"
    ]
    graphics [
      x 1339.2123320689425
      y 301.50089024133695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "N_space_(_minus_)ss_space_sgmRNA"
      name "N_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2355"
      uniprot "NA"
    ]
    graphics [
      x 1471.969663369252
      y 168.64153740443976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1096"
      uniprot "NA"
    ]
    graphics [
      x 469.29045161749457
      y 548.3172396230094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf9b_space_(_plus_)ss_space_sgmRNA"
      name "Orf9b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2109"
      uniprot "NA"
    ]
    graphics [
      x 745.3785500207966
      y 661.5999194220158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9b_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "UNIPROT:P0DTC8"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1879; sa2248"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 1664.1929015780474
      y 505.5844763316835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1017"
      uniprot "NA"
    ]
    graphics [
      x 1791.2479246526545
      y 463.93523121227435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1022"
      uniprot "NA"
    ]
    graphics [
      x 1164.451129345048
      y 1013.869694920768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re908"
      uniprot "NA"
    ]
    graphics [
      x 1468.1038729310512
      y 832.8302054750171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_155"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa1920"
      uniprot "NA"
    ]
    graphics [
      x 1549.5295067724992
      y 713.0121214014778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "(_plus_)ss_space_gRNA"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1675; sa2153"
      uniprot "NA"
    ]
    graphics [
      x 831.8112399599831
      y 1065.7439199897972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_plus_)ss_space_gRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1115"
      uniprot "NA"
    ]
    graphics [
      x 954.9079384181907
      y 903.1926362043351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1083"
      uniprot "NA"
    ]
    graphics [
      x 784.9426219267106
      y 512.2129151229243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1106"
      uniprot "NA"
    ]
    graphics [
      x 1081.2910784257365
      y 448.7391910930642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1004"
      uniprot "NA"
    ]
    graphics [
      x 510.45500170395997
      y 842.7019120774403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf14_space_(_minus_)ss_space_sgmRNA"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2301; sa2319"
      uniprot "NA"
    ]
    graphics [
      x 890.9889114012124
      y 524.6916527299763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf14_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1074"
      uniprot "NA"
    ]
    graphics [
      x 833.6864933953289
      y 274.4569597861571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_254"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2339"
      uniprot "NA"
    ]
    graphics [
      x 782.2408961908504
      y 142.88250157550783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1002"
      uniprot "NA"
    ]
    graphics [
      x 1164.974852151915
      y 557.08230426271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1118"
      uniprot "NA"
    ]
    graphics [
      x 1601.4551756312364
      y 101.40784456998006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_282"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2368"
      uniprot "NA"
    ]
    graphics [
      x 1682.5037711358782
      y 200.19342200253266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re989"
      uniprot "NA"
    ]
    graphics [
      x 789.4837424410684
      y 697.1277962622014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1023"
      uniprot "NA"
    ]
    graphics [
      x 1517.5656424087538
      y 1024.6752294610126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1011"
      uniprot "NA"
    ]
    graphics [
      x 1358.6399721744183
      y 1258.9613083394006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1109"
      uniprot "NA"
    ]
    graphics [
      x 698.8621136115564
      y 432.1791451123649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725312"
      hgnc "NA"
      map_id "Nsp11"
      name "Nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2364"
      uniprot "NA"
    ]
    graphics [
      x 797.7889681042716
      y 336.2996060004745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1069"
      uniprot "NA"
    ]
    graphics [
      x 1132.3705791061939
      y 303.95169980707476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_250"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2334"
      uniprot "NA"
    ]
    graphics [
      x 1261.923729079272
      y 220.37467932426807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1009"
      uniprot "NA"
    ]
    graphics [
      x 1607.0025254055047
      y 878.192391506851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7a_space_(_minus_)ss_space_sgmRNA"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2296; sa2324"
      uniprot "NA"
    ]
    graphics [
      x 579.9991231494151
      y 462.55156298982484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7a_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1068"
      uniprot "NA"
    ]
    graphics [
      x 487.472614344167
      y 312.94077630259005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_249"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2333"
      uniprot "NA"
    ]
    graphics [
      x 403.11034116050484
      y 245.5427137808755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1020"
      uniprot "NA"
    ]
    graphics [
      x 1852.4071099893781
      y 1269.9699550033563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf14_space_(_plus_)ss_space_sgmRNA"
      name "Orf14_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2110"
      uniprot "NA"
    ]
    graphics [
      x 1179.294811006616
      y 835.96117089369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf14_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1030"
      uniprot "NA"
    ]
    graphics [
      x 1261.9503843708944
      y 1063.351102499763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "UNIPROT:P0DTD3"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1877; sa2250"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 1265.9635567048294
      y 1312.78324248617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1066"
      uniprot "NA"
    ]
    graphics [
      x 822.5462942700174
      y 1324.9612343801216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_247"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2331"
      uniprot "NA"
    ]
    graphics [
      x 899.9861523833047
      y 1255.1620277584714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32362314"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re950"
      uniprot "NA"
    ]
    graphics [
      x 1712.7360844451637
      y 1393.5545802011575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubchem.compound:2536"
      hgnc "NA"
      map_id "Camostat_space_mesylate"
      name "Camostat_space_mesylate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1544"
      uniprot "NA"
    ]
    graphics [
      x 1722.255021105941
      y 1243.6431860074888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Camostat_space_mesylate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1085"
      uniprot "NA"
    ]
    graphics [
      x 867.8670941882051
      y 685.4103215226312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf14_space_ds_space_sgmRNA"
      name "Orf14_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2351"
      uniprot "NA"
    ]
    graphics [
      x 942.0416399278477
      y 578.5763594944857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf14_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re843"
      uniprot "NA"
    ]
    graphics [
      x 1388.1719238527298
      y 1480.8345119544592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000164733;urn:miriam:ncbigene:1508;urn:miriam:ncbigene:1508;urn:miriam:refseq:NM_147780;urn:miriam:uniprot:P07858;urn:miriam:uniprot:P07858;urn:miriam:hgnc:2527;urn:miriam:ec-code:3.4.22.1;urn:miriam:hgnc.symbol:CTSB;urn:miriam:hgnc.symbol:CTSB"
      hgnc "HGNC_SYMBOL:CTSB"
      map_id "UNIPROT:P07858"
      name "CTSB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1524"
      uniprot "UNIPROT:P07858"
    ]
    graphics [
      x 1193.0178748790179
      y 1443.0452077323105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07858"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7b_space_(_minus_)ss_space_sgmRNA"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2302; sa2318"
      uniprot "NA"
    ]
    graphics [
      x 713.6890280554064
      y 1276.5472398559173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7b_space_(_minus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1075"
      uniprot "NA"
    ]
    graphics [
      x 619.5543706234876
      y 1442.6043817715172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_255"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2340"
      uniprot "NA"
    ]
    graphics [
      x 688.3835789911157
      y 1546.0075518370622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1014"
      uniprot "NA"
    ]
    graphics [
      x 1690.2798354123952
      y 828.2441267783132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re859"
      uniprot "NA"
    ]
    graphics [
      x 1088.4036479134766
      y 1061.089682418945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1088"
      uniprot "NA"
    ]
    graphics [
      x 575.1170637252352
      y 1191.355399591732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1057"
      uniprot "NA"
    ]
    graphics [
      x 766.4985180815047
      y 982.3387650905463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_234"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2315"
      uniprot "NA"
    ]
    graphics [
      x 935.9470939643853
      y 836.5501509349355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1104"
      uniprot "NA"
    ]
    graphics [
      x 582.3546523180736
      y 859.1221428313819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1008"
      uniprot "NA"
    ]
    graphics [
      x 1287.3761490894758
      y 1225.3972113576901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7b_space_ds_space_sgmRNA"
      name "Orf7b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2352"
      uniprot "NA"
    ]
    graphics [
      x 716.9819270835892
      y 1413.3151487133541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7b_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1098"
      uniprot "NA"
    ]
    graphics [
      x 884.0343557207557
      y 1431.7587439859035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_90"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1105"
      uniprot "NA"
    ]
    graphics [
      x 199.81863977036357
      y 698.6306757310351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "Host_space_translation"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2358; sa2237"
      uniprot "NA"
    ]
    graphics [
      x 191.41740397384353
      y 575.6793408170025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Host_space_translation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1093"
      uniprot "NA"
    ]
    graphics [
      x 1238.4173857900198
      y 524.2573048294275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      name "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2108"
      uniprot "NA"
    ]
    graphics [
      x 1427.9523996791577
      y 617.5711184143146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf6_space__space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1019"
      uniprot "NA"
    ]
    graphics [
      x 1263.0389545447
      y 1491.8463779817505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1107"
      uniprot "NA"
    ]
    graphics [
      x 1114.6855769971805
      y 707.525087442158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1080"
      uniprot "NA"
    ]
    graphics [
      x 550.1382589512517
      y 604.5351438293576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "Orf7a_space_ds_space_sgmRNA"
      name "Orf7a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2346"
      uniprot "NA"
    ]
    graphics [
      x 609.2749391817123
      y 392.91215990729927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7a_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re952"
      uniprot "NA"
    ]
    graphics [
      x 1609.8743626422583
      y 1464.4448342686092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1073"
      uniprot "NA"
    ]
    graphics [
      x 311.4648810666964
      y 477.7723673346872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_253"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2338"
      uniprot "NA"
    ]
    graphics [
      x 302.6371155857091
      y 331.16386068537315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1092"
      uniprot "NA"
    ]
    graphics [
      x 787.9545129251046
      y 415.15564733450464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1029"
      uniprot "NA"
    ]
    graphics [
      x 1063.3158860000808
      y 831.6841564879898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1064"
      uniprot "NA"
    ]
    graphics [
      x 189.64843567605442
      y 952.1300173968668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_246"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2329"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 977.7207710091427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_106"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re841"
      uniprot "NA"
    ]
    graphics [
      x 1447.4949354624819
      y 1314.9849415748417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1016"
      uniprot "NA"
    ]
    graphics [
      x 1798.383233368921
      y 1437.5699701832614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:8830530"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1123"
      uniprot "NA"
    ]
    graphics [
      x 748.3398893425215
      y 825.6207022867724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1113"
      uniprot "NA"
    ]
    graphics [
      x 1057.674665440934
      y 907.7645676479751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re895"
      uniprot "NA"
    ]
    graphics [
      x 1371.7155379138078
      y 863.8836162185514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re838"
      uniprot "NA"
    ]
    graphics [
      x 1797.9619739640725
      y 1693.5515555551237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "Heparan_space_sulfate"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2369; sa2370"
      uniprot "NA"
    ]
    graphics [
      x 1737.0923982437157
      y 1821.306792834947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Heparan_space_sulfate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1079"
      uniprot "NA"
    ]
    graphics [
      x 895.310839756391
      y 1047.3863150286365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "S_space_ds_space_sgmRNA"
      name "S_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2345"
      uniprot "NA"
    ]
    graphics [
      x 1046.9230858595063
      y 1140.4016395616197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_space_ds_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1097"
      uniprot "NA"
    ]
    graphics [
      x 1061.8052674423484
      y 659.5263412241829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re894"
      uniprot "NA"
    ]
    graphics [
      x 1308.4069564032498
      y 852.5227300714162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1000"
      uniprot "NA"
    ]
    graphics [
      x 299.9270257600184
      y 659.4968979132051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re948"
      uniprot "NA"
    ]
    graphics [
      x 1252.9366293742085
      y 884.5468681834265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1001"
      uniprot "NA"
    ]
    graphics [
      x 762.4504312762185
      y 891.9306677094863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1063"
      uniprot "NA"
    ]
    graphics [
      x 685.7209747505358
      y 1105.0052030084062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1026"
      uniprot "NA"
    ]
    graphics [
      x 1582.925013097612
      y 774.4931522516433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1010"
      uniprot "NA"
    ]
    graphics [
      x 1602.8745584961646
      y 1638.16421812258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1021"
      uniprot "NA"
    ]
    graphics [
      x 1717.962374790779
      y 950.7768177154785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re972"
      uniprot "NA"
    ]
    graphics [
      x 953.1937347664134
      y 1002.8252095797528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1012"
      uniprot "NA"
    ]
    graphics [
      x 1677.7234299989402
      y 891.432879929805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32155444;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re852"
      uniprot "NA"
    ]
    graphics [
      x 1724.2225029447118
      y 1698.4375558848305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1091"
      uniprot "NA"
    ]
    graphics [
      x 1185.1770945190058
      y 1122.001745906917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "S_space_(_plus_)ss_space_sgmRNA"
      name "S_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2112"
      uniprot "NA"
    ]
    graphics [
      x 1392.3037573557767
      y 1172.8073868373997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1086"
      uniprot "NA"
    ]
    graphics [
      x 621.6272848101471
      y 1250.854479248253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1027"
      uniprot "NA"
    ]
    graphics [
      x 1608.5021769228651
      y 1109.9878506961818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1015"
      uniprot "NA"
    ]
    graphics [
      x 1793.460696885621
      y 1025.0327704916365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1028"
      uniprot "NA"
    ]
    graphics [
      x 1496.5228855118341
      y 592.3512263057228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1070"
      uniprot "NA"
    ]
    graphics [
      x 1082.5756496437662
      y 1337.6305710419244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_251"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2335"
      uniprot "NA"
    ]
    graphics [
      x 1225.6843594656848
      y 1414.408538561749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1072"
      uniprot "NA"
    ]
    graphics [
      x 914.936537378276
      y 196.84966498621168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_252"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2337"
      uniprot "NA"
    ]
    graphics [
      x 935.1661397047003
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1110"
      uniprot "NA"
    ]
    graphics [
      x 621.9458806872431
      y 623.3725157026951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1024"
      uniprot "NA"
    ]
    graphics [
      x 1570.7682615134063
      y 1226.0140529661371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 201
    source 19
    target 31
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S2"
      target_id "M18_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 31
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_121"
      target_id "S2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7a_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 33
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_44"
      target_id "UNIPROT:P0DTC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 4
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 35
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_114"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7b_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 34
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_50"
      target_id "UNIPROT:P0DTD8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 4
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 11
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 13
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 5
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 19
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "S2"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 39
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 39
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "UNIPROT:P0DTC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 39
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 39
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 28
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp10"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 30
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp16"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 26
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp15"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 29
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp14"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp13"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 42
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp12"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 43
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp9"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 20
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp8"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 23
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp7"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 21
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 25
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 22
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp4"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 21
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp6"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 23
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp7"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 20
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp8"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 43
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp9"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 28
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp10"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 44
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp2"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 44
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp2"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 40
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_97"
      target_id "Replication_space_transcription_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 4
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 11
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 5
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 13
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 19
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "S2"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 15
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cholesterol"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 16
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O14786"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 46
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 46
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 46
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 46
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "UNIPROT:P0DTC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf8_space_ds_space_sgmRNA"
      target_id "M18_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "Orf8_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 48
    target 50
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "Orf8_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_minus_4"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "Nsp3_minus_4"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 52
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "UNIPROT:Nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 52
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "Nsp4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf9b_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 45
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_72"
      target_id "Orf9b_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 45
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 56
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_102"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 4
    target 57
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 57
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_52"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 17
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S1"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 16
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14786"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 58
    target 16
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_104"
      target_id "UNIPROT:O14786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "E_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 45
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_66"
      target_id "E_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 9
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD2"
      target_id "M18_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 62
    target 9
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_37"
      target_id "UNIPROT:P0DTD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf3a_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 45
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_70"
      target_id "Orf3a_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 45
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_65"
      target_id "M_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 26
    target 69
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp15"
      target_id "M18_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_96"
      target_id "cellular_space_response_space_to_space_exogenous_space_dsRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 12
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 12
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 71
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 71
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "Nsp5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 71
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "Nsp3_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_56"
      target_id "M18_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "N_space__space_(_minus_)ss_space_sgmRNA"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 45
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_86"
      target_id "N_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 7
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 78
    target 7
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_32"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 61
    target 79
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "E_space_ds_space_sgmRNA"
      target_id "M18_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 79
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "E_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "E_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf6_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 45
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_69"
      target_id "Orf6_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 12
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 12
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 84
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 84
    target 44
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "Nsp2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 84
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "Nsp1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "N_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 34
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 86
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_115"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Endoplasmic_space_reticulum"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 22
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "Nsp4"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 25
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "UNIPROT:Nsp3"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 21
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "Nsp6"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 21
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "Nsp6"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_123"
      target_id "Double_minus_membrane_space_vesicle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 68
    target 90
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M_space_ds_space_sgmRNA"
      target_id "M18_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 90
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 65
    target 92
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf3a_space_ds_space_sgmRNA"
      target_id "M18_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 92
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "Orf3a_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "Orf3a_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 45
    target 94
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 94
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_101"
      target_id "Replication_space_transcription_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_minus_)ss_space_gRNA"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 45
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_64"
      target_id "ds_space_gRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 77
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "N_space_ds_space_sgmRNA"
      target_id "M18_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "N_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 98
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "N_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 55
    target 100
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf9b_space_ds_space_sgmRNA"
      target_id "M18_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 100
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "Orf9b_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "Orf9b_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC8"
      target_id "M18_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_36"
      target_id "UNIPROT:P0DTC8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 91
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 34
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 104
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_41"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 4
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_113"
      target_id "M18_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 107
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)ss_space_gRNA"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 34
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 108
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_99"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 49
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf8_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 45
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 109
    target 47
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_71"
      target_id "Orf8_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 51
    target 110
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_minus_4"
      target_id "M18_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 110
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_91"
      target_id "Nsp3_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 12
    target 111
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 111
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_25"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf14_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_62"
      target_id "M18_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 51
    target 115
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_minus_4"
      target_id "M18_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 115
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_24"
      target_id "Nsp3_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 99
    target 116
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "N_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_100"
      target_id "M18_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 12
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 12
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 118
    target 8
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 118
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "Nsp3_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 118
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "Nsp5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 80
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "E_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 34
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 119
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_42"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 5
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "M18_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 120
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_30"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 8
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 24
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "Nsp5"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 121
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 121
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 121
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 121
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 121
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "Nsp11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 81
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf6_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_58"
      target_id "M18_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 11
    target 125
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "M18_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 125
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_28"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7a_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_57"
      target_id "M18_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 38
    target 129
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD8"
      target_id "M18_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 129
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_39"
      target_id "UNIPROT:P0DTD8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf14_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 34
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_49"
      target_id "UNIPROT:P0DTD3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 59
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "E_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_55"
      target_id "M18_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 14
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 10
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09958"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 2
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15393"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "INHIBITION"
      source_id "Camostat_space_mesylate"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 135
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "S2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 135
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 135
    target 17
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "S1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 135
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 112
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf14_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 45
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_73"
      target_id "Orf14_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 14
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07858"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 3
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07711"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 139
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "S2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 139
    target 17
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "S1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7b_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 142
    target 143
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_63"
      target_id "M18_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 13
    target 144
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "M18_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 144
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_33"
      target_id "UNIPROT:P0DTC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 4
    target 145
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 145
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "(_plus_)ss_space_gRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 145
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 97
    target 146
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "ds_space_gRNA"
      target_id "M18_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 146
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "(_minus_)ss_space_gRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 146
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "(_plus_)ss_space_gRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 95
    target 147
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_minus_)ss_space_gRNA"
      target_id "M18_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_51"
      target_id "M18_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 8
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 8
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 149
    target 8
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 149
    target 44
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "Nsp2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 149
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "Nsp1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 5
    target 150
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "M18_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 150
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_27"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7b_space_ds_space_sgmRNA"
      target_id "M18_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 152
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "Orf7b_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 152
    target 36
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "Orf7b_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 27
    target 153
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp1"
      target_id "M18_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_90"
      target_id "Host_space_translation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 83
    target 155
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf6_space_ds_space_sgmRNA"
      target_id "M18_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 155
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "Orf6_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "Orf6_space__space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 132
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD3"
      target_id "M18_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 157
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_38"
      target_id "UNIPROT:P0DTD3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 51
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp3_minus_4"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 51
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "Nsp3_minus_4"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 158
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "Nsp4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 158
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "UNIPROT:Nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 126
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7a_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 45
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_68"
      target_id "Orf7a_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 19
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S2"
      target_id "M18_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 161
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_118"
      target_id "S2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 53
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf9b_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_61"
      target_id "M18_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 160
    target 164
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7a_space_ds_space_sgmRNA"
      target_id "M18_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 164
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "Orf7a_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 164
    target 32
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "Orf7a_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 101
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf9b_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 34
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 165
    target 9
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_48"
      target_id "UNIPROT:P0DTD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 66
    target 166
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_54"
      target_id "M18_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 4
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 14
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 168
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_106"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 6
    target 169
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "M18_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 169
    target 6
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_35"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 95
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_minus_)ss_space_gRNA"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 45
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 170
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf7b_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 170
    target 75
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "N_space__space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 170
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 170
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "E_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 170
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "S_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 170
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf7a_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 170
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf6_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 170
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf3a_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 170
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf8_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 170
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf9b_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 170
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "Orf14_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 107
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)ss_space_gRNA"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 34
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 171
    target 8
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_98"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 4
    target 172
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 172
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_112"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 1
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 7
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 7
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Heparan_space_sulfate"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 173
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_105"
      target_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 72
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 45
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_67"
      target_id "S_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 138
    target 177
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf14_space_ds_space_sgmRNA"
      target_id "M18_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 177
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "Orf14_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 177
    target 130
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "Orf14_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 4
    target 178
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 178
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_111"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 27
    target 179
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Nsp1"
      target_id "M18_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 179
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_22"
      target_id "Host_space_translation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 4
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 180
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_116"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 8
    target 181
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "M18_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 181
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_23"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 107
    target 182
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)ss_space_gRNA"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 45
    target 182
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 182
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_53"
      target_id "(_minus_)ss_space_gRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 156
    target 183
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 34
    target 183
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 183
    target 18
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_45"
      target_id "UNIPROT:P0DTC6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 7
    target 184
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 184
    target 7
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_29"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 13
    target 185
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "M18_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 185
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_40"
      target_id "UNIPROT:P0DTC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 4
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 107
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)ss_space_gRNA"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 186
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 186
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "Replication_space_transcription_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 11
    target 187
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "M18_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 187
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_31"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 7
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 1
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 7
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 174
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Heparan_space_sulfate"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 188
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_108"
      target_id "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 176
    target 189
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S_space_ds_space_sgmRNA"
      target_id "M18_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 189
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "S_space_(_minus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 189
    target 190
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "S_space_(_plus_)ss_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 141
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf7b_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 45
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 191
    target 151
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_74"
      target_id "Orf7b_space_ds_space_sgmRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 93
    target 192
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf3a_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 34
    target 192
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 192
    target 6
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_46"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 18
    target 193
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC6"
      target_id "M18_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 193
    target 18
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_34"
      target_id "UNIPROT:P0DTC6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 50
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf8_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 34
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 194
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_47"
      target_id "UNIPROT:P0DTC8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 63
    target 195
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf3a_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_59"
      target_id "M18_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 49
    target 197
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "Orf8_space_(_minus_)ss_space_sgmRNA"
      target_id "M18_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_60"
      target_id "M18_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 12
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 24
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "Nsp5"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 199
    target 41
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 199
    target 42
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 199
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 199
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 199
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 199
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 199
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 199
    target 30
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 199
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 199
    target 29
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "Nsp14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 190
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "S_space_(_plus_)ss_space_sgmRNA"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 34
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "Host_space_translation_space_complex"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 200
    target 7
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_43"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
