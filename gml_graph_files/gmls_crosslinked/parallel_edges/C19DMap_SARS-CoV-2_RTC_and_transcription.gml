# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917572; urn:miriam:ncbiprotein:YP_009725306; NA"
      hgnc "NA"
      map_id "nsp10"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de394; a99b9; a8b3c; b19e5; glyph61"
      uniprot "NA"
    ]
    graphics [
      x 376.52095049755917
      y 505.8512722227272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4861; WP4868; WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725310; NA"
      hgnc "NA"
      map_id "nsp15"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205; fa46a; beaa5; c625e; glyph52"
      uniprot "NA"
    ]
    graphics [
      x 781.7857490098354
      y 816.5362839680639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32592996; urn:miriam:wikidata:Q89686805; NA"
      hgnc "NA"
      map_id "nsp9"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94; cf2d5; de7fa; glyph51; glyph42"
      uniprot "NA"
    ]
    graphics [
      x 188.87815055100282
      y 439.3772542274838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038956; NA"
      hgnc "NA"
      map_id "nsp4"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e019b; e5589; glyph41"
      uniprot "NA"
    ]
    graphics [
      x 373.6045690762722
      y 385.0985345369721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q89006922; NA"
      hgnc "NA"
      map_id "nsp2"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6991; glyph37"
      uniprot "NA"
    ]
    graphics [
      x 485.96566836394726
      y 215.30848991322895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725311; NA"
      hgnc "NA"
      map_id "nsp16"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4; abf4b; c8bd9; glyph36; sa81"
      uniprot "NA"
    ]
    graphics [
      x 838.8427613139872
      y 740.3661020311008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q88656943; urn:miriam:ncbiprotein:YP_009725302; NA"
      hgnc "NA"
      map_id "nsp6"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7ad1; c2256; f6f18; glyph56"
      uniprot "NA"
    ]
    graphics [
      x 296.4213644082062
      y 151.68580271687546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q94648377; urn:miriam:refseq:QII57165.1; NA"
      hgnc "NA"
      map_id "nsp13"
      name "nsp13"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "c38dc; a8ee9; f1b6a; glyph34"
      uniprot "NA"
    ]
    graphics [
      x 819.2064074673074
      y 649.9128459081269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4846; WP5027; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882; urn:miriam:wikidata:Q90038952; urn:miriam:ncbiprotein:YP_009725297; NA"
      hgnc "NA"
      map_id "nsp1"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606; f9094; e6110; d2ec8; da173; glyph39"
      uniprot "NA"
    ]
    graphics [
      x 492.92023047470377
      y 287.3380470985534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038963; NA"
      hgnc "NA"
      map_id "nsp7"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "db214; e64da; glyph33"
      uniprot "NA"
    ]
    graphics [
      x 224.95202403525138
      y 167.3415782263961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917582; NA"
      hgnc "NA"
      map_id "nsp5"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5d9f; glyph67"
      uniprot "NA"
    ]
    graphics [
      x 416.56837819427415
      y 180.27078323900326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436; NA"
      hgnc "NA"
      map_id "nsp12"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d0526; a4ef4; glyph62"
      uniprot "NA"
    ]
    graphics [
      x 576.9207317377366
      y 708.1151143215116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; NA"
      hgnc "NA"
      map_id "nsp14"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9; glyph54; sa80"
      uniprot "NA"
    ]
    graphics [
      x 567.5229246302187
      y 779.5942118262087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp3"
      name "nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed5ac; glyph45"
      uniprot "NA"
    ]
    graphics [
      x 423.48132341236817
      y 322.05053918359465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp8_space_(I)"
      name "nsp8_space_(I)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph49"
      uniprot "NA"
    ]
    graphics [
      x 173.99018026592734
      y 222.83558393176415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp8_space_(I)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph76"
      uniprot "NA"
    ]
    graphics [
      x 67.23143830759705
      y 269.25086350424914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp8_space_(II)"
      name "nsp8_space_(II)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph50"
      uniprot "NA"
    ]
    graphics [
      x 178.3601316579921
      y 306.4492501643957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp8_space_(II)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "complex"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph8; glyph6"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 453.27789798035013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_38"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph73"
      uniprot "NA"
    ]
    graphics [
      x 398.5801077271869
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph85"
      uniprot "NA"
    ]
    graphics [
      x 351.0749737779156
      y 211.0880767754226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph13; glyph17"
      uniprot "NA"
    ]
    graphics [
      x 315.9538855801548
      y 443.4738208952697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "pp1a"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph63; glyph46"
      uniprot "NA"
    ]
    graphics [
      x 440.30674413668083
      y 122.49292889631198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pp1a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "CoV_space_poly_minus__br_merase_space_complex"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph5; glyph4"
      uniprot "NA"
    ]
    graphics [
      x 247.49950162770074
      y 680.5958195559342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoV_space_poly_minus__br_merase_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph77"
      uniprot "NA"
    ]
    graphics [
      x 123.46857941434996
      y 585.6128487425405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "Replication_space_transcription_space_complex_space_"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph3; glyph2"
      uniprot "NA"
    ]
    graphics [
      x 181.2496839633049
      y 760.6334326246449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Replication_space_transcription_space_complex_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_39"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph74"
      uniprot "NA"
    ]
    graphics [
      x 427.9355429002352
      y 1007.540405786395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph81"
      uniprot "NA"
    ]
    graphics [
      x 342.9730432578402
      y 936.8420033877128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_plus_)sgRNA_space_(2_minus_9)"
      name "(_plus_)sgRNA_space_(2_minus_9)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph20"
      uniprot "NA"
    ]
    graphics [
      x 506.5167675167127
      y 1053.1103295259154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_plus_)sgRNA_space_(2_minus_9)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph86"
      uniprot "NA"
    ]
    graphics [
      x 147.49780655108202
      y 883.8244009871562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_36"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph71"
      uniprot "NA"
    ]
    graphics [
      x 265.6038337425163
      y 888.6874568782089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_35"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph70"
      uniprot "NA"
    ]
    graphics [
      x 562.9827368524745
      y 608.4153787485134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph87"
      uniprot "NA"
    ]
    graphics [
      x 484.94161834500073
      y 534.1862830144895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon 1 pathway"
      full_annotation "NA; urn:miriam:pubmed:24622840;urn:miriam:ncbiprotein:YP_009724389"
      hgnc "NA"
      map_id "pp1ab"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph40; glyph53; sa184"
      uniprot "NA"
    ]
    graphics [
      x 647.8468591506227
      y 566.4906369426158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pp1ab"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph79"
      uniprot "NA"
    ]
    graphics [
      x 711.7994435899552
      y 716.0835857226945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_37"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph72"
      uniprot "NA"
    ]
    graphics [
      x 683.7214254181383
      y 421.70917896640015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph78"
      uniprot "NA"
    ]
    graphics [
      x 549.0380020160419
      y 459.40205381362085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_34"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph69"
      uniprot "NA"
    ]
    graphics [
      x 572.5841806871993
      y 198.93896992523221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph83"
      uniprot "NA"
    ]
    graphics [
      x 420.94055043700587
      y 257.8635972921303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_48"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph82"
      uniprot "NA"
    ]
    graphics [
      x 323.3658545757815
      y 287.5723863504025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp11"
      name "nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph66"
      uniprot "NA"
    ]
    graphics [
      x 463.5873586316406
      y 363.35829443308575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph80"
      uniprot "NA"
    ]
    graphics [
      x 188.01421757210716
      y 565.3069532518381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      name "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph16"
      uniprot "NA"
    ]
    graphics [
      x 286.77917406428
      y 607.6125321063557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_40"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph75"
      uniprot "NA"
    ]
    graphics [
      x 711.4070004590692
      y 1232.599799881951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph88"
      uniprot "NA"
    ]
    graphics [
      x 662.1117472711587
      y 1124.200817468289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "viral_space_accessory"
      name "viral_space_accessory"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph19"
      uniprot "NA"
    ]
    graphics [
      x 774.0549053022579
      y 1063.2765382338634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_accessory"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "structural_space_proteins"
      name "structural_space_proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph18"
      uniprot "NA"
    ]
    graphics [
      x 797.2606376024221
      y 1148.6817795534039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "structural_space_proteins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph84"
      uniprot "NA"
    ]
    graphics [
      x 424.79046887111144
      y 691.7761237956083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 48
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp8_space_(I)"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp8_space_(II)"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 10
    target 16
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp7"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_41"
      target_id "complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_38"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_51"
      target_id "pp1a"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "CoV_space_poly_minus__br_merase_space_complex"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 3
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp9"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 18
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "complex"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 21
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_42"
      target_id "Replication_space_transcription_space_complex_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_39"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 25
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_47"
      target_id "(_plus_)sgRNA_space_(2_minus_9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 25
    target 29
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_36"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 29
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_52"
      target_id "Replication_space_transcription_space_complex_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_35"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 21
    target 32
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_53"
      target_id "pp1ab"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "pp1ab"
      target_id "M15_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 34
    target 12
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 34
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 34
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 34
    target 6
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 34
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_37"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 21
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 36
    target 33
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_43"
      target_id "pp1ab"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_34"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 21
    target 38
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 38
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_49"
      target_id "pp1a"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 22
    target 39
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "pp1a"
      target_id "M15_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 39
    target 15
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp8_space_(I)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 39
    target 17
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp8_space_(II)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 39
    target 10
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 39
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 39
    target 9
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 39
    target 5
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 39
    target 14
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 39
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 39
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 39
    target 4
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 39
    target 1
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 25
    target 41
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 41
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "CoV_space_poly_minus__br_merase_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 41
    target 18
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 41
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 41
    target 21
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_40"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 28
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)sgRNA_space_(2_minus_9)"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "viral_space_accessory"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "structural_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 13
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp14"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 1
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp10"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 12
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp12"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 47
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_50"
      target_id "CoV_space_poly_minus__br_merase_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
