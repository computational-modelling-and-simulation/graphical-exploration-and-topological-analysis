# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 726.4648550388495
      y 841.5990076903073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:E protein interactions; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "UNIPROT:E"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131; sa13; sa32; sa22; sa19; sa83; sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 645.4007387326585
      y 1140.6775927610997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "UNIPROT:O60885"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12; sa16"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 533.3808573138306
      y 674.5657929199225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60885"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 555.68712410487
      y 901.1436673827397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:E protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29101"
      hgnc "NA"
      map_id "Na_plus_"
      name "Na_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa68; sa67; sa336; sa337"
      uniprot "NA"
    ]
    graphics [
      x 145.57279251205637
      y 1275.0061640573197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Na_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 202.43640248956416
      y 1394.1270329551835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481;urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476"
      hgnc "HGNC_SYMBOL:ATP1B1;HGNC_SYMBOL:ATP1A1"
      map_id "UNIPROT:P05026;UNIPROT:P05023"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P05026;UNIPROT:P05023"
    ]
    graphics [
      x 328.42006351391115
      y 1494.630168527121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05026;UNIPROT:P05023"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:ASIC1"
      map_id "UNIPROT:P78348"
      name "ASIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa25"
      uniprot "UNIPROT:P78348"
    ]
    graphics [
      x 544.6618242145476
      y 1065.3756147470162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_16"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 655.0912874328185
      y 954.833888433958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "UNIPROT:Q9BUF7"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29; sa34"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 485.80423976817224
      y 971.2117706607273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BUF7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 372.70817134433315
      y 921.5687589969168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:PALS1;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5;HGNC_SYMBOL:PALS1"
      map_id "UNIPROT:E;UNIPROT:Q8N3R9"
      name "E_minus_PALS1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      x 987.2932197632438
      y 1308.8438980427054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E;UNIPROT:Q8N3R9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_27"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 994.1074687168066
      y 1439.954052455113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Maintenance_space_of_space_tight_space_junction"
      name "Maintenance_space_of_space_tight_space_junction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa74"
      uniprot "NA"
    ]
    graphics [
      x 871.9561052626188
      y 1449.338255936311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Maintenance_space_of_space_tight_space_junction"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CDK9;HGNC_SYMBOL:CCNT1"
      map_id "UNIPROT:P50750;UNIPROT:O60563"
      name "P_minus_TEFb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      x 267.5571429765987
      y 172.05351417093493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50750;UNIPROT:O60563"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_22"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 204.86144325880372
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      name "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 139.10348711910706
      y 155.22769729395452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "UNIPROT:P20226"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18; sa17"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1091.8570231048811
      y 712.6263019559727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P20226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 974.663483781098
      y 724.6222503348162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "UNIPROT:P25440"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21; sa20"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 840.5810168373074
      y 758.2514128270376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25440"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_10"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 838.459599853707
      y 1222.2264475157128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "UNIPROT:Q8N3R9"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30; sa35"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 792.6267201532622
      y 1109.8339580273578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N3R9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_28"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 219.70752560767124
      y 1554.5765960517588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Activity_space_of_space_sodium_space_channels"
      name "Activity_space_of_space_sodium_space_channels"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa73"
      uniprot "NA"
    ]
    graphics [
      x 270.05359448410866
      y 1443.1014738168988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Activity_space_of_space_sodium_space_channels"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "UNIPROT:P50750"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10; sa15"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 298.80232474346116
      y 448.6527142892548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 386.6658350708117
      y 596.5886638106097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 488.7105058338685
      y 1182.7028302215554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033"
      hgnc "HGNC_SYMBOL:STOML3"
      map_id "UNIPROT:Q8TAV4"
      name "STOML3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:Q8TAV4"
    ]
    graphics [
      x 348.5226134426045
      y 1198.394975727761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TAV4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:STOML3;HGNC_SYMBOL:ASIC1"
      map_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
      name "ASIC1_space_trimer:H_plus_:STOML3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q8TAV4;UNIPROT:P78348"
    ]
    graphics [
      x 429.32350530996644
      y 1298.6074984181937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TAV4;UNIPROT:P78348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "UNIPROT:O60563"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11; sa14"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 416.96674488363044
      y 380.65381685993134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60563"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 491.3378034434318
      y 523.4397236272733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 776.2280194818717
      y 1001.6894297289346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14; urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504; urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504; urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14; HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H4C1; HGNC_SYMBOL:H4C1; HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1"
      map_id "UNIPROT:P62805"
      name "H4C14; H4_minus_16; H4C1; H4C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9; sa8; sa2; sa5"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 906.1262738202827
      y 387.97873561621753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62805"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 849.3424805255745
      y 291.22568479100045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464"
      hgnc "HGNC_SYMBOL:H3C15"
      map_id "UNIPROT:Q71DI3"
      name "H3C15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q71DI3"
    ]
    graphics [
      x 965.8019233636205
      y 231.4494328848882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q71DI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1"
      hgnc "HGNC_SYMBOL:H3C1"
      map_id "UNIPROT:P68431"
      name "H3C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P68431"
    ]
    graphics [
      x 973.6938331059971
      y 321.98132982691953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P68431"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349"
      hgnc "HGNC_SYMBOL:H2BC21"
      map_id "UNIPROT:Q16778"
      name "H2BC21"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q16778"
    ]
    graphics [
      x 880.8717364824797
      y 171.63420793881596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16778"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:interpro:IPR002119"
      hgnc "NA"
      map_id "H2A"
      name "H2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 774.9252918696963
      y 199.94258328189892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4-16"
      map_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      x 779.065516160843
      y 442.6012859808432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 649.544346551987
      y 1037.9177130808266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "UNIPROT:Q8NI35"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33; sa28"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 633.8183270867456
      y 880.4245458904702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NI35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9;urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:PATJ;HGNC_SYMBOL:MPP5;HGNC_SYMBOL:CRB3"
      map_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
      name "CRB3:PALS1:PATJ_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      x 719.747991501864
      y 1193.6993074556042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 784.9955119334816
      y 1342.3742790441293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 620.0899094553693
      y 767.1523336330354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 838.0854511805437
      y 969.0681067133744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:pubchem.compound:46907787"
      hgnc "NA"
      map_id "JQ_minus_1"
      name "JQ_minus_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 957.7940588966949
      y 996.1051521535298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "JQ_minus_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 710.4889203705686
      y 601.3030394659374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119"
      hgnc "HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H4C14"
      map_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      x 584.7229878291806
      y 521.7656655127475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:18406326"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_24"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 421.2288527404598
      y 508.36396563394993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Chromatin_space_organization"
      name "Chromatin_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 298.17624293072834
      y 549.5589383258605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Chromatin_space_organization"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_21"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 300.26797546558595
      y 305.95359974840324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476;urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481"
      hgnc "HGNC_SYMBOL:ATP1A1;HGNC_SYMBOL:ATP1B1"
      map_id "UNIPROT:P05023;UNIPROT:P05026"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P05023;UNIPROT:P05026"
    ]
    graphics [
      x 559.4574245034376
      y 1468.1302496050687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05023;UNIPROT:P05026"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 519.5261132700914
      y 1354.7333422383017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:E protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "K_plus_"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa65; sa66; sa521"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1346.3360108956585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "K_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 148.14311773946287
      y 1469.2767658682742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 276.83554066704653
      y 1305.3655471056163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_30"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 389.49643396133155
      y 1416.5195873642876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1122.654005128316
      y 1305.0437926644795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_58"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1197.3295174121488
      y 1213.2092334217195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 60
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60885"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_31"
      target_id "UNIPROT:O60885"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Na_plus_"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_20"
      target_id "Na_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78348"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 1
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_16"
      target_id "UNIPROT:P78348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BUF7"
      target_id "M17_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_13"
      target_id "UNIPROT:Q9BUF7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E;UNIPROT:Q8N3R9"
      target_id "M17_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_27"
      target_id "Maintenance_space_of_space_tight_space_junction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750;UNIPROT:O60563"
      target_id "M17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_22"
      target_id "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P20226"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P25440"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_34"
      target_id "UNIPROT:P20226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 2
    target 21
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 21
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_10"
      target_id "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 7
    target 23
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_28"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 3
    target 26
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_9"
      target_id "UNIPROT:P50750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 8
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78348"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 2
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 27
    target 29
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_17"
      target_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60563"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 3
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_19"
      target_id "UNIPROT:O60563"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 22
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 32
    target 22
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_12"
      target_id "UNIPROT:Q8N3R9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62805"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62805"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62805"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q71DI3"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 36
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P68431"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62805"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 37
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16778"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 38
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "H2A"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 34
    target 39
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_32"
      target_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 22
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 10
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BUF7"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_25"
      target_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
      target_id "M17_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 43
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_26"
      target_id "Maintenance_space_of_space_tight_space_junction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35"
      target_id "M17_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 44
    target 41
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_14"
      target_id "UNIPROT:Q8NI35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 20
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25440"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 2
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "JQ_minus_1"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 45
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_33"
      target_id "UNIPROT:P25440"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 39
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 3
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 20
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P25440"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_23"
      target_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
      target_id "M17_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_24"
      target_id "Chromatin_space_organization"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 25
    target 51
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 30
    target 51
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60563"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 51
    target 15
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_21"
      target_id "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05023;UNIPROT:P05026"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 2
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 53
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_15"
      target_id "UNIPROT:P05026;UNIPROT:P05023"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "K_plus_"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 7
    target 55
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_18"
      target_id "K_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 28
    target 56
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4"
      target_id "M17_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 56
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_29"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 29
    target 57
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
      target_id "M17_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 57
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_30"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 12
    target 58
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E;UNIPROT:Q8N3R9"
      target_id "M17_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_11"
      target_id "M17_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
