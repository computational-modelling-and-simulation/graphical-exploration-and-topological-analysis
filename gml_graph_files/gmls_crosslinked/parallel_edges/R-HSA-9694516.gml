# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "RTC_space_inhibitors"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1483; layout_1486; layout_2503; layout_2506"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 553.8607269112993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RTC_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SPO-2997551-3;urn:miriam:reactome:R-XTR-2997551;urn:miriam:reactome:R-SPO-2997551-2;urn:miriam:reactome:R-CEL-2997551;urn:miriam:reactome:R-SSC-2997551;urn:miriam:uniprot:P49841;urn:miriam:reactome:R-BTA-2997551;urn:miriam:reactome:R-SCE-2997551-2;urn:miriam:reactome:R-SCE-2997551-3;urn:miriam:reactome:R-SCE-2997551-4;urn:miriam:reactome:R-DRE-2997551;urn:miriam:reactome:R-SPO-2997551;urn:miriam:reactome:R-HSA-2997551;urn:miriam:reactome:R-DME-2997551;urn:miriam:reactome:R-RNO-198619;urn:miriam:reactome:R-SCE-2997551;urn:miriam:reactome:R-DME-2997551-2;urn:miriam:reactome:R-CEL-2997551-3;urn:miriam:reactome:R-DME-2997551-3;urn:miriam:reactome:R-CEL-2997551-4;urn:miriam:reactome:R-GGA-2997551;urn:miriam:reactome:R-CEL-2997551-2;urn:miriam:reactome:R-CFA-2997551;urn:miriam:reactome:R-MMU-2997551;urn:miriam:reactome:R-DDI-2997551; urn:miriam:reactome:R-HSA-9687663;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49841"
      name "GSK3B; GSK3B:GSKi"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_1717; layout_1895; layout_3593; layout_2349"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 1125.0601394105342
      y 395.27929650101856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687688;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "GSKi"
      name "GSKi"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1894; layout_2949"
      uniprot "NA"
    ]
    graphics [
      x 929.4048631265985
      y 699.0723102695978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSKi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1059.6420464810412
      y 903.0783344280788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P62987;urn:miriam:uniprot:P62979;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:P0CG47;urn:miriam:reactome:R-HSA-8943136"
      hgnc "NA"
      map_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      name "Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_364; layout_2409"
      uniprot "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    ]
    graphics [
      x 1353.0444650016616
      y 1353.124236443936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_methionine"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_422; layout_155; layout_182; layout_2299; layout_2282; layout_2431; layout_3787; layout_3766"
      uniprot "NA"
    ]
    graphics [
      x 464.8553110340998
      y 260.16160540698775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_methionine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_homocysteine"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_420; layout_158; layout_184; layout_2302; layout_2284; layout_2433; layout_3538"
      uniprot "NA"
    ]
    graphics [
      x 544.2838713765493
      y 437.7154485187594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_homocysteine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "NTP(4_minus_)"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_142; layout_149; layout_414; layout_174; layout_137; layout_3761; layout_2423; layout_2265; layout_2293; layout_2276"
      uniprot "NA"
    ]
    graphics [
      x 580.5872034665954
      y 115.14342119630214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NTP(4_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 481.67439802420404
      y 80.80918469757762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "a_space_nucleotide_space_sugar"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_48; layout_67; layout_2210; layout_2224"
      uniprot "NA"
    ]
    graphics [
      x 824.8503514878325
      y 235.60423787634488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a_space_nucleotide_space_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 1090.7915819308469
      y 456.25417788453825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_51; layout_69; layout_2213; layout_2227"
      uniprot "NA"
    ]
    graphics [
      x 901.9249511880996
      y 367.4658827042772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "nucleotide_minus_sugar"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_272; layout_389; layout_2366; layout_2418"
      uniprot "NA"
    ]
    graphics [
      x 1568.5881496161526
      y 551.4768919763195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleotide_minus_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_274; layout_388; layout_2368; layout_2420"
      uniprot "NA"
    ]
    graphics [
      x 1541.2151514536993
      y 488.73969554043333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685614; urn:miriam:reactome:R-ALL-9685610"
      hgnc "NA"
      map_id "CQ,_space_HCQ"
      name "CQ,_space_HCQ"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2035; layout_2029; layout_2931; layout_2932"
      uniprot "NA"
    ]
    graphics [
      x 1552.5765121654913
      y 611.3167153251563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ,_space_HCQ"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SSC-9660621;urn:miriam:reactome:R-DME-9660621-2;urn:miriam:reactome:R-CFA-9660621;urn:miriam:reactome:R-DRE-9660621;urn:miriam:reactome:R-MMU-9660621;urn:miriam:reactome:R-DDI-9660621;urn:miriam:reactome:R-HSA-9660621;urn:miriam:reactome:R-XTR-9660621;urn:miriam:reactome:R-CEL-9660621;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-RNO-9660621;urn:miriam:reactome:R-DME-9660621;urn:miriam:reactome:R-DME-9660621-3"
      hgnc "NA"
      map_id "UNIPROT:P06400"
      name "RB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_207; layout_2322"
      uniprot "UNIPROT:P06400"
    ]
    graphics [
      x 622.0950032166487
      y 1523.8272693469319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1167.765436429838
      y 596.9857388658118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9682035"
      hgnc "NA"
      map_id "TMPRSS2_space_inhibitors"
      name "TMPRSS2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1044; layout_2925"
      uniprot "NA"
    ]
    graphics [
      x 1440.0712633726055
      y 752.1594856845808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMPRSS2_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9698376;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "UNIPROT:Q8TBF4"
      name "ZCRB1; ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand); ZCRB1:m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_201; layout_202; layout_2316; layout_3236"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 1131.6875373213063
      y 614.0507685368743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-5205651;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-DDI-5205651-2;urn:miriam:reactome:R-DDI-5205651-3;urn:miriam:reactome:R-RNO-5205651;urn:miriam:reactome:R-CEL-5205651;urn:miriam:reactome:R-PFA-5205651;urn:miriam:reactome:R-BTA-5205651;urn:miriam:reactome:R-HSA-5205651;urn:miriam:reactome:R-DDI-5205651;urn:miriam:reactome:R-SCE-5205651;urn:miriam:reactome:R-SSC-5205651;urn:miriam:reactome:R-MMU-5205651;urn:miriam:reactome:R-DRE-5205651;urn:miriam:reactome:R-CFA-5205651;urn:miriam:reactome:R-SPO-5205651; urn:miriam:reactome:R-CEL-5683641;urn:miriam:reactome:R-DDI-5683641-3;urn:miriam:reactome:R-DDI-5683641-2;urn:miriam:reactome:R-RNO-5683641;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-5683641;urn:miriam:reactome:R-SPO-5683641;urn:miriam:reactome:R-CFA-5683641;urn:miriam:reactome:R-GGA-5683641;urn:miriam:reactome:R-MMU-5683641;urn:miriam:reactome:R-DRE-5683641;urn:miriam:reactome:R-DDI-5683641;urn:miriam:reactome:R-PFA-5683641;urn:miriam:reactome:R-SCE-5683641;urn:miriam:reactome:R-SSC-5683641;urn:miriam:reactome:R-BTA-5683641"
      hgnc "NA"
      map_id "UNIPROT:Q9GZQ8"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1492; layout_1507; layout_2510; layout_2513"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 584.7835558773119
      y 886.1004343871567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q99570;urn:miriam:reactome:R-CEL-5683632;urn:miriam:reactome:R-HSA-5683632;urn:miriam:reactome:R-XTR-5683632;urn:miriam:reactome:R-DME-5683632;urn:miriam:reactome:R-RNO-5683632;urn:miriam:reactome:R-SPO-5683632;urn:miriam:reactome:R-MMU-5683632;urn:miriam:reactome:R-DDI-5683632;urn:miriam:reactome:R-CFA-5683632;urn:miriam:reactome:R-GGA-5683632;urn:miriam:reactome:R-DRE-5683632;urn:miriam:reactome:R-SCE-5683632;urn:miriam:uniprot:Q14457;urn:miriam:reactome:R-SSC-5683632;urn:miriam:uniprot:Q9P2Y5;urn:miriam:uniprot:Q8NEB9"
      hgnc "NA"
      map_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      name "UVRAG_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1644; layout_2515"
      uniprot "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    ]
    graphics [
      x 842.6641960234866
      y 1081.968068557505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-XTR-917723;urn:miriam:uniprot:Q9UQN3;urn:miriam:reactome:R-DDI-917723;urn:miriam:reactome:R-DME-917723;urn:miriam:uniprot:Q8WUX9;urn:miriam:reactome:R-SSC-917723;urn:miriam:reactome:R-GGA-917723;urn:miriam:uniprot:Q96CF2;urn:miriam:reactome:R-RNO-917723;urn:miriam:uniprot:Q9Y3E7;urn:miriam:uniprot:Q9BY43;urn:miriam:reactome:R-DRE-917723;urn:miriam:reactome:R-HSA-917723;urn:miriam:reactome:R-CFA-917723;urn:miriam:reactome:R-BTA-917723;urn:miriam:uniprot:Q9H444;urn:miriam:uniprot:Q96FZ7;urn:miriam:reactome:R-SPO-917723;urn:miriam:reactome:R-MMU-917723;urn:miriam:uniprot:O43633;urn:miriam:reactome:R-SCE-917723;urn:miriam:reactome:R-CEL-917723"
      hgnc "NA"
      map_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      name "ESCRT_minus_III"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1643; layout_2514"
      uniprot "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    ]
    graphics [
      x 961.8229116765286
      y 1059.047348672934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-1181251;urn:miriam:reactome:R-HSA-1181251;urn:miriam:reactome:R-XTR-1181251;urn:miriam:reactome:R-CFA-1181251;urn:miriam:reactome:R-GGA-1181251;urn:miriam:reactome:R-MMU-1181251;urn:miriam:reactome:R-RNO-1181251;urn:miriam:reactome:R-DRE-1181251;urn:miriam:reactome:R-DDI-1181251;urn:miriam:reactome:R-PFA-1181251;urn:miriam:uniprot:P55072;urn:miriam:reactome:R-SPO-1181251;urn:miriam:reactome:R-SSC-1181251;urn:miriam:reactome:R-SCE-1181251;urn:miriam:reactome:R-DME-1181251"
      hgnc "NA"
      map_id "UNIPROT:P55072"
      name "VCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1642; layout_2497"
      uniprot "UNIPROT:P55072"
    ]
    graphics [
      x 1359.1916977716992
      y 493.880750662161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55072"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717; urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767; urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "NA; HGNC_SYMBOL:CTSL"
      map_id "UNIPROT:P07711"
      name "Cathepsin_space_L1; CTSL:CTSL_space_inhibitors; CTSL"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_837; layout_1038; layout_2492; layout_2493; sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 594.0166558240203
      y 1196.7679774651033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07711"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:kegg.compound:24848403;urn:miriam:reactome:R-ALL-9685618;urn:miriam:kegg.compound:78435478"
      hgnc "NA"
      map_id "CQ2_plus_,_space_HCQ2_plus_"
      name "CQ2_plus_,_space_HCQ2_plus_"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2030; layout_2935"
      uniprot "NA"
    ]
    graphics [
      x 1354.9744631105973
      y 754.3566323470797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ2_plus_,_space_HCQ2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1969; layout_2328"
      uniprot "NA"
    ]
    graphics [
      x 1179.3205889251967
      y 1527.625931195734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683736;urn:miriam:obo.chebi:CHEBI%3A15525"
      hgnc "NA"
      map_id "palmitoyl_minus_CoA"
      name "palmitoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_291; layout_2379"
      uniprot "NA"
    ]
    graphics [
      x 1266.0145084497458
      y 1589.478089918281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "palmitoyl_minus_CoA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-ALL-162743;urn:miriam:obo.chebi:CHEBI%3A57287; urn:miriam:obo.chebi:CHEBI%3A15346"
      hgnc "NA"
      map_id "CoA_minus_SH"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_292; layout_2380; sa71"
      uniprot "NA"
    ]
    graphics [
      x 1212.181687099312
      y 1578.4984606271175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA_minus_SH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 380.22173950679223
      y 527.3179613859588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 214.03082616128836
      y 219.71570115118527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 525.8176797424278
      y 321.2120221979852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9689912;urn:miriam:obo.chebi:CHEBI%3A26558"
      hgnc "NA"
      map_id "NMP"
      name "NMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1998; layout_2274"
      uniprot "NA"
    ]
    graphics [
      x 153.64333755441942
      y 803.1242074276689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165; urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165;urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279;UNIPROT:P63165"
      name "SUMO1:C93_minus_UBE2I"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1896; layout_3633"
      uniprot "UNIPROT:P63279;UNIPROT:P63165"
    ]
    graphics [
      x 1096.0320608623938
      y 618.300070672822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279;UNIPROT:P63165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279"
      name "UBE2I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_259; layout_3792"
      uniprot "UNIPROT:P63279"
    ]
    graphics [
      x 1282.1446930149855
      y 546.4131055255182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9682643"
      hgnc "NA"
      map_id "UNIPROT:P17844"
      name "DDX5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_204; layout_2319"
      uniprot "UNIPROT:P17844"
    ]
    graphics [
      x 848.0351366323204
      y 1181.179950583261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:MN908947.3"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1972; layout_2337"
      uniprot "NA"
    ]
    graphics [
      x 1923.6980580502536
      y 749.8071193657491
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-391423;urn:miriam:reactome:R-DDI-391423;urn:miriam:reactome:R-MMU-391423;urn:miriam:reactome:R-DME-391423;urn:miriam:reactome:R-XTR-391423;urn:miriam:reactome:R-SSC-391423;urn:miriam:reactome:R-RNO-391423;urn:miriam:reactome:R-GGA-391423;urn:miriam:reactome:R-DRE-391423;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-BTA-391423;urn:miriam:reactome:R-HSA-391423;urn:miriam:reactome:R-CFA-391423"
      hgnc "NA"
      map_id "UNIPROT:P40337"
      name "VHL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_215; layout_2325"
      uniprot "UNIPROT:P40337"
    ]
    graphics [
      x 863.4564486596188
      y 1273.8214939557365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9686811"
      hgnc "NA"
      map_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      name "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1079; layout_2955"
      uniprot "NA"
    ]
    graphics [
      x 446.3003947716595
      y 1748.4460677218353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_minus_alpha_minus_glucosidase_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682983;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:pubmed:25348530;urn:miriam:uniprot:Q13724; urn:miriam:reactome:R-HSA-9686842;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      name "ER_space_alpha_minus_glucosidases; ER_minus_alpha_space_glucosidases:ER_minus_alpha_space_glucosidase_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_280; layout_1080; layout_2373; layout_2374"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 547.2731888547986
      y 1449.1021935867705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-195906;urn:miriam:uniprot:P27824;urn:miriam:reactome:R-BTA-195906;urn:miriam:reactome:R-CFA-195906;urn:miriam:reactome:R-SPO-195906;urn:miriam:reactome:R-SSC-195906;urn:miriam:reactome:R-RNO-195906;urn:miriam:reactome:R-GGA-195906;urn:miriam:reactome:R-DRE-195906;urn:miriam:reactome:R-CEL-195906;urn:miriam:reactome:R-DDI-195906;urn:miriam:reactome:R-MMU-195906;urn:miriam:reactome:R-DME-195906;urn:miriam:reactome:R-XTR-195906;urn:miriam:reactome:R-DME-195906-2;urn:miriam:reactome:R-SCE-195906;urn:miriam:reactome:R-SPO-195906-2;urn:miriam:reactome:R-DME-195906-3;urn:miriam:reactome:R-SPO-195906-3;urn:miriam:reactome:R-DME-195906-4"
      hgnc "NA"
      map_id "UNIPROT:P27824"
      name "CANX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_288; layout_2377"
      uniprot "UNIPROT:P27824"
    ]
    graphics [
      x 869.109197512655
      y 1958.7869786170584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27824"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9693170;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "CTSL_space_inhibitors"
      name "CTSL_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2051; layout_2923"
      uniprot "NA"
    ]
    graphics [
      x 335.3932648010799
      y 1444.4831415522103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CTSL_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685933"
      hgnc "NA"
      map_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      name "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_583; layout_2457"
      uniprot "NA"
    ]
    graphics [
      x 1561.354397531541
      y 907.9269681380425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 962.2143923457182
      y 250.299808103013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 1027.7167234513136
      y 353.253665107148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694513;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685921; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694513;urn:miriam:reactome:R-COV-9685921"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      name "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1990; layout_2340"
      uniprot "NA"
    ]
    graphics [
      x 844.9735164978797
      y 655.945166631954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P49840;urn:miriam:reactome:R-HSA-198358;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49840;UNIPROT:P49841"
      name "GSK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2262; layout_2314"
      uniprot "UNIPROT:P49840;UNIPROT:P49841"
    ]
    graphics [
      x 1091.4290952623896
      y 147.56523872049593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49840;UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360; urn:miriam:obo.chebi:CHEBI%3A15846; urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "NAD_plus_"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439; layout_2443; sa3; sa222; sa256; sa108; sa45"
      uniprot "NA"
    ]
    graphics [
      x 860.9627951526081
      y 603.1182877810013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CFA-8938273;urn:miriam:uniprot:Q53GL7;urn:miriam:reactome:R-HSA-8938273;urn:miriam:reactome:R-XTR-8938273;urn:miriam:uniprot:Q2NL67;urn:miriam:reactome:R-DDI-8938273;urn:miriam:uniprot:Q8N3A8;urn:miriam:reactome:R-GGA-8938273;urn:miriam:reactome:R-RNO-8938273;urn:miriam:reactome:R-MMU-8938273;urn:miriam:reactome:R-BTA-8938273;urn:miriam:reactome:R-DRE-8938273;urn:miriam:reactome:R-DME-8938259;urn:miriam:uniprot:Q8N5Y8;urn:miriam:uniprot:Q460N5;urn:miriam:uniprot:Q8IXQ6;urn:miriam:uniprot:Q9UKK3;urn:miriam:reactome:R-SSC-8938273"
      hgnc "NA"
      map_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      name "PARPs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_914; layout_2446"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    ]
    graphics [
      x 797.5190152463155
      y 640.5138403365622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277; urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "NAM"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_716; layout_2444; sa221; sa253"
      uniprot "NA"
    ]
    graphics [
      x 827.4693463191728
      y 561.6730803982189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-964750;urn:miriam:reactome:R-XTR-964750;urn:miriam:reactome:R-GGA-964750;urn:miriam:reactome:R-DME-964750;urn:miriam:reactome:R-CEL-964750;urn:miriam:reactome:R-DDI-964750;urn:miriam:uniprot:P26572;urn:miriam:reactome:R-BTA-964750;urn:miriam:reactome:R-CFA-964750;urn:miriam:reactome:R-DRE-964750;urn:miriam:reactome:R-RNO-964750;urn:miriam:reactome:R-SSC-964750;urn:miriam:reactome:R-HSA-964750"
      hgnc "NA"
      map_id "UNIPROT:P26572"
      name "MGAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_320; layout_2390"
      uniprot "UNIPROT:P26572"
    ]
    graphics [
      x 640.7809721978243
      y 1346.3970323420388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P26572"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078; urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659; urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "UDP"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319; layout_2389; sa22; sa35"
      uniprot "NA"
    ]
    graphics [
      x 1020.9555085315105
      y 753.8590132737166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16846;urn:miriam:reactome:R-ALL-9683025"
      hgnc "NA"
      map_id "UDP_minus_GalNAc"
      name "UDP_minus_GalNAc"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_344; layout_2394"
      uniprot "NA"
    ]
    graphics [
      x 993.4826702517194
      y 812.4226009206828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus_GalNAc"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q10472;urn:miriam:reactome:R-HSA-9682906"
      hgnc "NA"
      map_id "UNIPROT:Q10472"
      name "GALNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_346; layout_2396"
      uniprot "UNIPROT:Q10472"
    ]
    graphics [
      x 1148.4750393463355
      y 857.7032630195894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10472"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685917;urn:miriam:reactome:R-COV-9694622;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694622;urn:miriam:reactome:R-COV-9685917"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1971; layout_2334"
      uniprot "NA"
    ]
    graphics [
      x 1176.6105885733118
      y 1268.9641814970878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15903;urn:miriam:reactome:R-ALL-9683087"
      hgnc "NA"
      map_id "beta_minus_D_minus_glucose"
      name "beta_minus_D_minus_glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_278; layout_2372"
      uniprot "NA"
    ]
    graphics [
      x 730.6272124298903
      y 1347.465727283504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "beta_minus_D_minus_glucose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1970; layout_2331"
      uniprot "NA"
    ]
    graphics [
      x 1624.2977762647865
      y 1229.8402670263395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9699093;urn:miriam:obo.chebi:CHEBI%3A16556"
      hgnc "NA"
      map_id "CMP_minus_Neu5Ac"
      name "CMP_minus_Neu5Ac"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2068; layout_3350"
      uniprot "NA"
    ]
    graphics [
      x 845.2635917133734
      y 1322.9007275939143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP_minus_Neu5Ac"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q16842;urn:miriam:reactome:R-HSA-9683042;urn:miriam:uniprot:Q11201;urn:miriam:uniprot:Q11203;urn:miriam:uniprot:Q9UJ37;urn:miriam:uniprot:Q9H4F1;urn:miriam:uniprot:P15907;urn:miriam:uniprot:Q8NDV1;urn:miriam:uniprot:Q11206"
      hgnc "NA"
      map_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      name "sialyltransferases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_351; layout_2401"
      uniprot "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    ]
    graphics [
      x 879.67330030298
      y 1469.7134114436499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094; urn:miriam:obo.chebi:CHEBI%3A17361"
      hgnc "NA"
      map_id "CMP"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2069; layout_3351; sa34"
      uniprot "NA"
    ]
    graphics [
      x 800.1546320602577
      y 1292.9847750052998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694471;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_345"
      name "Polyadenylation of SARS-CoV-2 genomic RNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694471__layout_2306"
      uniprot "NA"
    ]
    graphics [
      x 741.9510835894314
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694619;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9684636"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2305"
      uniprot "NA"
    ]
    graphics [
      x 720.965486661949
      y 174.81475712004192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685518;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2277; layout_2163; layout_3664"
      uniprot "NA"
    ]
    graphics [
      x 731.495152980287
      y 255.56260341431846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694737;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_395"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694737__layout_2297"
      uniprot "NA"
    ]
    graphics [
      x 345.21990389501684
      y 346.2951553736136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 47
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713644;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713311; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9713643;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713313;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9681663;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694725; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694255;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9680476; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713306;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713634;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:reactome:R-COV-9686016;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694302;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694458;urn:miriam:reactome:R-COV-9681659; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713317;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713652; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694479; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713302;urn:miriam:reactome:R-COV-9713651; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694366;urn:miriam:reactome:R-COV-9686003; urn:miriam:reactome:R-COV-9694770;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9686011; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694728; urn:miriam:reactome:R-COV-9682253;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694404;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684343;urn:miriam:reactome:R-COV-9694407; urn:miriam:reactome:R-COV-9694375;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32541865;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:pubmed:32272481; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694740;urn:miriam:reactome:R-COV-9683403; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32511376;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:pubmed:32709886;urn:miriam:reactome:R-COV-9694416;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:pubmed:32838362;urn:miriam:pubmed:34131072; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682469;urn:miriam:reactome:R-COV-9694618; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694327;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9687385; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694784;urn:miriam:reactome:R-COV-9684866; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210; urn:miriam:reactome:R-COV-9694760;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682222; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684877;urn:miriam:reactome:R-COV-9694428; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694735;urn:miriam:reactome:R-COV-9682205; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:33208736;urn:miriam:reactome:R-COV-9694717; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694577;urn:miriam:reactome:R-COV-9684876; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694358;urn:miriam:reactome:R-COV-9682215; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694688;urn:miriam:reactome:R-COV-9682545; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694731;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9682668;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691351; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691348;urn:miriam:pubmed:32277040; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691364;urn:miriam:pubmed:32277040; urn:miriam:reactome:R-COV-9694361;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682227; urn:miriam:reactome:R-COV-9682451;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694597; urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694748; urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682229; urn:miriam:reactome:R-COV-9694286;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694269;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682566; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691334"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      name "SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC; SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer; SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; RTC; SARS_minus_CoV_minus_2_space_gRNA:RTC; SARS_minus_CoV_minus_2_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC; SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; nsp3_minus_4; N_minus_glycan_space_nsp3_minus_4; SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; m7GpppA_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; nsp3:nsp4; nsp6; nsp3:nsp4:nsp6; SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors; 3CLp_space_dimer; 3CLp_space_dimer:3CLp_space_inhibitors; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15; nsp16:nsp10; SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand; SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors; nsp1_minus_4; nsp1; nsp2; nsp3; N_minus_glycan_space_nsp3; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13; nsp4; nsp10; nsp10:nsp14; SARS_space_coronavirus_space_2_space_gRNA_space_with_space_secondary_space_structure:RTC; N_minus_glycan_space_nsp4; nsp8; nsp7:nsp8; nsp7:nsp8:nsp12; nsp7:nsp8:nsp12:nsp14:nsp10; nsp5; nsp9; nsp9_space_tetramer; SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide; nsp7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3381; layout_3386; layout_2262; layout_2268; layout_3366; layout_2251; layout_2256; layout_3408; layout_3361; layout_2206; layout_2212; layout_3410; layout_3417; layout_2448; layout_2449; layout_2450; layout_2291; layout_2296; layout_2176; layout_2179; layout_2247; layout_2249; layout_2266; layout_2279; layout_2204; layout_2205; layout_2207; layout_2216; layout_2220; layout_2243; layout_2217; layout_2238; layout_2239; layout_2253; layout_2225; layout_2559; layout_2565; layout_2576; layout_2508; layout_2222; layout_2241; layout_2175; layout_2229; layout_2230; layout_3098; layout_2270; layout_2562"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 416.7574439112954
      y 454.3367832407102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694300;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683625; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729324; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9684230;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729277;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9729264;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9684199;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694612; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729335;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694659; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729308;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC9; urn:miriam:uniprot:P0DTC9; urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9; urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P0DTC9"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; N_space_tetramer; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand); N; p_minus_S188,206_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S,2T_minus_N; p_minus_11S,2T_minus_N; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S_minus_N; ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; p_minus_11S,2T_minus_metR95,177_minus_N; nucleocapsid_br_protein; Nucleocapsid; Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_3639; layout_2352; layout_2496; layout_3571; layout_2364; layout_2341; layout_3558; layout_2343; layout_2465; layout_3542; layout_3528; layout_2353; layout_2467; layout_3574; layout_2361; layout_2362; layout_3616; layout_3529; layout_2960; ff3a1; b0f05; b6d08; b9df2; sa1685; sa1887; csa365; csa369; csa374; csa398; sa1667; csa353; csa357; csa391; csa389; csa387; csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1064.0049536582296
      y 301.29170648855734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694363;PUBMED:32654247;PUBMED:33794152;PUBMED:33594360;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_323"
      name "Tetramerisation of nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694363__layout_3642"
      uniprot "NA"
    ]
    graphics [
      x 1151.95417897509
      y 188.5427066993144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP5038; WP5039; C19DMap:Virus replication cycle; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5; urn:miriam:uniprot:P0DTC5; urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "UNIPROT:P0DTC5"
      name "nascent_space_M; M; N_minus_glycan_space_M; M_space_lattice; membrane_br_glycoprotein; Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2338; layout_2416; layout_2419; layout_2463; layout_2469; d614c; d1b58; cedc7; sa2061; sa2116; sa2024; sa2066; sa1686; sa1891; sa1857; sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1617.9383145884467
      y 715.4025521409974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694555;PUBMED:32198291;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_364"
      name "Protein M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694555__layout_2415"
      uniprot "NA"
    ]
    graphics [
      x 1641.8417448285904
      y 855.7703650662504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694541;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:33264556;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_359"
      name "SARS-CoV-2 gRNA:RTC:RNA primer binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694541__layout_2502"
      uniprot "NA"
    ]
    graphics [
      x 146.63380292846932
      y 426.633954102102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 17
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3"
      name "O_minus_glycosyl_space_3a_space_tetramer; O_minus_glycosyl_space_3a; 3a; 3a:membranous_space_structure; GalNAc_minus_O_minus_3a; ORF3a; Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2458; layout_2461; layout_2399; layout_2403; layout_2332; layout_2392; layout_2459; layout_2455; layout_2395; cb0cc; ac4ba; ed8aa; sa1873; sa2247; sa1; sa169; sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1298.9704495019641
      y 1066.0635398318163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694727;PUBMED:19398035;PUBMED:15194747;PUBMED:16212942"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_392"
      name "Endocytosis of protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694727__layout_2460"
      uniprot "NA"
    ]
    graphics [
      x 1337.9077850082977
      y 959.085521033365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_392"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694585;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9684225;urn:miriam:reactome:R-COV-9694324;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9752958;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685539; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694500;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2490; layout_2478; layout_3798; layout_2482; layout_2485"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1174.59861022013
      y 944.7761568962121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694689;PUBMED:26311884"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_388"
      name "Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694689__layout_2494"
      uniprot "NA"
    ]
    graphics [
      x 1226.0487079478144
      y 563.9710167239257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694534;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9686705;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M:E:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2495"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1326.5760686643519
      y 426.01346867689483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 979.1259860766107
      y 1649.8530093684906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694656;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_382"
      name "Spike trimer glycoside chains are extended"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694656__layout_2386"
      uniprot "NA"
    ]
    graphics [
      x 754.454576346894
      y 1523.507958756501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-RNO-975823;urn:miriam:reactome:R-CFA-975823;urn:miriam:reactome:R-GGA-975823;urn:miriam:uniprot:Q16706;urn:miriam:reactome:R-HSA-975823;urn:miriam:reactome:R-DRE-975823;urn:miriam:reactome:R-XTR-975823;urn:miriam:reactome:R-MMU-975823;urn:miriam:reactome:R-SSC-975823"
      hgnc "NA"
      map_id "UNIPROT:Q16706"
      name "MAN2A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2905"
      uniprot "UNIPROT:Q16706"
    ]
    graphics [
      x 611.7603616019533
      y 1586.878284538298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16706"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-975830;urn:miriam:reactome:R-RNO-975830;urn:miriam:reactome:R-GGA-975830;urn:miriam:reactome:R-DRE-975830;urn:miriam:reactome:R-DME-975830;urn:miriam:reactome:R-MMU-975830;urn:miriam:reactome:R-CFA-975830;urn:miriam:reactome:R-BTA-975830;urn:miriam:reactome:R-HSA-975830;urn:miriam:reactome:R-XTR-975830;urn:miriam:uniprot:Q10469"
      hgnc "NA"
      map_id "UNIPROT:Q10469"
      name "MGAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2906"
      uniprot "UNIPROT:Q10469"
    ]
    graphics [
      x 679.8376245895302
      y 1438.5482685843963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10469"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694681;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_387"
      name "mRNA5 is translated to protein M"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694681__layout_2336"
      uniprot "NA"
    ]
    graphics [
      x 1807.547996054102
      y 694.9151668077413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698265;PUBMED:31231549;PUBMED:15331731;PUBMED:21799305;PUBMED:25135833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_405"
      name "Enhanced autophagosome formation"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698265__layout_3102"
      uniprot "NA"
    ]
    graphics [
      x 787.473729029808
      y 964.8257272953954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_405"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 15
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681986; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681994; urn:miriam:reactome:R-COV-9684311;urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694778; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681991; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681992; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681990; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681995; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681983; urn:miriam:uniprot:P0DTC1;urn:miriam:pubmed:32169673; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1"
      name "pp1a_minus_nsp6_minus_11; pp1a; pp1a_space_dimer; pp1a_minus_3CL; pp1a_minus_nsp1_minus_4; pp1a_minus_nsp9; pp1a_minus_nsp8; pp1a_minus_nsp11; pp1a_minus_nsp7; pp1a_minus_nsp6; pp1a_minus_nsp10; orf1a; pp1a_space_Nsp6_minus_11; pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2172; layout_2719; layout_2168; layout_2547; layout_2208; layout_2546; layout_2542; layout_2543; layout_2544; layout_2184; layout_2545; eb8ab; sa2224; sa1789; sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 692.9083709325877
      y 657.2785916042137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9694516; WP5038; WP5039; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2; urn:miriam:uniprot:P0DTD2; urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "UNIPROT:P0DTD2"
      name "9b_space_homodimer; ORF9b; PLpro; Orf9b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_3481; e62df; bca7b; f4882; sa139; sa1878; sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 903.1535335534553
      y 1080.806468503561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694662;PUBMED:32587976"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_384"
      name "Protein 3a forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694662__layout_2402"
      uniprot "NA"
    ]
    graphics [
      x 1405.935310922099
      y 990.3113988780216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694785;urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-HSA-9694758;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-HSA-9686692"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2; S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2971; layout_2972"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 976.2224855015272
      y 884.4252394645453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697423;PUBMED:15163706;PUBMED:15010527;PUBMED:22816037;PUBMED:15140961"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_404"
      name "Endocytosis of SARS-CoV-2 Virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697423__layout_2970"
      uniprot "NA"
    ]
    graphics [
      x 823.7784065301245
      y 882.6487547616489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_404"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694521;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_353"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694521__layout_2288"
      uniprot "NA"
    ]
    graphics [
      x 601.0657085644957
      y 308.09080817838503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685513;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694603"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2289"
      uniprot "NA"
    ]
    graphics [
      x 736.366730389298
      y 516.4589641599579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694334;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_317"
      name "mRNA1 is translated to pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694334__layout_2162"
      uniprot "NA"
    ]
    graphics [
      x 814.2124044547286
      y 464.74746498225795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9699007;PUBMED:32532959;PUBMED:32362314;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_407"
      name "FURIN Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9699007__layout_3308"
      uniprot "NA"
    ]
    graphics [
      x 1007.9175892976896
      y 993.6954707297613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_407"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-CFA-2159856;urn:miriam:reactome:R-DRE-2159856;urn:miriam:reactome:R-HSA-2159856;urn:miriam:reactome:R-XTR-2159856;urn:miriam:reactome:R-GGA-2159856;urn:miriam:pubmed:1629222;urn:miriam:uniprot:P09958;urn:miriam:reactome:R-MMU-2159856;urn:miriam:reactome:R-SSC-2159856;urn:miriam:reactome:R-RNO-2159856; urn:miriam:uniprot:P09958;urn:miriam:pubmed:32376634; urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "NA; HGNC_SYMBOL:FURIN"
      map_id "UNIPROT:P09958"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3309; ee829; sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 883.3858405809069
      y 896.5357699739018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09958"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694344;PUBMED:12917450;PUBMED:16928755;PUBMED:12927536;PUBMED:14569023;PUBMED:25736566;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_321"
      name "Synthesis of SARS-CoV-2 minus strand subgenomic mRNAs by template switching"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694344__layout_2422"
      uniprot "NA"
    ]
    graphics [
      x 664.1887047441517
      y 154.48989815871516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694492;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_348"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694492__layout_2280"
      uniprot "NA"
    ]
    graphics [
      x 311.189806032932
      y 290.32737040423774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694331;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_315"
      name "nsp3-4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694331__layout_2209"
      uniprot "NA"
    ]
    graphics [
      x 740.1581941504265
      y 364.93530291097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694790;PUBMED:16684538;PUBMED:20129637"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_397"
      name "E protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694790__layout_2404"
      uniprot "NA"
    ]
    graphics [
      x 1424.3133677518135
      y 658.8423755160267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP4861; WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC4; urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P0DTC4"
      name "nascent_space_E; N_minus_glycan_space_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; 3xPalmC_minus_E; envelope_br_protein; SARS_space_E; Envelope_space_Protein_space_E; E"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_2335; layout_2405; layout_2410; layout_2412; layout_2407; layout_2414; aa466; fce54; c7c21; sa2062; sa2115; sa2023; sa2065; sa1687; sa1892; sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1386.3961038125378
      y 1060.377659837516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694780;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_396"
      name "Spike trimer translocates to ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694780__layout_2384"
      uniprot "NA"
    ]
    graphics [
      x 989.7031805860755
      y 1776.1325242478497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694406;PUBMED:19224332"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_333"
      name "nsp13 binds DDX5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694406__layout_2318"
      uniprot "NA"
    ]
    graphics [
      x 674.7848005935007
      y 1104.0451922418304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 477.2547222488326
      y 980.9759441613486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:19224332;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-HSA-9682634;urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9694692"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P17844"
      name "nsp13:DDX5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3082"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P17844"
    ]
    graphics [
      x 775.6745614905526
      y 1126.9674089694468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P17844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694476;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_346"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694476__layout_2429"
      uniprot "NA"
    ]
    graphics [
      x 370.3377762729716
      y 272.87820325303494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694452;PUBMED:15522242;PUBMED:22819936"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_341"
      name "Protein E forms a homopentamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694452__layout_2411"
      uniprot "NA"
    ]
    graphics [
      x 1471.6257614387837
      y 1180.6053665584895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694568;PUBMED:17210170"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_366"
      name "Nucleoprotein translocates to the ERGIC outer membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694568__layout_2363"
      uniprot "NA"
    ]
    graphics [
      x 1152.6362364068802
      y 126.81490355227993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694794;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_400"
      name "mRNA3 is translated to protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694794__layout_2330"
      uniprot "NA"
    ]
    graphics [
      x 1521.6957899504005
      y 1147.8028857583536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729330;PUBMED:12134018;PUBMED:32817937;PUBMED:32637943;PUBMED:33248025"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_414"
      name "SRPK1/2 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729330__layout_3564"
      uniprot "NA"
    ]
    graphics [
      x 1202.9736018222193
      y 333.56197710631557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:12134018;urn:miriam:uniprot:P78362;urn:miriam:reactome:R-HSA-9729312;urn:miriam:pubmed:12565829;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q96SB4"
      hgnc "NA"
      map_id "UNIPROT:P78362;UNIPROT:Q96SB4"
      name "SRPK1_slash_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3791"
      uniprot "UNIPROT:P78362;UNIPROT:Q96SB4"
    ]
    graphics [
      x 1273.3338387293986
      y 462.85105673228844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78362;UNIPROT:Q96SB4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694553;PUBMED:24418573;PUBMED:22238235;PUBMED:17166901;PUBMED:27145752;PUBMED:25855243;PUBMED:20580052;PUBMED:20007283;PUBMED:18792806;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_363"
      name "Recruitment of Spike trimer to assembling virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694553__layout_2473"
      uniprot "NA"
    ]
    graphics [
      x 1108.3494363777331
      y 1402.8966955780622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684226;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694491;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "M_space_lattice:E_space_protein:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2472"
      uniprot "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1210.721899861841
      y 1106.3293728958404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9686310;urn:miriam:reactome:R-COV-9694321;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2475"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1215.578503748768
      y 1345.0257944489067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694367;PUBMED:33203855;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_325"
      name "Glycosylated M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694367__layout_2462"
      uniprot "NA"
    ]
    graphics [
      x 1736.8962895108257
      y 761.8044175951219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694630;PUBMED:18367524"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_378"
      name "Nsp3:nsp4 binds to nsp6"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694630__layout_2447"
      uniprot "NA"
    ]
    graphics [
      x 411.6410565091448
      y 569.3794587551577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694419;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_334"
      name "SARS-CoV-2 gRNA complement (minus strand):RTC binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694419__layout_2505"
      uniprot "NA"
    ]
    graphics [
      x 178.75162763574895
      y 509.57854650584767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694592;PUBMED:32045235;PUBMED:33452205;PUBMED:32541865;PUBMED:33984267;PUBMED:33062953;PUBMED:32272481;PUBMED:32374457;PUBMED:32896566;PUBMED:33152262;PUBMED:32737471;PUBMED:34726479;PUBMED:15507456;PUBMED:33574416"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_373"
      name "3CLp dimer binds 3CLp inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694592__layout_2452"
      uniprot "NA"
    ]
    graphics [
      x 212.46999804378663
      y 437.94964755482295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:reactome:R-ALL-9731705"
      hgnc "NA"
      map_id "3CLp_space_inhibitors"
      name "3CLp_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_3674"
      uniprot "NA"
    ]
    graphics [
      x 150.04406291238706
      y 579.0611318430409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "3CLp_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694377;PUBMED:15788388;PUBMED:21203998;PUBMED:27799534"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_328"
      name "pp1a cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694377__layout_2169"
      uniprot "NA"
    ]
    graphics [
      x 537.5253638717379
      y 659.8124961048684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694576;PUBMED:18827877;PUBMED:18255185"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_369"
      name "nsp3 binds to nsp7-8 and nsp12-16"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694576__layout_2451"
      uniprot "NA"
    ]
    graphics [
      x 265.1039308277294
      y 524.6985941678251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694633;PUBMED:16877062;PUBMED:9658133;PUBMED:10799570;PUBMED:31133031;PUBMED:16254320;PUBMED:25855243;PUBMED:18792806"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_380"
      name "SARS virus buds into ERGIC lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694633__layout_3799"
      uniprot "NA"
    ]
    graphics [
      x 1073.7095964795785
      y 980.3798519577477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697018;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_403"
      name "Addition of sialic acids on some Spike glycosyl sidechains"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697018__layout_3054"
      uniprot "NA"
    ]
    graphics [
      x 866.6006153216351
      y 1616.6001090066652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_403"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694373;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_327"
      name "Unphosphorylated nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694373__layout_2342"
      uniprot "NA"
    ]
    graphics [
      x 1221.3225753326597
      y 252.98290173058035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694529;PUBMED:20409569"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_357"
      name "Ubiquination of protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694529__layout_2408"
      uniprot "NA"
    ]
    graphics [
      x 1395.58987816723
      y 1231.4722872216228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694665;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_385"
      name "SARS-CoV-2 gRNA:RTC:nascent RNA minus strand binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694665__layout_2504"
      uniprot "NA"
    ]
    graphics [
      x 284.5057113773911
      y 608.3281875350588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694455;PUBMED:24418573;PUBMED:17002283;PUBMED:15147189;PUBMED:17379242;PUBMED:15849181;PUBMED:18456656;PUBMED:15094372;PUBMED:19052082;PUBMED:16214138;PUBMED:16103198;PUBMED:15020242;PUBMED:18561946;PUBMED:23717688;PUBMED:17881296;PUBMED:18631359;PUBMED:16228284"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_343"
      name "SUMO-p-N protein dimer binds genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694455__layout_2464"
      uniprot "NA"
    ]
    graphics [
      x 904.9145018744707
      y 196.0533555439847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696807;PUBMED:32366695;PUBMED:18003979"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_401"
      name "N-glycan mannose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696807__layout_3043"
      uniprot "NA"
    ]
    graphics [
      x 1097.316817559224
      y 1795.901496407831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_401"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q9BV94;urn:miriam:reactome:R-HSA-6782581;urn:miriam:uniprot:Q9UKM7"
      hgnc "NA"
      map_id "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
      name "MAN1B1,EDEM2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3044"
      uniprot "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
    ]
    graphics [
      x 1245.5969996514334
      y 1794.3681952036843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694392;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_331"
      name "3a translocates to the ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694392__layout_2391"
      uniprot "NA"
    ]
    graphics [
      x 1437.6306802147533
      y 1091.0633381849975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694641;PUBMED:31226023;PUBMED:16877062;PUBMED:25855243"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_381"
      name "Viral release"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694641__layout_2481"
      uniprot "NA"
    ]
    graphics [
      x 1105.0038728035045
      y 1044.7354196761994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694661;PUBMED:32532959;PUBMED:20926566;PUBMED:21325420;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_383"
      name "TMPRSS2 Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694661__layout_2498"
      uniprot "NA"
    ]
    graphics [
      x 1193.9241974782585
      y 815.1307295690161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729300;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_411"
      name "Unknown kinase phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729300__layout_3541"
      uniprot "NA"
    ]
    graphics [
      x 1142.383604180964
      y 306.912676132583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_411"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694575;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_368"
      name "Nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694575__layout_2351"
      uniprot "NA"
    ]
    graphics [
      x 1052.1146327921824
      y 109.17248141463358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694308;PUBMED:16352545"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_313"
      name "3a is externalized together with membrane structures"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694308__layout_2456"
      uniprot "NA"
    ]
    graphics [
      x 1484.4508343692964
      y 994.6275799952942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694447;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_340"
      name "mRNA2 is translated to Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694447__layout_2327"
      uniprot "NA"
    ]
    graphics [
      x 1075.8429858379955
      y 1601.6059294008026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694732;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_393"
      name "3CLp cleaves pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694732__layout_2188"
      uniprot "NA"
    ]
    graphics [
      x 327.7409316829493
      y 706.4135046883252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694436;PUBMED:32304108;PUBMED:32803198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_335"
      name "nsp15 forms a hexamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694436__layout_2244"
      uniprot "NA"
    ]
    graphics [
      x 455.5447689917911
      y 1135.3356652105372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694274;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_306"
      name "mRNA1 is translated to pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694274__layout_2165"
      uniprot "NA"
    ]
    graphics [
      x 617.7884471323076
      y 649.5431206633814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694401;PUBMED:22548323;PUBMED:16507314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_332"
      name "E protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694401__layout_2406"
      uniprot "NA"
    ]
    graphics [
      x 1338.1045335850827
      y 1422.7810129064083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694338;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_319"
      name "nsp1-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694338__layout_2202"
      uniprot "NA"
    ]
    graphics [
      x 522.0848509588743
      y 591.885163144626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694389;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_329"
      name "nsp3 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694389__layout_2219"
      uniprot "NA"
    ]
    graphics [
      x 782.4733397848735
      y 402.13785152590464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694506;PUBMED:12917450;PUBMED:12927536;PUBMED:32330414;PUBMED:14569023;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_351"
      name "Synthesis of SARS-CoV-2 plus strand subgenomic mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694506__layout_2427"
      uniprot "NA"
    ]
    graphics [
      x 357.2616557730621
      y 175.12132392449848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694539;PUBMED:28143984;PUBMED:16828802;PUBMED:16882730;PUBMED:18045871;PUBMED:16216269;PUBMED:17409150;PUBMED:18255185;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_358"
      name "nsp15 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694539__layout_2246"
      uniprot "NA"
    ]
    graphics [
      x 346.4458931132235
      y 784.2153652975093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694444;PUBMED:16684538;PUBMED:18703211;PUBMED:19322648;PUBMED:16254320;PUBMED:15474033;PUBMED:15147946;PUBMED:18792806;PUBMED:31226023;PUBMED:16877062;PUBMED:16507314;PUBMED:17530462;PUBMED:15713601;PUBMED:15351485;PUBMED:31133031;PUBMED:25855243;PUBMED:18753196;PUBMED:16343974;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_338"
      name "E and N are recruited to the M lattice"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694444__layout_2470"
      uniprot "NA"
    ]
    graphics [
      x 1321.1351057546244
      y 803.7834714728804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694337;PUBMED:22915798"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_318"
      name "Trimmed spike protein binds to calnexin"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694337__layout_2375"
      uniprot "NA"
    ]
    graphics [
      x 915.2448706604084
      y 1840.5792073168213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729260;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_408"
      name "GSK3 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729260__layout_3598"
      uniprot "NA"
    ]
    graphics [
      x 1119.4426569416612
      y 245.69963961174005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_408"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686790;PUBMED:16188993;PUBMED:24716661;PUBMED:23816430;PUBMED:19223639;PUBMED:23503623;PUBMED:7986008"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_300"
      name "ER-alpha glucosidases bind ER-alpha glucosidase inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686790__layout_2954"
      uniprot "NA"
    ]
    graphics [
      x 486.6438957929441
      y 1626.4195995084667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694287;PUBMED:22816037;PUBMED:19321428"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_310"
      name "Cleavage of S protein into S1:S2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694287__layout_2489"
      uniprot "NA"
    ]
    graphics [
      x 901.4670177272044
      y 1003.6754739229109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694345;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_322"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694345__layout_2360"
      uniprot "NA"
    ]
    graphics [
      x 1213.5373374614387
      y 187.58234552000704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694601;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_374"
      name "nsp3-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694601__layout_2214"
      uniprot "NA"
    ]
    graphics [
      x 465.51143727521475
      y 705.1389667808403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694572;PUBMED:19398035;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_367"
      name "3a localizes to the cell membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694572__layout_2454"
      uniprot "NA"
    ]
    graphics [
      x 1374.759762789134
      y 1154.3151694214962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694281;PUBMED:24418573;PUBMED:23717688;PUBMED:17229691;PUBMED:17379242;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_309"
      name "Encapsidation of SARS coronavirus genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694281__layout_2466"
      uniprot "NA"
    ]
    graphics [
      x 1021.1112675223169
      y 169.09111764071463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694542;PUBMED:25197083;PUBMED:32838362;PUBMED:22635272;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_360"
      name "nsp14 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694542__layout_2237"
      uniprot "NA"
    ]
    graphics [
      x 522.0381596292007
      y 815.4383958752363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694277;PUBMED:17024178;PUBMED:22039154"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_307"
      name "nsp8 generates RNA primers"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694277__layout_2260"
      uniprot "NA"
    ]
    graphics [
      x 396.98835165326545
      y 123.77635191871912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694467;PUBMED:32587972"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_344"
      name "Spike protein forms a homotrimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694467__layout_2382"
      uniprot "NA"
    ]
    graphics [
      x 897.1908320425266
      y 1743.1571317771316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694454;PUBMED:32358203;PUBMED:32526208;PUBMED:32438371;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_342"
      name "Replication transcription complex binds SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694454__layout_2250"
      uniprot "NA"
    ]
    graphics [
      x 486.4145291476963
      y 337.05867863777064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694611;PUBMED:17855519"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_376"
      name "nsp4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694611__layout_2223"
      uniprot "NA"
    ]
    graphics [
      x 828.2483248018308
      y 381.93226850207986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694520;PUBMED:25732088"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_352"
      name "nsp16 binds VHL"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694520__layout_2324"
      uniprot "NA"
    ]
    graphics [
      x 705.6037704625696
      y 1236.0140040050337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694398;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-HSA-9683453;urn:miriam:pubmed:25732088"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P40337"
      name "nsp16:VHL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3093"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P40337"
    ]
    graphics [
      x 826.3532582379776
      y 1372.8392492549663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P40337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691363;PUBMED:32358203;PUBMED:32438371;PUBMED:31138817;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_303"
      name "nsp12 binds nsp7 and nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691363__layout_2574"
      uniprot "NA"
    ]
    graphics [
      x 413.0748963297199
      y 792.5069712780969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694605;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_375"
      name "nsp12 synthesizes minus strand SARS-CoV-2 genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694605__layout_2264"
      uniprot "NA"
    ]
    graphics [
      x 418.9903127899949
      y 186.6498890685067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694317;PUBMED:20542253;PUBMED:24991833;PUBMED:28738245;PUBMED:23943763"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_314"
      name "Nsp3, nsp4, and nsp6 produce replicative organelles"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694317__layout_2507"
      uniprot "NA"
    ]
    graphics [
      x 316.0098363681112
      y 527.7759605427336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694718;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_389"
      name "O-glycosylation of 3a is terminated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694718__layout_2397"
      uniprot "NA"
    ]
    graphics [
      x 1006.5775965579331
      y 1294.8264250747445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_389"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694625;PUBMED:12917450;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_377"
      name "nsp3 cleaves nsp1-4"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694625__layout_2221"
      uniprot "NA"
    ]
    graphics [
      x 282.77509100988505
      y 413.29355959509223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729307;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_412"
      name "Nucleoprotein is SUMOylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729307__layout_3638"
      uniprot "NA"
    ]
    graphics [
      x 1175.720914468043
      y 463.7059270223728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694677;PUBMED:22362731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_386"
      name "ZCRB1 binds 5'UTR of SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694677__layout_2315"
      uniprot "NA"
    ]
    graphics [
      x 973.3455372762087
      y 426.2134878069788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_386"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694370;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_326"
      name "mRNA9a is translated to Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694370__layout_2339"
      uniprot "NA"
    ]
    graphics [
      x 920.7272768879329
      y 479.6291803007218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694721;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_390"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694721__layout_2304"
      uniprot "NA"
    ]
    graphics [
      x 586.3789803105659
      y 354.55190717879896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694445;PUBMED:20699222;PUBMED:22022266;PUBMED:32511376;PUBMED:32709886;PUBMED:32838362;PUBMED:16873246;PUBMED:20421945;PUBMED:16873247;PUBMED:34131072;PUBMED:25074927;PUBMED:26041293;PUBMED:21637813;PUBMED:21393853;PUBMED:22635272"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_339"
      name "nsp16 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694445__layout_2248"
      uniprot "NA"
    ]
    graphics [
      x 460.271971656269
      y 883.2904743670856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-ALL-1500648"
      hgnc "NA"
      map_id "Mg2_plus__slash_Mn2_plus_"
      name "Mg2_plus__slash_Mn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3797"
      uniprot "NA"
    ]
    graphics [
      x 544.215619586146
      y 1058.2970766490237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mg2_plus__slash_Mn2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694528;PUBMED:16840309;PUBMED:15807784;PUBMED:16894145;PUBMED:15781262;PUBMED:23202509;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_356"
      name "Accessory proteins are recruited to the maturing virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694528__layout_2476"
      uniprot "NA"
    ]
    graphics [
      x 1227.5850769120166
      y 1179.954821858786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9694750;urn:miriam:uniprot:P0DTC7; urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "UNIPROT:P0DTC7"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2477; sa1875; sa2060; sa2114; sa2025; sa2067; sa2245; sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1337.0520921522468
      y 1279.763353823842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-449651;urn:miriam:obo.chebi:CHEBI%3A53019"
      hgnc "NA"
      map_id "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      name "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2901"
      uniprot "NA"
    ]
    graphics [
      x 1347.9311408060012
      y 1683.4590722402754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694793;PUBMED:11470266;PUBMED:32366695;PUBMED:32363391;PUBMED:32676595;PUBMED:32518941"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_399"
      name "Spike protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694793__layout_2365"
      uniprot "NA"
    ]
    graphics [
      x 1198.4677784647847
      y 1732.8982835967322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P61803;urn:miriam:uniprot:P39656;urn:miriam:uniprot:Q13454;urn:miriam:uniprot:P46977;urn:miriam:reactome:R-HSA-532516;urn:miriam:uniprot:Q9H0U3;urn:miriam:uniprot:P04843;urn:miriam:uniprot:P04844"
      hgnc "NA"
      map_id "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
      name "OST_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2907"
      uniprot "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
    ]
    graphics [
      x 1300.674693531219
      y 1651.222756976289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16214;urn:miriam:reactome:R-ALL-449311"
      hgnc "NA"
      map_id "DOLP"
      name "DOLP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2902"
      uniprot "NA"
    ]
    graphics [
      x 1345.2434262448485
      y 1749.3723156219999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DOLP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698988;PUBMED:21068237;PUBMED:21325420;PUBMED:20926566;PUBMED:15474033;PUBMED:32532959;PUBMED:32362314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_406"
      name "Direct Host Cell Membrane Membrane Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698988__layout_3336"
      uniprot "NA"
    ]
    graphics [
      x 1042.1486061974338
      y 689.2905345598315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686688;urn:miriam:reactome:R-COV-9698997;urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M_space_lattice:E_space_protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3345"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1089.7488033760374
      y 829.3110559808982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694438;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_336"
      name "GalNAc is transferred onto 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694438__layout_2393"
      uniprot "NA"
    ]
    graphics [
      x 1179.1109202102427
      y 746.3702135785918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694581;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_372"
      name "RTC synthesizes SARS-CoV-2 plus strand genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694581__layout_2292"
      uniprot "NA"
    ]
    graphics [
      x 652.6617317906205
      y 389.89360779530506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696980;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_402"
      name "Spike trimer glycoside chains get additional branches"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696980__layout_3049"
      uniprot "NA"
    ]
    graphics [
      x 1287.254352224812
      y 1846.5928677533038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_402"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-1028782;urn:miriam:reactome:R-HSA-1028782;urn:miriam:reactome:R-XTR-1028782;urn:miriam:reactome:R-DME-1028782;urn:miriam:uniprot:Q9BYC5;urn:miriam:reactome:R-DDI-1028782;urn:miriam:reactome:R-MMU-1028782;urn:miriam:reactome:R-DRE-1028782;urn:miriam:reactome:R-CEL-1028782;urn:miriam:reactome:R-RNO-1028782"
      hgnc "NA"
      map_id "UNIPROT:Q9BYC5"
      name "FUT8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3051"
      uniprot "UNIPROT:Q9BYC5"
    ]
    graphics [
      x 1411.50729990515
      y 1904.1535809417849
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DME-975913;urn:miriam:reactome:R-BTA-975913;urn:miriam:reactome:R-HSA-975913;urn:miriam:reactome:R-GGA-975913;urn:miriam:reactome:R-DRE-975913;urn:miriam:reactome:R-RNO-975913;urn:miriam:reactome:R-SSC-975913;urn:miriam:uniprot:Q9UBM8;urn:miriam:uniprot:Q9UM21;urn:miriam:reactome:R-XTR-975913;urn:miriam:uniprot:Q9UQ53;urn:miriam:reactome:R-CFA-975913;urn:miriam:reactome:R-MMU-975913"
      hgnc "NA"
      map_id "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
      name "MGAT4s"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3052"
      uniprot "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
    ]
    graphics [
      x 1446.7637221493455
      y 1798.9204373003597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q09328;urn:miriam:reactome:R-CEL-975896;urn:miriam:reactome:R-RNO-975896;urn:miriam:reactome:R-GGA-975896;urn:miriam:reactome:R-DRE-975896;urn:miriam:reactome:R-MMU-975896;urn:miriam:reactome:R-SSC-975896;urn:miriam:reactome:R-CFA-975896;urn:miriam:reactome:R-BTA-975896;urn:miriam:reactome:R-HSA-975896;urn:miriam:reactome:R-XTR-975896"
      hgnc "NA"
      map_id "UNIPROT:Q09328"
      name "MGAT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3053"
      uniprot "UNIPROT:Q09328"
    ]
    graphics [
      x 1434.9076455440904
      y 1725.1470206679746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q09328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694304;PUBMED:25197083;PUBMED:16549795"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_312"
      name "nsp14 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694304__layout_2240"
      uniprot "NA"
    ]
    graphics [
      x 451.6850751540979
      y 621.6675558843502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729283"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_410"
      name "Nucleoprotein is methylated by PRMT1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729283__layout_3539"
      uniprot "NA"
    ]
    graphics [
      x 820.9577362453443
      y 315.1259222861727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_410"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-9632173;urn:miriam:reactome:R-CFA-9632173;urn:miriam:reactome:R-HSA-9632173;urn:miriam:reactome:R-DRE-9632173;urn:miriam:reactome:R-XTR-9632173;urn:miriam:uniprot:Q99873;urn:miriam:reactome:R-DME-9632173;urn:miriam:reactome:R-RNO-9632173"
      hgnc "NA"
      map_id "UNIPROT:Q99873"
      name "PRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3788"
      uniprot "UNIPROT:Q99873"
    ]
    graphics [
      x 692.7573312720717
      y 446.1347016517474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99873"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683478;PUBMED:21124966"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_298"
      name "CQ, HCQ diffuses from cytosol to endocytic vesicle lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683478__layout_2930"
      uniprot "NA"
    ]
    graphics [
      x 1683.2040203897475
      y 560.4007136189922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694341;PUBMED:17134730;PUBMED:33310888;PUBMED:20580052"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_320"
      name "Spike protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694341__layout_2378"
      uniprot "NA"
    ]
    graphics [
      x 1128.994801327012
      y 1677.7227647525892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694333;PUBMED:32198291"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_316"
      name "3CLp forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694333__layout_2174"
      uniprot "NA"
    ]
    graphics [
      x 420.54520246273404
      y 678.8716512290571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694495;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_349"
      name "RTC binds SARS-CoV-2 genomic RNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694495__layout_2290"
      uniprot "NA"
    ]
    graphics [
      x 606.8776005332421
      y 555.4789056000418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694524;PUBMED:32783916;PUBMED:31131400;PUBMED:33208736;PUBMED:33232691;PUBMED:22615777;PUBMED:17520018"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_354"
      name "nsp13 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694524__layout_2242"
      uniprot "NA"
    ]
    graphics [
      x 471.18425036823953
      y 764.5540287595622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681514;PUBMED:14647384;PUBMED:32142651;PUBMED:21068237;PUBMED:27550352;PUBMED:27277342;PUBMED:24227843;PUBMED:28414992;PUBMED:22496216"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_296"
      name "TMPRSS2 binds TMPRSS2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681514__layout_2924"
      uniprot "NA"
    ]
    graphics [
      x 1322.6136569010969
      y 668.0590016034732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694499;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_350"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694499__layout_2435"
      uniprot "NA"
    ]
    graphics [
      x 405.635036857825
      y 358.87672941059236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685891;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694259"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      name "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2437"
      uniprot "NA"
    ]
    graphics [
      x 528.7037577180207
      y 377.960015165787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694525;PUBMED:16442106"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_355"
      name "M protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694525__layout_2417"
      uniprot "NA"
    ]
    graphics [
      x 1448.9266983958676
      y 521.5976994731049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729318;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_413"
      name "CSNK1A1 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729318__layout_3550"
      uniprot "NA"
    ]
    graphics [
      x 952.2624236170094
      y 313.26676746568376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_413"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-BTA-195263-4;urn:miriam:reactome:R-BTA-195263-5;urn:miriam:reactome:R-BTA-195263-2;urn:miriam:reactome:R-BTA-195263-3;urn:miriam:reactome:R-CEL-195263;urn:miriam:reactome:R-MMU-195263;urn:miriam:reactome:R-DME-195263;urn:miriam:reactome:R-XTR-195263;urn:miriam:reactome:R-DME-195263-2;urn:miriam:reactome:R-DME-195263-5;urn:miriam:reactome:R-CEL-195263-5;urn:miriam:reactome:R-DME-195263-3;urn:miriam:reactome:R-DME-195263-4;urn:miriam:reactome:R-HSA-195263;urn:miriam:reactome:R-CEL-195263-2;urn:miriam:reactome:R-CEL-195263-4;urn:miriam:reactome:R-CEL-195263-3;urn:miriam:reactome:R-BTA-195263;urn:miriam:reactome:R-CFA-195263;urn:miriam:reactome:R-SSC-195263;urn:miriam:uniprot:P48729;urn:miriam:reactome:R-RNO-195263;urn:miriam:reactome:R-GGA-195263;urn:miriam:reactome:R-DRE-195263"
      hgnc "NA"
      map_id "UNIPROT:P48729"
      name "CSNK1A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3789"
      uniprot "UNIPROT:P48729"
    ]
    graphics [
      x 771.3076857366719
      y 305.2233906617448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48729"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694579;PUBMED:14647384;PUBMED:16166518;PUBMED:32125455;PUBMED:22816037"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_370"
      name "Spike glycoprotein of SARS-CoV-2 binds ACE2 on host cell"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694579__layout_2483"
      uniprot "NA"
    ]
    graphics [
      x 1063.1942600426291
      y 799.3110559808982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683467;PUBMED:9719345;PUBMED:32145363;PUBMED:16115318;PUBMED:28596841;PUBMED:25693996;PUBMED:32226290"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_297"
      name "CQ, HCQ are protonated to CQ2+, HCQ2+"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683467__layout_2933"
      uniprot "NA"
    ]
    graphics [
      x 1365.713372314439
      y 580.1391942610472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694265;PUBMED:32783916;PUBMED:32484220;PUBMED:19224332;PUBMED:16579970;PUBMED:12917423;PUBMED:15140959;PUBMED:32500504;PUBMED:20671029;PUBMED:22615777"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_305"
      name "nsp13 helicase melts secondary structures in SARS-CoV-2 genomic RNA template"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694265__layout_3127"
      uniprot "NA"
    ]
    graphics [
      x 741.2563008783591
      y 443.6612512631159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694261;PUBMED:19153232;PUBMED:32592996"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_304"
      name "nsp9 forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694261__layout_2228"
      uniprot "NA"
    ]
    graphics [
      x 267.7597878799062
      y 638.3281875350588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694580;PUBMED:15331731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_371"
      name "nsp8 binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694580__layout_2509"
      uniprot "NA"
    ]
    graphics [
      x 386.31250865944924
      y 737.4350726091071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694566;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-9687117"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
      name "nsp8:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3096"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 280.44179763491957
      y 855.3713791161288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694551;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_362"
      name "3CLp cleaves nsp6-11"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694551__layout_2180"
      uniprot "NA"
    ]
    graphics [
      x 492.08317897007186
      y 536.4674484992092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694280;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_308"
      name "mRNA4 is translated to protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694280__layout_2333"
      uniprot "NA"
    ]
    graphics [
      x 1297.4146325813076
      y 1215.3790798029495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694632;PUBMED:25197083;PUBMED:17927896;PUBMED:32938769;PUBMED:20463816;PUBMED:22635272;PUBMED:16549795;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_379"
      name "nsp14 acts as a 3'-to-5' exonuclease to remove misincorporated nucleotides from nascent RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694632__layout_2272"
      uniprot "NA"
    ]
    graphics [
      x 201.9210395655847
      y 633.9431058281214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694792;PUBMED:20463816"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_398"
      name "nsp12 misincorporates a nucleotide in nascent RNA minus strand"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694792__layout_2269"
      uniprot "NA"
    ]
    graphics [
      x 530.0524919661068
      y 191.276003272411
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_398"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694733;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:32511382;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_394"
      name "Polyadenylation of SARS-CoV-2 subgenomic mRNAs (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694733__layout_2438"
      uniprot "NA"
    ]
    graphics [
      x 661.8305485011772
      y 207.27085998335156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_394"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694561;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685910"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2440"
      uniprot "NA"
    ]
    graphics [
      x 577.3865669262016
      y 222.45845789513783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694294;PUBMED:34237302;PUBMED:21524776;PUBMED:33709461;PUBMED:21450821"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_311"
      name "E pentamer is transported to the Golgi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694294__layout_2413"
      uniprot "NA"
    ]
    graphics [
      x 1539.4043343682717
      y 1065.3363593270108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694487;PUBMED:16442106;PUBMED:18703211;PUBMED:9658133;PUBMED:16254320;PUBMED:20154085;PUBMED:7721788;PUBMED:15474033;PUBMED:19534833;PUBMED:15147946;PUBMED:16877062;PUBMED:10799570;PUBMED:18753196;PUBMED:23700447;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_347"
      name "M protein oligomerization"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694487__layout_2468"
      uniprot "NA"
    ]
    graphics [
      x 1709.9704175000013
      y 827.0314618319252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694390;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_330"
      name "nsp15 binds RB1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694390__layout_2321"
      uniprot "NA"
    ]
    graphics [
      x 534.6300918182217
      y 1336.5813741880856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694258;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-HSA-9682726;urn:miriam:pubmed:22301153"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P06400"
      name "nsp15:RB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3094"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P06400"
    ]
    graphics [
      x 650.5168693522257
      y 1480.8609153776588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P06400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729279;PUBMED:29199039;PUBMED:32029454"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_409"
      name "Nucleoprotein is ADP-ribosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729279__layout_3615"
      uniprot "NA"
    ]
    graphics [
      x 971.0718600569988
      y 518.0479214723221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_409"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694567;PUBMED:27799534;PUBMED:21203998"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_365"
      name "pp1a forms a dimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694567__layout_2167"
      uniprot "NA"
    ]
    graphics [
      x 883.5725496037522
      y 642.0028417267065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687724;PUBMED:34593624;PUBMED:19666099;PUBMED:19389332;PUBMED:11162580;PUBMED:24931005;PUBMED:18977324;PUBMED:18938143;PUBMED:19106108;PUBMED:18806775"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_301"
      name "GSK3B binds GSKi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687724__layout_3596"
      uniprot "NA"
    ]
    graphics [
      x 1027.7997279425067
      y 567.406088547428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694441;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_337"
      name "3CLp cleaves pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694441__layout_2177"
      uniprot "NA"
    ]
    graphics [
      x 547.8606079532342
      y 523.5384414012212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694723;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_391"
      name "Uncoating of SARS-CoV-2 Genome"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694723__layout_2501"
      uniprot "NA"
    ]
    graphics [
      x 904.9873390672568
      y 135.46865376877588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_391"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694549;PUBMED:25197083"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_361"
      name "RTC completes synthesis of the minus strand genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694549__layout_2275"
      uniprot "NA"
    ]
    graphics [
      x 486.6917122230595
      y 202.73640995262656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694364;PUBMED:32366695;PUBMED:10929008;PUBMED:12145188"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_324"
      name "N-glycan glucose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694364__layout_2370"
      uniprot "NA"
    ]
    graphics [
      x 635.0211688479026
      y 1227.8206179240174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-532676;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "UNIPROT:Q13724"
      name "MOGS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2904"
      uniprot "UNIPROT:Q13724"
    ]
    graphics [
      x 732.6647572249415
      y 1185.8234615817073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13724"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685655;PUBMED:32179150;PUBMED:23105391;PUBMED:16962401;PUBMED:26335104;PUBMED:26953343"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_299"
      name "CTSL bind CTSL inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685655__layout_2922"
      uniprot "NA"
    ]
    graphics [
      x 420.8634135716073
      y 1328.2820908470726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691335;PUBMED:32535228;PUBMED:32438371;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_302"
      name "nsp7 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691335__layout_2558"
      uniprot "NA"
    ]
    graphics [
      x 346.9351503919762
      y 585.4979825948903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 220
    source 43
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 61
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 60
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 60
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 29
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 30
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 64
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 6
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 64
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 63
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 63
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 63
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 63
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 65
    target 66
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 66
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_323"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 67
    target 68
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 68
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_364"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 64
    target 69
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 1
    target 69
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 69
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_359"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 70
    target 71
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_392"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 71
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_392"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 72
    target 73
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 23
    target 73
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P55072"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 73
    target 74
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 73
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 75
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 50
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P26572"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 77
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16706"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 78
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10469"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 76
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_382"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 36
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      target_id "R2_387"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 79
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_387"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 20
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 81
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 21
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 22
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 82
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTD2"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 80
    target 20
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_405"
      target_id "UNIPROT:Q9GZQ8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 70
    target 83
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_384"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 83
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_384"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 84
    target 85
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_404"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 85
    target 84
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_404"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 64
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 6
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 64
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 86
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 86
    target 87
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 86
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 62
    target 88
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 88
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_317"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 84
    target 89
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 90
    target 89
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09958"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 89
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 89
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 8
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 64
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 64
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 65
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 91
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_321"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 91
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_321"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 29
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 30
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 6
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 64
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 64
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 92
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 92
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 92
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 92
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 10
    target 93
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 64
    target 93
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 93
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 93
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 93
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 13
    target 94
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 95
    target 94
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 94
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 94
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 94
    target 14
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 75
    target 96
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_396"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 96
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_396"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 35
    target 97
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17844"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 98
    target 97
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 97
    target 99
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_333"
      target_id "UNIPROT:P0DTD1;UNIPROT:P17844"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 29
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 30
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 64
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 6
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 64
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 100
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 100
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 100
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 100
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 95
    target 101
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 101
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_341"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 65
    target 102
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 102
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_366"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 56
    target 103
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      target_id "R2_400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 103
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_400"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 43
    target 104
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 65
    target 104
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 105
    target 104
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P78362;UNIPROT:Q96SB4"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 104
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 104
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 104
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 75
    target 106
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 107
    target 106
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 106
    target 108
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_363"
      target_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 67
    target 109
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 109
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_325"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 64
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_378"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 64
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_378"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 110
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_378"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 64
    target 111
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 1
    target 111
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 111
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_334"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 64
    target 112
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 113
    target 112
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "3CLp_space_inhibitors"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 112
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_373"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 81
    target 114
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 29
    target 114
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 81
    target 114
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 114
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 114
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 114
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 64
    target 115
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 64
    target 115
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 64
    target 115
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 115
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_369"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 72
    target 116
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_380"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 116
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_380"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 75
    target 117
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 58
    target 117
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 117
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_403"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 65
    target 118
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 118
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_327"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 95
    target 119
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 5
    target 119
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 119
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_357"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 64
    target 120
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 1
    target 120
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 120
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_385"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 62
    target 121
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 65
    target 121
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 121
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_343"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 75
    target 122
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 123
    target 122
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 122
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_401"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 70
    target 124
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 124
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_331"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 72
    target 125
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_381"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 125
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_381"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 84
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 17
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15393"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 17
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:O15393"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 126
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 126
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 43
    target 127
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 65
    target 127
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 127
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 127
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 127
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 65
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 128
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_368"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 42
    target 129
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      target_id "R2_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 70
    target 129
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 129
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_313"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 26
    target 130
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      target_id "R2_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 130
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_340"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 98
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 29
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 64
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 64
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 131
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 98
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 132
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_335"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 62
    target 133
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 133
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_306"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 27
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 95
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 134
    target 28
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_332"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 134
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_332"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 29
    target 135
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 64
    target 135
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 81
    target 135
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 135
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 135
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 135
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 64
    target 136
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 10
    target 136
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 136
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 136
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 136
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 64
    target 137
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 8
    target 137
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 64
    target 137
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 137
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_351"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 137
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_351"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 98
    target 138
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 64
    target 138
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 138
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_358"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 67
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 95
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 65
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 139
    target 107
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_338"
      target_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 75
    target 140
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 40
    target 140
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27824"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 140
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_318"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 65
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 43
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 46
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49840;UNIPROT:P49841"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 2
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P49841"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 141
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 141
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 141
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 38
    target 142
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 39
    target 142
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 142
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_300"
      target_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 84
    target 143
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 24
    target 143
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07711"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 24
    target 143
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P07711"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 143
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 143
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 65
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 144
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_322"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 29
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 64
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 98
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 145
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_374"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 145
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_374"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 70
    target 146
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 146
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_367"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 65
    target 147
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 65
    target 147
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 147
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_309"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 98
    target 148
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 64
    target 148
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 148
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_360"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 8
    target 149
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 64
    target 149
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 64
    target 149
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 149
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_307"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 149
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_307"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 75
    target 150
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_344"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 150
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_344"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 62
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 64
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 151
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_342"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 64
    target 152
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 10
    target 152
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 152
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 152
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 152
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 37
    target 153
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P40337"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 98
    target 153
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 153
    target 154
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_352"
      target_id "UNIPROT:P0DTD1;UNIPROT:P40337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 64
    target 155
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 64
    target 155
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 98
    target 155
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 155
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_303"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 8
    target 156
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 64
    target 156
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 64
    target 156
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 64
    target 156
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 156
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_375"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 156
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_375"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 64
    target 157
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 64
    target 157
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 64
    target 157
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 157
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_314"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 157
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_314"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 57
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CMP_minus_Neu5Ac"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 70
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 58
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 158
    target 59
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_389"
      target_id "CMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 158
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_389"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 29
    target 159
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 64
    target 159
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 64
    target 159
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 159
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 159
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 159
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 159
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 33
    target 160
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P63279;UNIPROT:P63165"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 65
    target 160
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 160
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_412"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 160
    target 34
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_412"
      target_id "UNIPROT:P63279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 62
    target 161
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 19
    target 161
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBF4"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 161
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_386"
      target_id "UNIPROT:Q8TBF4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 45
    target 162
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      target_id "R2_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 162
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_326"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 64
    target 163
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 6
    target 163
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 64
    target 163
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 163
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 163
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 163
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 64
    target 164
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 165
    target 164
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "Mg2_plus__slash_Mn2_plus_"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 98
    target 164
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 164
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_339"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 108
    target 166
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 70
    target 166
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 167
    target 166
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 166
    target 72
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_356"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 168
    target 169
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 75
    target 169
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 170
    target 169
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 169
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_399"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 169
    target 171
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_399"
      target_id "DOLP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 84
    target 172
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 90
    target 172
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P09958"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 17
    target 172
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O15393"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 172
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 172
    target 173
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 172
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 70
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 52
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus_GalNAc"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 53
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10472"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 174
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 174
    target 51
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 174
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 64
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 8
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 64
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 64
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 175
    target 87
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 175
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 175
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 75
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 177
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYC5"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 178
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 179
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q09328"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 176
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_402"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 64
    target 180
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 64
    target 180
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 180
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_312"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 65
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 6
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 182
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99873"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 181
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 181
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 181
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 15
    target 183
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R2_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 183
    target 15
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_298"
      target_id "CQ,_space_HCQ"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 75
    target 184
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 27
    target 184
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 184
    target 28
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_320"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 184
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_320"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 64
    target 185
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 185
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_316"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 87
    target 186
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 64
    target 186
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 186
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_349"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 64
    target 187
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 98
    target 187
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 187
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_354"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 17
    target 188
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15393"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 18
    target 188
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "TMPRSS2_space_inhibitors"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 188
    target 17
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_296"
      target_id "UNIPROT:O15393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 64
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 6
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 64
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 189
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 189
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 189
    target 190
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 67
    target 191
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 13
    target 191
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 191
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 191
    target 14
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 191
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 65
    target 192
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 43
    target 192
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 193
    target 192
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48729"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 192
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 192
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 192
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 4
    target 194
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 72
    target 194
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 194
    target 84
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_370"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 11
    target 195
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "R2_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 15
    target 195
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R2_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 195
    target 25
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_297"
      target_id "CQ2_plus_,_space_HCQ2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 43
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 64
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 64
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 196
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 196
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 196
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 64
    target 197
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 197
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_304"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 64
    target 198
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 20
    target 198
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 198
    target 199
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_371"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 29
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 81
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 64
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 64
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 200
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 54
    target 201
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      target_id "R2_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 201
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_308"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 29
    target 202
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 64
    target 202
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 64
    target 202
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 202
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_379"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 202
    target 32
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_379"
      target_id "NMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 64
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 8
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 64
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 64
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 203
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_398"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 203
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_398"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 43
    target 204
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 190
    target 204
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 204
    target 205
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 204
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 95
    target 206
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 206
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_311"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 67
    target 207
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 67
    target 207
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 207
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_347"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 16
    target 208
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06400"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 98
    target 208
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 208
    target 209
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_330"
      target_id "UNIPROT:P0DTD1;UNIPROT:P06400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 65
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 47
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 48
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 210
    target 49
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 210
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 210
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 81
    target 211
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 211
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_365"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 2
    target 212
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49841"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 3
    target 212
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GSKi"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 212
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_301"
      target_id "UNIPROT:P49841"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 81
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 29
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 64
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 64
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 213
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 213
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 213
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 65
    target 214
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_391"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 214
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_391"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 214
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_391"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 64
    target 215
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 8
    target 215
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 64
    target 215
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 64
    target 215
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 215
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 215
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 215
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 75
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 29
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 39
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 217
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13724"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 39
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 216
    target 55
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_324"
      target_id "beta_minus_D_minus_glucose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 216
    target 75
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_324"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 41
    target 218
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CTSL_space_inhibitors"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 24
    target 218
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07711"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 218
    target 24
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_299"
      target_id "UNIPROT:P07711"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 64
    target 219
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 64
    target 219
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 219
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_302"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
