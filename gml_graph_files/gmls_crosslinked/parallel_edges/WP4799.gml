# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 316.86625821885946
      y 240.3590161356538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 156.4298314456476
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4799; WP4965; WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "Angiotensin_space_II"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b733a; bab13; f080e; ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 414.4003135282919
      y 532.5038781161297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5f17221c"
      uniprot "NA"
    ]
    graphics [
      x 300.7218407457229
      y 620.1017198055181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4799; C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P50052; urn:miriam:ensembl:ENSG00000180772;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc:338;urn:miriam:refseq:NM_000686;urn:miriam:uniprot:P50052;urn:miriam:uniprot:P50052;urn:miriam:ncbigene:186;urn:miriam:ncbigene:186"
      hgnc "NA; HGNC_SYMBOL:AGTR2"
      map_id "UNIPROT:P50052"
      name "AT2R; AGTR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee6b1; sa79; sa25"
      uniprot "UNIPROT:P50052"
    ]
    graphics [
      x 170.53176651578104
      y 649.6199655775565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50052"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_12"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idc652beda"
      uniprot "NA"
    ]
    graphics [
      x 223.08308281593924
      y 153.86878787041766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbca35504"
      uniprot "NA"
    ]
    graphics [
      x 488.29888611867256
      y 444.86565568220885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 11
      diagram "WP4799; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P30556; urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "NA; HGNC_SYMBOL:AGTR1"
      map_id "UNIPROT:P30556"
      name "AT1R; AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab2a6; sa102; sa167; sa139; sa140; sa26; sa204; sa500; sa484; sa519; sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 359.75097779460486
      y 443.20013469353637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4799"
      full_annotation "urn:miriam:uniprot:A0A0A0MSN4"
      hgnc "NA"
      map_id "UNIPROT:A0A0A0MSN4"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d08ac"
      uniprot "UNIPROT:A0A0A0MSN4"
    ]
    graphics [
      x 503.219786922109
      y 734.9065375043272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A0A0A0MSN4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2567d541"
      uniprot "NA"
    ]
    graphics [
      x 507.3169847391587
      y 621.7172972359526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc7eb7b47"
      uniprot "NA"
    ]
    graphics [
      x 222.26386508002926
      y 437.7876894052705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4799; WP4965"
      full_annotation "urn:miriam:wikipathways:WP5035"
      hgnc "NA"
      map_id "Lung_space_injury"
      name "Lung_space_injury"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "dd819; a6535"
      uniprot "NA"
    ]
    graphics [
      x 98.191333710684
      y 467.1628316761553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lung_space_injury"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_14"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide171a636"
      uniprot "NA"
    ]
    graphics [
      x 384.729489968194
      y 362.2415876375086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4799"
      full_annotation "NA"
      hgnc "NA"
      map_id "W2_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id6c434c1e"
      uniprot "NA"
    ]
    graphics [
      x 62.499999999999986
      y 581.3872911063479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W2_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 15
    source 3
    target 4
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W2_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 4
    target 5
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_9"
      target_id "UNIPROT:P50052"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 2
    target 6
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "W2_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 6
    target 1
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_12"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 3
    target 7
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W2_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 7
    target 8
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_11"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 9
    target 10
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:A0A0A0MSN4"
      target_id "W2_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 10
    target 3
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_8"
      target_id "Angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 8
    target 11
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "W2_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 11
    target 12
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_13"
      target_id "Lung_space_injury"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 1
    target 13
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "W2_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 13
    target 3
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_14"
      target_id "Angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 5
    target 14
    cd19dm [
      diagram "WP4799"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "W2_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 14
    target 12
    cd19dm [
      diagram "WP4799"
      edge_type "PRODUCTION"
      source_id "W2_10"
      target_id "Lung_space_injury"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
