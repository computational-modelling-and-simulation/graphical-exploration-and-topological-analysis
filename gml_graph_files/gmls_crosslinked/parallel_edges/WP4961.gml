# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4799; WP4965; WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "Angiotensin_space_II"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b733a; bab13; f080e; ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 561.5837111462348
      y 265.5294974463653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4961; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580; urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA"
      map_id "cGAMP"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ba77f; bd7f8; sa83"
      uniprot "NA"
    ]
    graphics [
      x 729.6688140490124
      y 781.846713547261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cGAMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000160703"
      hgnc "NA"
      map_id "NLRX1"
      name "NLRX1"
      node_subtype "GENE"
      node_type "species"
      org_id "c2498; ab61d"
      uniprot "NA"
    ]
    graphics [
      x 432.172172973711
      y 427.9609753687567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRX1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 281.8930315951553
      y 862.59634006191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940"
      hgnc "NA"
      map_id "Vitamin_space_D3"
      name "Vitamin_space_D3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac941; fef60"
      uniprot "NA"
    ]
    graphics [
      x 848.583122958868
      y 1010.8374949993438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Vitamin_space_D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4876; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "IL1B"
      name "IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "b860a; c9a82"
      uniprot "NA"
    ]
    graphics [
      x 1158.7778448384852
      y 642.1003301522007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IL1B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000184584"
      hgnc "NA"
      map_id "STING1"
      name "STING1"
      node_subtype "GENE"
      node_type "species"
      org_id "c14f2"
      uniprot "NA"
    ]
    graphics [
      x 679.8108055365744
      y 494.8883805413717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "STING1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0e13"
      uniprot "NA"
    ]
    graphics [
      x 823.3070403571717
      y 507.4738455549542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000162711"
      hgnc "NA"
      map_id "NLRP3"
      name "NLRP3"
      node_subtype "GENE"
      node_type "species"
      org_id "f72d3"
      uniprot "NA"
    ]
    graphics [
      x 957.1503601963009
      y 527.5401032874372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRP3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      name "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      node_subtype "GENE"
      node_type "species"
      org_id "fa31c"
      uniprot "NA"
    ]
    graphics [
      x 556.934876022947
      y 1342.3357851280698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fc1c4"
      uniprot "NA"
    ]
    graphics [
      x 644.7542993991136
      y 1265.9233685781164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000164430"
      hgnc "NA"
      map_id "CGAS"
      name "CGAS"
      node_subtype "GENE"
      node_type "species"
      org_id "a9c5b"
      uniprot "NA"
    ]
    graphics [
      x 734.5789540112211
      y 1117.9793717279044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CGAS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000150995"
      hgnc "NA"
      map_id "ITPR1"
      name "ITPR1"
      node_subtype "GENE"
      node_type "species"
      org_id "ea20e"
      uniprot "NA"
    ]
    graphics [
      x 916.288266889882
      y 295.9664670720033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ITPR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idab07d001"
      uniprot "NA"
    ]
    graphics [
      x 1049.538265595105
      y 237.9773445468711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Calcium_space_release"
      name "Calcium_space_release"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8293"
      uniprot "NA"
    ]
    graphics [
      x 1175.4699274267919
      y 150.43367697569215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Calcium_space_release"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cc8bf"
      uniprot "NA"
    ]
    graphics [
      x 1299.3963985119995
      y 108.71882398484831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000104518"
      hgnc "NA"
      map_id "GSDMD"
      name "GSDMD"
      node_subtype "GENE"
      node_type "species"
      org_id "dcc65"
      uniprot "NA"
    ]
    graphics [
      x 1340.8029102168607
      y 217.95023393183487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSDMD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e393d"
      uniprot "NA"
    ]
    graphics [
      x 603.3974767311333
      y 649.4417200844639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "fb896"
      name "fb896"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fb896"
      uniprot "NA"
    ]
    graphics [
      x 584.0606247924995
      y 818.4764330091868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fb896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca979"
      uniprot "NA"
    ]
    graphics [
      x 544.5167265589025
      y 465.7359048465098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf594d3e0"
      uniprot "NA"
    ]
    graphics [
      x 241.16236669333477
      y 755.7513998362172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "e7a9d"
      name "e7a9d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7a9d"
      uniprot "NA"
    ]
    graphics [
      x 761.0202648396146
      y 1475.1342899332462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e7a9d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_7"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b3b9f"
      uniprot "NA"
    ]
    graphics [
      x 908.0332920481922
      y 1493.5921764616241
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000162924;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "c329d"
      name "c329d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c329d"
      uniprot "NA"
    ]
    graphics [
      x 1045.037963940625
      y 1461.46153441951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c329d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "c72d7"
      name "c72d7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c72d7"
      uniprot "NA"
    ]
    graphics [
      x 693.4621502844758
      y 1219.7419819581564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c72d7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cb0f5"
      uniprot "NA"
    ]
    graphics [
      x 752.130210981061
      y 1313.7278826394681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000269335;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ensembl:ENSG00000213341"
      hgnc "NA"
      map_id "fef8c"
      name "fef8c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fef8c"
      uniprot "NA"
    ]
    graphics [
      x 669.2993860191214
      y 1132.5984759645903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fef8c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8097"
      uniprot "NA"
    ]
    graphics [
      x 719.9840856264703
      y 634.7417683492995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf6aa73a2"
      uniprot "NA"
    ]
    graphics [
      x 626.3470291468351
      y 365.1286524917888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d60dd"
      uniprot "NA"
    ]
    graphics [
      x 155.4374894743322
      y 893.5861181488898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "IFNB1"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "e699d; dcfff"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 828.4524792459215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFNB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id9ea5ec2d"
      uniprot "NA"
    ]
    graphics [
      x 425.84996504612826
      y 834.0443628066929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc9fd"
      uniprot "NA"
    ]
    graphics [
      x 1158.1969895477043
      y 1389.4403980453606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cytokines"
      name "Cytokines"
      node_subtype "GENE"
      node_type "species"
      org_id "b9d10"
      uniprot "NA"
    ]
    graphics [
      x 1217.2493617198343
      y 1278.8328366130413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytokines"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Pyroptosis"
      name "Pyroptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a367e"
      uniprot "NA"
    ]
    graphics [
      x 1093.94878238144
      y 128.88364154338865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pyroptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f3a2a"
      uniprot "NA"
    ]
    graphics [
      x 982.1731088155733
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000117525"
      hgnc "NA"
      map_id "F3"
      name "F3"
      node_subtype "GENE"
      node_type "species"
      org_id "ae651"
      uniprot "NA"
    ]
    graphics [
      x 872.6627175959666
      y 114.00097462575195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d99fa"
      uniprot "NA"
    ]
    graphics [
      x 615.7466577172115
      y 1531.561036339307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232"
      hgnc "NA"
      map_id "d1991"
      name "d1991"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1991"
      uniprot "NA"
    ]
    graphics [
      x 466.11914302077133
      y 1525.855455330699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d1991"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Intravenous_br_immunoglobulins"
      name "Intravenous_br_immunoglobulins"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa4ea"
      uniprot "NA"
    ]
    graphics [
      x 1047.9570350741928
      y 1076.9445477151573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Intravenous_br_immunoglobulins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_45"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id231333be"
      uniprot "NA"
    ]
    graphics [
      x 1030.0894958744575
      y 953.4442554767045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000143226"
      hgnc "NA"
      map_id "FCGR2A"
      name "FCGR2A"
      node_subtype "GENE"
      node_type "species"
      org_id "f7129"
      uniprot "NA"
    ]
    graphics [
      x 913.8481740036829
      y 878.846260906214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FCGR2A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c76d4"
      uniprot "NA"
    ]
    graphics [
      x 1077.8770427427594
      y 561.8122444137521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8f3d31d3"
      uniprot "NA"
    ]
    graphics [
      x 747.1133576151975
      y 957.365137192842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id394406f9"
      uniprot "NA"
    ]
    graphics [
      x 1214.4685165300893
      y 225.4108341520403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcfcc5c05"
      uniprot "NA"
    ]
    graphics [
      x 760.7907208073295
      y 852.0184336781772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15365"
      hgnc "NA"
      map_id "Aspirin"
      name "Aspirin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "feddd"
      uniprot "NA"
    ]
    graphics [
      x 463.70426327602297
      y 1141.8573871143121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Aspirin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcde3e513"
      uniprot "NA"
    ]
    graphics [
      x 580.4679629876969
      y 1123.756994690226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f375a"
      uniprot "NA"
    ]
    graphics [
      x 336.8981019584162
      y 1468.2792974980177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_6"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "b2dd0"
      uniprot "NA"
    ]
    graphics [
      x 256.90571045146726
      y 1366.8606486047108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d73de"
      uniprot "NA"
    ]
    graphics [
      x 789.2940862385797
      y 374.6736498731665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc321"
      uniprot "NA"
    ]
    graphics [
      x 603.7404335513219
      y 976.5904236593966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 53
    source 7
    target 8
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 8
    target 9
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_25"
      target_id "NLRP3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 10
    target 11
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      target_id "W20_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 11
    target 12
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_40"
      target_id "CGAS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 13
    target 14
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "ITPR1"
      target_id "W20_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 14
    target 15
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_49"
      target_id "Calcium_space_release"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 15
    target 16
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Calcium_space_release"
      target_id "W20_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 16
    target 17
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_18"
      target_id "GSDMD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 7
    target 18
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 18
    target 19
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_26"
      target_id "fb896"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 3
    target 20
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W20_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 20
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_16"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 4
    target 21
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W20_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 21
    target 4
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_52"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 22
    target 23
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "e7a9d"
      target_id "W20_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 23
    target 24
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_7"
      target_id "c329d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 25
    target 26
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "c72d7"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 27
    target 26
    cd19dm [
      diagram "WP4961"
      edge_type "PHYSICAL_STIMULATION"
      source_id "fef8c"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 26
    target 22
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_17"
      target_id "e7a9d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 2
    target 28
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "cGAMP"
      target_id "W20_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 28
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_37"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 1
    target 29
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W20_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 29
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_53"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 4
    target 30
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W20_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 30
    target 31
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_20"
      target_id "IFNB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 19
    target 32
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "fb896"
      target_id "W20_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 32
    target 4
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_48"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 24
    target 33
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "c329d"
      target_id "W20_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 33
    target 34
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_41"
      target_id "Cytokines"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 35
    target 36
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Pyroptosis"
      target_id "W20_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 36
    target 37
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_34"
      target_id "F3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 22
    target 38
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "e7a9d"
      target_id "W20_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 38
    target 39
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_23"
      target_id "d1991"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 40
    target 41
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Intravenous_br_immunoglobulins"
      target_id "W20_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 41
    target 42
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_45"
      target_id "FCGR2A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 9
    target 43
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "NLRP3"
      target_id "W20_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 43
    target 6
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_14"
      target_id "IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 12
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "CGAS"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 5
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "INHIBITION"
      source_id "Vitamin_space_D3"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 44
    target 2
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_47"
      target_id "cGAMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 17
    target 45
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "GSDMD"
      target_id "W20_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 45
    target 35
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_46"
      target_id "Pyroptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 42
    target 46
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "FCGR2A"
      target_id "W20_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 46
    target 19
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_51"
      target_id "fb896"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 47
    target 48
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Aspirin"
      target_id "W20_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 48
    target 12
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_50"
      target_id "CGAS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 39
    target 49
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "d1991"
      target_id "W20_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 49
    target 50
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_33"
      target_id "W20_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 7
    target 51
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 51
    target 13
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_21"
      target_id "ITPR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 19
    target 52
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "fb896"
      target_id "W20_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 52
    target 27
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_9"
      target_id "fef8c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
