# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1056.8322599588846
      y 417.5383577699041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 62.79694349522606
      y 1124.593021363077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:PAMP signalling; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P19525; urn:miriam:uniprot:P19525;urn:miriam:wikidata:Q2819370; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "NA; HGNC_SYMBOL:EIF2AK2"
      map_id "UNIPROT:P19525"
      name "PKR; d477c; EIF2AK2:dsRNA; EIF2AK2"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c0e50; d477c; csa96; sa454; path_0_sa287; path_0_sa210"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 1727.1947535223007
      y 1664.0984194639086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19525"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18850; urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "NA; HGNC_SYMBOL:ATF6"
      map_id "UNIPROT:P18850"
      name "ATF6; ATF6_minus_p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bfd13; b27d0; b1cbb; f77a3; path_0_sa3; path_0_sa65; path_0_sa62; path_0_sa64; path_0_sa57"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1205.1340310347207
      y 1326.109316988259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18850"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P11021; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:pubmed:32169481;urn:miriam:hgnc:5238;urn:miriam:pubmed:30978349;urn:miriam:refseq:NM_005347;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:pubmed:32340551;urn:miriam:ec-code:3.6.4.10; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "NA; HGNC_SYMBOL:HSPA5"
      map_id "UNIPROT:P11021"
      name "cc525; e10e4; dadd6; HSPA5"
      node_subtype "COMPLEX; PROTEIN; RNA; GENE"
      node_type "species"
      org_id "cc525; e10e4; dadd6; path_0_sa4; path_1_sa87; sa262; path_1_sa149; path_1_sa125"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1267.5958480131133
      y 717.058186451261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:O75460; urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "NA; HGNC_SYMBOL:ERN1"
      map_id "UNIPROT:O75460"
      name "f8553; ERN1; ERN1:Unfolded_space_protein"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "f8553; b9fe8; a273e; path_0_sa1; path_0_csa2; path_0_sa617"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 890.542522328946
      y 1480.3687780889966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75460"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18848; urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272; urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "NA; HGNC_SYMBOL:ATF4"
      map_id "UNIPROT:P18848"
      name "ATF4"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "e01f1; path_0_sa105; path_0_sa106; path_0_sa104"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 654.3883104667602
      y 1260.3512056202592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18848"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:O75807;urn:miriam:wikidata:Q7251492; urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645; urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645;urn:miriam:ncbigene:23645"
      hgnc "NA; HGNC_SYMBOL:PPP1R15A"
      map_id "UNIPROT:O75807"
      name "df8e0; PPP1R15A"
      node_subtype "COMPLEX; RNA; PROTEIN; GENE"
      node_type "species"
      org_id "df8e0; path_0_sa108; path_0_sa107; path_0_sa109"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 851.0453542383684
      y 1680.2971391655133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75807"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18850;urn:miriam:uniprot:P11021; urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "NA; HGNC_SYMBOL:ATF6;HGNC_SYMBOL:HSPA5"
      map_id "UNIPROT:P18850;UNIPROT:P11021"
      name "beb8c; ATF6:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "beb8c; path_0_csa1"
      uniprot "UNIPROT:P18850;UNIPROT:P11021"
    ]
    graphics [
      x 1296.514160635721
      y 1339.8496930895658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18850;UNIPROT:P11021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9NZJ5; urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "NA; HGNC_SYMBOL:EIF2AK3"
      map_id "UNIPROT:Q9NZJ5"
      name "fbd45; PERK; EIF2AK3:EIF2AK3; EIF2AK3"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "fbd45; a8ce8; path_0_csa21; path_0_sa2"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 1261.9757113637172
      y 1704.1066094820658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NZJ5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Interferon 1 pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P45983; urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599; urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "NA; HGNC_SYMBOL:MAPK8"
      map_id "UNIPROT:P45983"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b098e; sa63; path_0_sa40; path_0_sa41"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 1068.314926111509
      y 1831.161635934567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 955.5263195226339
      y 1043.203643814262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P17861; urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861; urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "NA; HGNC_SYMBOL:XBP1"
      map_id "UNIPROT:P17861"
      name "XBP1u; XBP1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "d3eb7; bbd1f; path_0_sa206; path_0_sa26; path_0_sa28; path_0_sa627; path_0_sa626"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 617.0851912181979
      y 1205.8545936715886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17861"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:JNK pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P10415; urn:miriam:hgnc.symbol:BCL2;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "NA; HGNC_SYMBOL:BCL2"
      map_id "UNIPROT:P10415"
      name "BCL2"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "c7da2; sa53; sa12; path_0_sa55; path_0_sa54; path_0_sa152"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 721.490677091254
      y 1202.8567394314448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10415"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H492; urn:miriam:obo.chebi:16038;urn:miriam:uniprot:Q9H492; urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492"
      hgnc "NA; HGNC_SYMBOL:MAP1LC3A"
      map_id "UNIPROT:Q9H492"
      name "LC3; b35a3; MAP1LC3A"
      node_subtype "GENE; COMPLEX; PROTEIN"
      node_type "species"
      org_id "b3be1; b35a3; path_0_sa239"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 484.78083348283053
      y 1770.367617416735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H492"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H1Y0; urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849"
      hgnc "NA; HGNC_SYMBOL:ATG5"
      map_id "UNIPROT:Q9H1Y0"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c82d3; path_0_sa241"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 521.9931829367157
      y 1983.8996540445169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "UNIPROT:P78536"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43; path_1_sa174; path_1_sa171"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 1366.569291347429
      y 1190.786164465621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78536"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:10406945;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:pubmed:10406945;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720; urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:pubmed:25704011;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720; urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "UNIPROT:Q99720"
      name "SIGMAR1; Nsp6:SIGMAR1; SIGMAR1:Drugs"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa53; sa226; sa227; csa18; csa19; csa53; csa92; path_1_sa86; path_1_sa82"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1329.6489914212002
      y 647.4415904180744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99720"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "UNIPROT:Q16539"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64; sa368; sa445; sa49; sa50; path_0_sa42; path_0_sa43; path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 668.5672857709045
      y 1607.6377325762762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:PAMP signalling; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032; urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "UNIPROT:Q12933"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa130; path_0_sa119; path_0_sa118; path_0_sa204"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 829.0538893674159
      y 1624.5082483597807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q12933"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619; urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "GDP"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa225; sa1274; path_0_sa100; path_0_sa103"
      uniprot "NA"
    ]
    graphics [
      x 311.0676672480939
      y 1057.2010353010983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812; urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "UNIPROT:Q07812"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa31; sa30; path_0_sa458; path_0_sa126; path_0_sa127"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1048.8181131036504
      y 1266.938870430257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07812"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:pubmed:32555321;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3;urn:miriam:hgnc.symbol:CASP3; urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:pubmed:32555321;urn:miriam:hgnc.symbol:CASP3; urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "UNIPROT:P42574"
      name "CASP3; cleaved~CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17; sa15; path_0_sa597; path_0_sa596"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 882.0429695620433
      y 1207.2351232579317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42574"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050; urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "UNIPROT:P55957"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89; sa22; sa23; path_0_sa436; path_0_sa435"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 914.587666606513
      y 1719.6913925190502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55957"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211; urn:miriam:hgnc.symbol:CASP9;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc.symbol:CSAP9;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211; urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9; HGNC_SYMBOL:CASP9;HGNC_SYMBOL:CSAP9"
      map_id "UNIPROT:P55211"
      name "CASP9; cleaved~CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18; sa19; sa47; path_0_sa598; path_0_sa599"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 632.6159112173777
      y 877.8706877695035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108; urn:miriam:pubmed:24231807;urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "Ca2_plus_"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa141; sa142; sa144; path_0_sa164; path_1_sa54; path_1_sa55; path_1_sa63; path_1_sa64; path_0_sa163"
      uniprot "NA"
    ]
    graphics [
      x 1790.174921825378
      y 696.8651943684263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_85"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re38"
      uniprot "NA"
    ]
    graphics [
      x 556.9258580805982
      y 1257.1536028618268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6; urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      name "DDIT3"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "path_0_sa46; path_0_sa69; path_0_sa71; path_0_sa630; path_0_sa70; path_0_sa629"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 685.9040854152485
      y 1347.9020452341613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35638;UNIPROT:P0DPQ6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611; urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "UNIPROT:Q13217"
      name "DNAJC3"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "path_0_sa517; path_0_sa515; path_0_sa624; path_0_sa516"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1704.317519855134
      y 1585.2123074930041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re274"
      uniprot "NA"
    ]
    graphics [
      x 1813.3409527957697
      y 1502.9307094648682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_34"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re19"
      uniprot "NA"
    ]
    graphics [
      x 465.64311070408894
      y 1180.0725555556774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:32075877;PUBMED:32225175"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_330"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 917.7411479471468
      y 512.2794321692282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa258; sa275"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    ]
    graphics [
      x 1055.2863219938886
      y 585.5621270359402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1007.1926078040779
      y 493.2364049661958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17090218"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_22"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_0_re1"
      uniprot "NA"
    ]
    graphics [
      x 1243.6656007796323
      y 1145.5083899325152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "Unfolded_space_protein"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa618; path_0_sa203; path_0_sa5"
      uniprot "NA"
    ]
    graphics [
      x 1157.8976258819348
      y 1431.7653012935084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Unfolded_space_protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217"
      hgnc "HGNC_SYMBOL:VAPB"
      map_id "UNIPROT:O95292"
      name "VAPB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa147"
      uniprot "UNIPROT:O95292"
    ]
    graphics [
      x 1662.1617747361283
      y 1712.8281814143816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_255"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re129"
      uniprot "NA"
    ]
    graphics [
      x 1712.3142053621532
      y 1896.3254070863038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051561;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "Ca2_plus__space_mitochondrial_space_concentration"
      name "Ca2_plus__space_mitochondrial_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa150"
      uniprot "NA"
    ]
    graphics [
      x 1820.21196096198
      y 1813.439680592065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca2_plus__space_mitochondrial_space_concentration"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re28"
      uniprot "NA"
    ]
    graphics [
      x 718.8987256907058
      y 1726.8326182304345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:TRAF2"
      map_id "UNIPROT:O75460;UNIPROT:Q12933"
      name "TRAF2:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa6"
      uniprot "UNIPROT:O75460;UNIPROT:Q12933"
    ]
    graphics [
      x 753.8020368768637
      y 1574.5017298866005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75460;UNIPROT:Q12933"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_33"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re18"
      uniprot "NA"
    ]
    graphics [
      x 889.4901354297214
      y 1373.5272952841033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BBC3"
      map_id "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      name "ERN1:BBC3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa51"
      uniprot "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 761.0032353475676
      y 1476.2020602580487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BCL2L11;HGNC_SYMBOL:ERN1"
      map_id "UNIPROT:O43521;UNIPROT:O75460"
      name "ERN1:BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa50"
      uniprot "UNIPROT:O43521;UNIPROT:O75460"
    ]
    graphics [
      x 986.6812617046879
      y 1475.6546887827985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43521;UNIPROT:O75460"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572; urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572"
      hgnc "HGNC_SYMBOL:BCAP31"
      map_id "UNIPROT:P51572"
      name "BCAP31; p20"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa156; path_1_sa166"
      uniprot "UNIPROT:P51572"
    ]
    graphics [
      x 1692.1285612145061
      y 1256.0747821796565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51572"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_256"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re133"
      uniprot "NA"
    ]
    graphics [
      x 1376.151492487785
      y 1252.4935778113015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364; urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:pubmed:26108729;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:PACS2"
      map_id "UNIPROT:Q86VP3"
      name "PACS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa3; path_1_sa175"
      uniprot "UNIPROT:Q86VP3"
    ]
    graphics [
      x 1576.8911034881114
      y 1452.9530978287094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86VP3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_77"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re363"
      uniprot "NA"
    ]
    graphics [
      x 894.1632809517259
      y 1128.7571089057908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:32340551;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:HSPA5"
      map_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
      name "HSPA5_minus_Spike_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
    ]
    graphics [
      x 1229.010139812199
      y 349.9703034590649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:29887526"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_339"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re143"
      uniprot "NA"
    ]
    graphics [
      x 1088.5727637938978
      y 315.50724165493773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0046718;urn:miriam:pubmed:19411314"
      hgnc "NA"
      map_id "viral_space_entry_space_into_space_host_space_cell"
      name "viral_space_entry_space_into_space_host_space_cell"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 928.0985515897293
      y 165.55525062617653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_entry_space_into_space_host_space_cell"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:pubmed:22511781;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:pubmed:29491369;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:pubmed:19052620; urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4; urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-; urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-; urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877"
      hgnc "HGNC_SYMBOL:MFN2; HGNC_SYMBOL:MFN2;HGNC_SYMBOL:CEACAMP4"
      map_id "UNIPROT:O95140"
      name "MFN2; MFN1:MFN2; MFN2:MFN2"
      node_subtype "PROTEIN; COMPLEX; RNA; GENE"
      node_type "species"
      org_id "path_1_sa98; path_1_csa4; path_1_sa153; path_1_csa17; path_1_sa148; path_1_sa122"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1898.1282853019509
      y 786.4789393355563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_276"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re53"
      uniprot "NA"
    ]
    graphics [
      x 2137.4563881264894
      y 834.688086259478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4"
      hgnc "HGNC_SYMBOL:CEACAMP4"
      map_id "MFN1"
      name "MFN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa99"
      uniprot "NA"
    ]
    graphics [
      x 2168.9492480743415
      y 986.3879763150396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MFN1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ensembl:ENSG00000197930; urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "UNIPROT:Q96HE7"
      name "ERO1A"
      node_subtype "GENE; RNA; PROTEIN"
      node_type "species"
      org_id "path_0_sa122; path_0_sa123; path_0_sa121; path_0_sa212; path_0_sa625"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 1396.7371974119023
      y 1629.1986924520277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96HE7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:25387528"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_101"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re76"
      uniprot "NA"
    ]
    graphics [
      x 1086.8674671950957
      y 1452.7367859248318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030970"
      hgnc "NA"
      map_id "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      name "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa15"
      uniprot "NA"
    ]
    graphics [
      x 1571.9520014115321
      y 1381.173542360956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re13"
      uniprot "NA"
    ]
    graphics [
      x 1699.2569006449885
      y 1473.2192057095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006511"
      hgnc "NA"
      map_id "protein_space_ubiquitination_space_and_space_destruction"
      name "protein_space_ubiquitination_space_and_space_destruction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa16"
      uniprot "NA"
    ]
    graphics [
      x 1636.327485311172
      y 1553.158733620975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "protein_space_ubiquitination_space_and_space_destruction"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12215209"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re30"
      uniprot "NA"
    ]
    graphics [
      x 703.3552039549727
      y 1854.8514492203033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q99683;urn:miriam:uniprot:Q99683;urn:miriam:ncbigene:4217;urn:miriam:ncbigene:4217;urn:miriam:hgnc:6857;urn:miriam:refseq:NM_005923;urn:miriam:hgnc.symbol:MAP3K5;urn:miriam:ensembl:ENSG00000197442;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K5"
      map_id "UNIPROT:Q99683"
      name "MAP3K5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa39; path_0_sa38"
      uniprot "UNIPROT:Q99683"
    ]
    graphics [
      x 821.7403884323023
      y 1974.262838011276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99683"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re2"
      uniprot "NA"
    ]
    graphics [
      x 1040.9127498100222
      y 1517.904590434684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_27"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re133"
      uniprot "NA"
    ]
    graphics [
      x 1390.6020890983696
      y 1800.6408755337636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_71"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re357"
      uniprot "NA"
    ]
    graphics [
      x 1038.8673271502407
      y 1713.970171435965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "UPR"
      name "UPR"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa36"
      uniprot "NA"
    ]
    graphics [
      x 844.7115141912611
      y 1514.6529161914193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UPR"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_99"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re68"
      uniprot "NA"
    ]
    graphics [
      x 836.0470981800399
      y 1828.960094319544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re45"
      uniprot "NA"
    ]
    graphics [
      x 1150.0677310837714
      y 1183.1111919728417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_015884;urn:miriam:hgnc:15455;urn:miriam:ec-code:3.4.24.85;urn:miriam:hgnc.symbol:MBTPS2;urn:miriam:ncbigene:51360;urn:miriam:ncbigene:51360;urn:miriam:ensembl:ENSG00000012174;urn:miriam:uniprot:O43462;urn:miriam:uniprot:O43462"
      hgnc "HGNC_SYMBOL:MBTPS2"
      map_id "UNIPROT:O43462"
      name "MBTPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa61"
      uniprot "UNIPROT:O43462"
    ]
    graphics [
      x 1097.0704407459339
      y 1063.3591427502388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43462"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_333"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re137"
      uniprot "NA"
    ]
    graphics [
      x 1110.4401125330971
      y 870.4485941335566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re359"
      uniprot "NA"
    ]
    graphics [
      x 1026.6308501392218
      y 1404.8692022036262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384; urn:miriam:obo.chebi:CHEBI%3A29108;urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384"
      hgnc "HGNC_SYMBOL:CAPN1"
      map_id "UNIPROT:P07384"
      name "CAPN1; CAPN1:Ca2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "path_0_sa602; path_0_csa56"
      uniprot "UNIPROT:P07384"
    ]
    graphics [
      x 1457.2862704001827
      y 811.9346523859228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:19931333"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re324"
      uniprot "NA"
    ]
    graphics [
      x 1647.3956038912568
      y 812.3101554834352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A1902656"
      hgnc "NA"
      map_id "high_space_Ca2_plus__space_cytosolic_space_concentration"
      name "high_space_Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa259"
      uniprot "NA"
    ]
    graphics [
      x 1704.451049505497
      y 922.7534584843154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "high_space_Ca2_plus__space_cytosolic_space_concentration"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_279"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re59"
      uniprot "NA"
    ]
    graphics [
      x 1457.7326138722058
      y 963.5973535466956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "ER_space_Stress"
      name "ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa58"
      uniprot "NA"
    ]
    graphics [
      x 1350.0025236564481
      y 1373.9421791076877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_space_Stress"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_65"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re349"
      uniprot "NA"
    ]
    graphics [
      x 1124.4078934358192
      y 1520.7130255477123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "Persistant_space_ER_space_Stress"
      name "Persistant_space_ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa231; path_0_sa633"
      uniprot "NA"
    ]
    graphics [
      x 834.3247155022292
      y 1571.5496334830382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Persistant_space_ER_space_Stress"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:9388233"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_261"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re146"
      uniprot "NA"
    ]
    graphics [
      x 1518.9610674446665
      y 874.809763573478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0032469"
      hgnc "NA"
      map_id "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
      name "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa143"
      uniprot "NA"
    ]
    graphics [
      x 1758.3359634503704
      y 999.6393352847425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0000266"
      hgnc "NA"
      map_id "mitochondrial_space_fission"
      name "mitochondrial_space_fission"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa169"
      uniprot "NA"
    ]
    graphics [
      x 1337.324502219408
      y 1991.6631119130002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitochondrial_space_fission"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_259"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re142"
      uniprot "NA"
    ]
    graphics [
      x 1201.4763372782386
      y 2121.113351300471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0043653"
      hgnc "NA"
      map_id "mitochondria_space_fragmentation"
      name "mitochondria_space_fragmentation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa154"
      uniprot "NA"
    ]
    graphics [
      x 1032.5963641239364
      y 2112.887063191539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitochondria_space_fragmentation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_252"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re126"
      uniprot "NA"
    ]
    graphics [
      x 2001.6158058178466
      y 1328.9081316550253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:FIS1"
      map_id "UNIPROT:Q9Y3D6"
      name "FIS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa158"
      uniprot "UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 2148.373821250653
      y 1555.067440599588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y3D6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:pubmed:21183955;urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572;urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:BCAP31;HGNC_SYMBOL:FIS1"
      map_id "UNIPROT:P51572;UNIPROT:Q9Y3D6"
      name "FIS1:BCAP31"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa1"
      uniprot "UNIPROT:P51572;UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 2026.6354384187362
      y 1116.706024173089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51572;UNIPROT:Q9Y3D6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re17"
      uniprot "NA"
    ]
    graphics [
      x 888.6644097930695
      y 1318.5269877114615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BAK1;HGNC_SYMBOL:ERN1"
      map_id "UNIPROT:Q16611;UNIPROT:O75460"
      name "BAK1:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa19"
      uniprot "UNIPROT:Q16611;UNIPROT:O75460"
    ]
    graphics [
      x 828.9352602200267
      y 1176.5932317518075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16611;UNIPROT:O75460"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BAX"
      map_id "UNIPROT:O75460;UNIPROT:Q07812"
      name "BAX:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa20"
      uniprot "UNIPROT:O75460;UNIPROT:Q07812"
    ]
    graphics [
      x 992.1458607037525
      y 1214.0063770306003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75460;UNIPROT:Q07812"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1"
      hgnc "HGNC_SYMBOL:BCL2L1"
      map_id "UNIPROT:Q07817"
      name "BCL2L1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa153"
      uniprot "UNIPROT:Q07817"
    ]
    graphics [
      x 941.7774557555592
      y 1189.335325796702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949"
      hgnc "HGNC_SYMBOL:BAK1"
      map_id "UNIPROT:Q16611"
      name "BAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa459"
      uniprot "UNIPROT:Q16611"
    ]
    graphics [
      x 845.3981638984963
      y 1116.364683892049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16611"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_253"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re127"
      uniprot "NA"
    ]
    graphics [
      x 2283.24655908951
      y 1638.3684643445506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051882;urn:miriam:pubmed:21183955"
      hgnc "NA"
      map_id "mitochondrial_space_outer_space_membrane_space_depolarization"
      name "mitochondrial_space_outer_space_membrane_space_depolarization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa159"
      uniprot "NA"
    ]
    graphics [
      x 2289.3168745542953
      y 1760.6369929900316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitochondrial_space_outer_space_membrane_space_depolarization"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:15277680"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re65"
      uniprot "NA"
    ]
    graphics [
      x 585.9398833609682
      y 1085.51678431634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S1"
      map_id "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
      name "EIF2_minus_P:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa33"
      uniprot "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
    ]
    graphics [
      x 653.2793652063192
      y 1178.6745955894864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977"
      hgnc "HGNC_SYMBOL:EIF2S2"
      map_id "UNIPROT:P20042"
      name "EIF2S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa88"
      uniprot "UNIPROT:P20042"
    ]
    graphics [
      x 393.9004988994908
      y 1415.4604694650434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P20042"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re62"
      uniprot "NA"
    ]
    graphics [
      x 526.4590357523614
      y 1477.1827641919663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415"
      hgnc "HGNC_SYMBOL:EIF2S3"
      map_id "UNIPROT:P41091"
      name "EIF2S3:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa31"
      uniprot "UNIPROT:P41091"
    ]
    graphics [
      x 441.58826113616067
      y 1360.3430108304037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P41091"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S1"
      map_id "UNIPROT:P05198"
      name "EIF2S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa90; path_0_sa87"
      uniprot "UNIPROT:P05198"
    ]
    graphics [
      x 924.5833478813377
      y 1535.0492518527826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180; urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:obo.chebi:CHEBI%3A57600;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3; HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
      name "EIF2:GDP; Ternary_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa30; path_0_csa8"
      uniprot "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      x 337.9942896050238
      y 1295.3748609108616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_75"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re361"
      uniprot "NA"
    ]
    graphics [
      x 922.870587440123
      y 1412.1909969562562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_249"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re120"
      uniprot "NA"
    ]
    graphics [
      x 1970.6937259276142
      y 837.5231367901447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CDK5;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:1774;urn:miriam:ensembl:ENSG00000164885;urn:miriam:ncbigene:1020;urn:miriam:ncbigene:1020;urn:miriam:uniprot:Q00535;urn:miriam:uniprot:Q00535;urn:miriam:refseq:NM_001164410"
      hgnc "HGNC_SYMBOL:CDK5"
      map_id "UNIPROT:Q00535"
      name "CDK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa147; path_0_sa146"
      uniprot "UNIPROT:Q00535"
    ]
    graphics [
      x 1303.7873433323532
      y 1301.3296641325437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00535"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "path_0_re94"
      uniprot "NA"
    ]
    graphics [
      x 1442.783837316714
      y 1234.088316193049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
      name "EIF2:GDP:Met_minus_tRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa7"
      uniprot "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    ]
    graphics [
      x 238.08130159773532
      y 1317.9310098210983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re63"
      uniprot "NA"
    ]
    graphics [
      x 150.85003925409376
      y 1190.6562712217815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000145191;urn:miriam:ncbigene:8893;urn:miriam:ncbigene:8893;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:refseq:NM_003907;urn:miriam:uniprot:Q13144;urn:miriam:uniprot:Q13144;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:hgnc:3261;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc:3257;urn:miriam:ncbigene:1967;urn:miriam:ncbigene:1967;urn:miriam:ensembl:ENSG00000111361;urn:miriam:uniprot:Q14232;urn:miriam:uniprot:Q14232;urn:miriam:refseq:NM_001414;urn:miriam:ensembl:ENSG00000070785;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc:3259;urn:miriam:ncbigene:8891;urn:miriam:ncbigene:8891;urn:miriam:refseq:NM_020365;urn:miriam:uniprot:Q9NR50;urn:miriam:uniprot:Q9NR50;urn:miriam:ncbigene:8890;urn:miriam:ncbigene:8890;urn:miriam:uniprot:Q9UI10;urn:miriam:uniprot:Q9UI10;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:ensembl:ENSG00000115211;urn:miriam:hgnc:3260;urn:miriam:refseq:NM_001034116;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:ncbigene:8892;urn:miriam:ncbigene:8892;urn:miriam:hgnc:3258;urn:miriam:uniprot:P49770;urn:miriam:uniprot:P49770;urn:miriam:ensembl:ENSG00000119718;urn:miriam:refseq:NM_014239"
      hgnc "HGNC_SYMBOL:EIF2B5;HGNC_SYMBOL:EIF2B1;HGNC_SYMBOL:EIF2B3;HGNC_SYMBOL:EIF2B4;HGNC_SYMBOL:EIF2B2"
      map_id "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
      name "EIF2B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa34"
      uniprot "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
    ]
    graphics [
      x 173.7520901609795
      y 1055.2379599997876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_268"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re42"
      uniprot "NA"
    ]
    graphics [
      x 1577.5147190803125
      y 594.1285129798415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710"
      hgnc "HGNC_SYMBOL:ITPR3"
      map_id "UNIPROT:Q14573"
      name "ITPR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa88; path_1_sa11"
      uniprot "UNIPROT:Q14573"
    ]
    graphics [
      x 1712.6799656270236
      y 569.4497382278736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14573"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "ER_space_stress"
      name "ER_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa69"
      uniprot "NA"
    ]
    graphics [
      x 1419.9727523263289
      y 628.0534541669607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_space_stress"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:obo.go:GO%3A0032471;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029"
      hgnc "NA"
      map_id "Ca2_plus__space_ER_space_depletion"
      name "Ca2_plus__space_ER_space_depletion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa60"
      uniprot "NA"
    ]
    graphics [
      x 1628.849357092333
      y 595.0048630792732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca2_plus__space_ER_space_depletion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:SIGMAR1"
      map_id "UNIPROT:Q14573;UNIPROT:Q99720"
      name "SIGMAR1:ITPR3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa14"
      uniprot "UNIPROT:Q14573;UNIPROT:Q99720"
    ]
    graphics [
      x 1740.5896105655536
      y 513.460051597795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14573;UNIPROT:Q99720"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re245"
      uniprot "NA"
    ]
    graphics [
      x 679.8997038201655
      y 1519.6227679585968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:BBC3"
      map_id "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      name "BBC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa476"
      uniprot "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 553.8241060308693
      y 1398.1514075114374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_282"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re71"
      uniprot "NA"
    ]
    graphics [
      x 1832.1143904853848
      y 922.1158686354245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030968;urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090"
      hgnc "NA"
      map_id "unfolded_space_protein_space_response_space_(UPR)"
      name "unfolded_space_protein_space_response_space_(UPR)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa120"
      uniprot "NA"
    ]
    graphics [
      x 1639.559638522141
      y 985.1701092240471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "unfolded_space_protein_space_response_space_(UPR)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19411314;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
      name "ACE2_minus_SARS_minus_CoV_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 813.6583274380826
      y 281.72301461905806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_337"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 787.6939866184172
      y 116.37023127432474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_minus_CoV_space_infection"
      name "SARS_minus_CoV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 671.171148039499
      y 89.7850269618549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_80"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re367"
      uniprot "NA"
    ]
    graphics [
      x 771.8205413286898
      y 842.1412132699438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0043066"
      hgnc "NA"
      map_id "Cell_space_survival"
      name "Cell_space_survival"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa135"
      uniprot "NA"
    ]
    graphics [
      x 729.9485824852136
      y 729.9139370908597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cell_space_survival"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_280"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re67"
      uniprot "NA"
    ]
    graphics [
      x 1162.6825205716507
      y 835.5466397574828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:18191217"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re29"
      uniprot "NA"
    ]
    graphics [
      x 1030.8251092704318
      y 1967.1752129396352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K4"
      map_id "UNIPROT:Q9Y6R4"
      name "MAP3K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa145; path_0_sa144"
      uniprot "UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 1182.8690403896576
      y 1794.8953043520446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6R4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_332"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 1235.5979347137018
      y 592.889285777276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_273"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re49"
      uniprot "NA"
    ]
    graphics [
      x 2042.437359072288
      y 842.3164000226203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:27457486;urn:miriam:pubmed:30590907;urn:miriam:pubmed:29491369;urn:miriam:obo.go:GO%3A1990456;urn:miriam:pubmed:28132811;urn:miriam:pubmed:19052620"
      hgnc "NA"
      map_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
      name "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa97"
      uniprot "NA"
    ]
    graphics [
      x 2043.825467213907
      y 1022.5013294674073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "UNIPROT:O43521"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa434; path_0_sa433; path_0_sa475"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1288.7956152162146
      y 1625.3475262578577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43521"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re249"
      uniprot "NA"
    ]
    graphics [
      x 1551.2227229753337
      y 1760.764166673774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc:9299"
      hgnc "HGNC_SYMBOL:PPP2CA"
      map_id "UNIPROT:P67775"
      name "PPP2CA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa603"
      uniprot "UNIPROT:P67775"
    ]
    graphics [
      x 1689.9021978346177
      y 1828.9003182800066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P67775"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0032471"
      hgnc "NA"
      map_id "release_space_of_space_ER_space_Ca2_plus_"
      name "release_space_of_space_ER_space_Ca2_plus_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa132"
      uniprot "NA"
    ]
    graphics [
      x 1364.3406320517104
      y 932.0485269551494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "release_space_of_space_ER_space_Ca2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_56"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re329"
      uniprot "NA"
    ]
    graphics [
      x 1571.6642542129698
      y 901.6990014846713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re221"
      uniprot "NA"
    ]
    graphics [
      x 1353.7233073309378
      y 1734.8132904840315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:TRM-CAT3-1"
      map_id "Met_minus_tRNA"
      name "Met_minus_tRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa293"
      uniprot "NA"
    ]
    graphics [
      x 502.06219676900366
      y 1576.1151333978894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Met_minus_tRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re165"
      uniprot "NA"
    ]
    graphics [
      x 436.03316322692785
      y 1456.751010958958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_86"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re42"
      uniprot "NA"
    ]
    graphics [
      x 1467.6773566832976
      y 1306.4759851759416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_263"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re152"
      uniprot "NA"
    ]
    graphics [
      x 1425.1969261804386
      y 1441.9183084696617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:32264791;urn:miriam:pubmed:26108729;urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729;urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:ADAM17;HGNC_SYMBOL:PACS2"
      map_id "UNIPROT:P78536;UNIPROT:Q86VP3"
      name "PACS2_minus_ADAM17_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa18"
      uniprot "UNIPROT:P78536;UNIPROT:Q86VP3"
    ]
    graphics [
      x 1510.6005575587105
      y 1530.9991980877194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78536;UNIPROT:Q86VP3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1; urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "UNIPROT:Q9Y4L1"
      name "HYOU1"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "path_0_sa604; path_0_sa606; path_0_sa605"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 1383.5977654381024
      y 852.4602691328713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4L1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_55"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re328"
      uniprot "NA"
    ]
    graphics [
      x 1463.5680917770503
      y 695.7771808560963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re323"
      uniprot "NA"
    ]
    graphics [
      x 795.5978524909631
      y 886.4878065306341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:3.4.22.57;urn:miriam:ensembl:ENSG00000196954;urn:miriam:ncbigene:837;urn:miriam:ncbigene:837;urn:miriam:hgnc:1505;urn:miriam:refseq:NM_001225;urn:miriam:hgnc.symbol:CASP4;urn:miriam:uniprot:P49662;urn:miriam:uniprot:P49662"
      hgnc "HGNC_SYMBOL:CASP4"
      map_id "UNIPROT:P49662"
      name "cleaved~CASP4; CASP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa595; path_0_sa594"
      uniprot "UNIPROT:P49662"
    ]
    graphics [
      x 969.9770671824494
      y 902.3895453841274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49662"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:30590033"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_265"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re19"
      uniprot "NA"
    ]
    graphics [
      x 1837.330386747093
      y 461.7040653734791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590033;urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000213585;urn:miriam:hgnc:12669;urn:miriam:refseq:NM_003374;urn:miriam:uniprot:P21796;urn:miriam:uniprot:P21796;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:ncbigene:7416;urn:miriam:ncbigene:7416;urn:miriam:hgnc:5244;urn:miriam:refseq:NM_004134;urn:miriam:ensembl:ENSG00000113013;urn:miriam:uniprot:P38646;urn:miriam:uniprot:P38646;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:ncbigene:3313;urn:miriam:ncbigene:3313"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:VDAC1;HGNC_SYMBOL:HSPA9"
      map_id "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
      name "ITPR3:HSPA9:VDAC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa2"
      uniprot "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
    ]
    graphics [
      x 1870.7628740981918
      y 541.2261614544007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_82"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re372"
      uniprot "NA"
    ]
    graphics [
      x 857.636830705989
      y 842.2414715907552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_67"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re352"
      uniprot "NA"
    ]
    graphics [
      x 1807.6128254598702
      y 810.585752748113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:28132811;urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217;urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:VAPB;HGNC_SYMBOL:RMDN3"
      map_id "UNIPROT:O95292;UNIPROT:Q96TC7"
      name "VAPB:RMDN3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa5"
      uniprot "UNIPROT:O95292;UNIPROT:Q96TC7"
    ]
    graphics [
      x 2017.1643052722466
      y 1430.0003759066628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95292;UNIPROT:Q96TC7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_274"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re50"
      uniprot "NA"
    ]
    graphics [
      x 2066.261045730594
      y 1215.7339762696809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_90"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re49"
      uniprot "NA"
    ]
    graphics [
      x 821.2615516861225
      y 1338.8045549237388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_247"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re118"
      uniprot "NA"
    ]
    graphics [
      x 1837.1460943193001
      y 993.6227874209993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006914;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "autophagy"
      name "autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa152"
      uniprot "NA"
    ]
    graphics [
      x 1754.8799203793951
      y 1114.600251531875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "autophagy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:RMDN3"
      map_id "UNIPROT:Q96TC7"
      name "RMDN3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa146"
      uniprot "UNIPROT:Q96TC7"
    ]
    graphics [
      x 1913.3026771464656
      y 1532.8823302662677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96TC7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_242"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "path_1_re111"
      uniprot "NA"
    ]
    graphics [
      x 1916.7264677331204
      y 1712.5759878840818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_264"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re153"
      uniprot "NA"
    ]
    graphics [
      x 1449.7250763719956
      y 1395.3014574861204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re162"
      uniprot "NA"
    ]
    graphics [
      x 1913.8282119465432
      y 1628.091375131446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0071359"
      hgnc "NA"
      map_id "presence_space_of_space_dsRNA"
      name "presence_space_of_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa288"
      uniprot "NA"
    ]
    graphics [
      x 2041.459871285222
      y 1565.1613627418592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "presence_space_of_space_dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_31"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re166"
      uniprot "NA"
    ]
    graphics [
      x 586.9621734313185
      y 1352.3085993311163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_250"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re122"
      uniprot "NA"
    ]
    graphics [
      x 1955.2483464221177
      y 956.9842887741431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re322"
      uniprot "NA"
    ]
    graphics [
      x 911.2459923077483
      y 1067.9263158080184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_59"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re333"
      uniprot "NA"
    ]
    graphics [
      x 757.4579758914213
      y 1300.3341900093574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_258"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re140"
      uniprot "NA"
    ]
    graphics [
      x 1431.9930633590272
      y 1760.9506668678746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re44"
      uniprot "NA"
    ]
    graphics [
      x 1278.8989099429516
      y 1448.8736588693737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:15456;urn:miriam:refseq:NM_003791;urn:miriam:ec-code:3.4.21.112;urn:miriam:hgnc.symbol:MBTPS1;urn:miriam:ncbigene:8720;urn:miriam:ncbigene:8720;urn:miriam:ensembl:ENSG00000140943;urn:miriam:uniprot:Q14703;urn:miriam:uniprot:Q14703"
      hgnc "HGNC_SYMBOL:MBTPS1"
      map_id "UNIPROT:Q14703"
      name "MBTPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa60"
      uniprot "UNIPROT:Q14703"
    ]
    graphics [
      x 1302.234754531823
      y 1556.642743279484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14703"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_97"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re66"
      uniprot "NA"
    ]
    graphics [
      x 668.4354204897102
      y 1111.567086263144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7; urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "UNIPROT:Q96RU7"
      name "TRIB3"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "path_0_sa485; path_0_sa486; path_0_sa484; sa257"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 490.1373518477178
      y 936.496260583431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96RU7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_42"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re253"
      uniprot "NA"
    ]
    graphics [
      x 427.5313046782039
      y 814.7425969482229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re321"
      uniprot "NA"
    ]
    graphics [
      x 1227.4092610323494
      y 875.353687334486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD;urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492;urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:ATG5;HGNC_SYMBOL:FADD;HGNC_SYMBOL:MAP1LC3A;HGNC_SYMBOL:CASP8"
      map_id "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
      name "CASP8:FADD:MAP1LC3A:SQSTM1:ATG5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa26"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
    ]
    graphics [
      x 594.5464956568609
      y 1662.0886615865084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re350"
      uniprot "NA"
    ]
    graphics [
      x 741.8874006344738
      y 1381.138012801286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_239"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_1_re108"
      uniprot "NA"
    ]
    graphics [
      x 1656.5454146112213
      y 706.5908989045886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12667446;PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re61"
      uniprot "NA"
    ]
    graphics [
      x 1456.8962462701234
      y 1653.4372781017557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:hgnc:19687;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:refseq:NM_001013703;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:ncbigene:440275;urn:miriam:ncbigene:440275;urn:miriam:ensembl:ENSG00000128829;urn:miriam:uniprot:Q9P2K8;urn:miriam:uniprot:Q9P2K8"
      hgnc "HGNC_SYMBOL:EIF2AK4"
      map_id "UNIPROT:Q9P2K8"
      name "GCN2:ATP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa35"
      uniprot "UNIPROT:Q9P2K8"
    ]
    graphics [
      x 1606.990920734448
      y 1630.491738807215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9P2K8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_89"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re46"
      uniprot "NA"
    ]
    graphics [
      x 1239.5539866078054
      y 1228.781839949269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_272"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re48"
      uniprot "NA"
    ]
    graphics [
      x 2107.5070275076323
      y 947.064465423408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_102"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re77"
      uniprot "NA"
    ]
    graphics [
      x 1487.5337358428053
      y 1740.2110925190789
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_277"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re57"
      uniprot "NA"
    ]
    graphics [
      x 1997.8285620763638
      y 660.3730563945264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19052620;urn:miriam:obo.go:GO%3A0007029"
      hgnc "NA"
      map_id "endoplasmic_space_reticulum_space_organization"
      name "endoplasmic_space_reticulum_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa109"
      uniprot "NA"
    ]
    graphics [
      x 1879.913523491703
      y 716.6699543446207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "endoplasmic_space_reticulum_space_organization"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_60"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re334"
      uniprot "NA"
    ]
    graphics [
      x 777.4586279278564
      y 1179.1160633955828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "PUBMED:9606;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_254"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re128"
      uniprot "NA"
    ]
    graphics [
      x 2035.0651873523982
      y 1733.5381571848022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_281"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re70"
      uniprot "NA"
    ]
    graphics [
      x 1696.7507275042426
      y 659.5748633662428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      name "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa336"
      uniprot "NA"
    ]
    graphics [
      x 1133.7764554425835
      y 1109.8855955128058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re356"
      uniprot "NA"
    ]
    graphics [
      x 990.3009678567071
      y 1313.6402189948494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_260"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re144"
      uniprot "NA"
    ]
    graphics [
      x 1663.3523892977373
      y 1122.6524766811979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0036503"
      hgnc "NA"
      map_id "ERAD"
      name "ERAD"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa37"
      uniprot "NA"
    ]
    graphics [
      x 928.4987812813529
      y 780.8268267659439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ERAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_81"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re371"
      uniprot "NA"
    ]
    graphics [
      x 1040.728616421543
      y 876.1407282687311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_240"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re109"
      uniprot "NA"
    ]
    graphics [
      x 1990.4421909601413
      y 888.5817245994596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:30604460"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_237"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re100"
      uniprot "NA"
    ]
    graphics [
      x 1199.6494423290458
      y 488.2077104941868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_341"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 1046.4311241599664
      y 197.77191724689465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:14647384;urn:miriam:obo.go:GO%3A0039694"
      hgnc "NA"
      map_id "viral_space_RNA_space_genome_space_replication"
      name "viral_space_RNA_space_genome_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa280"
      uniprot "NA"
    ]
    graphics [
      x 1030.3064291434605
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_RNA_space_genome_space_replication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_74"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re360"
      uniprot "NA"
    ]
    graphics [
      x 1120.1307472123444
      y 1341.0343576359612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MCU"
      map_id "UNIPROT:Q8NE86"
      name "MCU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa34"
      uniprot "UNIPROT:Q8NE86"
    ]
    graphics [
      x 1314.7433930226457
      y 753.326793210284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NE86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_270"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re46"
      uniprot "NA"
    ]
    graphics [
      x 1276.3677750753595
      y 603.9136377828574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055"
      hgnc "HGNC_SYMBOL:SMDT1"
      map_id "UNIPROT:Q9H4I9"
      name "SMDT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa35"
      uniprot "UNIPROT:Q9H4I9"
    ]
    graphics [
      x 1372.3997206037643
      y 686.6481744122913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H4I9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCU;HGNC_SYMBOL:MICU2"
      map_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
      name "MCU:MICU1:MICU2:SMDT1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa16"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    ]
    graphics [
      x 1144.7979037093294
      y 402.29787426241273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_271"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re47"
      uniprot "NA"
    ]
    graphics [
      x 2077.173866129492
      y 803.1534142847781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_251"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re123"
      uniprot "NA"
    ]
    graphics [
      x 1828.723668493866
      y 1219.0434199918845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_266"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re25"
      uniprot "NA"
    ]
    graphics [
      x 1539.4752395915975
      y 587.7139028460538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:26968367;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:hgnc:1530;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:26076;urn:miriam:hgnc.symbol:MCUB;urn:miriam:hgnc.symbol:MCUB;urn:miriam:ensembl:ENSG00000005059;urn:miriam:refseq:NM_017918;urn:miriam:ncbigene:55013;urn:miriam:ncbigene:55013;urn:miriam:uniprot:Q9NWR8;urn:miriam:uniprot:Q9NWR8;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCUB;HGNC_SYMBOL:MICU2;HGNC_SYMBOL:MCU"
      map_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
      name "Mitochondrial_space_calcium_space_uniporter_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa6"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
    ]
    graphics [
      x 1503.7570679710248
      y 348.3111018066178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384;PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_340"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 1001.2816617708233
      y 272.58409521197757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re80"
      uniprot "NA"
    ]
    graphics [
      x 1018.5332716358462
      y 1654.8537000189413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BQI3;urn:miriam:uniprot:Q9BQI3;urn:miriam:refseq:NM_014413;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK1;urn:miriam:hgnc:24921;urn:miriam:ncbigene:27102;urn:miriam:ncbigene:27102;urn:miriam:ensembl:ENSG00000086232"
      hgnc "HGNC_SYMBOL:EIF2AK1"
      map_id "UNIPROT:Q9BQI3"
      name "EIF2AK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa290"
      uniprot "UNIPROT:Q9BQI3"
    ]
    graphics [
      x 1146.4892548102441
      y 1691.191248643941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BQI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re75"
      uniprot "NA"
    ]
    graphics [
      x 764.9556108475856
      y 1796.2540190013128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_246"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re117"
      uniprot "NA"
    ]
    graphics [
      x 1809.486470843473
      y 1738.3581202528949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28132811;urn:miriam:obo.go:GO%3A0007204"
      hgnc "NA"
      map_id "Ca2_plus__space_cytosolic_space_concentration"
      name "Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa151"
      uniprot "NA"
    ]
    graphics [
      x 1641.8592600459274
      y 1886.915328153235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca2_plus__space_cytosolic_space_concentration"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re93"
      uniprot "NA"
    ]
    graphics [
      x 1227.5593979502125
      y 1520.5226321370694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:25387528;PUBMED:23027870"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_79"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re366"
      uniprot "NA"
    ]
    graphics [
      x 1296.1706944298232
      y 1808.8349762791931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0018158"
      hgnc "NA"
      map_id "hyperoxidation"
      name "hyperoxidation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa211"
      uniprot "NA"
    ]
    graphics [
      x 1155.405727098338
      y 1758.4937598668291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hyperoxidation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:22802018;PUBMED:17991856;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_91"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re50"
      uniprot "NA"
    ]
    graphics [
      x 824.6809978709214
      y 1284.3056769379705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_64"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re344"
      uniprot "NA"
    ]
    graphics [
      x 1410.8605361850587
      y 1329.2322539788886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:32169481;PUBMED:32340551"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_331"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 1259.364731954332
      y 497.545257768283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_257"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re137"
      uniprot "NA"
    ]
    graphics [
      x 1198.5035704122763
      y 547.7897039257348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_54"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re327"
      uniprot "NA"
    ]
    graphics [
      x 1319.6327421284668
      y 1068.4219707875482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_245"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re116"
      uniprot "NA"
    ]
    graphics [
      x 1483.7694180658584
      y 1881.473969761841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_244"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re114"
      uniprot "NA"
    ]
    graphics [
      x 1681.4985985945273
      y 1396.776065097615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re374"
      uniprot "NA"
    ]
    graphics [
      x 592.1580344317344
      y 1461.5125438631658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:CASP8;HGNC_SYMBOL:FADD"
      map_id "UNIPROT:Q14790;UNIPROT:Q13158"
      name "CASP8:CASP8_minus_ubq:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa25"
      uniprot "UNIPROT:Q14790;UNIPROT:Q13158"
    ]
    graphics [
      x 445.18342549181887
      y 1957.69563812609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14790;UNIPROT:Q13158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re373"
      uniprot "NA"
    ]
    graphics [
      x 536.7054654220867
      y 1844.8619973304271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11280;urn:miriam:ensembl:ENSG00000161011;urn:miriam:uniprot:Q13501;urn:miriam:uniprot:Q13501;urn:miriam:hgnc.symbol:SQSTM1;urn:miriam:ncbigene:8878;urn:miriam:refseq:NM_001142298;urn:miriam:ncbigene:8878"
      hgnc "HGNC_SYMBOL:SQSTM1"
      map_id "UNIPROT:Q13501"
      name "SQSTM1_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa238"
      uniprot "UNIPROT:Q13501"
    ]
    graphics [
      x 630.0715183971918
      y 1846.6426075460693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13501"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9976;urn:miriam:ensembl:ENSG00000204977;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:O60858;urn:miriam:uniprot:O60858;urn:miriam:ncbigene:10206;urn:miriam:ncbigene:10206;urn:miriam:hgnc.symbol:TRIM13;urn:miriam:refseq:NM_001007278"
      hgnc "HGNC_SYMBOL:TRIM13"
      map_id "UNIPROT:O60858"
      name "TRIM13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa254"
      uniprot "UNIPROT:O60858"
    ]
    graphics [
      x 403.0439454528255
      y 1887.0377337922032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60858"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_243"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re113"
      uniprot "NA"
    ]
    graphics [
      x 1778.6294475717586
      y 1274.49869702446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_238"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re103"
      uniprot "NA"
    ]
    graphics [
      x 1855.678703743434
      y 1604.7437276442831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_38"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re244"
      uniprot "NA"
    ]
    graphics [
      x 1046.306330020388
      y 1589.7943595344022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re64"
      uniprot "NA"
    ]
    graphics [
      x 216.03105163868952
      y 1172.8137955007458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_336"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 922.4393464040184
      y 427.31555410413273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re32"
      uniprot "NA"
    ]
    graphics [
      x 595.2351645341435
      y 1521.4778046144556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_275"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re51"
      uniprot "NA"
    ]
    graphics [
      x 1544.1458729811925
      y 210.47124159458622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:obo.go:GO%3A0006851"
      hgnc "NA"
      map_id "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
      name "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa31"
      uniprot "NA"
    ]
    graphics [
      x 1595.8019921537025
      y 119.10065772999019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26584763"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_98"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re67"
      uniprot "NA"
    ]
    graphics [
      x 797.0892391994437
      y 1415.33564870981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re353"
      uniprot "NA"
    ]
    graphics [
      x 1145.5142875438446
      y 1262.90774410757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030;PUBMED:32264791"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_334"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 1207.4771884342247
      y 790.7646079640874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU2"
      map_id "UNIPROT:Q8IYU8"
      name "MCU2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa33"
      uniprot "UNIPROT:Q8IYU8"
    ]
    graphics [
      x 745.5337184109665
      y 392.7448141115817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IYU8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_269"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re44"
      uniprot "NA"
    ]
    graphics [
      x 876.5538107707021
      y 367.143547558347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367"
      hgnc "HGNC_SYMBOL:MICU1"
      map_id "UNIPROT:Q9BPX6"
      name "MCU1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa32"
      uniprot "UNIPROT:Q9BPX6"
    ]
    graphics [
      x 743.6577211301762
      y 305.7601362184697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BPX6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:18191217;PUBMED:23430059;PUBMED:29450140"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re81"
      uniprot "NA"
    ]
    graphics [
      x 942.4092595524733
      y 1595.396680618654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_58"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re332"
      uniprot "NA"
    ]
    graphics [
      x 776.1449020409925
      y 966.5142528995941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_278"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re58"
      uniprot "NA"
    ]
    graphics [
      x 1335.15104535315
      y 307.4945533835545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_63"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re342"
      uniprot "NA"
    ]
    graphics [
      x 1207.0812602147275
      y 1056.708128250522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re129"
      uniprot "NA"
    ]
    graphics [
      x 1090.18666397886
      y 1652.633186774574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000125740;urn:miriam:hgnc.symbol:JUN;urn:miriam:refseq:NM_005252;urn:miriam:refseq:NM_005253;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc.symbol:FOSL1;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:FOSL2;urn:miriam:ncbigene:3726;urn:miriam:ncbigene:3726;urn:miriam:uniprot:P17535;urn:miriam:ensembl:ENSG00000170345;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:8061;urn:miriam:ncbigene:8061;urn:miriam:refseq:NM_002229;urn:miriam:refseq:NM_002228;urn:miriam:ensembl:ENSG00000075426;urn:miriam:hgnc:6206;urn:miriam:hgnc:6204;urn:miriam:hgnc:6205;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15408;urn:miriam:uniprot:P15408;urn:miriam:hgnc:13718;urn:miriam:ensembl:ENSG00000171223;urn:miriam:hgnc.symbol:FOSB;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:JUNB;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P17275;urn:miriam:refseq:NM_005438;urn:miriam:uniprot:P17275;urn:miriam:uniprot:P53539;urn:miriam:uniprot:P53539;urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:3796;urn:miriam:hgnc:3798;urn:miriam:hgnc:3797;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2354;urn:miriam:ncbigene:2354;urn:miriam:ensembl:ENSG00000175592;urn:miriam:refseq:NM_006732;urn:miriam:ensembl:ENSG00000177606"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOSL1;HGNC_SYMBOL:FOSL2;HGNC_SYMBOL:FOSB;HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUNB;HGNC_SYMBOL:JUND"
      map_id "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa260; path_0_sa261"
      uniprot "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      x 933.8676913575028
      y 1803.647773926462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re154"
      uniprot "NA"
    ]
    graphics [
      x 923.6423494980196
      y 1656.8292197337191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_262"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re147"
      uniprot "NA"
    ]
    graphics [
      x 1190.801179218221
      y 665.1693985038758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:pubmed:25704011;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:HSPA5;HGNC_SYMBOL:SIGMAR1"
      map_id "UNIPROT:P11021;UNIPROT:Q99720"
      name "SIGMAR1:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa13"
      uniprot "UNIPROT:P11021;UNIPROT:Q99720"
    ]
    graphics [
      x 1332.8799950094271
      y 560.854753976714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11021;UNIPROT:Q99720"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_23"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re107"
      uniprot "NA"
    ]
    graphics [
      x 1907.4610845169532
      y 1066.1904570207525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11723;urn:miriam:ensembl:ENSG00000139644;urn:miriam:uniprot:P55061;urn:miriam:uniprot:P55061;urn:miriam:hgnc.symbol:TMBIM6;urn:miriam:ncbigene:7009;urn:miriam:ncbigene:7009;urn:miriam:refseq:NM_003217"
      hgnc "HGNC_SYMBOL:TMBIM6"
      map_id "UNIPROT:P55061"
      name "TMBIM6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa487"
      uniprot "UNIPROT:P55061"
    ]
    graphics [
      x 1874.6293546268466
      y 1278.5502422123488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55061"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:6261;urn:miriam:refseq:NM_000540;urn:miriam:ncbigene:6261;urn:miriam:ensembl:ENSG00000196218;urn:miriam:hgnc:10483;urn:miriam:hgnc.symbol:RYR1;urn:miriam:uniprot:P21817;urn:miriam:uniprot:P21817"
      hgnc "HGNC_SYMBOL:RYR1"
      map_id "UNIPROT:P21817"
      name "RYR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa494"
      uniprot "UNIPROT:P21817"
    ]
    graphics [
      x 1908.919236743512
      y 968.7289031650337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:ITPR1;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR2;urn:miriam:ensembl:ENSG00000123104;urn:miriam:ncbigene:3710;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_002223;urn:miriam:refseq:NM_002222;urn:miriam:uniprot:Q14571;urn:miriam:uniprot:Q14571;urn:miriam:refseq:NM_002224;urn:miriam:hgnc:6180;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3708;urn:miriam:ncbigene:3708;urn:miriam:ensembl:ENSG00000150995;urn:miriam:hgnc:6181;urn:miriam:uniprot:Q14643;urn:miriam:hgnc:6182;urn:miriam:uniprot:Q14643;urn:miriam:ensembl:ENSG00000096433"
      hgnc "HGNC_SYMBOL:ITPR1;HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:ITPR2"
      map_id "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
      name "ITPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa583; path_0_sa582"
      uniprot "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      x 1922.8014345282495
      y 1244.346298302583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_61"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re337"
      uniprot "NA"
    ]
    graphics [
      x 166.68208989653124
      y 1364.1441232995378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "Translation_space_initiation"
      name "Translation_space_initiation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa96"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1321.4952060157925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Translation_space_initiation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:18940792"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_41"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re252"
      uniprot "NA"
    ]
    graphics [
      x 519.3668406920278
      y 1124.90439161748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_248"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re119"
      uniprot "NA"
    ]
    graphics [
      x 1724.5485710667626
      y 811.7599620816197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_43"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re273"
      uniprot "NA"
    ]
    graphics [
      x 1499.8070314886163
      y 1433.1239993804102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      annotation "PUBMED:18441099"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_335"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 1138.3832084659512
      y 226.62190159305771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:18441099;urn:miriam:taxonomy:9606;urn:miriam:mesh:D011658"
      hgnc "NA"
      map_id "pulmonary_space_fibrosis"
      name "pulmonary_space_fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 1175.628403037092
      y 99.66815400623716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pulmonary_space_fibrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_57"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re331"
      uniprot "NA"
    ]
    graphics [
      x 663.0126777096775
      y 1407.0686490558658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781;PUBMED:16940539"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_241"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re110"
      uniprot "NA"
    ]
    graphics [
      x 1088.4749782569531
      y 647.0515345966069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_283"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re73"
      uniprot "NA"
    ]
    graphics [
      x 1433.018949847619
      y 916.8407990458523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_78"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re364"
      uniprot "NA"
    ]
    graphics [
      x 719.0670815274225
      y 1008.475736370207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_267"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_1_re40"
      uniprot "NA"
    ]
    graphics [
      x 1442.2749867883772
      y 520.988583670562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re358"
      uniprot "NA"
    ]
    graphics [
      x 722.1237328094037
      y 1661.043773281528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:12847084"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re11"
      uniprot "NA"
    ]
    graphics [
      x 1432.820017880598
      y 1558.744713674151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_62"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re338"
      uniprot "NA"
    ]
    graphics [
      x 1030.651928455694
      y 1082.2363404750436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re355"
      uniprot "NA"
    ]
    graphics [
      x 1302.4529295507468
      y 1177.5693028177159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:11583631"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re222"
      uniprot "NA"
    ]
    graphics [
      x 952.8144544855097
      y 1898.2436401807927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_338"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 934.9304278308509
      y 280.96411990597915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_76"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re362"
      uniprot "NA"
    ]
    graphics [
      x 686.3810124745111
      y 943.4825080937712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re317"
      uniprot "NA"
    ]
    graphics [
      x 1756.8574293515176
      y 1411.66263431972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 266
    source 14
    target 27
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10415"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 27
    target 14
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_85"
      target_id "UNIPROT:P10415"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13217"
      target_id "M118_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_44"
      target_id "UNIPROT:Q13217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 13
    target 31
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "M118_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 31
    target 13
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_34"
      target_id "UNIPROT:P17861"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 1
    target 32
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 32
    target 34
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_330"
      target_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 9
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850;UNIPROT:P11021"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "Unfolded_space_protein"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 35
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "UNIPROT:P18850"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 35
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95292"
      target_id "M118_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_255"
      target_id "Ca2_plus__space_mitochondrial_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 20
    target 40
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q12933"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460;UNIPROT:Q12933"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 40
    target 20
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_45"
      target_id "UNIPROT:Q12933"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 13
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 4
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18850"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460;UNIPROT:Q12933"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 44
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43521;UNIPROT:O75460"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 42
    target 13
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_33"
      target_id "UNIPROT:P17861"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51572"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q86VP3"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 12
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Apoptosis"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_256"
      target_id "UNIPROT:P51572"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 23
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42574"
      target_id "M118_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 48
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_77"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
      target_id "M118_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_339"
      target_id "viral_space_entry_space_into_space_host_space_cell"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "MFN1"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_276"
      target_id "UNIPROT:O95140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96HE7"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 28
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 4
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18850"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_101"
      target_id "UNIPROT:Q96HE7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      target_id "M118_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_26"
      target_id "protein_space_ubiquitination_space_and_space_destruction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 19
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16539"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99683"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 60
    target 19
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_47"
      target_id "UNIPROT:Q16539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 6
    target 62
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 36
    target 62
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Unfolded_space_protein"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 62
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_35"
      target_id "UNIPROT:O75460"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 55
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96HE7"
      target_id "M118_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 63
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_27"
      target_id "UNIPROT:Q96HE7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 10
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZJ5"
      target_id "M118_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_71"
      target_id "UPR"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 8
    target 66
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75807"
      target_id "M118_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 66
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_99"
      target_id "UNIPROT:O75807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 4
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43462"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 67
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_88"
      target_id "UNIPROT:P18850"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 18
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M118_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 69
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_333"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 4
    target 70
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 70
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_73"
      target_id "UPR"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07384"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 26
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "high_space_Ca2_plus__space_cytosolic_space_concentration"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_53"
      target_id "UNIPROT:P07384"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 52
    target 74
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 74
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_279"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "ER_space_Stress"
      target_id "M118_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_65"
      target_id "Persistant_space_ER_space_Stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 5
    target 78
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021"
      target_id "M118_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_261"
      target_id "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "mitochondrial_space_fission"
      target_id "M118_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_259"
      target_id "mitochondria_space_fragmentation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 45
    target 83
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51572"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y3D6"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_252"
      target_id "UNIPROT:P51572;UNIPROT:Q9Y3D6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 6
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 20
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q12933"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16611;UNIPROT:O75460"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 88
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460;UNIPROT:Q07812"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 14
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:P10415"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 89
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q07817"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 77
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 22
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q07812"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 90
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16611"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 86
    target 41
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_32"
      target_id "UNIPROT:O75460;UNIPROT:Q12933"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 84
    target 91
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y3D6"
      target_id "M118_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_253"
      target_id "mitochondrial_space_outer_space_membrane_space_depolarization"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 7
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18848"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 93
    target 7
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_96"
      target_id "UNIPROT:P18848"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P20042"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P41091"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 98
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 96
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_93"
      target_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 24
    target 100
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55957"
      target_id "M118_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 100
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_75"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 52
    target 101
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 52
    target 101
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 101
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_249"
      target_id "UNIPROT:O95140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00535"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 75
    target 103
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "ER_space_Stress"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_106"
      target_id "UNIPROT:Q00535"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 2
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 105
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 105
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "GDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 18
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14573"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "ER_space_stress"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "Ca2_plus__space_ER_space_depletion"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 107
    target 111
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_268"
      target_id "UNIPROT:Q14573;UNIPROT:Q99720"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 6
    target 112
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 77
    target 112
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 112
    target 43
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_39"
      target_id "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 52
    target 114
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_282"
      target_id "unfolded_space_protein_space_response_space_(UPR)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 118
    target 117
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_infection"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 117
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_337"
      target_id "viral_space_entry_space_into_space_host_space_cell"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 12
    target 119
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Apoptosis"
      target_id "M118_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 119
    target 120
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_80"
      target_id "Cell_space_survival"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 109
    target 121
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "ER_space_stress"
      target_id "M118_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 121
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_280"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 11
    target 122
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45983"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y6R4"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 61
    target 122
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99683"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 122
    target 11
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_46"
      target_id "UNIPROT:P45983"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 18
    target 124
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M118_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 124
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_332"
      target_id "ER_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 52
    target 125
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_273"
      target_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43521"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 129
    target 128
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P67775"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_40"
      target_id "UNIPROT:O43521"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "release_space_of_space_ER_space_Ca2_plus_"
      target_id "M118_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 131
    target 73
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_56"
      target_id "high_space_Ca2_plus__space_cytosolic_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 127
    target 132
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43521"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 11
    target 132
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45983"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 132
    target 127
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_36"
      target_id "UNIPROT:O43521"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Met_minus_tRNA"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 99
    target 134
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 134
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_30"
      target_id "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 4
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 75
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "ER_space_Stress"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 57
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 135
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_86"
      target_id "UNIPROT:P18850"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 47
    target 136
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86VP3"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 17
    target 136
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78536"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_263"
      target_id "UNIPROT:P78536;UNIPROT:Q86VP3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4L1"
      target_id "M118_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 139
    target 138
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_55"
      target_id "UNIPROT:Q9Y4L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 25
    target 140
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55211"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 141
    target 140
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49662"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 140
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_52"
      target_id "UNIPROT:P55211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 26
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 108
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q14573"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 111
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q14573;UNIPROT:Q99720"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 142
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_265"
      target_id "Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 120
    target 144
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Cell_space_survival"
      target_id "M118_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 144
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_82"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 26
    target 145
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M118_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 145
    target 73
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_67"
      target_id "high_space_Ca2_plus__space_cytosolic_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95292;UNIPROT:Q96TC7"
      target_id "M118_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 147
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_274"
      target_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 28
    target 148
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 148
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_90"
      target_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 126
    target 149
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
      target_id "M118_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_247"
      target_id "autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96TC7"
      target_id "M118_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 152
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_242"
      target_id "Ca2_plus__space_mitochondrial_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 137
    target 153
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78536;UNIPROT:Q86VP3"
      target_id "M118_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 153
    target 17
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_264"
      target_id "UNIPROT:P78536"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 3
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19525"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "presence_space_of_space_dsRNA"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 29
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13217"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 154
    target 3
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_29"
      target_id "UNIPROT:P19525"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 98
    target 156
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 95
    target 156
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P20042"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 97
    target 156
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P41091"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 156
    target 94
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_31"
      target_id "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 52
    target 157
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 157
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_250"
      target_id "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 23
    target 158
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42574"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 141
    target 158
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49662"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 158
    target 23
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_51"
      target_id "UNIPROT:P42574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 19
    target 159
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16539"
      target_id "M118_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 159
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_59"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 47
    target 160
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86VP3"
      target_id "M118_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 160
    target 80
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_258"
      target_id "mitochondrial_space_fission"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 4
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 162
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14703"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 161
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_87"
      target_id "UNIPROT:P18850"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 7
    target 163
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18848"
      target_id "M118_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 163
    target 7
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_97"
      target_id "UNIPROT:P18848"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96RU7"
      target_id "M118_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 165
    target 164
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_42"
      target_id "UNIPROT:Q96RU7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 141
    target 166
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49662"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 71
    target 166
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07384"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 166
    target 141
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_50"
      target_id "UNIPROT:P49662"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
      target_id "M118_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 168
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_66"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 52
    target 169
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 109
    target 169
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "ER_space_stress"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 169
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_239"
      target_id "UNIPROT:O95140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 98
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 10
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NZJ5"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9P2K8"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 3
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P19525"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 29
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13217"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 170
    target 98
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_92"
      target_id "UNIPROT:P05198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 4
    target 172
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 172
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_89"
      target_id "UNIPROT:P18850"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 85
    target 173
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51572;UNIPROT:Q9Y3D6"
      target_id "M118_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 173
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_272"
      target_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 55
    target 174
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96HE7"
      target_id "M118_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 174
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_102"
      target_id "UNIPROT:Q96HE7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 52
    target 175
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_277"
      target_id "endoplasmic_space_reticulum_space_organization"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 28
    target 177
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 177
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_60"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 84
    target 178
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y3D6"
      target_id "M118_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 178
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_254"
      target_id "Ca2_plus__space_mitochondrial_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 52
    target 179
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 179
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_281"
      target_id "ER_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 180
    target 181
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      target_id "M118_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 181
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_70"
      target_id "UPR"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 45
    target 182
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51572"
      target_id "M118_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 182
    target 115
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_260"
      target_id "unfolded_space_protein_space_response_space_(UPR)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "ERAD"
      target_id "M118_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 184
    target 180
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_81"
      target_id "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 52
    target 185
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 185
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_240"
      target_id "UNIPROT:O95140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 1
    target 186
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 186
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_237"
      target_id "ER_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 1
    target 187
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 187
    target 188
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_341"
      target_id "viral_space_RNA_space_genome_space_replication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 127
    target 189
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43521"
      target_id "M118_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 189
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_74"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NE86"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 192
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H4I9"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 191
    target 193
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_270"
      target_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 52
    target 194
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95140"
      target_id "M118_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 194
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_271"
      target_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 47
    target 195
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86VP3"
      target_id "M118_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 195
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_251"
      target_id "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 26
    target 196
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 196
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_266"
      target_id "Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 1
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 51
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "viral_space_entry_space_into_space_host_space_cell"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 198
    target 1
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_340"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 98
    target 199
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 8
    target 199
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75807"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 200
    target 199
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BQI3"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 199
    target 98
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_103"
      target_id "UNIPROT:P05198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 61
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99683"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 20
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q12933"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 41
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460;UNIPROT:Q12933"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 201
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_100"
      target_id "UNIPROT:Q99683"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 151
    target 202
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96TC7"
      target_id "M118_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 202
    target 203
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_246"
      target_id "Ca2_plus__space_cytosolic_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 123
    target 204
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6R4"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 102
    target 204
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q00535"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 204
    target 123
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_105"
      target_id "UNIPROT:Q9Y6R4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 55
    target 205
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96HE7"
      target_id "M118_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 205
    target 206
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_79"
      target_id "hyperoxidation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 28
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 4
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18850"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 7
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P18848"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 77
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 7
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18848"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 77
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 13
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P17861"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 207
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_91"
      target_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 4
    target 208
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "M118_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 208
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_64"
      target_id "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 33
    target 209
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 5
    target 209
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 209
    target 49
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_331"
      target_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 5
    target 210
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 109
    target 210
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "ER_space_stress"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 1
    target 210
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 210
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_257"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 138
    target 211
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4L1"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 4
    target 211
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18850"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 211
    target 138
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_54"
      target_id "UNIPROT:Q9Y4L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 37
    target 212
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95292"
      target_id "M118_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 212
    target 203
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_245"
      target_id "Ca2_plus__space_cytosolic_space_concentration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 37
    target 213
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95292"
      target_id "M118_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 213
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_244"
      target_id "autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 28
    target 214
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 19
    target 214
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16539"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 214
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_84"
      target_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14790;UNIPROT:Q13158"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 217
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13501"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 15
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H492"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 16
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 218
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60858"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 77
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 216
    target 167
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_83"
      target_id "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 151
    target 219
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96TC7"
      target_id "M118_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 219
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_243"
      target_id "autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 37
    target 220
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95292"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 151
    target 220
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96TC7"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 220
    target 146
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_238"
      target_id "UNIPROT:O95292;UNIPROT:Q96TC7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 6
    target 221
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 127
    target 221
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43521"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 77
    target 221
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 221
    target 44
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_38"
      target_id "UNIPROT:O43521;UNIPROT:O75460"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 99
    target 222
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 21
    target 222
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 222
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 222
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "GTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 1
    target 223
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 33
    target 223
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 1
    target 223
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 223
    target 116
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_336"
      target_id "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 28
    target 224
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 19
    target 224
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16539"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 224
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_49"
      target_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 197
    target 225
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
      target_id "M118_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 225
    target 226
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_275"
      target_id "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 8
    target 227
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75807"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 7
    target 227
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18848"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 28
    target 227
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 227
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_98"
      target_id "UNIPROT:O75807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 36
    target 228
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Unfolded_space_protein"
      target_id "M118_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 228
    target 180
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_68"
      target_id "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 1
    target 229
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 17
    target 229
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P78536"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 229
    target 1
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 229
    target 1
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 230
    target 231
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IYU8"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 232
    target 231
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BPX6"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 231
    target 193
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_269"
      target_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 22
    target 233
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07812"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 11
    target 233
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45983"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 19
    target 233
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16539"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 233
    target 22
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_104"
      target_id "UNIPROT:Q07812"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 13
    target 234
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "M118_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 234
    target 183
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_58"
      target_id "ERAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 193
    target 235
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
      target_id "M118_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 235
    target 197
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_278"
      target_id "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 22
    target 236
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07812"
      target_id "M118_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 236
    target 130
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_63"
      target_id "release_space_of_space_ER_space_Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 10
    target 237
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZJ5"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 36
    target 237
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "Unfolded_space_protein"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 77
    target 237
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "Persistant_space_ER_space_Stress"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 237
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_25"
      target_id "UNIPROT:Q9NZJ5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 11
    target 239
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45983"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 28
    target 239
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 239
    target 238
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_28"
      target_id "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 5
    target 240
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 18
    target 240
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99720"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 240
    target 241
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_262"
      target_id "UNIPROT:P11021;UNIPROT:Q99720"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 26
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 243
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55061"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 244
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21817"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 245
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 242
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_23"
      target_id "Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 99
    target 246
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
      target_id "M118_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 246
    target 247
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_61"
      target_id "Translation_space_initiation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 164
    target 248
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96RU7"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 28
    target 248
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35638;UNIPROT:P0DPQ6"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 248
    target 164
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_41"
      target_id "UNIPROT:Q96RU7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 108
    target 249
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14573"
      target_id "M118_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 249
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_248"
      target_id "autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 29
    target 250
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13217"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 4
    target 250
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P18850"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 250
    target 29
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_43"
      target_id "UNIPROT:Q13217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 1
    target 251
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 251
    target 252
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_335"
      target_id "pulmonary_space_fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 13
    target 253
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "M118_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 253
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_57"
      target_id "UPR"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 5
    target 254
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 1
    target 254
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 33
    target 254
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 254
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_241"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 109
    target 255
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "ER_space_stress"
      target_id "M118_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 255
    target 115
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_283"
      target_id "unfolded_space_protein_space_response_space_(UPR)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 25
    target 256
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55211"
      target_id "M118_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 256
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_78"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 241
    target 257
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021;UNIPROT:Q99720"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 109
    target 257
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "ER_space_stress"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 110
    target 257
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "Ca2_plus__space_ER_space_depletion"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 257
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 257
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "UNIPROT:Q99720"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 41
    target 258
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460;UNIPROT:Q12933"
      target_id "M118_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 258
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_72"
      target_id "UPR"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 10
    target 259
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZJ5"
      target_id "M118_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 259
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_24"
      target_id "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 22
    target 260
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07812"
      target_id "M118_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 260
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_62"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 180
    target 261
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      target_id "M118_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 261
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_69"
      target_id "ER_space_Stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 24
    target 262
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55957"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 11
    target 262
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45983"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 262
    target 24
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_37"
      target_id "UNIPROT:P55957"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 1
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M118_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 263
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_338"
      target_id "viral_space_entry_space_into_space_host_space_cell"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 164
    target 264
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96RU7"
      target_id "M118_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 264
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_76"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 245
    target 265
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 243
    target 265
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55061"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 55
    target 265
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96HE7"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 265
    target 245
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_48"
      target_id "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
