# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 62.500000000000114
      y 1108.503126145035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 242.5645521691622
      y 1016.071674770423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4860; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:wikidata:Q25100575; urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "NA"
      map_id "Pevonedistat"
      name "Pevonedistat"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "dda75; sa54"
      uniprot "NA"
    ]
    graphics [
      x 1746.5600555131587
      y 350.27766564907176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92; sa55"
      uniprot "NA"
    ]
    graphics [
      x 1154.2523787330092
      y 1005.7559940068522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387; urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:P62877"
      name "RBX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa11; sa12; sa20; sa4"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 278.8660607146745
      y 405.3879985113638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083; urn:miriam:obo.chebi:CHEBI%3A456215; urn:miriam:obo.chebi:CHEBI%3A16027"
      hgnc "NA"
      map_id "AMP"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa301; sa97; sa224; sa45; sa216; sa288"
      uniprot "NA"
    ]
    graphics [
      x 285.4095951643651
      y 1091.4820117745362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370; urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOB"
      map_id "UNIPROT:Q15370"
      name "ELOB"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa17; sa9; sa1"
      uniprot "UNIPROT:Q15370"
    ]
    graphics [
      x 1280.9732407320926
      y 1521.1668427843583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_51"
      name "deg_EloB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1472.2537139284038
      y 1469.1160360421059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_133"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 1586.0139066119816
      y 1426.1541053764688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 1067.6485620793364
      y 1500.0492157483202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_75"
      name "diss_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1155.4578058223562
      y 1536.0852097273746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2; urn:miriam:interpro:IPR000608"
      hgnc "HGNC_SYMBOL:UBE2; NA"
      map_id "E2"
      name "E2"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa23; sa42; sa7; sa15; sa89"
      uniprot "NA"
    ]
    graphics [
      x 997.5537082500613
      y 1399.5310851706108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 1346.0568657058802
      y 1559.288105176343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593; urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1"
      map_id "UNIPROT:Q13564"
      name "NAE1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa176; sa174; sa179"
      uniprot "UNIPROT:Q13564"
    ]
    graphics [
      x 2141.207504038639
      y 477.0209702523338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13564"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_12"
      name "bind_NAE1_UBA3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE1_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1985.6256732802372
      y 397.2673564382036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470; urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:UBA3"
      map_id "UNIPROT:Q8TBC4"
      name "UBA3"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa177; sa180; sa175"
      uniprot "UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1845.1748899873837
      y 544.8536638718293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593; urn:miriam:hgnc.symbol:NAE1;urn:miriam:obo.chebi:CHEBI%3A145535;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3"
      map_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      name "NAE; NAE:Pevonedistat"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9; csa12"
      uniprot "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      x 1870.6323523252456
      y 300.24065265004026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    ]
    graphics [
      x 956.877831900868
      y 888.3912225261739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_78"
      name "diss_E3:NEDD8_E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_NEDD8_E3"
      uniprot "NA"
    ]
    graphics [
      x 890.3925175756171
      y 727.0039113083799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "CSN5"
      name "CSN5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "NA"
    ]
    graphics [
      x 929.9072226138971
      y 585.8063480906438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CSN5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      x 912.1245233503505
      y 840.7546804067281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 742.3429584850836
      y 1264.224396425308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_200"
      name "ubiquit_subs:E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 600.2393805044237
      y 1165.667294010174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    ]
    graphics [
      x 522.0853096608714
      y 1055.5016642586693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_64"
      name "deg_UBA3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1876.3486393967778
      y 665.9879583927428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_120"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1850.364616666463
      y 772.2509803789601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_56"
      name "deg_NAE1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 2162.3825637159066
      y 638.1408260180117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_117"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 2109.272477457002
      y 760.5156161672145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725; urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M"
      map_id "UNIPROT:P61081"
      name "UBE2M"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa102; sa255; sa256"
      uniprot "UNIPROT:P61081"
    ]
    graphics [
      x 1702.6221121983258
      y 1149.1343117470349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_66"
      name "deg_Ubc12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1822.7643200610505
      y 1016.281725305484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_131"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 1880.581375781111
      y 889.8587223324388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re99"
      uniprot "NA"
    ]
    graphics [
      x 949.8797770797133
      y 1286.6412997046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_140"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 787.8723023679524
      y 1220.874642271263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:0E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    ]
    graphics [
      x 920.1416341561055
      y 1609.5265223835754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_74"
      name "diss_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 989.9676143209729
      y 1499.8247966660474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      x 1082.4958300713213
      y 1338.9703931934976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "E1"
      name "E1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa22; sa14; sa6; sa41"
      uniprot "NA"
    ]
    graphics [
      x 389.6291895303865
      y 1255.1017614747577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_47"
      name "deg_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1"
      uniprot "NA"
    ]
    graphics [
      x 468.6658273210397
      y 1133.843980238564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_150"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 633.764936382694
      y 1069.1023430501252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_3"
      name "bind_E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1145.9641818298883
      y 1605.0724668496541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ubit_underscore_traget"
      name "ubit_underscore_traget"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162; sa171; sa105"
      uniprot "NA"
    ]
    graphics [
      x 1352.3752103784773
      y 904.5443221522222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubit_underscore_traget"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_72"
      name "deubiquit_subs:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deubiquit_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1423.6605072590098
      y 637.9572655537645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:DUB"
      hgnc "HGNC_SYMBOL:DUB"
      map_id "DUB"
      name "DUB"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa24; sa16; sa8"
      uniprot "NA"
    ]
    graphics [
      x 1474.458002885296
      y 396.9904645933865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DUB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648; urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC"
      map_id "UNIPROT:Q15369"
      name "ELOC"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa18; sa10; sa2"
      uniprot "UNIPROT:Q15369"
    ]
    graphics [
      x 855.8400926257256
      y 1777.2553191973393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_53"
      name "deg_EloC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC"
      uniprot "NA"
    ]
    graphics [
      x 647.4217366928885
      y 1743.7862231161926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_141"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 545.628322952054
      y 1822.9428313656401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 1455.1887634068116
      y 941.7816088028982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_81"
      name "diss_E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1409.3918387060348
      y 794.1764609932067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      name "Cul2_space_ubiquitin_space_ligase:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 1200.9775602792345
      y 725.5316753543614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB"
      map_id "UNIPROT:Q15369;UNIPROT:Q15370"
      name "ELOB:ELOC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 823.2586958722114
      y 1556.7460065407708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_86"
      name "diss_EloB:EloC"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 1036.364952222209
      y 1705.0595773640937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_48"
      name "deg_E1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 277.25484000202357
      y 1377.3556537281818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_149"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 331.6503167577654
      y 1480.338162686407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
    ]
    graphics [
      x 1260.5054864053964
      y 1142.3277720492486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_82"
      name "diss_E3:Orf10:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1253.9847013148828
      y 1000.0227182973496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 1166.7033983919955
      y 803.4221079353777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_2"
      name "bind_E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 927.3287901133338
      y 1478.1767171827091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559; urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843"
      name "NEDD8"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa118; sa178; sa173"
      uniprot "UNIPROT:Q15843"
    ]
    graphics [
      x 1347.056113375415
      y 198.81487954839997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_57"
      name "deg_NEDD8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1200.8791930499074
      y 261.1467090615055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_116"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa182"
      uniprot "NA"
    ]
    graphics [
      x 1097.3152793828635
      y 329.51729818957085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_197"
      name "ubiquit:E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 912.6074345607358
      y 1243.5642916658678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_46"
      name "deg_DUB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1469.9740048571823
      y 195.92829235298223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_152"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa39"
      uniprot "NA"
    ]
    graphics [
      x 1441.9338962386323
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_87"
      name "diss_NAE"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1974.0189099724346
      y 464.3183444080051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_11"
      name "bind_NAE_Pevonedistat"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1761.9738422610649
      y 238.81480360396313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      name "Orf10_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa278"
      uniprot "NA"
    ]
    graphics [
      x 1285.7927244985601
      y 681.2225772560339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_60"
      name "deg_Orf10_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1262.4231162967453
      y 550.6743812896216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_135"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 1222.682626913999
      y 444.12632923074983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820; urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q9C0D3"
      name "ZYG11B"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa19; sa11; sa3"
      uniprot "UNIPROT:Q9C0D3"
    ]
    graphics [
      x 512.832431044368
      y 1503.4904690869644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9C0D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_68"
      name "deg_Zyg11B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 529.4154862320017
      y 1639.081192589216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_144"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 649.556213489353
      y 1668.766413414312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_177"
      name "transcr_NEDD8"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1293.8384451859313
      y 85.60646870004791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_194"
      name "transl_Zyg11B"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 408.6123215965629
      y 1463.0892802353915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_184"
      name "transl_E1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E1"
      uniprot "NA"
    ]
    graphics [
      x 266.26801716646526
      y 1293.0094668426198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_172"
      name "transcr_E1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E1"
      uniprot "NA"
    ]
    graphics [
      x 278.6736883076204
      y 1212.8519680562447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    ]
    graphics [
      x 1229.755731421724
      y 1393.9778415602245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_71"
      name "deubiquit_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1202.1060501578447
      y 1477.9454805529244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094; urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:CUL2"
      map_id "UNIPROT:Q13617"
      name "CUL2"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "sa13; sa21; sa5"
      uniprot "UNIPROT:Q13617"
    ]
    graphics [
      x 483.52575335413167
      y 361.12082101450915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_44"
      name "deg_Cul2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 599.4590464602801
      y 334.59905137540295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_147"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 666.9314555307046
      y 237.8003709337628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_193"
      name "transl_Ubc12"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1806.4492089489584
      y 1102.4747370356354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_45"
      name "deg_DUB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1544.9140973046638
      y 261.78208329768677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_154"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 1643.2730963068775
      y 232.21457895382946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_43"
      name "deg_Cul2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 674.7298640542365
      y 347.1792853332819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_148"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 839.555546405278
      y 373.7518560805296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_173"
      name "transcr_E2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E2"
      uniprot "NA"
    ]
    graphics [
      x 899.5376051389958
      y 1320.9048689603924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_10"
      name "bind_EloB_EloC"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 1005.081606026168
      y 1653.8269508827557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_176"
      name "transcr_NAE1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 2108.5061631828285
      y 380.1185191676417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_79"
      name "diss_E3:Orf10"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1061.4261472504547
      y 797.5297511233889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_8"
      name "bind_Elo_Zyg11B"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 624.9715022030849
      y 1391.8454551782206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Zyg11B:EloBC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 679.6832816534492
      y 1129.863324526722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_52"
      name "deg_EloB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1330.5474171673686
      y 1212.7896336928293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_128"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 1327.3759200208065
      y 1033.79827511825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_70"
      name "deubiquit_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 883.6562893664272
      y 1392.8505202941258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_196"
      name "ubiquit_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E2"
      uniprot "NA"
    ]
    graphics [
      x 677.0466464977703
      y 1322.2388231679877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    ]
    graphics [
      x 1066.3175551492636
      y 1160.9807152902192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_76"
      name "diss_E2:E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1010.1554965469775
      y 1221.8285610702742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    ]
    graphics [
      x 1113.7424240145822
      y 1119.263072609647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_59"
      name "deg_Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1264.2024179510606
      y 1239.6899283834769
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_134"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 1329.691756398508
      y 1406.7333068147673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_183"
      name "transl_DUB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1461.9142321355223
      y 289.33490551259274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_170"
      name "transcr_Cul2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 580.7929022564047
      y 454.15680287393815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_174"
      name "transcr_EloB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1351.6138072633373
      y 1373.7006426650419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_85"
      name "diss_Elo:Zyg11B"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 686.4888897136871
      y 1409.4429726186816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_49"
      name "deg_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2"
      uniprot "NA"
    ]
    graphics [
      x 829.6279860399101
      y 1171.8119594425682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_191"
      name "transl_Rbx1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 162.25480532207052
      y 351.0444861992704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_67"
      name "deg_Ubc12_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1869.2273220221762
      y 1191.4596920108759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_132"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa258"
      uniprot "NA"
    ]
    graphics [
      x 1923.5220387456047
      y 1092.4638159329293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_9"
      name "bind_Elo:Zyg11B_Cul2:Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 659.3861068345487
      y 890.582726119514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:P62877"
      name "Rbx1:Cul2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 534.1584765927132
      y 698.1702981030536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_54"
      name "deg_EloC_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 742.9310256602636
      y 1617.0974129121628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_138"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 677.3487784554429
      y 1518.9317484802748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0000502"
      hgnc "NA"
      map_id "26S_minus_proteasom"
      name "26S_minus_proteasom"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "NA"
    ]
    graphics [
      x 1717.3999629030186
      y 1342.5320197086594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "26S_minus_proteasom"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_41"
      name "deg_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1768.054155462819
      y 1491.3640726364297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_136"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa268"
      uniprot "NA"
    ]
    graphics [
      x 1837.3428949473018
      y 1591.7543607694051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_199"
      name "ubiquit_subs:E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3"
      uniprot "NA"
    ]
    graphics [
      x 1160.9831092040852
      y 1272.1109612275723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_69"
      name "deg_Zyg11B_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 446.4875458064422
      y 1617.5042933813825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_142"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 589.7095278335363
      y 1592.8445767351254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_1"
      name "bind_Cul2_Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 393.4770599017394
      y 490.12500916601005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_187"
      name "transl_EloC"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloC"
      uniprot "NA"
    ]
    graphics [
      x 811.7387860091551
      y 1663.2114740121283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 1350.5492698517794
      y 1082.6545587796888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_84"
      name "diss_E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1193.1989563889751
      y 945.8990552155985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_182"
      name "transl_Cul2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 477.6364134895175
      y 456.6069808994026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
      name "NAE:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1650.0149764059877
      y 648.3811541895532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_15"
      name "bind_Ubc12_NAE:NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Ubc12_NAE_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1677.9416611483327
      y 937.3395050565457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2M;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843;UNIPROT:P61081"
      name "UBE2M:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:Q15843;UNIPROT:P61081"
    ]
    graphics [
      x 1609.6449687132217
      y 1138.1556833171016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843;UNIPROT:P61081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_58"
      name "deg_NEDD8_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1191.706609025892
      y 85.80949430963494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_115"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa181"
      uniprot "NA"
    ]
    graphics [
      x 1058.8702802701891
      y 70.20053369971754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_168"
      name "syn_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1847.0229861779637
      y 1456.2546661006888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_179"
      name "transcr_UBA3"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1777.6509041587474
      y 643.9200971048297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_88"
      name "diss_NAE:Pevonedistat"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1855.0685812451716
      y 389.3495012385299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_198"
      name "ubiquit:E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1214.7272940417167
      y 1554.856242881379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_13"
      name "bind_NEDD8_NAE"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NEDD8_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1621.1813845956553
      y 373.70836742127733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_5"
      name "bind_E3:Orf10:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1448.1805759013641
      y 1169.8417230137702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_185"
      name "transl_E2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E2"
      uniprot "NA"
    ]
    graphics [
      x 840.7742682482615
      y 1324.9945009570401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_14"
      name "bind_Orf10_E3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Orf10_E3"
      uniprot "NA"
    ]
    graphics [
      x 1064.1659569383378
      y 862.0479338067729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_77"
      name "diss_E3"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3"
      uniprot "NA"
    ]
    graphics [
      x 713.317985582305
      y 862.8523859265789
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_188"
      name "transl_NAE1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 2196.2867849607615
      y 569.7936640972889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_137"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa269"
      uniprot "NA"
    ]
    graphics [
      x 775.6809132218168
      y 438.9052450169275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_169"
      name "syn_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 806.5751578280549
      y 546.2420331836747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_180"
      name "transcr_Ubc12"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 1786.7066131306037
      y 1239.6004474311121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 566.4594823419819
      y 1247.658846914558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_83"
      name "diss_E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_sub_ub"
      uniprot "NA"
    ]
    graphics [
      x 1100.4107171943788
      y 963.2857579085113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_171"
      name "transcr_DUB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_DUB"
      uniprot "NA"
    ]
    graphics [
      x 1376.2259869119187
      y 336.7684717211258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_4"
      name "bind_E3:Orf10_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1343.5422545348517
      y 795.3777492201427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_7"
      name "bind_E3:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1513.9531027812845
      y 1277.8976309213188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_80"
      name "diss_E3:Orf10:NEDD8_E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_Orf10_NEDD8_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1077.203398706606
      y 652.6240921358582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_50"
      name "deg_E2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1035.8913932379512
      y 1559.8870845221354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_151"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa37"
      uniprot "NA"
    ]
    graphics [
      x 1113.6955706803235
      y 1463.9003131126478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_178"
      name "transcr_Rbx1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 162.47622513252463
      y 446.8638617754587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_181"
      name "transcr_Zyg11B"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 488.42156276657704
      y 1379.5415224111384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_175"
      name "transcr_EloC"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloC"
      uniprot "NA"
    ]
    graphics [
      x 751.8404340926184
      y 1833.5760325948054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_186"
      name "transl_EloB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloB"
      uniprot "NA"
    ]
    graphics [
      x 1400.8908224256988
      y 1455.5775796522557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_73"
      name "diss_Cul2:Rbx1"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 425.0569713357768
      y 542.408541501155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_63"
      name "deg_substrate:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_substrate_ub"
      uniprot "NA"
    ]
    graphics [
      x 1541.404916850959
      y 1121.0250669208028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_104"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1618.0147037764027
      y 1035.6047744652697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_42"
      name "deg_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 896.7794485459726
      y 460.74088426415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_189"
      name "transl_NEDD8"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1226.9095890073036
      y 173.1675217175689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_62"
      name "deg_Rbx1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 212.67480897864493
      y 512.9880423875995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_145"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 220.273340957389
      y 621.2280665893754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_6"
      name "bind_E3_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1172.15597381134
      y 893.9390190754206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_55"
      name "deg_NAE1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 2228.936754071912
      y 368.494218861774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_118"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 2126.859507064774
      y 293.9538937330485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_192"
      name "transl_UBA3"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1616.017161529774
      y 702.3729114747009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_61"
      name "deg_Rbx1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 269.30693124456866
      y 260.65159455043704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_146"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 383.0077307869004
      y 219.08045936652763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_65"
      name "deg_UBA3_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1702.3170088444272
      y 587.740444580088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_119"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa185"
      uniprot "NA"
    ]
    graphics [
      x 1576.3439971754797
      y 589.8845319940895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_190"
      name "transl_Orf10"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1261.0438951064102
      y 854.8987476224991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_195"
      name "ubiquit_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E1"
      uniprot "NA"
    ]
    graphics [
      x 179.65242164325514
      y 1147.7890742008276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "Ub"
      name "Ub"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa43"
      uniprot "NA"
    ]
    graphics [
      x 130.75574944820528
      y 1027.302201376036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 172
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_51"
      target_id "M119_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_12"
      target_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "CSN5"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_78"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_200"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 16
    target 25
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_64"
      target_id "M119_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 14
    target 27
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_56"
      target_id "M119_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_66"
      target_id "M119_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 12
    target 32
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_90"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
      target_id "M119_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 35
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_47"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 13
    target 40
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 12
    target 40
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 40
    target 10
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_3"
      target_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "DUB"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_72"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_53"
      target_id "M119_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 48
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 51
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 51
    target 44
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 37
    target 52
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_48"
      target_id "M119_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
      target_id "M119_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 55
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 36
    target 57
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 12
    target 57
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 57
    target 34
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_2"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_57"
      target_id "M119_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 36
    target 61
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 12
    target 61
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 61
    target 22
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_197"
      target_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 43
    target 62
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_46"
      target_id "M119_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 17
    target 64
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 64
    target 14
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 64
    target 16
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 17
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 3
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Pevonedistat"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 65
    target 17
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_11"
      target_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      target_id "M119_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_60"
      target_id "M119_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_68"
      target_id "M119_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 58
    target 72
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 72
    target 58
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_177"
      target_id "UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 69
    target 73
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 73
    target 69
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_194"
      target_id "UNIPROT:Q9C0D3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 37
    target 74
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 74
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_184"
      target_id "E1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 37
    target 75
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 75
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_172"
      target_id "E1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      target_id "M119_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 77
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 77
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_44"
      target_id "M119_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 29
    target 81
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 81
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_193"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 43
    target 82
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_45"
      target_id "M119_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 78
    target 84
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_43"
      target_id "M119_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 12
    target 86
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 86
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_173"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 7
    target 87
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 44
    target 87
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 87
    target 50
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_10"
      target_id "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 14
    target 88
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 88
    target 14
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_176"
      target_id "UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 49
    target 89
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M119_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 89
    target 4
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "Orf10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 89
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 50
    target 90
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 69
    target 90
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_8"
      target_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 7
    target 92
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_52"
      target_id "M119_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 22
    target 94
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 94
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 94
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 12
    target 95
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 37
    target 95
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 95
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_196"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 95
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_196"
      target_id "E1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
      target_id "M119_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 97
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 4
    target 99
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M119_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_59"
      target_id "M119_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 43
    target 101
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 101
    target 43
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_183"
      target_id "DUB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 78
    target 102
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 102
    target 78
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_170"
      target_id "UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 7
    target 103
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 103
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_174"
      target_id "UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 91
    target 104
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 104
    target 50
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 104
    target 69
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "UNIPROT:Q9C0D3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 12
    target 105
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 105
    target 39
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_49"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 5
    target 106
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 106
    target 5
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_191"
      target_id "UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 29
    target 107
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 107
    target 108
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_67"
      target_id "M119_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 91
    target 109
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 109
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_9"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 44
    target 111
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_54"
      target_id "M119_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "26S_minus_proteasom"
      target_id "M119_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_41"
      target_id "M119_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 76
    target 116
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      target_id "M119_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 116
    target 96
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_199"
      target_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 69
    target 117
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_69"
      target_id "M119_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 5
    target 119
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 78
    target 119
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 119
    target 110
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_1"
      target_id "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 44
    target 120
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 120
    target 44
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_187"
      target_id "UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 122
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 122
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 78
    target 123
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 123
    target 78
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_182"
      target_id "UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 29
    target 125
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_15"
      target_id "UNIPROT:Q15843;UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 58
    target 127
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_58"
      target_id "M119_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 115
    target 129
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_136"
      target_id "M119_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 129
    target 113
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_168"
      target_id "26S_minus_proteasom"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 16
    target 130
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 130
    target 16
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_179"
      target_id "UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 17
    target 131
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 131
    target 3
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_88"
      target_id "Pevonedistat"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 131
    target 17
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_88"
      target_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 13
    target 132
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 12
    target 132
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 132
    target 76
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_198"
      target_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 58
    target 133
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 17
    target 133
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 133
    target 124
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_13"
      target_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 47
    target 134
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 126
    target 134
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:P61081"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 134
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 134
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 12
    target 135
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 135
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_185"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 21
    target 136
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 4
    target 136
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 136
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_14"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 21
    target 137
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 137
    target 110
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 137
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 14
    target 138
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 138
    target 14
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_188"
      target_id "UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_137"
      target_id "M119_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 140
    target 20
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_169"
      target_id "CSN5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 29
    target 141
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 141
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_180"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 37
    target 142
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 142
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_89"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 98
    target 143
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
      target_id "M119_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 143
    target 18
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 143
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 43
    target 144
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 144
    target 43
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_171"
      target_id "DUB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 49
    target 145
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 41
    target 145
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 145
    target 47
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_4"
      target_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 126
    target 146
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:P61081"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 121
    target 146
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 146
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 146
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 56
    target 147
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 20
    target 147
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "CSN5"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 147
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_80"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 12
    target 148
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_50"
      target_id "M119_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 5
    target 150
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 150
    target 5
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_178"
      target_id "UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 69
    target 151
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 151
    target 69
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_181"
      target_id "UNIPROT:Q9C0D3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 44
    target 152
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 152
    target 44
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_175"
      target_id "UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 7
    target 153
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 153
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_186"
      target_id "UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 110
    target 154
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 154
    target 78
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 154
    target 5
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 41
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 113
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "26S_minus_proteasom"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_63"
      target_id "M119_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 20
    target 157
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "CSN5"
      target_id "M119_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 157
    target 139
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_42"
      target_id "M119_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 58
    target 158
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 158
    target 58
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_189"
      target_id "UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 5
    target 159
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_62"
      target_id "M119_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 41
    target 161
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 21
    target 161
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 161
    target 121
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_6"
      target_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 14
    target 162
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_55"
      target_id "M119_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 16
    target 164
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 164
    target 16
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_192"
      target_id "UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 5
    target 165
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_61"
      target_id "M119_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 16
    target 167
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_65"
      target_id "M119_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 66
    target 169
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      target_id "M119_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 169
    target 4
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_190"
      target_id "Orf10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 37
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Ub"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 2
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 170
    target 37
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "E1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 170
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 170
    target 1
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
