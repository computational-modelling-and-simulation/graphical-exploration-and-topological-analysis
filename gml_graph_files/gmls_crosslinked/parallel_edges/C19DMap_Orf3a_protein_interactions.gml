# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 17
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3"
      name "O_minus_glycosyl_space_3a_space_tetramer; O_minus_glycosyl_space_3a; 3a; 3a:membranous_space_structure; GalNAc_minus_O_minus_3a; ORF3a; Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2458; layout_2461; layout_2399; layout_2403; layout_2332; layout_2392; layout_2459; layout_2455; layout_2395; cb0cc; ac4ba; ed8aa; sa1873; sa2247; sa1; sa169; sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1236.9143968613196
      y 144.41463928528196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1351.5785233945871
      y 438.22150823739275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455; urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "NA; HGNC_SYMBOL:TLR3"
      map_id "UNIPROT:O15455"
      name "TLR3; TLR3_underscore_TRIF; TLR3_underscore_TRIF_underscore_RIPK1; TLR3:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "ab922; csa37; sa239; csa38; sa5; csa88; sa45; sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 827.1003485559153
      y 981.9927666282692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15455"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O00206; urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "NA; HGNC_SYMBOL:TLR4"
      map_id "UNIPROT:O00206"
      name "TLR4; TLR4_underscore_TRIF_underscore_TRAM"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c7d93; csa47; sa291; sa142"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 1302.162150659933
      y 1107.0289959998113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "UNIPROT:P01574"
      name "IFNB1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa3; sa27; sa26; sa34; sa33; sa147; sa89; sa87; sa91; sa88"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1391.8438985260063
      y 1598.2048243556878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01574"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:E protein interactions; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "UNIPROT:E"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131; sa13; sa32; sa22; sa19; sa83; sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 955.8847560581137
      y 312.98106530614064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206; urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "UNIPROT:P19838;UNIPROT:Q04206"
      name "p50_underscore_p65; NFKB; P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21; csa80; csa103; csa7"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1129.7230767567398
      y 177.36824454743748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838;UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "UNIPROT:Q13114"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82; sa83; sa493; sa11; sa116; sa99; sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1040.8073519442373
      y 273.6726003213074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "UNIPROT:Q99836"
      name "MYD88_underscore_TRAM; MYD88"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa20; sa57; sa77; sa433; sa143; sa100; sa49; sa144"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1330.1210672667376
      y 919.2224910910581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99836"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6; urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "UNIPROT:Q9Y4K3"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59; sa58; sa428; sa265; sa337; sa422; sa129; sa135; sa101; sa38"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1181.815559626837
      y 1299.132529532686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4K3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P19838"
      name "NFKB1; p105; p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269; sa483; sa81; sa151; sa84; sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 472.3414502725524
      y 1531.2032825784172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "UNIPROT:P25963"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa475; sa481; sa65; sa64"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 304.9162163705749
      y 1580.847513687957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25963"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "ssRNA"
      name "ssRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14; sa42"
      uniprot "NA"
    ]
    graphics [
      x 813.7246667057545
      y 658.8575788596595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ssRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720; urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
      name "IKK_space_Complex; NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa99; csa12; csa20"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 544.0955443450487
      y 1520.5033458358785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "UNIPROT:Q8IUC6"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa418; sa91; sa419; sa94; sa48"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 958.9938261639305
      y 699.0023028021077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUC6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 701.6444021686318
      y 1029.0003410344189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "double_minus_stranded_space_RNA"
      name "double_minus_stranded_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 583.457866699673
      y 1033.7764716052147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "double_minus_stranded_space_RNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1426.664619912845
      y 1049.7202038541686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IRF6"
      map_id "UNIPROT:O14896"
      name "IRF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:O14896"
    ]
    graphics [
      x 1307.2743657283058
      y 1025.486923313201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:11850;urn:miriam:hgnc:6121;urn:miriam:hgnc:7562;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468;urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:IRF6;HGNC_SYMBOL:MYD88;HGNC_SYMBOL:TLR4"
      map_id "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
      name "LPS_slash_TLR4_slash_MYD88"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
    ]
    graphics [
      x 1437.786533842582
      y 1192.520399670882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_53"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 1278.7674313099901
      y 421.98619744099017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 1327.904030229395
      y 288.41181681126636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1331.4076497507704
      y 1285.569851928905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_22"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 1262.46310733276
      y 209.32212010013245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:Coagulation pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538; urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553; urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "UNIPROT:P01584"
      name "IL1b; proIL_minus_1B; IL_minus_1B; IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127; sa92; sa15; sa17; sa172; sa21; sa244; sa462; sa464; sa460; sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1224.3042436098485
      y 325.827807209889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG"
      map_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
      name "IKBKG_slash_IKBKB_slash_CHUK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22; csa4"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 875.4961048003775
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 721.2813460979781
      y 148.20280586909064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5991;urn:miriam:hgnc:6121;urn:miriam:hgnc:11916;urn:miriam:hgnc:5991;urn:miriam:refseq:NM_000575;urn:miriam:hgnc.symbol:IL1A;urn:miriam:hgnc.symbol:IL1A;urn:miriam:uniprot:P01583;urn:miriam:uniprot:P01583;urn:miriam:ensembl:ENSG00000115008;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:7132;urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IL1A;HGNC_SYMBOL:TNFRSF1A;HGNC_SYMBOL:IRF6"
      map_id "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
      name "LPS_slash_TNF_space__alpha__slash_IL_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
    ]
    graphics [
      x 621.3334204183504
      y 256.60027204691823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166167;urn:miriam:uniprot:Q9Y297;urn:miriam:uniprot:Q9Y297;urn:miriam:hgnc:1144;urn:miriam:hgnc.symbol:BTRC;urn:miriam:hgnc.symbol:BTRC;urn:miriam:ncbigene:8945;urn:miriam:ncbigene:8945;urn:miriam:refseq:NM_033637"
      hgnc "HGNC_SYMBOL:BTRC"
      map_id "UNIPROT:Q9Y297"
      name "BTRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137; sa75"
      uniprot "UNIPROT:Q9Y297"
    ]
    graphics [
      x 185.68983385063825
      y 1501.0667361177675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1545.480636722404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16288;urn:miriam:ec-code:2.3.2.27;urn:miriam:ensembl:ENSG00000100505;urn:miriam:ncbigene:114088;urn:miriam:ncbigene:114088;urn:miriam:uniprot:Q9C026;urn:miriam:uniprot:Q9C026;urn:miriam:hgnc.symbol:TRIM9;urn:miriam:refseq:NM_015163;urn:miriam:hgnc.symbol:TRIM9"
      hgnc "HGNC_SYMBOL:TRIM9"
      map_id "UNIPROT:Q9C026"
      name "TRIM9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa74"
      uniprot "UNIPROT:Q9C026"
    ]
    graphics [
      x 147.27060399261165
      y 1621.1323884736603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9C026"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:22539786;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 949.0901476107448
      y 897.5167880118794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "UNIPROT:O00635"
      name "TRIM38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50; sa52"
      uniprot "UNIPROT:O00635"
    ]
    graphics [
      x 1057.8577852835633
      y 1012.4449219646223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1196.9714208503847
      y 858.6740193417309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR7;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR8"
      map_id "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
    ]
    graphics [
      x 1048.7940206893757
      y 819.1013145260712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_172016;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:56658;urn:miriam:ncbigene:56658;urn:miriam:hgnc:10065;urn:miriam:uniprot:Q9HCM9;urn:miriam:uniprot:Q9HCM9;urn:miriam:ensembl:ENSG00000204599"
      hgnc "HGNC_SYMBOL:TRIM39"
      map_id "UNIPROT:Q9HCM9"
      name "TRIM39"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76; sa139"
      uniprot "UNIPROT:Q9HCM9"
    ]
    graphics [
      x 197.9662340820389
      y 1106.2686055286124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HCM9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 207.98630993455288
      y 993.8839630542708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 17
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292; urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292; urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "UNIPROT:P29466"
      name "CASP1; Caspase_minus_1_space_Tetramer; CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa82; sa25; csa5; sa100; sa168; sa170; sa171; sa169; csa7; sa384; sa383; csa94; csa93; sa457; sa376; sa385; sa382"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1076.5843695168996
      y 528.3489657172441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29466"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 957.7429739597383
      y 604.9978067312807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3; urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "UNIPROT:Q9ULZ3"
      name "PYCARD; ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14; sa131; sa103; sa104; sa375"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1054.575705896812
      y 454.5365013791806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9ULZ3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q96P20"
      name "NLRP3"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa10; sa134; sa92; sa101; sa362; sa468; sa525; sa361; sa367"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 807.2802879292758
      y 717.3389902475428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 968.654930583299
      y 147.74729109134284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1148.1726037676133
      y 288.21415181773216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf3a protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2; urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "UNIPROT:P08697"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86; sa87; sa412"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 1188.4898505450956
      y 1712.8061546402091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08697"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1050.5967842822006
      y 1691.816570181686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:6859;urn:miriam:hgnc:30681;urn:miriam:hgnc:17075;urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25;urn:miriam:ensembl:ENSG00000157625;urn:miriam:ncbigene:257397;urn:miriam:ncbigene:257397;urn:miriam:hgnc.symbol:TAB3;urn:miriam:hgnc.symbol:TAB3;urn:miriam:uniprot:Q8N5C8;urn:miriam:uniprot:Q8N5C8;urn:miriam:hgnc:30681;urn:miriam:refseq:NM_152787"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:MAP3K7;HGNC_SYMBOL:TAB3"
      map_id "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
      name "TAB2_slash_TAB3_slash_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10; csa19"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      x 931.7428049387404
      y 1597.7879607825444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1317.2863676808652
      y 1692.7695588714153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 963.9105449844866
      y 482.2207626919292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:22588174"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 1401.272856885433
      y 730.7948270558509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM59;urn:miriam:hgnc.symbol:TRIM59;urn:miriam:uniprot:Q8IWR1;urn:miriam:uniprot:Q8IWR1;urn:miriam:ensembl:ENSG00000213186;urn:miriam:pubmed:22588174;urn:miriam:hgnc:30834;urn:miriam:ncbigene:286827;urn:miriam:refseq:NM_173084;urn:miriam:ncbigene:286827"
      hgnc "HGNC_SYMBOL:TRIM59"
      map_id "UNIPROT:Q8IWR1"
      name "TRIM59"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3; sa96"
      uniprot "UNIPROT:Q8IWR1"
    ]
    graphics [
      x 1429.9564031668292
      y 535.7502795234459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IWR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1392.8694939574025
      y 327.162338738402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780;PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1069.8604743496537
      y 65.21620708832006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 388.27316020801055
      y 1441.4806389715086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CACTIN;urn:miriam:hgnc.symbol:CACTIN;urn:miriam:uniprot:Q8WUQ7;urn:miriam:uniprot:Q8WUQ7;urn:miriam:ensembl:ENSG00000105298;urn:miriam:refseq:NM_001080543;urn:miriam:hgnc:29938;urn:miriam:ncbigene:58509;urn:miriam:ncbigene:58509"
      hgnc "HGNC_SYMBOL:CACTIN"
      map_id "UNIPROT:Q8WUQ7"
      name "CACTIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa138; sa77"
      uniprot "UNIPROT:Q8WUQ7"
    ]
    graphics [
      x 316.9305280937804
      y 1323.1514973399474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WUQ7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKBIA;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P25963;UNIPROT:P19838"
      name "NFKB1:NFKNIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      x 407.67210878557023
      y 1572.6159140639531
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25963;UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868;PUBMED:22539786;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1187.7786024949933
      y 1097.4306835210332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM23"
      map_id "UNIPROT:P36406"
      name "TRIM23"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P36406"
    ]
    graphics [
      x 1120.6081095775319
      y 1192.4390837468159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P36406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 256.00689276240064
      y 1212.3508766897285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 338.7478788160444
      y 1497.3011378140018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1096.4423776275703
      y 361.7344076543949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:31231549"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 648.0183827234944
      y 739.4353269618218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58760"
      hgnc "NA"
      map_id "ORF8b"
      name "ORF8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 530.0152052406528
      y 728.3035101617681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ORF8b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_63"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 913.2500494091071
      y 196.9810658733253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:15316659;PUBMED:17715238;PUBMED:25375324;PUBMED:19590927"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 1057.410576711484
      y 141.0760721709587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:14583;urn:miriam:uniprot:J9TC74;urn:miriam:hgnc:5013;urn:miriam:hgnc:20266;urn:miriam:hgnc:24048;urn:miriam:hgnc:20593;urn:miriam:ncbigene:23155;urn:miriam:ncbigene:23155;urn:miriam:hgnc:29675;urn:miriam:refseq:NM_015127;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:uniprot:Q96S66;urn:miriam:uniprot:Q96S66;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:ensembl:ENSG00000121940;urn:miriam:uniprot:Q9UH99;urn:miriam:uniprot:Q9UH99;urn:miriam:hgnc:14210;urn:miriam:ensembl:ENSG00000100242;urn:miriam:refseq:NM_001199579;urn:miriam:ncbigene:25777;urn:miriam:ncbigene:25777;urn:miriam:hgnc.symbol:SUN2;urn:miriam:hgnc.symbol:SUN2;urn:miriam:ncbigene:151188;urn:miriam:ncbigene:151188;urn:miriam:hgnc:24048;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:uniprot:Q8N6S5;urn:miriam:uniprot:Q8N6S5;urn:miriam:refseq:NM_152522;urn:miriam:ensembl:ENSG00000177917;urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18;urn:miriam:uniprot:Q96JC1;urn:miriam:uniprot:Q96JC1;urn:miriam:hgnc.symbol:VPS39;urn:miriam:refseq:NM_015289;urn:miriam:hgnc.symbol:VPS39;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ncbigene:23339;urn:miriam:ncbigene:23339;urn:miriam:hgnc:20593;urn:miriam:hgnc:14583;urn:miriam:ncbigene:55823;urn:miriam:refseq:NM_021729;urn:miriam:ncbigene:55823;urn:miriam:ensembl:ENSG00000160695;urn:miriam:hgnc.symbol:VPS11;urn:miriam:hgnc.symbol:VPS11;urn:miriam:uniprot:Q9H270;urn:miriam:uniprot:Q9H270;urn:miriam:hgnc.symbol:ALG5;urn:miriam:hgnc.symbol:ALG5;urn:miriam:ensembl:ENSG00000120697;urn:miriam:hgnc:20266;urn:miriam:ncbigene:29880;urn:miriam:ncbigene:29880;urn:miriam:uniprot:Q9Y673;urn:miriam:uniprot:Q9Y673;urn:miriam:ec-code:2.4.1.117;urn:miriam:refseq:NM_013338"
      hgnc "HGNC_SYMBOL:CLCC1;HGNC_SYMBOL:SUN2;HGNC_SYMBOL:ARL6IP6;HGNC_SYMBOL:HMOX1;HGNC_SYMBOL:VPS39;HGNC_SYMBOL:VPS11;HGNC_SYMBOL:ALG5"
      map_id "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
      name "Hops_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
    ]
    graphics [
      x 966.3655210565969
      y 246.76176956181644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR8;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR7"
      map_id "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
    ]
    graphics [
      x 745.6869384471436
      y 749.6430065509908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:21782231"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 883.0926702964424
      y 783.1262128208791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:18345001;PUBMED:25172371;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 1057.911693350518
      y 1450.765088984063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P15533;urn:miriam:hgnc:10059;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "UNIPROT:P15533;UNIPROT:O00635"
      name "TRIM30a_slash_TRIM38"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P15533;UNIPROT:O00635"
    ]
    graphics [
      x 1133.4196607362317
      y 1532.4286392486108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15533;UNIPROT:O00635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:27695001;PUBMED:26358190;PUBMED:23408607;PUBMED:23758787;PUBMED:24379373;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 708.7510381741229
      y 1637.072163118553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:16379;urn:miriam:uniprot:Q8IYM9;urn:miriam:uniprot:Q8IYM9;urn:miriam:ensembl:ENSG00000132274;urn:miriam:refseq:NM_006074;urn:miriam:ncbigene:10346;urn:miriam:ncbigene:10346;urn:miriam:hgnc.symbol:TRIM22;urn:miriam:hgnc.symbol:TRIM22"
      hgnc "HGNC_SYMBOL:TRIM22"
      map_id "UNIPROT:Q8IYM9"
      name "TRIM22"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa56"
      uniprot "UNIPROT:Q8IYM9"
    ]
    graphics [
      x 793.6972956765449
      y 1556.3787021564906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IYM9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:23650;urn:miriam:ncbigene:23650;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:refseq:NM_012101;urn:miriam:uniprot:Q14134;urn:miriam:uniprot:Q14134;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:ensembl:ENSG00000137699;urn:miriam:hgnc:17274"
      hgnc "HGNC_SYMBOL:TRIM29"
      map_id "UNIPROT:Q14134"
      name "TRIM29"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa67"
      uniprot "UNIPROT:Q14134"
    ]
    graphics [
      x 655.4193618238289
      y 1525.5057691941024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16283;urn:miriam:hgnc:660;urn:miriam:uniprot:Q14142;urn:miriam:uniprot:Q14142;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ensembl:ENSG00000106785;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ncbigene:9830;urn:miriam:refseq:NM_014788;urn:miriam:ncbigene:9830;urn:miriam:hgnc:16283;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM14;HGNC_SYMBOL:TRIM23"
      map_id "UNIPROT:Q14142;UNIPROT:P36406"
      name "TRIM14_slash_TRIM23"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q14142;UNIPROT:P36406"
    ]
    graphics [
      x 817.9027571050332
      y 1630.4870431329728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14142;UNIPROT:P36406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9975;urn:miriam:hgnc:11312;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:ensembl:ENSG00000204713;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:hgnc:9975;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_030950;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P14373;urn:miriam:uniprot:P14373;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P19474;urn:miriam:uniprot:P19474;urn:miriam:ncbigene:6737;urn:miriam:ncbigene:6737;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:11312;urn:miriam:refseq:NM_003141;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:ensembl:ENSG00000132109"
      hgnc "HGNC_SYMBOL:TRIM27;HGNC_SYMBOL:TRIM21"
      map_id "UNIPROT:P14373;UNIPROT:P19474"
      name "TRIM27_slash_TRIM21"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P14373;UNIPROT:P19474"
    ]
    graphics [
      x 729.8644011917385
      y 1511.5648763857203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P14373;UNIPROT:P19474"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 75
    source 3
    target 16
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15455"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "double_minus_stranded_space_RNA"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 16
    target 3
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_34"
      target_id "UNIPROT:O15455"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 4
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00206"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14896"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 9
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_27"
      target_id "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_53"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 1
    target 22
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC3"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 22
    target 2
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_46"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 10
    target 23
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 20
    target 23
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 23
    target 10
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_30"
      target_id "UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 7
    target 24
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M116_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_22"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_45"
      target_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y297"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q9C026"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_21"
      target_id "UNIPROT:Q9Y297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 15
    target 32
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IUC6"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 3
    target 32
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:O15455"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:O00635"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 32
    target 15
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_35"
      target_id "UNIPROT:Q8IUC6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 9
    target 34
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 34
    target 9
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_38"
      target_id "UNIPROT:Q99836"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCM9"
      target_id "M116_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_25"
      target_id "UNIPROT:Q9HCM9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 41
    target 39
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_29"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 26
    target 42
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 6
    target 42
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:E"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 42
    target 7
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_23"
      target_id "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 40
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 8
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13114"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 1
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC3"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 43
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_19"
      target_id "UNIPROT:Q9ULZ3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08697"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_42"
      target_id "UNIPROT:P08697"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 5
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 44
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P08697"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 47
    target 5
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_33"
      target_id "UNIPROT:P01574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 8
    target 48
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 15
    target 48
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q8IUC6"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 48
    target 8
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_43"
      target_id "UNIPROT:Q13114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 9
    target 49
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q8IWR1"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 49
    target 9
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_28"
      target_id "UNIPROT:Q99836"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IWR1"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 1
    target 51
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC3"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_36"
      target_id "UNIPROT:Q8IWR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 8
    target 52
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13114"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 26
    target 52
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 1
    target 52
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC3"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 52
    target 7
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_18"
      target_id "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 11
    target 53
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 12
    target 53
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25963"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q8WUQ7"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_44"
      target_id "UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 10
    target 56
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 9
    target 56
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q99836"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 33
    target 56
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:O00635"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P36406"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 56
    target 10
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_39"
      target_id "UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 54
    target 58
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8WUQ7"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 36
    target 58
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9HCM9"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 58
    target 54
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_24"
      target_id "UNIPROT:Q8WUQ7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 12
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25963"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 29
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y297"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 14
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 59
    target 12
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_31"
      target_id "UNIPROT:P25963"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 25
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01584"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 38
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P29466"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 6
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:E"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 7
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 60
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_32"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 41
    target 61
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "ORF8b"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 61
    target 41
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_20"
      target_id "UNIPROT:Q96P20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_63"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 1
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P0DTC3"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_26"
      target_id "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 13
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "ssRNA"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 67
    target 35
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_37"
      target_id "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 46
    target 68
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:P15533;UNIPROT:O00635"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 10
    target 68
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 68
    target 46
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_40"
      target_id "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 14
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 46
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q8IYM9"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 72
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q14134"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 73
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q14142;UNIPROT:P36406"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 74
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:P14373;UNIPROT:P19474"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 70
    target 14
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_41"
      target_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
