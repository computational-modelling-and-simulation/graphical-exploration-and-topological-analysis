# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H492; urn:miriam:obo.chebi:16038;urn:miriam:uniprot:Q9H492; urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492"
      hgnc "NA; HGNC_SYMBOL:MAP1LC3A"
      map_id "UNIPROT:Q9H492"
      name "LC3; b35a3; MAP1LC3A"
      node_subtype "GENE; COMPLEX; PROTEIN"
      node_type "species"
      org_id "b3be1; b35a3; path_0_sa239"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 62.5
      y 1126.9660999099845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H492"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7b795d10"
      uniprot "NA"
    ]
    graphics [
      x 130.9243792709127
      y 1224.4043838131274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038"
      hgnc "NA"
      map_id "PE"
      name "PE"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa8aa"
      uniprot "NA"
    ]
    graphics [
      x 92.52069512394917
      y 1325.0526625892776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8WYN0;urn:miriam:uniprot:O95352;urn:miriam:uniprot:Q9NT62"
      hgnc "NA"
      map_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
      name "b392b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b392b"
      uniprot "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      x 249.05449360323388
      y 1280.0515463546049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817;urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
      name "e7545"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7545"
      uniprot "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 493.68525570245095
      y 1389.8484066576054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1056"
      uniprot "NA"
    ]
    graphics [
      x 367.019447248681
      y 1341.9673175699652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "UNIPROT:O94817"
      name "ATG12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2b7f"
      uniprot "UNIPROT:O94817"
    ]
    graphics [
      x 1010.0911493888891
      y 1326.3001343990013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O94817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a613c"
      uniprot "NA"
    ]
    graphics [
      x 914.2002108928358
      y 1405.8003939673024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H1Y0; urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849"
      hgnc "NA; HGNC_SYMBOL:ATG5"
      map_id "UNIPROT:Q9H1Y0"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c82d3; path_0_sa241"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 933.5111480381993
      y 1278.4522459638838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H0Y0;urn:miriam:uniprot:O95352"
      hgnc "NA"
      map_id "UNIPROT:Q9H0Y0;UNIPROT:O95352"
      name "e453d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e453d"
      uniprot "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    ]
    graphics [
      x 846.1001834049779
      y 1310.0533178193289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
      name "a2002"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a2002"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      x 773.3196806368226
      y 1423.9986042767327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f0c56"
      uniprot "NA"
    ]
    graphics [
      x 630.4861780875389
      y 1411.1897262037705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
      name "ac403"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac403"
      uniprot "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 656.2943548276164
      y 1516.0099172671212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q5MNZ9;urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
      name "a61b2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a61b2"
      uniprot "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
    ]
    graphics [
      x 704.7553795549635
      y 923.2415731749138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id8d83e948"
      uniprot "NA"
    ]
    graphics [
      x 609.0272621151287
      y 982.2497358002403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophagosome"
      name "Autophagosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e8965"
      uniprot "NA"
    ]
    graphics [
      x 493.0846343324992
      y 1017.2080378631948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q14457;urn:miriam:uniprot:Q8NEB9;urn:miriam:uniprot:Q99570"
      hgnc "NA"
      map_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
      name "ac27b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac27b"
      uniprot "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    ]
    graphics [
      x 414.79590728762264
      y 98.76394759553489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3266321"
      uniprot "NA"
    ]
    graphics [
      x 537.2384149521638
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "UNIPROT:Q9HBF4"
      name "ZFYVE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f5ed9"
      uniprot "UNIPROT:Q9HBF4"
    ]
    graphics [
      x 643.002810215252
      y 120.27647555736678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8TDY2;urn:miriam:uniprot:O75385;urn:miriam:uniprot:O75143;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      name "autophagy_br_initiation_br_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ce113"
      uniprot "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 393.86800109902435
      y 334.1936265735137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4f4ebe11"
      uniprot "NA"
    ]
    graphics [
      x 338.1784429065792
      y 207.09558099307537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "d2646"
      name "d2646"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2646"
      uniprot "NA"
    ]
    graphics [
      x 446.2367351019473
      y 235.1485236833289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d2646"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "EDEMosome"
      name "EDEMosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4176"
      uniprot "NA"
    ]
    graphics [
      x 1191.439056931115
      y 607.7830304759254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EDEMosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id26b440cd"
      uniprot "NA"
    ]
    graphics [
      x 1224.9841948101289
      y 720.9120688662023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "DMV_space__br_double_space_membrane_space__br_vesicle"
      name "DMV_space__br_double_space_membrane_space__br_vesicle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e5087"
      uniprot "NA"
    ]
    graphics [
      x 1156.6098116082617
      y 811.7324319638996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DMV_space__br_double_space_membrane_space__br_vesicle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbfaf590f"
      uniprot "NA"
    ]
    graphics [
      x 506.95866503890124
      y 405.21701753952783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:P42345"
      hgnc "NA"
      map_id "UNIPROT:P42345"
      name "MTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "dc0f7; a9b05"
      uniprot "UNIPROT:P42345"
    ]
    graphics [
      x 493.3461961276495
      y 523.5828614688803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:ensembl:ENSG00000175224;urn:miriam:uniprot:O75385;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "UNIPROT:O75385;UNIPROT:Q8IYT8"
      name "fccb6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fccb6"
      uniprot "UNIPROT:O75385;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 614.042944441935
      y 474.77368737063466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75385;UNIPROT:Q8IYT8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5a4fd155"
      uniprot "NA"
    ]
    graphics [
      x 401.68291529072224
      y 585.2423430593547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Endoplasmic_space_Reticulum"
      name "Endoplasmic_space_Reticulum"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ceb35"
      uniprot "NA"
    ]
    graphics [
      x 1004.9703398658108
      y 505.67501509010845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Endoplasmic_space_Reticulum"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1d777c00"
      uniprot "NA"
    ]
    graphics [
      x 1117.5841087491099
      y 515.9285797047389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:24991833"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idceed974e"
      uniprot "NA"
    ]
    graphics [
      x 365.98239939925884
      y 1005.2128274722047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Lysozome"
      name "Lysozome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b5087"
      uniprot "NA"
    ]
    graphics [
      x 253.06249393500786
      y 971.6910059735304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lysozome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "b2eb5"
      name "b2eb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b2eb5"
      uniprot "NA"
    ]
    graphics [
      x 315.75124124945785
      y 893.2323214271098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b2eb5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autolysosome"
      name "Autolysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c2baf"
      uniprot "NA"
    ]
    graphics [
      x 419.19345132835815
      y 901.2795879157169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autolysosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 36
    source 1
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H492"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 3
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "PE"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 4
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 2
    target 1
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_34"
      target_id "UNIPROT:Q9H492"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 5
    target 6
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
      target_id "W7_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 6
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_25"
      target_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 7
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O94817"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 9
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 10
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9H0Y0;UNIPROT:O95352"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 8
    target 11
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_2"
      target_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 11
    target 12
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 13
    target 12
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 12
    target 5
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_24"
      target_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 14
    target 15
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
      target_id "W7_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 15
    target 16
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_35"
      target_id "Autophagosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 17
    target 18
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
      target_id "W7_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 18
    target 19
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_31"
      target_id "UNIPROT:Q9HBF4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 20
    target 21
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 22
    target 21
    cd19dm [
      diagram "WP4863"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d2646"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 21
    target 17
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_32"
      target_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 23
    target 24
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "EDEMosome"
      target_id "W7_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 24
    target 25
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_30"
      target_id "DMV_space__br_double_space_membrane_space__br_vesicle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 20
    target 26
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 27
    target 26
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P42345"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 26
    target 28
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_36"
      target_id "UNIPROT:O75385;UNIPROT:Q8IYT8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 27
    target 29
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42345"
      target_id "W7_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 29
    target 27
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_33"
      target_id "UNIPROT:P42345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 30
    target 31
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "Endoplasmic_space_Reticulum"
      target_id "W7_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 31
    target 23
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_29"
      target_id "EDEMosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 16
    target 32
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "Autophagosome"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 33
    target 32
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "Lysozome"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 34
    target 32
    cd19dm [
      diagram "WP4863"
      edge_type "INHIBITION"
      source_id "b2eb5"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 32
    target 35
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_37"
      target_id "Autolysosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
