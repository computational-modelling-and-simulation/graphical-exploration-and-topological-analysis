# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 393.4648664011267
      y 804.5913298060834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861; WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "BCL2"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "eef8e; e21a3"
      uniprot "NA"
    ]
    graphics [
      x 95.99478519032891
      y 750.3246801548747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BCL2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4877; WP4880; C19DMap:JNK pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416; urn:miriam:uniprot:P59633; urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "UNIPROT:P59633"
      name "ee4e9; 3b; Orf3b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "ee4e9; e6060; sa75; sa356; sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1657.762043047884
      y 604.8053368785226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id81131143"
      uniprot "NA"
    ]
    graphics [
      x 1604.0173905811903
      y 497.5861024241626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5594"
      hgnc "NA"
      map_id "f9084"
      name "f9084"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9084"
      uniprot "NA"
    ]
    graphics [
      x 1477.327697210674
      y 441.3493957309212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f9084"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5602"
      hgnc "NA"
      map_id "ca580"
      name "ca580"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca580"
      uniprot "NA"
    ]
    graphics [
      x 318.1153151558427
      y 774.0477103247646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ca580"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_44"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2520366d"
      uniprot "NA"
    ]
    graphics [
      x 488.56827425750254
      y 678.8426655224697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000170345;urn:miriam:ensembl:ENSG00000177606;urn:miriam:ncbigene:3726"
      hgnc "NA"
      map_id "d382f"
      name "d382f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d382f"
      uniprot "NA"
    ]
    graphics [
      x 666.3638750514784
      y 592.9934909755461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d382f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000085511;urn:miriam:ensembl:ENSG00000095015"
      hgnc "NA"
      map_id "c8921"
      name "c8921"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c8921"
      uniprot "NA"
    ]
    graphics [
      x 755.4561601790246
      y 1280.331055150144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c8921"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_63"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9808cc66"
      uniprot "NA"
    ]
    graphics [
      x 624.8435505683453
      y 1237.4863470325145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000065559"
      hgnc "NA"
      map_id "MAP2K4"
      name "MAP2K4"
      node_subtype "GENE"
      node_type "species"
      org_id "bed55"
      uniprot "NA"
    ]
    graphics [
      x 573.2458299522698
      y 1104.5069286792555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAP2K4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000117676;urn:miriam:ensembl:ENSG00000177189;urn:miriam:ensembl:ENSG00000071242"
      hgnc "NA"
      map_id "dd506"
      name "dd506"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd506"
      uniprot "NA"
    ]
    graphics [
      x 1341.4946979211863
      y 779.6775930237455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dd506"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc1c0f088"
      uniprot "NA"
    ]
    graphics [
      x 1269.1823264599627
      y 664.5824596814814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cell_space_survival_space__br_and_space_proliferation"
      name "Cell_space_survival_space__br_and_space_proliferation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8558"
      uniprot "NA"
    ]
    graphics [
      x 1229.331912035
      y 544.5623388889444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cell_space_survival_space__br_and_space_proliferation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_58"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id79ad2aef"
      uniprot "NA"
    ]
    graphics [
      x 754.1470844950645
      y 1061.754856627132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5600;urn:miriam:ncbigene:6300;urn:miriam:ensembl:ENSG00000112062;urn:miriam:ncbigene:5603"
      hgnc "NA"
      map_id "f53ff"
      name "f53ff"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f53ff"
      uniprot "NA"
    ]
    graphics [
      x 939.0152212152269
      y 979.5837880381616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f53ff"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4877; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185885; urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "IFITM1"
      name "IFITM1"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "cf41c; b8cbc; a93c7; e997b"
      uniprot "NA"
    ]
    graphics [
      x 498.1675401732404
      y 418.3794300650407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide8866e40"
      uniprot "NA"
    ]
    graphics [
      x 552.8461698021154
      y 522.2713799209698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "IFITM3"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f50e2; f1794"
      uniprot "NA"
    ]
    graphics [
      x 912.6526125831166
      y 600.2186685668048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad2dd"
      uniprot "NA"
    ]
    graphics [
      x 795.9343863945043
      y 598.778959700988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_61"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id81035407"
      uniprot "NA"
    ]
    graphics [
      x 239.24270297718147
      y 740.9393773704395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "UNIPROT:P59635;UNIPROT:P59632"
      name "e11b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e11b1"
      uniprot "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      x 888.1669104030966
      y 1327.5322850151406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635;UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_70"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcac13c17"
      uniprot "NA"
    ]
    graphics [
      x 893.6494535627756
      y 1147.612528558032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_MERS,_space__br_229E_space_infection"
      name "SARS,_space_MERS,_space__br_229E_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5c80"
      uniprot "NA"
    ]
    graphics [
      x 1020.8620457529323
      y 1517.480477483831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_MERS,_space__br_229E_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfb2829ab"
      uniprot "NA"
    ]
    graphics [
      x 910.8491545589042
      y 1466.4737794414577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000034152;urn:miriam:ensembl:ENSG00000108984"
      hgnc "NA"
      map_id "e6b7b"
      name "e6b7b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e6b7b"
      uniprot "NA"
    ]
    graphics [
      x 1043.2617295752816
      y 1277.5296473462063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e6b7b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id198c85f"
      uniprot "NA"
    ]
    graphics [
      x 1018.4728472202066
      y 1128.3002200459778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_229E_br_IBV_space_infection"
      name "SARS,_space_229E_br_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e046c"
      uniprot "NA"
    ]
    graphics [
      x 299.172977266578
      y 505.98181966060554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_229E_br_IBV_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided0c8de7"
      uniprot "NA"
    ]
    graphics [
      x 271.73630192593066
      y 391.853194022954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000076984"
      hgnc "NA"
      map_id "MAP2K7"
      name "MAP2K7"
      node_subtype "GENE"
      node_type "species"
      org_id "ac39a"
      uniprot "NA"
    ]
    graphics [
      x 172.07635996262582
      y 419.44001803707806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAP2K7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "IFITM2"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acf10; f247c"
      uniprot "NA"
    ]
    graphics [
      x 802.0318058984975
      y 710.4512943590246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ea117"
      uniprot "NA"
    ]
    graphics [
      x 688.3498718754532
      y 704.640617408025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_46"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id352f2389"
      uniprot "NA"
    ]
    graphics [
      x 206.15855514161524
      y 596.1732201958403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_43"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id1b1215f4"
      uniprot "NA"
    ]
    graphics [
      x 1085.9726068497966
      y 1411.1431460678286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id44f6de9d"
      uniprot "NA"
    ]
    graphics [
      x 1082.6711527530856
      y 985.0350966018324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000151247"
      hgnc "NA"
      map_id "EIF4E"
      name "EIF4E"
      node_subtype "GENE"
      node_type "species"
      org_id "ce80a"
      uniprot "NA"
    ]
    graphics [
      x 1220.300747338022
      y 964.5066692562862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF4E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000175197"
      hgnc "NA"
      map_id "DDIT3"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "a158c"
      uniprot "NA"
    ]
    graphics [
      x 655.9173873980742
      y 930.8394649198326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DDIT3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_73"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide06bd151"
      uniprot "NA"
    ]
    graphics [
      x 525.7849320221987
      y 873.0055700136917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_64"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2a9cdb0"
      uniprot "NA"
    ]
    graphics [
      x 1348.1406500957207
      y 362.95904034169564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000115966;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "b90b1"
      name "b90b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b90b1"
      uniprot "NA"
    ]
    graphics [
      x 1206.2764990290566
      y 300.9815519595089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b90b1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_MERS,_space_229E_br_infection"
      name "SARS,_space_MERS,_space_229E_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bf897"
      uniprot "NA"
    ]
    graphics [
      x 1570.045601640228
      y 192.256602035284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_MERS,_space_229E_br_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_55"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id62eba7d3"
      uniprot "NA"
    ]
    graphics [
      x 1488.2050836256826
      y 94.08048659989197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000169032;urn:miriam:ensembl:ENSG00000126934"
      hgnc "NA"
      map_id "e5ca8"
      name "e5ca8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e5ca8"
      uniprot "NA"
    ]
    graphics [
      x 1370.9287957043791
      y 129.31953082549262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e5ca8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_71"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idda26811d"
      uniprot "NA"
    ]
    graphics [
      x 903.9648317808208
      y 1238.4237551593487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000132155"
      hgnc "NA"
      map_id "RAF1"
      name "RAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fd989"
      uniprot "NA"
    ]
    graphics [
      x 1102.3094612400273
      y 71.04913922902438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RAF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idbeaaca30"
      uniprot "NA"
    ]
    graphics [
      x 1240.407659095207
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_57"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id6fd5bed2"
      uniprot "NA"
    ]
    graphics [
      x 806.9563685338177
      y 799.813822916818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:wikidata:Q89458416;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
      name "afd54"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afd54"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    ]
    graphics [
      x 409.15656230950407
      y 935.9616902406643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_65"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idac31cf2f"
      uniprot "NA"
    ]
    graphics [
      x 294.756801046813
      y 908.6161627525105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id559f5826"
      uniprot "NA"
    ]
    graphics [
      x 774.3373240898418
      y 416.8250403929673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Innate_br_immunity"
      name "Innate_br_immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc6a0"
      uniprot "NA"
    ]
    graphics [
      x 906.9399475610886
      y 313.5673766078448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Innate_br_immunity"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id589b86fe"
      uniprot "NA"
    ]
    graphics [
      x 1149.784789695278
      y 861.3118563908522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_br_infection"
      name "SARS_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f9996"
      uniprot "NA"
    ]
    graphics [
      x 1267.4716272145158
      y 789.9849410146809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_br_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_77"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ideee7f970"
      uniprot "NA"
    ]
    graphics [
      x 1400.7899859730737
      y 643.6587952386017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_47"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id392d909b"
      uniprot "NA"
    ]
    graphics [
      x 1352.0050350653607
      y 932.3157281417547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Protein_br_synthesis"
      name "Protein_br_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5ffe"
      uniprot "NA"
    ]
    graphics [
      x 1499.9793432580548
      y 955.9093988274532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Protein_br_synthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_50"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4b1d8b89"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 878.124710353073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4877; WP5038; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:wikipathways:WP4936; urn:miriam:obo.go:GO%3A0006914"
      hgnc "NA"
      map_id "Autophagy"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3; d0d85; sa18"
      uniprot "NA"
    ]
    graphics [
      x 161.90184322916923
      y 957.7449608490444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide37bb3b6"
      uniprot "NA"
    ]
    graphics [
      x 182.64201966419728
      y 818.5434649137394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_72"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide012a8a5"
      uniprot "NA"
    ]
    graphics [
      x 415.39894682685707
      y 997.8285071077055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id63f652b7"
      uniprot "NA"
    ]
    graphics [
      x 1419.5543353918097
      y 897.3572238580056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "BST2"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "a24ed; b17a4"
      uniprot "NA"
    ]
    graphics [
      x 649.3572233263193
      y 321.07027364227616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BST2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1e34"
      uniprot "NA"
    ]
    graphics [
      x 641.9258698203694
      y 444.8674693750926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c30bf1f"
      uniprot "NA"
    ]
    graphics [
      x 1572.7301457534388
      y 325.9758921894764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000173327;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ensembl:ENSG00000006432"
      hgnc "NA"
      map_id "b9bb1"
      name "b9bb1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9bb1"
      uniprot "NA"
    ]
    graphics [
      x 446.2401361794268
      y 261.24662923201964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b9bb1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_45"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id264ad72f"
      uniprot "NA"
    ]
    graphics [
      x 291.346616953165
      y 293.23238712454395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_48"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3d6c0762"
      uniprot "NA"
    ]
    graphics [
      x 1055.2254655172983
      y 279.1767672938743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_67"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc07c34c7"
      uniprot "NA"
    ]
    graphics [
      x 1439.5517286922218
      y 274.48757446778313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_60"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id80dca1f4"
      uniprot "NA"
    ]
    graphics [
      x 1211.2871205979945
      y 421.47860158267275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_51"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54d4a9a9"
      uniprot "NA"
    ]
    graphics [
      x 308.4824293151277
      y 634.0194851041315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_54"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id60fca41b"
      uniprot "NA"
    ]
    graphics [
      x 799.4261481519662
      y 956.2114921485905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "IFI27"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "f93d3; c5ac8"
      uniprot "NA"
    ]
    graphics [
      x 867.4267236830714
      y 459.04253003793804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFI27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc72f872e"
      uniprot "NA"
    ]
    graphics [
      x 755.456568687065
      y 505.72378220207764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 74
    source 3
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59633"
      target_id "W9_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 4
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_62"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 6
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "ca580"
      target_id "W9_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 7
    target 8
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_44"
      target_id "d382f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 9
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "c8921"
      target_id "W9_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 10
    target 11
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_63"
      target_id "MAP2K4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 12
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "dd506"
      target_id "W9_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 13
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_68"
      target_id "Cell_space_survival_space__br_and_space_proliferation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 11
    target 15
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K4"
      target_id "W9_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 15
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_58"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 17
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM1"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 8
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 18
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_75"
      target_id "IFITM1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 19
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM3"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 8
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 20
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_6"
      target_id "IFITM3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 2
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BCL2"
      target_id "W9_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 21
    target 1
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_61"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 22
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59635;UNIPROT:P59632"
      target_id "W9_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 23
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_70"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 24
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space__br_229E_space_infection"
      target_id "W9_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 25
    target 22
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_78"
      target_id "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 26
    target 27
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "e6b7b"
      target_id "W9_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 27
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_42"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 28
    target 29
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_229E_br_IBV_space_infection"
      target_id "W9_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 29
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_76"
      target_id "MAP2K7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 31
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM2"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 8
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 32
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_31"
      target_id "IFITM2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 30
    target 33
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K7"
      target_id "W9_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 33
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_46"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 24
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space__br_229E_space_infection"
      target_id "W9_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 34
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_43"
      target_id "e6b7b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 16
    target 35
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 35
    target 36
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_49"
      target_id "EIF4E"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 37
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "DDIT3"
      target_id "W9_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 38
    target 1
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_73"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 5
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f9084"
      target_id "W9_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 39
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_64"
      target_id "b90b1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 41
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space_229E_br_infection"
      target_id "W9_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 42
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_55"
      target_id "e5ca8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 9
    target 44
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "c8921"
      target_id "W9_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 44
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_71"
      target_id "e6b7b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 45
    target 46
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "RAF1"
      target_id "W9_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 46
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_66"
      target_id "e5ca8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 16
    target 47
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 47
    target 8
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_57"
      target_id "d382f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 48
    target 49
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
      target_id "W9_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 49
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_65"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 8
    target 50
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "d382f"
      target_id "W9_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 50
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_52"
      target_id "Innate_br_immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 16
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 53
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_br_infection"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 52
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_53"
      target_id "dd506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 5
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f9084"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 53
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "INHIBITION"
      source_id "SARS_br_infection"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 54
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_77"
      target_id "dd506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 36
    target 55
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "EIF4E"
      target_id "W9_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 55
    target 56
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_47"
      target_id "Protein_br_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 2
    target 57
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BCL2"
      target_id "W9_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 57
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_50"
      target_id "Autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 6
    target 59
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "ca580"
      target_id "W9_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 59
    target 2
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_74"
      target_id "BCL2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 11
    target 60
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K4"
      target_id "W9_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 60
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_72"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 12
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "dd506"
      target_id "W9_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 61
    target 56
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_56"
      target_id "Protein_br_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 62
    target 63
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BST2"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 8
    target 63
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 63
    target 62
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_34"
      target_id "BST2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 41
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space_229E_br_infection"
      target_id "W9_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 64
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_59"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 65
    target 66
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b9bb1"
      target_id "W9_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 66
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_45"
      target_id "MAP2K7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 40
    target 67
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b90b1"
      target_id "W9_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 67
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_48"
      target_id "Innate_br_immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 43
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "e5ca8"
      target_id "W9_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 68
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_67"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 40
    target 69
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b90b1"
      target_id "W9_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 69
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_60"
      target_id "Cell_space_survival_space__br_and_space_proliferation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 28
    target 70
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_229E_br_IBV_space_infection"
      target_id "W9_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 70
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_51"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 16
    target 71
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 71
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_54"
      target_id "DDIT3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 72
    target 73
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFI27"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 8
    target 73
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 73
    target 72
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_69"
      target_id "IFI27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
