# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 47
      diagram "R-HSA-9678108; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9682469;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694618;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694327;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9687385;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682451;urn:miriam:reactome:R-COV-9694597;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682616;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713302;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713651;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686016;urn:miriam:reactome:R-COV-9694302;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9681663;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694725; urn:miriam:reactome:R-COV-9694255;urn:miriam:reactome:R-COV-9680476;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684877;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694428; urn:miriam:reactome:R-COV-9682205;urn:miriam:reactome:R-COV-9694735;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713306;urn:miriam:reactome:R-COV-9713634;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713313;urn:miriam:reactome:R-COV-9713643;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694361;urn:miriam:reactome:R-COV-9682227;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686003;urn:miriam:reactome:R-COV-9694366;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694770;urn:miriam:reactome:R-COV-9686011;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713317;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713652;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9682229; urn:miriam:reactome:R-COV-9684862;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682241;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682253;urn:miriam:reactome:R-COV-9694404;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382;urn:miriam:uniprot:P0C6X7; urn:miriam:pubmed:31138817;urn:miriam:reactome:R-COV-9680807;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682545;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694688;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682215;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694358;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694731;urn:miriam:reactome:R-COV-9682668;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713644;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713311;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658; urn:miriam:reactome:R-COV-9694269;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682566;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694458;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681659;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694784;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684866;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694760;urn:miriam:reactome:R-COV-9682222;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694577;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684876;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682245;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9680806;urn:miriam:pubmed:31138817;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9683433;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694740;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9683403;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694728;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684343;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694407; urn:miriam:reactome:R-COV-9681588;urn:miriam:uniprot:P0C6U8;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694479;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694748; urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      name "SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand; SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors; nsp7:nsp8:nsp12:nsp14:nsp10; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13; m7GpppA_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; RTC; SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer; SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors; nsp3; N_minus_glycan_space_nsp3; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC; N_minus_glycan_space_nsp4; nsp6; nsp3:nsp4; SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_1_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC; SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; nsp9; nsp9_space_dimer; nsp8; SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors; nsp7:nsp8:nsp12; nsp10:nsp14; nsp10; SARS_space_coronavirus_space_gRNA_space_with_space_secondary_space_structure:RTC; SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC; SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide; SARS_minus_CoV_minus_1_space_gRNA:RTC; nsp1_minus_4; nsp2; nsp1; nsp4; nsp7; nsp7:nsp8; nsp16:nsp10; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15; nsp3:nsp4:nsp6; 3CLp_space_dimer; 3CLp_space_dimer:Î±_minus_Ketoamides; nsp3_minus_4; N_minus_glycan_space_nsp3_minus_4; nsp5; PLPro"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_141; layout_1411; layout_113; layout_115; layout_2103; layout_458; layout_138; layout_1266; layout_53; layout_57; layout_2081; layout_2093; layout_66; layout_1488; layout_449; layout_450; layout_2074; layout_2097; layout_2099; layout_71; layout_74; layout_98; layout_170; layout_1383; layout_106; layout_111; layout_108; layout_124; layout_2086; layout_144; layout_132; layout_41; layout_81; layout_46; layout_44; layout_54; layout_101; layout_104; layout_121; layout_119; layout_451; layout_14; layout_504; layout_45; layout_50; layout_1897; sa101"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1421.7155583257165
      y 1048.4053697456675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 21
      diagram "R-HSA-9678108; C19DMap:Nsp9 protein interactions; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694642;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678281; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684355;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678280; urn:miriam:reactome:R-COV-9684874;urn:miriam:reactome:R-COV-9694600;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682244;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682196;urn:miriam:reactome:R-COV-9694504;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684861;urn:miriam:reactome:R-COV-9694695;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682242;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694372;urn:miriam:reactome:R-COV-9682216;urn:miriam:uniprot:P0C6X7; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7; urn:miriam:doi:10.1101/2020.05.18.102467;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0C6X7"
      name "pp1ab_minus_nsp13; nsp15_space_hexamer; pp1ab_minus_nsp14; pp1ab; pp1ab_minus_nsp16; pp1ab_minus_nsp12; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; pp1ab_minus_nsp15; pp1ab_minus_nsp9; pp1ab_minus_nsp7; pp1ab_minus_nsp5; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp8; pp1ab_minus_nsp6; pp1ab_minus_nsp10; Nsp14; Nsp16; inhibited_space_ribosomal_space_40S_space_SU; Nsp1; inhibited_space_80S_space_ribosome"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_32; layout_117; layout_29; layout_5; layout_35; layout_34; layout_727; layout_30; layout_33; layout_37; layout_28; layout_27; layout_31; layout_2066; layout_38; sa1378; sa1426; sa1374; csa15; sa70; csa16"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 835.6177233138405
      y 447.9509298800999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP5038; WP5039; C19DMap:Virus replication cycle; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5; urn:miriam:uniprot:P0DTC5; urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "UNIPROT:P0DTC5"
      name "nascent_space_M; M; N_minus_glycan_space_M; M_space_lattice; membrane_br_glycoprotein; Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2338; layout_2416; layout_2419; layout_2463; layout_2469; d614c; d1b58; cedc7; sa2061; sa2116; sa2024; sa2066; sa1686; sa1891; sa1857; sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1048.3992462256
      y 1464.6415298133172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725311; NA"
      hgnc "NA"
      map_id "nsp16"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4; abf4b; c8bd9; glyph36; sa81"
      uniprot "NA"
    ]
    graphics [
      x 169.5696779198164
      y 693.5105621445846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; WP5038; WP5039; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q2819370; NA"
      hgnc "NA"
      map_id "dsRNA"
      name "dsRNA"
      node_subtype "RNA; UNKNOWN"
      node_type "species"
      org_id "a90b0; ff0ff; cb56c; sa6; sa26; sa27; sa456; sa93; sa82"
      uniprot "NA"
    ]
    graphics [
      x 957.9107634634073
      y 1064.0202476318977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4868; WP5039; C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ISGs"
      name "ISGs"
      node_subtype "PROTEIN; UNKNOWN; RNA"
      node_type "species"
      org_id "f1bfb; ad145; sa15; sa14"
      uniprot "NA"
    ]
    graphics [
      x 1027.6686052422974
      y 293.1193819179157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ISGs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; NA"
      hgnc "NA"
      map_id "nsp14"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9; glyph54; sa80"
      uniprot "NA"
    ]
    graphics [
      x 379.60563604266963
      y 540.0781254703622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 6
      diagram "WP5039; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pfam:PF15177; NA"
      hgnc "NA"
      map_id "IFN_minus_III"
      name "IFN_minus_III"
      node_subtype "GENE; RNA; PROTEIN"
      node_type "species"
      org_id "ae4ce; b3038; sa90; sa91; sa96; sa97"
      uniprot "NA"
    ]
    graphics [
      x 1466.5710895737118
      y 1404.4951581003438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_III"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 19
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "UNIPROT:Q14653"
      name "IRF3_underscore_homodimer; IRF3; IFNB1_space_expression_space_complex; p38_minus_NFkB"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa46; sa121; sa286; csa45; sa120; sa348; sa119; sa156; sa157; sa379; sa256; csa8; sa85; sa84; sa89; csa41; sa87; sa88; sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1557.4015910331336
      y 1667.779120068306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:Q9UHD2"
      name "TBK1; STING:TBK1"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa61; sa60; sa426; sa117; sa490; sa154; sa80; csa20; csa5; sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1784.305923268485
      y 623.3628234125358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:NLRP3 inflammasome activation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:Q7Z434"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151; sa156; sa127; sa128; sa102; sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1443.9425701563416
      y 1299.2887048966672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415; urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "UNIPROT:P42224"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa305; sa68; sa4; sa5"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 606.0699129366232
      y 1426.0885431125669
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581; urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "UNIPROT:P52630"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306; sa69; sa7; sa6"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 486.87617506186405
      y 1462.9681633422554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52630"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1; urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "UNIPROT:Q9BYX4"
      name "IFIH1; IFIH1:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa198; sa154; csa93; sa439; sa78"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 231.65518406361423
      y 950.6178486955475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYX4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:O95786"
      name "DDX58; DDX58:dsRNA; RIG_minus_I:dsRNA; RIG_minus_I"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa200; sa155; sa23; csa92; sa124; sa440; csa37; csa36; sa99"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1252.6166102599598
      y 1237.35209275896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227; urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:JAK1"
      map_id "UNIPROT:P23458"
      name "JAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9; sa36"
      uniprot "UNIPROT:P23458"
    ]
    graphics [
      x 467.06897444148717
      y 1614.8019427980864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23458"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597; urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:TYK2"
      map_id "UNIPROT:P29597"
      name "TYK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19; sa37"
      uniprot "UNIPROT:P29597"
    ]
    graphics [
      x 290.723734725712
      y 1441.383891100747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29597"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131; urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:IRF9"
      map_id "UNIPROT:Q00978"
      name "IRF9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70; sa10"
      uniprot "UNIPROT:Q00978"
    ]
    graphics [
      x 739.9414751060044
      y 1492.036436694228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00978"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "p38_minus_NFkB"
      name "p38_minus_NFkB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "NA"
    ]
    graphics [
      x 1310.666771421408
      y 1613.7043567708329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "p38_minus_NFkB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1428.0828889985046
      y 1710.94802541756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940;urn:miriam:ensembl:ENSG00000185745;urn:miriam:hgnc:5407;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:uniprot:P09914;urn:miriam:uniprot:P09914;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:refseq:NM_001548;urn:miriam:ncbigene:3434;urn:miriam:ncbigene:3434;urn:miriam:refseq:NM_080657;urn:miriam:hgnc:30908;urn:miriam:ncbigene:91543;urn:miriam:ncbigene:91543;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:ensembl:ENSG00000134321;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000126709;urn:miriam:ncbigene:2537;urn:miriam:ncbigene:2537;urn:miriam:refseq:NM_022873;urn:miriam:uniprot:P09912;urn:miriam:uniprot:P09912;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc:4054;urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347;urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:OAS2;HGNC_SYMBOL:OAS3;HGNC_SYMBOL:IFIT1;HGNC_SYMBOL:RSAD2;HGNC_SYMBOL:OAS1;HGNC_SYMBOL:IFI6;HGNC_SYMBOL:IRF7;HGNC_SYMBOL:IRF1;HGNC_SYMBOL:EIF2AK2"
      map_id "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
    ]
    graphics [
      x 968.8450773979707
      y 75.2131507417622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 847.5350231919938
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "antiviral_space_proteins"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa59"
      uniprot "NA"
    ]
    graphics [
      x 789.760870377273
      y 165.01191443430866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "antiviral_space_proteins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_74"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1158.3960770999429
      y 331.52132413627874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1668.4263114088762
      y 1493.0917513159404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:DDX58;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1730.4136062571438
      y 1299.9264453480428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2"
      map_id "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
    ]
    graphics [
      x 1947.734369190362
      y 980.7282739832812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_71"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1964.9557344304742
      y 1123.0990207673728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q8IZI9;urn:miriam:uniprot:Q8IZI9;urn:miriam:refseq:NM_172139;urn:miriam:ncbigene:282617;urn:miriam:ncbigene:282617;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc:18365;urn:miriam:ensembl:ENSG00000197110;urn:miriam:refseq:NM_172138;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:uniprot:Q8IZJ0;urn:miriam:uniprot:Q8IZJ0;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:hgnc:18364;urn:miriam:ncbigene:282616;urn:miriam:ensembl:ENSG00000183709;urn:miriam:ncbigene:282616;urn:miriam:uniprot:Q8IU54;urn:miriam:uniprot:Q8IU54;urn:miriam:ncbigene:282618;urn:miriam:refseq:NM_172140;urn:miriam:ncbigene:282618;urn:miriam:ensembl:ENSG00000182393;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc:18363;urn:miriam:ensembl:ENSG00000272395;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc:44480;urn:miriam:ncbigene:101180976;urn:miriam:uniprot:K9M1U5;urn:miriam:uniprot:K9M1U5;urn:miriam:ncbigene:101180976;urn:miriam:refseq:NM_001276254"
      hgnc "HGNC_SYMBOL:IFNL3;HGNC_SYMBOL:IFNL2;HGNC_SYMBOL:IFNL1;HGNC_SYMBOL:IFNL4"
      map_id "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
      name "Ifn_space_lambda"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    ]
    graphics [
      x 1831.8108131454273
      y 1212.8626252467766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1"
      map_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
    ]
    graphics [
      x 2031.7028550850823
      y 1030.9385521695904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "UNIPROT:P10914"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94; sa95; sa127; sa92"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1214.2929120453657
      y 1561.9457625352843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10914"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1061.9613941175257
      y 1411.9666662032353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:DDX58;HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:O95786;UNIPROT:Q7Z434"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21; csa22"
      uniprot "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      x 942.9586897302668
      y 1312.035297751298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95786;UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_56"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1107.1891238968703
      y 1625.8987690200297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ribosomal_space_60S_space_subunit"
      name "ribosomal_space_60S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "NA"
    ]
    graphics [
      x 1307.0465761675525
      y 531.2848370386239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ribosomal_space_60S_space_subunit"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_59"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1200.8462262256626
      y 450.9895299891465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ribosomal_space_40S_space_subunit"
      name "ribosomal_space_40S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "NA"
    ]
    graphics [
      x 1111.01642968031
      y 558.363348767165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ribosomal_space_40S_space_subunit"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "80S_space_ribosome"
      name "80S_space_ribosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 1093.4304635629915
      y 325.6047483505947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "80S_space_ribosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT1"
      map_id "UNIPROT:P52630;UNIPROT:P42224"
      name "ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P52630;UNIPROT:P42224"
    ]
    graphics [
      x 801.3888033953396
      y 1590.5471678260667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52630;UNIPROT:P42224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 989.2115100718651
      y 1729.3847153499005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT3"
      map_id "UNIPROT:P40763"
      name "STAT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:P40763"
    ]
    graphics [
      x 1162.161000046989
      y 1676.268161501936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40763"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT3"
      map_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
      name "Inhibited_space_ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
    ]
    graphics [
      x 1102.7102617419835
      y 1857.6120497026195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT2"
      map_id "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
    ]
    graphics [
      x 729.2179544161847
      y 1143.2593951963026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_30"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 781.3147294488105
      y 924.4553293194477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT1"
      map_id "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
    ]
    graphics [
      x 862.9203695021729
      y 715.2927284464763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_55"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1648.6590050468549
      y 1618.681185272901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1451.6921273623161
      y 1188.5709111784975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:Q7Z434;UNIPROT:O95786"
      name "RIG_minus_I_minus_mitoMAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q7Z434;UNIPROT:O95786"
    ]
    graphics [
      x 1600.7420744508
      y 1136.3805666364556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434;UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:22390971"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1267.5580662816428
      y 1099.1731899874262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481;urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:RNF135;HGNC_SYMBOL:TRIM25"
      map_id "UNIPROT:Q8IUD6;UNIPROT:Q14258"
      name "Riplet:TRIM25"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:Q8IUD6;UNIPROT:Q14258"
    ]
    graphics [
      x 1273.930032163136
      y 982.100213545452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUD6;UNIPROT:Q14258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "viral_space_RNA"
      name "viral_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa75"
      uniprot "NA"
    ]
    graphics [
      x 302.71634252049705
      y 713.8557272390592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_RNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 273.5612291294517
      y 840.990910670261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_109"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 351.8753928699515
      y 923.6524810451563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:SOCS1"
      map_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
    ]
    graphics [
      x 2085.2955301188704
      y 1304.6372002886396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 2027.830572720185
      y 1438.3190664711915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_89"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 1912.3634699603867
      y 1508.5223346938328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 769.9078681792089
      y 559.6549255384826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_93"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 715.474028305744
      y 663.3186471254318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IFNs"
      name "IFNs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54; csa53"
      uniprot "NA"
    ]
    graphics [
      x 1324.8358575013312
      y 231.35050685356134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFNs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_63"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 1322.0393808186966
      y 130.35809806003465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 870.659128342072
      y 1167.2766514452087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 274.2030311025426
      y 583.4843741121699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
      name "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
      node_subtype "RNA"
      node_type "species"
      org_id "sa76"
      uniprot "NA"
    ]
    graphics [
      x 152.65946117257965
      y 549.6499678985899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:25554382"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1351.328765155362
      y 1143.9504188815317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_65"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 2078.022308238781
      y 1141.6998042224807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:SOCS1"
      map_id "UNIPROT:O15524"
      name "SOCS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:O15524"
    ]
    graphics [
      x 1992.2457102077226
      y 1229.602360768932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15524"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 532.8883503133184
      y 1528.2121232970776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:SOCS3;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:IFNLR1"
      map_id "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
    ]
    graphics [
      x 1603.1178488375358
      y 789.0316556302603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1450.101024257207
      y 769.1654665486174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_90"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 1324.8708549389876
      y 768.5515490120129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1900.992405316883
      y 1080.7324152052222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_54"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1537.0610007336227
      y 1568.545079236008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "dsRNA_underscore_vesicle"
      name "dsRNA_underscore_vesicle"
      node_subtype "RNA"
      node_type "species"
      org_id "sa125"
      uniprot "NA"
    ]
    graphics [
      x 1171.4538388231638
      y 1114.732242614912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsRNA_underscore_vesicle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1062.8582357750117
      y 1097.96782703726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1215.7590838281558
      y 1942.292094159176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_88"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 1187.683264471013
      y 2051.251496330058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:32626922"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1149.9417679425935
      y 1356.1925151415933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:P0DTC5;UNIPROT:O95786"
      name "RIG_minus_1_space_M_minus_Protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0DTC5;UNIPROT:O95786"
    ]
    graphics [
      x 1123.9039896934887
      y 1478.7863706334729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5;UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1074.777595276128
      y 1004.7752011584945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB"
      map_id "UNIPROT:O14920;UNIPROT:O15111"
      name "IKK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa85"
      uniprot "UNIPROT:O14920;UNIPROT:O15111"
    ]
    graphics [
      x 1917.6717766781117
      y 654.5392892489025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14920;UNIPROT:O15111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1830.9086437839146
      y 738.356830083959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
      name "IKK_minus_TBK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1813.0625470310601
      y 909.9178938272308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_33"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 631.3495522998674
      y 1556.2800432277163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_61"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 946.1723946018411
      y 360.94548702701456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_60"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 958.4431111024862
      y 560.5049030188438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 638.3169528978889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "5'cap_minus_viral_minus_RNA"
      name "5'cap_minus_viral_minus_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 90.3872310534498
      y 757.3695690936236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5'cap_minus_viral_minus_RNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 1740.3324073666113
      y 1101.0523852334838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_77"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 1189.7211465551018
      y 239.83687569172162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_46"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 1410.3857642870462
      y 1561.9891124843184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 382.25041133393756
      y 1386.7159291593975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_78"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1048.9698652816296
      y 172.2441175840454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1236.4145148348357
      y 1467.1158574529436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 745.1115604082872
      y 1375.5720518940832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_64"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 1754.0226348583897
      y 871.8088918505074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3"
      hgnc "HGNC_SYMBOL:SOCS3"
      map_id "UNIPROT:O14543"
      name "SOCS3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa46"
      uniprot "UNIPROT:O14543"
    ]
    graphics [
      x 1651.3612522064277
      y 880.5844455005463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14543"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IFN_minus_sensitive_minus_response_minus_element"
      name "IFN_minus_sensitive_minus_response_minus_element"
      node_subtype "GENE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1052.7992724702435
      y 497.09950695897237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_sensitive_minus_response_minus_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_35"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 943.3637703976101
      y 492.95518930557364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_44"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1355.3459596862738
      y 1285.678102233906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "RNA"
      name "RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 829.8613830223336
      y 1242.3315422722687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 936.7222782054539
      y 1179.933641070732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_47"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 1649.9916001341787
      y 1320.5344544030963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 710.6121428014626
      y 421.090152364118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_91"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa139"
      uniprot "NA"
    ]
    graphics [
      x 640.9189540370976
      y 513.4794359811996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_45"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1310.1766069880487
      y 1365.657236954734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1091.8936993663335
      y 1179.488955337838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 107
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "p38_minus_NFkB"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 9
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 20
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_51"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
      target_id "M120_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_31"
      target_id "antiviral_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 6
    target 24
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ISGs"
      target_id "M120_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 24
    target 6
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_74"
      target_id "ISGs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 9
    target 25
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 25
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_40"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
      target_id "M120_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10914"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95786;UNIPROT:Q7Z434"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_43"
      target_id "UNIPROT:P10914"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 31
    target 34
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10914"
      target_id "M120_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 34
    target 31
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_56"
      target_id "UNIPROT:P10914"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ribosomal_space_60S_space_subunit"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ribosomal_space_40S_space_subunit"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_59"
      target_id "80S_space_ribosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630;UNIPROT:P42224"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P40763"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_62"
      target_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
      target_id "M120_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_30"
      target_id "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 9
    target 46
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M120_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 46
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_55"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 15
    target 47
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 11
    target 47
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_73"
      target_id "UNIPROT:Q7Z434;UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 15
    target 49
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8IUD6;UNIPROT:Q14258"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 49
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_75"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "viral_space_RNA"
      target_id "M120_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 14
    target 52
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYX4"
      target_id "M120_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_36"
      target_id "M120_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
      target_id "M120_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_67"
      target_id "M120_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 2
    target 57
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M120_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_70"
      target_id "M120_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFNs"
      target_id "M120_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_63"
      target_id "IFNs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 33
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786;UNIPROT:Q7Z434"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 5
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "dsRNA"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 61
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_42"
      target_id "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 51
    target 62
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "viral_space_RNA"
      target_id "M120_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 7
    target 62
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "nsp14"
      target_id "M120_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_37"
      target_id "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 15
    target 64
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 1
    target 64
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 64
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_76"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 27
    target 65
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15524"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 65
    target 54
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_65"
      target_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 12
    target 67
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 16
    target 67
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23458"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 67
    target 12
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_41"
      target_id "UNIPROT:P42224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
      target_id "M120_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_68"
      target_id "M120_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 30
    target 71
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 29
    target 71
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 71
    target 27
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_32"
      target_id "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 9
    target 72
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M120_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 72
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_54"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "dsRNA_underscore_vesicle"
      target_id "M120_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 74
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_53"
      target_id "dsRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 42
    target 75
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
      target_id "M120_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_66"
      target_id "M120_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 3
    target 77
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 15
    target 77
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_72"
      target_id "UNIPROT:P0DTC5;UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 5
    target 79
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "M120_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 79
    target 73
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_52"
      target_id "dsRNA_underscore_vesicle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14920;UNIPROT:O15111"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 10
    target 81
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_39"
      target_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 12
    target 83
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 13
    target 83
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 83
    target 39
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_33"
      target_id "UNIPROT:P52630;UNIPROT:P42224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 38
    target 84
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "80S_space_ribosome"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 2
    target 84
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 84
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_61"
      target_id "UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 37
    target 85
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ribosomal_space_40S_space_subunit"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 2
    target 85
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 85
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_60"
      target_id "UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 63
    target 86
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
      target_id "M120_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 4
    target 86
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "nsp16"
      target_id "M120_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_38"
      target_id "5'cap_minus_viral_minus_RNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 48
    target 88
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434;UNIPROT:O95786"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 82
    target 88
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 88
    target 26
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_50"
      target_id "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 6
    target 89
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ISGs"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 38
    target 89
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "80S_space_ribosome"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 89
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_77"
      target_id "IFNs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 8
    target 90
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_III"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 9
    target 90
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 31
    target 90
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P10914"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 90
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_46"
      target_id "IFN_minus_III"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 13
    target 91
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 17
    target 91
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P29597"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 91
    target 13
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_49"
      target_id "UNIPROT:P52630"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 6
    target 92
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "ISGs"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 38
    target 92
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "80S_space_ribosome"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 92
    target 21
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_78"
      target_id "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 31
    target 93
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10914"
      target_id "M120_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 93
    target 31
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_57"
      target_id "UNIPROT:P10914"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 39
    target 94
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630;UNIPROT:P42224"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 18
    target 94
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00978"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 94
    target 43
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_34"
      target_id "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 27
    target 95
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14543"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 95
    target 68
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_64"
      target_id "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_sensitive_minus_response_minus_element"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 45
    target 98
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 98
    target 6
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_35"
      target_id "ISGs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 8
    target 99
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_III"
      target_id "M120_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 99
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_44"
      target_id "IFN_minus_III"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "RNA"
      target_id "M120_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 101
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_58"
      target_id "dsRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 8
    target 102
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_III"
      target_id "M120_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 102
    target 29
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_47"
      target_id "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 2
    target 103
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M120_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_69"
      target_id "M120_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 8
    target 105
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "IFN_minus_III"
      target_id "M120_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 105
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_45"
      target_id "IFN_minus_III"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 15
    target 106
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 5
    target 106
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "dsRNA"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 106
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_48"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
