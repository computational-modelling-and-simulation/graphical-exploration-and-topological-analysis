# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1640.9474913719732
      y 756.4484839902723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP5038; WP5039; C19DMap:Virus replication cycle; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5; urn:miriam:uniprot:P0DTC5; urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "UNIPROT:P0DTC5"
      name "nascent_space_M; M; N_minus_glycan_space_M; M_space_lattice; membrane_br_glycoprotein; Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2338; layout_2416; layout_2419; layout_2463; layout_2469; d614c; d1b58; cedc7; sa2061; sa2116; sa2024; sa2066; sa1686; sa1891; sa1857; sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1156.1603163041852
      y 2057.580124612813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9694516; WP5038; WP5039; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2; urn:miriam:uniprot:P0DTD2; urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "UNIPROT:P0DTD2"
      name "9b_space_homodimer; ORF9b; PLpro; Orf9b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_3481; e62df; bca7b; f4882; sa139; sa1878; sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 641.9569091754621
      y 740.9365574874387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP4861; WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC4; urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P0DTC4"
      name "nascent_space_E; N_minus_glycan_space_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; 3xPalmC_minus_E; envelope_br_protein; SARS_space_E; Envelope_space_Protein_space_E; E"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_2335; layout_2405; layout_2410; layout_2412; layout_2407; layout_2414; aa466; fce54; c7c21; sa2062; sa2115; sa2023; sa2065; sa1687; sa1892; sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 666.039786270665
      y 1132.8246626149858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 230.8662748193144
      y 1284.9133718422054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; WP5038; WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "Endocytosis"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d77e4; c78a2; cde94"
      uniprot "NA"
    ]
    graphics [
      x 1410.8426658097287
      y 671.3738612942777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Endocytosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q88656943; urn:miriam:ncbiprotein:YP_009725302; NA"
      hgnc "NA"
      map_id "nsp6"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7ad1; c2256; f6f18; glyph56"
      uniprot "NA"
    ]
    graphics [
      x 216.48694159547313
      y 1511.8957388423332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q94648377; urn:miriam:refseq:QII57165.1; NA"
      hgnc "NA"
      map_id "nsp13"
      name "nsp13"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "c38dc; a8ee9; f1b6a; glyph34"
      uniprot "NA"
    ]
    graphics [
      x 1698.037307110421
      y 244.53194145125622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; WP5038; WP5039; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q2819370; NA"
      hgnc "NA"
      map_id "dsRNA"
      name "dsRNA"
      node_subtype "RNA; UNKNOWN"
      node_type "species"
      org_id "a90b0; ff0ff; cb56c; sa6; sa26; sa27; sa456; sa93; sa82"
      uniprot "NA"
    ]
    graphics [
      x 1153.1320090902166
      y 1773.2506956505504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4877; WP5038; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:wikipathways:WP4936; urn:miriam:obo.go:GO%3A0006914"
      hgnc "NA"
      map_id "Autophagy"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3; d0d85; sa18"
      uniprot "NA"
    ]
    graphics [
      x 836.9620312452432
      y 628.0731246869234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_30"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b4d82"
      uniprot "NA"
    ]
    graphics [
      x 1874.4764565351566
      y 597.7557203452408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_57"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "cb6e2"
      uniprot "NA"
    ]
    graphics [
      x 1771.7584971713636
      y 657.5572042864183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_55"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ca5d0"
      uniprot "NA"
    ]
    graphics [
      x 1129.6391745709584
      y 404.98309339640804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a40d3"
      uniprot "NA"
    ]
    graphics [
      x 957.618589012797
      y 443.10266217212165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q8N884; urn:miriam:pubmed:28363908;urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86;urn:miriam:obo.chebi:CHEBI%3A75909; urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86"
      hgnc "NA; HGNC_SYMBOL:CGAS"
      map_id "UNIPROT:Q8N884"
      name "cGAS; cGAS:dsDNA"
      node_subtype "GENE; COMPLEX; PROTEIN"
      node_type "species"
      org_id "c16eb; csa1; sa78"
      uniprot "UNIPROT:Q8N884"
    ]
    graphics [
      x 822.835401691211
      y 546.8570767932417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N884"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q9Y5S8"
      hgnc "NA"
      map_id "UNIPROT:Q9Y5S8"
      name "NOX1"
      node_subtype "GENE"
      node_type "species"
      org_id "b9d18"
      uniprot "UNIPROT:Q9Y5S8"
    ]
    graphics [
      x 1038.927643831837
      y 1080.385226535558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y5S8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:33106987"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1baa9d1"
      uniprot "NA"
    ]
    graphics [
      x 893.2774616450023
      y 1182.1170769396324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "ROS"
      name "ROS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ed2b8; sa362"
      uniprot "NA"
    ]
    graphics [
      x 717.7731785744324
      y 1255.7151492613161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ROS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:wikidata:Q106020256; urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "S1"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a19f1; bcd14; sa1539; sa1516"
      uniprot "NA"
    ]
    graphics [
      x 1732.9819862354086
      y 830.7111892172784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32818486;PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_86"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e5c68"
      uniprot "NA"
    ]
    graphics [
      x 1577.903050024555
      y 787.3040292474232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000135047"
      hgnc "NA"
      map_id "CTSL"
      name "CTSL"
      node_subtype "GENE"
      node_type "species"
      org_id "d39ba"
      uniprot "NA"
    ]
    graphics [
      x 1667.198936212833
      y 897.488307439567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CTSL"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC6; urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "UNIPROT:P0DTC6"
      name "orf6; Orf6"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e9623; sa1874; sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 310.4424423349071
      y 1114.7697840250407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id4345b2ab"
      uniprot "NA"
    ]
    graphics [
      x 382.8913725791333
      y 1238.9150240279143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000126456;urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "ab03e"
      name "ab03e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ab03e"
      uniprot "NA"
    ]
    graphics [
      x 368.22754545343764
      y 1364.8793748264284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ab03e"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd3bd"
      uniprot "NA"
    ]
    graphics [
      x 1593.8447825154876
      y 298.5338358722063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4868; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "TBK1"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "ad4b3; fb718; fbafc"
      uniprot "NA"
    ]
    graphics [
      x 1514.579820403951
      y 425.1391579792571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TBK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "Fusion"
      name "Fusion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ee6e6"
      uniprot "NA"
    ]
    graphics [
      x 1392.506320022711
      y 399.9014506916138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fusion"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aa497"
      uniprot "NA"
    ]
    graphics [
      x 1257.6154689464643
      y 328.90667713721166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:obo.chebi:CHEBI%3A48432; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_II"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb; sa23; sa95; sa194"
      uniprot "NA"
    ]
    graphics [
      x 1397.7031239711603
      y 1037.6884952937314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "da98c"
      uniprot "NA"
    ]
    graphics [
      x 1605.8588671773537
      y 1005.9940644493913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "angiotensin_space_(1_minus_7)"
      name "angiotensin_space_(1_minus_7)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a6d7b"
      uniprot "NA"
    ]
    graphics [
      x 1667.7004196489697
      y 1185.9491994535051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_(1_minus_7)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000184012"
      hgnc "NA"
      map_id "TMPRSS2"
      name "TMPRSS2"
      node_subtype "GENE"
      node_type "species"
      org_id "d8284"
      uniprot "NA"
    ]
    graphics [
      x 575.9102192695029
      y 112.16594085733527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMPRSS2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f843b"
      uniprot "NA"
    ]
    graphics [
      x 471.23273291936835
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:wikidata:Q106020384; urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "S2"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b27b6; sa2188; sa2052; sa1601; sa2063"
      uniprot "NA"
    ]
    graphics [
      x 465.78386518616185
      y 179.55194238788238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000080824.19;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "d836e"
      name "d836e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d836e"
      uniprot "NA"
    ]
    graphics [
      x 261.7190497612469
      y 1203.6088561316842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d836e"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:32979938;PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bacb0"
      uniprot "NA"
    ]
    graphics [
      x 225.47889843929
      y 1390.336678105981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "IKBKE"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "aa6d4; cb0df"
      uniprot "NA"
    ]
    graphics [
      x 128.69006912599525
      y 1354.3227619171155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKBKE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d15f2"
      uniprot "NA"
    ]
    graphics [
      x 906.8167431953789
      y 661.1093133768921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4961; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580; urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA"
      map_id "cGAMP"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ba77f; bd7f8; sa83"
      uniprot "NA"
    ]
    graphics [
      x 1047.0756709575949
      y 704.7495551255638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cGAMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000154174"
      hgnc "NA"
      map_id "TOMM70"
      name "TOMM70"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc950"
      uniprot "NA"
    ]
    graphics [
      x 426.43122766532827
      y 850.7478996276976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TOMM70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b6c5f"
      uniprot "NA"
    ]
    graphics [
      x 320.41456423561146
      y 1008.8427688467542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000160703"
      hgnc "NA"
      map_id "NLRX1"
      name "NLRX1"
      node_subtype "GENE"
      node_type "species"
      org_id "c2498; ab61d"
      uniprot "NA"
    ]
    graphics [
      x 842.4101133422766
      y 893.7454519570003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRX1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:23321557"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "cf0de"
      uniprot "NA"
    ]
    graphics [
      x 806.2467918685002
      y 750.1845352709729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikidata:Q104520877"
      hgnc "NA"
      map_id "orf9c"
      name "orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f975c"
      uniprot "NA"
    ]
    graphics [
      x 326.8214068028252
      y 873.6699259188246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "orf9c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f4935"
      uniprot "NA"
    ]
    graphics [
      x 574.9599321168042
      y 861.1456313827501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:19692591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cbf4e"
      uniprot "NA"
    ]
    graphics [
      x 660.006071259844
      y 909.5610782939908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP111"
      hgnc "NA"
      map_id "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
      name "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a2943"
      uniprot "NA"
    ]
    graphics [
      x 500.12222971453474
      y 950.9397966043146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:21187859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f5f6e"
      uniprot "NA"
    ]
    graphics [
      x 707.5999383180609
      y 1445.7986108562127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000077150;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "b6b94"
      name "b6b94"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6b94"
      uniprot "NA"
    ]
    graphics [
      x 741.7949540879916
      y 1594.8368361859323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b6b94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:33106987"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_110"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id57f20ac8"
      uniprot "NA"
    ]
    graphics [
      x 1214.9575251515469
      y 1074.3540234476663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:33937724"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e1040"
      uniprot "NA"
    ]
    graphics [
      x 508.4928124286882
      y 1352.8147998119991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP5039"
      full_annotation "urn:miriam:ncbigene:3438; urn:miriam:pfam:PF00143"
      hgnc "NA"
      map_id "IFN_minus_I"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "f26de; b9e39; b8a45"
      uniprot "NA"
    ]
    graphics [
      x 629.5837440018349
      y 1303.9774887244173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_33"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "b6e4c"
      uniprot "NA"
    ]
    graphics [
      x 1522.6966052419502
      y 557.6022335117393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4883; WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720; urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "AGT"
      name "AGT"
      node_subtype "SIMPLE_MOLECULE; GENE"
      node_type "species"
      org_id "b9dff; f53bd; cfd56"
      uniprot "NA"
    ]
    graphics [
      x 1430.2988512301717
      y 559.2040518225117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGT"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637;PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cecdf"
      uniprot "NA"
    ]
    graphics [
      x 1406.2807715902256
      y 754.9310412239555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_I"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a; sa21; sa195"
      uniprot "NA"
    ]
    graphics [
      x 1388.080898219024
      y 959.8505518223863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2"
      name "SARS_minus_CoV_minus_2"
      node_subtype "UNKNOWN; COMPLEX"
      node_type "species"
      org_id "c4774; aae40; e2279; de4ce"
      uniprot "NA"
    ]
    graphics [
      x 760.5770669961413
      y 978.8475475690977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b98e3"
      uniprot "NA"
    ]
    graphics [
      x 654.6989467482274
      y 1024.865027852945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4912; WP4880"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "TICAM1"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "cea88; f9104; b7b77"
      uniprot "NA"
    ]
    graphics [
      x 771.0135920718777
      y 1304.1044887311941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TICAM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_105"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "fe2c7"
      uniprot "NA"
    ]
    graphics [
      x 799.6726329785979
      y 1182.6533848195627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 797.0689776791999
      y 1039.0156438172867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000074582"
      hgnc "NA"
      map_id "BCS1L"
      name "BCS1L"
      node_subtype "GENE"
      node_type "species"
      org_id "a71fb"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1073.4024788411753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BCS1L"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a8e16"
      uniprot "NA"
    ]
    graphics [
      x 169.81963426275445
      y 1162.3300497527482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP4921"
      hgnc "NA"
      map_id "Mitochondrial_space_CIII_space_assembly"
      name "Mitochondrial_space_CIII_space_assembly"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a360f"
      uniprot "NA"
    ]
    graphics [
      x 342.090111597175
      y 1153.9345990482932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mitochondrial_space_CIII_space_assembly"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_111"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id68e36368"
      uniprot "NA"
    ]
    graphics [
      x 1444.663532167362
      y 1173.645362899798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000180772; urn:miriam:pubmed:18035185;urn:miriam:ncbigene:186"
      hgnc "NA"
      map_id "AGTR2"
      name "AGTR2"
      node_subtype "GENE"
      node_type "species"
      org_id "a4260; f2fd8"
      uniprot "NA"
    ]
    graphics [
      x 1462.5551902723787
      y 1303.8127470050142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGTR2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d1f58"
      uniprot "NA"
    ]
    graphics [
      x 1348.5770552013996
      y 1127.9507946344168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 7
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P12821; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "NA; HGNC_SYMBOL:ACE"
      map_id "UNIPROT:P12821"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e0c12; sa29; sa146; sa28; sa100; sa199; sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1399.57929868571
      y 1254.3116770274114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12821"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:32699849"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd4b4"
      uniprot "NA"
    ]
    graphics [
      x 132.79246245862169
      y 1098.8701391252464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q99623;urn:miriam:uniprot:P35232"
      hgnc "NA"
      map_id "UNIPROT:Q99623;UNIPROT:P35232"
      name "ca82b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca82b"
      uniprot "UNIPROT:Q99623;UNIPROT:P35232"
    ]
    graphics [
      x 91.85550576625553
      y 915.669735951155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99623;UNIPROT:P35232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:32574107"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c5b62"
      uniprot "NA"
    ]
    graphics [
      x 1066.0218189305785
      y 842.4123749062402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 10
      diagram "WP5038; C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q86WV6; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q86WV6;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:obo.go:GO%3A1990231;urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA; HGNC_SYMBOL:STING1"
      map_id "UNIPROT:Q86WV6"
      name "TMEM173; STING1; cGAMP:STING; cGAMP:STING:LC3; STING; cGAMP_minus_STING"
      node_subtype "GENE; PROTEIN; COMPLEX"
      node_type "species"
      org_id "c488d; sa172; sa171; csa3; csa4; csa13; sa100; sa98; csa11; csa16"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1281.3066128393996
      y 764.2794929750767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86WV6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32574107"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b73fa"
      uniprot "NA"
    ]
    graphics [
      x 1352.5483451208795
      y 896.8333513701979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "TRAF6"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "ea384; fe40f"
      uniprot "NA"
    ]
    graphics [
      x 956.2045890948053
      y 1557.064519296713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TRAF6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_10"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a5994"
      uniprot "NA"
    ]
    graphics [
      x 859.7340659966051
      y 1652.6852732465213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "eca63"
      uniprot "NA"
    ]
    graphics [
      x 426.02243694823585
      y 1055.320386584263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_88"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e7424"
      uniprot "NA"
    ]
    graphics [
      x 1791.2739494588186
      y 753.234796699143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "aa323"
      uniprot "NA"
    ]
    graphics [
      x 1243.7897903424046
      y 1698.4361210341929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000107201;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "b9671"
      name "b9671"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9671"
      uniprot "NA"
    ]
    graphics [
      x 1204.5246183386332
      y 1569.4318677298957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b9671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_116"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ide445d5"
      uniprot "NA"
    ]
    graphics [
      x 1674.090821589535
      y 1331.0746258584486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4883"
      full_annotation "urn:miriam:ensembl:ENSG00000206470; urn:miriam:ncbigene:4142"
      hgnc "NA"
      map_id "MAS1"
      name "MAS1"
      node_subtype "GENE"
      node_type "species"
      org_id "a876a; c93ee"
      uniprot "NA"
    ]
    graphics [
      x 1753.9046615913012
      y 1420.3780323351843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAS1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32728199"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ac21e"
      uniprot "NA"
    ]
    graphics [
      x 518.1311152979308
      y 713.9275027876215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ae213"
      uniprot "NA"
    ]
    graphics [
      x 206.99298109337008
      y 808.3392529548685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000137806;urn:miriam:ensembl:ENSG00000147684;urn:miriam:ensembl:ENSG00000130159;urn:miriam:ensembl:ENSG00000177646"
      hgnc "NA"
      map_id "c9f6c"
      name "c9f6c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c9f6c"
      uniprot "NA"
    ]
    graphics [
      x 266.52927335290383
      y 711.6727196499155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c9f6c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "angiotensin_minus_(1_minus_9)"
      name "angiotensin_minus_(1_minus_9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f991b"
      uniprot "NA"
    ]
    graphics [
      x 1574.5575447916272
      y 1106.2113995609504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_minus_(1_minus_9)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id925594bb"
      uniprot "NA"
    ]
    graphics [
      x 1553.2008056073257
      y 1244.7218952306143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_114"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idaa39462d"
      uniprot "NA"
    ]
    graphics [
      x 1161.2897144256349
      y 744.2274612653276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593;PUBMED:32726803"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d8629"
      uniprot "NA"
    ]
    graphics [
      x 735.0491599822114
      y 868.7183301569528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:A0A1B0GUZ2"
      hgnc "NA"
      map_id "UNIPROT:A0A1B0GUZ2"
      name "Renin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d29ef"
      uniprot "UNIPROT:A0A1B0GUZ2"
    ]
    graphics [
      x 1625.1388135511897
      y 609.2018443519557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A0A1B0GUZ2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "abdce"
      uniprot "NA"
    ]
    graphics [
      x 1581.5970038691873
      y 502.3005876509825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_4"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a32af"
      uniprot "NA"
    ]
    graphics [
      x 925.52926976611
      y 807.7616365387299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP3656"
      hgnc "NA"
      map_id "Interleukin_minus_1_space_Induced_space_Activation_space_of_space_NF_minus_kappa_minus_B"
      name "Interleukin_minus_1_space_Induced_space_Activation_space_of_space_NF_minus_kappa_minus_B"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ffab8"
      uniprot "NA"
    ]
    graphics [
      x 970.7258802046475
      y 697.5526494842633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Interleukin_minus_1_space_Induced_space_Activation_space_of_space_NF_minus_kappa_minus_B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd0be"
      uniprot "NA"
    ]
    graphics [
      x 338.14652487580975
      y 1442.1991557249376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000137806"
      hgnc "NA"
      map_id "NDUFAF1"
      name "NDUFAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "ee9df"
      uniprot "NA"
    ]
    graphics [
      x 467.67959855288166
      y 1507.750648480352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NDUFAF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_115"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddf33d8ac"
      uniprot "NA"
    ]
    graphics [
      x 1290.3811212617493
      y 858.621563388187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000144891; urn:miriam:pubmed:33375371;urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "AGTR1"
      name "AGTR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e034b; f99b1"
      uniprot "NA"
    ]
    graphics [
      x 1264.4034228326877
      y 657.101509700518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGTR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c1a5d"
      uniprot "NA"
    ]
    graphics [
      x 84.23278452143006
      y 756.8394903652295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000175104;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000154174"
      hgnc "NA"
      map_id "ad3f8"
      name "ad3f8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ad3f8"
      uniprot "NA"
    ]
    graphics [
      x 122.49289024626898
      y 634.5890168872772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ad3f8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_113"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ida0dbfe4d"
      uniprot "NA"
    ]
    graphics [
      x 816.0299786560435
      y 1374.0796032279923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP3404"
      hgnc "NA"
      map_id "Oxidative_space_Stress_space_Induced_space_Senescence_br_"
      name "Oxidative_space_Stress_space_Induced_space_Senescence_br_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b0078"
      uniprot "NA"
    ]
    graphics [
      x 860.1370646300636
      y 1479.911332050318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Oxidative_space_Stress_space_Induced_space_Senescence_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:28956771;PUBMED:21703540"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c58d8"
      uniprot "NA"
    ]
    graphics [
      x 954.5941767320933
      y 1091.6622134816491
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; WP4912; WP4868; WP4880; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "MAVS"
      name "MAVS"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e240d; f0e60; fa763; a978e; ae650"
      uniprot "NA"
    ]
    graphics [
      x 1055.0739524576734
      y 1270.3207146354343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAVS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d7e7c"
      uniprot "NA"
    ]
    graphics [
      x 1251.3297926110124
      y 540.216016765229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_85"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e45d6"
      uniprot "NA"
    ]
    graphics [
      x 1030.986482626392
      y 2013.2561302503918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_59"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cc8d7"
      uniprot "NA"
    ]
    graphics [
      x 1065.9603021103808
      y 2128.782552380038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637;PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d7130"
      uniprot "NA"
    ]
    graphics [
      x 1547.0956749395411
      y 946.3528039461453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_103"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fb344"
      uniprot "NA"
    ]
    graphics [
      x 385.5919536895208
      y 786.4701149292756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3897"
      uniprot "NA"
    ]
    graphics [
      x 577.2765458551683
      y 1119.5432611586054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32839770"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad224"
      uniprot "NA"
    ]
    graphics [
      x 175.26227700507218
      y 959.5469181474796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4912"
      full_annotation "urn:miriam:hgnc.symbol:IFIH1; urn:miriam:ensembl:ENSG00000115267"
      hgnc "HGNC_SYMBOL:IFIH1; NA"
      map_id "IFIH1"
      name "IFIH1"
      node_subtype "GENE"
      node_type "species"
      org_id "c1e37; e1fcd"
      uniprot "NA"
    ]
    graphics [
      x 1569.0549951816947
      y 865.4893045770012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFIH1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_14"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a88cd"
      uniprot "NA"
    ]
    graphics [
      x 1439.7702014267425
      y 823.990779902126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_80"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e0662"
      uniprot "NA"
    ]
    graphics [
      x 1432.2351245609989
      y 611.291977329314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_117"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf9d6210a"
      uniprot "NA"
    ]
    graphics [
      x 1286.7532538569967
      y 478.3784307039924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:32995797;PUBMED:17451827"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "db52e"
      uniprot "NA"
    ]
    graphics [
      x 1137.7906154593602
      y 1428.5302614131888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 115
    source 11
    target 12
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_30"
      target_id "W10_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 12
    target 1
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_57"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 13
    target 14
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_55"
      target_id "W10_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 14
    target 15
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_8"
      target_id "UNIPROT:Q8N884"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 16
    target 17
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y5S8"
      target_id "W10_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 17
    target 18
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_108"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 19
    target 20
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "S1"
      target_id "W10_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 21
    target 20
    cd19dm [
      diagram "WP5038"
      edge_type "PHYSICAL_STIMULATION"
      source_id "CTSL"
      target_id "W10_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 20
    target 6
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_86"
      target_id "Endocytosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 22
    target 23
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC6"
      target_id "W10_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 23
    target 24
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_109"
      target_id "ab03e"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 8
    target 25
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "nsp13"
      target_id "W10_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 25
    target 26
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_104"
      target_id "TBK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 27
    target 28
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "Fusion"
      target_id "W10_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 28
    target 13
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_18"
      target_id "W10_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 29
    target 30
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "W10_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 1
    target 30
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "W10_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 30
    target 31
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_75"
      target_id "angiotensin_space_(1_minus_7)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 32
    target 33
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "TMPRSS2"
      target_id "W10_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 33
    target 34
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_100"
      target_id "S2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 35
    target 36
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "d836e"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 37
    target 36
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "IKBKE"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 7
    target 36
    cd19dm [
      diagram "WP5038"
      edge_type "INHIBITION"
      source_id "nsp6"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 36
    target 24
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_40"
      target_id "ab03e"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 15
    target 38
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N884"
      target_id "W10_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 38
    target 39
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_64"
      target_id "cGAMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 40
    target 41
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "TOMM70"
      target_id "W10_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 41
    target 35
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_32"
      target_id "d836e"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 42
    target 43
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W10_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 43
    target 10
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_62"
      target_id "Autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 44
    target 45
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "orf9c"
      target_id "W10_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 45
    target 42
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_96"
      target_id "NLRX1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 42
    target 46
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W10_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 46
    target 47
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_58"
      target_id "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 18
    target 48
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "ROS"
      target_id "W10_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 48
    target 49
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_97"
      target_id "b6b94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 29
    target 50
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "W10_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 50
    target 16
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_110"
      target_id "UNIPROT:Q9Y5S8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 24
    target 51
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "ab03e"
      target_id "W10_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 51
    target 52
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_82"
      target_id "IFN_minus_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 1
    target 53
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "W10_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 53
    target 27
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_33"
      target_id "Fusion"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 54
    target 55
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "AGT"
      target_id "W10_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 55
    target 56
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_61"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 57
    target 58
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2"
      target_id "W10_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 58
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_36"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 59
    target 60
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "TICAM1"
      target_id "W10_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 60
    target 61
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_105"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 62
    target 63
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "BCS1L"
      target_id "W10_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 63
    target 64
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_15"
      target_id "Mitochondrial_space_CIII_space_assembly"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 29
    target 65
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "W10_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 65
    target 66
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_111"
      target_id "AGTR2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 56
    target 67
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "W10_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 68
    target 67
    cd19dm [
      diagram "WP5038"
      edge_type "MODULATION"
      source_id "UNIPROT:P12821"
      target_id "W10_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 67
    target 29
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_66"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 5
    target 69
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "W10_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 69
    target 70
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_78"
      target_id "UNIPROT:Q99623;UNIPROT:P35232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 42
    target 71
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W10_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 71
    target 72
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_51"
      target_id "UNIPROT:Q86WV6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 29
    target 73
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "W10_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 73
    target 72
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_34"
      target_id "UNIPROT:Q86WV6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 74
    target 75
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "TRAF6"
      target_id "W10_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 75
    target 49
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_10"
      target_id "b6b94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 64
    target 76
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "Mitochondrial_space_CIII_space_assembly"
      target_id "W10_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 76
    target 47
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_91"
      target_id "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 1
    target 77
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "W10_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 77
    target 19
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_88"
      target_id "S1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 9
    target 78
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "W10_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 78
    target 79
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_17"
      target_id "b9671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 31
    target 80
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_(1_minus_7)"
      target_id "W10_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 80
    target 81
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_116"
      target_id "MAS1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 3
    target 82
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD2"
      target_id "W10_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 82
    target 40
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_23"
      target_id "TOMM70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 44
    target 83
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "orf9c"
      target_id "W10_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 83
    target 84
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_27"
      target_id "c9f6c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 85
    target 86
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_minus_(1_minus_9)"
      target_id "W10_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 68
    target 86
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "W10_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 86
    target 31
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_112"
      target_id "angiotensin_space_(1_minus_7)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 39
    target 87
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "cGAMP"
      target_id "W10_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 87
    target 72
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_114"
      target_id "UNIPROT:Q86WV6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 3
    target 88
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD2"
      target_id "W10_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 88
    target 61
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_74"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 89
    target 90
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:A0A1B0GUZ2"
      target_id "W10_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 90
    target 54
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_22"
      target_id "AGT"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 42
    target 91
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W10_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 91
    target 92
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_4"
      target_id "Interleukin_minus_1_space_Induced_space_Activation_space_of_space_NF_minus_kappa_minus_B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 5
    target 93
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "W10_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 93
    target 94
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_77"
      target_id "NDUFAF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 29
    target 95
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "W10_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 95
    target 96
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_115"
      target_id "AGTR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 70
    target 97
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99623;UNIPROT:P35232"
      target_id "W10_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 97
    target 98
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_44"
      target_id "ad3f8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 18
    target 99
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "ROS"
      target_id "W10_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 99
    target 100
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_113"
      target_id "Oxidative_space_Stress_space_Induced_space_Senescence_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 42
    target 101
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W10_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 101
    target 102
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_50"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 6
    target 103
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "Endocytosis"
      target_id "W10_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 103
    target 13
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_71"
      target_id "W10_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 104
    target 105
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_85"
      target_id "W10_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 105
    target 2
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_59"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 56
    target 106
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "W10_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 1
    target 106
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "W10_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 106
    target 85
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_70"
      target_id "angiotensin_minus_(1_minus_9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 84
    target 107
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "c9f6c"
      target_id "W10_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 107
    target 47
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_103"
      target_id "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 47
    target 108
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
      target_id "W10_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 108
    target 18
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_6"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 44
    target 109
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "orf9c"
      target_id "W10_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 109
    target 62
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_24"
      target_id "BCS1L"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 110
    target 111
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "IFIH1"
      target_id "W10_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 111
    target 72
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_14"
      target_id "UNIPROT:Q86WV6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 72
    target 112
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "W10_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 112
    target 26
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_80"
      target_id "TBK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 96
    target 113
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "AGTR1"
      target_id "W10_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 113
    target 27
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_117"
      target_id "Fusion"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 79
    target 114
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "b9671"
      target_id "W10_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 114
    target 102
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_76"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
