# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 640.0393379998932
      y 147.8034188223213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 925.9290737621293
      y 403.21515361329256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917572; urn:miriam:ncbiprotein:YP_009725306; NA"
      hgnc "NA"
      map_id "nsp10"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de394; a99b9; a8b3c; b19e5; glyph61"
      uniprot "NA"
    ]
    graphics [
      x 706.3481756789953
      y 940.8794882120355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4861; WP4868; WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725310; NA"
      hgnc "NA"
      map_id "nsp15"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205; fa46a; beaa5; c625e; glyph52"
      uniprot "NA"
    ]
    graphics [
      x 650.1848254931901
      y 1223.4168346928795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725311; NA"
      hgnc "NA"
      map_id "nsp16"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4; abf4b; c8bd9; glyph36; sa81"
      uniprot "NA"
    ]
    graphics [
      x 312.95523056520074
      y 816.5129333961163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q94648377; urn:miriam:refseq:QII57165.1; NA"
      hgnc "NA"
      map_id "nsp13"
      name "nsp13"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "c38dc; a8ee9; f1b6a; glyph34"
      uniprot "NA"
    ]
    graphics [
      x 532.6107372404848
      y 1772.94695730678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4846; WP5027; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882; urn:miriam:wikidata:Q90038952; urn:miriam:ncbiprotein:YP_009725297; NA"
      hgnc "NA"
      map_id "nsp1"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606; f9094; e6110; d2ec8; da173; glyph39"
      uniprot "NA"
    ]
    graphics [
      x 1780.0288753217756
      y 626.6793685311782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4868; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "TBK1"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "ad4b3; fb718; fbafc"
      uniprot "NA"
    ]
    graphics [
      x 422.4148432451726
      y 1691.127223005066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TBK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "IKBKE"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "aa6d4; cb0df"
      uniprot "NA"
    ]
    graphics [
      x 1156.4018152849744
      y 1572.6487274006936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKBKE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 1611.040817442533
      y 927.2750871138626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "TRAF6"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "ea384; fe40f"
      uniprot "NA"
    ]
    graphics [
      x 1354.553389524342
      y 1376.5597556570124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TRAF6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; WP4912; WP4868; WP4880; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "MAVS"
      name "MAVS"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e240d; f0e60; fa763; a978e; ae650"
      uniprot "NA"
    ]
    graphics [
      x 587.2849755388459
      y 952.5928374834294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAVS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_46"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d1353"
      uniprot "NA"
    ]
    graphics [
      x 630.0838693330395
      y 1062.5647545253903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115267"
      hgnc "NA"
      map_id "MDA5_space_"
      name "MDA5_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b93b0"
      uniprot "NA"
    ]
    graphics [
      x 507.1453658938127
      y 1070.618368209208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MDA5_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_73"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e7327"
      uniprot "NA"
    ]
    graphics [
      x 659.6212153339411
      y 800.2098877611854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "RIG_minus_I_space_(DDX58)"
      name "RIG_minus_I_space_(DDX58)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d88ba"
      uniprot "NA"
    ]
    graphics [
      x 519.5371515178424
      y 713.7898266144471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RIG_minus_I_space_(DDX58)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000162434;urn:miriam:ensembl:ENSG00000105397"
      hgnc "NA"
      map_id "dd322"
      name "dd322"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd322"
      uniprot "NA"
    ]
    graphics [
      x 2014.9359966293828
      y 813.6242319163097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dd322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id38b9357c"
      uniprot "NA"
    ]
    graphics [
      x 1975.8797201989064
      y 935.9442407179549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "STAT1"
      name "STAT1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9489; f2e43"
      uniprot "NA"
    ]
    graphics [
      x 1850.7279485103197
      y 937.4055221639485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "STAT1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "NFkB"
      name "NFkB"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e788c"
      uniprot "NA"
    ]
    graphics [
      x 998.0680024826264
      y 1593.872605733016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NFkB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fbdcb"
      uniprot "NA"
    ]
    graphics [
      x 794.7777659074006
      y 1578.6968626370508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "Pro_minus_inflammatory_space__br_cytokines_space_production"
      name "Pro_minus_inflammatory_space__br_cytokines_space_production"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d99d5"
      uniprot "NA"
    ]
    graphics [
      x 603.2779836562406
      y 1511.8011148224336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pro_minus_inflammatory_space__br_cytokines_space_production"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ae85d"
      uniprot "NA"
    ]
    graphics [
      x 384.0628508438838
      y 704.3335094540084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4868; WP5039; C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ISGs"
      name "ISGs"
      node_subtype "PROTEIN; UNKNOWN; RNA"
      node_type "species"
      org_id "f1bfb; ad145; sa15; sa14"
      uniprot "NA"
    ]
    graphics [
      x 458.2121729715765
      y 477.602646028223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ISGs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c04cc"
      uniprot "NA"
    ]
    graphics [
      x 660.5688674511375
      y 369.01288001597175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ensembl:ENSG00000111331;urn:miriam:ensembl:ENSG00000111335"
      hgnc "NA"
      map_id "c82e3"
      name "c82e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c82e3"
      uniprot "NA"
    ]
    graphics [
      x 846.1985307779584
      y 244.27370954298988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c82e3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "TRAF3"
      name "TRAF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f673f"
      uniprot "NA"
    ]
    graphics [
      x 1060.74533021352
      y 1087.8152526274475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TRAF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ef27c"
      uniprot "NA"
    ]
    graphics [
      x 1166.244008165279
      y 924.5618484549882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "bc0e3"
      name "bc0e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bc0e3"
      uniprot "NA"
    ]
    graphics [
      x 1348.2344811619075
      y 850.5300078629704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bc0e3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "TRIF_br_"
      name "TRIF_br_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8003"
      uniprot "NA"
    ]
    graphics [
      x 1032.4680066908427
      y 1164.3715427880525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TRIF_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a7711"
      uniprot "NA"
    ]
    graphics [
      x 946.2941994064423
      y 1157.107740418389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000198001"
      hgnc "NA"
      map_id "IRAK4"
      name "IRAK4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33f8"
      uniprot "NA"
    ]
    graphics [
      x 1388.2118255949117
      y 1187.0895961439287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRAK4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c6c90"
      uniprot "NA"
    ]
    graphics [
      x 1462.6833197313597
      y 1312.3550447441073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_66"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1751"
      uniprot "NA"
    ]
    graphics [
      x 532.77005015186
      y 1203.7497404419041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9Y2C9"
      hgnc "NA"
      map_id "UNIPROT:Q9Y2C9"
      name "TLR6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5481"
      uniprot "UNIPROT:Q9Y2C9"
    ]
    graphics [
      x 1064.1497080625936
      y 596.1172983371471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y2C9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b5fd0"
      uniprot "NA"
    ]
    graphics [
      x 1192.167426825126
      y 712.987733874909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "MYD88"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "dfbb7; f8b63"
      uniprot "NA"
    ]
    graphics [
      x 1286.6346019336218
      y 896.026403955196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MYD88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; NA"
      hgnc "NA"
      map_id "nsp14"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9; glyph54; sa80"
      uniprot "NA"
    ]
    graphics [
      x 423.4347541504576
      y 1041.9467563966773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca320"
      uniprot "NA"
    ]
    graphics [
      x 394.0187684859733
      y 1151.6881700219242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "af7a1"
      uniprot "NA"
    ]
    graphics [
      x 1033.1154150300536
      y 237.01574144063625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:obo.go:GO%3A0045087"
      hgnc "NA"
      map_id "Innate_space_Immunity"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c1ec8; sa16"
      uniprot "NA"
    ]
    graphics [
      x 1165.3808079045473
      y 288.2062075200545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Innate_space_Immunity"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "b8d9b"
      name "b8d9b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8d9b"
      uniprot "NA"
    ]
    graphics [
      x 1622.8297597759274
      y 594.6656415721792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b8d9b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbbf051e0"
      uniprot "NA"
    ]
    graphics [
      x 1663.8875024313884
      y 451.2602633351098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "INF_minus_I_space_alpha_slash__space_beta"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1657; e6fbe; bd4e9"
      uniprot "NA"
    ]
    graphics [
      x 1835.116139505528
      y 377.1852980354496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "INF_minus_I_space_alpha_slash__space_beta"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938;PUBMED:33337934;PUBMED:32733001"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_113"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcb3aff58"
      uniprot "NA"
    ]
    graphics [
      x 422.94559978579093
      y 1807.4373016016702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cfeb0"
      uniprot "NA"
    ]
    graphics [
      x 1734.5298865117652
      y 814.4188433996949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000185507;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "db1fb"
      name "db1fb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "db1fb"
      uniprot "NA"
    ]
    graphics [
      x 1848.1051577174148
      y 679.4190858347512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "db1fb"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "b8407"
      name "b8407"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8407"
      uniprot "NA"
    ]
    graphics [
      x 2058.8231730563657
      y 536.1314888527637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b8407"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_81"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed722"
      uniprot "NA"
    ]
    graphics [
      x 2071.795016978577
      y 680.2754061063976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_109"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id782ae218"
      uniprot "NA"
    ]
    graphics [
      x 1703.6972223632074
      y 355.6485280679465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_minus_CoV_space_2"
      name "SARS_minus_CoV_space_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c8487; a63e3"
      uniprot "NA"
    ]
    graphics [
      x 807.183176257365
      y 428.44797240149154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_space_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_78"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "eaa62"
      uniprot "NA"
    ]
    graphics [
      x 683.6036800840961
      y 286.24523974318697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455; urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "NA; HGNC_SYMBOL:TLR3"
      map_id "UNIPROT:O15455"
      name "TLR3; TLR3_underscore_TRIF; TLR3_underscore_TRIF_underscore_RIPK1; TLR3:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "ab922; csa37; sa239; csa38; sa5; csa88; sa45; sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 840.9043744852956
      y 1140.1312656544137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15455"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "da0cb"
      uniprot "NA"
    ]
    graphics [
      x 919.4121077139966
      y 1251.0947322502648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc80b"
      uniprot "NA"
    ]
    graphics [
      x 1880.758480733035
      y 522.1959591760337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_4"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "aabad"
      uniprot "NA"
    ]
    graphics [
      x 893.885963403571
      y 598.6608738932008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O00206; urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "NA; HGNC_SYMBOL:TLR4"
      map_id "UNIPROT:O00206"
      name "TLR4; TLR4_underscore_TRIF_underscore_TRAM"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c7d93; csa47; sa291; sa142"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 991.4041409650401
      y 759.0650367637309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NR96; urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633"
      hgnc "NA; HGNC_SYMBOL:TLR9"
      map_id "UNIPROT:Q9NR96"
      name "TLR9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "adc15; sa238"
      uniprot "UNIPROT:Q9NR96"
    ]
    graphics [
      x 1060.4152696540036
      y 833.5274368785394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NR96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b1a40"
      uniprot "NA"
    ]
    graphics [
      x 1209.4681242438946
      y 818.1623507391923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fa67c"
      uniprot "NA"
    ]
    graphics [
      x 1494.7582334965352
      y 859.294754788315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5bdc9aa8"
      uniprot "NA"
    ]
    graphics [
      x 777.3134534739722
      y 608.0055276733251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q82069695"
      hgnc "NA"
      map_id "SARS_minus_CoV_space_2_space_RNA"
      name "SARS_minus_CoV_space_2_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "ff396; bbbdd"
      uniprot "NA"
    ]
    graphics [
      x 795.5841753313482
      y 805.1946880851016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_space_2_space_RNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id619b1996"
      uniprot "NA"
    ]
    graphics [
      x 1748.971259340238
      y 277.85472595150316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "f2bcd"
      uniprot "NA"
    ]
    graphics [
      x 1812.0264483684123
      y 782.1205999288306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_114"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idd0587e82"
      uniprot "NA"
    ]
    graphics [
      x 845.1961530622989
      y 976.8049940571274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d0ce3"
      uniprot "NA"
    ]
    graphics [
      x 1322.5859208638649
      y 1139.6181199907119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_102"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3cf0d202"
      uniprot "NA"
    ]
    graphics [
      x 1982.2263205906802
      y 413.2185996826372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000213928;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "d46a8"
      name "d46a8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d46a8"
      uniprot "NA"
    ]
    graphics [
      x 181.0344818626968
      y 828.3388287600925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d46a8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc446"
      uniprot "NA"
    ]
    graphics [
      x 287.39889171253856
      y 627.0076304801219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikidata:Q27097846; urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "GRL0617"
      name "GRL0617"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "ebd2b; sa168"
      uniprot "NA"
    ]
    graphics [
      x 1008.0563663023192
      y 1442.2229095492003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GRL0617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887;PUBMED:18852458"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_68"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e29be"
      uniprot "NA"
    ]
    graphics [
      x 1150.3732828690063
      y 1401.3985583289575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:refseq:YP_009725299.1"
      hgnc "NA"
      map_id "PLpro_space_(nsp3)"
      name "PLpro_space_(nsp3)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9fd0"
      uniprot "NA"
    ]
    graphics [
      x 1299.0941273615445
      y 1326.2839629956686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PLpro_space_(nsp3)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd2da"
      uniprot "NA"
    ]
    graphics [
      x 1618.5172091791055
      y 754.6174203886212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "dffb5"
      name "dffb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dffb5"
      uniprot "NA"
    ]
    graphics [
      x 100.58251659975292
      y 982.0434947926142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dffb5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0118"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 871.6544919963911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_80"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed2f7"
      uniprot "NA"
    ]
    graphics [
      x 520.9960641220533
      y 820.2943916342276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8a9c"
      uniprot "NA"
    ]
    graphics [
      x 1140.3648821130107
      y 799.8459576708077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d00bf"
      uniprot "NA"
    ]
    graphics [
      x 463.5248930782245
      y 877.0341876030318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_40"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c8d59"
      uniprot "NA"
    ]
    graphics [
      x 984.4572103840721
      y 897.5939592108008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000196664"
      hgnc "NA"
      map_id "TLR7"
      name "TLR7"
      node_subtype "GENE"
      node_type "species"
      org_id "e4953; c3d61"
      uniprot "NA"
    ]
    graphics [
      x 1176.0321564361222
      y 988.8912060669297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TLR7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bebd6"
      uniprot "NA"
    ]
    graphics [
      x 1012.3891962986218
      y 975.5396396194883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id5e8cde6a"
      uniprot "NA"
    ]
    graphics [
      x 661.7337539650423
      y 706.3216120469781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_17"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b5e59"
      uniprot "NA"
    ]
    graphics [
      x 912.718733792858
      y 861.007784670777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_24"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba761"
      uniprot "NA"
    ]
    graphics [
      x 1384.4395556153438
      y 1469.6087977466545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_105"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id5db145b0"
      uniprot "NA"
    ]
    graphics [
      x 1761.0869064119215
      y 486.27846113175434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id51069b65"
      uniprot "NA"
    ]
    graphics [
      x 801.9790111023126
      y 1039.8616693991758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_57"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "dca81"
      uniprot "NA"
    ]
    graphics [
      x 1160.5513187910142
      y 1238.2419880909106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33596266"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd827ab35"
      uniprot "NA"
    ]
    graphics [
      x 531.803125190909
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "Endosome"
      name "Endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b8c2d"
      uniprot "NA"
    ]
    graphics [
      x 421.38572334890466
      y 114.39347513780763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Endosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q165399;urn:miriam:pubmed:32205204"
      hgnc "NA"
      map_id "azithromycin"
      name "azithromycin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac609"
      uniprot "NA"
    ]
    graphics [
      x 288.02418149861546
      y 1252.261063152715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "azithromycin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:19893639;PUBMED:17667842"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b22d1"
      uniprot "NA"
    ]
    graphics [
      x 428.7190442899348
      y 1404.8232992273076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:30406125"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_67"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1bdb"
      uniprot "NA"
    ]
    graphics [
      x 205.6109702287423
      y 1053.3141246471005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_23"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "b9579"
      uniprot "NA"
    ]
    graphics [
      x 1013.3494596765764
      y 449.9224113694347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O60603"
      hgnc "NA"
      map_id "UNIPROT:O60603"
      name "TLR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33e6"
      uniprot "UNIPROT:O60603"
    ]
    graphics [
      x 1175.5168163857047
      y 532.2425132994574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60603"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q6UXN2; urn:miriam:ensembl:ENSG00000188056;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:285852;urn:miriam:ncbigene:285852;urn:miriam:refseq:NM_198153;urn:miriam:hgnc.symbol:TREML4;urn:miriam:hgnc.symbol:TREML4;urn:miriam:uniprot:Q6UXN2;urn:miriam:uniprot:Q6UXN2;urn:miriam:hgnc:30807"
      hgnc "NA; HGNC_SYMBOL:TREML4"
      map_id "UNIPROT:Q6UXN2"
      name "TREML4_space_; TREML4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d4fdc; sa166"
      uniprot "UNIPROT:Q6UXN2"
    ]
    graphics [
      x 1557.440866274535
      y 1023.580025456977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6UXN2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_100"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2aa49a5d"
      uniprot "NA"
    ]
    graphics [
      x 1726.7333921705163
      y 1021.5099658763531
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d52dc"
      uniprot "NA"
    ]
    graphics [
      x 1247.2845200682432
      y 1467.290481131427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id97a8368b"
      uniprot "NA"
    ]
    graphics [
      x 1278.7947504522172
      y 685.9179326004488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_116"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idf6e0bc7f"
      uniprot "NA"
    ]
    graphics [
      x 1362.710780739698
      y 984.2219884751808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_61"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4da"
      uniprot "NA"
    ]
    graphics [
      x 745.526519503058
      y 249.83312847450054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f88d9"
      uniprot "NA"
    ]
    graphics [
      x 1400.8606776553952
      y 1040.7546847066476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_48"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d3137"
      uniprot "NA"
    ]
    graphics [
      x 356.4834277006778
      y 960.1189750327485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aef8e"
      uniprot "NA"
    ]
    graphics [
      x 1206.8718941257448
      y 1533.854445713632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_87"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f3314"
      uniprot "NA"
    ]
    graphics [
      x 644.4463331445489
      y 923.9296134344844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_25"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "bb605"
      uniprot "NA"
    ]
    graphics [
      x 862.2313611358124
      y 321.9667238411819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bd252"
      uniprot "NA"
    ]
    graphics [
      x 470.08822453601647
      y 970.8638368515868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_60"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4c5"
      uniprot "NA"
    ]
    graphics [
      x 917.6939758593483
      y 514.6082196640509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7c297d34"
      uniprot "NA"
    ]
    graphics [
      x 1222.2667623348723
      y 1150.2872989707835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_108"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id72e167d2"
      uniprot "NA"
    ]
    graphics [
      x 1486.7670700166736
      y 1149.7955389485076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 110
    source 3
    target 13
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp10"
      target_id "W16_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 13
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_46"
      target_id "MDA5_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 3
    target 15
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp10"
      target_id "W16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 15
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_73"
      target_id "RIG_minus_I_space_(DDX58)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 17
    target 18
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "dd322"
      target_id "W16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 18
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_101"
      target_id "STAT1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 20
    target 21
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "NFkB"
      target_id "W16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 21
    target 22
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_96"
      target_id "Pro_minus_inflammatory_space__br_cytokines_space_production"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 5
    target 23
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp16"
      target_id "W16_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 23
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_10"
      target_id "RIG_minus_I_space_(DDX58)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 24
    target 25
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "ISGs"
      target_id "W16_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 25
    target 26
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_33"
      target_id "c82e3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 27
    target 28
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "TRAF3"
      target_id "W16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 28
    target 29
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_82"
      target_id "bc0e3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 30
    target 31
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "TRIF_br_"
      target_id "W16_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 31
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_2"
      target_id "TRAF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 32
    target 33
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "IRAK4"
      target_id "W16_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 33
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_35"
      target_id "TRAF6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 4
    target 34
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp15"
      target_id "W16_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 34
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_66"
      target_id "MDA5_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 35
    target 36
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y2C9"
      target_id "W16_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 36
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_18"
      target_id "MYD88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 38
    target 39
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp14"
      target_id "W16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 39
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_41"
      target_id "MDA5_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 26
    target 40
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "c82e3"
      target_id "W16_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 40
    target 41
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_12"
      target_id "Innate_space_Immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 42
    target 43
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "b8d9b"
      target_id "W16_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 43
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_112"
      target_id "INF_minus_I_space_alpha_slash__space_beta"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 6
    target 45
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp13"
      target_id "W16_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 45
    target 8
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_113"
      target_id "TBK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 10
    target 46
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 46
    target 47
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_43"
      target_id "db1fb"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 48
    target 49
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "b8407"
      target_id "W16_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 49
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_81"
      target_id "dd322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 44
    target 50
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "INF_minus_I_space_alpha_slash__space_beta"
      target_id "W16_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 50
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_109"
      target_id "INF_minus_I_space_alpha_slash__space_beta"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 51
    target 52
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 52
    target 1
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_78"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 53
    target 54
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15455"
      target_id "W16_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 54
    target 30
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_54"
      target_id "TRIF_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 47
    target 55
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "db1fb"
      target_id "W16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 55
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_97"
      target_id "INF_minus_I_space_alpha_slash__space_beta"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 51
    target 56
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 56
    target 57
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_4"
      target_id "UNIPROT:O00206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 58
    target 59
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NR96"
      target_id "W16_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 59
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_14"
      target_id "MYD88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 29
    target 60
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "bc0e3"
      target_id "W16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 60
    target 10
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_93"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 51
    target 61
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 61
    target 62
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_104"
      target_id "SARS_minus_CoV_space_2_space_RNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 44
    target 63
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "INF_minus_I_space_alpha_slash__space_beta"
      target_id "W16_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 63
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_107"
      target_id "INF_minus_I_space_alpha_slash__space_beta"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 7
    target 64
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp1"
      target_id "W16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 64
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_86"
      target_id "STAT1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 62
    target 65
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2_space_RNA"
      target_id "W16_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 65
    target 53
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_114"
      target_id "UNIPROT:O15455"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 37
    target 66
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "MYD88"
      target_id "W16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 66
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_45"
      target_id "TRAF6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 44
    target 67
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "INF_minus_I_space_alpha_slash__space_beta"
      target_id "W16_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 67
    target 48
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_102"
      target_id "b8407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 68
    target 69
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "d46a8"
      target_id "W16_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 69
    target 24
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_28"
      target_id "ISGs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 70
    target 71
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "GRL0617"
      target_id "W16_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 71
    target 72
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_68"
      target_id "PLpro_space_(nsp3)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 10
    target 73
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 73
    target 42
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_58"
      target_id "b8d9b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 74
    target 75
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "dffb5"
      target_id "W16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 75
    target 68
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_65"
      target_id "d46a8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 16
    target 76
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "RIG_minus_I_space_(DDX58)"
      target_id "W16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 76
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_80"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 57
    target 77
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00206"
      target_id "W16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 77
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_90"
      target_id "MYD88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 38
    target 78
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp14"
      target_id "W16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 78
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_44"
      target_id "RIG_minus_I_space_(DDX58)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 62
    target 79
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2_space_RNA"
      target_id "W16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 79
    target 80
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_40"
      target_id "TLR7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 57
    target 81
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00206"
      target_id "W16_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 81
    target 30
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_32"
      target_id "TRIF_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 62
    target 82
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2_space_RNA"
      target_id "W16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 82
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_106"
      target_id "RIG_minus_I_space_(DDX58)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 62
    target 83
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2_space_RNA"
      target_id "W16_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 83
    target 58
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_17"
      target_id "UNIPROT:Q9NR96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 72
    target 84
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "PLpro_space_(nsp3)"
      target_id "W16_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 84
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_24"
      target_id "TRAF6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 7
    target 85
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp1"
      target_id "W16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 85
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_105"
      target_id "INF_minus_I_space_alpha_slash__space_beta"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 12
    target 86
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "MAVS"
      target_id "W16_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 86
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_103"
      target_id "TRAF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 72
    target 87
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "PLpro_space_(nsp3)"
      target_id "W16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 87
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_57"
      target_id "TRAF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 1
    target 88
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "W16_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 88
    target 89
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_115"
      target_id "Endosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 90
    target 91
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "azithromycin"
      target_id "W16_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 91
    target 22
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_15"
      target_id "Pro_minus_inflammatory_space__br_cytokines_space_production"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 90
    target 92
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "azithromycin"
      target_id "W16_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 92
    target 68
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_67"
      target_id "d46a8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 51
    target 93
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 93
    target 94
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_23"
      target_id "UNIPROT:O60603"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 95
    target 96
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q6UXN2"
      target_id "W16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 96
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_100"
      target_id "STAT1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 72
    target 97
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "PLpro_space_(nsp3)"
      target_id "W16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 97
    target 9
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_51"
      target_id "IKBKE"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 94
    target 98
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60603"
      target_id "W16_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 98
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_111"
      target_id "MYD88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 80
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "TLR7"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 95
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q6UXN2"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 99
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_116"
      target_id "MYD88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 51
    target 100
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 100
    target 1
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_61"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 37
    target 101
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "MYD88"
      target_id "W16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 101
    target 32
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_89"
      target_id "IRAK4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 5
    target 102
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "nsp16"
      target_id "W16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 102
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_48"
      target_id "MDA5_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 11
    target 103
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "TRAF6"
      target_id "W16_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 103
    target 20
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_11"
      target_id "NFkB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 62
    target 104
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2_space_RNA"
      target_id "W16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 104
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_87"
      target_id "MDA5_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 51
    target 105
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 105
    target 2
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_25"
      target_id "UNIPROT:O15393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 14
    target 106
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "MDA5_space_"
      target_id "W16_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 106
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_29"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 51
    target 107
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_space_2"
      target_id "W16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 107
    target 35
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_60"
      target_id "UNIPROT:Q9Y2C9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 32
    target 108
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "IRAK4"
      target_id "W16_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 108
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_110"
      target_id "TRAF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 72
    target 109
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "PLpro_space_(nsp3)"
      target_id "W16_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 109
    target 10
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_108"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
