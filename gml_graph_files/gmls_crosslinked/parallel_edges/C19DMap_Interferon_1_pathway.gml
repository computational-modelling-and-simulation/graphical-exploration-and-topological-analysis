# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9694516; WP5038; WP5039; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2; urn:miriam:uniprot:P0DTD2; urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "UNIPROT:P0DTD2"
      name "9b_space_homodimer; ORF9b; PLpro; Orf9b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_3481; e62df; bca7b; f4882; sa139; sa1878; sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 750.5199350332136
      y 1539.7593940807074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1451.9096236513708
      y 1255.2238679753457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NYK1; urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284; urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "NA; HGNC_SYMBOL:TLR7"
      map_id "UNIPROT:Q9NYK1"
      name "TLR7; TLR7:ssRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "d516b; sa55; sa430; csa90"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 541.0490143301654
      y 987.1078127440746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Interferon 1 pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P45983; urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599; urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "NA; HGNC_SYMBOL:MAPK8"
      map_id "UNIPROT:P45983"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b098e; sa63; path_0_sa40; path_0_sa41"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 617.1289240458071
      y 1531.2529875932182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 10
      diagram "WP5038; C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q86WV6; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q86WV6;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:obo.go:GO%3A1990231;urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA; HGNC_SYMBOL:STING1"
      map_id "UNIPROT:Q86WV6"
      name "TMEM173; STING1; cGAMP:STING; cGAMP:STING:LC3; STING; cGAMP_minus_STING"
      node_subtype "GENE; PROTEIN; COMPLEX"
      node_type "species"
      org_id "c488d; sa172; sa171; csa3; csa4; csa13; sa100; sa98; csa11; csa16"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1308.8956343775508
      y 1629.5911441001117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86WV6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455; urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "NA; HGNC_SYMBOL:TLR3"
      map_id "UNIPROT:O15455"
      name "TLR3; TLR3_underscore_TRIF; TLR3_underscore_TRIF_underscore_RIPK1; TLR3:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "ab922; csa37; sa239; csa38; sa5; csa88; sa45; sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 680.2813787672553
      y 1259.988352111403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15455"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O00206; urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "NA; HGNC_SYMBOL:TLR4"
      map_id "UNIPROT:O00206"
      name "TLR4; TLR4_underscore_TRIF_underscore_TRAM"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c7d93; csa47; sa291; sa142"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 888.8299136842559
      y 1124.032976046291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NR96; urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633"
      hgnc "NA; HGNC_SYMBOL:TLR9"
      map_id "UNIPROT:Q9NR96"
      name "TLR9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "adc15; sa238"
      uniprot "UNIPROT:Q9NR96"
    ]
    graphics [
      x 558.5072885848039
      y 1056.4194511082462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NR96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikidata:Q27097846; urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "GRL0617"
      name "GRL0617"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "ebd2b; sa168"
      uniprot "NA"
    ]
    graphics [
      x 694.9317359615691
      y 696.3549748781081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GRL0617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q6UXN2; urn:miriam:ensembl:ENSG00000188056;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:285852;urn:miriam:ncbigene:285852;urn:miriam:refseq:NM_198153;urn:miriam:hgnc.symbol:TREML4;urn:miriam:hgnc.symbol:TREML4;urn:miriam:uniprot:Q6UXN2;urn:miriam:uniprot:Q6UXN2;urn:miriam:hgnc:30807"
      hgnc "NA; HGNC_SYMBOL:TREML4"
      map_id "UNIPROT:Q6UXN2"
      name "TREML4_space_; TREML4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d4fdc; sa166"
      uniprot "UNIPROT:Q6UXN2"
    ]
    graphics [
      x 924.9090011899663
      y 854.590648412228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6UXN2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4880; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:wikidata:Q89457519; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58761;urn:miriam:ncbiprotein:YP_009724397.2; urn:miriam:ncbiprotein:1798174255"
      hgnc "NA"
      map_id "N"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e932d; sa140; sa352; sa381"
      uniprot "NA"
    ]
    graphics [
      x 1157.5160897056512
      y 1139.5826343562194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1; urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "Nsp6"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713; sa48; sa307; sa304; sa2204; sa2367"
      uniprot "NA"
    ]
    graphics [
      x 1364.6047760223944
      y 1112.7668768562382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742612; urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "Nsp5"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa693; sa251; sa2241; sa2366"
      uniprot "NA"
    ]
    graphics [
      x 1315.8258553844846
      y 830.3321892996862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:APO40582;urn:miriam:pubmed:16845612;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "UNIPROT:M"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193; sa134; sa226; sa232; sa408; sa359; sa353; sa403; sa42"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1273.2136815579224
      y 1180.5558354059124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:M"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3; urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "UNIPROT:Nsp3"
      name "Nsp3; Nsp3:Nsp4:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa162; csa39; sa123; sa169; sa2222; sa357; sa361; sa349; sa354; sa363"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 813.7015122429152
      y 1145.718569608317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon 1 pathway"
      full_annotation "NA; urn:miriam:pubmed:24622840;urn:miriam:ncbiprotein:YP_009724389"
      hgnc "NA"
      map_id "pp1ab"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph40; glyph53; sa184"
      uniprot "NA"
    ]
    graphics [
      x 1107.64745933839
      y 1483.974985188838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pp1ab"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023"
      hgnc "NA"
      map_id "M16_238"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 429.15283908964665
      y 394.3807237815053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 553.9418548872973
      y 435.7351636698722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT1;HGNC_SYMBOL:IRF9"
      map_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      name "ISRE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
    ]
    graphics [
      x 728.9927079536069
      y 358.45706031196283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940"
      hgnc "HGNC_SYMBOL:OAS3"
      map_id "UNIPROT:Q9Y6K5"
      name "OAS3"
      node_subtype "GENE; RNA; PROTEIN"
      node_type "species"
      org_id "sa77; sa78; sa157; sa80"
      uniprot "UNIPROT:Q9Y6K5"
    ]
    graphics [
      x 358.6139023332105
      y 433.5229179316289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6K5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 19
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "UNIPROT:Q14653"
      name "IRF3_underscore_homodimer; IRF3; IFNB1_space_expression_space_complex; p38_minus_NFkB"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa46; sa121; sa286; csa45; sa120; sa348; sa119; sa156; sa157; sa379; sa256; csa8; sa85; sa84; sa89; csa41; sa87; sa88; sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 801.9333389223111
      y 589.0009420118004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "EIF2AK"
      name "EIF2AK"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "sa48; sa51; sa47; sa160"
      uniprot "NA"
    ]
    graphics [
      x 492.1433127387903
      y 364.66172763920304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF2AK"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 375.83529136807306
      y 327.2936964895791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528; urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "UNIPROT:Q14164"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa119; sa116; sa118; sa427; sa491"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 1014.939336574445
      y 1483.5497081154472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:18353649;PUBMED:31226023;PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 849.671819081461
      y 1370.268190401564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:Q9UHD2"
      name "TBK1; STING:TBK1"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa61; sa60; sa426; sa117; sa490; sa154; sa80; csa20; csa5; sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 835.2962220679034
      y 1485.2424681915545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_55"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re129"
      uniprot "NA"
    ]
    graphics [
      x 861.643414922561
      y 692.8095159526439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84"
      hgnc "HGNC_SYMBOL:OAS2"
      map_id "UNIPROT:P29728"
      name "OAS2"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "sa74; sa158; sa76; sa73"
      uniprot "UNIPROT:P29728"
    ]
    graphics [
      x 421.8217741092709
      y 498.873310920222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29728"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 331.77787303478306
      y 609.7296583249048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454"
      hgnc "HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNAR1"
      map_id "UNIPROT:P48551;UNIPROT:P17181"
      name "IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P48551;UNIPROT:P17181"
    ]
    graphics [
      x 1787.1322093380118
      y 303.56924944871827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48551;UNIPROT:P17181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32913009;PUBMED:24362405"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1785.1605749782905
      y 452.07600008083244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "UNIPROT:P01574"
      name "IFNB1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa3; sa27; sa26; sa34; sa33; sa147; sa89; sa87; sa91; sa88"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1555.2448037413533
      y 499.53945496735747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01574"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454"
      hgnc "HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNB1;HGNC_SYMBOL:IFNAR1"
      map_id "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
      name "IFNB1_underscore_IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
    ]
    graphics [
      x 1895.0167151984358
      y 422.5670255088705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840;PUBMED:25636800;PUBMED:26631542;PUBMED:32979938;PUBMED:33337934;PUBMED:32733001"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1208.022799981989
      y 1465.7337589063973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:NLRP3 inflammasome activation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:Q7Z434"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151; sa156; sa127; sa128; sa102; sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1115.5928290350146
      y 1390.5508295636155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:CASP1;HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q9ULZ3;UNIPROT:P29466;UNIPROT:Q96P20"
      name "NLRP3_underscore_inflammasome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:P29466;UNIPROT:Q96P20"
    ]
    graphics [
      x 798.8297351928106
      y 936.0584689780493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9ULZ3;UNIPROT:P29466;UNIPROT:Q96P20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:32133002;PUBMED:24265316;PUBMED:28531279"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 832.2278149278715
      y 824.4778234649697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:E protein interactions; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "UNIPROT:E"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131; sa13; sa32; sa22; sa19; sa83; sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 594.8532964374815
      y 886.8751999504753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:Q04206;UNIPROT:P19838"
      name "p50_underscore_p65; IkB_underscore_p50_underscore_p65; NF_minus_kB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34; csa41; csa42; csa6; csa19"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1075.1332092851658
      y 463.6368027042521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04206;UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "MNS"
      name "MNS"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa237"
      uniprot "NA"
    ]
    graphics [
      x 724.4599850476679
      y 878.4756204391143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MNS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3; HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:CASP1"
      map_id "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
      name "NLRP3_underscore_inflammasome; NLRP3_space_oligomer:ASC:Caspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30; csa78"
      uniprot "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 930.1919306965891
      y 993.2679754632925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840;PUBMED:22312431"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1244.478572432914
      y 1538.9296881254022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re121"
      uniprot "NA"
    ]
    graphics [
      x 740.0943608090738
      y 531.0571512904609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IKKa_underscore_IKKb_underscore_NEMO"
      name "IKKa_underscore_IKKb_underscore_NEMO"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "NA"
    ]
    graphics [
      x 420.51024836027307
      y 654.5173925269175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKKa_underscore_IKKb_underscore_NEMO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 548.4031481338645
      y 686.9900937254345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:1802476818"
      hgnc "NA"
      map_id "Nsp15"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa129; sa227; sa316; sa2205; sa415; sa416"
      uniprot "NA"
    ]
    graphics [
      x 747.4876160448698
      y 794.9798265073684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 728.6935773200358
      y 1224.1425413829015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:32133002"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_35"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1067.6443095565505
      y 1023.5915540273609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:mesh:D007249"
      hgnc "NA"
      map_id "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
      name "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 1189.5083376606337
      y 892.1014212574778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415; urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "UNIPROT:P42224"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa305; sa68; sa4; sa5"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 1413.0292771913043
      y 835.2975355095004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 1491.585045107116
      y 1050.2906524490454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725297; urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "Nsp1"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132; sa2216; sa2356"
      uniprot "NA"
    ]
    graphics [
      x 1527.3958532940892
      y 898.4172603106848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725318.1"
      hgnc "NA"
      map_id "Orf7b"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa308"
      uniprot "NA"
    ]
    graphics [
      x 1647.8367361543228
      y 967.8424038241552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:pubmed:32979938"
      hgnc "NA"
      map_id "Orf3a"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1495.2119039884155
      y 953.3303055924299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf3a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_214"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 793.9749794798835
      y 320.4515369907662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 724.404261054062
      y 432.70279412421263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ensembl:ENSG00000089127; urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS1"
      map_id "UNIPROT:P00973"
      name "OAS1"
      node_subtype "GENE; RNA; PROTEIN"
      node_type "species"
      org_id "sa44; sa45; sa159; sa50"
      uniprot "UNIPROT:P00973"
    ]
    graphics [
      x 579.0815542006596
      y 340.55400890038993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00973"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 652.2805315161783
      y 232.47817948311945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581; urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "UNIPROT:P52630"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306; sa69; sa7; sa6"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 1478.685243371599
      y 852.5898022372294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52630"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re137"
      uniprot "NA"
    ]
    graphics [
      x 1574.8533028611946
      y 1066.9877122078433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009724395.1"
      hgnc "NA"
      map_id "Orf7a"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 1626.4070260113817
      y 895.638589202807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf7a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS3;HGNC_SYMBOL:OAS2;HGNC_SYMBOL:OAS1"
      map_id "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
      name "OAS1_underscore_EIF2AK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
    ]
    graphics [
      x 559.1619599256387
      y 165.5042389144711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_107"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 742.4655037837002
      y 263.0750742963712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:mesh:D007113"
      hgnc "NA"
      map_id "ISG_space_expression_underscore_antiviral_space_response"
      name "ISG_space_expression_underscore_antiviral_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa81"
      uniprot "NA"
    ]
    graphics [
      x 923.7468590210943
      y 465.6500064699549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ISG_space_expression_underscore_antiviral_space_response"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "NAP1"
      name "NAP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa296; sa297"
      uniprot "NA"
    ]
    graphics [
      x 1003.9768001664562
      y 1403.5191540407027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 928.1946394225404
      y 1277.0559333704778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206; urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "UNIPROT:P19838;UNIPROT:Q04206"
      name "p50_underscore_p65; NFKB; P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21; csa80; csa103; csa7"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1321.4369980967917
      y 328.6381471055233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838;UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1208.524699501712
      y 350.2652815324548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TBK1;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE"
      map_id "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
      name "TRAF3_underscore_TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      x 1109.1081997589304
      y 1788.9707466926393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1188.8786675014085
      y 1679.7876652891214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:24622840;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:IKBKE"
      map_id "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
      name "TRAF3_underscore_TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
    ]
    graphics [
      x 1137.5322838641782
      y 1548.8249581305008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:33037393;PUBMED:19380580;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 1061.6295223009076
      y 1323.7251437950829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TANK"
      map_id "UNIPROT:Q92844"
      name "TANK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209; sa492"
      uniprot "UNIPROT:Q92844"
    ]
    graphics [
      x 1196.6682289266723
      y 1355.0160532716395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:TANK"
      map_id "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
      name "TRAF3_underscore_TANK_underscore_TBK1_underscore_IKKepsilon"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
    ]
    graphics [
      x 1014.9503104428917
      y 1154.460221830742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053; urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "UNIPROT:P05161"
      name "ISG15"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "sa299; sa301; sa298"
      uniprot "UNIPROT:P05161"
    ]
    graphics [
      x 845.5969564850599
      y 403.04929126147437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:29769653"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 921.2821706614093
      y 266.3409207454107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 536.9593273086778
      y 617.289305266986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_47"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 1603.0412027280638
      y 333.5144178991388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "type_space_I_space_IFN_space_response"
      name "type_space_I_space_IFN_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa260"
      uniprot "NA"
    ]
    graphics [
      x 1609.2966005392238
      y 169.068466697119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "type_space_I_space_IFN_space_response"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "UNIPROT:Q13114"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82; sa83; sa493; sa11; sa116; sa99; sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 870.7526321436774
      y 1556.8749394397837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 742.3002160442318
      y 1362.1173241015322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "UNIPROT:Q99836"
      name "MYD88_underscore_TRAM; MYD88"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa20; sa57; sa77; sa433; sa143; sa100; sa49; sa144"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 605.6673269184213
      y 1229.5899527547822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99836"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "UNIPROT:P01562"
      name "IFNA1"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa30; sa31; sa38; sa39; sa5; sa146; sa88; sa90; sa86"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 1449.8938228727613
      y 326.54757068569097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01562"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 1511.7167595406006
      y 229.67899420353717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1; urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "UNIPROT:Q9BYX4"
      name "IFIH1; IFIH1:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa198; sa154; csa93; sa439; sa78"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 1163.2959234753987
      y 1001.1725809148112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYX4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:33348292;PUBMED:28158275"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1040.1282170919817
      y 850.9791414917585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2955"
      hgnc "NA"
      map_id "Azithromycin"
      name "Azithromycin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa320; sa170; sa321"
      uniprot "NA"
    ]
    graphics [
      x 1031.746543969465
      y 963.246943476393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Azithromycin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:12692549"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_103"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 1069.285183615299
      y 387.0857169871948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:33348292"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_43"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 851.7467377668459
      y 1056.0070391646757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "TRIF"
      name "TRIF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa240"
      uniprot "NA"
    ]
    graphics [
      x 824.5331016044515
      y 1218.308180749939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TRIF"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "Viral_space_dsRNA"
      name "Viral_space_dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa259; sa187; sa186"
      uniprot "NA"
    ]
    graphics [
      x 987.5546320086703
      y 844.5726201616875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Viral_space_dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:O95786"
      name "DDX58; DDX58:dsRNA; RIG_minus_I:dsRNA; RIG_minus_I"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa200; sa155; sa23; csa92; sa124; sa440; csa37; csa36; sa99"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1322.0784867258458
      y 988.0525861119609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309;PUBMED:33024073;PUBMED:33348292;PUBMED:28148787"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 1210.1337001394295
      y 958.5491384758222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:TRIM25"
      map_id "UNIPROT:Q14258"
      name "TRIM25"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201; sa122"
      uniprot "UNIPROT:Q14258"
    ]
    graphics [
      x 1152.7998710598645
      y 1079.284672088298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481"
      hgnc "HGNC_SYMBOL:RNF135"
      map_id "UNIPROT:Q8IUD6"
      name "RNF135"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202; sa123"
      uniprot "UNIPROT:Q8IUD6"
    ]
    graphics [
      x 1276.1782668584246
      y 877.3071878405613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUD6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "UNIPROT:Q16539"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64; sa368; sa445; sa49; sa50; path_0_sa42; path_0_sa43; path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 464.2829554028196
      y 1589.1728009903823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 515.1353376347979
      y 1463.5928692491477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25; urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "UNIPROT:O43318"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86; sa85; sa367; sa435"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 373.45683197594997
      y 1556.0735983140971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK14"
      map_id "UNIPROT:P45983;UNIPROT:Q16539"
      name "MAPK8_slash_14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P45983;UNIPROT:Q16539"
    ]
    graphics [
      x 440.1969535976747
      y 1227.2704533736892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983;UNIPROT:Q16539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "UNIPROT:Q92985"
      name "IRF7_underscore_homodimer; IRF7"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa43; csa44; sa281; sa211; sa213; sa255; sa347; sa120"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1074.628307795973
      y 632.0973325671291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92985"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_54"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re128"
      uniprot "NA"
    ]
    graphics [
      x 960.3001664623481
      y 529.7619666875653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:STAT2;HGNC_SYMBOL:IRF9"
      map_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
      name "STAT1_slash_2_underscore_IRF9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
    ]
    graphics [
      x 1140.7171361826934
      y 604.787006441194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_65"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 988.7779971873712
      y 572.2978343402997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724394.1"
      hgnc "NA"
      map_id "Orf6"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa311; sa127; sa312"
      uniprot "NA"
    ]
    graphics [
      x 1094.8288022839922
      y 777.4981904673372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_106"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 690.2064373097573
      y 994.9101542463258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IRAK1_slash_4"
      name "IRAK1_slash_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314; sa292"
      uniprot "NA"
    ]
    graphics [
      x 695.4727223612681
      y 1067.6536102643424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRAK1_slash_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:18157;urn:miriam:refseq:NM_153497;urn:miriam:uniprot:Q15750;urn:miriam:uniprot:Q15750;urn:miriam:ensembl:ENSG00000100324;urn:miriam:hgnc.symbol:TAB1;urn:miriam:hgnc.symbol:TAB1;urn:miriam:ncbigene:10454;urn:miriam:ncbigene:10454"
      hgnc "HGNC_SYMBOL:TAB1"
      map_id "UNIPROT:Q15750"
      name "TAB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa254"
      uniprot "UNIPROT:Q15750"
    ]
    graphics [
      x 247.26305800085902
      y 1332.0979238553687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_45"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 335.0980488090679
      y 1235.1544096217076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118"
      hgnc "HGNC_SYMBOL:TAB2"
      map_id "UNIPROT:Q9NYJ8"
      name "TAB2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:Q9NYJ8"
    ]
    graphics [
      x 218.46340146277964
      y 1184.8371608364887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYJ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6; urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "UNIPROT:Q9Y4K3"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59; sa58; sa428; sa265; sa337; sa422; sa129; sa135; sa101; sa38"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 384.06588222986977
      y 1391.814683303353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4K3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "TAK1"
      name "TAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 200.12112440418332
      y 1263.6767966875734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TAK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc:18157;urn:miriam:refseq:NM_153497;urn:miriam:uniprot:Q15750;urn:miriam:uniprot:Q15750;urn:miriam:ensembl:ENSG00000100324;urn:miriam:hgnc.symbol:TAB1;urn:miriam:hgnc.symbol:TAB1;urn:miriam:ncbigene:10454;urn:miriam:ncbigene:10454"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:TAB1"
      map_id "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
      name "TAB1_slash_2_underscore_TRAF6_underscore_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
    ]
    graphics [
      x 219.909914648327
      y 1019.0277859802848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:25135833;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 595.7940553865697
      y 1359.7520860357488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "UNIPROT:Q96J02"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa204; sa203; sa436; sa405"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 691.657697871654
      y 1532.3081980896072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96J02"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:JUN"
      map_id "UNIPROT:P05412"
      name "JUN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P05412"
    ]
    graphics [
      x 460.6897830245552
      y 834.1945557821996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 422.90257323617965
      y 970.2338638122133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:FOS"
      map_id "UNIPROT:P01100"
      name "FOS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:P01100"
    ]
    graphics [
      x 368.9801941676211
      y 842.8840910608183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS; urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOS"
      map_id "UNIPROT:P05412;UNIPROT:P01100"
      name "AP_minus_1; AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10; csa79; csa104"
      uniprot "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      x 570.0481157976648
      y 802.8620753152484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05412;UNIPROT:P01100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_187"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa300"
      uniprot "NA"
    ]
    graphics [
      x 768.4822818492919
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:29769653;PUBMED:32553163"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 793.2212684543758
      y 198.34310833036943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:20457564"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_109"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re99"
      uniprot "NA"
    ]
    graphics [
      x 1156.267651710761
      y 693.3342697088683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_211"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 1126.0314419747394
      y 339.78052462797086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 1220.6776781109854
      y 445.1111362775926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228; urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUN"
      map_id "UNIPROT:P01100;UNIPROT:P05412"
      name "AP_minus_1; AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35; csa95"
      uniprot "UNIPROT:P01100;UNIPROT:P05412"
    ]
    graphics [
      x 1067.3591017085764
      y 541.8949645292794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01100;UNIPROT:P05412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 960.0141857851775
      y 337.4692264623645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227; urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:JAK1"
      map_id "UNIPROT:P23458"
      name "JAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9; sa36"
      uniprot "UNIPROT:P23458"
    ]
    graphics [
      x 1870.4686892970112
      y 326.733516930311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23458"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 1739.5655842227663
      y 379.32317081350595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597; urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:TYK2"
      map_id "UNIPROT:P29597"
      name "TYK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19; sa37"
      uniprot "UNIPROT:P29597"
    ]
    graphics [
      x 1718.031704007994
      y 479.3774486707105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29597"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNAR1;HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNA1"
      map_id "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
      name "IFNA1_underscore_IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
    ]
    graphics [
      x 1766.215376347841
      y 219.41887248859337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:TYK2;HGNC_SYMBOL:JAK1"
      map_id "UNIPROT:P29597;UNIPROT:P23458"
      name "JAK1_underscore_TYK2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P29597;UNIPROT:P23458"
    ]
    graphics [
      x 1502.3621771697117
      y 545.9282247234675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29597;UNIPROT:P23458"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_46"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 1500.0742868042007
      y 147.17013719359034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:17761676;PUBMED:25636800;PUBMED:17108024;PUBMED:32979938;PUBMED:29294448;PUBMED:14679297;PUBMED:31226023;PUBMED:24622840;PUBMED:25481026;PUBMED:33337934;PUBMED:18440553"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1004.3678210806969
      y 1074.0028780584503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:YP_009724396.1; urn:miriam:ncbiprotein:1796318604"
      hgnc "NA"
      map_id "Orf8"
      name "Orf8"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa27; sa355"
      uniprot "NA"
    ]
    graphics [
      x 1084.3946735228226
      y 1185.4175626852048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32913009"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 1653.4198581821986
      y 251.4978732925266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32665127"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1346.069712724311
      y 558.1643858546133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:mesh:D014779"
      hgnc "NA"
      map_id "Viral_space_replication"
      name "Viral_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1344.699310894526
      y 454.07271433317237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Viral_space_replication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1611.7926010708757
      y 617.6068703218299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_234"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa75"
      uniprot "NA"
    ]
    graphics [
      x 484.905622741783
      y 598.0526564586846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 597.3548135247906
      y 492.45454888606235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:Q14164;UNIPROT:Q9UHD2"
      name "TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q14164;UNIPROT:Q9UHD2"
    ]
    graphics [
      x 963.4803833057847
      y 1566.2503055660882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14164;UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 1015.9783600737046
      y 1677.0805596969153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re127"
      uniprot "NA"
    ]
    graphics [
      x 909.3969398299554
      y 582.4093342933236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32726355;PUBMED:19052324"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 1109.1890913705727
      y 981.5345770713868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "Nsp10"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa317; sa2200; sa2360"
      uniprot "NA"
    ]
    graphics [
      x 1249.3262825384538
      y 1046.8431157260015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "Nsp14"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318; sa2206; sa35; sa120; sa410"
      uniprot "NA"
    ]
    graphics [
      x 1258.493886552667
      y 981.1317629354182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "Nsp16"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa319; sa2199"
      uniprot "NA"
    ]
    graphics [
      x 1215.390468015511
      y 1093.8921575521285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:IFIH1;HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:Q9BYX4;UNIPROT:O95786"
      name "RIG1_underscore_MDA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q9BYX4;UNIPROT:O95786"
    ]
    graphics [
      x 1020.0735088674352
      y 1246.2120262473209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYX4;UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_108"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 898.556021177596
      y 791.6299141247663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:RELA"
      map_id "UNIPROT:Q04206"
      name "RELA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa268; sa482"
      uniprot "UNIPROT:Q04206"
    ]
    graphics [
      x 999.1744416146428
      y 86.73971362342274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:33139913;PUBMED:31426357"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_49"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 1061.239685670574
      y 195.88331068924538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P19838"
      name "NFKB1; p105; p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269; sa483; sa81; sa151; sa84; sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 920.8236334198701
      y 152.0845540638934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IkB"
      name "IkB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa270; sa278"
      uniprot "NA"
    ]
    graphics [
      x 1168.042473761427
      y 109.60944580144655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IkB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 572.1277228348739
      y 1534.3564047847817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1410.584074605063
      y 202.9080103335085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33348292;PUBMED:19380580"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 926.6752620245273
      y 1358.8890827562252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re131"
      uniprot "NA"
    ]
    graphics [
      x 735.9566640195633
      y 1152.7924211770176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 419.9352584706017
      y 262.6257226208421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:33473130"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 795.4980055025935
      y 751.5153852745243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "UNIPROT:P05161;UNIPROT:Nsp3"
      name "ISG15_underscore_Nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:P05161;UNIPROT:Nsp3"
    ]
    graphics [
      x 845.583626432686
      y 638.9664208332275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05161;UNIPROT:Nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re130"
      uniprot "NA"
    ]
    graphics [
      x 952.1877130343164
      y 679.4381621796441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 661.3012959195346
      y 561.4150740203431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 1008.3190887983808
      y 715.1827383955418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1672.2821740078984
      y 552.0805214902493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_52"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re122"
      uniprot "NA"
    ]
    graphics [
      x 1196.877667149425
      y 249.31903659712975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33024073"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1282.9513792658117
      y 727.8010933523623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131; urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:IRF9"
      map_id "UNIPROT:Q00978"
      name "IRF9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70; sa10"
      uniprot "UNIPROT:Q00978"
    ]
    graphics [
      x 1196.722811037584
      y 634.5953460445248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00978"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 232.5700646595667
      y 379.50140653781904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:19380580"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 1016.3536620319758
      y 908.5636839779503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 783.1166900443669
      y 667.2152198181857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_44"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 600.3812592223628
      y 1126.4709272437226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "RIP1"
      name "RIP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa247"
      uniprot "NA"
    ]
    graphics [
      x 651.4007744269591
      y 946.8937391154903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RIP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 272.6099380644122
      y 1501.7031145112892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_217"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 566.0581552603778
      y 267.5396731889373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 638.0841745194763
      y 396.82696298525116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "NEMO"
      name "NEMO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 135.89875946731274
      y 657.0313841904824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NEMO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "PUBMED:33139913;PUBMED:31426357"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re118"
      uniprot "NA"
    ]
    graphics [
      x 187.14046712389177
      y 787.1789352524695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IKKa"
      name "IKKa"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 63.55936855243385
      y 730.4413044810797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKKa"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "IKKb"
      name "IKKb"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa267"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 823.3917483112455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKKb"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_207"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 1348.2348725841143
      y 657.4933206754538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33348292"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 1229.7674496454297
      y 600.8378022773968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:19052324;PUBMED:25135833"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 908.98310906143
      y 1464.2926558608215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 597.6685017315638
      y 583.6464377919082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 183
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_238"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 20
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q9Y6K5"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 21
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q14653"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 20
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9Y6K5"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 21
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_77"
      target_id "UNIPROT:Q9Y6K5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "EIF2AK"
      target_id "M16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_72"
      target_id "EIF2AK"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14164"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 15
    target 25
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UHD2"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_82"
      target_id "UNIPROT:Q14164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 21
    target 27
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M16_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 21
    target 27
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M16_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 27
    target 21
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_55"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29728"
      target_id "M16_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_76"
      target_id "UNIPROT:P29728"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P48551;UNIPROT:P17181"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01574"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_37"
      target_id "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 26
    target 34
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 24
    target 34
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14164"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 5
    target 34
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q86WV6"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q7Z434"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 2
    target 34
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTD1"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 34
    target 26
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_74"
      target_id "UNIPROT:Q9UHD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3;UNIPROT:P29466;UNIPROT:Q96P20"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 38
    target 37
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:E"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 39
    target 37
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 40
    target 37
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "MNS"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 37
    target 41
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_100"
      target_id "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 5
    target 42
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 35
    target 42
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q7Z434"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 16
    target 42
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "pp1ab"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 42
    target 5
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_92"
      target_id "UNIPROT:Q86WV6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 39
    target 43
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "IKKa_underscore_IKKb_underscore_NEMO"
      target_id "M16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 43
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_51"
      target_id "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 28
    target 45
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29728"
      target_id "M16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 45
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_87"
      target_id "UNIPROT:P29728"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 15
    target 47
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 47
    target 15
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_91"
      target_id "UNIPROT:Nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 41
    target 48
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
      target_id "M16_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_35"
      target_id "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp1"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 12
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp6"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 2
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTD1"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 53
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf7b"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 54
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf3a"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 14
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_63"
      target_id "UNIPROT:P42224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_214"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 19
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P00973"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 21
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q14653"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 19
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P00973"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 21
    target 56
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_69"
      target_id "UNIPROT:P00973"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00973"
      target_id "M16_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_71"
      target_id "UNIPROT:P00973"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf7a"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 53
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf7b"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 12
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp6"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 2
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTD1"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_64"
      target_id "UNIPROT:P52630"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
      target_id "M16_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_107"
      target_id "ISG_space_expression_underscore_antiviral_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "NAP1"
      target_id "M16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 7
    target 66
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O00206"
      target_id "M16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_59"
      target_id "NAP1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M16_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 68
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_84"
      target_id "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 5
    target 70
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q86WV6"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 16
    target 70
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "pp1ab"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_93"
      target_id "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92844"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 6
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15455"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 14
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 35
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q7Z434"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_98"
      target_id "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05161"
      target_id "M16_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_61"
      target_id "UNIPROT:P05161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 20
    target 77
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K5"
      target_id "M16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 46
    target 77
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 77
    target 20
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_86"
      target_id "UNIPROT:Q9Y6K5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 32
    target 78
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M16_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_47"
      target_id "type_space_I_space_IFN_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99836"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 15
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_79"
      target_id "UNIPROT:Q13114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 83
    target 84
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M16_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_95"
      target_id "UNIPROT:P01562"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYX4"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 46
    target 86
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Azithromycin"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_94"
      target_id "UNIPROT:Q9BYX4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 39
    target 88
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 88
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_103"
      target_id "ISG_space_expression_underscore_antiviral_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 6
    target 89
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15455"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 90
    target 89
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "TRIF"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 91
    target 89
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "Viral_space_dsRNA"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 87
    target 89
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Azithromycin"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 89
    target 6
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_43"
      target_id "UNIPROT:O15455"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 11
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "N"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14258"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 95
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8IUD6"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 13
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp5"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 87
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Azithromycin"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_96"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16539"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 4
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45983"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 26
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UHD2"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43318"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_38"
      target_id "UNIPROT:P45983;UNIPROT:Q16539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M16_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_54"
      target_id "UNIPROT:Q92985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
      target_id "M16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf6"
      target_id "M16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 103
    target 19
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_65"
      target_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 82
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "IRAK1_slash_4"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 10
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6UXN2"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 3
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NYK1"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 8
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NR96"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 91
    target 105
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "Viral_space_dsRNA"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 105
    target 82
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_106"
      target_id "UNIPROT:Q99836"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 107
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15750"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NYJ8"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 110
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 111
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "TAK1"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 6
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15455"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 108
    target 112
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_45"
      target_id "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 110
    target 113
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 82
    target 113
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99836"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 15
    target 113
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q96J02"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 113
    target 110
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_73"
      target_id "UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05412"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 117
    target 116
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01100"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 38
    target 116
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:E"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 99
    target 116
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45983;UNIPROT:Q16539"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 116
    target 118
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_39"
      target_id "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 119
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_187"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 75
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P05161"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 19
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 75
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P05161"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 19
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 120
    target 75
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_60"
      target_id "UNIPROT:P05161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 39
    target 121
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 121
    target 49
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_109"
      target_id "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_211"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P01100;UNIPROT:P05412"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 83
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P01562"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 39
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 100
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q92985"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01100;UNIPROT:P05412"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 83
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01562"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 39
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 100
    target 123
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q92985"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 123
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_56"
      target_id "UNIPROT:P01562"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 75
    target 125
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05161"
      target_id "M16_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 125
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_66"
      target_id "ISG_space_expression_underscore_antiviral_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P23458"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29597"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 129
    target 127
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 33
    target 127
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 127
    target 130
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_105"
      target_id "UNIPROT:P29597;UNIPROT:P23458"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 83
    target 131
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M16_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 131
    target 79
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_46"
      target_id "type_space_I_space_IFN_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 21
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 15
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 11
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "N"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 133
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf8"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 104
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf6"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 16
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "pp1ab"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 35
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q7Z434"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 74
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 12
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp6"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 46
    target 132
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 132
    target 21
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_83"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 30
    target 134
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P48551;UNIPROT:P17181"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 83
    target 134
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 83
    target 134
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01562"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 134
    target 129
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_36"
      target_id "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 91
    target 135
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "Viral_space_dsRNA"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "Viral_space_replication"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 83
    target 135
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01562"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 32
    target 135
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01574"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 135
    target 91
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_90"
      target_id "Viral_space_dsRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 32
    target 137
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M16_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 137
    target 32
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_102"
      target_id "UNIPROT:P01574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_234"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 19
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 28
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P29728"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 21
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q14653"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 19
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 28
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P29728"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 21
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 139
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_75"
      target_id "UNIPROT:P29728"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 140
    target 141
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14164;UNIPROT:Q9UHD2"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 80
    target 141
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 16
    target 141
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "pp1ab"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 141
    target 69
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_42"
      target_id "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 100
    target 142
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 100
    target 142
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 142
    target 100
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_53"
      target_id "UNIPROT:Q92985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 92
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 85
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYX4"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 91
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Viral_space_dsRNA"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 11
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "N"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 46
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 14
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 46
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp10"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 145
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp14"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 146
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp16"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 143
    target 147
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_41"
      target_id "UNIPROT:Q9BYX4;UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 15
    target 148
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 148
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_108"
      target_id "ISG_space_expression_underscore_antiviral_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 151
    target 150
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 152
    target 150
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "IkB"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 150
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_49"
      target_id "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 114
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96J02"
      target_id "M16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 1
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD2"
      target_id "M16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 153
    target 114
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_97"
      target_id "UNIPROT:Q96J02"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 83
    target 154
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M16_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 154
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_67"
      target_id "UNIPROT:P01562"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 26
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 24
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14164"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 80
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13114"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 15
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 65
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "NAP1"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 87
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Azithromycin"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 155
    target 140
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_40"
      target_id "UNIPROT:Q14164;UNIPROT:Q9UHD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 7
    target 156
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00206"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 106
    target 156
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "IRAK1_slash_4"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 90
    target 156
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "TRIF"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 156
    target 7
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_58"
      target_id "UNIPROT:O00206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 57
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00973"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 20
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K5"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 22
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "EIF2AK"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 28
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29728"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 157
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_104"
      target_id "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 75
    target 158
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05161"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 15
    target 158
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Nsp3"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 9
    target 158
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "GRL0617"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_62"
      target_id "UNIPROT:P05161;UNIPROT:Nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 21
    target 160
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 104
    target 160
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf6"
      target_id "M16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 160
    target 21
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_57"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 57
    target 161
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00973"
      target_id "M16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 46
    target 161
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 161
    target 57
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_88"
      target_id "UNIPROT:P00973"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 87
    target 162
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "Azithromycin"
      target_id "M16_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 162
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_68"
      target_id "ISG_space_expression_underscore_antiviral_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 32
    target 163
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M16_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 163
    target 32
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_50"
      target_id "UNIPROT:P01574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 39
    target 164
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 164
    target 67
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_52"
      target_id "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 164
    target 152
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_52"
      target_id "IkB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 50
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42224"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 166
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00978"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 59
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52630"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 104
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Orf6"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 10
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6UXN2"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 130
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P29597;UNIPROT:P23458"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 13
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp5"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 165
    target 102
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_101"
      target_id "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 20
    target 167
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K5"
      target_id "M16_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 167
    target 20
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_78"
      target_id "UNIPROT:Q9Y6K5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 100
    target 168
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 74
    target 168
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
      target_id "M16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 168
    target 100
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_99"
      target_id "UNIPROT:Q92985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 118
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05412;UNIPROT:P01100"
      target_id "M16_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 169
    target 124
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_81"
      target_id "UNIPROT:P01100;UNIPROT:P05412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 6
    target 170
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15455"
      target_id "M16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "RIP1"
      target_id "M16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 170
    target 6
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_44"
      target_id "UNIPROT:O15455"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 98
    target 172
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43318"
      target_id "M16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 110
    target 172
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 172
    target 98
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_80"
      target_id "UNIPROT:O43318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 173
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_217"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 19
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 22
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "EIF2AK"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 21
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q14653"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 19
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 22
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "EIF2AK"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 21
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 174
    target 22
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_70"
      target_id "EIF2AK"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "NEMO"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 177
    target 176
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "IKKa"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 178
    target 176
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "IKKb"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 112
    target 176
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 176
    target 44
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_48"
      target_id "IKKa_underscore_IKKb_underscore_NEMO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_207"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 124
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P01100;UNIPROT:P05412"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 32
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P01574"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 39
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 21
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q14653"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 100
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q92985"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 124
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01100;UNIPROT:P05412"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 32
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01574"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 39
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 21
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 100
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q92985"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 87
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "Azithromycin"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 180
    target 32
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_34"
      target_id "UNIPROT:P01574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 35
    target 181
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 147
    target 181
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYX4;UNIPROT:O95786"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 1
    target 181
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTD2"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 114
    target 181
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q96J02"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 181
    target 35
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_85"
      target_id "UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 22
    target 182
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "EIF2AK"
      target_id "M16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 46
    target 182
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 182
    target 22
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_89"
      target_id "EIF2AK"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
