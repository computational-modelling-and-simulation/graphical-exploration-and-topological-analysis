# generated with VANTED V2.8.2 at Fri Mar 04 10:03:43 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1112.0247807824974
      y 668.8398219623248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 11
      diagram "WP4799; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P30556; urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "NA; HGNC_SYMBOL:AGTR1"
      map_id "UNIPROT:P30556"
      name "AT1R; AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab2a6; sa102; sa167; sa139; sa140; sa26; sa204; sa500; sa484; sa519; sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1735.0063162118493
      y 857.1395041428984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:obo.chebi:CHEBI%3A48432; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_II"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb; sa23; sa95; sa194"
      uniprot "NA"
    ]
    graphics [
      x 1632.4179644463616
      y 859.2804812914607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_I"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a; sa21; sa195"
      uniprot "NA"
    ]
    graphics [
      x 1854.9774975294458
      y 923.100626433524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 7
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P12821; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "NA; HGNC_SYMBOL:ACE"
      map_id "UNIPROT:P12821"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e0c12; sa29; sa146; sa28; sa100; sa199; sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1613.8251469014963
      y 1182.8695147551973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12821"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hmdb:HMDB0004246; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A3165"
      hgnc "NA"
      map_id "Bradykinin"
      name "Bradykinin"
      node_subtype "SIMPLE_MOLECULE; PROTEIN"
      node_type "species"
      org_id "bc2f2; sa402"
      uniprot "NA"
    ]
    graphics [
      x 1291.4120856312009
      y 836.2954837566494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bradykinin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4927; C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "NA; HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P00747"
      name "Plasmin; PLG; Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1440; sa1433; sa1431; sa211; sa212; sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 851.9173438712797
      y 983.867630703363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4927; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734; urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147; urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "NA; HGNC_SYMBOL:F2"
      map_id "UNIPROT:P00734"
      name "Thrombin; Prothrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "af109; sa181; sa498; sa203; sa182"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 423.3182876691751
      y 1362.7153955378835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38; sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 686.8513128223934
      y 891.6651117300166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P04201"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa77; sa141; sa496; sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1346.6804655801152
      y 175.39880153358058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "UNIPROT:P01019"
      name "AGT"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "sa34; sa174; sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 1630.9829543483897
      y 775.1560466160374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01019"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "aldosterone"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195; sa509; sa503"
      uniprot "NA"
    ]
    graphics [
      x 1802.0653931441311
      y 1182.6992201281146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aldosterone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070527; urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_aggregation"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa209; sa409"
      uniprot "NA"
    ]
    graphics [
      x 1427.673989635507
      y 1795.5551984422116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_aggregation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "UNIPROT:P00797"
      name "REN; Prorenin"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "sa36; sa35; sa71; sa415; sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 1628.1371310119794
      y 687.7236999466891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00797"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:C000657245; urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_infection"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa165; sa207; sa499; sa480; sa481"
      uniprot "NA"
    ]
    graphics [
      x 931.2705233918595
      y 1246.4689090451345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:E protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "K_plus_"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa65; sa66; sa521"
      uniprot "NA"
    ]
    graphics [
      x 1592.8265548151073
      y 1444.8664013055413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "K_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810; urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:hgnc.symbol:TNF;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01375;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "UNIPROT:P01375"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3; sa238"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 1196.379903229154
      y 1477.373611486107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930; urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "UNIPROT:P00750"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1432; sa213; sa225"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1128.4606666973843
      y 756.2552892863996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:Coagulation pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538; urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553; urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "UNIPROT:P01584"
      name "IL1b; proIL_minus_1B; IL_minus_1B; IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127; sa92; sa15; sa17; sa172; sa21; sa244; sa462; sa464; sa460; sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1374.8596254558604
      y 1423.474469213126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf3a protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2; urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "UNIPROT:P08697"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86; sa87; sa412"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 693.2016898728345
      y 808.6296906473631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08697"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718; urn:miriam:hgnc.symbol:C3;urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718; urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:mesh:D015926;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "UNIPROT:P01024"
      name "C3b; C3; C3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa310; sa252; sa308"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 135.44817203309685
      y 1566.06269717152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01024"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_61"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 219.12668674220254
      y 1679.5941665444507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D003179;urn:miriam:brenda:3.4.21.47;urn:miriam:taxonomy:9606;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:mesh:D051561;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629"
      hgnc "HGNC_SYMBOL:C3;HGNC_SYMBOL:CFB"
      map_id "UNIPROT:P01024;UNIPROT:P00751"
      name "C3b:Bb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P01024;UNIPROT:P00751"
    ]
    graphics [
      x 258.772092970528
      y 1567.6030047411116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01024;UNIPROT:P00751"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:34.4.21.47;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:mesh:D051566;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:CFB;HGNC_SYMBOL:C3"
      map_id "UNIPROT:P00751;UNIPROT:P01024"
      name "C3b:Bb:C3b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      x 392.6094135078697
      y 1598.7357596118482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00751;UNIPROT:P01024"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_271"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa532"
      uniprot "NA"
    ]
    graphics [
      x 1339.5164665719594
      y 1790.3016179068409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re398"
      uniprot "NA"
    ]
    graphics [
      x 1529.7132965179244
      y 1734.3797606064686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D007008;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Hypokalemia"
      name "Hypokalemia"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa531"
      uniprot "NA"
    ]
    graphics [
      x 1565.2994133461007
      y 1550.4017629648183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Hypokalemia"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D050776;urn:miriam:mesh:C050974;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C5"
      map_id "UNIPROT:P13671;UNIPROT:P01031"
      name "C5b:C6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      x 1420.2558161613456
      y 1650.090464495855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671;UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_53"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re264"
      uniprot "NA"
    ]
    graphics [
      x 1365.6575391662564
      y 1590.9527249042112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C7"
      map_id "UNIPROT:P10643"
      name "C7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P10643"
    ]
    graphics [
      x 1492.4740742392241
      y 1561.1448063334255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28630159;urn:miriam:mesh:C037453;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C6"
      map_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
      name "C5b:C6:C7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      x 1211.9634396539118
      y 1539.2591853915908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_236"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa462"
      uniprot "NA"
    ]
    graphics [
      x 1213.397663584692
      y 1254.7638853295452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_78"
      name "PMICID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re324"
      uniprot "NA"
    ]
    graphics [
      x 1102.4431146582785
      y 1318.7607953463473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8B"
      map_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      x 1080.1169527658694
      y 1405.6432668290888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:3850647;PUBMED:89876;PUBMED:6539333;PUBMED:2966802"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_35"
      name "PMID: 89876, PMID:385647"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re178"
      uniprot "NA"
    ]
    graphics [
      x 953.9323084847551
      y 872.0023006083425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "UNIPROT:P00749"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236; sa356"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 1037.4647370026048
      y 719.8603320896402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00749"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:mesh:D015945;urn:miriam:hgnc.symbol:F11;urn:miriam:brenda:3.4.21.27;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128; urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "UNIPROT:P03951"
      name "F11a; F11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa173; sa172"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 737.1494277722757
      y 1106.2164271203726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P03951"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1; urn:miriam:brenda:3.4.21.34;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:mesh:D020842;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "UNIPROT:P03952"
      name "KLKB1; Kallikrein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167; sa251"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 1203.5362634192497
      y 866.2844322337796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P03952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_43"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re227"
      uniprot "NA"
    ]
    graphics [
      x 1512.3638322804056
      y 1287.9172222304198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "Thrombosis"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271; sa529"
      uniprot "NA"
    ]
    graphics [
      x 1482.1992631048179
      y 1073.495844205424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Thrombosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:692685"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_83"
      name "PMID:692685"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re330"
      uniprot "NA"
    ]
    graphics [
      x 1449.3265500897905
      y 768.7961285096313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:hgnc:12726;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:GP6;HGNC_SYMBOL:VWF;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      name "GP6:alpha2beta1:VWF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      x 707.9393708392174
      y 453.5725930796882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:19286885"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_98"
      name "PMID:19286885"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re352"
      uniprot "NA"
    ]
    graphics [
      x 789.0099729459413
      y 396.9019189777355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:19286885;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_223"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa440"
      uniprot "NA"
    ]
    graphics [
      x 908.2929762580932
      y 417.7932627037186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042;PUBMED:190881"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_25"
      name "PMID:19065996"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1767.995489563136
      y 1037.551943458368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_45"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re231"
      uniprot "NA"
    ]
    graphics [
      x 1393.721670825655
      y 1312.7856945333306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:8388351;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_47"
      name "PMID:8388351"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 207.95922480074478
      y 1223.6599693195246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD"
      hgnc "HGNC_SYMBOL:THBD"
      map_id "UNIPROT:P07204"
      name "Thrombomodulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa301"
      uniprot "UNIPROT:P07204"
    ]
    graphics [
      x 328.7133638629757
      y 1085.928257344355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:11784;urn:miriam:taxonomy:9986;urn:miriam:pubmed:6282863;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:THBD;HGNC_SYMBOL:F2"
      map_id "UNIPROT:P07204;UNIPROT:P00734"
      name "Thrombin:Thrombomodulin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      x 284.2538008249787
      y 1143.7970000957632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07204;UNIPROT:P00734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_108"
      name "PMID:18026570"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re364"
      uniprot "NA"
    ]
    graphics [
      x 1429.7435262321105
      y 369.9961016013159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "angiotensin_space_I_minus_7"
      name "angiotensin_space_I_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa400"
      uniprot "NA"
    ]
    graphics [
      x 1493.6014704019155
      y 609.4423293394962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "UNIPROT:P05121"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224; sa505"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 1376.8187482981402
      y 834.9486477014633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:22449964"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_39"
      name "PMID:22449964"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 1155.799127993238
      y 948.664969675274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:9051;urn:miriam:pubmed:22449964;urn:miriam:hgnc:8593;urn:miriam:intact:EBI-7800882;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583;urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:SERPINE1;HGNC_SYMBOL:PLAT"
      map_id "UNIPROT:P05121;UNIPROT:P00750"
      name "PLAT:SERPINE1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      x 1013.8360063841897
      y 941.2726485765082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05121;UNIPROT:P00750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_248"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 1084.464111573777
      y 526.2473977466826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:9066005"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_113"
      name "PMID:9066005"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re372"
      uniprot "NA"
    ]
    graphics [
      x 1204.856221686575
      y 583.963569605608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_82"
      name "PMID:21304106"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re329"
      uniprot "NA"
    ]
    graphics [
      x 1089.6637271436957
      y 1023.0139326235795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:3530;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:mesh:D015956;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:brenda:3.4.21.38;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:ec-code:3.4.21.38; urn:miriam:hgnc:3530;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:hgnc.symbol:F12;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "UNIPROT:P00748"
      name "F12a; F12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165; sa170"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 878.4438584718761
      y 1157.666084500917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00748"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:3569;urn:miriam:ncbigene:3569;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05231;urn:miriam:uniprot:P05231;urn:miriam:ensembl:ENSG00000136244;urn:miriam:hgnc:6018;urn:miriam:hgnc.symbol:IL6;urn:miriam:hgnc.symbol:IL6;urn:miriam:refseq:NM_000600"
      hgnc "HGNC_SYMBOL:IL6"
      map_id "UNIPROT:P05231"
      name "IL6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa242"
      uniprot "UNIPROT:P05231"
    ]
    graphics [
      x 1365.874504794593
      y 966.4965239110779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_44"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re229"
      uniprot "NA"
    ]
    graphics [
      x 1505.3490623667276
      y 954.6528091872013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010; urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "UNIPROT:P00451"
      name "F8; F8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa179; sa180"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 708.0896202699447
      y 1669.7018489036473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00451"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:15746105"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_122"
      name "NA"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re390"
      uniprot "NA"
    ]
    graphics [
      x 673.8368472975028
      y 1389.3609877174654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "UNIPROT:P04070"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205; sa482"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 789.5325175754178
      y 1025.5959297287375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04070"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "Small_space_peptide"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa183; sa171; sa178; sa397"
      uniprot "NA"
    ]
    graphics [
      x 907.9872317940205
      y 1364.3155653970703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Small_space_peptide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_207"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa399"
      uniprot "NA"
    ]
    graphics [
      x 1401.7309146457003
      y 474.73921721476916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:28116710"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_69"
      name "PMID:20689271"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re306"
      uniprot "NA"
    ]
    graphics [
      x 1286.4355990588917
      y 559.3393423260061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:7391081;PUBMED:864009"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_26"
      name "PMID:7391081"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1050.7117752770805
      y 1144.0706327612202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:14388;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGB;HGNC_SYMBOL:FGG;HGNC_SYMBOL:GP6"
      map_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
      name "Fibrinogen:GP6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      x 754.1727934310516
      y 880.5185955078775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_101"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re356"
      uniprot "NA"
    ]
    graphics [
      x 809.9309632947536
      y 570.1892713808295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_activation"
      name "platelet_space_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa430"
      uniprot "NA"
    ]
    graphics [
      x 934.0509690339618
      y 344.975076141558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_activation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727; urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727; urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:mesh:D015936;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "UNIPROT:P01031"
      name "C5; C5b; C5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314; sa315; sa253"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 1004.5191077033984
      y 1393.4883139752396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:30083158;PUBMED:12878586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_51"
      name "PMID:30083158, PMID: 12878586"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 583.3825025449753
      y 1422.5135349963398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D051574;urn:miriam:taxonomy:9606;urn:miriam:brenda:3.4.21.43;urn:miriam:mesh:D050678;urn:miriam:hgnc:1324;urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5;urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C4B;HGNC_SYMBOL:C2"
      map_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      name "C2a:C4b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      x 452.4083600762982
      y 1175.6128756731555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L5;UNIPROT:P06681"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "UNIPROT:P50454"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226; sa227"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 482.83025894863556
      y 999.4506027390489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50454"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:23809134"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_38"
      name "PMID:23809134"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 389.96772873459565
      y 1196.9062548605245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_126"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re394"
      uniprot "NA"
    ]
    graphics [
      x 1649.9406514095917
      y 1331.0067695277671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0001974"
      hgnc "NA"
      map_id "vascular_space_remodeling"
      name "vascular_space_remodeling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa518"
      uniprot "NA"
    ]
    graphics [
      x 1439.2723151190976
      y 1406.3053803737607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vascular_space_remodeling"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3662;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3661;urn:miriam:hgnc:3694;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGG;HGNC_SYMBOL:FGB"
      map_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      name "Fibrinogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 586.3527147601096
      y 1332.612123079253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:28228446;PUBMED:6282863;PUBMED:2117226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_46"
      name "PMID:28228446, PMID: 6282863, PMID:2117226 "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re234"
      uniprot "NA"
    ]
    graphics [
      x 481.57970999186034
      y 1265.1667271157917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C011468;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Fibrin_space_monomer"
      name "Fibrin_space_monomer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 273.6961145768744
      y 1240.5183680722505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrin_space_monomer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:pubmed:28630159;urn:miriam:hgnc:1354;urn:miriam:mesh:C042295;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C8B;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C6;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7"
      map_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
      name "C5b:C6:C7:C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      x 936.1091646275237
      y 1540.8407244640398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:6796960"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_55"
      name "PMID:6796960"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 946.7461111310749
      y 1470.752949275452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748"
      hgnc "HGNC_SYMBOL:C9"
      map_id "UNIPROT:P02748"
      name "C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:P02748"
    ]
    graphics [
      x 1019.4224361563107
      y 1337.9668639428048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02748"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_235"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 1159.6407753180113
      y 1184.0606913945956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_86"
      name "PMCID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re334"
      uniprot "NA"
    ]
    graphics [
      x 1064.1172189107251
      y 1252.4344112588012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "C5b_minus_9_space_deposition"
      name "C5b_minus_9_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa390"
      uniprot "NA"
    ]
    graphics [
      x 1501.4049997155923
      y 1835.5467618758803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C5b_minus_9_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_104"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re359"
      uniprot "NA"
    ]
    graphics [
      x 1536.0911379130016
      y 1697.5778089003634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4b;urn:miriam:mesh:C032261;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5; urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4b;HGNC_SYMBOL:C4B; HGNC_SYMBOL:C4B"
      map_id "UNIPROT:P0C0L5"
      name "C4d; C4b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa387; sa365"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 900.8949577178303
      y 545.0581457778362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_91"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re340"
      uniprot "NA"
    ]
    graphics [
      x 1198.0329707351889
      y 796.2990264675527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:8202152"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_119"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re379"
      uniprot "NA"
    ]
    graphics [
      x 1943.0172458209454
      y 1213.6508367596257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_249"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa495"
      uniprot "NA"
    ]
    graphics [
      x 821.1070986860128
      y 1191.8966383117788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:9012652"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_92"
      name "PMID:9012652"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re341"
      uniprot "NA"
    ]
    graphics [
      x 943.2722240812661
      y 1195.9816860999886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_228"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa454"
      uniprot "NA"
    ]
    graphics [
      x 974.5227664869265
      y 1024.4634278233393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_85"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re333"
      uniprot "NA"
    ]
    graphics [
      x 1103.17999748365
      y 1107.1492533544078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2504360"
      hgnc "NA"
      map_id "cytokine_space_storm"
      name "cytokine_space_storm"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa416"
      uniprot "NA"
    ]
    graphics [
      x 1007.5936096732397
      y 1283.0088527205849
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cytokine_space_storm"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:1401;urn:miriam:ncbigene:1401;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2367;urn:miriam:hgnc.symbol:CRP;urn:miriam:uniprot:P02741;urn:miriam:uniprot:P02741;urn:miriam:hgnc.symbol:CRP;urn:miriam:ensembl:ENSG00000132693;urn:miriam:refseq:NM_000567"
      hgnc "HGNC_SYMBOL:CRP"
      map_id "UNIPROT:P02741"
      name "CRP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa410"
      uniprot "UNIPROT:P02741"
    ]
    graphics [
      x 1314.0216574743276
      y 1391.4071442397476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02741"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:9490235"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_75"
      name "PMID:9490235"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re320"
      uniprot "NA"
    ]
    graphics [
      x 1449.3864363890116
      y 1248.1513815331125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_266"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 1889.0105376943734
      y 1287.4078089136917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:5932931"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re391"
      uniprot "NA"
    ]
    graphics [
      x 1781.1265351150578
      y 1284.5396226629612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:7944388"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_28"
      name "PMID:7944388"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1286.2503370667794
      y 650.8126559358284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889; urn:miriam:mesh:D019679;urn:miriam:hgnc.symbol:KNG1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383; HGNC_SYMBOL:KNG1"
      map_id "UNIPROT:P01042"
      name "KNG1; Kininogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169; sa250"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 1193.8266981070847
      y 627.6988338202125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01042"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:17598838;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-10087151;urn:miriam:hgnc:6371;urn:miriam:hgnc:6383;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383;HGNC_SYMBOL:KLKB1"
      map_id "UNIPROT:P01042;UNIPROT:P03952"
      name "KNG1:KLKB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      x 1344.9776030999076
      y 526.6493903157207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01042;UNIPROT:P03952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_127"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re395"
      uniprot "NA"
    ]
    graphics [
      x 1888.848144019748
      y 1363.5276514198354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "vascular_space_inflammation"
      name "vascular_space_inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa530"
      uniprot "NA"
    ]
    graphics [
      x 1912.551637917848
      y 1488.3468401994696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vascular_space_inflammation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "C4d_space_deposition"
      name "C4d_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa388"
      uniprot "NA"
    ]
    graphics [
      x 1274.9898233119438
      y 191.8085015350955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C4d_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_106"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re361"
      uniprot "NA"
    ]
    graphics [
      x 1426.1892593714647
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D007681"
      hgnc "NA"
      map_id "septal_space_capillary_space_necrosis"
      name "septal_space_capillary_space_necrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa389"
      uniprot "NA"
    ]
    graphics [
      x 1478.6572943001715
      y 170.1481196022204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "septal_space_capillary_space_necrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_64"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re292"
      uniprot "NA"
    ]
    graphics [
      x 1293.3751740180037
      y 1654.6876359795097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:27077125;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_237"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa467"
      uniprot "NA"
    ]
    graphics [
      x 1032.021529630672
      y 900.0851807446551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:27077125"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_94"
      name "PMID:27077125"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re343"
      uniprot "NA"
    ]
    graphics [
      x 980.2302642779688
      y 1089.7271822945897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_77"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re323"
      uniprot "NA"
    ]
    graphics [
      x 898.464569049404
      y 1087.7566696767553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:284414"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_54"
      name "PMID:284414"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re266"
      uniprot "NA"
    ]
    graphics [
      x 974.1466517555909
      y 1423.1710376258238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:mesh:D003185;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C8B"
      map_id "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
      name "C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    ]
    graphics [
      x 868.5064670593574
      y 1335.2283429332495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:GP6;HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
      name "GP6:alpha2:beta1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 624.698294539149
      y 669.8738733676965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_103"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re358"
      uniprot "NA"
    ]
    graphics [
      x 841.3814683813563
      y 628.2694384728992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "UNIPROT:P04275"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa411; sa472"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 1057.292545280783
      y 813.4511267209797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_71"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re308"
      uniprot "NA"
    ]
    graphics [
      x 720.4117473629246
      y 661.6192173632036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_229"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa455"
      uniprot "NA"
    ]
    graphics [
      x 890.6831117229538
      y 1557.0640938435324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:32504360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_95"
      name "PMID:32504360"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re344"
      uniprot "NA"
    ]
    graphics [
      x 1023.4449964545264
      y 1488.2465388365922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:4627469;PUBMED:6768384"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_42"
      name "PMID:6768384, PMID:4627469"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 1338.4143833935664
      y 685.83524828965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159; urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "UNIPROT:P00742"
      name "F10a; F10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa176; sa177"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 466.24756289824995
      y 1441.7950973508266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00742"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:2303476"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_32"
      name "PMID:2303476"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 318.51171548695163
      y 1352.0072423060196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F5"
      map_id "UNIPROT:P12259"
      name "F5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201"
      uniprot "UNIPROT:P12259"
    ]
    graphics [
      x 344.4939862030533
      y 1137.5051007118586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D015951;urn:miriam:pubmed:2303476;urn:miriam:mesh:D15943;urn:miriam:mesh:C022475;urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159;urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F10;HGNC_SYMBOL:F5"
      map_id "UNIPROT:P00742;UNIPROT:P12259"
      name "F5a:F10a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      x 187.5549967268605
      y 1313.63819409883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00742;UNIPROT:P12259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106;PUBMED:8631976"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_27"
      name "PMID:21304106, PMID:8631976"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 621.8939932814807
      y 1220.1079896117076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:8158359"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_110"
      name "PMID:8158359"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re366"
      uniprot "NA"
    ]
    graphics [
      x 1748.7755232571055
      y 711.5144404179105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_230"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa456"
      uniprot "NA"
    ]
    graphics [
      x 1059.2589301685916
      y 1611.810112162804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:32171076"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_73"
      name "PMID:32171076"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re315"
      uniprot "NA"
    ]
    graphics [
      x 1109.2789251533284
      y 1468.9207761000343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:GP6"
      map_id "UNIPROT:Q9HCN6"
      name "GP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa431"
      uniprot "UNIPROT:Q9HCN6"
    ]
    graphics [
      x 541.8479453396683
      y 968.2035742102642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HCN6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_102"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re357"
      uniprot "NA"
    ]
    graphics [
      x 459.1266736135549
      y 767.5687921096315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-16428357;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:P17301;UNIPROT:P05556"
      name "ITGA2:ITGAB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 333.56341562959915
      y 724.3291197576132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17301;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106;urn:miriam:doi:10.1101/2020.04.25.200"
      hgnc "NA"
      map_id "M121_240"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa478"
      uniprot "NA"
    ]
    graphics [
      x 468.9248385784457
      y 1094.467589895659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_81"
      name "DOI:10.1101/2020.04.25.20077842"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re328"
      uniprot "NA"
    ]
    graphics [
      x 570.592943368652
      y 1153.5952454687563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:20591974;PUBMED:8034668;PUBMED:11983698;PUBMED:2091055"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re377"
      uniprot "NA"
    ]
    graphics [
      x 1562.9209746799888
      y 870.1213421830075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_63"
      name "PMID: 32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 1052.3713973003391
      y 287.58326594247364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_128"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re396"
      uniprot "NA"
    ]
    graphics [
      x 1267.4400402445658
      y 1444.7589376367716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_255"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa501"
      uniprot "NA"
    ]
    graphics [
      x 1428.4955770493675
      y 972.4505025026972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_116"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re375"
      uniprot "NA"
    ]
    graphics [
      x 1307.6641580324367
      y 1035.3865412721652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:579490"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_34"
      name "PMID:579490"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 374.2754851798255
      y 1530.4221810676308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:SERPINC1"
      map_id "UNIPROT:P01008"
      name "Antithrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202"
      uniprot "UNIPROT:P01008"
    ]
    graphics [
      x 524.8153223992989
      y 1509.5588590224902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01008"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28304;urn:miriam:taxonomy:9606;urn:miriam:pubmed:708377"
      hgnc "NA"
      map_id "Heparin"
      name "Heparin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 411.5361097467271
      y 1676.7005668345646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Heparin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:RPS3AP29;urn:miriam:refseq:NG_011230;urn:miriam:ensembl:ENSG00000237818;urn:miriam:hgnc:35531;urn:miriam:ncbigene:730861"
      hgnc "HGNC_SYMBOL:RPS3AP29"
      map_id "F9"
      name "F9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa174"
      uniprot "NA"
    ]
    graphics [
      x 684.4605281440333
      y 1208.3449837801256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:9100000"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_29"
      name "PMID:9100000"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re169"
      uniprot "NA"
    ]
    graphics [
      x 681.5693295241305
      y 1336.0540086307255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F9"
      map_id "UNIPROT:P00740"
      name "F9a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P00740"
    ]
    graphics [
      x 670.61019347305
      y 1620.9704429509961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:15853774;PUBMED:11551226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_24"
      name "PMID:11551226"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 606.6677621683177
      y 1550.4798391262543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_40"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1819.4773959020567
      y 840.4142266893456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:11983698"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re386"
      uniprot "NA"
    ]
    graphics [
      x 1830.3033338292248
      y 768.8931758491833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000584;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000169429;urn:miriam:hgnc:6025;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:uniprot:P10145;urn:miriam:uniprot:P10145;urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3576"
      hgnc "HGNC_SYMBOL:CXCL8"
      map_id "UNIPROT:P10145"
      name "IL8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P10145"
    ]
    graphics [
      x 1362.7168103362515
      y 1206.22572890861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_48"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 1509.951404147741
      y 1169.8985840361815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:4430674;PUBMED:3818642"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_33"
      name "PMID:4430674,PMID:3818642"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 310.6497107740556
      y 1294.731925751901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:7577232"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_96"
      name "PMID:29096812, PMID:7577232"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re345"
      uniprot "NA"
    ]
    graphics [
      x 195.56495974073084
      y 1094.8938705529956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D026122;urn:miriam:taxonomy:9606;urn:miriam:brenda:2.3.2.13;urn:miriam:hgnc.symbol:F13"
      hgnc "HGNC_SYMBOL:F13"
      map_id "F13a"
      name "F13a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa425"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1084.3811616167154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F13a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C465961"
      hgnc "NA"
      map_id "Fibrin_space_polymer"
      name "Fibrin_space_polymer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa424"
      uniprot "NA"
    ]
    graphics [
      x 403.117372179022
      y 1033.9700436611033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrin_space_polymer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_257"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa504"
      uniprot "NA"
    ]
    graphics [
      x 1704.544682060195
      y 1114.9529607089396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:173529;PUBMED:8404594;PUBMED:32565254"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re376"
      uniprot "NA"
    ]
    graphics [
      x 1572.8275798899315
      y 1117.081568596854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A3892;urn:miriam:hgnc:9201"
      hgnc "NA"
      map_id "ACTH"
      name "ACTH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 1690.3979936361043
      y 1194.0891840349857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACTH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:taxonomy:9606;urn:miriam:hgnc:13557;urn:miriam:taxonomy:2697049;urn:miriam:pdb:6CS2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 568.7377267285538
      y 564.6318564433541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_114"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re373"
      uniprot "NA"
    ]
    graphics [
      x 392.99918600690853
      y 490.3806416308136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_viral_space_entry"
      name "SARS_minus_CoV_minus_2_space_viral_space_entry"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa485"
      uniprot "NA"
    ]
    graphics [
      x 262.716255277199
      y 481.1064905371969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_viral_space_entry"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:10807586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_105"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re360"
      uniprot "NA"
    ]
    graphics [
      x 1655.5450665382687
      y 1892.6145889114086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006915"
      hgnc "NA"
      map_id "apoptosis"
      name "apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa408"
      uniprot "NA"
    ]
    graphics [
      x 1779.895948518913
      y 1849.6506680616392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_93"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re342"
      uniprot "NA"
    ]
    graphics [
      x 1325.9064297862546
      y 1277.5016579015778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_226"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa452"
      uniprot "NA"
    ]
    graphics [
      x 1238.7802212211259
      y 1350.736490667924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_87"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re336"
      uniprot "NA"
    ]
    graphics [
      x 1151.964046683529
      y 1267.2195733578174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3541;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F3;urn:miriam:refseq:NM_001993;urn:miriam:hgnc.symbol:F3;urn:miriam:ncbigene:2152;urn:miriam:ncbigene:2152;urn:miriam:ensembl:ENSG00000117525;urn:miriam:uniprot:P13726;urn:miriam:uniprot:P13726"
      hgnc "HGNC_SYMBOL:F3"
      map_id "UNIPROT:P13726"
      name "F5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa200"
      uniprot "UNIPROT:P13726"
    ]
    graphics [
      x 307.352639318275
      y 875.9598923898407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13726"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:2322551;PUBMED:6572921;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_31"
      name "PMID:2322551, PMID:6282863, PMID:6572921"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re171"
      uniprot "NA"
    ]
    graphics [
      x 407.8407413358069
      y 968.9385962811967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:32252108"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_130"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re399"
      uniprot "NA"
    ]
    graphics [
      x 1736.0363164627672
      y 1409.1431492472443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01589;urn:miriam:uniprot:P01589;urn:miriam:ncbigene:3559;urn:miriam:ncbigene:3559;urn:miriam:hgnc:6008;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:refseq:NM_000417;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:ensembl:ENSG00000134460"
      hgnc "HGNC_SYMBOL:IL2RA"
      map_id "UNIPROT:P01589"
      name "IL2RA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:P01589"
    ]
    graphics [
      x 1196.6267569126492
      y 1094.2541413721463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01589"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:20483636"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_49"
      name "PMID:20483636"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 1368.421428774989
      y 1093.6952472294474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:22471307"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_30"
      name "PMID:22471307"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re170"
      uniprot "NA"
    ]
    graphics [
      x 778.9558288062576
      y 1764.4715297823066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:22471307;urn:miriam:intact:EBI-11621595;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3546;urn:miriam:hgnc:35531;urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010;urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F8;HGNC_SYMBOL:F9"
      map_id "UNIPROT:P00451;UNIPROT:P00740"
      name "F8:F9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      x 942.4326428400916
      y 1718.4818926473022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00451;UNIPROT:P00740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_60"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re278"
      uniprot "NA"
    ]
    graphics [
      x 574.0177062354508
      y 744.1298723281027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717; urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:hgnc.symbol:C2;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717; urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050679;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "UNIPROT:P06681"
      name "C2a; C2; C2b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa367; sa366; sa368"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 500.79506876763674
      y 511.8355836473954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06681"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "UNIPROT:P48740"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa458; sa358"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 643.5546205634852
      y 221.78557145134403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_58"
      name "PMID:11290788"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 833.0621343545664
      y 261.4326128360284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "UNIPROT:O00187"
      name "MBL2; MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362; sa459; sa357"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 992.9171817643686
      y 445.08018411795024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:17395591;PUBMED:427127"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_50"
      name "PMID:427127, PMID:17395591"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 237.44566985264282
      y 1424.0586434155455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "PUBMED:3124286;PUBMED:3096399;PUBMED:12091055;PUBMED:10373228"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_37"
      name "PMID:10373228, PMID:3124286"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 1300.1936896307734
      y 768.0849434867605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570;PUBMED:21789389"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re365"
      uniprot "NA"
    ]
    graphics [
      x 1439.6316791682925
      y 136.32932680417787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "thrombus_space_formation"
      name "thrombus_space_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa441"
      uniprot "NA"
    ]
    graphics [
      x 1484.1018978486286
      y 302.2998832678934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "thrombus_space_formation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_231"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa457"
      uniprot "NA"
    ]
    graphics [
      x 847.699528137681
      y 1716.1760800192737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_65"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re295"
      uniprot "NA"
    ]
    graphics [
      x 822.8194991211697
      y 1567.9203583699732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_251"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa497"
      uniprot "NA"
    ]
    graphics [
      x 675.8343096524773
      y 1531.4834148450782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_41"
      name "PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 743.3830059838835
      y 1420.2632714919596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_68"
      name "PMID:32275855"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re305"
      uniprot "NA"
    ]
    graphics [
      x 780.5644483541012
      y 680.4480725289429
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_234"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa460"
      uniprot "NA"
    ]
    graphics [
      x 839.5456767286902
      y 713.183002499075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_79"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re325"
      uniprot "NA"
    ]
    graphics [
      x 869.5599601785618
      y 846.311203564756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:10574983;PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_97"
      name "PMID:29096812, PMID:10574983, PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re346"
      uniprot "NA"
    ]
    graphics [
      x 648.1954901911583
      y 1016.6393600066665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C036309;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "D_minus_dimer"
      name "D_minus_dimer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "NA"
    ]
    graphics [
      x 737.703295495069
      y 845.4591266885895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D_minus_dimer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_115"
      name "PMID:10749699"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re374"
      uniprot "NA"
    ]
    graphics [
      x 1284.1991794878543
      y 1134.818901688521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C079000;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Bradykinin(1_minus_5)"
      name "Bradykinin(1_minus_5)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 1285.2690841333838
      y 1236.3577550541095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bradykinin(1_minus_5)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_99"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re353"
      uniprot "NA"
    ]
    graphics [
      x 750.1486908585714
      y 312.55054180107413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:26709040"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_125"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re393"
      uniprot "NA"
    ]
    graphics [
      x 1653.4627848765922
      y 968.5852870467204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461;PUBMED:6172448;PUBMED:30934934"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_23"
      name "PMID:10585461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 1765.4578721318505
      y 768.922321509442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_70"
      name "PMID:23392115, PMID:32336612"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re307"
      uniprot "NA"
    ]
    graphics [
      x 1428.982552563986
      y 690.8467278928933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_62"
      name "PMID:19362461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 838.4396058801564
      y 343.9641298667025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.46;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2771;urn:miriam:hgnc.symbol:CFD;urn:miriam:hgnc.symbol:CFD;urn:miriam:refseq:NM_001928;urn:miriam:ensembl:ENSG00000197766;urn:miriam:uniprot:P00746;urn:miriam:uniprot:P00746;urn:miriam:ncbigene:1675;urn:miriam:ncbigene:1675"
      hgnc "HGNC_SYMBOL:CFD"
      map_id "UNIPROT:P00746"
      name "CFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P00746"
    ]
    graphics [
      x 789.6133491929183
      y 197.16078829053765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00746"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:21199867"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_56"
      name "PMID:21199867"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 1212.0251097199439
      y 727.2452897415434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_227"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa453"
      uniprot "NA"
    ]
    graphics [
      x 1137.7194093757428
      y 1549.3480915540877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "PUBMED:32359396"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_84"
      name "PMID:32359396"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re332"
      uniprot "NA"
    ]
    graphics [
      x 1155.8181334923995
      y 1393.6981999581153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:16391415"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_111"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re369"
      uniprot "NA"
    ]
    graphics [
      x 1649.3997822909382
      y 544.7406044857652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_72"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re309"
      uniprot "NA"
    ]
    graphics [
      x 758.8739667756295
      y 1213.587807900854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:C8B;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C6;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      x 579.5715892048523
      y 1279.1725645327583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "UNIPROT:P08235"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa506; sa516"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 1934.7356838022408
      y 949.7575629484816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:21349712;PUBMED:7045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re382"
      uniprot "NA"
    ]
    graphics [
      x 1886.1075034094906
      y 1032.3572905013866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "PUBMED:20975035"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_112"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re371"
      uniprot "NA"
    ]
    graphics [
      x 1593.2121030816336
      y 990.5955212302727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:16008552"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_67"
      name "PMID:32336612, PMID:16008552"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re304"
      uniprot "NA"
    ]
    graphics [
      x 1073.1792126805153
      y 947.2863043754423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_52"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 1219.8405182680226
      y 1621.6997593515875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C6"
      map_id "UNIPROT:P13671"
      name "C6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318"
      uniprot "UNIPROT:P13671"
    ]
    graphics [
      x 1300.649783722371
      y 1725.501955871648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_74"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re319"
      uniprot "NA"
    ]
    graphics [
      x 1024.599694279526
      y 1023.4915776418499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:8136018"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_36"
      name "PMID: 8136018"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re179"
      uniprot "NA"
    ]
    graphics [
      x 652.8533551826421
      y 1452.9052826566174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:mesh:C046193;urn:miriam:pubmed:22930518;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147;urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:F2;HGNC_SYMBOL:SERPINC1"
      map_id "UNIPROT:P00734;UNIPROT:P01008"
      name "TAT_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      x 729.9126352233952
      y 1565.8210303115434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00734;UNIPROT:P01008"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_90"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re339"
      uniprot "NA"
    ]
    graphics [
      x 1151.12803469486
      y 371.01555435977355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "MASP2_space_deposition"
      name "MASP2_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa417"
      uniprot "NA"
    ]
    graphics [
      x 1264.754493312805
      y 315.9248896285425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MASP2_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "PUBMED:10946292"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_89"
      name "PMID:10946292"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re338"
      uniprot "NA"
    ]
    graphics [
      x 517.4666425914506
      y 326.0234879555301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_225"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa451"
      uniprot "NA"
    ]
    graphics [
      x 846.6623898175928
      y 1095.7326493178916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_88"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re337"
      uniprot "NA"
    ]
    graphics [
      x 988.6053886356881
      y 1143.0313943292467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776;PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_57"
      name "PMID:11290788, PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 947.880959288293
      y 801.4409812864872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961;PUBMED:19465929"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_100"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re355"
      uniprot "NA"
    ]
    graphics [
      x 1207.8831302285764
      y 267.64760536664505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_107"
      name "PMID:29472360"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re363"
      uniprot "NA"
    ]
    graphics [
      x 628.2607601831123
      y 1103.2489396406118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32302438;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_241"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa479"
      uniprot "NA"
    ]
    graphics [
      x 800.1127988577747
      y 1401.2302885066065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_66"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re296"
      uniprot "NA"
    ]
    graphics [
      x 742.0243790710953
      y 1309.3203064571476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:2437112"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_80"
      name "PMID:2437112, DOI:10.1101/2020.04.25.20077842"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re326"
      uniprot "NA"
    ]
    graphics [
      x 735.4963216509611
      y 979.2190125609386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2437112;urn:miriam:hgnc:9075;urn:miriam:mesh:D005341;urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:SERPINF2;HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P08697;UNIPROT:P00747"
      name "SERPINF2:Plasmin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      x 628.0784315576057
      y 856.5031290367609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08697;UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:3165516"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_124"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re392"
      uniprot "NA"
    ]
    graphics [
      x 1733.1357529474367
      y 988.7673554013984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D00318"
      hgnc "NA"
      map_id "C4"
      name "C4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 1103.9975039060735
      y 271.63882501233354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:21664989"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_59"
      name "PMID:22949645"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 960.2380288572258
      y 280.9221947368882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc:1323;urn:miriam:ensembl:ENSG00000244731;urn:miriam:uniprot:P0C0L4;urn:miriam:uniprot:P0C0L4;urn:miriam:refseq:NM_007293"
      hgnc "HGNC_SYMBOL:C4A"
      map_id "UNIPROT:P0C0L4"
      name "C4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P0C0L4"
    ]
    graphics [
      x 1041.3511207420975
      y 343.99256142856825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:3500650"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_76"
      name "PMID:3500650"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re321"
      uniprot "NA"
    ]
    graphics [
      x 1281.332043161524
      y 937.8784094155469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 230
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024;UNIPROT:P00751"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_61"
      target_id "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_271"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "Hypokalemia"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 26
    target 16
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_129"
      target_id "K_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P01031"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10643"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 29
    target 31
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_53"
      target_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_236"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 15
    target 33
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_78"
      target_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 7
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00747"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 18
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00750"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00749"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 37
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03951"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 38
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 35
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_35"
      target_id "UNIPROT:P00747"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 19
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01584"
      target_id "M121_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_43"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 14
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 38
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 41
    target 14
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_83"
      target_id "UNIPROT:P00797"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      target_id "M121_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_98"
      target_id "M121_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 4
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 5
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 45
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_25"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 17
    target 46
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01375"
      target_id "M121_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 46
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_45"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 8
    target 47
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07204"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 47
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_47"
      target_id "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 10
    target 50
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 50
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_108"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05121"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 18
    target 53
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00750"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 15
    target 53
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_39"
      target_id "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_248"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 6
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Bradykinin"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 56
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_113"
      target_id "UNIPROT:P00750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 38
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P03952"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00748"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 57
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_82"
      target_id "UNIPROT:P03952"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05231"
      target_id "M121_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 60
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_44"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00451"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 8
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 62
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "UNIPROT:P00451"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_207"
      target_id "M121_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 66
    target 1
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_69"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 58
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00748"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 38
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 67
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "UNIPROT:P00748"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 67
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
      target_id "M121_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_101"
      target_id "platelet_space_activation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 24
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00751;UNIPROT:P01024"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50454"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 8
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 75
    target 74
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_38"
      target_id "UNIPROT:P50454"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 12
    target 76
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_126"
      target_id "vascular_space_remodeling"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 8
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 49
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P07204;UNIPROT:P00734"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 15
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_46"
      target_id "Fibrin_space_monomer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02748"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 82
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_55"
      target_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_235"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 15
    target 85
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 85
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_86"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C5b_minus_9_space_deposition"
      target_id "M121_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 87
    target 13
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_104"
      target_id "platelet_space_aggregation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 89
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_91"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 12
    target 90
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 90
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_119"
      target_id "aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_249"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 34
    target 92
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 92
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_92"
      target_id "UNIPROT:P00747"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_228"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 15
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 94
    target 59
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_85"
      target_id "UNIPROT:P05231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02741"
      target_id "M121_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 97
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_75"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_266"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 12
    target 99
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "aldosterone"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 99
    target 5
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_123"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 38
    target 100
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P03952"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01042"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 100
    target 102
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_28"
      target_id "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 12
    target 103
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_127"
      target_id "vascular_space_inflammation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C4d_space_deposition"
      target_id "M121_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_106"
      target_id "septal_space_capillary_space_necrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 34
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 108
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_64"
      target_id "C5b_minus_9_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 109
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_237"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 7
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "UNIPROT:P00747"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 110
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_94"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 63
    target 111
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04070"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 15
    target 111
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 111
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_77"
      target_id "UNIPROT:P04070"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 31
    target 112
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 112
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_54"
      target_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 115
    target 42
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_103"
      target_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 9
    target 117
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 117
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_71"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_229"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 15
    target 119
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 95
    target 119
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 119
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_95"
      target_id "UNIPROT:P01375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 102
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01042;UNIPROT:P03952"
      target_id "M121_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 102
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01042;UNIPROT:P03952"
      target_id "M121_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 120
    target 101
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "UNIPROT:P01042"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 120
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "Bradykinin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 120
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "UNIPROT:P03952"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00742"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12259"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 122
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_32"
      target_id "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 58
    target 125
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00748"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 8
    target 125
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 125
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "UNIPROT:P03951"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 125
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "UNIPROT:P03951"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 2
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 3
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 126
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_110"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_230"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 15
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 128
    target 96
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_73"
      target_id "UNIPROT:P02741"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:P05556"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 130
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_102"
      target_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_240"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 15
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 133
    target 48
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_81"
      target_id "UNIPROT:P07204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 52
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05121"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 12
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "aldosterone"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 3
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 51
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 59
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P05231"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 2
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 134
    target 52
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_118"
      target_id "UNIPROT:P05121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 88
    target 135
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 135
    target 105
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_63"
      target_id "C4d_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 15
    target 136
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 136
    target 27
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_128"
      target_id "Hypokalemia"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_255"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 15
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 138
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_116"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 8
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01008"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 141
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "Heparin"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 139
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_34"
      target_id "UNIPROT:P00734"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 142
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "F9"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 37
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03951"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 143
    target 144
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_29"
      target_id "UNIPROT:P00740"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 121
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00742"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00740"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 140
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01008"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 145
    target 121
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "UNIPROT:P00742"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 145
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 4
    target 146
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M121_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 146
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_40"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 2
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 3
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 147
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_121"
      target_id "UNIPROT:P30556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10145"
      target_id "M121_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 149
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_48"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 8
    target 150
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 124
    target 150
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00742;UNIPROT:P12259"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 150
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_33"
      target_id "UNIPROT:P00734"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 80
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Fibrin_space_monomer"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "F13a"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 151
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_96"
      target_id "Fibrin_space_polymer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 154
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_257"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 15
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 3
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 16
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "K_plus_"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 2
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 156
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "ACTH"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 155
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_117"
      target_id "aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
      target_id "M121_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_114"
      target_id "SARS_minus_CoV_minus_2_space_viral_space_entry"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 86
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C5b_minus_9_space_deposition"
      target_id "M121_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 160
    target 161
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_105"
      target_id "apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 34
    target 162
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 162
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_93"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 163
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_226"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 15
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 95
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 164
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_87"
      target_id "UNIPROT:P10145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13726"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 49
    target 166
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P07204;UNIPROT:P00734"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 63
    target 166
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 166
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_31"
      target_id "UNIPROT:P12259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 12
    target 167
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 167
    target 27
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_130"
      target_id "Hypokalemia"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 168
    target 169
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01589"
      target_id "M121_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 169
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_49"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 144
    target 170
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00740"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 61
    target 170
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00451"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_30"
      target_id "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 88
    target 172
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 173
    target 172
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06681"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 172
    target 73
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_60"
      target_id "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 174
    target 175
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P48740"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 176
    target 175
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O00187"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_58"
      target_id "UNIPROT:P48740"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 21
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 23
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01024;UNIPROT:P00751"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 73
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 177
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "UNIPROT:P01024"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 177
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "UNIPROT:P01024"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 18
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00750"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 52
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P05121"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 6
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "Bradykinin"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 3
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 51
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 63
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 178
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_37"
      target_id "UNIPROT:P00750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 10
    target 179
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M121_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_109"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 181
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_231"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 15
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 182
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_65"
      target_id "UNIPROT:P00451"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_251"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 15
    target 184
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 184
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_41"
      target_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 1
    target 185
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 9
    target 185
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 185
    target 157
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_68"
      target_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 186
    target 187
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_234"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 15
    target 187
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 187
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_79"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 153
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Fibrin_space_polymer"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 7
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 74
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P50454"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 15
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_97"
      target_id "D_minus_dimer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 6
    target 190
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Bradykinin"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 5
    target 190
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "Bradykinin(1_minus_5)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 190
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 42
    target 192
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      target_id "M121_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 192
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_99"
      target_id "platelet_space_activation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 2
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 193
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_125"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 11
    target 194
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 14
    target 194
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00797"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 194
    target 4
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_23"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 3
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 1
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 195
    target 51
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_70"
      target_id "angiotensin_space_I_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 88
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00746"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 196
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_62"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 36
    target 198
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00749"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 52
    target 198
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P05121"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 198
    target 36
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_56"
      target_id "UNIPROT:P00749"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 199
    target 200
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_227"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 15
    target 200
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 95
    target 200
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 200
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_84"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 2
    target 201
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 201
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_111"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 34
    target 202
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 9
    target 202
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 202
    target 203
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_72"
      target_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08235"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 12
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aldosterone"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 2
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 205
    target 204
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_120"
      target_id "UNIPROT:P08235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 3
    target 206
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M121_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 206
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_112"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 1
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 15
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 207
    target 1
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_67"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 71
    target 208
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 209
    target 208
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 208
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_52"
      target_id "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 116
    target 210
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 15
    target 210
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 210
    target 116
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_74"
      target_id "UNIPROT:P04275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 140
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01008"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 8
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 15
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 211
    target 212
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_36"
      target_id "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 176
    target 213
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00187"
      target_id "M121_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 213
    target 214
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_90"
      target_id "MASP2_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 173
    target 215
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06681"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 174
    target 215
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48740"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 215
    target 173
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "UNIPROT:P06681"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 215
    target 173
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "UNIPROT:P06681"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_225"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 15
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 95
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 217
    target 168
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_88"
      target_id "UNIPROT:P01589"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 176
    target 218
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00187"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 15
    target 218
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 218
    target 176
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_57"
      target_id "UNIPROT:O00187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 70
    target 219
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "platelet_space_activation"
      target_id "M121_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 219
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_100"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 78
    target 220
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 129
    target 220
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 220
    target 68
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_107"
      target_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_241"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 15
    target 222
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 222
    target 140
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_66"
      target_id "UNIPROT:P01008"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 7
    target 223
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00747"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 20
    target 223
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08697"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 15
    target 223
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_80"
      target_id "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 204
    target 225
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08235"
      target_id "M121_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 225
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_124"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 226
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C4"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 176
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O00187"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 227
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 227
    target 228
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "UNIPROT:P0C0L4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 116
    target 229
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 229
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_76"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
