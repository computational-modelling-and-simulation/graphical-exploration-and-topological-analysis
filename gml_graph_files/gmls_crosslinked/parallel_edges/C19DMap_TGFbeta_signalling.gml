# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 13
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623; urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408; urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754; urn:miriam:uniprot:P59637; urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E; urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321; urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P59637"
      name "3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; nascent_space_E; N_minus_glycan_space_E; E; Orf3a; SARS_space_E"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_362; layout_365; layout_371; layout_373; layout_233; layout_355; e5c2e; sa69; sa48; sa92; sa140; sa146; sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 475.49831558336325
      y 641.0373077436351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 573.5427946785613
      y 435.5874966491735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:reactome:R-COV-9686193; urn:miriam:uniprot:P59635; urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "UNIPROT:P59635"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_614; c0237; sa84; sa76"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 305.2247976774156
      y 158.09747794381312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "UNIPROT:P23443"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa41; sa40"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 62.5
      y 235.54805318037697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23443"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 124.00728755517423
      y 121.82122729498417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "UNIPROT:P30153;UNIPROT:P67775"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16; csa15"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 256.9492427615569
      y 73.03355789073629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30153;UNIPROT:P67775"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "UNIPROT:Q15796;UNIPROT:P84022"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5; csa4"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 825.7490751394035
      y 576.3798354532098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15796;UNIPROT:P84022"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_14"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 862.8972743375264
      y 716.1161314718001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:obo.go:GO%3A0000074"
      hgnc "NA"
      map_id "Modulation_space_of_space_cell_space_cycle"
      name "Modulation_space_of_space_cell_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 815.0505735189918
      y 838.5751120196186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Modulation_space_of_space_cell_space_cycle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "UNIPROT:P01137"
      name "TGFB1; TGFB_slash_TGFBR"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa10; csa11"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 577.9472257861185
      y 257.49410107921057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 681.3405475855814
      y 183.2147592766325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.30;urn:miriam:refseq:NM_001024847;urn:miriam:hgnc:11773;urn:miriam:ensembl:ENSG00000163513;urn:miriam:ncbigene:7048;urn:miriam:ncbigene:7048;urn:miriam:uniprot:P37173;urn:miriam:uniprot:P37173;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:ensembl:ENSG00000106799;urn:miriam:uniprot:P36897;urn:miriam:uniprot:P36897;urn:miriam:ncbigene:7046;urn:miriam:ncbigene:7046;urn:miriam:ec-code:2.7.11.30;urn:miriam:hgnc:11772;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:refseq:NM_001130916"
      hgnc "HGNC_SYMBOL:TGFBR2;HGNC_SYMBOL:TGFBR1"
      map_id "UNIPROT:P37173;UNIPROT:P36897"
      name "TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P37173;UNIPROT:P36897"
    ]
    graphics [
      x 630.9813733165065
      y 67.45364614143685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P37173;UNIPROT:P36897"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_206943;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:uniprot:Q14766;urn:miriam:uniprot:Q14766;urn:miriam:hgnc:6714;urn:miriam:ensembl:ENSG00000049323"
      hgnc "HGNC_SYMBOL:LTBP1"
      map_id "UNIPROT:Q14766"
      name "LTBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q14766"
    ]
    graphics [
      x 724.5159508327679
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14766"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13145;urn:miriam:uniprot:Q13145;urn:miriam:refseq:NM_012342;urn:miriam:ensembl:ENSG00000095739;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc:30251"
      hgnc "HGNC_SYMBOL:BAMBI"
      map_id "UNIPROT:Q13145"
      name "BAMBI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q13145"
    ]
    graphics [
      x 636.7858159936557
      y 361.1918432404227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489677;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa72"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    ]
    graphics [
      x 792.4781634421292
      y 130.40056291746222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:pubmed:17016423;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "UNIPROT:P61586"
      name "RHOA; RGcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1; sa38; sa1305; csa101"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 360.7339476024447
      y 394.38539990591585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61586"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 482.8249651512096
      y 394.47791052139416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "UNIPROT:Q7TFA1"
      name "Nsp7; Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76; sa70; sa64"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 612.2170770523849
      y 488.87570843677497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7TFA1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92; sa55"
      uniprot "NA"
    ]
    graphics [
      x 1279.5176130375391
      y 530.2750921736351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_18"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1381.5391659552188
      y 467.8291153631966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOB;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:CUL2"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 1270.8877780972266
      y 421.70466888188923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    ]
    graphics [
      x 1354.832199917314
      y 353.6497399035917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 510.6341611425436
      y 312.9841088116709
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_35"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 481.7980496423462
      y 194.5189479687294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD5;HGNC_SYMBOL:SMAD9"
      map_id "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    ]
    graphics [
      x 474.47741140698247
      y 556.9670012430614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 593.0928936786461
      y 589.2541401127054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:169;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:uniprot:P61160;urn:miriam:uniprot:P61160;urn:miriam:ncbigene:10097;urn:miriam:ncbigene:10097;urn:miriam:ensembl:ENSG00000138071;urn:miriam:refseq:NM_001005386;urn:miriam:ncbigene:659;urn:miriam:ncbigene:659;urn:miriam:ensembl:ENSG00000204217;urn:miriam:hgnc:1078;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q13873;urn:miriam:uniprot:Q13873;urn:miriam:refseq:NM_001204;urn:miriam:hgnc.symbol:BMPR2;urn:miriam:hgnc.symbol:BMPR2"
      hgnc "HGNC_SYMBOL:ACTR2;HGNC_SYMBOL:BMPR2"
      map_id "UNIPROT:P61160;UNIPROT:Q13873"
      name "BMPR1_slash_2_slash_ACTR2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P61160;UNIPROT:Q13873"
    ]
    graphics [
      x 541.3372795670559
      y 697.6793446556529
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61160;UNIPROT:Q13873"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000102882;urn:miriam:hgnc:6877;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:refseq:NM_001040056;urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5595;urn:miriam:uniprot:P27361;urn:miriam:uniprot:P27361"
      hgnc "HGNC_SYMBOL:MAPK3"
      map_id "UNIPROT:P27361"
      name "MAPK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P27361"
    ]
    graphics [
      x 710.1797437851392
      y 555.8101204836332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD9;HGNC_SYMBOL:SMAD5"
      map_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      x 640.4089674473012
      y 740.0813441294836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 727.5292165365031
      y 426.8229296098936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387; urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:P62877"
      name "RBX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa11; sa12; sa20; sa4"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 844.0451036029792
      y 463.3107997184911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:90;urn:miriam:ncbigene:90;urn:miriam:hgnc:171;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q04771;urn:miriam:uniprot:Q04771;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:refseq:NM_001105;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:ensembl:ENSG00000115170"
      hgnc "HGNC_SYMBOL:ACVR1"
      map_id "UNIPROT:Q04771"
      name "ACVR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:Q04771"
    ]
    graphics [
      x 777.5521239598694
      y 316.7145092031808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04771"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:uniprot:P36896;urn:miriam:uniprot:P36896;urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:ec-code:2.7.11.30;urn:miriam:ncbigene:91;urn:miriam:ncbigene:91;urn:miriam:hgnc:172;urn:miriam:ensembl:ENSG00000135503;urn:miriam:refseq:NM_020328"
      hgnc "HGNC_SYMBOL:ACVR1B"
      map_id "UNIPROT:P36896"
      name "ACVR1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:P36896"
    ]
    graphics [
      x 844.8179060910768
      y 375.10356983540464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P36896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_13"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 691.13619152702
      y 857.4374496569407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 411.8323064427283
      y 124.77100882542013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 36
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P23443"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30153;UNIPROT:P67775"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_12"
      target_id "UNIPROT:P23443"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15796;UNIPROT:P84022"
      target_id "M19_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_14"
      target_id "Modulation_space_of_space_cell_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01137"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P37173;UNIPROT:P36897"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 13
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q14766"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 14
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 15
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_17"
      target_id "UNIPROT:P01137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61586"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 10
    target 17
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_20"
      target_id "UNIPROT:P61586"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_18"
      target_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 14
    target 23
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13145"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 2
    target 23
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_15"
      target_id "M19_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 14
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61160;UNIPROT:Q13873"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 28
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P27361"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 2
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 18
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 1
    target 26
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59637"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 26
    target 29
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_16"
      target_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 7
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15796;UNIPROT:P84022"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 14
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P62877"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 10
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q04771"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 33
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P36896"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P27361"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 2
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 18
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 30
    target 7
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_19"
      target_id "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 29
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
      target_id "M19_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 34
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_13"
      target_id "Modulation_space_of_space_cell_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 6
    target 35
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30153;UNIPROT:P67775"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 10
    target 35
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 3
    target 35
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 35
    target 6
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_21"
      target_id "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
