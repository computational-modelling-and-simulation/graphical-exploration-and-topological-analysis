# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4969; WP4880; WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "NFKB1"
      name "NFKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "ae038; d3bcb; dc151"
      uniprot "NA"
    ]
    graphics [
      x 516.740131123268
      y 662.6576317461693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NFKB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "NFKB1_space_p105"
      name "NFKB1_space_p105"
      node_subtype "GENE"
      node_type "species"
      org_id "ae8f7"
      uniprot "NA"
    ]
    graphics [
      x 454.38023849902424
      y 582.1363757720599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NFKB1_space_p105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id56c2671f"
      uniprot "NA"
    ]
    graphics [
      x 397.58788017437035
      y 685.1364350356671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:refseq:YP_009724391;urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "b6589"
      name "b6589"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6589"
      uniprot "NA"
    ]
    graphics [
      x 306.9224162330926
      y 591.3903170481494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b6589"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "pro_minus_IL1B"
      name "pro_minus_IL1B"
      node_subtype "RNA; GENE"
      node_type "species"
      org_id "d3559; a1e57"
      uniprot "NA"
    ]
    graphics [
      x 329.3780135007795
      y 200.98897134727872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pro_minus_IL1B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaa5a11ed"
      uniprot "NA"
    ]
    graphics [
      x 354.73247497833574
      y 86.15654459569532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000137752"
      hgnc "NA"
      map_id "pro_minus_CASP1"
      name "pro_minus_CASP1"
      node_subtype "GENE"
      node_type "species"
      org_id "a2318"
      uniprot "NA"
    ]
    graphics [
      x 157.289365943849
      y 520.5181619684807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pro_minus_CASP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7ef1c6cf"
      uniprot "NA"
    ]
    graphics [
      x 257.8356199318739
      y 464.6526165423517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000137752"
      hgnc "NA"
      map_id "CASP1"
      name "CASP1"
      node_subtype "GENE"
      node_type "species"
      org_id "b43c2"
      uniprot "NA"
    ]
    graphics [
      x 240.36940503684696
      y 323.71034427944727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CASP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "d72a0"
      name "d72a0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d72a0"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 221.53156396638116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d72a0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide9837dad"
      uniprot "NA"
    ]
    graphics [
      x 181.75599775052254
      y 232.71098515388508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ed3f9"
      uniprot "NA"
    ]
    graphics [
      x 226.37576235277106
      y 165.9249903616078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4876; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "IL1B"
      name "IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "b860a; c9a82"
      uniprot "NA"
    ]
    graphics [
      x 167.6887069163465
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IL1B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:wikidata:Q422438"
      hgnc "NA"
      map_id "Chloroquine"
      name "Chloroquine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d8fdf"
      uniprot "NA"
    ]
    graphics [
      x 560.097697303862
      y 150.70892234977714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Chloroquine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:16418198"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idb1ca554"
      uniprot "NA"
    ]
    graphics [
      x 455.6521955011252
      y 194.44962135430558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 16
    source 2
    target 3
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "NFKB1_space_p105"
      target_id "W18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 4
    target 3
    cd19dm [
      diagram "WP4876"
      edge_type "PHYSICAL_STIMULATION"
      source_id "b6589"
      target_id "W18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 1
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_12"
      target_id "NFKB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 5
    target 6
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "pro_minus_IL1B"
      target_id "W18_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 6
    target 5
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_14"
      target_id "pro_minus_IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 7
    target 8
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "pro_minus_CASP1"
      target_id "W18_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 4
    target 8
    cd19dm [
      diagram "WP4876"
      edge_type "PHYSICAL_STIMULATION"
      source_id "b6589"
      target_id "W18_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 8
    target 9
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_13"
      target_id "CASP1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 10
    target 11
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "d72a0"
      target_id "W18_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 11
    target 5
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_16"
      target_id "pro_minus_IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 5
    target 12
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "pro_minus_IL1B"
      target_id "W18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 9
    target 12
    cd19dm [
      diagram "WP4876"
      edge_type "CATALYSIS"
      source_id "CASP1"
      target_id "W18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 12
    target 13
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_11"
      target_id "IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 14
    target 15
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "Chloroquine"
      target_id "W18_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 15
    target 5
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_15"
      target_id "pro_minus_IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
