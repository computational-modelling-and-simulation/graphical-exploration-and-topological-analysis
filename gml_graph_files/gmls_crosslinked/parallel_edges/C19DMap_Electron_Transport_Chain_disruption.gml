# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 755.3083576592726
      y 743.1510001616577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 472.6178842773594
      y 500.97020762880993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 483.26327500197704
      y 649.4190296913152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 438.66670146393756
      y 595.3375956694347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360; urn:miriam:obo.chebi:CHEBI%3A15846; urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "NAD_plus_"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439; layout_2443; sa3; sa222; sa256; sa108; sa45"
      uniprot "NA"
    ]
    graphics [
      x 417.08532901069236
      y 972.3519323658787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "ROS"
      name "ROS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ed2b8; sa362"
      uniprot "NA"
    ]
    graphics [
      x 954.6697318731607
      y 814.6199559871387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ROS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_162"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa664"
      uniprot "NA"
    ]
    graphics [
      x 519.4088224003685
      y 1886.3411872201364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 409.0380148654873
      y 1772.4403881551666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:AIA62288; urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "Orf9c"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498; sa192"
      uniprot "NA"
    ]
    graphics [
      x 356.8067808492526
      y 1675.5028269827194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:4715;urn:miriam:ncbigene:4715;urn:miriam:refseq:NM_005005;urn:miriam:ensembl:ENSG00000147684;urn:miriam:hgnc:7704;urn:miriam:hgnc.symbol:NDUFB9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:hgnc.symbol:NDUFB9"
      hgnc "HGNC_SYMBOL:NDUFB9"
      map_id "UNIPROT:Q9Y6M9"
      name "NDUFB9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa661"
      uniprot "UNIPROT:Q9Y6M9"
    ]
    graphics [
      x 488.0056506981549
      y 1642.5962055230693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6M9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_169"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa672"
      uniprot "NA"
    ]
    graphics [
      x 702.1490144575753
      y 1638.9537817058058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re156"
      uniprot "NA"
    ]
    graphics [
      x 550.3411543779328
      y 1710.992925586989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:28976;urn:miriam:ncbigene:28976;urn:miriam:refseq:NM_014049;urn:miriam:ec-code:1.3.8.-;urn:miriam:ensembl:ENSG00000177646;urn:miriam:hgnc:21497;urn:miriam:hgnc.symbol:ACAD9;urn:miriam:uniprot:Q9H845;urn:miriam:uniprot:Q9H845;urn:miriam:hgnc.symbol:ACAD9"
      hgnc "HGNC_SYMBOL:ACAD9"
      map_id "UNIPROT:Q9H845"
      name "ACAD9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa668"
      uniprot "UNIPROT:Q9H845"
    ]
    graphics [
      x 471.3608139627687
      y 1861.9046751084336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H845"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18421;urn:miriam:obo.chebi:CHEBI%3A1842"
      hgnc "NA"
      map_id "superoxide"
      name "superoxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa360; sa354"
      uniprot "NA"
    ]
    graphics [
      x 994.7276413617869
      y 754.2674514846076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "superoxide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 861.1104067763553
      y 703.7541723168613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000112096;urn:miriam:hgnc:11180;urn:miriam:ncbigene:6648;urn:miriam:ncbigene:6648;urn:miriam:uniprot:P04179;urn:miriam:refseq:NM_000636;urn:miriam:hgnc.symbol:SOD2;urn:miriam:hgnc.symbol:SOD2;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD2"
      map_id "UNIPROT:P04179"
      name "SOD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358; sa357"
      uniprot "UNIPROT:P04179"
    ]
    graphics [
      x 727.0005942585494
      y 942.5212239184052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 988.0774117415302
      y 443.95000358471236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_49"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 582.3790234232413
      y 572.0745168951576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005753;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc:7414;urn:miriam:ncbigene:4508;urn:miriam:ncbigene:4508;urn:miriam:ensembl:ENSG00000198899;urn:miriam:refseq:YP_003024031;urn:miriam:uniprot:P00846;urn:miriam:uniprot:P00846;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:ensembl:ENSG00000130770;urn:miriam:ncbigene:93974;urn:miriam:ncbigene:93974;urn:miriam:refseq:NM_016311;urn:miriam:hgnc:871;urn:miriam:uniprot:Q9UII2;urn:miriam:uniprot:Q9UII2"
      hgnc "HGNC_SYMBOL:MT-ATP6;HGNC_SYMBOL:ATP5IF1"
      map_id "UNIPROT:P00846;UNIPROT:Q9UII2"
      name "ATP_space_Synthase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      x 806.1997455622037
      y 584.2023394749267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00846;UNIPROT:Q9UII2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_161"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa663"
      uniprot "NA"
    ]
    graphics [
      x 327.11054419226537
      y 1412.8745650445403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 332.4642099720511
      y 1541.85298927615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_004541;urn:miriam:ensembl:ENSG00000125356;urn:miriam:uniprot:O15239;urn:miriam:uniprot:O15239;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:ncbigene:4694;urn:miriam:ncbigene:4694;urn:miriam:hgnc:7683"
      hgnc "HGNC_SYMBOL:NDUFA1"
      map_id "UNIPROT:O15239"
      name "NDUFA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa665"
      uniprot "UNIPROT:O15239"
    ]
    graphics [
      x 455.34669529204075
      y 1528.286244648397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_182"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa714"
      uniprot "NA"
    ]
    graphics [
      x 483.35504512500506
      y 1805.563295251879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 376.53825477541
      y 1883.1837146651828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:Q9BQ95;urn:miriam:uniprot:Q9BQ95;urn:miriam:ensembl:ENSG00000130159;urn:miriam:hgnc:29548;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:refseq:NM_016581;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:ncbigene:51295;urn:miriam:ncbigene:51295"
      hgnc "HGNC_SYMBOL:ECSIT"
      map_id "UNIPROT:Q9BQ95"
      name "ECSIT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa667"
      uniprot "UNIPROT:Q9BQ95"
    ]
    graphics [
      x 259.01098469219687
      y 1807.6173630915778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BQ95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000003509;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:ec-code:2.1.1.320;urn:miriam:hgnc:28816;urn:miriam:ncbigene:55471;urn:miriam:uniprot:Q7L592;urn:miriam:uniprot:Q7L592;urn:miriam:ncbigene:55471;urn:miriam:refseq:NM_144736"
      hgnc "HGNC_SYMBOL:NDUFAF7"
      map_id "UNIPROT:Q7L592"
      name "NDUFAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa669"
      uniprot "UNIPROT:Q7L592"
    ]
    graphics [
      x 197.3421766686355
      y 1895.4382566514655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7L592"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "OXPHOS_space_factors"
      name "OXPHOS_space_factors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 653.1716329982564
      y 1712.5472082385409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "OXPHOS_space_factors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 929.1333437288376
      y 289.2320983007047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16856"
      hgnc "NA"
      map_id "glutathione"
      name "glutathione"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa381"
      uniprot "NA"
    ]
    graphics [
      x 865.8343977218058
      y 138.51469493213983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glutathione"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167468;urn:miriam:ncbigene:2879;urn:miriam:ncbigene:2879;urn:miriam:hgnc:4556;urn:miriam:hgnc.symbol:GPX4;urn:miriam:hgnc.symbol:GPX4;urn:miriam:refseq:NM_002085;urn:miriam:uniprot:P36969;urn:miriam:ec-code:1.11.1.12"
      hgnc "HGNC_SYMBOL:GPX4"
      map_id "UNIPROT:P36969"
      name "GPX4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa379; sa167"
      uniprot "UNIPROT:P36969"
    ]
    graphics [
      x 992.1514398217827
      y 175.76487108772108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P36969"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:2876;urn:miriam:ncbigene:2876;urn:miriam:hgnc:4553;urn:miriam:ensembl:ENSG00000233276;urn:miriam:uniprot:P07203;urn:miriam:hgnc.symbol:GPX1;urn:miriam:hgnc.symbol:GPX1;urn:miriam:refseq:NM_000581;urn:miriam:ec-code:1.11.1.9"
      hgnc "HGNC_SYMBOL:GPX1"
      map_id "UNIPROT:P07203"
      name "GPX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa378"
      uniprot "UNIPROT:P07203"
    ]
    graphics [
      x 1047.1090728616973
      y 422.96097772959763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa356; sa369"
      uniprot "NA"
    ]
    graphics [
      x 1037.2555816186996
      y 316.6283070702923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58297"
      hgnc "NA"
      map_id "glutathione_space_disulfide"
      name "glutathione_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa380"
      uniprot "NA"
    ]
    graphics [
      x 868.0933845109013
      y 388.0616979752756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glutathione_space_disulfide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_32"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 662.4733917926264
      y 671.7827029923619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS25;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS27"
      map_id "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
    ]
    graphics [
      x 1667.5082244270154
      y 1542.223227911938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 1579.8557501828827
      y 1694.7342015052527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742615; urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "Nsp8"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa494; sa2202; sa2362"
      uniprot "NA"
    ]
    graphics [
      x 1632.4001667383804
      y 1585.6812156368162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS27;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS25"
      map_id "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa90"
      uniprot "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
    ]
    graphics [
      x 1374.8869916711135
      y 1826.3005147928693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:doi:10.1042/EBC20170103;urn:miriam:obo.go:GO%3A0006264;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:refseq:NM_007215;urn:miriam:uniprot:Q9UHN1;urn:miriam:uniprot:Q9UHN1;urn:miriam:hgnc:9180;urn:miriam:ensembl:ENSG00000256525;urn:miriam:refseq:NM_018109;urn:miriam:ensembl:ENSG00000107951;urn:miriam:ncbigene:55149;urn:miriam:uniprot:Q9NVV4;urn:miriam:uniprot:Q9NVV4;urn:miriam:ncbigene:55149;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:ec-code:2.7.7.19;urn:miriam:hgnc:25532;urn:miriam:hgnc:29666;urn:miriam:ensembl:ENSG00000103707;urn:miriam:ncbigene:123263;urn:miriam:ncbigene:123263;urn:miriam:uniprot:Q96DP5;urn:miriam:uniprot:Q96DP5;urn:miriam:ec-code:2.1.2.9;urn:miriam:refseq:NM_139242;urn:miriam:hgnc.symbol:MTFMT;urn:miriam:hgnc.symbol:MTFMT"
      hgnc "HGNC_SYMBOL:POLG;HGNC_SYMBOL:POLG2;HGNC_SYMBOL:MTPAP;HGNC_SYMBOL:MTFMT"
      map_id "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
      name "Mt_space_replication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
    ]
    graphics [
      x 1458.7109361121752
      y 1439.1188594254306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_82"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1548.27137075376
      y 1594.9013788266918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "mt_space_DNA_space_replication"
      name "mt_space_DNA_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa463"
      uniprot "NA"
    ]
    graphics [
      x 1648.382278151739
      y 1777.0868509484349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mt_space_DNA_space_replication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_180"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa712"
      uniprot "NA"
    ]
    graphics [
      x 1234.6185659062696
      y 852.0164608774664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re164"
      uniprot "NA"
    ]
    graphics [
      x 1158.1460045539668
      y 690.0179763411709
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964;urn:miriam:uniprot:O75964; urn:miriam:uniprot:O75964;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964; urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG; NA"
      map_id "UNIPROT:O75964"
      name "ATP5MG; F_minus_ATPase; F_minus_ATPase:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa710; csa42; csa43; sa255; csa97"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1294.1102357243903
      y 725.0616591189469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75964"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613; urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049; urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1; urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "Nsp6"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713; sa48; sa307; sa304; sa2204; sa2367"
      uniprot "NA"
    ]
    graphics [
      x 1280.5755290036732
      y 798.7103845121735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 948.3730403711916
      y 537.2145684476963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16234"
      hgnc "NA"
      map_id "hydroxide"
      name "hydroxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa364"
      uniprot "NA"
    ]
    graphics [
      x 1189.5799466103354
      y 571.4960319595533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hydroxide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0000262"
      hgnc "NA"
      map_id "mt_space_DNA"
      name "mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa652"
      uniprot "NA"
    ]
    graphics [
      x 1677.3970268147873
      y 1926.056908748284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mt_space_DNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_84"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 1516.4898765780922
      y 1710.151934787103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:refseq:NM_002047;urn:miriam:ensembl:ENSG00000106105;urn:miriam:uniprot:P41250;urn:miriam:uniprot:P41250;urn:miriam:hgnc.symbol:GARS1;urn:miriam:hgnc.symbol:GARS1;urn:miriam:ec-code:2.7.7.-;urn:miriam:ncbigene:2617;urn:miriam:ncbigene:2617;urn:miriam:hgnc:4162;urn:miriam:ec-code:6.1.1.14;urn:miriam:hgnc:25538;urn:miriam:hgnc.symbol:DARS2;urn:miriam:hgnc.symbol:DARS2;urn:miriam:ncbigene:55157;urn:miriam:refseq:NM_018122;urn:miriam:ncbigene:55157;urn:miriam:ensembl:ENSG00000117593;urn:miriam:ec-code:6.1.1.12;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q5JTZ9;urn:miriam:uniprot:Q5JTZ9;urn:miriam:ensembl:ENSG00000124608;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc:21022;urn:miriam:ncbigene:57505;urn:miriam:ncbigene:57505;urn:miriam:refseq:NM_020745;urn:miriam:ec-code:6.1.1.7;urn:miriam:ncbigene:3735;urn:miriam:ncbigene:3735;urn:miriam:ensembl:ENSG00000065427;urn:miriam:hgnc:6215;urn:miriam:ec-code:2.7.7.-;urn:miriam:uniprot:Q15046;urn:miriam:uniprot:Q15046;urn:miriam:refseq:NM_005548;urn:miriam:ec-code:6.1.1.6;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc:21406;urn:miriam:hgnc.symbol:RARS2;urn:miriam:hgnc.symbol:RARS2;urn:miriam:ncbigene:57038;urn:miriam:refseq:NM_020320;urn:miriam:ncbigene:57038;urn:miriam:ec-code:6.1.1.19;urn:miriam:uniprot:Q5T160;urn:miriam:uniprot:Q5T160;urn:miriam:ensembl:ENSG00000146282"
      hgnc "HGNC_SYMBOL:GARS1;HGNC_SYMBOL:DARS2;HGNC_SYMBOL:AARS2;HGNC_SYMBOL:KARS1;HGNC_SYMBOL:RARS2"
      map_id "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
      name "Mt_minus_tRNA_space_synthetase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
    ]
    graphics [
      x 1409.4215153291934
      y 1739.380865598755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:28752201;urn:miriam:refseq:NM_017722;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:ensembl:ENSG00000104907;urn:miriam:uniprot:Q9NXH9;urn:miriam:uniprot:Q9NXH9;urn:miriam:ec-code:2.1.1.216;urn:miriam:hgnc:25980"
      hgnc "HGNC_SYMBOL:TRMT1"
      map_id "UNIPROT:Q9NXH9"
      name "TRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa692"
      uniprot "UNIPROT:Q9NXH9"
    ]
    graphics [
      x 1548.87251972769
      y 1459.8210134694184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NXH9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ensembl:ENSG00000210174;urn:miriam:ncbigene:4573;urn:miriam:hgnc:7496;urn:miriam:hgnc.symbol:MT-TR;urn:miriam:hgnc:7489;urn:miriam:hgnc.symbol:MT-TK;urn:miriam:ensembl:ENSG00000210156;urn:miriam:ncbigene:4566;urn:miriam:hgnc:7501;urn:miriam:hgnc.symbol:MT-TW;urn:miriam:ensembl:ENSG00000210117;urn:miriam:ncbigene:4578;urn:miriam:ensembl:ENSG00000210077;urn:miriam:hgnc:7500;urn:miriam:hgnc.symbol:MT-TV;urn:miriam:ncbigene:4577;urn:miriam:ensembl:ENSG00000210100;urn:miriam:hgnc:7488;urn:miriam:hgnc.symbol:MT-TI;urn:miriam:ncbigene:4565;urn:miriam:hgnc.symbol:MT-TP;urn:miriam:ensembl:ENSG00000210196;urn:miriam:ncbigene:4571;urn:miriam:hgnc:7494;urn:miriam:ensembl:ENSG00000210151;urn:miriam:hgnc.symbol:MT-TS1;urn:miriam:ncbigene:4574;urn:miriam:hgnc:7497;urn:miriam:ncbigene:4549;urn:miriam:uniprot:A0A0C5B5G6;urn:miriam:hgnc:7470;urn:miriam:ensembl:ENSG00000211459;urn:miriam:hgnc.symbol:MT-RNR1;urn:miriam:hgnc:7479;urn:miriam:ensembl:ENSG00000210194;urn:miriam:hgnc.symbol:MT-TE;urn:miriam:ncbigene:4556;urn:miriam:hgnc.symbol:MT-TL2;urn:miriam:ensembl:ENSG00000210191;urn:miriam:hgnc:7491;urn:miriam:ncbigene:4568;urn:miriam:hgnc:7499;urn:miriam:ensembl:ENSG00000210195;urn:miriam:hgnc.symbol:MT-TT;urn:miriam:ncbigene:4576;urn:miriam:hgnc:7498;urn:miriam:ensembl:ENSG00000210184;urn:miriam:hgnc.symbol:MT-TS2;urn:miriam:ncbigene:4575;urn:miriam:ncbigene:4558;urn:miriam:hgnc:7481;urn:miriam:hgnc.symbol:MT-TF;urn:miriam:ensembl:ENSG00000210049;urn:miriam:hgnc.symbol:MT-TL1;urn:miriam:hgnc:7490;urn:miriam:ensembl:ENSG00000209082;urn:miriam:ncbigene:4567;urn:miriam:hgnc.symbol:MT-TN;urn:miriam:ncbigene:4570;urn:miriam:hgnc:7493;urn:miriam:ensembl:ENSG00000210135;urn:miriam:ensembl:ENSG00000210176;urn:miriam:hgnc:7487;urn:miriam:ncbigene:4564;urn:miriam:hgnc.symbol:MT-TH;urn:miriam:ensembl:ENSG00000210107;urn:miriam:ncbigene:4572;urn:miriam:hgnc.symbol:MT-TQ;urn:miriam:hgnc:7495;urn:miriam:hgnc:7477;urn:miriam:ensembl:ENSG00000210140;urn:miriam:hgnc.symbol:MT-TC;urn:miriam:ncbigene:4511"
      hgnc "HGNC_SYMBOL:MT-TR;HGNC_SYMBOL:MT-TK;HGNC_SYMBOL:MT-TW;HGNC_SYMBOL:MT-TV;HGNC_SYMBOL:MT-TI;HGNC_SYMBOL:MT-TP;HGNC_SYMBOL:MT-TS1;HGNC_SYMBOL:MT-RNR1;HGNC_SYMBOL:MT-TE;HGNC_SYMBOL:MT-TL2;HGNC_SYMBOL:MT-TT;HGNC_SYMBOL:MT-TS2;HGNC_SYMBOL:MT-TF;HGNC_SYMBOL:MT-TL1;HGNC_SYMBOL:MT-TN;HGNC_SYMBOL:MT-TH;HGNC_SYMBOL:MT-TQ;HGNC_SYMBOL:MT-TC"
      map_id "UNIPROT:A0A0C5B5G6"
      name "MT_space_tRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa55"
      uniprot "UNIPROT:A0A0C5B5G6"
    ]
    graphics [
      x 1375.7441587431679
      y 1664.2600208492477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A0A0C5B5G6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0047485"
      hgnc "NA"
      map_id "precursor_space_protein_space_N_minus_terminus_space_binding"
      name "precursor_space_protein_space_N_minus_terminus_space_binding"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa688; sa690"
      uniprot "NA"
    ]
    graphics [
      x 1639.531037933848
      y 1389.5124486154723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "precursor_space_protein_space_N_minus_terminus_space_binding"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_48"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1499.2108558325824
      y 1352.855778276215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005742;urn:miriam:uniprot:TOMM37;urn:miriam:refseq:NM_014820;urn:miriam:uniprot:O94826;urn:miriam:uniprot:O94826;urn:miriam:hgnc:11985;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:ncbigene:9868;urn:miriam:ncbigene:9868;urn:miriam:ensembl:ENSG00000154174;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7;urn:miriam:refseq:NM_020243;urn:miriam:uniprot:Q9NS69;urn:miriam:uniprot:Q9NS69;urn:miriam:ncbigene:56993;urn:miriam:ncbigene:56993;urn:miriam:hgnc:18002;urn:miriam:ensembl:ENSG00000100216;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:hgnc.symbol:TOMM22"
      hgnc "HGNC_SYMBOL:TOMM70;HGNC_SYMBOL:TIMM9;HGNC_SYMBOL:TOMM22"
      map_id "UNIPROT:TOMM37;UNIPROT:O94826;UNIPROT:Q9Y5J7;UNIPROT:Q9NS69"
      name "TOM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:TOMM37;UNIPROT:O94826;UNIPROT:Q9Y5J7;UNIPROT:Q9NS69"
    ]
    graphics [
      x 1601.0060959936122
      y 1282.7105703124262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:TOMM37;UNIPROT:O94826;UNIPROT:Q9Y5J7;UNIPROT:Q9NS69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0042721;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643;urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337"
      hgnc "HGNC_SYMBOL:SDHC;HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM22"
      map_id "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
      name "TIM22_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
    ]
    graphics [
      x 1313.9979105834605
      y 1443.2808778852943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:APO40587"
      hgnc "NA"
      map_id "Orf9b"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa691"
      uniprot "NA"
    ]
    graphics [
      x 1645.3075712485147
      y 1324.9173461344567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf9b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005744;urn:miriam:ncbigene:10440;urn:miriam:uniprot:Q99595;urn:miriam:uniprot:Q99595;urn:miriam:ncbigene:10440;urn:miriam:hgnc:17315;urn:miriam:refseq:NM_006335;urn:miriam:ensembl:ENSG00000134375;urn:miriam:hgnc.symbol:TIMM17A;urn:miriam:hgnc.symbol:TIMM17A;urn:miriam:hgnc:17310;urn:miriam:hgnc.symbol:TIMM17B;urn:miriam:hgnc.symbol:TIMM17B;urn:miriam:ensembl:ENSG00000126768;urn:miriam:ncbigene:10245;urn:miriam:ncbigene:10245;urn:miriam:uniprot:O60830;urn:miriam:uniprot:O60830;urn:miriam:refseq:NM_005834;urn:miriam:hgnc:17312;urn:miriam:hgnc.symbol:TIMM23;urn:miriam:hgnc.symbol:TIMM23;urn:miriam:ensembl:ENSG00000265354;urn:miriam:uniprot:O14925;urn:miriam:uniprot:O14925;urn:miriam:refseq:NM_006327.2;urn:miriam:ncbigene:100287932;urn:miriam:ncbigene:100287932"
      hgnc "HGNC_SYMBOL:TIMM17A;HGNC_SYMBOL:TIMM17B;HGNC_SYMBOL:TIMM23"
      map_id "UNIPROT:Q99595;UNIPROT:O60830;UNIPROT:O14925"
      name "TIM23_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:Q99595;UNIPROT:O60830;UNIPROT:O14925"
    ]
    graphics [
      x 1447.4656559906548
      y 1249.2152195497345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99595;UNIPROT:O60830;UNIPROT:O14925"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0042719;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "UNIPROT:P62072;UNIPROT:Q9Y5J7"
      name "TIM9_minus_TIM10_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa95; csa96"
      uniprot "UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 1439.9329231562642
      y 1495.6079240878391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62072;UNIPROT:Q9Y5J7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1131.6231525944356
      y 223.7415582627351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_012473;urn:miriam:hgnc:17772;urn:miriam:uniprot:Q99757;urn:miriam:ncbigene:25828;urn:miriam:ncbigene:25828;urn:miriam:hgnc.symbol:TXN2;urn:miriam:hgnc.symbol:TXN2;urn:miriam:ensembl:ENSG00000100348"
      hgnc "HGNC_SYMBOL:TXN2"
      map_id "UNIPROT:Q99757"
      name "TXN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa392; sa391"
      uniprot "UNIPROT:Q99757"
    ]
    graphics [
      x 1214.1573076197024
      y 268.34295075830073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99757"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:26067716;urn:miriam:uniprot:P30044;urn:miriam:uniprot:P30044;urn:miriam:refseq:NM_181651;urn:miriam:ensembl:ENSG00000126432;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc:9355;urn:miriam:ncbigene:25824;urn:miriam:ncbigene:25824;urn:miriam:ec-code:3.1.1.4;urn:miriam:hgnc:16753;urn:miriam:ec-code:2.3.1.23;urn:miriam:refseq:NM_004905;urn:miriam:ensembl:ENSG00000117592;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:ncbigene:9588;urn:miriam:ncbigene:9588;urn:miriam:uniprot:P30041;urn:miriam:uniprot:P30041;urn:miriam:ec-code:1.11.1.27;urn:miriam:uniprot:Q06830;urn:miriam:uniprot:Q06830;urn:miriam:refseq:NM_181697;urn:miriam:ncbigene:5052;urn:miriam:ncbigene:5052;urn:miriam:hgnc:9352;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000117450;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:ncbigene:7001;urn:miriam:ncbigene:7001;urn:miriam:refseq:NM_005809;urn:miriam:uniprot:P32119;urn:miriam:uniprot:P32119;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000167815;urn:miriam:hgnc:9353;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:ensembl:ENSG00000165672;urn:miriam:ncbigene:10935;urn:miriam:ncbigene:10935;urn:miriam:uniprot:P30048;urn:miriam:uniprot:P30048;urn:miriam:refseq:NM_006793;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc:9354"
      hgnc "HGNC_SYMBOL:PRDX5;HGNC_SYMBOL:PRDX6;HGNC_SYMBOL:PRDX1;HGNC_SYMBOL:PRDX2;HGNC_SYMBOL:PRDX3"
      map_id "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
      name "PRDX"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
    ]
    graphics [
      x 1290.6347239557163
      y 242.528088125777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA; urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "e_minus_"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa645; sa650; sa651; sa242; sa195; sa320"
      uniprot "NA"
    ]
    graphics [
      x 485.13580218361164
      y 854.9940590320718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e_minus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_37"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 631.2333698583361
      y 909.0564707200182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "Q"
      name "Q"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa219; sa6"
      uniprot "NA"
    ]
    graphics [
      x 725.9260830670685
      y 1066.214569954321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Q"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_81"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 1646.4779702442893
      y 2131.7409786121907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_003201;urn:miriam:hgnc:11741;urn:miriam:hgnc.symbol:TFAM;urn:miriam:hgnc.symbol:TFAM;urn:miriam:uniprot:Q00059;urn:miriam:ensembl:ENSG00000108064;urn:miriam:ncbigene:7019;urn:miriam:ncbigene:7019"
      hgnc "HGNC_SYMBOL:TFAM"
      map_id "UNIPROT:Q00059"
      name "TFAM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa460"
      uniprot "UNIPROT:Q00059"
    ]
    graphics [
      x 1766.8947180671962
      y 2019.4737131498941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00059"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:18391175;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:ncbigene:51106;urn:miriam:ncbigene:51106;urn:miriam:hgnc:17037;urn:miriam:ensembl:ENSG00000029639;urn:miriam:uniprot:Q8WVM0;urn:miriam:uniprot:Q8WVM0;urn:miriam:ec-code:2.1.1.-;urn:miriam:refseq:NM_001350501;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:uniprot:Q9H5Q4;urn:miriam:uniprot:Q9H5Q4;urn:miriam:hgnc:18559;urn:miriam:refseq:NM_022366;urn:miriam:ec-code:2.1.1.-;urn:miriam:ensembl:ENSG00000162851;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:uniprot:O00411;urn:miriam:uniprot:O00411;urn:miriam:refseq:NM_005035;urn:miriam:ncbigene:5442;urn:miriam:ncbigene:5442;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9200;urn:miriam:ensembl:ENSG00000099821"
      hgnc "HGNC_SYMBOL:TFB1M;HGNC_SYMBOL:TFB2M;HGNC_SYMBOL:POLRMT"
      map_id "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
      name "MT_space_transcription"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
    ]
    graphics [
      x 1585.7490287956853
      y 2058.9382168760626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "damaged_space_mt_space_DNA"
      name "damaged_space_mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa653"
      uniprot "NA"
    ]
    graphics [
      x 1799.064312584684
      y 2106.730376688292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "damaged_space_mt_space_DNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33699"
      hgnc "NA"
      map_id "mt_space_mRNA"
      name "mt_space_mRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa459"
      uniprot "NA"
    ]
    graphics [
      x 1396.2677061401669
      y 2040.271067406638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mt_space_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_159"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa659"
      uniprot "NA"
    ]
    graphics [
      x 518.7775667782396
      y 1451.2024218499353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 608.2934242411864
      y 1528.2699854654347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "mtDNA_space_encoded_space_OXPHOS_space_units"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa98; csa60; csa103"
      uniprot "NA"
    ]
    graphics [
      x 862.736437279393
      y 1704.4149654369799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mtDNA_space_encoded_space_OXPHOS_space_units"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045271;urn:miriam:doi:10.1016/j.bbabio.2011.08.010;urn:miriam:pubmed:19355884;urn:miriam:ensembl:ENSG00000023228;urn:miriam:refseq:NM_005006;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc:7707;urn:miriam:uniprot:P28331;urn:miriam:uniprot:P28331;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4723;urn:miriam:ncbigene:4723;urn:miriam:uniprot:P49821;urn:miriam:uniprot:P49821;urn:miriam:refseq:NM_007103;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc:7716;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000167792;urn:miriam:ncbigene:4537;urn:miriam:ncbigene:4537;urn:miriam:uniprot:P03897;urn:miriam:uniprot:P03897;urn:miriam:hgnc:7458;urn:miriam:refseq:YP_003024033;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ensembl:ENSG00000198840;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ec-code:7.1.1.2;urn:miriam:uniprot:P03923;urn:miriam:uniprot:P03923;urn:miriam:refseq:YP_003024037;urn:miriam:ncbigene:4541;urn:miriam:ncbigene:4541;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7462;urn:miriam:ensembl:ENSG00000198695;urn:miriam:ensembl:ENSG00000178127;urn:miriam:refseq:NM_021074;urn:miriam:ncbigene:4729;urn:miriam:ncbigene:4729;urn:miriam:uniprot:P19404;urn:miriam:uniprot:P19404;urn:miriam:hgnc:7717;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:ensembl:ENSG00000110717;urn:miriam:refseq:NM_002496;urn:miriam:ncbigene:4728;urn:miriam:ncbigene:4728;urn:miriam:uniprot:O00217;urn:miriam:uniprot:O00217;urn:miriam:hgnc:7715;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4539;urn:miriam:uniprot:P03901;urn:miriam:uniprot:P03901;urn:miriam:ncbigene:4539;urn:miriam:ensembl:ENSG00000212907;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc:7460;urn:miriam:refseq:YP_003024034;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7456;urn:miriam:ncbigene:4536;urn:miriam:ncbigene:4536;urn:miriam:refseq:YP_003024027;urn:miriam:uniprot:P03891;urn:miriam:uniprot:P03891;urn:miriam:ensembl:ENSG00000198763;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc:7710;urn:miriam:uniprot:O75489;urn:miriam:uniprot:O75489;urn:miriam:ensembl:ENSG00000213619;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:refseq:NM_004551;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:ncbigene:4722;urn:miriam:ncbigene:4722;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:YP_003024036;urn:miriam:ensembl:ENSG00000198786;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7461;urn:miriam:uniprot:P03915;urn:miriam:uniprot:P03915;urn:miriam:ncbigene:4538;urn:miriam:ncbigene:4538;urn:miriam:hgnc:7459;urn:miriam:refseq:YP_003024035;urn:miriam:ensembl:ENSG00000198886;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:uniprot:P03905;urn:miriam:uniprot:P03905;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000115286;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc:7714;urn:miriam:uniprot:O75251;urn:miriam:uniprot:O75251;urn:miriam:ncbigene:374291;urn:miriam:ncbigene:374291;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:NM_024407;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:ensembl:ENSG00000158864;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:refseq:NM_004550;urn:miriam:uniprot:O75306;urn:miriam:uniprot:O75306;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7708;urn:miriam:ncbigene:4720;urn:miriam:ncbigene:4720;urn:miriam:ensembl:ENSG00000160194;urn:miriam:refseq:NM_001001503;urn:miriam:uniprot:P56181;urn:miriam:uniprot:P56181;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc:7719;urn:miriam:ncbigene:4731;urn:miriam:ncbigene:4731;urn:miriam:hgnc:7455;urn:miriam:refseq:YP_003024026;urn:miriam:uniprot:P03886;urn:miriam:uniprot:P03886;urn:miriam:ensembl:ENSG00000198888;urn:miriam:ncbigene:4535;urn:miriam:ncbigene:4535;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND1;urn:miriam:hgnc.symbol:MT-ND1"
      hgnc "HGNC_SYMBOL:NDUFS1;HGNC_SYMBOL:NDUFV1;HGNC_SYMBOL:MT-ND3;HGNC_SYMBOL:MT-ND6;HGNC_SYMBOL:NDUFV2;HGNC_SYMBOL:NDUFS8;HGNC_SYMBOL:MT-ND4L;HGNC_SYMBOL:MT-ND2;HGNC_SYMBOL:NDUFS3;HGNC_SYMBOL:MT-ND5;HGNC_SYMBOL:MT-ND4;HGNC_SYMBOL:NDUFS7;HGNC_SYMBOL:NDUFS2;HGNC_SYMBOL:NDUFV3;HGNC_SYMBOL:MT-ND1"
      map_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
      name "Complex_space_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
    ]
    graphics [
      x 675.2771855470407
      y 1166.8846355677742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1098.8005523554802
      y 735.7456850577034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 607.3195850972431
      y 1156.9968941805434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:23410;urn:miriam:ncbigene:23410;urn:miriam:uniprot:Q9NTG7;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc:14931;urn:miriam:refseq:NM_001017524;urn:miriam:ec-code:2.3.1.286;urn:miriam:ensembl:ENSG00000142082"
      hgnc "HGNC_SYMBOL:SIRT3"
      map_id "UNIPROT:Q9NTG7"
      name "SIRT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa359; sa205"
      uniprot "UNIPROT:Q9NTG7"
    ]
    graphics [
      x 612.0055565142759
      y 1343.02887206121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NTG7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_35"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 732.2324075186691
      y 642.8129975785155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_168"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa671"
      uniprot "NA"
    ]
    graphics [
      x 249.7446894969164
      y 1524.827172098571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 225.13196988335665
      y 1670.4859037236513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 665.649933941437
      y 972.9101598821313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_79"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1727.5663077653694
      y 1863.3065998436002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1400.9870310553333
      y 544.1135260110774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "Fe2_plus_"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa367; sa365; sa375; sa163; sa55; sa162; sa28; sa135; sa238; sa240; sa357"
      uniprot "NA"
    ]
    graphics [
      x 1279.7834490901396
      y 531.4017817820255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fe2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A49648"
      hgnc "NA"
      map_id "HO"
      name "HO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 1422.9217585658882
      y 419.34067399556056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "Fe3_plus_"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa368; sa366; sa376; sa138; sa229"
      uniprot "NA"
    ]
    graphics [
      x 1287.9203183817758
      y 594.7616318695636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fe3_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_178"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa706"
      uniprot "NA"
    ]
    graphics [
      x 1118.9169035765378
      y 1479.6386182095403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1158.1787955542727
      y 1577.7382741468323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580"
      hgnc "HGNC_SYMBOL:TIMM29"
      map_id "UNIPROT:Q9BSF4"
      name "TIMM29"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa700"
      uniprot "UNIPROT:Q9BSF4"
    ]
    graphics [
      x 1038.788893580608
      y 1668.842938538187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BSF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611; urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "Nsp4"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa695; sa4; sa217; sa2228"
      uniprot "NA"
    ]
    graphics [
      x 1250.2259077586923
      y 1622.5925667016015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643"
      hgnc "HGNC_SYMBOL:SDHD;HGNC_SYMBOL:SDHC"
      map_id "UNIPROT:O14521;UNIPROT:Q99643"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24; csa23"
      uniprot "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      x 730.97401498001
      y 1394.5974465973995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14521;UNIPROT:Q99643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:31082116"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 862.079061346608
      y 1428.2915868953473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "TCA"
      name "TCA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa500"
      uniprot "NA"
    ]
    graphics [
      x 973.9165645586552
      y 1402.703067616499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TCA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683"
      hgnc "HGNC_SYMBOL:SDHC;HGNC_SYMBOL:SDHD"
      map_id "UNIPROT:Q99643;UNIPROT:O14521"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:Q99643;UNIPROT:O14521"
    ]
    graphics [
      x 826.3065850737468
      y 1324.5960064429312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99643;UNIPROT:O14521"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_86"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 1175.1436492270368
      y 1890.9762923328594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:hgnc.symbol:TWNK;urn:miriam:hgnc.symbol:TWNK;urn:miriam:ncbigene:56652;urn:miriam:ncbigene:56652;urn:miriam:ec-code:3.6.4.12;urn:miriam:refseq:NM_021830;urn:miriam:ensembl:ENSG00000107815;urn:miriam:hgnc:1160;urn:miriam:uniprot:Q96RR1;urn:miriam:uniprot:Q96RR1;urn:miriam:ensembl:ENSG00000123297;urn:miriam:uniprot:P43897;urn:miriam:uniprot:P43897;urn:miriam:hgnc:12367;urn:miriam:ncbigene:10102;urn:miriam:refseq:NM_005726;urn:miriam:ncbigene:10102;urn:miriam:hgnc.symbol:TSFM;urn:miriam:hgnc.symbol:TSFM;urn:miriam:ncbigene:7284;urn:miriam:ncbigene:7284;urn:miriam:ensembl:ENSG00000178952;urn:miriam:refseq:NM_003321;urn:miriam:hgnc:12420;urn:miriam:uniprot:P49411;urn:miriam:uniprot:P49411;urn:miriam:hgnc.symbol:TUFM;urn:miriam:hgnc.symbol:TUFM;urn:miriam:uniprot:Q96RP9;urn:miriam:uniprot:Q96RP9;urn:miriam:ncbigene:85476;urn:miriam:ncbigene:85476;urn:miriam:hgnc.symbol:GFM1;urn:miriam:hgnc.symbol:GFM1;urn:miriam:refseq:NM_024996;urn:miriam:ensembl:ENSG00000168827;urn:miriam:hgnc:13780"
      hgnc "HGNC_SYMBOL:TWNK;HGNC_SYMBOL:TSFM;HGNC_SYMBOL:TUFM;HGNC_SYMBOL:GFM1"
      map_id "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
      name "Mt_space_translation"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
    ]
    graphics [
      x 1205.188394924449
      y 2008.3372558511342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ensembl:ENSG00000175110;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ncbigene:56945;urn:miriam:ncbigene:56945;urn:miriam:hgnc:14508;urn:miriam:uniprot:P82650;urn:miriam:uniprot:P82650;urn:miriam:refseq:NM_020191;urn:miriam:refseq:NM_007208;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc:10379;urn:miriam:ensembl:ENSG00000114686;urn:miriam:uniprot:P09001;urn:miriam:uniprot:P09001;urn:miriam:ensembl:ENSG00000182180;urn:miriam:hgnc:14048;urn:miriam:ncbigene:51021;urn:miriam:ncbigene:51021;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:refseq:NM_016065;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:uniprot:Q9Y3D3;urn:miriam:uniprot:Q9Y3D3"
      hgnc "HGNC_SYMBOL:MRPS22;HGNC_SYMBOL:MRPL3;HGNC_SYMBOL:MRPS16"
      map_id "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
      name "Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
    ]
    graphics [
      x 1103.3699164436296
      y 1991.7559753744745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1269.9661626117108
      y 425.5830562862823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 783.9080235070509
      y 176.2059391177703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474; urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "NADPH"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382; sa395; sa30; sa295; sa153; sa25; sa33; sa24; sa100; sa356; sa379"
      uniprot "NA"
    ]
    graphics [
      x 925.194494109089
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000104687;urn:miriam:ec-code:1.8.1.7;urn:miriam:hgnc:4623;urn:miriam:ncbigene:2936;urn:miriam:ncbigene:2936;urn:miriam:refseq:NM_000637;urn:miriam:hgnc.symbol:GSR;urn:miriam:hgnc.symbol:GSR;urn:miriam:uniprot:P00390"
      hgnc "HGNC_SYMBOL:GSR"
      map_id "UNIPROT:P00390"
      name "GSR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa384"
      uniprot "UNIPROT:P00390"
    ]
    graphics [
      x 770.2972126375143
      y 297.65934709560224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "NADP(_plus_)"
      name "NADP(_plus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa383; sa394"
      uniprot "NA"
    ]
    graphics [
      x 926.2871607368227
      y 197.2746648539827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADP(_plus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 731.7010953554084
      y 1246.4085054351258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      name "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220; sa255"
      uniprot "NA"
    ]
    graphics [
      x 812.1098316503487
      y 1116.4810367670639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 714.7947515495184
      y 548.3684273039628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 959.2443682150787
      y 904.5986290604671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905;urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "paraquat_space_dication"
      name "paraquat_space_dication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "NA"
    ]
    graphics [
      x 1091.1340414929248
      y 1071.0566070940445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "paraquat_space_dication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:18039652;PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 923.7185481213485
      y 1030.82692527286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905"
      hgnc "NA"
      map_id "paraquat"
      name "paraquat"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa397"
      uniprot "NA"
    ]
    graphics [
      x 1026.5147895162934
      y 1080.6744408929508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "paraquat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re162"
      uniprot "NA"
    ]
    graphics [
      x 1340.3829408385013
      y 1556.6756818188296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re120"
      uniprot "NA"
    ]
    graphics [
      x 1134.6685326777733
      y 385.1265673823301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_001752;urn:miriam:ncbigene:847;urn:miriam:ncbigene:847;urn:miriam:ec-code:1.11.1.6;urn:miriam:ensembl:ENSG00000121691;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc:1516;urn:miriam:uniprot:P04040"
      hgnc "HGNC_SYMBOL:CAT"
      map_id "UNIPROT:P04040"
      name "CAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P04040"
    ]
    graphics [
      x 1212.072584984878
      y 478.98627617515194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04040"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_167"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa670"
      uniprot "NA"
    ]
    graphics [
      x 104.22198464929522
      y 1699.5412513507417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re158"
      uniprot "NA"
    ]
    graphics [
      x 63.69507510329879
      y 1803.1648489821623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "Nsp7"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa499; sa2203; sa2363"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1643.1742099798118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_38"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 582.1846351657039
      y 796.7837677172704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS; urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "UNIPROT:P99999"
      name "Cyt_space_C; CYCS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253; sa13; sa24; sa25"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 725.2121960545351
      y 846.076238741171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P99999"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 1829.7945487850056
      y 1929.073235375467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "mt_space_DNA_space_damage"
      name "mt_space_DNA_space_damage"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 1794.5500334847543
      y 1737.8700706402315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mt_space_DNA_space_damage"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1078.2456286020247
      y 645.4290366636374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa374; sa361; sa24"
      uniprot "NA"
    ]
    graphics [
      x 1037.8435661431126
      y 826.0755618766111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1144.4908227574897
      y 787.580873114428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 675.2974749487464
      y 1515.1367417323713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:12032145"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1082.2051174632984
      y 131.10516567876084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ec-code:1.8.1.9;urn:miriam:ensembl:ENSG00000184470;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:refseq:NM_006440;urn:miriam:hgnc:18155;urn:miriam:ncbigene:10587;urn:miriam:ncbigene:10587;urn:miriam:uniprot:Q9NNW7"
      hgnc "HGNC_SYMBOL:TXNRD2"
      map_id "UNIPROT:Q9NNW7"
      name "TXNRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa393"
      uniprot "UNIPROT:Q9NNW7"
    ]
    graphics [
      x 1112.5350930028176
      y 293.4627673444618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NNW7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908; urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153; urn:miriam:obo.chebi:CHEBI%3A57945"
      hgnc "NA"
      map_id "NADH"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2; sa322; sa207; sa79; sa1264; sa286; sa44"
      uniprot "NA"
    ]
    graphics [
      x 451.9364231718354
      y 1044.8545403308585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:19355884"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 549.7288512218333
      y 963.2028389990337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_34"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 671.7286873333833
      y 600.8719688416788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_185"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa717"
      uniprot "NA"
    ]
    graphics [
      x 1047.260024020054
      y 1731.0076140334068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 863.2526618518018
      y 1799.5606333550922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045277;urn:miriam:uniprot:P00414;urn:miriam:uniprot:P00414;urn:miriam:ncbigene:4514;urn:miriam:ncbigene:4514;urn:miriam:hgnc:7422;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ensembl:ENSG00000198938;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ec-code:7.1.1.9;urn:miriam:refseq:YP_003024032;urn:miriam:uniprot:P10176;urn:miriam:uniprot:P10176;urn:miriam:ncbigene:1351;urn:miriam:ncbigene:1351;urn:miriam:refseq:NM_004074;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc:2294;urn:miriam:ensembl:ENSG00000176340;urn:miriam:uniprot:P00403;urn:miriam:uniprot:P00403;urn:miriam:refseq:YP_003024029;urn:miriam:hgnc:7421;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:ec-code:7.1.1.9;urn:miriam:ensembl:ENSG00000198712;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:1337;urn:miriam:ncbigene:1337;urn:miriam:hgnc:2277;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:refseq:NM_004373;urn:miriam:ensembl:ENSG00000111775;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P14854;urn:miriam:uniprot:P14854;urn:miriam:ensembl:ENSG00000126267;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:ncbigene:1340;urn:miriam:ncbigene:1340;urn:miriam:hgnc:2280;urn:miriam:refseq:NM_001863;urn:miriam:refseq:NM_001862;urn:miriam:ncbigene:1329;urn:miriam:ncbigene:1329;urn:miriam:hgnc:2269;urn:miriam:hgnc.symbol:COX5B;urn:miriam:uniprot:P10606;urn:miriam:uniprot:P10606;urn:miriam:hgnc.symbol:COX5B;urn:miriam:ensembl:ENSG00000135940;urn:miriam:uniprot:P15954;urn:miriam:uniprot:P15954;urn:miriam:ncbigene:1350;urn:miriam:refseq:NM_001867;urn:miriam:ncbigene:1350;urn:miriam:hgnc.symbol:COX7C;urn:miriam:hgnc.symbol:COX7C;urn:miriam:ensembl:ENSG00000127184;urn:miriam:hgnc:2292;urn:miriam:refseq:NM_005205;urn:miriam:uniprot:Q02221;urn:miriam:uniprot:Q02221;urn:miriam:ncbigene:1339;urn:miriam:ncbigene:1339;urn:miriam:hgnc:2279;urn:miriam:ensembl:ENSG00000156885;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:ncbigene:1349;urn:miriam:ncbigene:1349;urn:miriam:uniprot:P24311;urn:miriam:uniprot:P24311;urn:miriam:ensembl:ENSG00000131174;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc:2291;urn:miriam:refseq:NM_001866;urn:miriam:hgnc:24381;urn:miriam:ensembl:ENSG00000170516;urn:miriam:refseq:NM_130902;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:ncbigene:170712;urn:miriam:ncbigene:170712;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q6YFQ2;urn:miriam:uniprot:Q6YFQ2;urn:miriam:ensembl:ENSG00000160471;urn:miriam:hgnc:24380;urn:miriam:refseq:NM_144613;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:ncbigene:125965;urn:miriam:ncbigene:125965;urn:miriam:hgnc:2265;urn:miriam:ncbigene:1327;urn:miriam:refseq:NM_001861;urn:miriam:ncbigene:1327;urn:miriam:ensembl:ENSG00000131143;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:uniprot:P13073;urn:miriam:uniprot:P13073;urn:miriam:ncbigene:1346;urn:miriam:ncbigene:1346;urn:miriam:hgnc:2287;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:uniprot:P24310;urn:miriam:uniprot:P24310;urn:miriam:refseq:NM_001864;urn:miriam:ensembl:ENSG00000161281;urn:miriam:uniprot:P14406;urn:miriam:uniprot:P14406;urn:miriam:ncbigene:1347;urn:miriam:ncbigene:1347;urn:miriam:hgnc:2288;urn:miriam:ensembl:ENSG00000112695;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:refseq:NM_001865;urn:miriam:ensembl:ENSG00000178741;urn:miriam:ncbigene:9377;urn:miriam:ncbigene:9377;urn:miriam:refseq:NM_004255;urn:miriam:hgnc:2267;urn:miriam:uniprot:P20674;urn:miriam:uniprot:P20674;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc:16232;urn:miriam:ensembl:ENSG00000131055;urn:miriam:uniprot:Q96KJ9;urn:miriam:uniprot:Q96KJ9;urn:miriam:ncbigene:84701;urn:miriam:ncbigene:84701;urn:miriam:refseq:NM_032609;urn:miriam:hgnc:24382;urn:miriam:ensembl:ENSG00000187581;urn:miriam:uniprot:Q7Z4L0;urn:miriam:uniprot:Q7Z4L0;urn:miriam:refseq:NM_182971;urn:miriam:ncbigene:341947;urn:miriam:ncbigene:341947;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc:2285;urn:miriam:ensembl:ENSG00000164919;urn:miriam:uniprot:P09669;urn:miriam:uniprot:P09669;urn:miriam:refseq:NM_004374;urn:miriam:hgnc.symbol:COX6C;urn:miriam:hgnc.symbol:COX6C;urn:miriam:ncbigene:1345;urn:miriam:ncbigene:1345;urn:miriam:ec-code:7.1.1.9;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:ensembl:ENSG00000198804;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:refseq:YP_003024028;urn:miriam:uniprot:P00395;urn:miriam:uniprot:P00395;urn:miriam:hgnc:7419;urn:miriam:ncbigene:4512;urn:miriam:ncbigene:4512"
      hgnc "HGNC_SYMBOL:MT-CO3;HGNC_SYMBOL:COX8A;HGNC_SYMBOL:MT-CO2;HGNC_SYMBOL:COX6A1;HGNC_SYMBOL:COX6B1;HGNC_SYMBOL:COX5B;HGNC_SYMBOL:COX7C;HGNC_SYMBOL:COX6A2;HGNC_SYMBOL:COX7B;HGNC_SYMBOL:COX7B2;HGNC_SYMBOL:COX6B2;HGNC_SYMBOL:COX4I1;HGNC_SYMBOL:COX7A1;HGNC_SYMBOL:COX7A2;HGNC_SYMBOL:COX5A;HGNC_SYMBOL:COX4I2;HGNC_SYMBOL:COX8C;HGNC_SYMBOL:COX6C;HGNC_SYMBOL:MT-CO1"
      map_id "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
      name "complex_space_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
    ]
    graphics [
      x 952.498662714105
      y 1673.7193061254998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1128.7699491538233
      y 860.5783211156104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045275;urn:miriam:ensembl:ENSG00000140740;urn:miriam:refseq:NM_003366;urn:miriam:uniprot:P22695;urn:miriam:uniprot:P22695;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:ncbigene:7385;urn:miriam:ncbigene:7385;urn:miriam:hgnc:12586;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc:30862;urn:miriam:ensembl:ENSG00000127540;urn:miriam:refseq:NM_006830;urn:miriam:ncbigene:10975;urn:miriam:ncbigene:10975;urn:miriam:uniprot:O14957;urn:miriam:uniprot:O14957;urn:miriam:refseq:NM_014402;urn:miriam:ncbigene:27089;urn:miriam:ncbigene:27089;urn:miriam:uniprot:O14949;urn:miriam:uniprot:O14949;urn:miriam:ensembl:ENSG00000164405;urn:miriam:hgnc:29594;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:refseq:NM_003365;urn:miriam:uniprot:P31930;urn:miriam:uniprot:P31930;urn:miriam:ncbigene:7384;urn:miriam:ensembl:ENSG00000010256;urn:miriam:ncbigene:7384;urn:miriam:hgnc:12585;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc:1020;urn:miriam:uniprot:Q9Y276;urn:miriam:uniprot:Q9Y276;urn:miriam:ensembl:ENSG00000074582;urn:miriam:ncbigene:617;urn:miriam:ncbigene:617;urn:miriam:refseq:NM_004328;urn:miriam:ncbigene:100128525;urn:miriam:hgnc:12588;urn:miriam:uniprot:P0C7P4;urn:miriam:uniprot:P0C7P4;urn:miriam:refseq:NG_009458;urn:miriam:ensembl:ENSG00000226085;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:ncbigene:7381;urn:miriam:ncbigene:7381;urn:miriam:ensembl:ENSG00000156467;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:refseq:NM_006294;urn:miriam:uniprot:P14927;urn:miriam:uniprot:P14927;urn:miriam:hgnc:12582;urn:miriam:hgnc:12590;urn:miriam:uniprot:P07919;urn:miriam:uniprot:P07919;urn:miriam:refseq:NM_006004;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:ncbigene:7388;urn:miriam:ncbigene:7388;urn:miriam:ensembl:ENSG00000173660;urn:miriam:refseq:NM_013387;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc:30863;urn:miriam:ncbigene:29796;urn:miriam:ncbigene:29796;urn:miriam:uniprot:Q9UDW1;urn:miriam:uniprot:Q9UDW1;urn:miriam:ensembl:ENSG00000184076"
      hgnc "HGNC_SYMBOL:UQCRC2;HGNC_SYMBOL:UQCR11;HGNC_SYMBOL:UQCRQ;HGNC_SYMBOL:UQCRC1;HGNC_SYMBOL:BCS1L;HGNC_SYMBOL:UQCRFS1P1;HGNC_SYMBOL:UQCRB;HGNC_SYMBOL:UQCRH;HGNC_SYMBOL:UQCR10"
      map_id "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
      name "complex_space_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
    ]
    graphics [
      x 830.1751602193577
      y 1202.6229416692543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_33"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 786.6100669368293
      y 988.8677024261701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ec-code:6.2.1.5;urn:miriam:hgnc:11448;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:refseq:NM_003850;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:ensembl:ENSG00000136143;urn:miriam:uniprot:Q9P2R7;urn:miriam:uniprot:Q9P2R7;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477;urn:miriam:ec-code:6.2.1.5;urn:miriam:ensembl:ENSG00000163541;urn:miriam:hgnc:11449;urn:miriam:ec-code:6.2.1.4;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:refseq:NM_003849;urn:miriam:uniprot:P53597;urn:miriam:uniprot:P53597;urn:miriam:ncbigene:8802;urn:miriam:ncbigene:8802;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:ncbigene:1716;urn:miriam:ncbigene:1716;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:refseq:NM_001318859;urn:miriam:uniprot:Q16854;urn:miriam:uniprot:Q16854;urn:miriam:hgnc:2858;urn:miriam:ensembl:ENSG00000114956;urn:miriam:hgnc.symbol:TK2;urn:miriam:hgnc.symbol:TK2;urn:miriam:ncbigene:7084;urn:miriam:ncbigene:7084;urn:miriam:hgnc:11831;urn:miriam:ensembl:ENSG00000166548;urn:miriam:ec-code:2.7.1.21;urn:miriam:refseq:NM_001172643;urn:miriam:uniprot:O00142;urn:miriam:uniprot:O00142"
      hgnc "HGNC_SYMBOL:SUCLA2;HGNC_SYMBOL:RRM2B;HGNC_SYMBOL:SUCLG1;HGNC_SYMBOL:DGUOK;HGNC_SYMBOL:TK2"
      map_id "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
      name "Mt_minus_dNTP_space_pool"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa52"
      uniprot "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
    ]
    graphics [
      x 1527.1732371427179
      y 2083.225351820972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_85"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 1526.4282337393172
      y 1931.8184211934956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 972.5479778573333
      y 633.1330865996931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 891.1894440731461
      y 605.4688410763943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_000454;urn:miriam:uniprot:P00441;urn:miriam:ensembl:ENSG00000142168;urn:miriam:hgnc:11179;urn:miriam:hgnc.symbol:SOD1;urn:miriam:hgnc.symbol:SOD1;urn:miriam:ncbigene:6647;urn:miriam:ncbigene:6647;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD1"
      map_id "UNIPROT:P00441"
      name "SOD1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa372"
      uniprot "UNIPROT:P00441"
    ]
    graphics [
      x 1011.1908401752721
      y 561.7726910906821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00441"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1035.8210895450848
      y 893.3297612822931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 1097.8900319567806
      y 955.6827344588404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 851.5421844649048
      y 959.9427142622206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29356"
      hgnc "NA"
      map_id "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
      name "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 827.6578485914874
      y 855.1673856541778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_184"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa716"
      uniprot "NA"
    ]
    graphics [
      x 825.8810768329812
      y 1621.7666794393076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 790.171658786599
      y 1522.5456084759455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 1571.4347001937972
      y 1229.4213736100062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009742612; urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "Nsp5"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa693; sa251; sa2241; sa2366"
      uniprot "NA"
    ]
    graphics [
      x 1673.9474834909583
      y 1155.2170663016063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_175"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa694"
      uniprot "NA"
    ]
    graphics [
      x 1582.6943085992125
      y 1108.953995812749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_158"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa655"
      uniprot "NA"
    ]
    graphics [
      x 1889.20410516942
      y 1717.685923785114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 1815.5117309415136
      y 1826.771884701499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 1676.273306296809
      y 2052.376522420094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:ncbigene:1161;urn:miriam:ncbigene:1161;urn:miriam:uniprot:Q13216;urn:miriam:uniprot:Q13216;urn:miriam:hgnc:3439;urn:miriam:refseq:NM_000082;urn:miriam:ensembl:ENSG00000049167;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:ensembl:ENSG00000225830;urn:miriam:refseq:NM_000124;urn:miriam:ncbigene:2074;urn:miriam:ncbigene:2074;urn:miriam:hgnc:3438;urn:miriam:uniprot:P0DP91;urn:miriam:uniprot:P0DP91;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:ec-code:3.6.4.-"
      hgnc "HGNC_SYMBOL:ERCC8;HGNC_SYMBOL:POLG;HGNC_SYMBOL:ERCC6"
      map_id "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
      name "Mt_minus_DNA_space_repair"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
    ]
    graphics [
      x 1577.6246151370624
      y 1898.5677708910816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 153
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_162"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Orf9c"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 8
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_42"
      target_id "UNIPROT:Q9Y6M9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_169"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Orf9c"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_45"
      target_id "UNIPROT:Q9H845"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "superoxide"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 1
    target 15
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P04179"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_64"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 4
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "ADP"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 2
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00846;UNIPROT:Q9UII2"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 1
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PHYSICAL_STIMULATION"
      source_id "H_plus_"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 18
    target 3
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_49"
      target_id "ATP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_161"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 9
    target 21
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Orf9c"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_43"
      target_id "UNIPROT:O15239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_182"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 13
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H845"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BQ95"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 26
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7L592"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 24
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_44"
      target_id "OXPHOS_space_factors"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 17
    target 28
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "glutathione"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 30
    target 28
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P36969"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 31
    target 28
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07203"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 28
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 28
    target 33
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "glutathione_space_disulfide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 19
    target 34
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00846;UNIPROT:Q9UII2"
      target_id "M13_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 34
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_32"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Nsp8"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_41"
      target_id "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
      target_id "M13_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_82"
      target_id "mt_space_DNA_space_replication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_180"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75964"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 45
    target 43
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Nsp6"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 43
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_53"
      target_id "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 17
    target 46
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "hydroxide"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 46
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "superoxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 46
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 46
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mt_space_DNA"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NXH9"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 49
    target 52
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_84"
      target_id "UNIPROT:A0A0C5B5G6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "precursor_space_protein_space_N_minus_terminus_space_binding"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:TOMM37;UNIPROT:O94826;UNIPROT:Q9Y5J7;UNIPROT:Q9NS69"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 56
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 57
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Orf9b"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 58
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99595;UNIPROT:O60830;UNIPROT:O14925"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 59
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P62072;UNIPROT:Q9Y5J7"
      target_id "M13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_48"
      target_id "precursor_space_protein_space_N_minus_terminus_space_binding"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 17
    target 60
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99757"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 60
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "UNIPROT:Q99757"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "e_minus_"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "Q"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_37"
      target_id "e_minus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 48
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mt_space_DNA"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q00059"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 68
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 69
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "damaged_space_mt_space_DNA"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 66
    target 70
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_81"
      target_id "mt_space_mRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_159"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 22
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15239"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 10
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6M9"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mtDNA_space_encoded_space_OXPHOS_space_units"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 27
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "OXPHOS_space_factors"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_29"
      target_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 47
    target 75
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "hydroxide"
      target_id "M13_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 75
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_69"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 16
    target 76
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04179"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 77
    target 76
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NTG7"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 76
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_63"
      target_id "UNIPROT:P04179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 1
    target 78
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 78
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_35"
      target_id "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_168"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 9
    target 80
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Orf9c"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 80
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_46"
      target_id "UNIPROT:Q9BQ95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 1
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 74
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 81
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_57"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 67
    target 82
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00059"
      target_id "M13_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 82
    target 41
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_79"
      target_id "mt_space_DNA_space_replication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 47
    target 83
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "hydroxide"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "Fe2_plus_"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "HO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 83
    target 86
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "Fe3_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_178"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BSF4"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Nsp4"
      target_id "M13_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 88
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_52"
      target_id "UNIPROT:Q99643;UNIPROT:Q9BSF4;UNIPROT:Q9Y584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14521;UNIPROT:Q99643"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "TCA"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 92
    target 94
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_55"
      target_id "UNIPROT:Q99643;UNIPROT:O14521"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 70
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mt_space_mRNA"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 97
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 38
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 95
    target 73
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_86"
      target_id "mtDNA_space_encoded_space_OXPHOS_space_units"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 17
    target 98
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 84
    target 98
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "Fe2_plus_"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 98
    target 85
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "HO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 98
    target 86
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "Fe3_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 98
    target 47
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "hydroxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 33
    target 99
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "glutathione_space_disulfide"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 100
    target 99
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 101
    target 99
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00390"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 99
    target 29
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "glutathione"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 99
    target 102
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "NADP(_plus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 94
    target 103
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99643;UNIPROT:O14521"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 65
    target 103
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "Q"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 74
    target 103
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 103
    target 91
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 1
    target 105
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 105
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_31"
      target_id "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 14
    target 106
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "superoxide"
      target_id "M13_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 106
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_67"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 107
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "paraquat_space_dication"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 14
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "superoxide"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 74
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_78"
      target_id "paraquat"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 59
    target 110
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62072;UNIPROT:Q9Y5J7"
      target_id "M13_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 90
    target 110
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Nsp4"
      target_id "M13_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 110
    target 59
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_51"
      target_id "UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 17
    target 111
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P04040"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 111
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_30"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_167"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 115
    target 114
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "Nsp7"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 114
    target 26
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_47"
      target_id "UNIPROT:Q7L592"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 63
    target 116
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "e_minus_"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 117
    target 116
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P99999"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 116
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_38"
      target_id "e_minus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 48
    target 118
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mt_space_DNA"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 119
    target 118
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "mt_space_DNA_space_damage"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 118
    target 69
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_39"
      target_id "damaged_space_mt_space_DNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 14
    target 120
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "superoxide"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 86
    target 120
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "Fe3_plus_"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 120
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 120
    target 84
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "Fe2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 122
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_73"
      target_id "superoxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 91
    target 123
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14521;UNIPROT:Q99643"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 77
    target 123
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NTG7"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 27
    target 123
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "OXPHOS_space_factors"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 123
    target 91
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_54"
      target_id "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 61
    target 124
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99757"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 100
    target 124
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NNW7"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 124
    target 61
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "UNIPROT:Q99757"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 124
    target 102
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "NADP(_plus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "NADH"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 74
    target 127
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 127
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "NAD_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 127
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 127
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "e_minus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 1
    target 128
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 128
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_34"
      target_id "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_185"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 73
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mtDNA_space_encoded_space_OXPHOS_space_units"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 27
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "OXPHOS_space_factors"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_59"
      target_id "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 121
    target 132
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 132
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_65"
      target_id "superoxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
      target_id "M13_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 134
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_33"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
      target_id "M13_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 136
    target 41
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_85"
      target_id "mt_space_DNA_space_replication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 17
    target 137
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 137
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_71"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 14
    target 138
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "superoxide"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 1
    target 138
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 139
    target 138
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00441"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 138
    target 17
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_60"
      target_id "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 14
    target 140
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "superoxide"
      target_id "M13_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 140
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_66"
      target_id "ROS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 109
    target 141
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "paraquat"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 121
    target 141
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 141
    target 107
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "paraquat_space_dication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 141
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "superoxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 121
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 117
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P99999"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 1
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 104
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 133
    target 142
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 142
    target 143
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 142
    target 117
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "UNIPROT:P99999"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 142
    target 65
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "Q"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 142
    target 1
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_184"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 73
    target 145
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "mtDNA_space_encoded_space_OXPHOS_space_units"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 27
    target 145
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "OXPHOS_space_factors"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 145
    target 133
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_58"
      target_id "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 51
    target 146
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NXH9"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 147
    target 146
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "Nsp5"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 146
    target 148
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_50"
      target_id "M13_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_158"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 119
    target 150
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "mt_space_DNA_space_damage"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 41
    target 150
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "mt_space_DNA_space_replication"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 150
    target 48
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_80"
      target_id "mt_space_DNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 69
    target 151
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "damaged_space_mt_space_DNA"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 67
    target 151
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q00059"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 135
    target 151
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 151
    target 48
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_40"
      target_id "mt_space_DNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
