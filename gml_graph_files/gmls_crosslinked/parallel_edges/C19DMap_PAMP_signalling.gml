# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; WP4880; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59636;urn:miriam:reactome:R-COV-9689378;urn:miriam:reactome:R-COV-9694649; urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59636"
      name "9b; Orf9; Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1900; e9876; sa362; sa77; sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1203.3309003359855
      y 619.0143220936686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59636"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NYK1; urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284; urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "NA; HGNC_SYMBOL:TLR7"
      map_id "UNIPROT:Q9NYK1"
      name "TLR7; TLR7:ssRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "d516b; sa55; sa430; csa90"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 1087.3491400895782
      y 160.53554786473705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NYK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; WP5038; WP5039; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q2819370; NA"
      hgnc "NA"
      map_id "dsRNA"
      name "dsRNA"
      node_subtype "RNA; UNKNOWN"
      node_type "species"
      org_id "a90b0; ff0ff; cb56c; sa6; sa26; sa27; sa456; sa93; sa82"
      uniprot "NA"
    ]
    graphics [
      x 533.5035759065752
      y 1284.6977406326607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:PAMP signalling; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P19525; urn:miriam:uniprot:P19525;urn:miriam:wikidata:Q2819370; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "NA; HGNC_SYMBOL:EIF2AK2"
      map_id "UNIPROT:P19525"
      name "PKR; d477c; EIF2AK2:dsRNA; EIF2AK2"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c0e50; d477c; csa96; sa454; path_0_sa287; path_0_sa210"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 958.184720863963
      y 1585.8471107838166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19525"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4877; WP4880; C19DMap:JNK pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416; urn:miriam:uniprot:P59633; urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "UNIPROT:P59633"
      name "ee4e9; 3b; Orf3b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "ee4e9; e6060; sa75; sa356; sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 390.02162314255213
      y 1727.7720103050187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4868; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455; urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "NA; HGNC_SYMBOL:TLR3"
      map_id "UNIPROT:O15455"
      name "TLR3; TLR3_underscore_TRIF; TLR3_underscore_TRIF_underscore_RIPK1; TLR3:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "ab922; csa37; sa239; csa38; sa5; csa88; sa45; sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 198.67582653302793
      y 1063.771128044048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15455"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4880; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:wikidata:Q89457519; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58761;urn:miriam:ncbiprotein:YP_009724397.2; urn:miriam:ncbiprotein:1798174255"
      hgnc "NA"
      map_id "N"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e932d; sa140; sa352; sa381"
      uniprot "NA"
    ]
    graphics [
      x 1365.7036633109365
      y 1461.3077509930745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5039; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q14790; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "NA; HGNC_SYMBOL:CASP8"
      map_id "UNIPROT:Q14790"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc51a; sa459; sa390; sa14; sa13"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 1003.4076104943746
      y 995.7410544545792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14790"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:APO40582;urn:miriam:pubmed:16845612;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "UNIPROT:M"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193; sa134; sa226; sa232; sa408; sa359; sa353; sa403; sa42"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 612.90358073277
      y 1234.6739440807082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:M"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3; urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761; urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3; urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "UNIPROT:Nsp3"
      name "Nsp3; Nsp3:Nsp4:Nsp6"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa162; csa39; sa123; sa169; sa2222; sa357; sa361; sa349; sa354; sa363"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 427.3573777807288
      y 1389.0031126546191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 19
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "UNIPROT:Q14653"
      name "IRF3_underscore_homodimer; IRF3; IFNB1_space_expression_space_complex; p38_minus_NFkB"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa46; sa121; sa286; csa45; sa120; sa348; sa119; sa156; sa157; sa379; sa256; csa8; sa85; sa84; sa89; csa41; sa87; sa88; sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 737.9580537341496
      y 1594.1063760730253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528; urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "UNIPROT:Q14164"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa119; sa116; sa118; sa427; sa491"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 638.0441313206543
      y 1123.89435047094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:Q9UHD2"
      name "TBK1; STING:TBK1"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa61; sa60; sa426; sa117; sa490; sa154; sa80; csa20; csa5; sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 328.6670090156098
      y 1663.9164080618268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:NLRP3 inflammasome activation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:Q7Z434"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151; sa156; sa127; sa128; sa102; sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 706.4471162109207
      y 919.3332370952639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:YP_009725310; urn:miriam:ncbiprotein:1802476818"
      hgnc "NA"
      map_id "Nsp15"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa129; sa227; sa316; sa2205; sa415; sa416"
      uniprot "NA"
    ]
    graphics [
      x 663.0992454485191
      y 1270.526766958914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206; urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "UNIPROT:P19838;UNIPROT:Q04206"
      name "p50_underscore_p65; NFKB; P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21; csa80; csa103; csa7"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1400.9047393724036
      y 1334.4246934923335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838;UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TANK"
      map_id "UNIPROT:Q92844"
      name "TANK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209; sa492"
      uniprot "UNIPROT:Q92844"
    ]
    graphics [
      x 1047.3538901038064
      y 1323.0834507142406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "UNIPROT:Q13114"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82; sa83; sa493; sa11; sa116; sa99; sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1046.031473529987
      y 1235.3392025230664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468; urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "UNIPROT:Q99836"
      name "MYD88_underscore_TRAM; MYD88"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa20; sa57; sa77; sa433; sa143; sa100; sa49; sa144"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 858.7107312129751
      y 434.7999512076889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99836"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1; urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "UNIPROT:Q9BYX4"
      name "IFIH1; IFIH1:dsRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa198; sa154; csa93; sa439; sa78"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 573.0747659347403
      y 1086.2236479831174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYX4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58; urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "UNIPROT:O95786"
      name "DDX58; DDX58:dsRNA; RIG_minus_I:dsRNA; RIG_minus_I"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa200; sa155; sa23; csa92; sa124; sa440; csa37; csa36; sa99"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 851.8762207157351
      y 1390.1753527257324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95786"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:TRIM25"
      map_id "UNIPROT:Q14258"
      name "TRIM25"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201; sa122"
      uniprot "UNIPROT:Q14258"
    ]
    graphics [
      x 1260.196340471466
      y 1554.7302636557492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481"
      hgnc "HGNC_SYMBOL:RNF135"
      map_id "UNIPROT:Q8IUD6"
      name "RNF135"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202; sa123"
      uniprot "UNIPROT:Q8IUD6"
    ]
    graphics [
      x 1182.806682047847
      y 1629.6377300648783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUD6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "UNIPROT:Q16539"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64; sa368; sa445; sa49; sa50; path_0_sa42; path_0_sa43; path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1040.090085946074
      y 1752.8927691653603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25; urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "UNIPROT:O43318"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86; sa85; sa367; sa435"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 739.9944484445218
      y 1780.7224279170712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "UNIPROT:Q92985"
      name "IRF7_underscore_homodimer; IRF7"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa43; csa44; sa281; sa211; sa213; sa255; sa347; sa120"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 776.6668121817597
      y 883.8144668779368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92985"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6; urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "UNIPROT:Q9Y4K3"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59; sa58; sa428; sa265; sa337; sa422; sa129; sa135; sa101; sa38"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 528.6483976498145
      y 961.1879914125021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4K3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "UNIPROT:Q96J02"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa204; sa203; sa436; sa405"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 874.2441655536703
      y 610.2206780216823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96J02"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS; urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOS"
      map_id "UNIPROT:P05412;UNIPROT:P01100"
      name "AP_minus_1; AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10; csa79; csa104"
      uniprot "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      x 864.990521247544
      y 1336.3559669975389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05412;UNIPROT:P01100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228; urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUN"
      map_id "UNIPROT:P01100;UNIPROT:P05412"
      name "AP_minus_1; AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35; csa95"
      uniprot "UNIPROT:P01100;UNIPROT:P05412"
    ]
    graphics [
      x 969.0408701693889
      y 1536.9934151310895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01100;UNIPROT:P05412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:YP_009724396.1; urn:miriam:ncbiprotein:1796318604"
      hgnc "NA"
      map_id "Orf8"
      name "Orf8"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa27; sa355"
      uniprot "NA"
    ]
    graphics [
      x 516.0042793784771
      y 1738.2646949745404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:RELA"
      map_id "UNIPROT:Q04206"
      name "RELA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa268; sa482"
      uniprot "UNIPROT:Q04206"
    ]
    graphics [
      x 1910.0711378149372
      y 1317.298570428264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P19838"
      name "NFKB1; p105; p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269; sa483; sa81; sa151; sa84; sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1914.4187544572565
      y 1188.176343717919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:JNK pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7; urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "UNIPROT:O14733"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44; sa5; sa372; sa453"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 861.4642928054697
      y 2124.711139972333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14733"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:JNK pathway; C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416; urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "UNIPROT:P45985"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69; sa4; sa371; sa452"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 821.6551769748909
      y 2055.4523710018666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45985"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309;PUBMED:28148787"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1153.3788571974842
      y 1485.1226106895228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 1052.504551489145
      y 577.2877662055224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re253"
      uniprot "NA"
    ]
    graphics [
      x 240.329829549897
      y 1545.8308042276792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528;urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TANK"
      map_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83; csa82"
      uniprot "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    ]
    graphics [
      x 337.6020537624995
      y 1410.0320434883133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKBIA;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
      name "NFKB:NFKBIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa102"
      uniprot "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      x 1518.2286197070212
      y 1241.649899692447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:16143815"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_67"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re280"
      uniprot "NA"
    ]
    graphics [
      x 1549.1550016440233
      y 1372.541436437605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "UNIPROT:P25963"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa475; sa481; sa65; sa64"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 1706.3286281720966
      y 1299.278669892637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25963"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 920.752112991596
      y 1043.8313002608988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "transcription_space_of_space_proinflammatory_space_proteins"
      name "transcription_space_of_space_proinflammatory_space_proteins"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa480"
      uniprot "NA"
    ]
    graphics [
      x 1001.9931560350599
      y 1267.628405778299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcription_space_of_space_proinflammatory_space_proteins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:15383579"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re255"
      uniprot "NA"
    ]
    graphics [
      x 638.3789817305001
      y 649.4758451364748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6112;urn:miriam:uniprot:P51617;urn:miriam:uniprot:P51617;urn:miriam:ec-code:2.7.11.1;urn:miriam:ensembl:ENSG00000184216;urn:miriam:refseq:NM_001025242;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654"
      hgnc "HGNC_SYMBOL:IRAK1"
      map_id "UNIPROT:P51617"
      name "IRAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:P51617"
    ]
    graphics [
      x 747.2901169791171
      y 578.9659812241985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NWZ3;urn:miriam:uniprot:Q9NWZ3;urn:miriam:refseq:NM_001114182;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:51135;urn:miriam:ncbigene:51135;urn:miriam:hgnc:17967;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:ensembl:ENSG00000198001"
      hgnc "HGNC_SYMBOL:IRAK4"
      map_id "UNIPROT:Q9NWZ3"
      name "IRAK4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa333"
      uniprot "UNIPROT:Q9NWZ3"
    ]
    graphics [
      x 754.1549625781362
      y 662.9826479101486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NWZ3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:uniprot:Q9NWZ3;urn:miriam:uniprot:Q9NWZ3;urn:miriam:refseq:NM_001114182;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:51135;urn:miriam:ncbigene:51135;urn:miriam:hgnc:17967;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc:6112;urn:miriam:uniprot:P51617;urn:miriam:uniprot:P51617;urn:miriam:ec-code:2.7.11.1;urn:miriam:ensembl:ENSG00000184216;urn:miriam:refseq:NM_001025242;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:IRAK4;HGNC_SYMBOL:IRAK1;HGNC_SYMBOL:MYD88"
      map_id "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
      name "MYD88:IRAK:TRAF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
    ]
    graphics [
      x 505.51026476053516
      y 725.0793013308082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:17761676"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 452.22333715262687
      y 1609.5940922196492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_41"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re250"
      uniprot "NA"
    ]
    graphics [
      x 328.92491669903086
      y 1176.0152902808795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re176"
      uniprot "NA"
    ]
    graphics [
      x 369.5129976379428
      y 819.6925705078129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000177889;urn:miriam:ec-code:2.3.2.23;urn:miriam:hgnc.symbol:UBE2N;urn:miriam:hgnc.symbol:UBE2N;urn:miriam:ncbigene:7334;urn:miriam:ncbigene:7334;urn:miriam:refseq:NM_003348;urn:miriam:hgnc:12492;urn:miriam:uniprot:P61088;urn:miriam:uniprot:P61088"
      hgnc "HGNC_SYMBOL:UBE2N"
      map_id "UNIPROT:P61088"
      name "UBE2N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa331"
      uniprot "UNIPROT:P61088"
    ]
    graphics [
      x 255.290458890516
      y 761.4435061748289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61088"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:uniprot:O00463;urn:miriam:uniprot:O00463;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ncbigene:7188;urn:miriam:ncbigene:7188;urn:miriam:refseq:NM_004619;urn:miriam:hgnc:12035;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:TRAF2;HGNC_SYMBOL:TRAF5;HGNC_SYMBOL:TRAF6"
      map_id "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
      name "MAVS:TRAF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 406.7325148327566
      y 695.2743689072463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 605.6863441207026
      y 899.3682309668259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKG"
      map_id "UNIPROT:Q9Y6K9"
      name "IKBKG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa484"
      uniprot "UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 532.9398137108307
      y 1603.864226592326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6K9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 650.2520366971004
      y 1506.9907227956971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147"
      hgnc "HGNC_SYMBOL:CHUK"
      map_id "UNIPROT:O15111"
      name "CHUK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa485"
      uniprot "UNIPROT:O15111"
    ]
    graphics [
      x 667.4718229338129
      y 1640.175734400091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:IKBKB"
      map_id "UNIPROT:O14920"
      name "IKBKB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa486"
      uniprot "UNIPROT:O14920"
    ]
    graphics [
      x 603.3764663820277
      y 1605.3843885627352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14920"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB"
      map_id "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
      name "IKK_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa75"
      uniprot "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
    ]
    graphics [
      x 750.365990509398
      y 1294.6258621588172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 873.3172301520196
      y 1616.303101837491
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK9"
      map_id "UNIPROT:P45983;UNIPROT:P45984"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:P45983;UNIPROT:P45984"
    ]
    graphics [
      x 931.299918886053
      y 1849.1778651479344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983;UNIPROT:P45984"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TANK;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE"
      map_id "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa105; csa89"
      uniprot "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      x 682.7569838269798
      y 1356.4749859417545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:19380580;PUBMED:27164085"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 582.4487708828856
      y 1337.5381242688677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ncbigene:8717;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:TRADD"
      map_id "UNIPROT:Q7Z434;UNIPROT:Q15628"
      name "MAVS:TRADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa86"
      uniprot "UNIPROT:Q7Z434;UNIPROT:Q15628"
    ]
    graphics [
      x 866.9563001002412
      y 1176.4736517166857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434;UNIPROT:Q15628"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR8"
      map_id "UNIPROT:Q9NR97"
      name "TLR8; TLR8:ssRNA"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa429; csa91"
      uniprot "UNIPROT:Q9NR97"
    ]
    graphics [
      x 1187.7990909478317
      y 309.9457120729428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NR97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 1291.5660624049244
      y 215.37682297613856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "ssRNA"
      name "ssRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14; sa42"
      uniprot "NA"
    ]
    graphics [
      x 1306.4676759775487
      y 90.58348354257441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ssRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re241"
      uniprot "NA"
    ]
    graphics [
      x 848.7684191725026
      y 783.1571995785127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_139"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 986.5918370182986
      y 786.4549330733082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 1127.6432802776305
      y 928.2491618040586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:RIPK1;HGNC_SYMBOL:FADD"
      map_id "UNIPROT:Q13546;UNIPROT:Q13158"
      name "RIPK1:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:Q13546;UNIPROT:Q13158"
    ]
    graphics [
      x 1230.4554963732526
      y 909.331864597666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13546;UNIPROT:Q13158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re254"
      uniprot "NA"
    ]
    graphics [
      x 432.96757508885185
      y 1257.0159456110678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:5606;urn:miriam:ncbigene:5606;urn:miriam:ec-code:2.7.12.2;urn:miriam:uniprot:P46734;urn:miriam:uniprot:P46734;urn:miriam:hgnc:6843;urn:miriam:refseq:NM_145109;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:ensembl:ENSG00000034152"
      hgnc "HGNC_SYMBOL:MAP2K3"
      map_id "UNIPROT:P46734"
      name "MAP2K3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa369; sa451"
      uniprot "UNIPROT:P46734"
    ]
    graphics [
      x 1036.2259299892537
      y 1946.3935703905036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P46734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re271"
      uniprot "NA"
    ]
    graphics [
      x 876.6262096136802
      y 1905.6837270551805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737"
      hgnc "HGNC_SYMBOL:FADD;HGNC_SYMBOL:RIPK1"
      map_id "UNIPROT:Q13158;UNIPROT:Q13546"
      name "RIPK1:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:Q13158;UNIPROT:Q13546"
    ]
    graphics [
      x 1223.7174934920417
      y 1028.4050468882826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13158;UNIPROT:Q13546"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 1093.9769192399376
      y 1043.7503787676476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ensembl:ENSG00000108984;urn:miriam:ncbigene:5608;urn:miriam:ncbigene:5608;urn:miriam:uniprot:P52564;urn:miriam:uniprot:P52564;urn:miriam:refseq:NM_002758;urn:miriam:hgnc:6846;urn:miriam:hgnc.symbol:MAP2K6;urn:miriam:hgnc.symbol:MAP2K6"
      hgnc "HGNC_SYMBOL:MAP2K6"
      map_id "UNIPROT:P52564"
      name "MAP2K6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa370; sa450"
      uniprot "UNIPROT:P52564"
    ]
    graphics [
      x 1147.3525916698695
      y 1773.65194595985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52564"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re270"
      uniprot "NA"
    ]
    graphics [
      x 950.2461897629382
      y 1753.0151062908085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_84"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 832.1732412703658
      y 1550.562858500175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re291"
      uniprot "NA"
    ]
    graphics [
      x 882.7055331290082
      y 1262.7623778391082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re257"
      uniprot "NA"
    ]
    graphics [
      x 1183.4120469742675
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872;PUBMED:19366914;PUBMED:694009;PUBMED:17705188"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re279"
      uniprot "NA"
    ]
    graphics [
      x 840.2795213195067
      y 1060.952152251899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CASP10;urn:miriam:refseq:NM_032977;urn:miriam:hgnc.symbol:CASP10;urn:miriam:uniprot:Q92851;urn:miriam:uniprot:Q92851;urn:miriam:ncbigene:843;urn:miriam:ncbigene:843;urn:miriam:ec-code:3.4.22.63;urn:miriam:hgnc:1500;urn:miriam:ensembl:ENSG00000003400"
      hgnc "HGNC_SYMBOL:CASP10"
      map_id "UNIPROT:Q92851"
      name "CASP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa389; sa463"
      uniprot "UNIPROT:Q92851"
    ]
    graphics [
      x 981.892434283082
      y 913.2415546365717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92851"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720; urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
      name "IKK_space_Complex; NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa99; csa12; csa20"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 1154.021215180041
      y 1112.5853138374825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:20303872;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 615.7044892263066
      y 1406.6315023471384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF5;urn:miriam:uniprot:O00463;urn:miriam:uniprot:O00463;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ncbigene:7188;urn:miriam:ncbigene:7188;urn:miriam:refseq:NM_004619;urn:miriam:hgnc:12035"
      hgnc "HGNC_SYMBOL:TRAF5"
      map_id "UNIPROT:O00463"
      name "TRAF5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131"
      uniprot "UNIPROT:O00463"
    ]
    graphics [
      x 600.7162622575752
      y 571.6490328073082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00463"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re242"
      uniprot "NA"
    ]
    graphics [
      x 740.7804319632365
      y 475.69966418808383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_136"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa404"
      uniprot "NA"
    ]
    graphics [
      x 846.3448693480614
      y 489.55703581737737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKBIA"
      map_id "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
      name "NFKB:NFKBIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
    ]
    graphics [
      x 1624.3458089441642
      y 1181.1248538789478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1419.3843881621435
      y 1143.251514461718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re277"
      uniprot "NA"
    ]
    graphics [
      x 1127.5931431148842
      y 831.433890069125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:27164085"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re265"
      uniprot "NA"
    ]
    graphics [
      x 432.47361314402656
      y 1144.8357160585904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re282"
      uniprot "NA"
    ]
    graphics [
      x 953.4098057074768
      y 1184.4804617262532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re290"
      uniprot "NA"
    ]
    graphics [
      x 351.80325135267174
      y 1542.674530258569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1783.0424570027042
      y 1147.587024124564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "ubiquitin_minus_proteasome_space_complex"
      name "ubiquitin_minus_proteasome_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa87"
      uniprot "NA"
    ]
    graphics [
      x 1726.5482590191789
      y 1043.2444861894107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquitin_minus_proteasome_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_98"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa146"
      uniprot "NA"
    ]
    graphics [
      x 1811.4030715588137
      y 1020.4048967613453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re287"
      uniprot "NA"
    ]
    graphics [
      x 1805.6456327593633
      y 1252.0812265540328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 549.7362203776341
      y 1682.8482312609908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "UNIPROT:Q8IUC6"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa418; sa91; sa419; sa94; sa48"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 141.51152788555646
      y 1101.0510768149873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUC6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re249"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1021.2948480202158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 730.8869244458597
      y 1131.5909622299293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re283"
      uniprot "NA"
    ]
    graphics [
      x 1220.4933183850465
      y 1299.9164705569797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 525.384025815446
      y 1393.1662268733007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737"
      hgnc "HGNC_SYMBOL:TICAM1;HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:RIPK1"
      map_id "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
      name "TICAM1:TRAF3:TRAF6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
    ]
    graphics [
      x 338.33783964207385
      y 1272.0624756405173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re252"
      uniprot "NA"
    ]
    graphics [
      x 463.6203989861481
      y 1330.388895399297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 1024.8997541697715
      y 300.6969566206634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_32"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 885.3996907549217
      y 1659.1055816746693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re273"
      uniprot "NA"
    ]
    graphics [
      x 791.9646072629446
      y 1973.2229937288766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_68"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re281"
      uniprot "NA"
    ]
    graphics [
      x 762.6241495819078
      y 1228.1006153727421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_71"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re284"
      uniprot "NA"
    ]
    graphics [
      x 942.7060400220059
      y 1462.697326179693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:28484023;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 501.8032458881358
      y 1197.6620635131092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:28484023"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 789.386668015081
      y 1376.9161351991208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re251"
      uniprot "NA"
    ]
    graphics [
      x 316.8863722008126
      y 1069.8451112651128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 778.0473100093657
      y 1021.1532660350633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ncbigene:8717;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc.symbol:TRADD; urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:TRADD"
      map_id "UNIPROT:Q15628"
      name "TRADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa400; sa44"
      uniprot "UNIPROT:Q15628"
    ]
    graphics [
      x 823.5861593230973
      y 1137.7513457671694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15628"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_33"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 679.4325883738913
      y 761.2788165271227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re269"
      uniprot "NA"
    ]
    graphics [
      x 1150.5414912035537
      y 1885.0564352950678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re272"
      uniprot "NA"
    ]
    graphics [
      x 724.4268486028989
      y 1938.8826624083533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:PAMP signalling; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032; urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "UNIPROT:Q12933"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa130; path_0_sa119; path_0_sa118; path_0_sa204"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 443.6381919725003
      y 784.0104783646547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q12933"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 555.7615465204258
      y 778.1338376212339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_81"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1464.561888229558
      y 1431.2859058598674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re286"
      uniprot "NA"
    ]
    graphics [
      x 899.9929447655632
      y 1430.4992401541324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK9;HGNC_SYMBOL:MAPK8"
      map_id "UNIPROT:P45984;UNIPROT:P45983"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa77"
      uniprot "UNIPROT:P45984;UNIPROT:P45983"
    ]
    graphics [
      x 1093.6562833076493
      y 2082.825631617122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45984;UNIPROT:P45983"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 960.1516032697101
      y 2055.976627559101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re243"
      uniprot "NA"
    ]
    graphics [
      x 744.0962613042609
      y 771.1472666670095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_138"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa406"
      uniprot "NA"
    ]
    graphics [
      x 879.9392504306237
      y 721.1148341046145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 128
    source 21
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 23
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "UNIPROT:Q8IUD6"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 22
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "UNIPROT:Q14258"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 23
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8IUD6"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 22
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14258"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 7
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "N"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 21
    target 36
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95786"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 36
    target 21
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_79"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 28
    target 37
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96J02"
      target_id "M111_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 1
    target 37
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59636"
      target_id "M111_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 37
    target 28
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_52"
      target_id "UNIPROT:Q96J02"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 13
    target 38
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M111_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
      target_id "M111_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 38
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_44"
      target_id "UNIPROT:Q9UHD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
      target_id "M111_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 7
    target 41
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "N"
      target_id "M111_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 41
    target 16
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_67"
      target_id "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_67"
      target_id "UNIPROT:P25963"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 26
    target 43
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M111_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_72"
      target_id "transcription_space_of_space_proinflammatory_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 19
    target 45
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51617"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 47
    target 45
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NWZ3"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 27
    target 45
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 45
    target 48
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_46"
      target_id "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 11
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 13
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UHD2"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 10
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 5
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "UNKNOWN_INHIBITION"
      source_id "UNIPROT:P59633"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 31
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "UNKNOWN_INHIBITION"
      source_id "Orf8"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 49
    target 11
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_30"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 6
    target 50
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15455"
      target_id "M111_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 3
    target 50
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "M111_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 50
    target 6
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_41"
      target_id "UNIPROT:O15455"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 27
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P61088"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 48
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61088"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 48
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 53
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 51
    target 27
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_35"
      target_id "UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 26
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 12
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14164"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 48
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 54
    target 26
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_31"
      target_id "UNIPROT:Q92985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K9"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15111"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 58
    target 56
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14920"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 56
    target 59
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_75"
      target_id "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 30
    target 60
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01100;UNIPROT:P05412"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 24
    target 60
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q16539"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P45983;UNIPROT:P45984"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 60
    target 29
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_55"
      target_id "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7Z434;UNIPROT:Q15628"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 10
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 9
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 63
    target 39
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_76"
      target_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NR97"
      target_id "M111_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "ssRNA"
      target_id "M111_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_47"
      target_id "UNIPROT:Q9NR97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 14
    target 68
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M111_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 28
    target 68
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96J02"
      target_id "M111_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_37"
      target_id "M111_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 8
    target 70
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14790"
      target_id "M111_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13546;UNIPROT:Q13158"
      target_id "M111_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 70
    target 8
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_64"
      target_id "UNIPROT:Q14790"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 12
    target 72
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14164"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 39
    target 72
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 10
    target 72
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 72
    target 12
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_45"
      target_id "UNIPROT:Q14164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P46734"
      target_id "M111_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 25
    target 74
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43318"
      target_id "M111_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_59"
      target_id "UNIPROT:P46734"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13158;UNIPROT:Q13546"
      target_id "M111_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 64
    target 76
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7Z434;UNIPROT:Q15628"
      target_id "M111_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 76
    target 71
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_63"
      target_id "UNIPROT:Q13546;UNIPROT:Q13158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P52564"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 4
    target 78
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P19525"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 25
    target 78
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43318"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_58"
      target_id "UNIPROT:P52564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 11
    target 79
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 79
    target 11
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_84"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 18
    target 80
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 17
    target 80
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92844"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 12
    target 80
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14164"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 80
    target 62
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_78"
      target_id "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 2
    target 81
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NYK1"
      target_id "M111_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 67
    target 81
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "ssRNA"
      target_id "M111_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 81
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_48"
      target_id "UNIPROT:Q9NYK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 59
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 27
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 8
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14790"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 9
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q92851"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 82
    target 84
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_66"
      target_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 3
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 21
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95786"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 9
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 10
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 85
    target 21
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_29"
      target_id "UNIPROT:O95786"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00463"
      target_id "M111_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 28
    target 87
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96J02"
      target_id "M111_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_38"
      target_id "M111_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
      target_id "M111_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 84
    target 90
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
      target_id "M111_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 90
    target 40
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_36"
      target_id "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 83
    target 91
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92851"
      target_id "M111_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 71
    target 91
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13546;UNIPROT:Q13158"
      target_id "M111_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 91
    target 83
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_65"
      target_id "UNIPROT:Q92851"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 27
    target 92
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 10
    target 92
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Nsp3"
      target_id "M111_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 92
    target 27
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_54"
      target_id "UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 29
    target 93
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05412;UNIPROT:P01100"
      target_id "M111_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 93
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_69"
      target_id "transcription_space_of_space_proinflammatory_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 13
    target 94
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M111_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 39
    target 94
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
      target_id "M111_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 94
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_77"
      target_id "UNIPROT:Q9UHD2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 42
    target 95
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25963"
      target_id "M111_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "ubiquitin_minus_proteasome_space_complex"
      target_id "M111_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 95
    target 97
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_82"
      target_id "M111_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 42
    target 98
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25963"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 32
    target 98
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 33
    target 98
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 98
    target 89
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_74"
      target_id "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 11
    target 99
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 13
    target 99
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UHD2"
      target_id "M111_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 99
    target 11
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_83"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IUC6"
      target_id "M111_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 6
    target 101
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O15455"
      target_id "M111_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_40"
      target_id "UNIPROT:Q8IUC6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 14
    target 102
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 21
    target 102
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95786"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 20
    target 102
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYX4"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 102
    target 14
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_80"
      target_id "UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 16
    target 103
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M111_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 103
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_70"
      target_id "transcription_space_of_space_proinflammatory_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 25
    target 104
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43318"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 27
    target 104
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 104
    target 25
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_50"
      target_id "UNIPROT:O43318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 62
    target 106
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 9
    target 106
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 106
    target 39
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_43"
      target_id "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 19
    target 107
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99836"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 2
    target 107
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9NYK1"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 65
    target 107
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9NR97"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 107
    target 19
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_49"
      target_id "UNIPROT:Q99836"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 11
    target 108
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 108
    target 11
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_32"
      target_id "UNIPROT:Q14653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 34
    target 109
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14733"
      target_id "M111_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 25
    target 109
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43318"
      target_id "M111_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 109
    target 34
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_61"
      target_id "UNIPROT:O14733"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 29
    target 110
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05412;UNIPROT:P01100"
      target_id "M111_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 110
    target 29
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_68"
      target_id "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 11
    target 111
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 111
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_71"
      target_id "transcription_space_of_space_proinflammatory_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 3
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 20
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYX4"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 9
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 10
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Nsp3"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 15
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 112
    target 20
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_34"
      target_id "UNIPROT:Q9BYX4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 3
    target 113
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 4
    target 113
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19525"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 15
    target 113
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "Nsp15"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 113
    target 4
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_62"
      target_id "UNIPROT:P19525"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 100
    target 114
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IUC6"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 27
    target 114
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 100
    target 114
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IUC6"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 114
    target 105
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_42"
      target_id "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 14
    target 115
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M111_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15628"
      target_id "M111_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 115
    target 64
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_53"
      target_id "UNIPROT:Q7Z434;UNIPROT:Q15628"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 26
    target 117
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92985"
      target_id "M111_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 117
    target 26
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_33"
      target_id "UNIPROT:Q92985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 24
    target 118
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16539"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 77
    target 118
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P52564"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 73
    target 118
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P46734"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 118
    target 24
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_57"
      target_id "UNIPROT:Q16539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 35
    target 119
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45985"
      target_id "M111_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 25
    target 119
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43318"
      target_id "M111_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 119
    target 35
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_60"
      target_id "UNIPROT:P45985"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q12933"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 86
    target 121
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00463"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 27
    target 121
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 14
    target 121
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 121
    target 53
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_51"
      target_id "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 16
    target 122
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M111_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 122
    target 16
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_81"
      target_id "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 11
    target 123
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M111_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 123
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_73"
      target_id "transcription_space_of_space_proinflammatory_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45984;UNIPROT:P45983"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 34
    target 125
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O14733"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 35
    target 125
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P45985"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 125
    target 61
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_56"
      target_id "UNIPROT:P45983;UNIPROT:P45984"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 27
    target 126
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y4K3"
      target_id "M111_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 28
    target 126
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96J02"
      target_id "M111_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_39"
      target_id "M111_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
