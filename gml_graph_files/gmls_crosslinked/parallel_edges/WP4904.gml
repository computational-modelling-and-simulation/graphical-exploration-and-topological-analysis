# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000267690"
      hgnc "NA"
      map_id "LDLRAD4_minus_AS1"
      name "LDLRAD4_minus_AS1"
      node_subtype "RNA"
      node_type "species"
      org_id "c9042"
      uniprot "NA"
    ]
    graphics [
      x 233.51009557425016
      y 927.6332957626441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LDLRAD4_minus_AS1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32111819"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfb4f20fd"
      uniprot "NA"
    ]
    graphics [
      x 229.49308377763367
      y 808.0093811982524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "LDLRAD4"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "e9bfd; ca96b; d98b8"
      uniprot "NA"
    ]
    graphics [
      x 253.53363611029894
      y 662.2325805017249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LDLRAD4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:31819986"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idfdf4dec2"
      uniprot "NA"
    ]
    graphics [
      x 115.68084802002915
      y 563.6425913532357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:mirbase.mature:MIMAT0000075"
      hgnc "NA"
      map_id "hsa_minus_miR_minus_20a_minus_5p"
      name "hsa_minus_miR_minus_20a_minus_5p"
      node_subtype "RNA"
      node_type "species"
      org_id "f25b2"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 426.2988217120951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hsa_minus_miR_minus_20a_minus_5p"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "NEDD4"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c276a; c628c"
      uniprot "NA"
    ]
    graphics [
      x 599.0908040924264
      y 609.88096718764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NEDD4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id71ecd0f3"
      uniprot "NA"
    ]
    graphics [
      x 740.0583524484455
      y 612.3297008665925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP4217"
      hgnc "NA"
      map_id "Ebola_space_virus_br_pathway_space_in_space_host"
      name "Ebola_space_virus_br_pathway_space_in_space_host"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b8756"
      uniprot "NA"
    ]
    graphics [
      x 855.0984156497994
      y 582.6934645145742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ebola_space_virus_br_pathway_space_in_space_host"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000163513;urn:miriam:ensembl:ENSG00000106799"
      hgnc "NA"
      map_id "e3254"
      name "e3254"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e3254"
      uniprot "NA"
    ]
    graphics [
      x 531.883567895651
      y 978.3384029081934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e3254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:29945215;PUBMED:24627487;PUBMED:24438557"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide33a2255"
      uniprot "NA"
    ]
    graphics [
      x 647.5739213621284
      y 980.8357618575291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000124225;urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "b0004"
      name "b0004"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0004"
      uniprot "NA"
    ]
    graphics [
      x 730.3604998151385
      y 904.2222084733854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b0004"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000175387"
      hgnc "NA"
      map_id "df78d"
      name "df78d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "df78d"
      uniprot "NA"
    ]
    graphics [
      x 628.2021183050373
      y 853.250559744042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "df78d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophagosome_br_formation"
      name "Autophagosome_br_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5486"
      uniprot "NA"
    ]
    graphics [
      x 373.6554087121904
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagosome_br_formation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_19"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id694a53fa"
      uniprot "NA"
    ]
    graphics [
      x 262.3333676150917
      y 97.4551104147319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000085978"
      hgnc "NA"
      map_id "ATG16L1"
      name "ATG16L1"
      node_subtype "GENE"
      node_type "species"
      org_id "f314c"
      uniprot "NA"
    ]
    graphics [
      x 164.03230608009903
      y 178.3759965780264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG16L1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Neurodevelopmental_br_disorders"
      name "Neurodevelopmental_br_disorders"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1533"
      uniprot "NA"
    ]
    graphics [
      x 232.84457005863115
      y 434.9979283108291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Neurodevelopmental_br_disorders"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:31602316"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6fa109c9"
      uniprot "NA"
    ]
    graphics [
      x 215.98524470778887
      y 546.5934573252623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf54d419"
      uniprot "NA"
    ]
    graphics [
      x 342.1788144904507
      y 738.6884419933765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7947a72f"
      uniprot "NA"
    ]
    graphics [
      x 150.63055744197794
      y 721.5379110680667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:28888937"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6cc9fb0d"
      uniprot "NA"
    ]
    graphics [
      x 441.3306442556691
      y 644.6008956980402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP3874"
      hgnc "NA"
      map_id "TGF_minus_B_space_signaling_br_pathway"
      name "TGF_minus_B_space_signaling_br_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bab41"
      uniprot "NA"
    ]
    graphics [
      x 503.0874610795331
      y 740.4881525777951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TGF_minus_B_space_signaling_br_pathway"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Blood_br_pressure"
      name "Blood_br_pressure"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e3ffd"
      uniprot "NA"
    ]
    graphics [
      x 406.96350733985787
      y 471.9846602506176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Blood_br_pressure"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:19430479"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_25"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idef60a322"
      uniprot "NA"
    ]
    graphics [
      x 335.88461573101097
      y 564.0858767198712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3b8d16d5"
      uniprot "NA"
    ]
    graphics [
      x 89.37580596721926
      y 289.6646893185135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fe61d16"
      uniprot "NA"
    ]
    graphics [
      x 660.6188688186458
      y 511.24359806929493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 26
    source 1
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4_minus_AS1"
      target_id "W23_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 2
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_27"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 3
    target 4
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 4
    target 5
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_28"
      target_id "hsa_minus_miR_minus_20a_minus_5p"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 6
    target 7
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "NEDD4"
      target_id "W23_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 7
    target 8
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_22"
      target_id "Ebola_space_virus_br_pathway_space_in_space_host"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 9
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "e3254"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 11
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "INHIBITION"
      source_id "b0004"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 10
    target 12
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_24"
      target_id "df78d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 13
    target 14
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Autophagosome_br_formation"
      target_id "W23_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 14
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_19"
      target_id "ATG16L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 16
    target 17
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Neurodevelopmental_br_disorders"
      target_id "W23_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 17
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_21"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 3
    target 18
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 18
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_26"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 3
    target 19
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 19
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_23"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 3
    target 20
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 20
    target 6
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "NEDD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 20
    target 21
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "TGF_minus_B_space_signaling_br_pathway"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 22
    target 23
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Blood_br_pressure"
      target_id "W23_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 23
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_25"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 5
    target 24
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "hsa_minus_miR_minus_20a_minus_5p"
      target_id "W23_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 24
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_17"
      target_id "ATG16L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 6
    target 25
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "NEDD4"
      target_id "W23_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 25
    target 6
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_18"
      target_id "NEDD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
