# generated with VANTED V2.8.2 at Fri Mar 04 10:03:45 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 15
      diagram "R-HSA-9678108; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:pubmed:15094372;urn:miriam:reactome:R-COV-9683649;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9694659; urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9684199;urn:miriam:reactome:R-COV-9697428;urn:miriam:reactome:R-COV-9694612;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9694300;urn:miriam:pubmed:15496142;urn:miriam:uniprot:P59595;urn:miriam:pubmed:12775768;urn:miriam:reactome:R-COV-9683625; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9694573;urn:miriam:reactome:R-COV-9684203;urn:miriam:reactome:R-COV-9697429;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9684230;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P59595; urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683754;urn:miriam:pubmed:19106108; urn:miriam:pubmed:32654247;urn:miriam:pubmed:33264373;urn:miriam:pubmed:32416961;urn:miriam:pubmed:16112641;urn:miriam:hgnc.symbol:N;urn:miriam:pubmed:32363136;urn:miriam:uniprot:P59595;urn:miriam:pubmed:16845612;urn:miriam:ncbigene:1489678"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P59595"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer; SUMO_minus_p_minus_N_space_dimer; SUMO1_minus_K62_minus_ADPr_minus_p_minus_S177_minus_N; encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA; encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA_space_(plus_space_strand); N; ADPr_minus_p_minus_S177_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_space_coronavirus_space_genomic_space_RNA; p_minus_S177_minus_N"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_739; layout_263; layout_395; layout_261; layout_598; layout_863; layout_239; layout_2265; layout_394; layout_590; layout_591; layout_2261; layout_393; layout_250; sa74"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 800.913341232552
      y 1149.3617118909638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59595"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 13
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623; urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408; urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754; urn:miriam:uniprot:P59637; urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E; urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321; urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P59637"
      name "3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; nascent_space_E; N_minus_glycan_space_E; E; Orf3a; SARS_space_E"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_362; layout_365; layout_371; layout_373; layout_233; layout_355; e5c2e; sa69; sa48; sa92; sa140; sa146; sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 785.8347702862262
      y 485.83997684940164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; C19DMap:JNK pathway; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9682868; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683594;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:reactome:R-COV-9683676;urn:miriam:pubmed:15367599;urn:miriam:uniprot:P59594; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683608;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683638;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9683768; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683716;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S; urn:miriam:ncbigene:1489668;urn:miriam:pubmed:32275855;urn:miriam:pubmed:32075877;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32225176;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P59594"
      name "nascent_space_Spike; N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike; complex_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_unfolded_space_N_minus_glycan_space_Spike; S"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_227; layout_273; layout_300; layout_302; layout_287; layout_293; layout_317; layout_279; sa78; sa76"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1166.7007230355275
      y 979.7594546709381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; WP4880; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59636;urn:miriam:reactome:R-COV-9689378;urn:miriam:reactome:R-COV-9694649; urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59636"
      name "9b; Orf9; Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1900; e9876; sa362; sa77; sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1004.9307042614341
      y 1185.3852151690116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59636"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 918.2943921334306
      y 935.7365807313402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4936; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P31749; urn:miriam:ncbigene:207;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:391;urn:miriam:hgnc.symbol:AKT1;urn:miriam:refseq:NM_005163;urn:miriam:uniprot:P31749;urn:miriam:ensembl:ENSG00000142208"
      hgnc "NA; HGNC_SYMBOL:AKT1"
      map_id "UNIPROT:P31749"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c34e3; sa29; sa28"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 231.20803352836492
      y 815.8510335335665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31749"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4877; WP4880; C19DMap:JNK pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416; urn:miriam:uniprot:P59633; urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "UNIPROT:P59633"
      name "ee4e9; 3b; Orf3b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "ee4e9; e6060; sa75; sa356; sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1087.5596420689333
      y 805.7171440290745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4880; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59634; urn:miriam:uniprot:P59634;urn:miriam:ncbigene:1489673"
      hgnc "NA"
      map_id "UNIPROT:P59634"
      name "6; Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ac5ae; sa75"
      uniprot "UNIPROT:P59634"
    ]
    graphics [
      x 1070.2130920696457
      y 687.9500347739826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59634"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5039; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q14790; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012; urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "NA; HGNC_SYMBOL:CASP8"
      map_id "UNIPROT:Q14790"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc51a; sa459; sa390; sa14; sa13"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 717.7366236230444
      y 538.2761217732079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14790"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS; urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "UNIPROT:P99999"
      name "Cyt_space_C; CYCS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253; sa13; sa24; sa25"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 285.61204823471326
      y 677.574153536828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P99999"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M; urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M; urn:miriam:ncbiprotein:APO40582;urn:miriam:pubmed:16845612;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "UNIPROT:M"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193; sa134; sa226; sa232; sa408; sa359; sa353; sa403; sa42"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 439.2686372932332
      y 980.2281070143533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:M"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315; urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "UNIPROT:Q16539"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64; sa368; sa445; sa49; sa50; path_0_sa42; path_0_sa43; path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 596.4402127506719
      y 726.0297036396797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ncbigene:8717;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc.symbol:TRADD; urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:TRADD"
      map_id "UNIPROT:Q15628"
      name "TRADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa400; sa44"
      uniprot "UNIPROT:Q15628"
    ]
    graphics [
      x 1376.702750896704
      y 569.1392858141811
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15628"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:FADD"
      map_id "UNIPROT:Q13158"
      name "FADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10; sa9"
      uniprot "UNIPROT:Q13158"
    ]
    graphics [
      x 1145.2065904257222
      y 418.93913549060403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_14"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1283.456522677106
      y 483.0635296277644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810;urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916"
      hgnc "HGNC_SYMBOL:TNF;HGNC_SYMBOL:TNFRSF1A"
      map_id "UNIPROT:P01375;UNIPROT:P19438"
      name "TNF_slash_TNFRSF1A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P01375;UNIPROT:P19438"
    ]
    graphics [
      x 1443.9343576723047
      y 448.44482355877983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01375;UNIPROT:P19438"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:TRADD;HGNC_SYMBOL:FADD"
      map_id "UNIPROT:Q15628;UNIPROT:Q13158"
      name "TRADD_slash_FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q15628;UNIPROT:Q13158"
    ]
    graphics [
      x 1122.6610287797741
      y 507.20156092032073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15628;UNIPROT:Q13158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936"
      hgnc "HGNC_SYMBOL:BAD"
      map_id "UNIPROT:Q92934"
      name "BAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78; sa79"
      uniprot "UNIPROT:Q92934"
    ]
    graphics [
      x 473.21082960306956
      y 579.8859685069291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92934"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:15694340"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 359.88243491853984
      y 732.6956384967039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 749.0465306516584
      y 645.1272981744131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936;urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BBC3;HGNC_SYMBOL:BAD;HGNC_SYMBOL:BCL2L11"
      map_id "UNIPROT:Q9BXH1;UNIPROT:Q96PG8;UNIPROT:Q92934;UNIPROT:O43521"
      name "BAD_slash_BBC3_slash_BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q9BXH1;UNIPROT:Q96PG8;UNIPROT:Q92934;UNIPROT:O43521"
    ]
    graphics [
      x 320.85477108833106
      y 113.71533293478547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BXH1;UNIPROT:Q96PG8;UNIPROT:Q92934;UNIPROT:O43521"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 426.20296663125777
      y 126.88081750203969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990;urn:miriam:ncbigene:4170;urn:miriam:uniprot:Q07820;urn:miriam:hgnc:6943;urn:miriam:refseq:NM_021960;urn:miriam:ensembl:ENSG00000143384;urn:miriam:hgnc.symbol:MCL1"
      hgnc "HGNC_SYMBOL:BCL2L1;HGNC_SYMBOL:BCL2;HGNC_SYMBOL:MCL1"
      map_id "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
      name "BCL2_slash_MCL1_slash_BCL2L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
    ]
    graphics [
      x 473.44277502049005
      y 259.9775770425311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:uniprot:O43521;urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:BCL2L11;HGNC_SYMBOL:BAD;HGNC_SYMBOL:BBC3"
      map_id "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      name "BAD_slash_BBC3_slash_BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 356.8776456103278
      y 235.83934256020683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_24"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1040.2744414059584
      y 1021.2508689169745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_25"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 951.9402012076464
      y 1077.882798937222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_10"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 439.2245321040797
      y 689.5197067766378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812; urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "UNIPROT:Q07812"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa31; sa30; path_0_sa458; path_0_sa126; path_0_sa127"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 414.9950976795848
      y 526.7830203103969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07812"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1016.0533612259194
      y 804.6768461838716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990;urn:miriam:ncbigene:4170;urn:miriam:uniprot:Q07820;urn:miriam:hgnc:6943;urn:miriam:refseq:NM_021960;urn:miriam:ensembl:ENSG00000143384;urn:miriam:hgnc.symbol:MCL1;urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1"
      hgnc "HGNC_SYMBOL:BCL2;HGNC_SYMBOL:MCL1;HGNC_SYMBOL:BCL2L1"
      map_id "UNIPROT:P10415;UNIPROT:Q07820;UNIPROT:Q07817"
      name "BCL2_slash_MCL1_slash_BCL2L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P10415;UNIPROT:Q07820;UNIPROT:Q07817"
    ]
    graphics [
      x 652.9955847715273
      y 322.0910595690905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10415;UNIPROT:Q07820;UNIPROT:Q07817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:15694340;PUBMED:17428862"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 569.9969001199827
      y 425.0999439390996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:ORF7a;urn:miriam:uniprot:Q19QW4;urn:miriam:ncbigene:1489674"
      hgnc "HGNC_SYMBOL:ORF7a"
      map_id "UNIPROT:Q19QW4"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:Q19QW4"
    ]
    graphics [
      x 571.9331824286458
      y 286.0522217560725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q19QW4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:pubmed:32555321;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3;urn:miriam:hgnc.symbol:CASP3; urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:pubmed:32555321;urn:miriam:hgnc.symbol:CASP3; urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "UNIPROT:P42574"
      name "CASP3; cleaved~CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17; sa15; path_0_sa597; path_0_sa596"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 642.760386205718
      y 817.9016054939109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42574"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_28"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 766.5519777066443
      y 908.1486411557339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 417.3352986917804
      y 362.2058469756998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050; urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "UNIPROT:P55957"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89; sa22; sa23; path_0_sa436; path_0_sa435"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 591.2408685091784
      y 351.8332069698933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55957"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_19"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 953.9981861400744
      y 819.1364888891023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 304.89443212272306
      y 944.2232048986191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:1508;urn:miriam:hgnc.symbol:CASP7;urn:miriam:ncbigene:840;urn:miriam:ec-code:3.4.22.60;urn:miriam:refseq:NM_033338;urn:miriam:ensembl:ENSG00000165806;urn:miriam:uniprot:P55210"
      hgnc "HGNC_SYMBOL:CASP7"
      map_id "UNIPROT:P55210"
      name "CASP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40; sa39"
      uniprot "UNIPROT:P55210"
    ]
    graphics [
      x 681.7714855492984
      y 652.5109567431336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_12"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 810.2064576194182
      y 785.7069567232595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_21"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 839.2910705151824
      y 1044.0239469920039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211; urn:miriam:hgnc.symbol:CASP9;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc.symbol:CSAP9;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211; urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9; HGNC_SYMBOL:CASP9;HGNC_SYMBOL:CSAP9"
      map_id "UNIPROT:P55211"
      name "CASP9; cleaved~CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18; sa19; sa47; path_0_sa598; path_0_sa599"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 351.5166330264508
      y 613.3621055950434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 199.96288613731622
      y 705.037206781641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:ncbigene:317;urn:miriam:hgnc:576;urn:miriam:refseq:NM_181861.1;urn:miriam:hgnc.symbol:APAF1;urn:miriam:uniprot:O14727;urn:miriam:ensembl:ENSG00000120868"
      hgnc "HGNC_SYMBOL:CYCS;HGNC_SYMBOL:CASP9;HGNC_SYMBOL:APAF1"
      map_id "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
      name "Apoptosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
    ]
    graphics [
      x 62.5
      y 720.2345386294901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 709.2707741810551
      y 412.1239057620787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32555321"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 949.9380325182078
      y 475.2790892543022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "UNIPROT:Q7TFA0"
      name "Orf8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa73"
      uniprot "UNIPROT:Q7TFA0"
    ]
    graphics [
      x 1147.5798830843164
      y 1052.6494127792569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7TFA0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_20"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1063.5204913218888
      y 950.3188884047495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 561.7514271531584
      y 559.6200030085988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916"
      hgnc "HGNC_SYMBOL:TNFRSF1A"
      map_id "UNIPROT:P19438"
      name "TNFRSF1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P19438"
    ]
    graphics [
      x 1637.4866151880478
      y 285.4250701966767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19438"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_18"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1580.0486401133712
      y 395.785251051506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810; urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:hgnc.symbol:TNF;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01375;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "UNIPROT:P01375"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3; sa238"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 1523.7868426652612
      y 289.08337517058015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 554.397747476798
      y 659.3177550046973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1258.9716137150936
      y 328.7309606724982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:FASLG;urn:miriam:ncbigene:356;urn:miriam:refseq:NM_000639;urn:miriam:ensembl:ENSG00000117560;urn:miriam:uniprot:P48023;urn:miriam:hgnc:11936;urn:miriam:hgnc:11920;urn:miriam:uniprot:P25445;urn:miriam:refseq:NM_000043;urn:miriam:ensembl:ENSG00000026103;urn:miriam:ncbigene:355;urn:miriam:hgnc.symbol:FAS"
      hgnc "HGNC_SYMBOL:FASLG;HGNC_SYMBOL:FAS"
      map_id "UNIPROT:P48023;UNIPROT:P25445"
      name "FAS_slash_FASL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P48023;UNIPROT:P25445"
    ]
    graphics [
      x 1339.4801491025153
      y 214.6214821743581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48023;UNIPROT:P25445"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:FASLG;urn:miriam:ncbigene:356;urn:miriam:refseq:NM_000639;urn:miriam:doi:10.1101/2020.12.04.412494;urn:miriam:ensembl:ENSG00000117560;urn:miriam:uniprot:P48023;urn:miriam:hgnc:11936"
      hgnc "HGNC_SYMBOL:FASLG"
      map_id "UNIPROT:P48023"
      name "FASLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:P48023"
    ]
    graphics [
      x 1212.530316004818
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48023"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_9"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1326.9613653288297
      y 88.35881738198486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:11920;urn:miriam:uniprot:P25445;urn:miriam:refseq:NM_000043;urn:miriam:ensembl:ENSG00000026103;urn:miriam:ncbigene:355;urn:miriam:hgnc.symbol:FAS"
      hgnc "HGNC_SYMBOL:FAS"
      map_id "UNIPROT:P25445"
      name "FAS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P25445"
    ]
    graphics [
      x 1236.5564447376373
      y 168.47046813371077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25445"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_29"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 741.7099144342963
      y 305.6374724985051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 161.8213842067188
      y 639.9488976457305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:317;urn:miriam:hgnc:576;urn:miriam:refseq:NM_181861.1;urn:miriam:hgnc.symbol:APAF1;urn:miriam:uniprot:O14727;urn:miriam:ensembl:ENSG00000120868"
      hgnc "HGNC_SYMBOL:APAF1"
      map_id "UNIPROT:O14727"
      name "APAF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27"
      uniprot "UNIPROT:O14727"
    ]
    graphics [
      x 124.7083799632801
      y 764.8219397598783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14727"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 62
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13158"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 13
    target 15
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15628"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01375;UNIPROT:P19438"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_14"
      target_id "UNIPROT:Q15628;UNIPROT:Q13158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92934"
      target_id "M113_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 6
    target 19
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P31749"
      target_id "M113_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_27"
      target_id "UNIPROT:Q92934"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 12
    target 20
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16539"
      target_id "M113_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 2
    target 20
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59637"
      target_id "M113_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 20
    target 12
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_15"
      target_id "UNIPROT:Q16539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BXH1;UNIPROT:Q96PG8;UNIPROT:Q92934;UNIPROT:O43521"
      target_id "M113_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "UNKNOWN_INHIBITION"
      source_id "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
      target_id "M113_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_16"
      target_id "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 3
    target 25
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "M113_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 25
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_24"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 4
    target 26
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59636"
      target_id "M113_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 26
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_25"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 10
    target 27
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P99999"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 12
    target 27
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16539"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q07812"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 27
    target 10
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_10"
      target_id "UNIPROT:P99999"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 8
    target 29
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59634"
      target_id "M113_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 29
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_26"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10415;UNIPROT:Q07820;UNIPROT:Q07817"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q19QW4"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 2
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "UNKNOWN_INHIBITION"
      source_id "UNIPROT:P59637"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 18
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q92934"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 31
    target 23
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_17"
      target_id "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42574"
      target_id "M113_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 34
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_28"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 28
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07812"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 24
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 23
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55957"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 35
    target 28
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_34"
      target_id "UNIPROT:Q07812"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 7
    target 37
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59633"
      target_id "M113_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 37
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_19"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 6
    target 38
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P31749"
      target_id "M113_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 11
    target 38
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:M"
      target_id "M113_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 38
    target 6
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_13"
      target_id "UNIPROT:P31749"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55210"
      target_id "M113_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 40
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_12"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 1
    target 41
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "M113_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 41
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_21"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55211"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 6
    target 43
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P31749"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_32"
      target_id "UNIPROT:P55211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 36
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55957"
      target_id "M113_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 9
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14790"
      target_id "M113_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 45
    target 36
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_33"
      target_id "UNIPROT:P55957"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 9
    target 46
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14790"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 14
    target 46
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13158"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 17
    target 46
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q15628;UNIPROT:Q13158"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 2
    target 46
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59637"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 46
    target 9
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_30"
      target_id "UNIPROT:Q14790"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7TFA0"
      target_id "M113_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 48
    target 5
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_20"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 39
    target 49
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55210"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 42
    target 49
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55211"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 9
    target 49
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14790"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 49
    target 39
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_11"
      target_id "UNIPROT:P55210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19438"
      target_id "M113_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01375"
      target_id "M113_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 51
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_18"
      target_id "UNIPROT:P01375;UNIPROT:P19438"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 33
    target 53
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42574"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 9
    target 53
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14790"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 42
    target 53
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55211"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 53
    target 33
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_31"
      target_id "UNIPROT:P42574"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 14
    target 54
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13158"
      target_id "M113_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48023;UNIPROT:P25445"
      target_id "M113_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 54
    target 14
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_22"
      target_id "UNIPROT:Q13158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P48023"
      target_id "M113_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25445"
      target_id "M113_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 57
    target 55
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_9"
      target_id "UNIPROT:P48023;UNIPROT:P25445"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 36
    target 59
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55957"
      target_id "M113_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 59
    target 36
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_29"
      target_id "UNIPROT:P55957"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 10
    target 60
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P99999"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14727"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 42
    target 60
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55211"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 6
    target 60
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P31749"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 60
    target 44
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_23"
      target_id "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
