# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4860; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:wikidata:Q25100575; urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "NA"
      map_id "Pevonedistat"
      name "Pevonedistat"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "dda75; sa54"
      uniprot "NA"
    ]
    graphics [
      x 62.499999999999986
      y 100.93425595300761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_6"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idc583d98b"
      uniprot "NA"
    ]
    graphics [
      x 167.73917429731438
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:ensembl:ENSG00000159593"
      hgnc "NA"
      map_id "c466c"
      name "c466c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c466c"
      uniprot "NA"
    ]
    graphics [
      x 191.50325075022542
      y 173.31864186159873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c466c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "Substrate"
      name "Substrate"
      node_subtype "GENE"
      node_type "species"
      org_id "d1f64"
      uniprot "NA"
    ]
    graphics [
      x 530.2605024747913
      y 437.4309632690366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Substrate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id92c39589"
      uniprot "NA"
    ]
    graphics [
      x 598.1397426422769
      y 347.5020119485914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:wikipathways:WP183"
      hgnc "NA"
      map_id "Proteasome_space_degradation"
      name "Proteasome_space_degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5bba"
      uniprot "NA"
    ]
    graphics [
      x 500.3899493821484
      y 290.4253858909975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Proteasome_space_degradation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 7
    source 1
    target 2
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "Pevonedistat"
      target_id "W5_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 2
    target 3
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_6"
      target_id "c466c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 4
    target 5
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "Substrate"
      target_id "W5_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 5
    target 6
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_5"
      target_id "Proteasome_space_degradation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
