# generated with VANTED V2.8.2 at Fri Mar 04 10:03:46 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 13
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623; urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408; urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754; urn:miriam:uniprot:P59637; urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E; urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321; urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P59637"
      name "3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; nascent_space_E; N_minus_glycan_space_E; E; Orf3a; SARS_space_E"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_362; layout_365; layout_371; layout_373; layout_233; layout_355; e5c2e; sa69; sa48; sa92; sa140; sa146; sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 507.45520268079235
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; WP4880; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59636;urn:miriam:reactome:R-COV-9689378;urn:miriam:reactome:R-COV-9694649; urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59636"
      name "9b; Orf9; Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1900; e9876; sa362; sa77; sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1449.834359970246
      y 1157.2309908127736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59636"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 1184.8586265606841
      y 863.1686893440937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1684.1717796318005
      y 896.5722560951659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:1511;urn:miriam:uniprot:P08311;urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448; urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "UNIPROT:P08311"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137; sa26; sa381"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 549.6682281603914
      y 1639.0360641475295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "Fe2_plus_"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa367; sa365; sa375; sa163; sa55; sa162; sa28; sa135; sa238; sa240; sa357"
      uniprot "NA"
    ]
    graphics [
      x 1044.5535181111634
      y 889.6938607468475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fe2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474; urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "NADPH"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382; sa395; sa30; sa295; sa153; sa25; sa33; sa24; sa100; sa356; sa379"
      uniprot "NA"
    ]
    graphics [
      x 1221.9808510437101
      y 788.0339652536841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:NLRP3 inflammasome activation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746; urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:Q7Z434"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151; sa156; sa127; sa128; sa102; sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1346.7922729647476
      y 1327.2821310445336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033; urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "UNIPROT:Q13114"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82; sa83; sa493; sa11; sa116; sa99; sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1609.317283915681
      y 1149.0001115583036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P19838"
      name "NFKB1; p105; p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269; sa483; sa81; sa151; sa84; sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1431.7451515419193
      y 620.5517587817662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:Coagulation pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538; urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553; urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "UNIPROT:P01584"
      name "IL1b; proIL_minus_1B; IL_minus_1B; IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127; sa92; sa15; sa17; sa172; sa21; sa244; sa462; sa464; sa460; sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 725.7150320363374
      y 1212.6715424272988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 17
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292; urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292; urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "UNIPROT:P29466"
      name "CASP1; Caspase_minus_1_space_Tetramer; CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa82; sa25; csa5; sa100; sa168; sa170; sa171; sa169; csa7; sa384; sa383; csa94; csa93; sa457; sa376; sa385; sa382"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 545.0159402470168
      y 1409.4779658430816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29466"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3; urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "UNIPROT:Q9ULZ3"
      name "PYCARD; ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14; sa131; sa103; sa104; sa375"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1142.871306405035
      y 1242.3231246618845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9ULZ3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q96P20"
      name "NLRP3"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa10; sa134; sa92; sa101; sa362; sa468; sa525; sa361; sa367"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 633.627860752787
      y 782.0887536058906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3; urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD; HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS"
      map_id "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27; csa29"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1643.4318566412503
      y 1237.0492217776898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 1442.8814605506532
      y 1254.7054820746516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "proIL_minus_18"
      name "proIL_minus_18"
      node_subtype "RNA; GENE"
      node_type "species"
      org_id "sa93; sa95"
      uniprot "NA"
    ]
    graphics [
      x 626.4408186345634
      y 869.8521901233698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "proIL_minus_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 502.79752635143996
      y 1000.1252826065232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "UNIPROT:Q14116"
      name "proIL_minus_18; IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14; sa16; sa18; sa20; sa463; sa465; sa461; sa467"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 406.22819944069136
      y 1131.3060353621336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_46"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 742.0101238505193
      y 601.4663577979532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "UNIPROT:Q9H3M7"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8; sa363"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 936.9168647017054
      y 445.95393557739754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H3M7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP; urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:TXNIP; HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3; csa74"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 607.3949136961079
      y 588.9626134885482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
      name "ASC:MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1427.1451308110236
      y 1395.3287214699747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1572.5694977363687
      y 1323.4889815226836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1515.3137646879745
      y 1064.9073400034297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_83"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1389.4459489994629
      y 1082.7058703116713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1653.274477661892
      y 1001.01697411291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "UNIPROT:P59632;UNIPROT:Q13114"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30; csa24"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 1760.502563801358
      y 903.4750604743774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:Q13114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP; urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN;HGNC_SYMBOL:TXNIP; HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:TXN"
      map_id "UNIPROT:P10599;UNIPROT:Q9H3M7"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1; csa69"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1172.8752102894532
      y 308.9837178835713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10599;UNIPROT:Q9H3M7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_17"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1070.2291436085336
      y 377.9989854188846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "UNIPROT:P10599"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175; sa7; sa364; sa366"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 921.984935268872
      y 326.33582413822694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10599"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 420.7220872516806
      y 1280.953793951818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_81"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 748.6145377322266
      y 424.9274715648772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 804.722663485996
      y 522.7201352636116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "CO"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158; sa31; sa362"
      uniprot "NA"
    ]
    graphics [
      x 959.7285368307195
      y 637.6780766099583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "Reactive_space_Oxygen_space_Species"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1; sa2; sa165; sa524; sa493"
      uniprot "NA"
    ]
    graphics [
      x 647.170180265584
      y 451.4751160362771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Reactive_space_Oxygen_space_Species"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_27"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1048.382687592412
      y 293.85327109056493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_49"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1610.1139259946024
      y 743.8284951240468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 685.0644917611986
      y 1354.363310345398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108; urn:miriam:pubmed:24231807;urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "Ca2_plus_"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa141; sa142; sa144; path_0_sa164; path_1_sa54; path_1_sa55; path_1_sa63; path_1_sa64; path_0_sa163"
      uniprot "NA"
    ]
    graphics [
      x 432.2548622314777
      y 317.03867519994867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 401.14085573014785
      y 159.19990869329683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_33"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1491.7534402019305
      y 515.6809277125072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "Heme"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa23; sa21; sa197; sa355"
      uniprot "NA"
    ]
    graphics [
      x 1095.3114485090427
      y 660.0609890529213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Heme"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1100.934992138102
      y 772.0312965298577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 13
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "O2"
      name "O2"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "sa154; sa24; sa121; sa124; sa156; sa136; sa213; sa22; sa250; sa4; sa358; sa39; sa383"
      uniprot "NA"
    ]
    graphics [
      x 1116.8058850072566
      y 900.0492154690619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "O2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18; urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601; urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "UNIPROT:P09601"
      name "HMOX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa161; sa168; sa50; sa29; sa10; sa341; sa351; sa352; sa340"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 993.3584176134001
      y 834.0791790216138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09601"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "Biliverdin"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156; sa22; sa361"
      uniprot "NA"
    ]
    graphics [
      x 1196.0786232515666
      y 701.7584310041638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Biliverdin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "NADP_plus_"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155; sa27; sa34; sa241; sa23; sa104; sa354; sa380"
      uniprot "NA"
    ]
    graphics [
      x 993.0941083910664
      y 747.6462715291094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADP_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_26"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 448.2558221182021
      y 1435.689503151597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 610.3768956458571
      y 1327.6479636734039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
      name "NLRP3_space_oligomer:ASC:proCaspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 405.9939433731358
      y 1556.0251263112232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1254.9579426531923
      y 1378.3022499583012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 528.6587664736476
      y 1519.176562506891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_32"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1735.8952748621573
      y 1016.7427433946934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_25"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 287.3242961886129
      y 1150.0010940779462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1195.2417112512935
      y 542.5937068093605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653"
      hgnc "HGNC_SYMBOL:NFKB2"
      map_id "UNIPROT:Q00653"
      name "p65"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q00653"
    ]
    graphics [
      x 1295.955121106526
      y 562.1741599421354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:Q00653;UNIPROT:P19838"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22; csa23"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 931.2030996519679
      y 528.4014997529696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00653;UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 460.15979194564505
      y 780.0756699057134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4; csa73"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 336.0583256038501
      y 687.4038896908355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548; urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1;HGNC_SYMBOL:NLRP3; HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:SUGT1"
      map_id "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6; csa72"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 447.1615094047385
      y 671.59931619386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 490.4147078452306
      y 1317.9278394712396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 756.8933497747092
      y 329.05480074418097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 983.0189523012147
      y 372.22551760506553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:P19838;UNIPROT:Q00653"
      name "NF_minus_KB_space_COMPLEX:IKBA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P19838;UNIPROT:Q00653"
    ]
    graphics [
      x 979.432433062763
      y 219.03798836623344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19838;UNIPROT:Q00653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_29"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 766.9936412750017
      y 705.635541520534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_41"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1754.3053673572824
      y 1096.7012928253484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_20"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 628.0549665615603
      y 1498.8103066553804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1336.2392603798594
      y 1200.8606033745223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "proIL_minus_1B"
      name "proIL_minus_1B"
      node_subtype "RNA; GENE"
      node_type "species"
      org_id "sa94; sa96"
      uniprot "NA"
    ]
    graphics [
      x 807.2763995909761
      y 891.8964560834958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "proIL_minus_1B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_43"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 749.1104177243898
      y 1049.1513658448598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 483.17348465738183
      y 183.88096600134384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "IKBA"
      name "IKBA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 873.7767742792075
      y 151.61493159985707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IKBA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 874.6093848329601
      y 283.044945450522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_23"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 810.751495074594
      y 1294.5530871773026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "UNIPROT:P08238"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4; sa374"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 62.5
      y 668.4606899619602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:29712950;PUBMED:17435760"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 179.83850647196505
      y 695.6107119521345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "UNIPROT:Q9Y2Z0"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5; sa373"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 127.34301919444442
      y 800.4271649385912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y2Z0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 814.4409098192641
      y 1119.7557188251915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:25770182;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_37"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 485.52921560288866
      y 555.6728954763349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "DAMPs"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa91; sa527"
      uniprot "NA"
    ]
    graphics [
      x 506.0747345313142
      y 440.9470850378441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DAMPs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "PAMPs"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa90; sa528"
      uniprot "NA"
    ]
    graphics [
      x 416.5047102193704
      y 450.507401195852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PAMPs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "NLRP3_space_Elicitor_space_Small_space_Molecules"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa135; sa494"
      uniprot "NA"
    ]
    graphics [
      x 367.7034713979285
      y 583.2674070464894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRP3_space_Elicitor_space_Small_space_Molecules"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136; sa495"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 361.615251579316
      y 505.0954145093131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_28"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 866.8810036565557
      y 716.3457508980563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 86
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
      target_id "M117_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 16
    target 13
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "UNIPROT:Q9ULZ3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 16
    target 9
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "UNIPROT:Q13114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 16
    target 8
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "proIL_minus_18"
      target_id "M117_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_44"
      target_id "UNIPROT:Q14116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 14
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H3M7"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_46"
      target_id "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 9
    target 24
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 24
    target 15
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_39"
      target_id "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 9
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 2
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59636"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_53"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 4
    target 27
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 9
    target 27
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_48"
      target_id "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10599;UNIPROT:Q9H3M7"
      target_id "M117_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 30
    target 21
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "UNIPROT:Q9H3M7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "UNIPROT:P10599"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 19
    target 32
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14116"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 12
    target 32
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P29466"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 32
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "UNIPROT:Q14116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 32
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "UNIPROT:Q14116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_81"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "CO"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 34
    target 36
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_51"
      target_id "Reactive_space_Oxygen_space_Species"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 21
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H3M7"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 31
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10599"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 37
    target 29
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_27"
      target_id "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 10
    target 38
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 28
    target 38
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "UNIPROT:P59632;UNIPROT:Q13114"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 38
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_49"
      target_id "UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 11
    target 39
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01584"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 12
    target 39
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P29466"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 39
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 39
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 1
    target 41
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59637"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_45"
      target_id "Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 10
    target 42
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M117_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 42
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_33"
      target_id "UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "Heme"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 7
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 46
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09601"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 44
    target 47
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "Biliverdin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 44
    target 3
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 44
    target 48
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 44
    target 35
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "CO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 44
    target 6
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "Fe2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 12
    target 49
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 49
    target 51
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_26"
      target_id "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 13
    target 52
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 8
    target 52
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 52
    target 23
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_38"
      target_id "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 51
    target 53
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 5
    target 53
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08311"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 53
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 53
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 53
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 53
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 53
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 4
    target 54
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 9
    target 54
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 54
    target 28
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_32"
      target_id "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 19
    target 55
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14116"
      target_id "M117_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 55
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_25"
      target_id "UNIPROT:Q14116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 10
    target 56
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19838"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00653"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_31"
      target_id "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 14
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_42"
      target_id "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 12
    target 62
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466"
      target_id "M117_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 62
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_21"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 31
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10599"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 36
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "Reactive_space_Oxygen_space_Species"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 63
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_19"
      target_id "UNIPROT:P10599"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 58
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00653;UNIPROT:P19838"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "UNIPROT:P19838;UNIPROT:Q00653"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 64
    target 58
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_36"
      target_id "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 17
    target 66
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "proIL_minus_18"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 58
    target 66
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q00653;UNIPROT:P19838"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 66
    target 17
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_29"
      target_id "proIL_minus_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 15
    target 67
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 28
    target 67
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "UNIPROT:P59632;UNIPROT:Q13114"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 67
    target 15
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_41"
      target_id "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 12
    target 68
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 12
    target 68
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29466"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 68
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_20"
      target_id "UNIPROT:P29466"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 8
    target 69
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z434"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 2
    target 69
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59636"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 69
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_52"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "proIL_minus_1B"
      target_id "M117_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 71
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_43"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 40
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "Ca2_plus_"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 1
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P59637"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 72
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_47"
      target_id "Ca2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "IKBA"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 58
    target 74
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00653;UNIPROT:P19838"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 74
    target 65
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_35"
      target_id "UNIPROT:P19838;UNIPROT:Q00653"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 11
    target 75
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01584"
      target_id "M117_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 75
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_23"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08238"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y2Z0"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 77
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_30"
      target_id "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 13
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULZ3"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 14
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 79
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_34"
      target_id "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 61
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "DAMPs"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "PAMPs"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 83
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "NLRP3_space_Elicitor_space_Small_space_Molecules"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 84
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 36
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "Reactive_space_Oxygen_space_Species"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 22
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 40
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "Ca2_plus_"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "DAMPs"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "PAMPs"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 83
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "NLRP3_space_Elicitor_space_Small_space_Molecules"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 84
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 36
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "Reactive_space_Oxygen_space_Species"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 22
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 40
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "Ca2_plus_"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 80
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "UNIPROT:Q96P20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 80
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 70
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "proIL_minus_1B"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 58
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q00653;UNIPROT:P19838"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 85
    target 70
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_28"
      target_id "proIL_minus_1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
