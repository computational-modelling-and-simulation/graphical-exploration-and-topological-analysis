# generated with VANTED V2.8.2 at Fri Mar 04 09:59:53 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 164.95287278264186
      y 446.40551742032005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 514.9080629649468
      y 818.6890887934559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 777.8047582478335
      y 1205.360728265579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 374.48444667344063
      y 1043.5754122494998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 659.5475647352409
      y 1095.964352570527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 638.7643311275608
      y 1023.1825816884682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078; urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659; urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "UDP"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319; layout_2389; sa22; sa35"
      uniprot "NA"
    ]
    graphics [
      x 1388.6627669811182
      y 646.7001001431817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094; urn:miriam:obo.chebi:CHEBI%3A17361"
      hgnc "NA"
      map_id "CMP"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2069; layout_3351; sa34"
      uniprot "NA"
    ]
    graphics [
      x 1331.5624055988362
      y 737.7940205809635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q8N884; urn:miriam:pubmed:28363908;urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86;urn:miriam:obo.chebi:CHEBI%3A75909; urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86"
      hgnc "NA; HGNC_SYMBOL:CGAS"
      map_id "UNIPROT:Q8N884"
      name "cGAS; cGAS:dsDNA"
      node_subtype "GENE; COMPLEX; PROTEIN"
      node_type "species"
      org_id "c16eb; csa1; sa78"
      uniprot "UNIPROT:Q8N884"
    ]
    graphics [
      x 1108.4355068041807
      y 1033.5708360590984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N884"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4961; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580; urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA"
      map_id "cGAMP"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ba77f; bd7f8; sa83"
      uniprot "NA"
    ]
    graphics [
      x 583.3384414465833
      y 992.175849997156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cGAMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 10
      diagram "WP5038; C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q86WV6; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q86WV6;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:obo.go:GO%3A1990231;urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061; urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584; urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA; HGNC_SYMBOL:STING1"
      map_id "UNIPROT:Q86WV6"
      name "TMEM173; STING1; cGAMP:STING; cGAMP:STING:LC3; STING; cGAMP_minus_STING"
      node_subtype "GENE; PROTEIN; COMPLEX"
      node_type "species"
      org_id "c488d; sa172; sa171; csa3; csa4; csa13; sa100; sa98; csa11; csa16"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 266.51203135290723
      y 616.2760673943109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86WV6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 19
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661; urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "UNIPROT:Q14653"
      name "IRF3_underscore_homodimer; IRF3; IFNB1_space_expression_space_complex; p38_minus_NFkB"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa46; sa121; sa286; csa45; sa120; sa348; sa119; sa156; sa157; sa379; sa256; csa8; sa85; sa84; sa89; csa41; sa87; sa88; sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 844.9305776081053
      y 1226.3872534293052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14653"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:PAMP signalling; C19DMap:Pyrimidine deprivation; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110; urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "UNIPROT:Q9UHD2"
      name "TBK1; STING:TBK1"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa61; sa60; sa426; sa117; sa490; sa154; sa80; csa20; csa5; sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 537.8281847749829
      y 660.7020968113999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UHD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 10
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456; urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456; urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "UNIPROT:P01574"
      name "IFNB1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa3; sa27; sa26; sa34; sa33; sa147; sa89; sa87; sa91; sa88"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 950.6722263973437
      y 1521.0414222160643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01574"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998; urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "UNIPROT:Q04206;UNIPROT:P19838"
      name "p50_underscore_p65; IkB_underscore_p50_underscore_p65; NF_minus_kB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34; csa41; csa42; csa6; csa19"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1116.395047820439
      y 1195.2747170183327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04206;UNIPROT:P19838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919; urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "UNIPROT:P01562"
      name "IFNA1"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa30; sa31; sa38; sa39; sa5; sa146; sa88; sa90; sa86"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 783.0781307911377
      y 1766.8368084725598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01562"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526;urn:miriam:pubchem.compound:280; urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "CO2"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136; sa186; sa26; sa70; sa122; sa40; sa201"
      uniprot "NA"
    ]
    graphics [
      x 516.9279837865104
      y 872.351799242495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CO2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubchem.compound:6133;urn:miriam:obo.chebi:CHEBI%3A15713; urn:miriam:obo.chebi:CHEBI%3A15713"
      hgnc "NA"
      map_id "UTP"
      name "UTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa13; sa40"
      uniprot "NA"
    ]
    graphics [
      x 1736.5729863055358
      y 579.3293369170535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:27324217"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 137.96810937808675
      y 613.7796687762909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17562"
      hgnc "NA"
      map_id "cytidine"
      name "cytidine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31; sa38"
      uniprot "NA"
    ]
    graphics [
      x 1233.3983502511328
      y 1126.3775452523655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cytidine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:7923172;PUBMED:15689149"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 1290.1663290448555
      y 1280.8075852990692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:P32320;urn:miriam:ncbigene:978;urn:miriam:refseq:NM_001785;urn:miriam:ensembl:ENSG00000158825;urn:miriam:ec-code:3.5.4.5;urn:miriam:hgnc.symbol:CDA;urn:miriam:hgnc:1712"
      hgnc "HGNC_SYMBOL:CDA"
      map_id "UNIPROT:P32320"
      name "CDA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa32"
      uniprot "UNIPROT:P32320"
    ]
    graphics [
      x 1309.5665485809395
      y 1401.6993719006527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P32320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16704"
      hgnc "NA"
      map_id "uridine"
      name "uridine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa30; sa37"
      uniprot "NA"
    ]
    graphics [
      x 1307.3708240774913
      y 1155.5709517724226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "uridine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:16979567"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 805.144410715479
      y 1508.529903464042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 128.25165362071368
      y 702.7619132885592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "HCoVs_space_autophagy_space_(WP4863)"
      name "HCoVs_space_autophagy_space_(WP4863)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa177"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 792.6477246660752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HCoVs_space_autophagy_space_(WP4863)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 377.30983850120106
      y 627.4241247869523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:5411547"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1863.2944673803477
      y 617.7580399801585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1503;urn:miriam:ncbigene:1503;urn:miriam:ec-code:6.3.4.2;urn:miriam:refseq:NM_001905;urn:miriam:uniprot:P17812;urn:miriam:hgnc.symbol:CTPS1;urn:miriam:hgnc.symbol:CTPS1;urn:miriam:ensembl:ENSG00000171793;urn:miriam:hgnc:2519"
      hgnc "HGNC_SYMBOL:CTPS1"
      map_id "UNIPROT:P17812"
      name "CTPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:P17812"
    ]
    graphics [
      x 1963.3500242116997
      y 716.8362333819841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17812"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17677"
      hgnc "NA"
      map_id "CTP"
      name "CTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1801.581637521183
      y 516.4168530825314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17239"
      hgnc "NA"
      map_id "CDP"
      name "CDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 1543.9104475786407
      y 332.04078089363975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:24809024"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1549.5309348926257
      y 196.69224018952957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:brenda:1.17.4.1;urn:miriam:obo.go:GO%3A0005971;urn:miriam:refseq:NM_001034;urn:miriam:ncbigene:6241;urn:miriam:ncbigene:6241;urn:miriam:hgnc:10452;urn:miriam:ec-code:1.17.4.1;urn:miriam:hgnc.symbol:RRM2;urn:miriam:hgnc.symbol:RRM2;urn:miriam:ensembl:ENSG00000171848;urn:miriam:uniprot:P31350;urn:miriam:uniprot:P31350"
      hgnc "HGNC_SYMBOL:RRM2"
      map_id "UNIPROT:P31350"
      name "ribonucleoside_minus_diphosphate_space_reductase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P31350"
    ]
    graphics [
      x 1459.031192356244
      y 335.63098157839323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28846"
      hgnc "NA"
      map_id "dCDP"
      name "dCDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 1500.3893072578767
      y 80.96679239743264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dCDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:15130468"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1306.3266131457153
      y 950.3678890997506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:7371;urn:miriam:hgnc.symbol:UCK1;urn:miriam:ncbigene:7371;urn:miriam:uniprot:Q9HA47;urn:miriam:refseq:NM_012474;urn:miriam:hgnc:12562;urn:miriam:uniprot:Q9BZX2;urn:miriam:ncbigene:83549;urn:miriam:uniprot:Q9BZX2;urn:miriam:hgnc.symbol:UCK2;urn:miriam:hgnc.symbol:UCK2;urn:miriam:ec-code:2.7.1.48;urn:miriam:ensembl:ENSG00000143179"
      hgnc "HGNC_SYMBOL:UCK1;HGNC_SYMBOL:UCK2"
      map_id "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
      name "UCK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
    ]
    graphics [
      x 1277.0934141682947
      y 1041.0641737881456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 830.7692777989954
      y 1685.4694825659203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18075"
      hgnc "NA"
      map_id "dTDP"
      name "dTDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa60"
      uniprot "NA"
    ]
    graphics [
      x 1814.4387483275439
      y 1090.5028161597766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dTDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1948.3371030709905
      y 1031.2004906857558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18077"
      hgnc "NA"
      map_id "dTTP"
      name "dTTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa62"
      uniprot "NA"
    ]
    graphics [
      x 1977.943044730853
      y 889.6060664031015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dTTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:20610653"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 888.2481741450326
      y 1380.3489360839158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1021.9693473629176
      y 1677.6937299088122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
      name "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 1016.2050609999557
      y 1823.553159676685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:28579255"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1178.7583032371913
      y 1080.0233985186658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 1398.0951348983217
      y 1242.818242878056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_76"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 697.758425376591
      y 1682.0988412563265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "Interferon_space_1_space_pathway"
      name "Interferon_space_1_space_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa176"
      uniprot "NA"
    ]
    graphics [
      x 713.5433822880232
      y 1764.7503682813617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Interferon_space_1_space_pathway"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17672"
      hgnc "NA"
      map_id "CAP"
      name "CAP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 443.5308914970024
      y 1125.612384008148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CAP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 322.9033019860649
      y 1120.1358114419781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29991"
      hgnc "NA"
      map_id "L_minus_Asp"
      name "L_minus_Asp"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 243.9347122364536
      y 1214.3272287086677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Asp"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001306079;urn:miriam:ec-code:3.5.2.3;urn:miriam:hgnc:1424;urn:miriam:hgnc.symbol:CAD;urn:miriam:uniprot:P27708;urn:miriam:ncbigene:790;urn:miriam:ensembl:ENSG00000084774;urn:miriam:ec-code:6.3.5.5;urn:miriam:ec-code:2.1.3.2"
      hgnc "HGNC_SYMBOL:CAD"
      map_id "UNIPROT:P27708"
      name "CAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:P27708"
    ]
    graphics [
      x 484.2030509173538
      y 1014.4519156897799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27708"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A32814"
      hgnc "NA"
      map_id "N_minus_carbamoyl_minus_L_minus_aspartate"
      name "N_minus_carbamoyl_minus_L_minus_aspartate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 432.20301024495154
      y 952.7186250402001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_minus_carbamoyl_minus_L_minus_aspartate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:10462544"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 1363.4729163755603
      y 531.6826113592539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:CMPK1;urn:miriam:hgnc.symbol:CMPK1;urn:miriam:hgnc:18170;urn:miriam:ensembl:ENSG00000162368;urn:miriam:ec-code:2.7.4.14;urn:miriam:ec-code:2.7.4.6;urn:miriam:ncbigene:51727;urn:miriam:ncbigene:51727;urn:miriam:uniprot:P30085;urn:miriam:refseq:NM_016308"
      hgnc "HGNC_SYMBOL:CMPK1"
      map_id "UNIPROT:P30085"
      name "CMPK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P30085"
    ]
    graphics [
      x 1241.6549904756087
      y 613.2307689769581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30085"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_75"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 827.7217319501235
      y 1620.1195720870555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:17827303"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1436.051735144147
      y 197.72299508525793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME1"
      map_id "UNIPROT:P15531"
      name "NME1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa51; sa42"
      uniprot "UNIPROT:P15531"
    ]
    graphics [
      x 1595.9381807623145
      y 370.62245171045566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15531"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16311"
      hgnc "NA"
      map_id "dCTP"
      name "dCTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 1235.1521652924341
      y 198.35206970471063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dCTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:28458037"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1723.256063780937
      y 362.1739092093982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "RTC_space_and_space_transcription_space_pw"
      name "RTC_space_and_space_transcription_space_pw"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1611.2733623919385
      y 833.8603825163503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RTC_space_and_space_transcription_space_pw"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32531208"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 1447.922438447298
      y 859.6565492063164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0032774"
      hgnc "NA"
      map_id "RNA_space_biosynthesis"
      name "RNA_space_biosynthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 1316.9667123671056
      y 847.3509146312065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RNA_space_biosynthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 894.691653721172
      y 1759.9264298917683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57865"
      hgnc "NA"
      map_id "UMP"
      name "UMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 955.024893072929
      y 829.3165204471544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:10462544"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 1173.7232210267036
      y 722.5539141524142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:15130468"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1154.3321361681978
      y 974.9708405049597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 570.3049524499877
      y 741.9791910990839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_underscore_plus"
      name "H_underscore_plus"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa17"
      uniprot "NA"
    ]
    graphics [
      x 695.6085804282995
      y 729.8821733805659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_underscore_plus"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30864"
      hgnc "NA"
      map_id "(S)_minus_dihydroorotate"
      name "(S)_minus_dihydroorotate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 624.4839383541489
      y 455.79992002703807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(S)_minus_dihydroorotate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 544.2372383868068
      y 446.3821143052105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816;urn:miriam:obo.chebi:CHEBI%3A75909; urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:obo.chebi:CHEBI%3A75909;urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816"
      hgnc "HGNC_SYMBOL:MRE11;HGNC_SYMBOL:RAD50"
      map_id "UNIPROT:P49959;UNIPROT:Q92878"
      name "MRE11:RAD50:DNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24; csa10"
      uniprot "UNIPROT:P49959;UNIPROT:Q92878"
    ]
    graphics [
      x 821.6277250403908
      y 420.31193528654495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49959;UNIPROT:Q92878"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 796.962958575218
      y 1423.095627051583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "HCoVs_space_IFN_space_induction_space_WP4880"
      name "HCoVs_space_IFN_space_induction_space_WP4880"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 666.2302569529838
      y 1511.3137754749384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HCoVs_space_IFN_space_induction_space_WP4880"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 635.7031490315997
      y 1658.314667897688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:29622565"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_38"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 817.9306335546885
      y 1067.070171006309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:drugbank:DB08880"
      hgnc "NA"
      map_id "Teriflunomide"
      name "Teriflunomide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa76"
      uniprot "NA"
    ]
    graphics [
      x 857.9711021380601
      y 142.28132326502964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Teriflunomide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:29221122"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1103.2986887809534
      y 159.0147127771246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "DNA_space_damage"
      name "DNA_space_damage"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 1318.66009046212
      y 278.1650835797865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DNA_space_damage"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17625"
      hgnc "NA"
      map_id "dUTP"
      name "dUTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 1275.440948406365
      y 524.8898211459004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dUTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:18837522"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1133.3383960237088
      y 569.7831700390916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001025248;urn:miriam:ncbigene:1854;urn:miriam:ncbigene:1854;urn:miriam:hgnc:3078;urn:miriam:uniprot:P33316;urn:miriam:ensembl:ENSG00000128951;urn:miriam:hgnc.symbol:DUT;urn:miriam:hgnc.symbol:DUT;urn:miriam:ec-code:3.6.1.23"
      hgnc "HGNC_SYMBOL:DUT"
      map_id "UNIPROT:P33316"
      name "DUT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa54"
      uniprot "UNIPROT:P33316"
    ]
    graphics [
      x 1022.3733538546611
      y 575.9571863670834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P33316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17622"
      hgnc "NA"
      map_id "dUMP"
      name "dUMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa52"
      uniprot "NA"
    ]
    graphics [
      x 1060.0397612143172
      y 693.4032250193088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dUMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 981.1487747224895
      y 1227.6439295637897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30839"
      hgnc "NA"
      map_id "orotate"
      name "orotate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 421.5593109276533
      y 338.29192758632973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "orotate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 285.9463494450164
      y 540.5314796221138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "PRPP"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29; sa210; sa187; sa202"
      uniprot "NA"
    ]
    graphics [
      x 143.61136205568403
      y 518.660363029817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PRPP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:7372;urn:miriam:ec-code:2.4.2.10;urn:miriam:refseq:NM_000373;urn:miriam:hgnc.symbol:UMPS;urn:miriam:hgnc:12563;urn:miriam:ensembl:ENSG00000114491;urn:miriam:uniprot:P11172;urn:miriam:ec-code:4.1.1.23"
      hgnc "HGNC_SYMBOL:UMPS"
      map_id "UNIPROT:P11172"
      name "UMPS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P11172"
    ]
    graphics [
      x 400.7363969211192
      y 707.46680471497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15842"
      hgnc "NA"
      map_id "orotidine_space_5'_minus_monophosphate"
      name "orotidine_space_5'_minus_monophosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 455.43450621423926
      y 663.8128485137421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "orotidine_space_5'_minus_monophosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 371.5963913373372
      y 813.8976021495876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:28458037"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1585.390369058046
      y 562.6665289784537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1025.8241152561955
      y 1345.9104456340426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A75909"
      hgnc "NA"
      map_id "dsDNA"
      name "dsDNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 1608.594406338085
      y 687.8373956887838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsDNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_37"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1383.0653141084879
      y 893.318169850638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15918"
      hgnc "NA"
      map_id "dCMP"
      name "dCMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa53"
      uniprot "NA"
    ]
    graphics [
      x 928.0484989606892
      y 353.5825755138216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dCMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 904.1716747214969
      y 559.1903000505874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001012732;urn:miriam:uniprot:P32321;urn:miriam:ncbigene:1635;urn:miriam:ncbigene:1635;urn:miriam:hgnc:2710;urn:miriam:ensembl:ENSG00000129187;urn:miriam:hgnc.symbol:DCTD;urn:miriam:hgnc.symbol:DCTD;urn:miriam:ec-code:3.5.4.12"
      hgnc "HGNC_SYMBOL:DCTD"
      map_id "UNIPROT:P32321"
      name "DCTD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa57"
      uniprot "UNIPROT:P32321"
    ]
    graphics [
      x 775.831611033017
      y 607.9873894282734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P32321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:30842662;PUBMED:29241549"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 494.1782638770382
      y 496.70407540936117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_41"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 378.8291599663719
      y 536.6575215619532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816"
      hgnc "HGNC_SYMBOL:RAD50"
      map_id "UNIPROT:Q92878"
      name "RAD50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa97"
      uniprot "UNIPROT:Q92878"
    ]
    graphics [
      x 1183.4776428904102
      y 379.8176780021123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92878"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 1072.9067732242288
      y 394.01702806432655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0003690;urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:obo.chebi:CHEBI%3A75909; urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230"
      hgnc "HGNC_SYMBOL:MRE11"
      map_id "UNIPROT:P49959"
      name "MRE11:dsDNA; MRE11"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa23; sa96"
      uniprot "UNIPROT:P49959"
    ]
    graphics [
      x 1315.7060050782948
      y 420.59803867588096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49959"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 628.9147152122364
      y 785.5057026992689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:24809024"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1425.9149079966833
      y 517.6362304962036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28850"
      hgnc "NA"
      map_id "dUDP"
      name "dUDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 1473.4316055844192
      y 595.7671337477524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dUDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 943.4505192737626
      y 414.6192835081469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:29241549"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 280.96137662407364
      y 480.16788993882204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 598.77623482755
      y 212.519965289046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "CoQ"
      name "CoQ"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 705.3992768054611
      y 219.7230304108772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoQ"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1723;urn:miriam:hgnc.symbol:DHODH;urn:miriam:hgnc:2867"
      hgnc "HGNC_SYMBOL:DHODH"
      map_id "dihydroorotate_space_dehydrogenase_space_holoenzyme"
      name "dihydroorotate_space_dehydrogenase_space_holoenzyme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 662.6266398457531
      y 86.16514229747793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dihydroorotate_space_dehydrogenase_space_holoenzyme"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:31740051"
      hgnc "NA"
      map_id "P1788"
      name "P1788"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa64"
      uniprot "NA"
    ]
    graphics [
      x 609.5494401941567
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "P1788"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "QH2"
      name "QH2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 698.1416279216799
      y 138.6613727273501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "QH2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_97"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa179"
      uniprot "NA"
    ]
    graphics [
      x 1551.8968512521221
      y 608.944796766199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1519.1269941737335
      y 453.59001638241045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:13788541"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 1061.7727346963043
      y 235.25357069273298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:DCTPP1;urn:miriam:hgnc.symbol:DCTPP1;urn:miriam:hgnc:28777;urn:miriam:uniprot:Q9H773;urn:miriam:ensembl:ENSG00000179958;urn:miriam:ec-code:3.6.1.12;urn:miriam:refseq:NM_024096;urn:miriam:ncbigene:79077;urn:miriam:ncbigene:79077"
      hgnc "HGNC_SYMBOL:DCTPP1"
      map_id "UNIPROT:Q9H773"
      name "DCTPP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa56"
      uniprot "UNIPROT:Q9H773"
    ]
    graphics [
      x 954.3406440592473
      y 192.03827493893982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H773"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:32531208"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_67"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 1721.688327580874
      y 725.0923980989033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800;PUBMED:22394562"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 722.3091867030694
      y 933.6231003905452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050; urn:miriam:obo.chebi:CHEBI%3A58359"
      hgnc "NA"
      map_id "L_minus_Gln"
      name "L_minus_Gln"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1; sa197"
      uniprot "NA"
    ]
    graphics [
      x 432.7668270132468
      y 874.956523384685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Gln"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_73577"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 520.2542446952139
      y 969.8791995293072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17544"
      hgnc "NA"
      map_id "HCO3_underscore_minus"
      name "HCO3_underscore_minus"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa7"
      uniprot "NA"
    ]
    graphics [
      x 624.8658241165859
      y 919.030878631804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HCO3_underscore_minus"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015; urn:miriam:obo.chebi:CHEBI%3A29985"
      hgnc "NA"
      map_id "L_minus_Glu"
      name "L_minus_Glu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10; sa212"
      uniprot "NA"
    ]
    graphics [
      x 377.52908518743163
      y 932.9724421343351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Glu"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_74"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 889.892507047047
      y 1871.8036053344304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_52"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1546.2335728838661
      y 513.3017992326651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17013"
      hgnc "NA"
      map_id "dTMP"
      name "dTMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 1451.3602656563623
      y 996.9157440785567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dTMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:8024690"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1646.3024618993436
      y 1083.2242849581805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1841;urn:miriam:ncbigene:1841;urn:miriam:refseq:NM_012145;urn:miriam:hgnc.symbol:DTYMK;urn:miriam:ensembl:ENSG00000168393;urn:miriam:hgnc.symbol:DTYMK;urn:miriam:uniprot:P23919;urn:miriam:hgnc:3061;urn:miriam:ec-code:2.7.4.9"
      hgnc "HGNC_SYMBOL:DTYMK"
      map_id "UNIPROT:P23919"
      name "dTYMK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa61"
      uniprot "UNIPROT:P23919"
    ]
    graphics [
      x 1701.5215982600703
      y 1192.414755510083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23919"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:30842662"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_56"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 431.20668126826524
      y 555.111104588116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "LC3"
      name "LC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "NA"
    ]
    graphics [
      x 550.5078058819191
      y 580.766012116419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_015610;urn:miriam:hgnc:32225;urn:miriam:hgnc.symbol:WIPI2;urn:miriam:ensembl:ENSG00000157954;urn:miriam:ncbigene:26100;urn:miriam:uniprot:Q9Y4P8"
      hgnc "HGNC_SYMBOL:WIPI2"
      map_id "UNIPROT:Q9Y4P8"
      name "WIPI2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa93"
      uniprot "UNIPROT:Q9Y4P8"
    ]
    graphics [
      x 551.2333150460724
      y 515.3841862550656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y4P8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 232.1309257389372
      y 417.43172231536346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q9NR31;urn:miriam:ensembl:ENSG00000079332;urn:miriam:hgnc:10534;urn:miriam:hgnc.symbol:SAR1A;urn:miriam:refseq:NM_001142648;urn:miriam:ncbigene:56681"
      hgnc "HGNC_SYMBOL:SAR1A"
      map_id "UNIPROT:Q9NR31"
      name "SAR1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q9NR31"
    ]
    graphics [
      x 283.3987867454663
      y 299.9709318590177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NR31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1076.1812186302475
      y 1086.5526118637256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:17827303"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1444.837268084019
      y 460.2695089620676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 993.3881098581804
      y 1406.2327146405273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1879.329137034922
      y 721.7498071564478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0071897"
      hgnc "NA"
      map_id "DNA_space_biosynthesis"
      name "DNA_space_biosynthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 1846.692404171784
      y 819.4210659651774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DNA_space_biosynthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0019079"
      hgnc "NA"
      map_id "virus_space_replication"
      name "virus_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa143"
      uniprot "NA"
    ]
    graphics [
      x 1931.9981182642293
      y 594.8741664534125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "virus_space_replication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "DNA"
      name "DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 1950.592144262053
      y 814.6647981109356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:3099389"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1225.4505517997704
      y 855.4074529700596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:P04818;urn:miriam:ncbigene:7298;urn:miriam:ncbigene:7298;urn:miriam:hgnc.symbol:TYMS;urn:miriam:refseq:NM_001071;urn:miriam:hgnc.symbol:TYMS;urn:miriam:hgnc:12441;urn:miriam:ensembl:ENSG00000176890;urn:miriam:ec-code:2.1.1.45"
      hgnc "HGNC_SYMBOL:TYMS"
      map_id "UNIPROT:P04818"
      name "TYMS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59"
      uniprot "UNIPROT:P04818"
    ]
    graphics [
      x 1107.8897989744999
      y 864.3427233943893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04818"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 141
    source 11
    target 19
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "cytidine"
      target_id "M114_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P32320"
      target_id "M114_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 21
    target 23
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_16"
      target_id "uridine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 16
    target 24
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 12
    target 24
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M114_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 11
    target 25
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_69"
      target_id "HCoVs_space_autophagy_space_(WP4863)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 13
    target 27
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M114_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 11
    target 27
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 18
    target 28
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UTP"
      target_id "M114_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P17812"
      target_id "M114_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_26"
      target_id "CTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CDP"
      target_id "M114_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31350"
      target_id "M114_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 32
    target 34
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_30"
      target_id "dCDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 20
    target 35
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "cytidine"
      target_id "M114_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
      target_id "M114_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 35
    target 8
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_18"
      target_id "CMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 16
    target 37
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dTDP"
      target_id "M114_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_36"
      target_id "dTTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 14
    target 41
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 12
    target 41
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q14653"
      target_id "M114_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 14
    target 42
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_72"
      target_id "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 15
    target 44
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M114_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 23
    target 45
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "uridine"
      target_id "M114_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 16
    target 46
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_76"
      target_id "Interferon_space_1_space_pathway"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CAP"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Asp"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27708"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 49
    target 52
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_33"
      target_id "N_minus_carbamoyl_minus_L_minus_aspartate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 49
    target 4
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_33"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 8
    target 53
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CMP"
      target_id "M114_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30085"
      target_id "M114_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 53
    target 31
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_22"
      target_id "CDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 14
    target 55
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 55
    target 47
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_75"
      target_id "Interferon_space_1_space_pathway"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 34
    target 56
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dCDP"
      target_id "M114_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15531"
      target_id "M114_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_29"
      target_id "dCTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 31
    target 59
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CDP"
      target_id "M114_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 57
    target 59
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15531"
      target_id "M114_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 59
    target 30
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_25"
      target_id "CTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_and_space_transcription_space_pw"
      target_id "M114_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_68"
      target_id "RNA_space_biosynthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 16
    target 63
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UMP"
      target_id "M114_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 54
    target 65
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30085"
      target_id "M114_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 65
    target 7
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_21"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 23
    target 66
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "uridine"
      target_id "M114_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 36
    target 66
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
      target_id "M114_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 66
    target 64
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_17"
      target_id "UMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 52
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "N_minus_carbamoyl_minus_L_minus_aspartate"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "H_underscore_plus"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 51
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27708"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 67
    target 69
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_39"
      target_id "(S)_minus_dihydroorotate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 67
    target 2
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_39"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 11
    target 70
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49959;UNIPROT:Q92878"
      target_id "M114_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 14
    target 72
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_73"
      target_id "HCoVs_space_IFN_space_induction_space_WP4880"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 16
    target 74
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_70"
      target_id "HCoVs_space_IFN_space_induction_space_WP4880"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 5
    target 75
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 3
    target 75
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 9
    target 75
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8N884"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 75
    target 10
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_38"
      target_id "cGAMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "Teriflunomide"
      target_id "M114_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_66"
      target_id "DNA_space_damage"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dUTP"
      target_id "M114_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P33316"
      target_id "M114_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 80
    target 82
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_31"
      target_id "dUMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 15
    target 83
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q04206;UNIPROT:P19838"
      target_id "M114_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 12
    target 83
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M114_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "orotate"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "PRPP"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 87
    target 85
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P11172"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 85
    target 88
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_61"
      target_id "orotidine_space_5'_minus_monophosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 85
    target 1
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_61"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 10
    target 89
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "cGAMP"
      target_id "M114_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 11
    target 89
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 7
    target 90
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UDP"
      target_id "M114_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 57
    target 90
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15531"
      target_id "M114_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 90
    target 18
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_23"
      target_id "UTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 14
    target 91
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dsDNA"
      target_id "M114_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 9
    target 93
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N884"
      target_id "M114_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dCMP"
      target_id "M114_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P32321"
      target_id "M114_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 95
    target 82
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_65"
      target_id "dUMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 13
    target 97
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M114_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 13
    target 98
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UHD2"
      target_id "M114_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 11
    target 98
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92878"
      target_id "M114_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49959"
      target_id "M114_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 100
    target 71
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_53"
      target_id "UNIPROT:P49959;UNIPROT:Q92878"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 88
    target 102
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "orotidine_space_5'_minus_monophosphate"
      target_id "M114_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 87
    target 102
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P11172"
      target_id "M114_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 102
    target 64
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_71"
      target_id "UMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 102
    target 17
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_71"
      target_id "CO2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 7
    target 103
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UDP"
      target_id "M114_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 33
    target 103
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31350"
      target_id "M114_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_27"
      target_id "dUDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 71
    target 105
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49959;UNIPROT:Q92878"
      target_id "M114_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 11
    target 106
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 69
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "(S)_minus_dihydroorotate"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CoQ"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "dihydroorotate_space_dehydrogenase_space_holoenzyme"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "P1788"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 76
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "Teriflunomide"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 107
    target 84
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_49"
      target_id "orotate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 107
    target 111
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_49"
      target_id "QH2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_97"
      target_id "M114_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 78
    target 113
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "DNA_space_damage"
      target_id "M114_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 113
    target 92
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_64"
      target_id "dsDNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 58
    target 114
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dCTP"
      target_id "M114_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 115
    target 114
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9H773"
      target_id "M114_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 114
    target 94
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_32"
      target_id "dCMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 30
    target 116
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "CTP"
      target_id "M114_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 116
    target 60
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_67"
      target_id "RTC_space_and_space_transcription_space_pw"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 12
    target 117
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14653"
      target_id "M114_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 13
    target 117
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UHD2"
      target_id "M114_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Gln"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 120
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "HCO3_underscore_minus"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 2
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 5
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 51
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27708"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 119
    target 48
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "CAP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 119
    target 121
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "L_minus_Glu"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 119
    target 6
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 119
    target 4
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 16
    target 122
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01562"
      target_id "M114_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 122
    target 43
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_74"
      target_id "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 92
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dsDNA"
      target_id "M114_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 101
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49959"
      target_id "M114_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dTMP"
      target_id "M114_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23919"
      target_id "M114_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 125
    target 38
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_35"
      target_id "dTDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 11
    target 127
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "LC3"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 129
    target 127
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y4P8"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 11
    target 130
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86WV6"
      target_id "M114_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NR31"
      target_id "M114_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 20
    target 132
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "cytidine"
      target_id "M114_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 104
    target 133
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dUDP"
      target_id "M114_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 57
    target 133
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15531"
      target_id "M114_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 133
    target 79
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_28"
      target_id "dUTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 14
    target 134
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01574"
      target_id "M114_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 92
    target 135
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dsDNA"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 40
    target 135
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dTTP"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "DNA_space_biosynthesis"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 137
    target 135
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "virus_space_replication"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 135
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_60"
      target_id "DNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 82
    target 139
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "dUMP"
      target_id "M114_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P04818"
      target_id "M114_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 139
    target 124
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_34"
      target_id "dTMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
