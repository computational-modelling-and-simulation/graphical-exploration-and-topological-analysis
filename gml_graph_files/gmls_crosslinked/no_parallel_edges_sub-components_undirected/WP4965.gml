# generated with VANTED V2.8.2 at Fri Mar 04 09:59:53 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4799; WP4965; WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "Angiotensin_space_II"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b733a; bab13; f080e; ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 374.6104004639723
      y 148.38857640846481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4799; WP4965"
      full_annotation "urn:miriam:wikipathways:WP5035"
      hgnc "NA"
      map_id "Lung_space_injury"
      name "Lung_space_injury"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "dd819; a6535"
      uniprot "NA"
    ]
    graphics [
      x 160.8152008249979
      y 573.3456818475181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lung_space_injury"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000049115"
      hgnc "NA"
      map_id "Agtr1a"
      name "Agtr1a"
      node_subtype "GENE"
      node_type "species"
      org_id "e2b6c"
      uniprot "NA"
    ]
    graphics [
      x 134.00395538981212
      y 280.041156066447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Agtr1a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_15"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "idaa16182f"
      uniprot "NA"
    ]
    graphics [
      x 119.08485584838975
      y 446.16331605574334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_7"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c9c43"
      uniprot "NA"
    ]
    graphics [
      x 741.3661815474716
      y 369.51607949046434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_13"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id1fae067f"
      uniprot "NA"
    ]
    graphics [
      x 720.9103843880239
      y 249.17262378103547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000015405"
      hgnc "NA"
      map_id "Ace2"
      name "Ace2"
      node_subtype "GENE"
      node_type "species"
      org_id "f86f8"
      uniprot "NA"
    ]
    graphics [
      x 610.197536482353
      y 173.8745351016998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ace2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3c85f717"
      uniprot "NA"
    ]
    graphics [
      x 505.1545766797642
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "Angiotensin_minus_(1_minus_7)"
      name "Angiotensin_minus_(1_minus_7)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f34cf"
      uniprot "NA"
    ]
    graphics [
      x 512.6425253804912
      y 160.3404494303843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_minus_(1_minus_7)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_20"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfa0e6009"
      uniprot "NA"
    ]
    graphics [
      x 394.17016328169956
      y 328.50015184391214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000068122"
      hgnc "NA"
      map_id "Agtr2"
      name "Agtr2"
      node_subtype "GENE"
      node_type "species"
      org_id "c8f71"
      uniprot "NA"
    ]
    graphics [
      x 366.0808632765363
      y 478.95128608662367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Agtr2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720"
      hgnc "NA"
      map_id "Angiotensinogen"
      name "Angiotensinogen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b3515"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 378.8607070976817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensinogen"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc4c8fdab"
      uniprot "NA"
    ]
    graphics [
      x 193.09293563544693
      y 367.22516525264285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4965; WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0061196; urn:miriam:pubchem.compound:3081372"
      hgnc "NA"
      map_id "Angiotensin_space_I"
      name "Angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ad44a; c3eaf"
      uniprot "NA"
    ]
    graphics [
      x 321.61353610480177
      y 307.09885899318573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_19"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idf8eda287"
      uniprot "NA"
    ]
    graphics [
      x 286.8597058522284
      y 589.1882935351038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_1"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a2a1e"
      uniprot "NA"
    ]
    graphics [
      x 493.5698188519757
      y 274.6304203649678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "Angiotensin_minus_(1_minus_9)"
      name "Angiotensin_minus_(1_minus_9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e166c"
      uniprot "NA"
    ]
    graphics [
      x 547.8620938893777
      y 379.2375182814277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_minus_(1_minus_9)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc0a82d8"
      uniprot "NA"
    ]
    graphics [
      x 269.6299597238734
      y 205.37516827706213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000020681"
      hgnc "NA"
      map_id "Ace"
      name "Ace"
      node_subtype "GENE"
      node_type "species"
      org_id "ebb43"
      uniprot "NA"
    ]
    graphics [
      x 148.07513160870232
      y 155.78972872669294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ace"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_18"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcc7fc0b6"
      uniprot "NA"
    ]
    graphics [
      x 228.53994279351545
      y 144.34402758810415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 21
    source 3
    target 4
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Agtr1a"
      target_id "W12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 4
    target 2
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_15"
      target_id "Lung_space_injury"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 5
    target 6
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_7"
      target_id "W12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 6
    target 7
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_13"
      target_id "Ace2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 1
    target 8
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 7
    target 8
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "Ace2"
      target_id "W12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 8
    target 9
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_14"
      target_id "Angiotensin_minus_(1_minus_7)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 1
    target 10
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 10
    target 11
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_20"
      target_id "Agtr2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 12
    target 13
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensinogen"
      target_id "W12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 13
    target 14
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_17"
      target_id "Angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 11
    target 15
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Agtr2"
      target_id "W12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 15
    target 2
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_19"
      target_id "Lung_space_injury"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 14
    target 16
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_I"
      target_id "W12_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 7
    target 16
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "Ace2"
      target_id "W12_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 16
    target 17
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_1"
      target_id "Angiotensin_minus_(1_minus_9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 14
    target 18
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_I"
      target_id "W12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 19
    target 18
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "Ace"
      target_id "W12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 18
    target 1
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_16"
      target_id "Angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 1
    target 20
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 20
    target 3
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_18"
      target_id "Agtr1a"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
