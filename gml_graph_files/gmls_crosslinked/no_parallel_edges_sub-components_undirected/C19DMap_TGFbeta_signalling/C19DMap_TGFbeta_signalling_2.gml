# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92; sa55"
      uniprot "NA"
    ]
    graphics [
      x 216.51291056228658
      y 559.7928681806013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_18"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 115.118458207618
      y 509.68209871686076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOB;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:CUL2"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 62.5
      y 409.80146016686655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    ]
    graphics [
      x 189.37489145327567
      y 421.43042427069605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_18"
      target_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
