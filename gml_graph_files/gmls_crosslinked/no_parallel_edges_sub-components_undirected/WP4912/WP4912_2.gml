# generated with VANTED V2.8.2 at Fri Mar 04 10:06:58 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4912; WP4880"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "TICAM1"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "cea88; f9104; b7b77"
      uniprot "NA"
    ]
    graphics [
      x 1032.4942887845718
      y 856.1833474119937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TICAM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id95166c5e"
      uniprot "NA"
    ]
    graphics [
      x 917.1856489420077
      y 834.0778494440626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 789.399456997925
      y 834.8993051889342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbcd773be"
      uniprot "NA"
    ]
    graphics [
      x 653.958910049681
      y 833.9876049677958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "d1b60"
      name "d1b60"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b60"
      uniprot "NA"
    ]
    graphics [
      x 522.9712514829089
      y 872.2978409056902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d1b60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "ea539"
      name "ea539"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea539"
      uniprot "NA"
    ]
    graphics [
      x 646.7614208806556
      y 710.6428677493317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ea539"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:15681410"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_36"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided8e62a3"
      uniprot "NA"
    ]
    graphics [
      x 620.4347501875116
      y 595.3750330763162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "IFN_minus_beta_br_Response_space_element"
      name "IFN_minus_beta_br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "c7d27"
      uniprot "NA"
    ]
    graphics [
      x 516.0900802383703
      y 555.2662511960259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFN_minus_beta_br_Response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf94c3c70"
      uniprot "NA"
    ]
    graphics [
      x 395.5628413124558
      y 901.8658135665779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 5
      diagram "WP5038; WP4912; WP4868; WP4880; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "MAVS"
      name "MAVS"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "e240d; f0e60; fa763; a978e; ae650"
      uniprot "NA"
    ]
    graphics [
      x 276.63698355857747
      y 942.1915870091461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAVS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3573c07"
      uniprot "NA"
    ]
    graphics [
      x 173.46661424660425
      y 1009.033624739895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115267;urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "b9f09"
      name "b9f09"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9f09"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 995.9885186559152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b9f09"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_16"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ecbcf"
      uniprot "NA"
    ]
    graphics [
      x 193.28179122296757
      y 1122.4454550683017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 1
    target 2
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "TICAM1"
      target_id "W15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 2
    target 3
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_31"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 3
    target 4
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 5
    target 4
    cd19dm [
      diagram "WP4912"
      edge_type "CATALYSIS"
      source_id "d1b60"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 4
    target 6
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_34"
      target_id "ea539"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 9
    target 5
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_38"
      target_id "d1b60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 6
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "ea539"
      target_id "W15_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 7
    target 8
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_36"
      target_id "IFN_minus_beta_br_Response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 10
    target 9
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "MAVS"
      target_id "W15_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 11
    target 10
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_27"
      target_id "MAVS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 12
    target 11
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "b9f09"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 11
    cd19dm [
      diagram "WP4912"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W15_16"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
