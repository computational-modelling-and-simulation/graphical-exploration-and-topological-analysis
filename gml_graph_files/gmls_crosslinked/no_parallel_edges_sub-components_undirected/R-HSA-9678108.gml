# generated with VANTED V2.8.2 at Fri Mar 04 09:59:53 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 47
      diagram "R-HSA-9678108; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9682469;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694618;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694327;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9687385;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682451;urn:miriam:reactome:R-COV-9694597;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682616;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713302;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713651;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686016;urn:miriam:reactome:R-COV-9694302;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9681663;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694725; urn:miriam:reactome:R-COV-9694255;urn:miriam:reactome:R-COV-9680476;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684877;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694428; urn:miriam:reactome:R-COV-9682205;urn:miriam:reactome:R-COV-9694735;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713306;urn:miriam:reactome:R-COV-9713634;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713313;urn:miriam:reactome:R-COV-9713643;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694361;urn:miriam:reactome:R-COV-9682227;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686003;urn:miriam:reactome:R-COV-9694366;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694770;urn:miriam:reactome:R-COV-9686011;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713317;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713652;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9682229; urn:miriam:reactome:R-COV-9684862;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682241;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682253;urn:miriam:reactome:R-COV-9694404;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382;urn:miriam:uniprot:P0C6X7; urn:miriam:pubmed:31138817;urn:miriam:reactome:R-COV-9680807;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682545;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694688;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682215;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694358;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694731;urn:miriam:reactome:R-COV-9682668;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9713644;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713311;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658; urn:miriam:reactome:R-COV-9694269;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682566;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694458;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681659;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694784;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684866;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694760;urn:miriam:reactome:R-COV-9682222;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694577;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684876;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682245;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9680806;urn:miriam:pubmed:31138817;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9683433;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694740;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9683403;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694728;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684343;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694407; urn:miriam:reactome:R-COV-9681588;urn:miriam:uniprot:P0C6U8;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694479;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694748; urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      name "SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand; SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors; nsp7:nsp8:nsp12:nsp14:nsp10; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13; m7GpppA_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; RTC; SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer; SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors; nsp3; N_minus_glycan_space_nsp3; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC; N_minus_glycan_space_nsp4; nsp6; nsp3:nsp4; SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_1_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC; SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; nsp9; nsp9_space_dimer; nsp8; SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors; nsp7:nsp8:nsp12; nsp10:nsp14; nsp10; SARS_space_coronavirus_space_gRNA_space_with_space_secondary_space_structure:RTC; SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC; SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide; SARS_minus_CoV_minus_1_space_gRNA:RTC; nsp1_minus_4; nsp2; nsp1; nsp4; nsp7; nsp7:nsp8; nsp16:nsp10; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15; nsp3:nsp4:nsp6; 3CLp_space_dimer; 3CLp_space_dimer:Î±_minus_Ketoamides; nsp3_minus_4; N_minus_glycan_space_nsp3_minus_4; nsp5; PLPro"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_141; layout_1411; layout_113; layout_115; layout_2103; layout_458; layout_138; layout_1266; layout_53; layout_57; layout_2081; layout_2093; layout_66; layout_1488; layout_449; layout_450; layout_2074; layout_2097; layout_2099; layout_71; layout_74; layout_98; layout_170; layout_1383; layout_106; layout_111; layout_108; layout_124; layout_2086; layout_144; layout_132; layout_41; layout_81; layout_46; layout_44; layout_54; layout_101; layout_104; layout_121; layout_119; layout_451; layout_14; layout_504; layout_45; layout_50; layout_1897; sa101"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 997.7641106055339
      y 495.6196108482675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687388;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_368"
      name "SARS-CoV-1 gRNA:RTC:nascent RNA minus strand binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687388__layout_1410"
      uniprot "NA"
    ]
    graphics [
      x 986.9182536983552
      y 203.80427594955881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "RTC_space_inhibitors"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1483; layout_1486; layout_2503; layout_2506"
      uniprot "NA"
    ]
    graphics [
      x 1059.3015126634773
      y 79.46252600545404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RTC_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 21
      diagram "R-HSA-9678108; C19DMap:Nsp9 protein interactions; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694642;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678281; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684355;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678280; urn:miriam:reactome:R-COV-9684874;urn:miriam:reactome:R-COV-9694600;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682244;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682196;urn:miriam:reactome:R-COV-9694504;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684861;urn:miriam:reactome:R-COV-9694695;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682242;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694372;urn:miriam:reactome:R-COV-9682216;urn:miriam:uniprot:P0C6X7; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7; urn:miriam:doi:10.1101/2020.05.18.102467;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0C6X7"
      name "pp1ab_minus_nsp13; nsp15_space_hexamer; pp1ab_minus_nsp14; pp1ab; pp1ab_minus_nsp16; pp1ab_minus_nsp12; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; pp1ab_minus_nsp15; pp1ab_minus_nsp9; pp1ab_minus_nsp7; pp1ab_minus_nsp5; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp8; pp1ab_minus_nsp6; pp1ab_minus_nsp10; Nsp14; Nsp16; inhibited_space_ribosomal_space_40S_space_SU; Nsp1; inhibited_space_80S_space_ribosome"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_32; layout_117; layout_29; layout_5; layout_35; layout_34; layout_727; layout_30; layout_33; layout_37; layout_28; layout_27; layout_31; layout_2066; layout_38; sa1378; sa1426; sa1374; csa15; sa70; csa16"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 540.4178858569486
      y 412.26442934919805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682626;PUBMED:31131400;PUBMED:22615777;PUBMED:17520018"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_276"
      name "nsp13 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682626__layout_114"
      uniprot "NA"
    ]
    graphics [
      x 705.1254844468742
      y 395.44035222867376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SPO-2997551-3;urn:miriam:reactome:R-XTR-2997551;urn:miriam:reactome:R-SPO-2997551-2;urn:miriam:reactome:R-CEL-2997551;urn:miriam:reactome:R-SSC-2997551;urn:miriam:uniprot:P49841;urn:miriam:reactome:R-BTA-2997551;urn:miriam:reactome:R-SCE-2997551-2;urn:miriam:reactome:R-SCE-2997551-3;urn:miriam:reactome:R-SCE-2997551-4;urn:miriam:reactome:R-DRE-2997551;urn:miriam:reactome:R-SPO-2997551;urn:miriam:reactome:R-HSA-2997551;urn:miriam:reactome:R-DME-2997551;urn:miriam:reactome:R-RNO-198619;urn:miriam:reactome:R-SCE-2997551;urn:miriam:reactome:R-DME-2997551-2;urn:miriam:reactome:R-CEL-2997551-3;urn:miriam:reactome:R-DME-2997551-3;urn:miriam:reactome:R-CEL-2997551-4;urn:miriam:reactome:R-GGA-2997551;urn:miriam:reactome:R-CEL-2997551-2;urn:miriam:reactome:R-CFA-2997551;urn:miriam:reactome:R-MMU-2997551;urn:miriam:reactome:R-DDI-2997551; urn:miriam:reactome:R-HSA-9687663;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49841"
      name "GSK3B; GSK3B:GSKi"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_1717; layout_1895; layout_3593; layout_2349"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 696.774837941422
      y 1366.8667851515286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687724;PUBMED:34593624;PUBMED:19666099;PUBMED:19389332;PUBMED:11162580;PUBMED:24931005;PUBMED:18977324;PUBMED:18938143;PUBMED:19106108;PUBMED:18806775"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_370"
      name "GSK3B binds GSKi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687724__layout_1893"
      uniprot "NA"
    ]
    graphics [
      x 516.4091641871003
      y 1469.2665577564735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687688;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "GSKi"
      name "GSKi"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1894; layout_2949"
      uniprot "NA"
    ]
    graphics [
      x 432.12805060595866
      y 1568.207843224419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSKi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1414.0457323036308
      y 684.7005643411422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9695415;PUBMED:32619700;PUBMED:34052578;PUBMED:32345140;PUBMED:33497607;PUBMED:32149769;PUBMED:33609497;PUBMED:33303459;PUBMED:33309272"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_371"
      name "glycosylated-ACE2 binds ACE2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9695415__layout_2063"
      uniprot "NA"
    ]
    graphics [
      x 1418.090000948071
      y 446.4376937231062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9695417;urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499"
      hgnc "NA"
      map_id "ACE2_space_inhibitors"
      name "ACE2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2064"
      uniprot "NA"
    ]
    graphics [
      x 1457.0321139286102
      y 276.91715797063307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 15
      diagram "R-HSA-9678108; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:pubmed:15094372;urn:miriam:reactome:R-COV-9683649;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9694659; urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9684199;urn:miriam:reactome:R-COV-9697428;urn:miriam:reactome:R-COV-9694612;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9694300;urn:miriam:pubmed:15496142;urn:miriam:uniprot:P59595;urn:miriam:pubmed:12775768;urn:miriam:reactome:R-COV-9683625; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9694573;urn:miriam:reactome:R-COV-9684203;urn:miriam:reactome:R-COV-9697429;urn:miriam:uniprot:P59595; urn:miriam:reactome:R-COV-9684230;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P59595; urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683754;urn:miriam:pubmed:19106108; urn:miriam:pubmed:32654247;urn:miriam:pubmed:33264373;urn:miriam:pubmed:32416961;urn:miriam:pubmed:16112641;urn:miriam:hgnc.symbol:N;urn:miriam:pubmed:32363136;urn:miriam:uniprot:P59595;urn:miriam:pubmed:16845612;urn:miriam:ncbigene:1489678"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P59595"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer; SUMO_minus_p_minus_N_space_dimer; SUMO1_minus_K62_minus_ADPr_minus_p_minus_S177_minus_N; encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA; encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA_space_(plus_space_strand); N; ADPr_minus_p_minus_S177_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_space_coronavirus_space_genomic_space_RNA; p_minus_S177_minus_N"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_739; layout_263; layout_395; layout_261; layout_598; layout_863; layout_239; layout_2265; layout_394; layout_590; layout_591; layout_2261; layout_393; layout_250; sa74"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 764.8827707681937
      y 1121.7497459257622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59595"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683606;PUBMED:15848177"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_286"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683606__layout_262"
      uniprot "NA"
    ]
    graphics [
      x 615.0363467913955
      y 1084.9450876021506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 13
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623; urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408; urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754; urn:miriam:uniprot:P59637; urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E; urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321; urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P59637"
      name "3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; nascent_space_E; N_minus_glycan_space_E; E; Orf3a; SARS_space_E"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_362; layout_365; layout_371; layout_373; layout_233; layout_355; e5c2e; sa69; sa48; sa92; sa140; sa146; sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 800.1233079042166
      y 1627.8842938464059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683679;PUBMED:20409569"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_300"
      name "Ubiquination of protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683679__layout_363"
      uniprot "NA"
    ]
    graphics [
      x 839.3396131094493
      y 1704.3126567337135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P62987;urn:miriam:uniprot:P62979;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:P0CG47;urn:miriam:reactome:R-HSA-8943136"
      hgnc "NA"
      map_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      name "Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_364; layout_2409"
      uniprot "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    ]
    graphics [
      x 870.3236120907685
      y 1606.6403978777826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684033;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_322"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684033__layout_427"
      uniprot "NA"
    ]
    graphics [
      x 1391.3250786747603
      y 539.3906830939984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_methionine"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_422; layout_155; layout_182; layout_2299; layout_2282; layout_2431; layout_3787; layout_3766"
      uniprot "NA"
    ]
    graphics [
      x 1469.122819975514
      y 374.7626235397281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_methionine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_homocysteine"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_420; layout_158; layout_184; layout_2302; layout_2284; layout_2433; layout_3538"
      uniprot "NA"
    ]
    graphics [
      x 1514.2806230726928
      y 477.17149336345005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_homocysteine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685891;urn:miriam:reactome:R-COV-9694259;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
      name "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_429"
      uniprot "NA"
    ]
    graphics [
      x 1392.7738698872668
      y 734.3433829739868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681674;PUBMED:25197083;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_268"
      name "nsp12 synthesizes minus strand SARS-CoV-1 genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681674__layout_140"
      uniprot "NA"
    ]
    graphics [
      x 1022.7688248590466
      y 593.7628781797746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "NTP(4_minus_)"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_142; layout_149; layout_414; layout_174; layout_137; layout_3761; layout_2423; layout_2265; layout_2293; layout_2276"
      uniprot "NA"
    ]
    graphics [
      x 1045.129932872228
      y 686.1600774610308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NTP(4_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 1121.9020627959708
      y 677.3304169069671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "a_space_nucleotide_space_sugar"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_48; layout_67; layout_2210; layout_2224"
      uniprot "NA"
    ]
    graphics [
      x 830.5042495981759
      y 936.8885465398033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a_space_nucleotide_space_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684290;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_333"
      name "nsp3 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684290__layout_56"
      uniprot "NA"
    ]
    graphics [
      x 971.7532343480475
      y 877.9482419726296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 1006.21556761
      y 1157.9041010367055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_51; layout_69; layout_2213; layout_2227"
      uniprot "NA"
    ]
    graphics [
      x 826.1372369820522
      y 823.4251977451237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684030;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_320"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684030__layout_167"
      uniprot "NA"
    ]
    graphics [
      x 1374.1208269126003
      y 322.8419873215563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685513;urn:miriam:reactome:R-COV-9694603;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1941"
      uniprot "NA"
    ]
    graphics [
      x 1365.393999099544
      y 382.20905806307474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; C19DMap:JNK pathway; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9682868; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683594;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:reactome:R-COV-9683676;urn:miriam:pubmed:15367599;urn:miriam:uniprot:P59594; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683608;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683638;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9683768; urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683716;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722; urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S; urn:miriam:ncbigene:1489668;urn:miriam:pubmed:32275855;urn:miriam:pubmed:32075877;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32225176;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P59594"
      name "nascent_space_Spike; N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_N_minus_glycan_space_Spike; trimmed_space_N_minus_glycan_minus_PALM_minus_Spike; complex_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; trimmed_space_unfolded_space_N_minus_glycan_space_Spike; S"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_227; layout_273; layout_300; layout_302; layout_287; layout_293; layout_317; layout_279; sa78; sa76"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1393.4596967761763
      y 1329.9600357512995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683755;PUBMED:20129637;PUBMED:15831954;PUBMED:12775768"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_311"
      name "Spike protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683755__layout_271"
      uniprot "NA"
    ]
    graphics [
      x 1132.4078842986473
      y 1356.4414717508853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "nucleotide_minus_sugar"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_272; layout_389; layout_2366; layout_2418"
      uniprot "NA"
    ]
    graphics [
      x 979.5232204190365
      y 1441.1094297179816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleotide_minus_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_274; layout_388; layout_2368; layout_2420"
      uniprot "NA"
    ]
    graphics [
      x 963.5945109826748
      y 1368.6945506994998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684032;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_321"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684032__layout_185"
      uniprot "NA"
    ]
    graphics [
      x 1338.8056555781657
      y 530.6405323776664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694619;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9684636"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1914"
      uniprot "NA"
    ]
    graphics [
      x 1246.6930737586435
      y 705.9488318552098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685614; urn:miriam:reactome:R-ALL-9685610"
      hgnc "NA"
      map_id "CQ,_space_HCQ"
      name "CQ,_space_HCQ"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2035; layout_2029; layout_2931; layout_2932"
      uniprot "NA"
    ]
    graphics [
      x 1453.208176381348
      y 993.858809638157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ,_space_HCQ"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683478;PUBMED:21124966"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_285"
      name "CQ, HCQ diffuses from cytosol to endocytic vesicle lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683478__layout_2034"
      uniprot "NA"
    ]
    graphics [
      x 1538.7569529211173
      y 817.8906417776475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683634;PUBMED:15094372;PUBMED:15848177;PUBMED:23717688"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_291"
      name "Dimerisation of nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683634__layout_260"
      uniprot "NA"
    ]
    graphics [
      x 663.3564964613406
      y 1126.122324784323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686015;PUBMED:20542253;PUBMED:24991833;PUBMED:28738245;PUBMED:23943763"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_356"
      name "Nsp3, nsp4, and nsp6 produce replicative organelles"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686015__layout_1487"
      uniprot "NA"
    ]
    graphics [
      x 964.686511563801
      y 352.97855973047155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SSC-9660621;urn:miriam:reactome:R-DME-9660621-2;urn:miriam:reactome:R-CFA-9660621;urn:miriam:reactome:R-DRE-9660621;urn:miriam:reactome:R-MMU-9660621;urn:miriam:reactome:R-DDI-9660621;urn:miriam:reactome:R-HSA-9660621;urn:miriam:reactome:R-XTR-9660621;urn:miriam:reactome:R-CEL-9660621;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-RNO-9660621;urn:miriam:reactome:R-DME-9660621;urn:miriam:reactome:R-DME-9660621-3"
      hgnc "NA"
      map_id "UNIPROT:P06400"
      name "RB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_207; layout_2322"
      uniprot "UNIPROT:P06400"
    ]
    graphics [
      x 191.3608515149158
      y 547.6046687592527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682712;PUBMED:22301153"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_279"
      name "nsp15 binds RB1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682712__layout_206"
      uniprot "NA"
    ]
    graphics [
      x 286.7995459562278
      y 427.714591846742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694258;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-HSA-9682726;urn:miriam:pubmed:22301153;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "UNIPROT:P06400;UNIPROT:P0C6X7"
      name "nsp15:RB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_208"
      uniprot "UNIPROT:P06400;UNIPROT:P0C6X7"
    ]
    graphics [
      x 158.1406338460854
      y 476.2427714829464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06400;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682465;PUBMED:25197083"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_272"
      name "RTC completes synthesis of the minus strand genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682465__layout_150"
      uniprot "NA"
    ]
    graphics [
      x 920.1210637881505
      y 679.8752124731575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9697431;urn:miriam:reactome:R-COV-9685518;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1905; layout_1903"
      uniprot "NA"
    ]
    graphics [
      x 741.5219063575097
      y 739.2431669117841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1058.4584462908076
      y 777.3655126284958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681514;PUBMED:14647384;PUBMED:32142651;PUBMED:21068237;PUBMED:27550352;PUBMED:27277342;PUBMED:24227843;PUBMED:28414992;PUBMED:22496216"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_265"
      name "TMPRSS2 binds TMPRSS2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681514__layout_1043"
      uniprot "NA"
    ]
    graphics [
      x 822.0751901343034
      y 693.3119808528378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9682035"
      hgnc "NA"
      map_id "TMPRSS2_space_inhibitors"
      name "TMPRSS2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1044; layout_2925"
      uniprot "NA"
    ]
    graphics [
      x 666.971398075535
      y 584.7068264168616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMPRSS2_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1932"
      uniprot "NA"
    ]
    graphics [
      x 157.6263429555977
      y 1459.097087746452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682009;PUBMED:22362731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_270"
      name "ZCRB1 binds 5'UTR of SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682009__layout_200"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1544.5317548707762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9698376;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "UNIPROT:Q8TBF4"
      name "ZCRB1; ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand); ZCRB1:m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_201; layout_202; layout_2316; layout_3236"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 146.16978386332198
      y 1622.4899300088666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683635;PUBMED:21524776;PUBMED:21450821"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_292"
      name "E pentamer is transported to the Golgi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683635__layout_372"
      uniprot "NA"
    ]
    graphics [
      x 715.9910552096641
      y 1732.8888321745148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-5205651;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-DDI-5205651-2;urn:miriam:reactome:R-DDI-5205651-3;urn:miriam:reactome:R-RNO-5205651;urn:miriam:reactome:R-CEL-5205651;urn:miriam:reactome:R-PFA-5205651;urn:miriam:reactome:R-BTA-5205651;urn:miriam:reactome:R-HSA-5205651;urn:miriam:reactome:R-DDI-5205651;urn:miriam:reactome:R-SCE-5205651;urn:miriam:reactome:R-SSC-5205651;urn:miriam:reactome:R-MMU-5205651;urn:miriam:reactome:R-DRE-5205651;urn:miriam:reactome:R-CFA-5205651;urn:miriam:reactome:R-SPO-5205651; urn:miriam:reactome:R-CEL-5683641;urn:miriam:reactome:R-DDI-5683641-3;urn:miriam:reactome:R-DDI-5683641-2;urn:miriam:reactome:R-RNO-5683641;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-5683641;urn:miriam:reactome:R-SPO-5683641;urn:miriam:reactome:R-CFA-5683641;urn:miriam:reactome:R-GGA-5683641;urn:miriam:reactome:R-MMU-5683641;urn:miriam:reactome:R-DRE-5683641;urn:miriam:reactome:R-DDI-5683641;urn:miriam:reactome:R-PFA-5683641;urn:miriam:reactome:R-SCE-5683641;urn:miriam:reactome:R-SSC-5683641;urn:miriam:reactome:R-BTA-5683641"
      hgnc "NA"
      map_id "UNIPROT:Q9GZQ8"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1492; layout_1507; layout_2510; layout_2513"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 549.3596645844949
      y 267.3658397578222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687435;PUBMED:31231549;PUBMED:15331731;PUBMED:21799305;PUBMED:25135833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_369"
      name "Enhanced autophagosome formation"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687435__layout_1506"
      uniprot "NA"
    ]
    graphics [
      x 547.2689051493753
      y 484.6446280948543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; WP4880; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59636;urn:miriam:reactome:R-COV-9689378;urn:miriam:reactome:R-COV-9694649; urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636; urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59636"
      name "9b; Orf9; Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1900; e9876; sa362; sa77; sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 596.2088911664468
      y 623.4249647152445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59636"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:reactome:R-COV-9678168"
      hgnc "NA"
      map_id "UNIPROT:Q80H93"
      name "8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1496"
      uniprot "UNIPROT:Q80H93"
    ]
    graphics [
      x 423.9021249913828
      y 306.6224080274276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q80H93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q99570;urn:miriam:reactome:R-CEL-5683632;urn:miriam:reactome:R-HSA-5683632;urn:miriam:reactome:R-XTR-5683632;urn:miriam:reactome:R-DME-5683632;urn:miriam:reactome:R-RNO-5683632;urn:miriam:reactome:R-SPO-5683632;urn:miriam:reactome:R-MMU-5683632;urn:miriam:reactome:R-DDI-5683632;urn:miriam:reactome:R-CFA-5683632;urn:miriam:reactome:R-GGA-5683632;urn:miriam:reactome:R-DRE-5683632;urn:miriam:reactome:R-SCE-5683632;urn:miriam:uniprot:Q14457;urn:miriam:reactome:R-SSC-5683632;urn:miriam:uniprot:Q9P2Y5;urn:miriam:uniprot:Q8NEB9"
      hgnc "NA"
      map_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      name "UVRAG_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1644; layout_2515"
      uniprot "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    ]
    graphics [
      x 629.1455837087324
      y 678.8281906071345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-XTR-917723;urn:miriam:uniprot:Q9UQN3;urn:miriam:reactome:R-DDI-917723;urn:miriam:reactome:R-DME-917723;urn:miriam:uniprot:Q8WUX9;urn:miriam:reactome:R-SSC-917723;urn:miriam:reactome:R-GGA-917723;urn:miriam:uniprot:Q96CF2;urn:miriam:reactome:R-RNO-917723;urn:miriam:uniprot:Q9Y3E7;urn:miriam:uniprot:Q9BY43;urn:miriam:reactome:R-DRE-917723;urn:miriam:reactome:R-HSA-917723;urn:miriam:reactome:R-CFA-917723;urn:miriam:reactome:R-BTA-917723;urn:miriam:uniprot:Q9H444;urn:miriam:uniprot:Q96FZ7;urn:miriam:reactome:R-SPO-917723;urn:miriam:reactome:R-MMU-917723;urn:miriam:uniprot:O43633;urn:miriam:reactome:R-SCE-917723;urn:miriam:reactome:R-CEL-917723"
      hgnc "NA"
      map_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      name "ESCRT_minus_III"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1643; layout_2514"
      uniprot "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    ]
    graphics [
      x 692.8373537291935
      y 668.4858417422225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 12
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9684357;urn:miriam:uniprot:P0C6U8; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326; urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9678259; urn:miriam:reactome:R-COV-9684311;urn:miriam:reactome:R-COV-9694778;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678266;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678271;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678273;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678275;urn:miriam:uniprot:P0C6U8; urn:miriam:reactome:R-COV-9678272;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "UNIPROT:P0C6U8"
      name "pp1a_minus_nsp6_minus_11; pp1a; pp1a_space_dimer; pp1a_minus_nsp5; pp1a_minus_nsp1_minus_4; pp1a_minus_nsp7; pp1a_minus_nsp11; pp1a_minus_nsp6; pp1a_minus_nsp9; pp1a_minus_nsp8; pp1a_minus_nsp10"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_11; layout_3; layout_7; layout_12; layout_10; layout_23; layout_19; layout_21; layout_20; layout_24; layout_22; layout_75"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 699.3308123534489
      y 239.29975155166267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6U8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684238;PUBMED:16684538;PUBMED:18703211;PUBMED:19322648;PUBMED:16254320;PUBMED:15474033;PUBMED:15147946;PUBMED:18792806;PUBMED:31226023;PUBMED:16877062;PUBMED:16507314;PUBMED:17530462;PUBMED:15713601;PUBMED:15351485;PUBMED:31133031;PUBMED:25855243;PUBMED:18753196;PUBMED:16343974;PUBMED:15507643"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_326"
      name "E and N are recruited to the M lattice"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684238__layout_603"
      uniprot "NA"
    ]
    graphics [
      x 786.1551740655424
      y 1400.606263550609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; WP4864; WP4880"
      full_annotation "urn:miriam:reactome:R-COV-9684216;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694371; urn:miriam:reactome:R-COV-9694446;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9684206; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P59596; urn:miriam:pubmed:16442106;urn:miriam:reactome:R-COV-9683588;urn:miriam:reactome:R-COV-9694684;urn:miriam:uniprot:P59596; urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "UNIPROT:P59596"
      name "M_space_lattice; N_minus_glycan_space_M; M; nascent_space_M"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_602; layout_601; layout_600; layout_236; layout_387; b1ff7; ea9e0"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 535.4238187322719
      y 1225.384882882252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684226;urn:miriam:uniprot:P59637;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694491;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
      name "M_space_lattice:E_space_protein:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_605"
      uniprot "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1103.3439771133003
      y 1454.3063028367467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685681;PUBMED:12917450;PUBMED:12927536;PUBMED:14569023;PUBMED:26919232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_349"
      name "Synthesis of SARS-CoV-1 plus strand subgenomic mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685681__layout_415"
      uniprot "NA"
    ]
    graphics [
      x 1139.3764481360367
      y 592.2033491081195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684282;PUBMED:19153232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_332"
      name "nsp9 forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684282__layout_70"
      uniprot "NA"
    ]
    graphics [
      x 875.7637162235948
      y 523.2110653476927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684218;PUBMED:16442106;PUBMED:18703211;PUBMED:9658133;PUBMED:16254320;PUBMED:20154085;PUBMED:7721788;PUBMED:15474033;PUBMED:19534833;PUBMED:15147946;PUBMED:16877062;PUBMED:10799570;PUBMED:18753196;PUBMED:23700447;PUBMED:15507643"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_323"
      name "M protein oligomerization"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684218__layout_599"
      uniprot "NA"
    ]
    graphics [
      x 431.3946719676069
      y 1118.3905286396468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694585;urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596; urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694500;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9685506; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9684225;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694324;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596; urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9752958;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9685539"
      hgnc "NA"
      map_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      name "S1:S2:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_842; layout_729; layout_612; layout_616; layout_618"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1532.7397000925394
      y 977.0796443610143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686699;PUBMED:26311884"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_359"
      name "Fusion and Release of SARS-CoV-1 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686699__layout_861"
      uniprot "NA"
    ]
    graphics [
      x 1145.0291160398601
      y 932.977819525162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-1181251;urn:miriam:reactome:R-HSA-1181251;urn:miriam:reactome:R-XTR-1181251;urn:miriam:reactome:R-CFA-1181251;urn:miriam:reactome:R-GGA-1181251;urn:miriam:reactome:R-MMU-1181251;urn:miriam:reactome:R-RNO-1181251;urn:miriam:reactome:R-DRE-1181251;urn:miriam:reactome:R-DDI-1181251;urn:miriam:reactome:R-PFA-1181251;urn:miriam:uniprot:P55072;urn:miriam:reactome:R-SPO-1181251;urn:miriam:reactome:R-SSC-1181251;urn:miriam:reactome:R-SCE-1181251;urn:miriam:reactome:R-DME-1181251"
      hgnc "NA"
      map_id "UNIPROT:P55072"
      name "VCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1642; layout_2497"
      uniprot "UNIPROT:P55072"
    ]
    graphics [
      x 1204.3605478160655
      y 755.9479385530641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55072"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694534;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9686705"
      hgnc "NA"
      map_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59596"
      name "S1:S2:M:E:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_862"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59596"
    ]
    graphics [
      x 1167.0224865350594
      y 725.6248733921996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-HSA-9694758;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-HSA-9686692;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694585;urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596; urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-HSA-9686311;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_795; layout_714"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1412.0553985872125
      y 951.0842915217422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686710;PUBMED:22816037;PUBMED:19321428"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_361"
      name "Cleavage of S protein into S1:S2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686710__layout_835"
      uniprot "NA"
    ]
    graphics [
      x 1460.4451716578583
      y 848.0881850389878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717; urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767; urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "NA; HGNC_SYMBOL:CTSL"
      map_id "UNIPROT:P07711"
      name "Cathepsin_space_L1; CTSL:CTSL_space_inhibitors; CTSL"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_837; layout_1038; layout_2492; layout_2493; sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 1337.6917314473606
      y 705.0525978467265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07711"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687121;PUBMED:15331731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_366"
      name "nsp8 binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687121__layout_1491"
      uniprot "NA"
    ]
    graphics [
      x 771.9711742222213
      y 222.6963002207438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694566;urn:miriam:reactome:R-HSA-9687117;urn:miriam:uniprot:Q9GZQ8;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "UNIPROT:Q9GZQ8;UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      name "nsp8:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1493"
      uniprot "UNIPROT:Q9GZQ8;UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 740.8657055678306
      y 80.35597697685125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9GZQ8;UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687384;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_367"
      name "SARS-CoV-1 gRNA complement (minus strand):RTC binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687384__layout_1484"
      uniprot "NA"
    ]
    graphics [
      x 1100.7635801219326
      y 235.68317531314392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683467;PUBMED:9719345;PUBMED:32145363;PUBMED:16115318;PUBMED:28596841;PUBMED:25693996;PUBMED:32226290"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_284"
      name "CQ, HCQ are protonated to CQ2+, HCQ2+"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683467__layout_2027"
      uniprot "NA"
    ]
    graphics [
      x 1250.1264639235999
      y 1124.125945191508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:kegg.compound:24848403;urn:miriam:reactome:R-ALL-9685618;urn:miriam:kegg.compound:78435478"
      hgnc "NA"
      map_id "CQ2_plus_,_space_HCQ2_plus_"
      name "CQ2_plus_,_space_HCQ2_plus_"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2030; layout_2935"
      uniprot "NA"
    ]
    graphics [
      x 1378.3967214970207
      y 1154.3587139916183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ2_plus_,_space_HCQ2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682258;PUBMED:25197083;PUBMED:16549795"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_271"
      name "nsp14 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682258__layout_112"
      uniprot "NA"
    ]
    graphics [
      x 1113.5675138843321
      y 380.75352958335634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1969; layout_2328"
      uniprot "NA"
    ]
    graphics [
      x 1365.0855530682466
      y 839.7545344139648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683624;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_289"
      name "mRNA2 is translated to Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683624__layout_225"
      uniprot "NA"
    ]
    graphics [
      x 1432.9376177859936
      y 1083.4659270706143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9678128;PUBMED:14647384;PUBMED:16166518;PUBMED:32125455;PUBMED:22816037"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_260"
      name "Spike glycoprotein of SARS coronavirus binds ACE2 on host cell"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9678128__layout_712"
      uniprot "NA"
    ]
    graphics [
      x 1525.3116626202836
      y 866.9276145297242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683720;PUBMED:22548323;PUBMED:16507314"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_306"
      name "E protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683720__layout_361"
      uniprot "NA"
    ]
    graphics [
      x 1039.7604948473672
      y 1733.599043571118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683736;urn:miriam:obo.chebi:CHEBI%3A15525"
      hgnc "NA"
      map_id "palmitoyl_minus_CoA"
      name "palmitoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_291; layout_2379"
      uniprot "NA"
    ]
    graphics [
      x 1178.9166726539513
      y 1646.9691901085935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "palmitoyl_minus_CoA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-ALL-162743;urn:miriam:obo.chebi:CHEBI%3A57287; urn:miriam:obo.chebi:CHEBI%3A15346"
      hgnc "NA"
      map_id "CoA_minus_SH"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_292; layout_2380; sa71"
      uniprot "NA"
    ]
    graphics [
      x 1213.6207164771604
      y 1733.1242130821715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA_minus_SH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682544;PUBMED:25197083;PUBMED:22635272;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_273"
      name "nsp14 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682544__layout_107"
      uniprot "NA"
    ]
    graphics [
      x 704.6654998944412
      y 455.06215265686603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681314;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_264"
      name "Replication transcription complex binds SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681314__layout_122"
      uniprot "NA"
    ]
    graphics [
      x 843.0540863590396
      y 616.2952235795458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683670;PUBMED:15522242;PUBMED:22819936"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_299"
      name "Protein E forms a homopentamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683670__layout_370"
      uniprot "NA"
    ]
    graphics [
      x 859.3405588409422
      y 1755.4874213204444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684277;PUBMED:15680415"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_331"
      name "mRNA1 is translated to pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684277__layout_4"
      uniprot "NA"
    ]
    graphics [
      x 544.2973792515886
      y 599.1814849842744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681840;PUBMED:25197083;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_269"
      name "RTC synthesizes SARS-CoV-1 plus strand genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681840__layout_171"
      uniprot "NA"
    ]
    graphics [
      x 1183.7786875644877
      y 532.9831696075912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685597;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_346"
      name "RTC binds SARS-CoV-1 genomic RNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685597__layout_169"
      uniprot "NA"
    ]
    graphics [
      x 1196.8764418835858
      y 344.3055009614066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683714;PUBMED:16103198"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_303"
      name "Unphosphorylated nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683714__layout_2264"
      uniprot "NA"
    ]
    graphics [
      x 622.0458630658911
      y 1168.1185888012371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686711;PUBMED:15163706;PUBMED:15010527;PUBMED:22816037;PUBMED:15140961"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_362"
      name "Endocytois of SARS-CoV-1 Virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686711__layout_794"
      uniprot "NA"
    ]
    graphics [
      x 1291.4020236294796
      y 1020.8804399144672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 1044.8042188520471
      y 446.3396723549785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684017;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_318"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684017__layout_177"
      uniprot "NA"
    ]
    graphics [
      x 1292.856105196715
      y 494.60098762461246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 1444.653726830331
      y 510.4287826487341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 1302.9888638968282
      y 622.1560359940116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683764;PUBMED:31226023"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_313"
      name "Spike trimer translocates to ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683764__layout_301"
      uniprot "NA"
    ]
    graphics [
      x 1520.8842191745155
      y 1324.031677695021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682603;PUBMED:25197083;PUBMED:17927896;PUBMED:20463816;PUBMED:22635272;PUBMED:16549795;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_275"
      name "nsp14 acts as a 3'-to-5' exonuclease to remove misincorporated nucleotides from nascent RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682603__layout_146"
      uniprot "NA"
    ]
    graphics [
      x 915.6517127650179
      y 462.1594115110136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9689912;urn:miriam:obo.chebi:CHEBI%3A26558"
      hgnc "NA"
      map_id "NMP"
      name "NMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1998; layout_2274"
      uniprot "NA"
    ]
    graphics [
      x 924.0940352117856
      y 567.6545722966564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683653;PUBMED:17134730;PUBMED:20580052"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_294"
      name "Spike protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683653__layout_290"
      uniprot "NA"
    ]
    graphics [
      x 1327.0175710397623
      y 1598.5566675243058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165; urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165;urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279;UNIPROT:P63165"
      name "SUMO1:C93_minus_UBE2I"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1896; layout_3633"
      uniprot "UNIPROT:P63279;UNIPROT:P63165"
    ]
    graphics [
      x 367.45820878749566
      y 1301.9113324732832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279;UNIPROT:P63165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683687;PUBMED:15848177"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_301"
      name "Nucleoprotein is SUMOylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683687__layout_257"
      uniprot "NA"
    ]
    graphics [
      x 521.2943769480003
      y 1292.1279023693555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279"
      name "UBE2I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_259; layout_3792"
      uniprot "UNIPROT:P63279"
    ]
    graphics [
      x 403.55832878084095
      y 1364.0901444104377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684234;PUBMED:24418573;PUBMED:23717688;PUBMED:17229691;PUBMED:17379242;PUBMED:16873249"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_325"
      name "Encapsidation of SARS coronavirus genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684234__layout_597"
      uniprot "NA"
    ]
    graphics [
      x 706.4362418797115
      y 1222.834191635729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9682643"
      hgnc "NA"
      map_id "UNIPROT:P17844"
      name "DDX5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_204; layout_2319"
      uniprot "UNIPROT:P17844"
    ]
    graphics [
      x 748.1954992576805
      y 359.9602812235151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682631;PUBMED:19224332"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_277"
      name "nsp13 binds DDX5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682631__layout_203"
      uniprot "NA"
    ]
    graphics [
      x 617.6758363125823
      y 392.5686401348109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:19224332;urn:miriam:reactome:R-HSA-9682634;urn:miriam:uniprot:P17844;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-HSA-9694692"
      hgnc "NA"
      map_id "UNIPROT:P17844;UNIPROT:P0C6X7"
      name "nsp13:DDX5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_205"
      uniprot "UNIPROT:P17844;UNIPROT:P0C6X7"
    ]
    graphics [
      x 696.0453176543105
      y 305.3756743669875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17844;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:MN908947.3"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1972; layout_2337"
      uniprot "NA"
    ]
    graphics [
      x 290.3037696851303
      y 808.232748267054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683622;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_288"
      name "mRNA5 is translated to protein M"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683622__layout_234"
      uniprot "NA"
    ]
    graphics [
      x 357.0787266084615
      y 1027.1091248075033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684016;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_317"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684016__layout_417"
      uniprot "NA"
    ]
    graphics [
      x 1310.4788165292562
      y 436.4117252735022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681651;PUBMED:17024178;PUBMED:22039154"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_267"
      name "nsp8 generates RNA primers"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681651__layout_136"
      uniprot "NA"
    ]
    graphics [
      x 1080.0976198659102
      y 572.3883000628726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684321;PUBMED:12917450;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_336"
      name "nsp3 cleaves nsp1-4"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684321__layout_58"
      uniprot "NA"
    ]
    graphics [
      x 937.6667560792974
      y 403.3546869850889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680812;PUBMED:31138817;PUBMED:16228002"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_263"
      name "nsp7 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680812__layout_97"
      uniprot "NA"
    ]
    graphics [
      x 1023.1918223238699
      y 386.3943751792151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686709;PUBMED:31226023"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_360"
      name "Uncoating of SARS-CoV-1 Genome"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686709__layout_913"
      uniprot "NA"
    ]
    graphics [
      x 648.5886087834666
      y 1281.7315854335143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684443;urn:miriam:reactome:R-COV-9694712;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1923"
      uniprot "NA"
    ]
    graphics [
      x 560.3432394579393
      y 1378.937856992991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683455;PUBMED:25732088"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_283"
      name "nsp16 binds VHL"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683455__layout_214"
      uniprot "NA"
    ]
    graphics [
      x 360.40373516298894
      y 564.1893143517973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-391423;urn:miriam:reactome:R-DDI-391423;urn:miriam:reactome:R-MMU-391423;urn:miriam:reactome:R-DME-391423;urn:miriam:reactome:R-XTR-391423;urn:miriam:reactome:R-SSC-391423;urn:miriam:reactome:R-RNO-391423;urn:miriam:reactome:R-GGA-391423;urn:miriam:reactome:R-DRE-391423;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-BTA-391423;urn:miriam:reactome:R-HSA-391423;urn:miriam:reactome:R-CFA-391423"
      hgnc "NA"
      map_id "UNIPROT:P40337"
      name "VHL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_215; layout_2325"
      uniprot "UNIPROT:P40337"
    ]
    graphics [
      x 336.66289935007023
      y 729.1329566341806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694398;urn:miriam:reactome:R-HSA-9683453;urn:miriam:uniprot:P40337;urn:miriam:pubmed:25732088;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "UNIPROT:P40337;UNIPROT:P0C6X7"
      name "nsp16:VHL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_216"
      uniprot "UNIPROT:P40337;UNIPROT:P0C6X7"
    ]
    graphics [
      x 242.80331247019888
      y 614.7164092713366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40337;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1618.132509675269
      y 706.182427168251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686174;PUBMED:16840309;PUBMED:15807784;PUBMED:16894145;PUBMED:15781262;PUBMED:23202509;PUBMED:15194747"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_358"
      name "Accessory proteins are recruited to the maturing virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686174__layout_613"
      uniprot "NA"
    ]
    graphics [
      x 1709.1911275636521
      y 1001.1911691249596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:reactome:R-COV-9686193; urn:miriam:uniprot:P59635; urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "UNIPROT:P59635"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_614; c0237; sa84; sa76"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 1806.1440722514074
      y 1082.7984298464012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686310;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694321;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_670"
      uniprot "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1605.542062174898
      y 1237.377078101645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9686811"
      hgnc "NA"
      map_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      name "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1079; layout_2955"
      uniprot "NA"
    ]
    graphics [
      x 1057.242049744932
      y 1331.8733214663102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_minus_alpha_minus_glucosidase_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686790;PUBMED:16188993;PUBMED:24716661;PUBMED:23816430;PUBMED:19223639;PUBMED:23503623;PUBMED:7986008"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_364"
      name "ER-alpha glucosidases bind ER-alpha glucosidase inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686790__layout_1078"
      uniprot "NA"
    ]
    graphics [
      x 1138.540395701553
      y 1253.4535947909199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682983;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:pubmed:25348530;urn:miriam:uniprot:Q13724; urn:miriam:reactome:R-HSA-9686842;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      name "ER_space_alpha_minus_glucosidases; ER_minus_alpha_space_glucosidases:ER_minus_alpha_space_glucosidase_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_280; layout_1080; layout_2373; layout_2374"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 1165.415145841664
      y 1123.4115249167771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685639;PUBMED:12917450;PUBMED:16928755;PUBMED:12927536;PUBMED:14569023;PUBMED:25736566;PUBMED:26919232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_347"
      name "Synthesis of SARS-CoV-1 minus strand subgenomic mRNAs by template switching"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685639__layout_402"
      uniprot "NA"
    ]
    graphics [
      x 930.2326173058595
      y 770.8255709830037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683765;PUBMED:17210170"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_314"
      name "Nucleoprotein translocates to the ERGIC outer membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683765__layout_269"
      uniprot "NA"
    ]
    graphics [
      x 697.6335567570304
      y 1047.0564240159601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684351;PUBMED:15788388;PUBMED:21203998;PUBMED:27799534"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_341"
      name "pp1a cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684351__layout_8"
      uniprot "NA"
    ]
    graphics [
      x 869.6480970600519
      y 229.18399182910832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685542;PUBMED:31226023;PUBMED:16877062;PUBMED:25855243"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_345"
      name "Viral release"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685542__layout_617"
      uniprot "NA"
    ]
    graphics [
      x 1622.0737896942537
      y 1072.953880700449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685950;PUBMED:19398035;PUBMED:15194747;PUBMED:16212942"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_352"
      name "Endocytosis of protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685950__layout_586"
      uniprot "NA"
    ]
    graphics [
      x 1763.720822248788
      y 677.0286210267994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684241;PUBMED:24418573;PUBMED:22238235;PUBMED:17166901;PUBMED:27145752;PUBMED:25855243;PUBMED:20580052;PUBMED:20007283;PUBMED:18792806;PUBMED:16873249"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_327"
      name "Recruitment of Spike trimer to assembling virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684241__layout_611"
      uniprot "NA"
    ]
    graphics [
      x 1408.1499936894966
      y 1426.82676694027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683772;PUBMED:22915798"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_316"
      name "Trimmed spike protein binds to calnexin"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683772__layout_286"
      uniprot "NA"
    ]
    graphics [
      x 1590.9403808952345
      y 1390.6181111289243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-195906;urn:miriam:uniprot:P27824;urn:miriam:reactome:R-BTA-195906;urn:miriam:reactome:R-CFA-195906;urn:miriam:reactome:R-SPO-195906;urn:miriam:reactome:R-SSC-195906;urn:miriam:reactome:R-RNO-195906;urn:miriam:reactome:R-GGA-195906;urn:miriam:reactome:R-DRE-195906;urn:miriam:reactome:R-CEL-195906;urn:miriam:reactome:R-DDI-195906;urn:miriam:reactome:R-MMU-195906;urn:miriam:reactome:R-DME-195906;urn:miriam:reactome:R-XTR-195906;urn:miriam:reactome:R-DME-195906-2;urn:miriam:reactome:R-SCE-195906;urn:miriam:reactome:R-SPO-195906-2;urn:miriam:reactome:R-DME-195906-3;urn:miriam:reactome:R-SPO-195906-3;urn:miriam:reactome:R-DME-195906-4"
      hgnc "NA"
      map_id "UNIPROT:P27824"
      name "CANX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_288; layout_2377"
      uniprot "UNIPROT:P27824"
    ]
    graphics [
      x 1716.998138234241
      y 1305.4481562498404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27824"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680811;PUBMED:22318142;PUBMED:31138817"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_262"
      name "nsp12 binds nsp7 and nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680811__layout_105"
      uniprot "NA"
    ]
    graphics [
      x 764.8890178994973
      y 420.7200191733314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687109;PUBMED:31231549"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_365"
      name "8b binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687109__layout_1495"
      uniprot "NA"
    ]
    graphics [
      x 398.560420173161
      y 167.96764679541752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-9687111"
      hgnc "NA"
      map_id "UNIPROT:Q80H93;UNIPROT:Q9GZQ8"
      name "8b:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1497"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 337.8311827117668
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q80H93;UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683712;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_302"
      name "3a translocates to the ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683712__layout_341"
      uniprot "NA"
    ]
    graphics [
      x 1754.5713736807747
      y 752.9628431591426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684350;PUBMED:27799534;PUBMED:21203998"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_340"
      name "pp1a forms a dimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684350__layout_6"
      uniprot "NA"
    ]
    graphics [
      x 589.3713723895697
      y 169.9915605818726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686005;PUBMED:18255185;PUBMED:18827877"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_354"
      name "nsp3 binds to nsp7-8 and nsp12-16"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686005__layout_457"
      uniprot "NA"
    ]
    graphics [
      x 1072.9326250710592
      y 344.0325674571177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683719;PUBMED:15367599"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_305"
      name "Spike protein forms a homotrimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683719__layout_299"
      uniprot "NA"
    ]
    graphics [
      x 1467.3617592340424
      y 1196.92270602236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9693170;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "CTSL_space_inhibitors"
      name "CTSL_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2051; layout_2923"
      uniprot "NA"
    ]
    graphics [
      x 1253.604168055294
      y 411.49627399322264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CTSL_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685655;PUBMED:32179150;PUBMED:23105391;PUBMED:16962401;PUBMED:26335104;PUBMED:26953343"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_348"
      name "CTSL bind CTSL inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685655__layout_1036"
      uniprot "NA"
    ]
    graphics [
      x 1281.627303607884
      y 566.7671987606634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684309;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_335"
      name "3CLp cleaves nsp6-11"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684309__layout_17"
      uniprot "NA"
    ]
    graphics [
      x 884.6144486012344
      y 347.4327044668878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683630;PUBMED:16103198"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_290"
      name "Nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683630__layout_255"
      uniprot "NA"
    ]
    graphics [
      x 813.7414938900353
      y 1042.9538875743513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684229;PUBMED:24418573;PUBMED:17002283;PUBMED:15147189;PUBMED:17379242;PUBMED:15849181;PUBMED:18456656;PUBMED:15094372;PUBMED:19052082;PUBMED:16214138;PUBMED:16103198;PUBMED:15020242;PUBMED:18561946;PUBMED:23717688;PUBMED:17881296;PUBMED:18631359;PUBMED:16228284"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_324"
      name "SUMO-p-N protein dimer binds genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684229__layout_589"
      uniprot "NA"
    ]
    graphics [
      x 720.3893452835654
      y 928.6303128872561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685933"
      hgnc "NA"
      map_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      name "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_583; layout_2457"
      uniprot "NA"
    ]
    graphics [
      x 1490.7789760025198
      y 440.09860170389436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685956;PUBMED:16352545"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_353"
      name "3a is externalized together with membrane structures"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685956__layout_582"
      uniprot "NA"
    ]
    graphics [
      x 1639.146852391377
      y 533.2220368724227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 1043.3569696865477
      y 973.8320462254884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685906;PUBMED:27760233"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_350"
      name "Polyadenylation of SARS-CoV-1 subgenomic mRNAs (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685906__layout_430"
      uniprot "NA"
    ]
    graphics [
      x 1204.8688997456052
      y 859.0191443882567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694561;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685910"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_432"
      uniprot "NA"
    ]
    graphics [
      x 1198.858309220313
      y 1005.6133336784212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683429;PUBMED:20699222;PUBMED:22022266;PUBMED:26041293;PUBMED:21637813;PUBMED:21393853;PUBMED:22635272;PUBMED:16873246;PUBMED:20421945;PUBMED:16873247;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_282"
      name "nsp16 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683429__layout_120"
      uniprot "NA"
    ]
    graphics [
      x 753.0591631243556
      y 529.7050474892551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686013;PUBMED:18367524"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_355"
      name "Nsp3:nsp4 binds to nsp6"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686013__layout_448"
      uniprot "NA"
    ]
    graphics [
      x 1122.3910546427958
      y 436.515729754166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684352;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_342"
      name "nsp3-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684352__layout_52"
      uniprot "NA"
    ]
    graphics [
      x 823.2893549778104
      y 379.6602365831301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684301;PUBMED:15680415"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_334"
      name "mRNA1 is translated to pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684301__layout_1"
      uniprot "NA"
    ]
    graphics [
      x 680.1334281741447
      y 515.6426521894707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683751;PUBMED:16442106"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_310"
      name "M protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683751__layout_386"
      uniprot "NA"
    ]
    graphics [
      x 834.2079494985093
      y 1314.1441865020113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684275;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_330"
      name "nsp3-4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684275__layout_47"
      uniprot "NA"
    ]
    graphics [
      x 914.3926197546753
      y 842.7505761870991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682695;PUBMED:19224332;PUBMED:16579970;PUBMED:15140959;PUBMED:12917423;PUBMED:20671029;PUBMED:22615777"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_278"
      name "nsp13 helicase melts secondary structures in SARS-CoV-1 genomic RNA template"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682695__layout_130"
      uniprot "NA"
    ]
    graphics [
      x 1112.4265487309235
      y 802.1097103864943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 987.3987347317251
      y 1023.2418863887141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683393;PUBMED:28143984;PUBMED:16828802;PUBMED:16882730;PUBMED:18045871;PUBMED:16216269;PUBMED:18255185;PUBMED:17409150;PUBMED:22301153"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_281"
      name "nsp15 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683393__layout_118"
      uniprot "NA"
    ]
    graphics [
      x 761.7286720212023
      y 476.19971898820666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684273;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_329"
      name "3CLp cleaves pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684273__layout_15"
      uniprot "NA"
    ]
    graphics [
      x 928.9770763458469
      y 275.2980984955259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683718;PUBMED:19534833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_304"
      name "Protein M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683718__layout_374"
      uniprot "NA"
    ]
    graphics [
      x 398.734136360035
      y 1233.925287838628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682718;PUBMED:18045871;PUBMED:16882730;PUBMED:16828802;PUBMED:16216269;PUBMED:22301153;PUBMED:17409150"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_280"
      name "nsp15 forms a hexamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682718__layout_116"
      uniprot "NA"
    ]
    graphics [
      x 408.6922510776215
      y 389.9253288759466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694513;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685921; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694513;urn:miriam:reactome:R-COV-9685921"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      name "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1990; layout_2340"
      uniprot "NA"
    ]
    graphics [
      x 491.09351902212126
      y 750.5533192655249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683735;PUBMED:31226023;PUBMED:16877062;PUBMED:12917450;PUBMED:15848177;PUBMED:17210170;PUBMED:15496142;PUBMED:12775768"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_308"
      name "mRNA9 is translated to Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683735__layout_237"
      uniprot "NA"
    ]
    graphics [
      x 594.4977870341543
      y 930.4131568379885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686731;PUBMED:20926566;PUBMED:21325420;PUBMED:21068237"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_363"
      name "TMPRSS2 Mediated SARS-CoV-1 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686731__layout_892"
      uniprot "NA"
    ]
    graphics [
      x 1330.1869113796936
      y 884.3801019386847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681596;PUBMED:32045235;PUBMED:15507456"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_266"
      name "3CLp dimer binds Î±-Ketoamides"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681596__layout_577"
      uniprot "NA"
    ]
    graphics [
      x 1016.1963238206124
      y 322.98086112174457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683734;PUBMED:19534833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_307"
      name "Glycosylated M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683734__layout_587"
      uniprot "NA"
    ]
    graphics [
      x 388.8706758919429
      y 1175.6987858483653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683664;PUBMED:16103198;PUBMED:19106108"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_297"
      name "GSK3 phosphorylates Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683664__layout_247"
      uniprot "NA"
    ]
    graphics [
      x 891.0347331751389
      y 1177.4293774093173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P49840;urn:miriam:reactome:R-HSA-198358;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49840;UNIPROT:P49841"
      name "GSK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2262; layout_2314"
      uniprot "UNIPROT:P49840;UNIPROT:P49841"
    ]
    graphics [
      x 803.4810949217544
      y 1245.7263236055337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49840;UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685519;PUBMED:30918070;PUBMED:10799579;PUBMED:27760233"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_343"
      name "Polyadenylation of SARS-CoV-1 genomic RNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685519__layout_192"
      uniprot "NA"
    ]
    graphics [
      x 1022.3490941683069
      y 823.9239364285013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685531;PUBMED:16877062;PUBMED:9658133;PUBMED:10799570;PUBMED:31133031;PUBMED:16254320;PUBMED:25855243;PUBMED:18792806"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_344"
      name "SARS virus buds into ERGIC lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685531__layout_615"
      uniprot "NA"
    ]
    graphics [
      x 1649.4592557054757
      y 947.5676696809136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360; urn:miriam:obo.chebi:CHEBI%3A15846; urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "NAD_plus_"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439; layout_2443; sa3; sa222; sa256; sa108; sa45"
      uniprot "NA"
    ]
    graphics [
      x 536.8373765582187
      y 862.1602540261396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686061;PUBMED:29199039;PUBMED:32029454"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_357"
      name "Nucleoprotein is ADP-ribosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686061__layout_438"
      uniprot "NA"
    ]
    graphics [
      x 635.3552313455623
      y 997.9647283200404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CFA-8938273;urn:miriam:uniprot:Q53GL7;urn:miriam:reactome:R-HSA-8938273;urn:miriam:reactome:R-XTR-8938273;urn:miriam:uniprot:Q2NL67;urn:miriam:reactome:R-DDI-8938273;urn:miriam:uniprot:Q8N3A8;urn:miriam:reactome:R-GGA-8938273;urn:miriam:reactome:R-RNO-8938273;urn:miriam:reactome:R-MMU-8938273;urn:miriam:reactome:R-BTA-8938273;urn:miriam:reactome:R-DRE-8938273;urn:miriam:reactome:R-DME-8938259;urn:miriam:uniprot:Q8N5Y8;urn:miriam:uniprot:Q460N5;urn:miriam:uniprot:Q8IXQ6;urn:miriam:uniprot:Q9UKK3;urn:miriam:reactome:R-SSC-8938273"
      hgnc "NA"
      map_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      name "PARPs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_914; layout_2446"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    ]
    graphics [
      x 468.4623036280054
      y 921.3430483579677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277; urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "NAM"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_716; layout_2444; sa221; sa253"
      uniprot "NA"
    ]
    graphics [
      x 467.98962446908513
      y 856.0447516228646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683746;PUBMED:30761102"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_309"
      name "Protein 3a forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683746__layout_352"
      uniprot "NA"
    ]
    graphics [
      x 1701.8708273708803
      y 805.4940003618952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57705;urn:miriam:reactome:R-ALL-9683631"
      hgnc "NA"
      map_id "UDP_minus_N_minus_acetyl_minus_alpha_minus_D_minus_glucosamine(2âˆ’)"
      name "UDP_minus_N_minus_acetyl_minus_alpha_minus_D_minus_glucosamine(2âˆ’)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_316"
      uniprot "NA"
    ]
    graphics [
      x 1238.331583096804
      y 1496.1910262131164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus_N_minus_acetyl_minus_alpha_minus_D_minus_glucosamine(2âˆ’)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683648;PUBMED:20129637;PUBMED:15367599;PUBMED:15831954"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_293"
      name "Spike trimer glycoside chains are extended"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683648__layout_315"
      uniprot "NA"
    ]
    graphics [
      x 1235.248730228855
      y 1365.0746159054818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-964750;urn:miriam:reactome:R-XTR-964750;urn:miriam:reactome:R-GGA-964750;urn:miriam:reactome:R-DME-964750;urn:miriam:reactome:R-CEL-964750;urn:miriam:reactome:R-DDI-964750;urn:miriam:uniprot:P26572;urn:miriam:reactome:R-BTA-964750;urn:miriam:reactome:R-CFA-964750;urn:miriam:reactome:R-DRE-964750;urn:miriam:reactome:R-RNO-964750;urn:miriam:reactome:R-SSC-964750;urn:miriam:reactome:R-HSA-964750"
      hgnc "NA"
      map_id "UNIPROT:P26572"
      name "MGAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_320; layout_2390"
      uniprot "UNIPROT:P26572"
    ]
    graphics [
      x 1317.0675802052435
      y 1471.2281631501342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P26572"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078; urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659; urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "UDP"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319; layout_2389; sa22; sa35"
      uniprot "NA"
    ]
    graphics [
      x 1319.896295899781
      y 1271.987871869574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16846;urn:miriam:reactome:R-ALL-9683025"
      hgnc "NA"
      map_id "UDP_minus_GalNAc"
      name "UDP_minus_GalNAc"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_344; layout_2394"
      uniprot "NA"
    ]
    graphics [
      x 1296.3299259881685
      y 1184.899398269983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus_GalNAc"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683760;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_312"
      name "GalNAc is transferred onto 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683760__layout_343"
      uniprot "NA"
    ]
    graphics [
      x 1353.7082788460848
      y 1070.8222694967694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q10472;urn:miriam:reactome:R-HSA-9682906"
      hgnc "NA"
      map_id "UNIPROT:Q10472"
      name "GALNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_346; layout_2396"
      uniprot "UNIPROT:Q10472"
    ]
    graphics [
      x 1359.3330206217372
      y 1212.0195987357451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10472"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684261;PUBMED:17855519"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_328"
      name "nsp4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684261__layout_65"
      uniprot "NA"
    ]
    graphics [
      x 920.8988443763376
      y 910.4610867464212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682563;PUBMED:20463816"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_274"
      name "nsp12 misincorporates a nucleotide in nascent RNA minus strand"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682563__layout_143"
      uniprot "NA"
    ]
    graphics [
      x 976.5434621538695
      y 636.6521444670964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685939;PUBMED:19398035;PUBMED:15194747"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_351"
      name "3a localizes to the cell membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685939__layout_580"
      uniprot "NA"
    ]
    graphics [
      x 1723.9212623512099
      y 615.6493973596237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685917;urn:miriam:reactome:R-COV-9694622;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694622;urn:miriam:reactome:R-COV-9685917"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1971; layout_2334"
      uniprot "NA"
    ]
    graphics [
      x 1055.0699068915665
      y 1545.022191602127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683656;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_295"
      name "mRNA4 is translated to protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683656__layout_231"
      uniprot "NA"
    ]
    graphics [
      x 952.6269453874318
      y 1653.4327910155955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683663;PUBMED:25348530"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_296"
      name "N-glycan trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683663__layout_276"
      uniprot "NA"
    ]
    graphics [
      x 1234.5206402504514
      y 946.1181837311457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15903;urn:miriam:reactome:R-ALL-9683087"
      hgnc "NA"
      map_id "beta_minus_D_minus_glucose"
      name "beta_minus_D_minus_glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_278; layout_2372"
      uniprot "NA"
    ]
    graphics [
      x 1130.5620621077321
      y 1009.2353907835519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "beta_minus_D_minus_glucose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684336;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_338"
      name "nsp1-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684336__layout_39"
      uniprot "NA"
    ]
    graphics [
      x 867.769470298827
      y 297.05311484433804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684018;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_319"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684018__layout_153"
      uniprot "NA"
    ]
    graphics [
      x 1245.7140566740013
      y 473.40876451310885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684323;PUBMED:14965777;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_337"
      name "3CLp forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684323__layout_13"
      uniprot "NA"
    ]
    graphics [
      x 1109.151554697393
      y 496.5496890852951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1970; layout_2331"
      uniprot "NA"
    ]
    graphics [
      x 1269.5513870092261
      y 803.4244604373101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683618;PUBMED:12917450;PUBMED:15963240;PUBMED:16500894"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_287"
      name "mRNA3 is translated to protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683618__layout_228"
      uniprot "NA"
    ]
    graphics [
      x 1447.1176596522682
      y 782.9783813317534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683669;PUBMED:16684538;PUBMED:20129637"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_298"
      name "E protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683669__layout_354"
      uniprot "NA"
    ]
    graphics [
      x 883.3217767375897
      y 1447.6507152322051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684340;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_339"
      name "3CLp cleaves pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684340__layout_25"
      uniprot "NA"
    ]
    graphics [
      x 831.1744702441773
      y 448.6394727125025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680262;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_261"
      name "SARS coronavirus gRNA:RTC:RNA primer binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680262__layout_1260"
      uniprot "NA"
    ]
    graphics [
      x 1040.9238079337674
      y 231.31327092339575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683769;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_315"
      name "O-glycosylation of 3a is terminated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683769__layout_347"
      uniprot "NA"
    ]
    graphics [
      x 1393.0150143563114
      y 261.26407678721534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9699093;urn:miriam:obo.chebi:CHEBI%3A16556"
      hgnc "NA"
      map_id "CMP_minus_Neu5Ac"
      name "CMP_minus_Neu5Ac"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2068; layout_3350"
      uniprot "NA"
    ]
    graphics [
      x 1222.451382780568
      y 188.55437311952505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP_minus_Neu5Ac"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q16842;urn:miriam:reactome:R-HSA-9683042;urn:miriam:uniprot:Q11201;urn:miriam:uniprot:Q11203;urn:miriam:uniprot:Q9UJ37;urn:miriam:uniprot:Q9H4F1;urn:miriam:uniprot:P15907;urn:miriam:uniprot:Q8NDV1;urn:miriam:uniprot:Q11206"
      hgnc "NA"
      map_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      name "sialyltransferases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_351; layout_2401"
      uniprot "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    ]
    graphics [
      x 1270.587688328514
      y 148.39721851441664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094; urn:miriam:obo.chebi:CHEBI%3A17361"
      hgnc "NA"
      map_id "CMP"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2069; layout_3351; sa34"
      uniprot "NA"
    ]
    graphics [
      x 1372.7129304828682
      y 129.58033234142306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 202
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 3
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R1_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 4
    target 5
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 1
    target 5
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 6
    target 7
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49841"
      target_id "R1_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 8
    target 7
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "GSKi"
      target_id "R1_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 9
    target 10
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "R1_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 11
    target 10
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ACE2_space_inhibitors"
      target_id "R1_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 12
    target 13
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 14
    target 15
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 16
    target 15
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      target_id "R1_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 1
    target 17
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 18
    target 17
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 17
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_322"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 17
    target 20
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_322"
      target_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 1
    target 21
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 22
    target 21
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 21
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_268"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 24
    target 25
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R1_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 1
    target 25
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 25
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_333"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 25
    target 27
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_333"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 1
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 18
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 28
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_320"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 28
    target 29
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_320"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 30
    target 31
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 32
    target 31
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R1_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 31
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_311"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 31
    target 33
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_311"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 1
    target 34
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 18
    target 34
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 34
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_321"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 34
    target 35
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_321"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 36
    target 37
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R1_285"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 12
    target 38
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_291"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 1
    target 39
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 40
    target 41
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06400"
      target_id "R1_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 4
    target 41
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 41
    target 42
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_279"
      target_id "UNIPROT:P06400;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 1
    target 43
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 22
    target 43
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 43
    target 44
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_272"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 43
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_272"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 45
    target 46
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15393"
      target_id "R1_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 47
    target 46
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "TMPRSS2_space_inhibitors"
      target_id "R1_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 48
    target 49
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 50
    target 49
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBF4"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 14
    target 51
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_292"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 52
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 54
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59636"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 55
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q80H93"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 56
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 57
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 58
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 12
    target 59
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 60
    target 59
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 14
    target 59
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 59
    target 61
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_326"
      target_id "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 1
    target 62
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 22
    target 62
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 62
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_349"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 1
    target 63
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 60
    target 64
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "R1_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 65
    target 66
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 67
    target 66
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P55072"
      target_id "R1_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 66
    target 68
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_359"
      target_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 66
    target 12
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_359"
      target_id "UNIPROT:P59595"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 69
    target 70
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 71
    target 70
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07711"
      target_id "R1_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 70
    target 65
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_361"
      target_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 70
    target 9
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_361"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 52
    target 72
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R1_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 1
    target 72
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 72
    target 73
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_366"
      target_id "UNIPROT:Q9GZQ8;UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 1
    target 74
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 3
    target 74
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R1_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 26
    target 75
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "R1_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 36
    target 75
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R1_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 75
    target 76
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_284"
      target_id "CQ2_plus_,_space_HCQ2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 1
    target 77
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 78
    target 79
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      target_id "R1_289"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 79
    target 30
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_289"
      target_id "UNIPROT:P59594"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 9
    target 80
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "R1_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 65
    target 80
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 80
    target 69
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_260"
      target_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 14
    target 81
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 82
    target 81
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R1_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 81
    target 83
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_306"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 1
    target 84
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 4
    target 84
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 1
    target 85
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 44
    target 85
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 14
    target 86
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 44
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 87
    target 4
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_331"
      target_id "UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 1
    target 88
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 22
    target 88
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 88
    target 29
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 88
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 1
    target 89
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 29
    target 89
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      target_id "R1_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 12
    target 90
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 69
    target 91
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 92
    target 93
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 94
    target 93
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 1
    target 93
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 18
    target 93
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 93
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 93
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 93
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 30
    target 96
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 92
    target 97
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 1
    target 97
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 97
    target 98
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_275"
      target_id "NMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 30
    target 99
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 82
    target 99
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R1_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 99
    target 83
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_294"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 100
    target 101
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P63279;UNIPROT:P63165"
      target_id "R1_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 12
    target 101
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 101
    target 102
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_301"
      target_id "UNIPROT:P63279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 12
    target 103
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 104
    target 105
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17844"
      target_id "R1_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 4
    target 105
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 105
    target 106
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_277"
      target_id "UNIPROT:P17844;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 107
    target 108
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      target_id "R1_288"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 108
    target 60
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_288"
      target_id "UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 1
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 92
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 94
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 18
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 109
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 109
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 109
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 1
    target 110
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 22
    target 110
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 110
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_267"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 92
    target 111
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 1
    target 111
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 1
    target 112
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 12
    target 113
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 113
    target 114
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_360"
      target_id "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 4
    target 115
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 116
    target 115
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P40337"
      target_id "R1_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 115
    target 117
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_283"
      target_id "UNIPROT:P40337;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 118
    target 119
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 120
    target 119
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59635"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 121
    target 119
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 119
    target 65
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_358"
      target_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 122
    target 123
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      target_id "R1_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 124
    target 123
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R1_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 1
    target 125
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 22
    target 125
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 12
    target 125
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59595"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 125
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_347"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 12
    target 126
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 58
    target 127
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 92
    target 127
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 65
    target 128
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 118
    target 129
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 61
    target 130
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 30
    target 130
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 130
    target 121
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_327"
      target_id "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 30
    target 131
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 132
    target 131
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27824"
      target_id "R1_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 1
    target 133
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 4
    target 133
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 52
    target 134
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R1_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 55
    target 134
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q80H93"
      target_id "R1_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 134
    target 135
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_365"
      target_id "UNIPROT:Q80H93;UNIPROT:Q9GZQ8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 118
    target 136
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 58
    target 137
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 1
    target 138
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 30
    target 139
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 140
    target 141
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "CTSL_space_inhibitors"
      target_id "R1_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 71
    target 141
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07711"
      target_id "R1_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 92
    target 142
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 58
    target 142
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 1
    target 142
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 12
    target 143
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 44
    target 144
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 12
    target 144
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 145
    target 146
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      target_id "R1_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 118
    target 146
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 147
    target 148
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R1_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 20
    target 148
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
      target_id "R1_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 148
    target 149
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_350"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 148
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_350"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 1
    target 150
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 4
    target 150
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 1
    target 151
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 92
    target 152
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 1
    target 152
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 4
    target 152
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 44
    target 153
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 153
    target 58
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_334"
      target_id "UNIPROT:P0C6U8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 60
    target 154
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "R1_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 32
    target 154
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R1_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 154
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_310"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 154
    target 33
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_310"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 24
    target 155
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R1_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 1
    target 155
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 155
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_330"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 155
    target 27
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_330"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 1
    target 156
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 147
    target 156
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R1_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 156
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_278"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 156
    target 157
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_278"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 4
    target 158
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 1
    target 158
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 58
    target 159
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 92
    target 159
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 1
    target 159
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 60
    target 160
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "R1_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 4
    target 161
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 162
    target 163
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      target_id "R1_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 163
    target 12
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_308"
      target_id "UNIPROT:P59595"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 69
    target 164
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 45
    target 164
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15393"
      target_id "R1_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 164
    target 9
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_363"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 164
    target 65
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_363"
      target_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 1
    target 165
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 60
    target 166
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "R1_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 147
    target 167
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 12
    target 167
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 168
    target 167
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49840;UNIPROT:P49841"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 6
    target 167
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "UNIPROT:P49841"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 167
    target 157
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_297"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 167
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_297"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 147
    target 169
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R1_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 35
    target 169
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 169
    target 44
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_343"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 169
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_343"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 65
    target 170
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
      target_id "R1_344"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 171
    target 172
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 12
    target 172
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 173
    target 172
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 172
    target 174
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_357"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 172
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_357"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 118
    target 175
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 176
    target 177
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus_N_minus_acetyl_minus_alpha_minus_D_minus_glucosamine(2âˆ’)"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 30
    target 177
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 178
    target 177
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P26572"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 177
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_293"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 177
    target 179
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_293"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 180
    target 181
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus_GalNAc"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 118
    target 181
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 182
    target 181
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10472"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 181
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_312"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 181
    target 179
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_312"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 24
    target 183
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R1_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 1
    target 183
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 183
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_328"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 183
    target 27
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_328"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 1
    target 184
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 22
    target 184
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 184
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_274"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 118
    target 185
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 186
    target 187
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      target_id "R1_295"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 187
    target 14
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_295"
      target_id "UNIPROT:P59637"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 92
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 30
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59594"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 124
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 188
    target 189
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_296"
      target_id "beta_minus_D_minus_glucose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 92
    target 190
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 1
    target 190
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 58
    target 190
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6U8"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 92
    target 191
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 94
    target 191
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 18
    target 191
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 1
    target 191
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 191
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 191
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 191
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 1
    target 192
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 193
    target 194
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      target_id "R1_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 194
    target 118
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_287"
      target_id "UNIPROT:P59632"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 14
    target 195
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59637"
      target_id "R1_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 32
    target 195
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R1_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 195
    target 26
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_298"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 195
    target 33
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_298"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 4
    target 196
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 92
    target 196
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 1
    target 196
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 1
    target 197
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
      target_id "R1_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 3
    target 197
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R1_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 118
    target 198
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 199
    target 198
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "CMP_minus_Neu5Ac"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 200
    target 198
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 198
    target 201
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_315"
      target_id "CMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
