# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4860; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:wikidata:Q25100575; urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "NA"
      map_id "Pevonedistat"
      name "Pevonedistat"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "dda75; sa54"
      uniprot "NA"
    ]
    graphics [
      x 196.13209180335932
      y 79.44513423185956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_6"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idc583d98b"
      uniprot "NA"
    ]
    graphics [
      x 85.38872197103086
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:ensembl:ENSG00000159593"
      hgnc "NA"
      map_id "c466c"
      name "c466c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c466c"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 172.08306111659834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c466c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "Pevonedistat"
      target_id "W5_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_6"
      target_id "c466c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
