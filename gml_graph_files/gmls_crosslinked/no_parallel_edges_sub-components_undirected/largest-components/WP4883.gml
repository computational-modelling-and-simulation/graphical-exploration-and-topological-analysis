# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2"
      name "SARS_minus_CoV_minus_2"
      node_subtype "UNKNOWN; COMPLEX"
      node_type "species"
      org_id "c4774; aae40; e2279; de4ce"
      uniprot "NA"
    ]
    graphics [
      x 1001.5321553332835
      y 635.1574332832884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_26"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id27ade87d"
      uniprot "NA"
    ]
    graphics [
      x 1131.185963751885
      y 592.6419274247568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_33"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id770baa8e"
      uniprot "NA"
    ]
    graphics [
      x 878.6963809737981
      y 582.432427815774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a0ca7"
      uniprot "NA"
    ]
    graphics [
      x 1084.9505045246551
      y 733.1963540535144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_34"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id7806bdcd"
      uniprot "NA"
    ]
    graphics [
      x 1057.3552350733817
      y 498.583590739935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_39"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddb6a1659"
      uniprot "NA"
    ]
    graphics [
      x 862.4768258014926
      y 705.9566845749864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf45aaf4a"
      uniprot "NA"
    ]
    graphics [
      x 960.2763983055871
      y 525.3373381256115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4883; WP4969; WP4912; WP5039; WP4853"
      full_annotation "urn:miriam:ncbigene:59272; urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272; urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "ACE2"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "bf1a9; aa820; dc981; c2d8e; a9be1; f1b6b; f3245"
      uniprot "NA"
    ]
    graphics [
      x 722.6655962683492
      y 750.2756813195238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_28"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2fc925af"
      uniprot "NA"
    ]
    graphics [
      x 706.0951516697984
      y 871.0046140950062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc649fb6a"
      uniprot "NA"
    ]
    graphics [
      x 595.1477311757316
      y 782.5735364597708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:59272"
      hgnc "NA"
      map_id "Soluble_space_ACE2"
      name "Soluble_space_ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "dac5e"
      uniprot "NA"
    ]
    graphics [
      x 542.4672740967218
      y 888.3954171817086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Soluble_space_ACE2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "Accumulation_space_of_space__br_angiotensin"
      name "Accumulation_space_of_space__br_angiotensin"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "db780"
      uniprot "NA"
    ]
    graphics [
      x 808.6012846685551
      y 908.1411751065652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Accumulation_space_of_space__br_angiotensin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "Spike_space_vaccine"
      name "Spike_space_vaccine"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "bc845"
      uniprot "NA"
    ]
    graphics [
      x 1085.1459300667177
      y 382.8263049228293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Spike_space_vaccine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90710"
      hgnc "NA"
      map_id "ARBs_space_surface_space__br_receptor_space_blocker"
      name "ARBs_space_surface_space__br_receptor_space_blocker"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cf757"
      uniprot "NA"
    ]
    graphics [
      x 1197.0984160383837
      y 745.3135587723237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ARBs_space_surface_space__br_receptor_space_blocker"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90710"
      hgnc "NA"
      map_id "ACE2_space_surface_space__br_receptor_space_blocker"
      name "ACE2_space_surface_space__br_receptor_space_blocker"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d2e3a"
      uniprot "NA"
    ]
    graphics [
      x 778.1509490755172
      y 520.2338233686735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2_space_surface_space__br_receptor_space_blocker"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A35222"
      hgnc "NA"
      map_id "TMPRSS2_space_inhibitor"
      name "TMPRSS2_space_inhibitor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a45e2"
      uniprot "NA"
    ]
    graphics [
      x 1225.0403104599204
      y 522.2586170553772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMPRSS2_space_inhibitor"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 17
    source 2
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_26"
      target_id "SARS_minus_CoV_minus_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_33"
      target_id "SARS_minus_CoV_minus_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 4
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_2"
      target_id "SARS_minus_CoV_minus_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 5
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_34"
      target_id "SARS_minus_CoV_minus_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 1
    target 6
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2"
      target_id "W11_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 7
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2"
      target_id "W11_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 16
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "TMPRSS2_space_inhibitor"
      target_id "W11_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 15
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "ACE2_space_surface_space__br_receptor_space_blocker"
      target_id "W11_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 14
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "ARBs_space_surface_space__br_receptor_space_blocker"
      target_id "W11_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 13
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "Spike_space_vaccine"
      target_id "W11_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 6
    target 8
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_39"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 8
    target 9
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "ACE2"
      target_id "W11_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 8
    target 10
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "ACE2"
      target_id "W11_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 9
    target 12
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_28"
      target_id "Accumulation_space_of_space__br_angiotensin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 10
    target 11
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_38"
      target_id "Soluble_space_ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
