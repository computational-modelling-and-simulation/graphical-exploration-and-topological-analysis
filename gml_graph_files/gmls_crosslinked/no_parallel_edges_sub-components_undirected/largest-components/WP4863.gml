# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H492; urn:miriam:obo.chebi:16038;urn:miriam:uniprot:Q9H492; urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492"
      hgnc "NA; HGNC_SYMBOL:MAP1LC3A"
      map_id "UNIPROT:Q9H492"
      name "LC3; b35a3; MAP1LC3A"
      node_subtype "GENE; COMPLEX; PROTEIN"
      node_type "species"
      org_id "b3be1; b35a3; path_0_sa239"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 183.9879453277511
      y 433.2614905128794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H492"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7b795d10"
      uniprot "NA"
    ]
    graphics [
      x 69.20830235837946
      y 494.3102440314075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038"
      hgnc "NA"
      map_id "PE"
      name "PE"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa8aa"
      uniprot "NA"
    ]
    graphics [
      x 178.38487778412917
      y 530.3360033824863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8WYN0;urn:miriam:uniprot:O95352;urn:miriam:uniprot:Q9NT62"
      hgnc "NA"
      map_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
      name "b392b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b392b"
      uniprot "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      x 62.5
      y 627.444214975815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1056"
      uniprot "NA"
    ]
    graphics [
      x 135.54860940789501
      y 738.2755894881559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817;urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
      name "e7545"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7545"
      uniprot "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 256.8122157601574
      y 798.8730809315333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f0c56"
      uniprot "NA"
    ]
    graphics [
      x 396.20264997440876
      y 803.115199461788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
      name "a2002"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a2002"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      x 505.85568743169165
      y 721.1900868866852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
      name "ac403"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac403"
      uniprot "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 366.90303184804543
      y 697.4154532744528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a613c"
      uniprot "NA"
    ]
    graphics [
      x 590.182661173465
      y 615.3514906267072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "UNIPROT:O94817"
      name "ATG12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2b7f"
      uniprot "UNIPROT:O94817"
    ]
    graphics [
      x 625.8629111291514
      y 498.6643793575654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O94817"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4863; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9H1Y0; urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849"
      hgnc "NA; HGNC_SYMBOL:ATG5"
      map_id "UNIPROT:Q9H1Y0"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c82d3; path_0_sa241"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 702.315943756189
      y 559.4868694210729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H1Y0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H0Y0;urn:miriam:uniprot:O95352"
      hgnc "NA"
      map_id "UNIPROT:Q9H0Y0;UNIPROT:O95352"
      name "e453d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e453d"
      uniprot "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    ]
    graphics [
      x 701.6880377325713
      y 659.872194555824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 1
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H492"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 3
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "PE"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 4
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 5
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_25"
      target_id "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 6
    target 5
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
      target_id "W7_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 7
    target 6
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_24"
      target_id "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 8
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 9
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 10
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_2"
      target_id "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 11
    target 10
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O94817"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 12
    target 10
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H1Y0"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 10
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9H0Y0;UNIPROT:O95352"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
