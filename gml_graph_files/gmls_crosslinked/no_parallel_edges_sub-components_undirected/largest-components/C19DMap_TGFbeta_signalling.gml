# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 13
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:Apoptosis pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626; urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623; urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408; urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754; urn:miriam:uniprot:P59637; urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E; urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321; urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P59637"
      name "3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; nascent_space_E; N_minus_glycan_space_E; E; Orf3a; SARS_space_E"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_362; layout_365; layout_371; layout_373; layout_233; layout_355; e5c2e; sa69; sa48; sa92; sa140; sa146; sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 948.2064753207092
      y 208.7036283996584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1043.0929382370375
      y 270.98124060750035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD5;HGNC_SYMBOL:SMAD9"
      map_id "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    ]
    graphics [
      x 1103.2731992073077
      y 167.7303379545334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13145;urn:miriam:uniprot:Q13145;urn:miriam:refseq:NM_012342;urn:miriam:ensembl:ENSG00000095739;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc:30251"
      hgnc "HGNC_SYMBOL:BAMBI"
      map_id "UNIPROT:Q13145"
      name "BAMBI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q13145"
    ]
    graphics [
      x 1078.4449963071452
      y 486.79498664126135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:169;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:uniprot:P61160;urn:miriam:uniprot:P61160;urn:miriam:ncbigene:10097;urn:miriam:ncbigene:10097;urn:miriam:ensembl:ENSG00000138071;urn:miriam:refseq:NM_001005386;urn:miriam:ncbigene:659;urn:miriam:ncbigene:659;urn:miriam:ensembl:ENSG00000204217;urn:miriam:hgnc:1078;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q13873;urn:miriam:uniprot:Q13873;urn:miriam:refseq:NM_001204;urn:miriam:hgnc.symbol:BMPR2;urn:miriam:hgnc.symbol:BMPR2"
      hgnc "HGNC_SYMBOL:ACTR2;HGNC_SYMBOL:BMPR2"
      map_id "UNIPROT:P61160;UNIPROT:Q13873"
      name "BMPR1_slash_2_slash_ACTR2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P61160;UNIPROT:Q13873"
    ]
    graphics [
      x 1016.2961860627802
      y 145.36715462949508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61160;UNIPROT:Q13873"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000102882;urn:miriam:hgnc:6877;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:refseq:NM_001040056;urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5595;urn:miriam:uniprot:P27361;urn:miriam:uniprot:P27361"
      hgnc "HGNC_SYMBOL:MAPK3"
      map_id "UNIPROT:P27361"
      name "MAPK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P27361"
    ]
    graphics [
      x 1037.7972075309485
      y 376.9635032322131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1123.9338821258286
      y 382.90662359327956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "UNIPROT:Q7TFA1"
      name "Nsp7; Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76; sa70; sa64"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 868.0265989012713
      y 366.6048851056605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7TFA1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD9;HGNC_SYMBOL:SMAD5"
      map_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      x 898.5531757665096
      y 129.93150509629982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_13"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 757.1027800620789
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:obo.go:GO%3A0000074"
      hgnc "NA"
      map_id "Modulation_space_of_space_cell_space_cycle"
      name "Modulation_space_of_space_cell_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 627.9965618662808
      y 92.5219454851848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Modulation_space_of_space_cell_space_cycle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_14"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 672.541020139813
      y 217.68595520784697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "UNIPROT:Q15796;UNIPROT:P84022"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5; csa4"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 784.1624824947882
      y 326.89919811948937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15796;UNIPROT:P84022"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 948.2241259611649
      y 452.1805256251992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387; urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:P62877"
      name "RBX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa11; sa12; sa20; sa4"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 833.7498329596083
      y 471.71203633549743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "UNIPROT:P01137"
      name "TGFB1; TGFB_slash_TGFBR"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa10; csa11"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 817.4706227013205
      y 601.610690185008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:90;urn:miriam:ncbigene:90;urn:miriam:hgnc:171;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q04771;urn:miriam:uniprot:Q04771;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:refseq:NM_001105;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:ensembl:ENSG00000115170"
      hgnc "HGNC_SYMBOL:ACVR1"
      map_id "UNIPROT:Q04771"
      name "ACVR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:Q04771"
    ]
    graphics [
      x 937.5400747085504
      y 557.0705449296482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q04771"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:uniprot:P36896;urn:miriam:uniprot:P36896;urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:ec-code:2.7.11.30;urn:miriam:ncbigene:91;urn:miriam:ncbigene:91;urn:miriam:hgnc:172;urn:miriam:ensembl:ENSG00000135503;urn:miriam:refseq:NM_020328"
      hgnc "HGNC_SYMBOL:ACVR1B"
      map_id "UNIPROT:P36896"
      name "ACVR1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:P36896"
    ]
    graphics [
      x 934.9493971892331
      y 337.4858915041094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P36896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1001.449325440584
      y 659.5262200926908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 736.1912870499236
      y 476.2213466337597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 698.1525216038718
      y 742.2253281487838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "UNIPROT:P30153;UNIPROT:P67775"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16; csa15"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 630.4502251398343
      y 876.6201392080985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30153;UNIPROT:P67775"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:reactome:R-COV-9686193; urn:miriam:uniprot:P59635; urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "UNIPROT:P59635"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_614; c0237; sa84; sa76"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 577.7876554308965
      y 748.351590025489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 553.9770892924021
      y 983.5021845703745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "UNIPROT:P23443"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa41; sa40"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 457.7444531845306
      y 1049.752695716527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23443"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:pubmed:17016423;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "UNIPROT:P61586"
      name "RHOA; RGcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1; sa38; sa1305; csa101"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 613.6767458010946
      y 472.2249578099417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61586"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.30;urn:miriam:refseq:NM_001024847;urn:miriam:hgnc:11773;urn:miriam:ensembl:ENSG00000163513;urn:miriam:ncbigene:7048;urn:miriam:ncbigene:7048;urn:miriam:uniprot:P37173;urn:miriam:uniprot:P37173;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:ensembl:ENSG00000106799;urn:miriam:uniprot:P36897;urn:miriam:uniprot:P36897;urn:miriam:ncbigene:7046;urn:miriam:ncbigene:7046;urn:miriam:ec-code:2.7.11.30;urn:miriam:hgnc:11772;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:refseq:NM_001130916"
      hgnc "HGNC_SYMBOL:TGFBR2;HGNC_SYMBOL:TGFBR1"
      map_id "UNIPROT:P37173;UNIPROT:P36897"
      name "TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P37173;UNIPROT:P36897"
    ]
    graphics [
      x 895.29517150677
      y 663.5204826444278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P37173;UNIPROT:P36897"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_206943;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:uniprot:Q14766;urn:miriam:uniprot:Q14766;urn:miriam:hgnc:6714;urn:miriam:ensembl:ENSG00000049323"
      hgnc "HGNC_SYMBOL:LTBP1"
      map_id "UNIPROT:Q14766"
      name "LTBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q14766"
    ]
    graphics [
      x 941.613093835676
      y 776.5249169906884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14766"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489677;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa72"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    ]
    graphics [
      x 882.7341964131667
      y 734.29349959194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1217.5501289743418
      y 470.4860414567165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_35"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1191.4242730188657
      y 584.1532247334259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 32
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59637"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61160;UNIPROT:Q13873"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 6
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P27361"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 7
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 8
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 2
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_16"
      target_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 4
    target 19
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 4
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13145"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 4
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q13145"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P27361"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 7
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 7
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59632"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 8
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 8
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q7TFA1"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
      target_id "M19_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_13"
      target_id "Modulation_space_of_space_cell_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_14"
      target_id "Modulation_space_of_space_cell_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15796;UNIPROT:P84022"
      target_id "M19_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15796;UNIPROT:P84022"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:P62877"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 17
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q04771"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 18
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P36896"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01137"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P01137"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 27
    target 19
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P37173;UNIPROT:P36897"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 28
    target 19
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "UNIPROT:Q14766"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 29
    target 19
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 26
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61586"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30153;UNIPROT:P67775"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 23
    target 21
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P59635"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30153;UNIPROT:P67775"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P23443"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_15"
      target_id "M19_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
