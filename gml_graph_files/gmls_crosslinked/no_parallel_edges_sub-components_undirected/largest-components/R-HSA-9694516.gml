# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "RTC_space_inhibitors"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1483; layout_1486; layout_2503; layout_2506"
      uniprot "NA"
    ]
    graphics [
      x 1368.5278250748634
      y 1542.6723226049812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RTC_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694541;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:33264556;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_359"
      name "SARS-CoV-2 gRNA:RTC:RNA primer binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694541__layout_2502"
      uniprot "NA"
    ]
    graphics [
      x 1382.8121916296643
      y 1396.6219604278408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694419;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_334"
      name "SARS-CoV-2 gRNA complement (minus strand):RTC binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694419__layout_2505"
      uniprot "NA"
    ]
    graphics [
      x 1459.8491741396467
      y 1418.9799996706765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694665;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_385"
      name "SARS-CoV-2 gRNA:RTC:nascent RNA minus strand binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694665__layout_2504"
      uniprot "NA"
    ]
    graphics [
      x 1428.653027449111
      y 1365.8777196101698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 47
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713644;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713311; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9713643;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713313;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9681663;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694725; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694255;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9680476; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713306;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713634;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:reactome:R-COV-9686016;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694302;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694458;urn:miriam:reactome:R-COV-9681659; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713317;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713652; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694479; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713302;urn:miriam:reactome:R-COV-9713651; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694366;urn:miriam:reactome:R-COV-9686003; urn:miriam:reactome:R-COV-9694770;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9686011; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694728; urn:miriam:reactome:R-COV-9682253;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694404;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684343;urn:miriam:reactome:R-COV-9694407; urn:miriam:reactome:R-COV-9694375;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32541865;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:pubmed:32272481; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694740;urn:miriam:reactome:R-COV-9683403; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32511376;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:pubmed:32709886;urn:miriam:reactome:R-COV-9694416;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:pubmed:32838362;urn:miriam:pubmed:34131072; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682469;urn:miriam:reactome:R-COV-9694618; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694327;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9687385; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694784;urn:miriam:reactome:R-COV-9684866; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210; urn:miriam:reactome:R-COV-9694760;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682222; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684877;urn:miriam:reactome:R-COV-9694428; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694735;urn:miriam:reactome:R-COV-9682205; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:33208736;urn:miriam:reactome:R-COV-9694717; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694577;urn:miriam:reactome:R-COV-9684876; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694358;urn:miriam:reactome:R-COV-9682215; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694688;urn:miriam:reactome:R-COV-9682545; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694731;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9682668;urn:miriam:obo.chebi:CHEBI%3A18420; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691351; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691348;urn:miriam:pubmed:32277040; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691364;urn:miriam:pubmed:32277040; urn:miriam:reactome:R-COV-9694361;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682227; urn:miriam:reactome:R-COV-9682451;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694597; urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694748; urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682229; urn:miriam:reactome:R-COV-9694286;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694269;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682566; urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691334"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      name "SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC; SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer; SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors; m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; RTC; SARS_minus_CoV_minus_2_space_gRNA:RTC; SARS_minus_CoV_minus_2_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC; SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC; nsp3_minus_4; N_minus_glycan_space_nsp3_minus_4; SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; m7GpppA_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC; nsp3:nsp4; nsp6; nsp3:nsp4:nsp6; SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC; SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors; 3CLp_space_dimer; 3CLp_space_dimer:3CLp_space_inhibitors; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15; nsp16:nsp10; SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand; SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors; nsp1_minus_4; nsp1; nsp2; nsp3; N_minus_glycan_space_nsp3; nsp7:nsp8:nsp12:nsp14:nsp10:nsp13; nsp4; nsp10; nsp10:nsp14; SARS_space_coronavirus_space_2_space_gRNA_space_with_space_secondary_space_structure:RTC; N_minus_glycan_space_nsp4; nsp8; nsp7:nsp8; nsp7:nsp8:nsp12; nsp7:nsp8:nsp12:nsp14:nsp10; nsp5; nsp9; nsp9_space_tetramer; SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide; nsp7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3381; layout_3386; layout_2262; layout_2268; layout_3366; layout_2251; layout_2256; layout_3408; layout_3361; layout_2206; layout_2212; layout_3410; layout_3417; layout_2448; layout_2449; layout_2450; layout_2291; layout_2296; layout_2176; layout_2179; layout_2247; layout_2249; layout_2266; layout_2279; layout_2204; layout_2205; layout_2207; layout_2216; layout_2220; layout_2243; layout_2217; layout_2238; layout_2239; layout_2253; layout_2225; layout_2559; layout_2565; layout_2576; layout_2508; layout_2222; layout_2241; layout_2175; layout_2229; layout_2230; layout_3098; layout_2270; layout_2562"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1554.2092459783255
      y 1114.2072604968018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694737;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_395"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694737__layout_2297"
      uniprot "NA"
    ]
    graphics [
      x 1739.5454858939218
      y 988.0530092884749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694521;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_353"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694521__layout_2288"
      uniprot "NA"
    ]
    graphics [
      x 1646.2155152705577
      y 897.5278750906856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694344;PUBMED:12917450;PUBMED:16928755;PUBMED:12927536;PUBMED:14569023;PUBMED:25736566;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_321"
      name "Synthesis of SARS-CoV-2 minus strand subgenomic mRNAs by template switching"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694344__layout_2422"
      uniprot "NA"
    ]
    graphics [
      x 1286.5938668473161
      y 1294.0238335659515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694492;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_348"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694492__layout_2280"
      uniprot "NA"
    ]
    graphics [
      x 1828.7982967450876
      y 991.0986157700057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694331;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_315"
      name "nsp3-4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694331__layout_2209"
      uniprot "NA"
    ]
    graphics [
      x 1218.5071329396176
      y 1056.1045081416964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694476;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_346"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694476__layout_2429"
      uniprot "NA"
    ]
    graphics [
      x 1781.9361366310482
      y 1026.981764716999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694630;PUBMED:18367524"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_378"
      name "Nsp3:nsp4 binds to nsp6"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694630__layout_2447"
      uniprot "NA"
    ]
    graphics [
      x 1447.7367016534176
      y 1243.8202762498163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694592;PUBMED:32045235;PUBMED:33452205;PUBMED:32541865;PUBMED:33984267;PUBMED:33062953;PUBMED:32272481;PUBMED:32374457;PUBMED:32896566;PUBMED:33152262;PUBMED:32737471;PUBMED:34726479;PUBMED:15507456;PUBMED:33574416"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_373"
      name "3CLp dimer binds 3CLp inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694592__layout_2452"
      uniprot "NA"
    ]
    graphics [
      x 1350.9187245323078
      y 1328.4921811533127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694576;PUBMED:18827877;PUBMED:18255185"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_369"
      name "nsp3 binds to nsp7-8 and nsp12-16"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694576__layout_2451"
      uniprot "NA"
    ]
    graphics [
      x 1637.9051462894752
      y 1166.2496586944212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694732;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_393"
      name "3CLp cleaves pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694732__layout_2188"
      uniprot "NA"
    ]
    graphics [
      x 1460.9337521573393
      y 999.6827287448548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694338;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_319"
      name "nsp1-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694338__layout_2202"
      uniprot "NA"
    ]
    graphics [
      x 1793.7705496293106
      y 1185.1089909590705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694389;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_329"
      name "nsp3 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694389__layout_2219"
      uniprot "NA"
    ]
    graphics [
      x 1252.2522654707914
      y 1012.9929062141249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694506;PUBMED:12917450;PUBMED:12927536;PUBMED:32330414;PUBMED:14569023;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_351"
      name "Synthesis of SARS-CoV-2 plus strand subgenomic mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694506__layout_2427"
      uniprot "NA"
    ]
    graphics [
      x 1539.4608258378546
      y 1318.488481887659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694539;PUBMED:28143984;PUBMED:16828802;PUBMED:16882730;PUBMED:18045871;PUBMED:16216269;PUBMED:17409150;PUBMED:18255185;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_358"
      name "nsp15 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694539__layout_2246"
      uniprot "NA"
    ]
    graphics [
      x 1429.2241217749902
      y 943.2410689298026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694601;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_374"
      name "nsp3-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694601__layout_2214"
      uniprot "NA"
    ]
    graphics [
      x 1502.3787005432205
      y 967.9457455843865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694542;PUBMED:25197083;PUBMED:32838362;PUBMED:22635272;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_360"
      name "nsp14 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694542__layout_2237"
      uniprot "NA"
    ]
    graphics [
      x 1450.381756474433
      y 894.5266374404405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694277;PUBMED:17024178;PUBMED:22039154"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_307"
      name "nsp8 generates RNA primers"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694277__layout_2260"
      uniprot "NA"
    ]
    graphics [
      x 1566.7107287674926
      y 1440.8154340882647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694454;PUBMED:32358203;PUBMED:32526208;PUBMED:32438371;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_342"
      name "Replication transcription complex binds SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694454__layout_2250"
      uniprot "NA"
    ]
    graphics [
      x 1348.1827551690226
      y 1251.996141425021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694611;PUBMED:17855519"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_376"
      name "nsp4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694611__layout_2223"
      uniprot "NA"
    ]
    graphics [
      x 1250.6475119708082
      y 947.4960217386481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691363;PUBMED:32358203;PUBMED:32438371;PUBMED:31138817;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_303"
      name "nsp12 binds nsp7 and nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691363__layout_2574"
      uniprot "NA"
    ]
    graphics [
      x 1388.3250084792096
      y 983.3016747912218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694605;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_375"
      name "nsp12 synthesizes minus strand SARS-CoV-2 genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694605__layout_2264"
      uniprot "NA"
    ]
    graphics [
      x 1624.2517193430226
      y 1403.196373758838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694317;PUBMED:20542253;PUBMED:24991833;PUBMED:28738245;PUBMED:23943763"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_314"
      name "Nsp3, nsp4, and nsp6 produce replicative organelles"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694317__layout_2507"
      uniprot "NA"
    ]
    graphics [
      x 1582.7535476836456
      y 1231.446227112093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694625;PUBMED:12917450;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_377"
      name "nsp3 cleaves nsp1-4"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694625__layout_2221"
      uniprot "NA"
    ]
    graphics [
      x 1497.5241522486806
      y 1151.6831884986952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694721;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_390"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694721__layout_2304"
      uniprot "NA"
    ]
    graphics [
      x 1785.9130146594507
      y 939.0583252004224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694445;PUBMED:20699222;PUBMED:22022266;PUBMED:32511376;PUBMED:32709886;PUBMED:32838362;PUBMED:16873246;PUBMED:20421945;PUBMED:16873247;PUBMED:34131072;PUBMED:25074927;PUBMED:26041293;PUBMED:21637813;PUBMED:21393853;PUBMED:22635272"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_339"
      name "nsp16 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694445__layout_2248"
      uniprot "NA"
    ]
    graphics [
      x 1350.6169494916635
      y 1039.5228016241067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694581;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_372"
      name "RTC synthesizes SARS-CoV-2 plus strand genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694581__layout_2292"
      uniprot "NA"
    ]
    graphics [
      x 1601.8299488700584
      y 1340.639615290963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694304;PUBMED:25197083;PUBMED:16549795"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_312"
      name "nsp14 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694304__layout_2240"
      uniprot "NA"
    ]
    graphics [
      x 1446.473475175196
      y 1074.173718987514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694333;PUBMED:32198291"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_316"
      name "3CLp forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694333__layout_2174"
      uniprot "NA"
    ]
    graphics [
      x 1437.1081444206343
      y 1189.502907179156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694495;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_349"
      name "RTC binds SARS-CoV-2 genomic RNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694495__layout_2290"
      uniprot "NA"
    ]
    graphics [
      x 1662.675423435171
      y 1208.5832872276771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694524;PUBMED:32783916;PUBMED:31131400;PUBMED:33208736;PUBMED:33232691;PUBMED:22615777;PUBMED:17520018"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_354"
      name "nsp13 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694524__layout_2242"
      uniprot "NA"
    ]
    graphics [
      x 1380.8283612375783
      y 918.2079208392211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694499;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_350"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694499__layout_2435"
      uniprot "NA"
    ]
    graphics [
      x 1645.7158701035996
      y 798.8910286432673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694265;PUBMED:32783916;PUBMED:32484220;PUBMED:19224332;PUBMED:16579970;PUBMED:12917423;PUBMED:15140959;PUBMED:32500504;PUBMED:20671029;PUBMED:22615777"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_305"
      name "nsp13 helicase melts secondary structures in SARS-CoV-2 genomic RNA template"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694265__layout_3127"
      uniprot "NA"
    ]
    graphics [
      x 1339.3944198703143
      y 1137.0401027230964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694261;PUBMED:19153232;PUBMED:32592996"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_304"
      name "nsp9 forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694261__layout_2228"
      uniprot "NA"
    ]
    graphics [
      x 1421.76553968742
      y 1126.5227790537954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694580;PUBMED:15331731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_371"
      name "nsp8 binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694580__layout_2509"
      uniprot "NA"
    ]
    graphics [
      x 1848.9075029363582
      y 906.0136273595035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694551;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_362"
      name "3CLp cleaves nsp6-11"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694551__layout_2180"
      uniprot "NA"
    ]
    graphics [
      x 1814.2345018306683
      y 1231.5234588359147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694632;PUBMED:25197083;PUBMED:17927896;PUBMED:32938769;PUBMED:20463816;PUBMED:22635272;PUBMED:16549795;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_379"
      name "nsp14 acts as a 3'-to-5' exonuclease to remove misincorporated nucleotides from nascent RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694632__layout_2272"
      uniprot "NA"
    ]
    graphics [
      x 1660.646120120126
      y 974.3970196089806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694792;PUBMED:20463816"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_398"
      name "nsp12 misincorporates a nucleotide in nascent RNA minus strand"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694792__layout_2269"
      uniprot "NA"
    ]
    graphics [
      x 1512.561424063379
      y 1358.4843925978091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_398"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694441;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_337"
      name "3CLp cleaves pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694441__layout_2177"
      uniprot "NA"
    ]
    graphics [
      x 1741.413065806019
      y 1132.5938591040367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694549;PUBMED:25197083"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_361"
      name "RTC completes synthesis of the minus strand genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694549__layout_2275"
      uniprot "NA"
    ]
    graphics [
      x 1459.1237706767333
      y 1314.8028185777246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691335;PUBMED:32535228;PUBMED:32438371;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_302"
      name "nsp7 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691335__layout_2558"
      uniprot "NA"
    ]
    graphics [
      x 1509.3985309560148
      y 1241.60613740918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "NTP(4_minus_)"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_142; layout_149; layout_414; layout_174; layout_137; layout_3761; layout_2423; layout_2265; layout_2293; layout_2276"
      uniprot "NA"
    ]
    graphics [
      x 1447.8369246333941
      y 1496.7658151955493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NTP(4_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685518;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2277; layout_2163; layout_3664"
      uniprot "NA"
    ]
    graphics [
      x 1365.1898428883765
      y 1196.8411539152153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 1631.043251411327
      y 1260.8230259609654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694471;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_345"
      name "Polyadenylation of SARS-CoV-2 genomic RNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694471__layout_2306"
      uniprot "NA"
    ]
    graphics [
      x 1527.1844502382705
      y 1186.6219260572293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694733;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:32511382;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_394"
      name "Polyadenylation of SARS-CoV-2 subgenomic mRNAs (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694733__layout_2438"
      uniprot "NA"
    ]
    graphics [
      x 1508.2573112851971
      y 1060.5948552728387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_394"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 1183.2993258892498
      y 1133.439700371887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685891;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694259"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      name "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2437"
      uniprot "NA"
    ]
    graphics [
      x 1572.4603385010462
      y 861.7907539098375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694561;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685910"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2440"
      uniprot "NA"
    ]
    graphics [
      x 1586.0195448027807
      y 957.5834829492502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729330;PUBMED:12134018;PUBMED:32817937;PUBMED:32637943;PUBMED:33248025"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_414"
      name "SRPK1/2 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729330__layout_3564"
      uniprot "NA"
    ]
    graphics [
      x 1028.8782640364223
      y 1089.3150922519535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729300;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_411"
      name "Unknown kinase phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729300__layout_3541"
      uniprot "NA"
    ]
    graphics [
      x 1016.7489049785196
      y 1028.29969718948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_411"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729260;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_408"
      name "GSK3 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729260__layout_3598"
      uniprot "NA"
    ]
    graphics [
      x 950.4631865857281
      y 996.2578564596242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_408"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729318;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_413"
      name "CSNK1A1 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729318__layout_3550"
      uniprot "NA"
    ]
    graphics [
      x 981.6052127482483
      y 1131.3126406728093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_413"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694300;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683625; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729324; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9684230;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729277;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9729264;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9684199;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694612; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729335;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694659; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729308;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC9; urn:miriam:uniprot:P0DTC9; urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9; urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P0DTC9"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; N_space_tetramer; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand); N; p_minus_S188,206_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S,2T_minus_N; p_minus_11S,2T_minus_N; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S_minus_N; ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; p_minus_11S,2T_minus_metR95,177_minus_N; nucleocapsid_br_protein; Nucleocapsid; Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_3639; layout_2352; layout_2496; layout_3571; layout_2364; layout_2341; layout_3558; layout_2343; layout_2465; layout_3542; layout_3528; layout_2353; layout_2467; layout_3574; layout_2361; layout_2362; layout_3616; layout_3529; layout_2960; ff3a1; b0f05; b6d08; b9df2; sa1685; sa1887; csa365; csa369; csa374; csa398; sa1667; csa353; csa357; csa391; csa389; csa387; csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 886.4181847714208
      y 1114.5044909368548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-BTA-195263-4;urn:miriam:reactome:R-BTA-195263-5;urn:miriam:reactome:R-BTA-195263-2;urn:miriam:reactome:R-BTA-195263-3;urn:miriam:reactome:R-CEL-195263;urn:miriam:reactome:R-MMU-195263;urn:miriam:reactome:R-DME-195263;urn:miriam:reactome:R-XTR-195263;urn:miriam:reactome:R-DME-195263-2;urn:miriam:reactome:R-DME-195263-5;urn:miriam:reactome:R-CEL-195263-5;urn:miriam:reactome:R-DME-195263-3;urn:miriam:reactome:R-DME-195263-4;urn:miriam:reactome:R-HSA-195263;urn:miriam:reactome:R-CEL-195263-2;urn:miriam:reactome:R-CEL-195263-4;urn:miriam:reactome:R-CEL-195263-3;urn:miriam:reactome:R-BTA-195263;urn:miriam:reactome:R-CFA-195263;urn:miriam:reactome:R-SSC-195263;urn:miriam:uniprot:P48729;urn:miriam:reactome:R-RNO-195263;urn:miriam:reactome:R-GGA-195263;urn:miriam:reactome:R-DRE-195263"
      hgnc "NA"
      map_id "UNIPROT:P48729"
      name "CSNK1A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3789"
      uniprot "UNIPROT:P48729"
    ]
    graphics [
      x 959.9490973169973
      y 1284.8904276326289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48729"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 1070.853286022413
      y 1160.8841889601758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 1088.0901549137866
      y 884.6013740560577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694790;PUBMED:16684538;PUBMED:20129637"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_397"
      name "E protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694790__layout_2404"
      uniprot "NA"
    ]
    graphics [
      x 1346.376140943973
      y 645.24082025503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694438;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_336"
      name "GalNAc is transferred onto 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694438__layout_2393"
      uniprot "NA"
    ]
    graphics [
      x 673.2651777975191
      y 564.5871039811332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729283"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_410"
      name "Nucleoprotein is methylated by PRMT1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729283__layout_3539"
      uniprot "NA"
    ]
    graphics [
      x 1384.8771903799116
      y 836.561007608006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_410"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694525;PUBMED:16442106"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_355"
      name "M protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694525__layout_2417"
      uniprot "NA"
    ]
    graphics [
      x 1480.3961251525539
      y 855.8663038257522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729279;PUBMED:29199039;PUBMED:32029454"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_409"
      name "Nucleoprotein is ADP-ribosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729279__layout_3615"
      uniprot "NA"
    ]
    graphics [
      x 932.1324744927316
      y 1215.6538068416507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_409"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683467;PUBMED:9719345;PUBMED:32145363;PUBMED:16115318;PUBMED:28596841;PUBMED:25693996;PUBMED:32226290"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_297"
      name "CQ, HCQ are protonated to CQ2+, HCQ2+"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683467__layout_2933"
      uniprot "NA"
    ]
    graphics [
      x 1272.5423642018727
      y 659.0044260293773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685614; urn:miriam:reactome:R-ALL-9685610"
      hgnc "NA"
      map_id "CQ,_space_HCQ"
      name "CQ,_space_HCQ"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2035; layout_2029; layout_2931; layout_2932"
      uniprot "NA"
    ]
    graphics [
      x 1472.7710493763898
      y 524.8495072180544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ,_space_HCQ"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:kegg.compound:24848403;urn:miriam:reactome:R-ALL-9685618;urn:miriam:kegg.compound:78435478"
      hgnc "NA"
      map_id "CQ2_plus_,_space_HCQ2_plus_"
      name "CQ2_plus_,_space_HCQ2_plus_"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2030; layout_2935"
      uniprot "NA"
    ]
    graphics [
      x 1340.5407797325388
      y 553.1995489509062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CQ2_plus_,_space_HCQ2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683478;PUBMED:21124966"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_298"
      name "CQ, HCQ diffuses from cytosol to endocytic vesicle lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683478__layout_2930"
      uniprot "NA"
    ]
    graphics [
      x 1644.4902462605746
      y 483.80235513744043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360; urn:miriam:obo.chebi:CHEBI%3A15846; urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "NAD_plus_"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439; layout_2443; sa3; sa222; sa256; sa108; sa45"
      uniprot "NA"
    ]
    graphics [
      x 957.566667125673
      y 1375.3877466039444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CFA-8938273;urn:miriam:uniprot:Q53GL7;urn:miriam:reactome:R-HSA-8938273;urn:miriam:reactome:R-XTR-8938273;urn:miriam:uniprot:Q2NL67;urn:miriam:reactome:R-DDI-8938273;urn:miriam:uniprot:Q8N3A8;urn:miriam:reactome:R-GGA-8938273;urn:miriam:reactome:R-RNO-8938273;urn:miriam:reactome:R-MMU-8938273;urn:miriam:reactome:R-BTA-8938273;urn:miriam:reactome:R-DRE-8938273;urn:miriam:reactome:R-DME-8938259;urn:miriam:uniprot:Q8N5Y8;urn:miriam:uniprot:Q460N5;urn:miriam:uniprot:Q8IXQ6;urn:miriam:uniprot:Q9UKK3;urn:miriam:reactome:R-SSC-8938273"
      hgnc "NA"
      map_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      name "PARPs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_914; layout_2446"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    ]
    graphics [
      x 889.0257381538926
      y 1373.9481004195404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277; urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "NAM"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_716; layout_2444; sa221; sa253"
      uniprot "NA"
    ]
    graphics [
      x 835.8223633884029
      y 1335.0206625073633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP5038; WP5039; C19DMap:Virus replication cycle; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5; urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5; urn:miriam:uniprot:P0DTC5; urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "UNIPROT:P0DTC5"
      name "nascent_space_M; M; N_minus_glycan_space_M; M_space_lattice; membrane_br_glycoprotein; Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2338; layout_2416; layout_2419; layout_2463; layout_2469; d614c; d1b58; cedc7; sa2061; sa2116; sa2024; sa2066; sa1686; sa1891; sa1857; sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1665.092971975504
      y 1301.6007192846994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "nucleotide_minus_sugar"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_272; layout_389; layout_2366; layout_2418"
      uniprot "NA"
    ]
    graphics [
      x 1507.7568647422713
      y 691.5702019683681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleotide_minus_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_274; layout_388; layout_2368; layout_2420"
      uniprot "NA"
    ]
    graphics [
      x 1443.655254365131
      y 715.9584356851427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694681;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_387"
      name "mRNA5 is translated to protein M"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694681__layout_2336"
      uniprot "NA"
    ]
    graphics [
      x 1815.3951259062778
      y 1498.0150382444124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694555;PUBMED:32198291;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_364"
      name "Protein M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694555__layout_2415"
      uniprot "NA"
    ]
    graphics [
      x 1745.1239918092087
      y 1434.7326347897842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694367;PUBMED:33203855;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_325"
      name "Glycosylated M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694367__layout_2462"
      uniprot "NA"
    ]
    graphics [
      x 1690.9126121655027
      y 1469.7194639849008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694444;PUBMED:16684538;PUBMED:18703211;PUBMED:19322648;PUBMED:16254320;PUBMED:15474033;PUBMED:15147946;PUBMED:18792806;PUBMED:31226023;PUBMED:16877062;PUBMED:16507314;PUBMED:17530462;PUBMED:15713601;PUBMED:15351485;PUBMED:31133031;PUBMED:25855243;PUBMED:18753196;PUBMED:16343974;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_338"
      name "E and N are recruited to the M lattice"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694444__layout_2470"
      uniprot "NA"
    ]
    graphics [
      x 1270.4399485823242
      y 868.8699857800561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694487;PUBMED:16442106;PUBMED:18703211;PUBMED:9658133;PUBMED:16254320;PUBMED:20154085;PUBMED:7721788;PUBMED:15474033;PUBMED:19534833;PUBMED:15147946;PUBMED:16877062;PUBMED:10799570;PUBMED:18753196;PUBMED:23700447;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_347"
      name "M protein oligomerization"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694487__layout_2468"
      uniprot "NA"
    ]
    graphics [
      x 1800.5467691038984
      y 1385.1176109564021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP4861; WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC4; urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P0DTC4"
      name "nascent_space_E; N_minus_glycan_space_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; 3xPalmC_minus_E; envelope_br_protein; SARS_space_E; Envelope_space_Protein_space_E; E"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_2335; layout_2405; layout_2410; layout_2412; layout_2407; layout_2414; aa466; fce54; c7c21; sa2062; sa2115; sa2023; sa2065; sa1687; sa1892; sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1398.743689003944
      y 376.78735365595713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684226;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694491;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "M_space_lattice:E_space_protein:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2472"
      uniprot "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1120.8719729930424
      y 741.1476834687176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694553;PUBMED:24418573;PUBMED:22238235;PUBMED:17166901;PUBMED:27145752;PUBMED:25855243;PUBMED:20580052;PUBMED:20007283;PUBMED:18792806;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_363"
      name "Recruitment of Spike trimer to assembling virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694553__layout_2473"
      uniprot "NA"
    ]
    graphics [
      x 976.2546634844908
      y 629.3085009971551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1268.1065679036337
      y 546.3541020402682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9686310;urn:miriam:reactome:R-COV-9694321;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2475"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 668.7843699994985
      y 635.4271334555962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694528;PUBMED:16840309;PUBMED:15807784;PUBMED:16894145;PUBMED:15781262;PUBMED:23202509;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_356"
      name "Accessory proteins are recruited to the maturing virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694528__layout_2476"
      uniprot "NA"
    ]
    graphics [
      x 395.91155997074395
      y 650.1610793613668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 17
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3"
      name "O_minus_glycosyl_space_3a_space_tetramer; O_minus_glycosyl_space_3a; 3a; 3a:membranous_space_structure; GalNAc_minus_O_minus_3a; ORF3a; Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2458; layout_2461; layout_2399; layout_2403; layout_2332; layout_2392; layout_2459; layout_2455; layout_2395; cb0cc; ac4ba; ed8aa; sa1873; sa2247; sa1; sa169; sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 441.5460299192242
      y 397.99215385015884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9694750;urn:miriam:uniprot:P0DTC7; urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "UNIPROT:P0DTC7"
      name "7a; Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2477; sa1875; sa2060; sa2114; sa2025; sa2067; sa2245; sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 505.11422181234116
      y 699.1407481352886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694585;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9684225;urn:miriam:reactome:R-COV-9694324;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9752958;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685539; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694500;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer; S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2490; layout_2478; layout_3798; layout_2482; layout_2485"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 246.04185141476842
      y 916.1621238920966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9699007;PUBMED:32532959;PUBMED:32362314;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_407"
      name "FURIN Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9699007__layout_3308"
      uniprot "NA"
    ]
    graphics [
      x 96.48937777495428
      y 1016.4196275833782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_407"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694661;PUBMED:32532959;PUBMED:20926566;PUBMED:21325420;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_383"
      name "TMPRSS2 Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694661__layout_2498"
      uniprot "NA"
    ]
    graphics [
      x 283.38953631833624
      y 1062.4676723489492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694287;PUBMED:22816037;PUBMED:19321428"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_310"
      name "Cleavage of S protein into S1:S2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694287__layout_2489"
      uniprot "NA"
    ]
    graphics [
      x 261.6963592977439
      y 1150.3173408802259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694689;PUBMED:26311884"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_388"
      name "Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694689__layout_2494"
      uniprot "NA"
    ]
    graphics [
      x 528.7686746971424
      y 993.5308799664367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694633;PUBMED:16877062;PUBMED:9658133;PUBMED:10799570;PUBMED:31133031;PUBMED:16254320;PUBMED:25855243;PUBMED:18792806"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_380"
      name "SARS virus buds into ERGIC lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694633__layout_3799"
      uniprot "NA"
    ]
    graphics [
      x 164.62506469231664
      y 970.5733303805118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694641;PUBMED:31226023;PUBMED:16877062;PUBMED:25855243"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_381"
      name "Viral release"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694641__layout_2481"
      uniprot "NA"
    ]
    graphics [
      x 135.1746859824152
      y 843.4868431702724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694579;PUBMED:14647384;PUBMED:16166518;PUBMED:32125455;PUBMED:22816037"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_370"
      name "Spike glycoprotein of SARS-CoV-2 binds ACE2 on host cell"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694579__layout_2483"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1074.5264220393035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 164.30092503388767
      y 1118.5700857170077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694785;urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-HSA-9694758;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-HSA-9686692"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2; S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2971; layout_2972"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 145.79712855713183
      y 1189.2843798481417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697423;PUBMED:15163706;PUBMED:15010527;PUBMED:22816037;PUBMED:15140961"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_404"
      name "Endocytosis of SARS-CoV-2 Virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697423__layout_2970"
      uniprot "NA"
    ]
    graphics [
      x 144.6452122554665
      y 1308.306993628348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_404"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698988;PUBMED:21068237;PUBMED:21325420;PUBMED:20926566;PUBMED:15474033;PUBMED:32532959;PUBMED:32362314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_406"
      name "Direct Host Cell Membrane Membrane Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698988__layout_3336"
      uniprot "NA"
    ]
    graphics [
      x 377.5134398912413
      y 1126.7342610406001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-CFA-2159856;urn:miriam:reactome:R-DRE-2159856;urn:miriam:reactome:R-HSA-2159856;urn:miriam:reactome:R-XTR-2159856;urn:miriam:reactome:R-GGA-2159856;urn:miriam:pubmed:1629222;urn:miriam:uniprot:P09958;urn:miriam:reactome:R-MMU-2159856;urn:miriam:reactome:R-SSC-2159856;urn:miriam:reactome:R-RNO-2159856; urn:miriam:uniprot:P09958;urn:miriam:pubmed:32376634; urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "NA; HGNC_SYMBOL:FURIN"
      map_id "UNIPROT:P09958"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3309; ee829; sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 237.20624812614335
      y 1022.6299211910915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09958"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 475.1725333730436
      y 931.9801641406003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686688;urn:miriam:reactome:R-COV-9698997;urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M_space_lattice:E_space_protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3345"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 437.5146248845879
      y 1045.3756013443349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681514;PUBMED:14647384;PUBMED:32142651;PUBMED:21068237;PUBMED:27550352;PUBMED:27277342;PUBMED:24227843;PUBMED:28414992;PUBMED:22496216"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_296"
      name "TMPRSS2 binds TMPRSS2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681514__layout_2924"
      uniprot "NA"
    ]
    graphics [
      x 707.3884339797814
      y 713.3399913641391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9682035"
      hgnc "NA"
      map_id "TMPRSS2_space_inhibitors"
      name "TMPRSS2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1044; layout_2925"
      uniprot "NA"
    ]
    graphics [
      x 848.7716088704285
      y 558.220446087281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMPRSS2_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-1181251;urn:miriam:reactome:R-HSA-1181251;urn:miriam:reactome:R-XTR-1181251;urn:miriam:reactome:R-CFA-1181251;urn:miriam:reactome:R-GGA-1181251;urn:miriam:reactome:R-MMU-1181251;urn:miriam:reactome:R-RNO-1181251;urn:miriam:reactome:R-DRE-1181251;urn:miriam:reactome:R-DDI-1181251;urn:miriam:reactome:R-PFA-1181251;urn:miriam:uniprot:P55072;urn:miriam:reactome:R-SPO-1181251;urn:miriam:reactome:R-SSC-1181251;urn:miriam:reactome:R-SCE-1181251;urn:miriam:reactome:R-DME-1181251"
      hgnc "NA"
      map_id "UNIPROT:P55072"
      name "VCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1642; layout_2497"
      uniprot "UNIPROT:P55072"
    ]
    graphics [
      x 432.81143024012397
      y 884.1455721073268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55072"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694534;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9686705;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      name "S1:S2:M:E:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2495"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 396.89149896743527
      y 972.1178555206176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 5
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717; urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767; urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "NA; HGNC_SYMBOL:CTSL"
      map_id "UNIPROT:P07711"
      name "Cathepsin_space_L1; CTSL:CTSL_space_inhibitors; CTSL"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_837; layout_1038; layout_2492; layout_2493; sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 446.99245103032547
      y 1153.5010533745447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07711"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685655;PUBMED:32179150;PUBMED:23105391;PUBMED:16962401;PUBMED:26335104;PUBMED:26953343"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_299"
      name "CTSL bind CTSL inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685655__layout_2922"
      uniprot "NA"
    ]
    graphics [
      x 612.3058291630364
      y 1051.4073690877713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9693170;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "CTSL_space_inhibitors"
      name "CTSL_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2051; layout_2923"
      uniprot "NA"
    ]
    graphics [
      x 760.3738907430882
      y 997.6763814225801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CTSL_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694794;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_400"
      name "mRNA3 is translated to protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694794__layout_2330"
      uniprot "NA"
    ]
    graphics [
      x 273.97838966663016
      y 310.17075302579497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694727;PUBMED:19398035;PUBMED:15194747;PUBMED:16212942"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_392"
      name "Endocytosis of protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694727__layout_2460"
      uniprot "NA"
    ]
    graphics [
      x 500.16688341710403
      y 300.28561532538606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_392"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694662;PUBMED:32587976"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_384"
      name "Protein 3a forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694662__layout_2402"
      uniprot "NA"
    ]
    graphics [
      x 310.27049482689836
      y 390.7631157207918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694392;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_331"
      name "3a translocates to the ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694392__layout_2391"
      uniprot "NA"
    ]
    graphics [
      x 334.8493686557914
      y 464.6036294164576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694308;PUBMED:16352545"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_313"
      name "3a is externalized together with membrane structures"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694308__layout_2456"
      uniprot "NA"
    ]
    graphics [
      x 411.81254976609796
      y 219.14871413122364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694572;PUBMED:19398035;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_367"
      name "3a localizes to the cell membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694572__layout_2454"
      uniprot "NA"
    ]
    graphics [
      x 364.93884825803093
      y 310.83538770039615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694718;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_389"
      name "O-glycosylation of 3a is terminated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694718__layout_2397"
      uniprot "NA"
    ]
    graphics [
      x 611.4871013760476
      y 224.47303955247526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_389"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9699093;urn:miriam:obo.chebi:CHEBI%3A16556"
      hgnc "NA"
      map_id "CMP_minus_Neu5Ac"
      name "CMP_minus_Neu5Ac"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2068; layout_3350"
      uniprot "NA"
    ]
    graphics [
      x 565.7589747184982
      y 102.21241966260038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP_minus_Neu5Ac"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q16842;urn:miriam:reactome:R-HSA-9683042;urn:miriam:uniprot:Q11201;urn:miriam:uniprot:Q11203;urn:miriam:uniprot:Q9UJ37;urn:miriam:uniprot:Q9H4F1;urn:miriam:uniprot:P15907;urn:miriam:uniprot:Q8NDV1;urn:miriam:uniprot:Q11206"
      hgnc "NA"
      map_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      name "sialyltransferases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_351; layout_2401"
      uniprot "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    ]
    graphics [
      x 821.5382336038167
      y 239.13898317099097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094; urn:miriam:obo.chebi:CHEBI%3A17361"
      hgnc "NA"
      map_id "CMP"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2069; layout_3351; sa34"
      uniprot "NA"
    ]
    graphics [
      x 656.8535630389222
      y 105.54493561805577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697018;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_403"
      name "Addition of sialic acids on some Spike glycosyl sidechains"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697018__layout_3054"
      uniprot "NA"
    ]
    graphics [
      x 1038.6129985771042
      y 340.8610217807126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_403"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9685933"
      hgnc "NA"
      map_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      name "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_583; layout_2457"
      uniprot "NA"
    ]
    graphics [
      x 370.5751404331294
      y 94.11720332014283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1970; layout_2331"
      uniprot "NA"
    ]
    graphics [
      x 322.2209542288913
      y 209.4135341277356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694447;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_340"
      name "mRNA2 is translated to Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694447__layout_2327"
      uniprot "NA"
    ]
    graphics [
      x 1074.650573982981
      y 686.82516621898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694656;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_382"
      name "Spike trimer glycoside chains are extended"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694656__layout_2386"
      uniprot "NA"
    ]
    graphics [
      x 1038.5349834225071
      y 502.60067915511354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694780;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_396"
      name "Spike trimer translocates to ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694780__layout_2384"
      uniprot "NA"
    ]
    graphics [
      x 1163.5724061362998
      y 484.7424162203692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696807;PUBMED:32366695;PUBMED:18003979"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_401"
      name "N-glycan mannose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696807__layout_3043"
      uniprot "NA"
    ]
    graphics [
      x 1260.2929501491863
      y 406.4740812816205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_401"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694337;PUBMED:22915798"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_318"
      name "Trimmed spike protein binds to calnexin"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694337__layout_2375"
      uniprot "NA"
    ]
    graphics [
      x 1174.4775258981451
      y 809.1444142178345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694467;PUBMED:32587972"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_344"
      name "Spike protein forms a homotrimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694467__layout_2382"
      uniprot "NA"
    ]
    graphics [
      x 1167.899937524845
      y 563.1202806619326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694793;PUBMED:11470266;PUBMED:32366695;PUBMED:32363391;PUBMED:32676595;PUBMED:32518941"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_399"
      name "Spike protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694793__layout_2365"
      uniprot "NA"
    ]
    graphics [
      x 1487.260499592707
      y 416.49379381220035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696980;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_402"
      name "Spike trimer glycoside chains get additional branches"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696980__layout_3049"
      uniprot "NA"
    ]
    graphics [
      x 1416.8555935094191
      y 466.2405438413069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_402"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694341;PUBMED:17134730;PUBMED:33310888;PUBMED:20580052"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_320"
      name "Spike protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694341__layout_2378"
      uniprot "NA"
    ]
    graphics [
      x 1365.2107867881155
      y 245.3851533678444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694364;PUBMED:32366695;PUBMED:10929008;PUBMED:12145188"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_324"
      name "N-glycan glucose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694364__layout_2370"
      uniprot "NA"
    ]
    graphics [
      x 1699.0001241146883
      y 906.5020084906484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 1684.3278556348569
      y 1051.9590890765726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682983;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:pubmed:25348530;urn:miriam:uniprot:Q13724; urn:miriam:reactome:R-HSA-9686842;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      name "ER_space_alpha_minus_glucosidases; ER_minus_alpha_space_glucosidases:ER_minus_alpha_space_glucosidase_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_280; layout_1080; layout_2373; layout_2374"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 1912.9852157068221
      y 995.0668142736097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-532676;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "UNIPROT:Q13724"
      name "MOGS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2904"
      uniprot "UNIPROT:Q13724"
    ]
    graphics [
      x 1805.1800874708804
      y 1120.9695819384656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13724"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15903;urn:miriam:reactome:R-ALL-9683087"
      hgnc "NA"
      map_id "beta_minus_D_minus_glucose"
      name "beta_minus_D_minus_glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_278; layout_2372"
      uniprot "NA"
    ]
    graphics [
      x 1838.182441966392
      y 1049.8023105118975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "beta_minus_D_minus_glucose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686790;PUBMED:16188993;PUBMED:24716661;PUBMED:23816430;PUBMED:19223639;PUBMED:23503623;PUBMED:7986008"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_300"
      name "ER-alpha glucosidases bind ER-alpha glucosidase inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686790__layout_2954"
      uniprot "NA"
    ]
    graphics [
      x 2007.0925240179076
      y 926.76535672512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9686811"
      hgnc "NA"
      map_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      name "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1079; layout_2955"
      uniprot "NA"
    ]
    graphics [
      x 1900.0095601891958
      y 896.3660610906655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_minus_alpha_minus_glucosidase_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694377;PUBMED:15788388;PUBMED:21203998;PUBMED:27799534"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_328"
      name "pp1a cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694377__layout_2169"
      uniprot "NA"
    ]
    graphics [
      x 1764.1867379930814
      y 1283.9721553599256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 15
      diagram "R-HSA-9694516; WP4846; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681986; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681994; urn:miriam:reactome:R-COV-9684311;urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694778; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681991; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681992; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681990; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681995; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256; urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681983; urn:miriam:uniprot:P0DTC1;urn:miriam:pubmed:32169673; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1"
      name "pp1a_minus_nsp6_minus_11; pp1a; pp1a_space_dimer; pp1a_minus_3CL; pp1a_minus_nsp1_minus_4; pp1a_minus_nsp9; pp1a_minus_nsp8; pp1a_minus_nsp11; pp1a_minus_nsp7; pp1a_minus_nsp6; pp1a_minus_nsp10; orf1a; pp1a_space_Nsp6_minus_11; pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2172; layout_2719; layout_2168; layout_2547; layout_2208; layout_2546; layout_2542; layout_2543; layout_2544; layout_2184; layout_2545; eb8ab; sa2224; sa1789; sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1953.1238531547906
      y 1247.9775160287124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694334;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_317"
      name "mRNA1 is translated to pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694334__layout_2162"
      uniprot "NA"
    ]
    graphics [
      x 1709.881459049041
      y 1257.3497582166829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698265;PUBMED:31231549;PUBMED:15331731;PUBMED:21799305;PUBMED:25135833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_405"
      name "Enhanced autophagosome formation"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698265__layout_3102"
      uniprot "NA"
    ]
    graphics [
      x 2173.8850643202404
      y 1146.4265660792148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_405"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694567;PUBMED:27799534;PUBMED:21203998"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_365"
      name "pp1a forms a dimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694567__layout_2167"
      uniprot "NA"
    ]
    graphics [
      x 1955.2529150564883
      y 1366.2171811195776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-5205651;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-DDI-5205651-2;urn:miriam:reactome:R-DDI-5205651-3;urn:miriam:reactome:R-RNO-5205651;urn:miriam:reactome:R-CEL-5205651;urn:miriam:reactome:R-PFA-5205651;urn:miriam:reactome:R-BTA-5205651;urn:miriam:reactome:R-HSA-5205651;urn:miriam:reactome:R-DDI-5205651;urn:miriam:reactome:R-SCE-5205651;urn:miriam:reactome:R-SSC-5205651;urn:miriam:reactome:R-MMU-5205651;urn:miriam:reactome:R-DRE-5205651;urn:miriam:reactome:R-CFA-5205651;urn:miriam:reactome:R-SPO-5205651; urn:miriam:reactome:R-CEL-5683641;urn:miriam:reactome:R-DDI-5683641-3;urn:miriam:reactome:R-DDI-5683641-2;urn:miriam:reactome:R-RNO-5683641;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-5683641;urn:miriam:reactome:R-SPO-5683641;urn:miriam:reactome:R-CFA-5683641;urn:miriam:reactome:R-GGA-5683641;urn:miriam:reactome:R-MMU-5683641;urn:miriam:reactome:R-DRE-5683641;urn:miriam:reactome:R-DDI-5683641;urn:miriam:reactome:R-PFA-5683641;urn:miriam:reactome:R-SCE-5683641;urn:miriam:reactome:R-SSC-5683641;urn:miriam:reactome:R-BTA-5683641"
      hgnc "NA"
      map_id "UNIPROT:Q9GZQ8"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1492; layout_1507; layout_2510; layout_2513"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 2067.511838533413
      y 984.7399871615455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q99570;urn:miriam:reactome:R-CEL-5683632;urn:miriam:reactome:R-HSA-5683632;urn:miriam:reactome:R-XTR-5683632;urn:miriam:reactome:R-DME-5683632;urn:miriam:reactome:R-RNO-5683632;urn:miriam:reactome:R-SPO-5683632;urn:miriam:reactome:R-MMU-5683632;urn:miriam:reactome:R-DDI-5683632;urn:miriam:reactome:R-CFA-5683632;urn:miriam:reactome:R-GGA-5683632;urn:miriam:reactome:R-DRE-5683632;urn:miriam:reactome:R-SCE-5683632;urn:miriam:uniprot:Q14457;urn:miriam:reactome:R-SSC-5683632;urn:miriam:uniprot:Q9P2Y5;urn:miriam:uniprot:Q8NEB9"
      hgnc "NA"
      map_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      name "UVRAG_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1644; layout_2515"
      uniprot "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    ]
    graphics [
      x 2268.3529559257913
      y 1046.3325211870415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-XTR-917723;urn:miriam:uniprot:Q9UQN3;urn:miriam:reactome:R-DDI-917723;urn:miriam:reactome:R-DME-917723;urn:miriam:uniprot:Q8WUX9;urn:miriam:reactome:R-SSC-917723;urn:miriam:reactome:R-GGA-917723;urn:miriam:uniprot:Q96CF2;urn:miriam:reactome:R-RNO-917723;urn:miriam:uniprot:Q9Y3E7;urn:miriam:uniprot:Q9BY43;urn:miriam:reactome:R-DRE-917723;urn:miriam:reactome:R-HSA-917723;urn:miriam:reactome:R-CFA-917723;urn:miriam:reactome:R-BTA-917723;urn:miriam:uniprot:Q9H444;urn:miriam:uniprot:Q96FZ7;urn:miriam:reactome:R-SPO-917723;urn:miriam:reactome:R-MMU-917723;urn:miriam:uniprot:O43633;urn:miriam:reactome:R-SCE-917723;urn:miriam:reactome:R-CEL-917723"
      hgnc "NA"
      map_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      name "ESCRT_minus_III"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1643; layout_2514"
      uniprot "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    ]
    graphics [
      x 2218.7346727365566
      y 992.3286856510781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9694516; WP5038; WP5039; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2; urn:miriam:uniprot:P0DTD2; urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "UNIPROT:P0DTD2"
      name "9b_space_homodimer; ORF9b; PLpro; Orf9b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_3481; e62df; bca7b; f4882; sa139; sa1878; sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 2179.5357900461127
      y 1042.7263870609906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683736;urn:miriam:obo.chebi:CHEBI%3A15525"
      hgnc "NA"
      map_id "palmitoyl_minus_CoA"
      name "palmitoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_291; layout_2379"
      uniprot "NA"
    ]
    graphics [
      x 1339.2468315135525
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "palmitoyl_minus_CoA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 3
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:reactome:R-ALL-162743;urn:miriam:obo.chebi:CHEBI%3A57287; urn:miriam:obo.chebi:CHEBI%3A15346"
      hgnc "NA"
      map_id "CoA_minus_SH"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_292; layout_2380; sa71"
      uniprot "NA"
    ]
    graphics [
      x 1492.6383769919528
      y 172.82223509802168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA_minus_SH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694401;PUBMED:22548323;PUBMED:16507314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_332"
      name "E protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694401__layout_2406"
      uniprot "NA"
    ]
    graphics [
      x 1414.0820770699056
      y 131.94274677112833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-1028782;urn:miriam:reactome:R-HSA-1028782;urn:miriam:reactome:R-XTR-1028782;urn:miriam:reactome:R-DME-1028782;urn:miriam:uniprot:Q9BYC5;urn:miriam:reactome:R-DDI-1028782;urn:miriam:reactome:R-MMU-1028782;urn:miriam:reactome:R-DRE-1028782;urn:miriam:reactome:R-CEL-1028782;urn:miriam:reactome:R-RNO-1028782"
      hgnc "NA"
      map_id "UNIPROT:Q9BYC5"
      name "FUT8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3051"
      uniprot "UNIPROT:Q9BYC5"
    ]
    graphics [
      x 1433.6196765785135
      y 570.0587472524244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYC5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DME-975913;urn:miriam:reactome:R-BTA-975913;urn:miriam:reactome:R-HSA-975913;urn:miriam:reactome:R-GGA-975913;urn:miriam:reactome:R-DRE-975913;urn:miriam:reactome:R-RNO-975913;urn:miriam:reactome:R-SSC-975913;urn:miriam:uniprot:Q9UBM8;urn:miriam:uniprot:Q9UM21;urn:miriam:reactome:R-XTR-975913;urn:miriam:uniprot:Q9UQ53;urn:miriam:reactome:R-CFA-975913;urn:miriam:reactome:R-MMU-975913"
      hgnc "NA"
      map_id "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
      name "MGAT4s"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3052"
      uniprot "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
    ]
    graphics [
      x 1522.5979525895964
      y 573.7459944273623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q09328;urn:miriam:reactome:R-CEL-975896;urn:miriam:reactome:R-RNO-975896;urn:miriam:reactome:R-GGA-975896;urn:miriam:reactome:R-DRE-975896;urn:miriam:reactome:R-MMU-975896;urn:miriam:reactome:R-SSC-975896;urn:miriam:reactome:R-CFA-975896;urn:miriam:reactome:R-BTA-975896;urn:miriam:reactome:R-HSA-975896;urn:miriam:reactome:R-XTR-975896"
      hgnc "NA"
      map_id "UNIPROT:Q09328"
      name "MGAT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3053"
      uniprot "UNIPROT:Q09328"
    ]
    graphics [
      x 1552.2184130567443
      y 496.843665779337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q09328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-449651;urn:miriam:obo.chebi:CHEBI%3A53019"
      hgnc "NA"
      map_id "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      name "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2901"
      uniprot "NA"
    ]
    graphics [
      x 1562.7087711457839
      y 306.5498886693771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P61803;urn:miriam:uniprot:P39656;urn:miriam:uniprot:Q13454;urn:miriam:uniprot:P46977;urn:miriam:reactome:R-HSA-532516;urn:miriam:uniprot:Q9H0U3;urn:miriam:uniprot:P04843;urn:miriam:uniprot:P04844"
      hgnc "NA"
      map_id "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
      name "OST_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2907"
      uniprot "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
    ]
    graphics [
      x 1616.5091123137404
      y 356.7356382703873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16214;urn:miriam:reactome:R-ALL-449311"
      hgnc "NA"
      map_id "DOLP"
      name "DOLP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2902"
      uniprot "NA"
    ]
    graphics [
      x 1603.547291564539
      y 437.4854807865858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DOLP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-195906;urn:miriam:uniprot:P27824;urn:miriam:reactome:R-BTA-195906;urn:miriam:reactome:R-CFA-195906;urn:miriam:reactome:R-SPO-195906;urn:miriam:reactome:R-SSC-195906;urn:miriam:reactome:R-RNO-195906;urn:miriam:reactome:R-GGA-195906;urn:miriam:reactome:R-DRE-195906;urn:miriam:reactome:R-CEL-195906;urn:miriam:reactome:R-DDI-195906;urn:miriam:reactome:R-MMU-195906;urn:miriam:reactome:R-DME-195906;urn:miriam:reactome:R-XTR-195906;urn:miriam:reactome:R-DME-195906-2;urn:miriam:reactome:R-SCE-195906;urn:miriam:reactome:R-SPO-195906-2;urn:miriam:reactome:R-DME-195906-3;urn:miriam:reactome:R-SPO-195906-3;urn:miriam:reactome:R-DME-195906-4"
      hgnc "NA"
      map_id "UNIPROT:P27824"
      name "CANX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_288; layout_2377"
      uniprot "UNIPROT:P27824"
    ]
    graphics [
      x 1115.4331541683428
      y 1033.639229120842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27824"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q9BV94;urn:miriam:reactome:R-HSA-6782581;urn:miriam:uniprot:Q9UKM7"
      hgnc "NA"
      map_id "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
      name "MAN1B1,EDEM2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3044"
      uniprot "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
    ]
    graphics [
      x 1135.4337852501126
      y 396.2459330056638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-964750;urn:miriam:reactome:R-XTR-964750;urn:miriam:reactome:R-GGA-964750;urn:miriam:reactome:R-DME-964750;urn:miriam:reactome:R-CEL-964750;urn:miriam:reactome:R-DDI-964750;urn:miriam:uniprot:P26572;urn:miriam:reactome:R-BTA-964750;urn:miriam:reactome:R-CFA-964750;urn:miriam:reactome:R-DRE-964750;urn:miriam:reactome:R-RNO-964750;urn:miriam:reactome:R-SSC-964750;urn:miriam:reactome:R-HSA-964750"
      hgnc "NA"
      map_id "UNIPROT:P26572"
      name "MGAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_320; layout_2390"
      uniprot "UNIPROT:P26572"
    ]
    graphics [
      x 911.7481157049998
      y 440.7548889443613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P26572"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-RNO-975823;urn:miriam:reactome:R-CFA-975823;urn:miriam:reactome:R-GGA-975823;urn:miriam:uniprot:Q16706;urn:miriam:reactome:R-HSA-975823;urn:miriam:reactome:R-DRE-975823;urn:miriam:reactome:R-XTR-975823;urn:miriam:reactome:R-MMU-975823;urn:miriam:reactome:R-SSC-975823"
      hgnc "NA"
      map_id "UNIPROT:Q16706"
      name "MAN2A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2905"
      uniprot "UNIPROT:Q16706"
    ]
    graphics [
      x 925.1013134190208
      y 556.3740131189018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16706"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-975830;urn:miriam:reactome:R-RNO-975830;urn:miriam:reactome:R-GGA-975830;urn:miriam:reactome:R-DRE-975830;urn:miriam:reactome:R-DME-975830;urn:miriam:reactome:R-MMU-975830;urn:miriam:reactome:R-CFA-975830;urn:miriam:reactome:R-BTA-975830;urn:miriam:reactome:R-HSA-975830;urn:miriam:reactome:R-XTR-975830;urn:miriam:uniprot:Q10469"
      hgnc "NA"
      map_id "UNIPROT:Q10469"
      name "MGAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2906"
      uniprot "UNIPROT:Q10469"
    ]
    graphics [
      x 963.4898309391818
      y 381.0190977425213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10469"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1969; layout_2328"
      uniprot "NA"
    ]
    graphics [
      x 961.7732359382445
      y 827.3729457240389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694280;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_308"
      name "mRNA4 is translated to protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694280__layout_2333"
      uniprot "NA"
    ]
    graphics [
      x 1320.1571857689446
      y 189.10155449406034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694452;PUBMED:15522242;PUBMED:22819936"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_341"
      name "Protein E forms a homopentamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694452__layout_2411"
      uniprot "NA"
    ]
    graphics [
      x 1324.1157698707264
      y 315.18908655797907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694529;PUBMED:20409569"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_357"
      name "Ubiquination of protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694529__layout_2408"
      uniprot "NA"
    ]
    graphics [
      x 1564.0495873768618
      y 211.68804587312684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694294;PUBMED:34237302;PUBMED:21524776;PUBMED:33709461;PUBMED:21450821"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_311"
      name "E pentamer is transported to the Golgi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694294__layout_2413"
      uniprot "NA"
    ]
    graphics [
      x 1462.259982664091
      y 265.33887226238573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P62987;urn:miriam:uniprot:P62979;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:P0CG47;urn:miriam:reactome:R-HSA-8943136"
      hgnc "NA"
      map_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      name "Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_364; layout_2409"
      uniprot "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    ]
    graphics [
      x 1651.6128082894725
      y 107.6618528594687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685917;urn:miriam:reactome:R-COV-9694622;urn:miriam:refseq:NC_004718.3; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694622;urn:miriam:reactome:R-COV-9685917"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1971; layout_2334"
      uniprot "NA"
    ]
    graphics [
      x 1251.3580989542377
      y 276.6698628241678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:NC_004718.3; urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:MN908947.3"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1972; layout_2337"
      uniprot "NA"
    ]
    graphics [
      x 1910.7562717238611
      y 1590.2564652460744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 8
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_methionine"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_422; layout_155; layout_182; layout_2299; layout_2282; layout_2431; layout_3787; layout_3766"
      uniprot "NA"
    ]
    graphics [
      x 1785.1941641811568
      y 811.0859995137711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_methionine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-9632173;urn:miriam:reactome:R-CFA-9632173;urn:miriam:reactome:R-HSA-9632173;urn:miriam:reactome:R-DRE-9632173;urn:miriam:reactome:R-XTR-9632173;urn:miriam:uniprot:Q99873;urn:miriam:reactome:R-DME-9632173;urn:miriam:reactome:R-RNO-9632173"
      hgnc "NA"
      map_id "UNIPROT:Q99873"
      name "PRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3788"
      uniprot "UNIPROT:Q99873"
    ]
    graphics [
      x 1519.641387086834
      y 921.459556895462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99873"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "S_minus_adenosyl_minus_L_minus_homocysteine"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_420; layout_158; layout_184; layout_2302; layout_2284; layout_2433; layout_3538"
      uniprot "NA"
    ]
    graphics [
      x 1724.2743934939897
      y 797.4179518354497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_adenosyl_minus_L_minus_homocysteine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16846;urn:miriam:reactome:R-ALL-9683025"
      hgnc "NA"
      map_id "UDP_minus_GalNAc"
      name "UDP_minus_GalNAc"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_344; layout_2394"
      uniprot "NA"
    ]
    graphics [
      x 596.8884711792771
      y 662.747946637716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus_GalNAc"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q10472;urn:miriam:reactome:R-HSA-9682906"
      hgnc "NA"
      map_id "UNIPROT:Q10472"
      name "GALNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_346; layout_2396"
      uniprot "UNIPROT:Q10472"
    ]
    graphics [
      x 541.0755936699102
      y 557.5121080802077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10472"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078; urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659; urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "UDP"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319; layout_2389; sa22; sa35"
      uniprot "NA"
    ]
    graphics [
      x 584.0258918571617
      y 470.30805807248737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694370;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_326"
      name "mRNA9a is translated to Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694370__layout_2339"
      uniprot "NA"
    ]
    graphics [
      x 680.429573843574
      y 1184.5502826358281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694363;PUBMED:32654247;PUBMED:33794152;PUBMED:33594360;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_323"
      name "Tetramerisation of nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694363__layout_3642"
      uniprot "NA"
    ]
    graphics [
      x 857.6509428927872
      y 1230.7714826572592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694568;PUBMED:17210170"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_366"
      name "Nucleoprotein translocates to the ERGIC outer membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694568__layout_2363"
      uniprot "NA"
    ]
    graphics [
      x 793.2704877118128
      y 1055.897883890794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694373;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_327"
      name "Unphosphorylated nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694373__layout_2342"
      uniprot "NA"
    ]
    graphics [
      x 758.8842196149092
      y 1201.6465657566735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694455;PUBMED:24418573;PUBMED:17002283;PUBMED:15147189;PUBMED:17379242;PUBMED:15849181;PUBMED:18456656;PUBMED:15094372;PUBMED:19052082;PUBMED:16214138;PUBMED:16103198;PUBMED:15020242;PUBMED:18561946;PUBMED:23717688;PUBMED:17881296;PUBMED:18631359;PUBMED:16228284"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_343"
      name "SUMO-p-N protein dimer binds genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694455__layout_2464"
      uniprot "NA"
    ]
    graphics [
      x 1127.5759906967448
      y 1199.9009057907806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694575;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_368"
      name "Nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694575__layout_2351"
      uniprot "NA"
    ]
    graphics [
      x 738.1827846726457
      y 1100.343294191519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694345;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_322"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694345__layout_2360"
      uniprot "NA"
    ]
    graphics [
      x 785.1084011350134
      y 1147.7062088677708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694281;PUBMED:24418573;PUBMED:23717688;PUBMED:17229691;PUBMED:17379242;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_309"
      name "Encapsidation of SARS coronavirus genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694281__layout_2466"
      uniprot "NA"
    ]
    graphics [
      x 800.2605640319023
      y 1247.605023626597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729307;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_412"
      name "Nucleoprotein is SUMOylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729307__layout_3638"
      uniprot "NA"
    ]
    graphics [
      x 703.1143552399055
      y 1339.6806923857496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694723;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_391"
      name "Uncoating of SARS-CoV-2 Genome"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694723__layout_2501"
      uniprot "NA"
    ]
    graphics [
      x 1113.5791140918836
      y 1260.3261377628778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_391"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165; urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165;urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279;UNIPROT:P63165"
      name "SUMO1:C93_minus_UBE2I"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1896; layout_3633"
      uniprot "UNIPROT:P63279;UNIPROT:P63165"
    ]
    graphics [
      x 588.8884061039792
      y 1414.1268553740415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279;UNIPROT:P63165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "UNIPROT:P63279"
      name "UBE2I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_259; layout_3792"
      uniprot "UNIPROT:P63279"
    ]
    graphics [
      x 661.6922928462004
      y 1470.695235572248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P63279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694513;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685921; urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694513;urn:miriam:reactome:R-COV-9685921"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      name "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1990; layout_2340"
      uniprot "NA"
    ]
    graphics [
      x 567.9243801139025
      y 1143.3633454400888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P49840;urn:miriam:reactome:R-HSA-198358;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49840;UNIPROT:P49841"
      name "GSK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2262; layout_2314"
      uniprot "UNIPROT:P49840;UNIPROT:P49841"
    ]
    graphics [
      x 880.0263037141493
      y 929.0909634926196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49840;UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SPO-2997551-3;urn:miriam:reactome:R-XTR-2997551;urn:miriam:reactome:R-SPO-2997551-2;urn:miriam:reactome:R-CEL-2997551;urn:miriam:reactome:R-SSC-2997551;urn:miriam:uniprot:P49841;urn:miriam:reactome:R-BTA-2997551;urn:miriam:reactome:R-SCE-2997551-2;urn:miriam:reactome:R-SCE-2997551-3;urn:miriam:reactome:R-SCE-2997551-4;urn:miriam:reactome:R-DRE-2997551;urn:miriam:reactome:R-SPO-2997551;urn:miriam:reactome:R-HSA-2997551;urn:miriam:reactome:R-DME-2997551;urn:miriam:reactome:R-RNO-198619;urn:miriam:reactome:R-SCE-2997551;urn:miriam:reactome:R-DME-2997551-2;urn:miriam:reactome:R-CEL-2997551-3;urn:miriam:reactome:R-DME-2997551-3;urn:miriam:reactome:R-CEL-2997551-4;urn:miriam:reactome:R-GGA-2997551;urn:miriam:reactome:R-CEL-2997551-2;urn:miriam:reactome:R-CFA-2997551;urn:miriam:reactome:R-MMU-2997551;urn:miriam:reactome:R-DDI-2997551; urn:miriam:reactome:R-HSA-9687663;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "UNIPROT:P49841"
      name "GSK3B; GSK3B:GSKi"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_1717; layout_1895; layout_3593; layout_2349"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 829.33484346003
      y 808.3819013371284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687724;PUBMED:34593624;PUBMED:19666099;PUBMED:19389332;PUBMED:11162580;PUBMED:24931005;PUBMED:18977324;PUBMED:18938143;PUBMED:19106108;PUBMED:18806775"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_301"
      name "GSK3B binds GSKi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687724__layout_3596"
      uniprot "NA"
    ]
    graphics [
      x 773.0889319806324
      y 659.9365753346815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687688;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "GSKi"
      name "GSKi"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1894; layout_2949"
      uniprot "NA"
    ]
    graphics [
      x 741.8940113378324
      y 527.9067280746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSKi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:12134018;urn:miriam:uniprot:P78362;urn:miriam:reactome:R-HSA-9729312;urn:miriam:pubmed:12565829;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q96SB4"
      hgnc "NA"
      map_id "UNIPROT:P78362;UNIPROT:Q96SB4"
      name "SRPK1_slash_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3791"
      uniprot "UNIPROT:P78362;UNIPROT:Q96SB4"
    ]
    graphics [
      x 1021.1168698884921
      y 1239.0689938562057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78362;UNIPROT:Q96SB4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694619;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9684636"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2305"
      uniprot "NA"
    ]
    graphics [
      x 1757.258555357596
      y 1085.63059417062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694274;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_306"
      name "mRNA1 is translated to pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694274__layout_2165"
      uniprot "NA"
    ]
    graphics [
      x 1310.9387051995168
      y 977.511654551947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694677;PUBMED:22362731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_386"
      name "ZCRB1 binds 5'UTR of SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694677__layout_2315"
      uniprot "NA"
    ]
    graphics [
      x 1524.2416700258937
      y 1406.5642919362253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_386"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9698376;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "UNIPROT:Q8TBF4"
      name "ZCRB1; ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand); ZCRB1:m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_201; layout_202; layout_2316; layout_3236"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 1551.7521178306727
      y 1565.2161804415878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1313.701159589423
      y 794.9665286809822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694406;PUBMED:19224332"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_333"
      name "nsp13 binds DDX5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694406__layout_2318"
      uniprot "NA"
    ]
    graphics [
      x 1042.1706393580362
      y 779.1237221074844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694436;PUBMED:32304108;PUBMED:32803198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_335"
      name "nsp15 forms a hexamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694436__layout_2244"
      uniprot "NA"
    ]
    graphics [
      x 1231.6155209978333
      y 720.431890797022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694520;PUBMED:25732088"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_352"
      name "nsp16 binds VHL"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694520__layout_2324"
      uniprot "NA"
    ]
    graphics [
      x 1234.5097831592104
      y 492.1190456721075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694390;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_330"
      name "nsp15 binds RB1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694390__layout_2321"
      uniprot "NA"
    ]
    graphics [
      x 1129.9779208063999
      y 625.3347791555136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SSC-9660621;urn:miriam:reactome:R-DME-9660621-2;urn:miriam:reactome:R-CFA-9660621;urn:miriam:reactome:R-DRE-9660621;urn:miriam:reactome:R-MMU-9660621;urn:miriam:reactome:R-DDI-9660621;urn:miriam:reactome:R-HSA-9660621;urn:miriam:reactome:R-XTR-9660621;urn:miriam:reactome:R-CEL-9660621;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-RNO-9660621;urn:miriam:reactome:R-DME-9660621;urn:miriam:reactome:R-DME-9660621-3"
      hgnc "NA"
      map_id "UNIPROT:P06400"
      name "RB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_207; layout_2322"
      uniprot "UNIPROT:P06400"
    ]
    graphics [
      x 995.4750586486529
      y 533.10210287067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694258;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-HSA-9682726;urn:miriam:pubmed:22301153"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P06400"
      name "nsp15:RB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3094"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P06400"
    ]
    graphics [
      x 1090.9288012023776
      y 488.89296592861734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P06400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-391423;urn:miriam:reactome:R-DDI-391423;urn:miriam:reactome:R-MMU-391423;urn:miriam:reactome:R-DME-391423;urn:miriam:reactome:R-XTR-391423;urn:miriam:reactome:R-SSC-391423;urn:miriam:reactome:R-RNO-391423;urn:miriam:reactome:R-GGA-391423;urn:miriam:reactome:R-DRE-391423;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-BTA-391423;urn:miriam:reactome:R-HSA-391423;urn:miriam:reactome:R-CFA-391423"
      hgnc "NA"
      map_id "UNIPROT:P40337"
      name "VHL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_215; layout_2325"
      uniprot "UNIPROT:P40337"
    ]
    graphics [
      x 1210.0818089546615
      y 350.1358457946992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694398;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-HSA-9683453;urn:miriam:pubmed:25732088"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P40337"
      name "nsp16:VHL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3093"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P40337"
    ]
    graphics [
      x 1149.80076118863
      y 337.6887680544454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P40337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9682643"
      hgnc "NA"
      map_id "UNIPROT:P17844"
      name "DDX5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_204; layout_2319"
      uniprot "UNIPROT:P17844"
    ]
    graphics [
      x 897.1652817627337
      y 730.0231534389576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:19224332;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-HSA-9682634;urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9694692"
      hgnc "NA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P17844"
      name "nsp13:DDX5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3082"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P17844"
    ]
    graphics [
      x 895.7225429300822
      y 835.4817663441886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P17844"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 2
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9689912;urn:miriam:obo.chebi:CHEBI%3A26558"
      hgnc "NA"
      map_id "NMP"
      name "NMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1998; layout_2274"
      uniprot "NA"
    ]
    graphics [
      x 1756.4246487257894
      y 875.4509474971306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694566;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-9687117"
      hgnc "NA"
      map_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
      name "nsp8:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3096"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 1842.619720830205
      y 744.2823116181428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 1677.5803545490896
      y 1115.9321404029577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685513;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694603"
      hgnc "NA"
      map_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2289"
      uniprot "NA"
    ]
    graphics [
      x 1621.630024599308
      y 1096.5705773632733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-ALL-1500648"
      hgnc "NA"
      map_id "Mg2_plus__slash_Mn2_plus_"
      name "Mg2_plus__slash_Mn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3797"
      uniprot "NA"
    ]
    graphics [
      x 1262.4942809922475
      y 1146.2097842583103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mg2_plus__slash_Mn2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "a_space_nucleotide_space_sugar"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_48; layout_67; layout_2210; layout_2224"
      uniprot "NA"
    ]
    graphics [
      x 1159.5203245920748
      y 926.0013339712272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a_space_nucleotide_space_sugar"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_51; layout_69; layout_2213; layout_2227"
      uniprot "NA"
    ]
    graphics [
      x 1120.1295441656775
      y 974.7991423242122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:reactome:R-ALL-9731705"
      hgnc "NA"
      map_id "3CLp_space_inhibitors"
      name "3CLp_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_3674"
      uniprot "NA"
    ]
    graphics [
      x 1221.8645849149987
      y 1441.5799687791675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "3CLp_space_inhibitors"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 1940.1139559379694
      y 1064.7364825951945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 220
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 1
    target 3
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 1
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "RTC_space_inhibitors"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 5
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 5
    target 3
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 5
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 5
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 5
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 5
    target 8
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 5
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 5
    target 10
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 5
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 5
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_378"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 5
    target 13
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 5
    target 14
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 5
    target 15
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 5
    target 16
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 5
    target 17
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 5
    target 18
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 5
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 5
    target 20
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 5
    target 21
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 5
    target 22
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 5
    target 23
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 5
    target 24
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 5
    target 25
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 5
    target 26
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 5
    target 27
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 5
    target 28
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 5
    target 29
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 5
    target 30
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 5
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 5
    target 32
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 5
    target 33
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 5
    target 34
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 5
    target 35
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 5
    target 36
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 5
    target 37
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 5
    target 38
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 5
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 5
    target 40
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 5
    target 41
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 5
    target 42
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 5
    target 43
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 5
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 5
    target 45
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
      target_id "R2_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 135
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 219
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 172
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 6
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 6
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 6
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 172
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 7
    target 214
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 7
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 46
    target 8
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 58
    target 8
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 8
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_321"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 135
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 219
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 172
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 9
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 9
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 9
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 216
    target 10
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 10
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 10
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 135
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 219
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 172
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 11
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 11
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 11
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 218
    target 13
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "3CLp_space_inhibitors"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 200
    target 15
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 135
    target 15
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 135
    target 16
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 142
    target 16
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 216
    target 17
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 17
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 17
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 46
    target 18
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 18
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_351"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 200
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 135
    target 20
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 200
    target 20
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 200
    target 21
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 46
    target 22
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 22
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_307"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 47
    target 23
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 216
    target 24
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "a_space_nucleotide_space_sugar"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 24
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 24
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "nucleoside_space_5'_minus_diphosphate(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 200
    target 25
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 46
    target 26
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 26
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_375"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 135
    target 28
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 172
    target 29
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 29
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 29
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 215
    target 30
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "Mg2_plus__slash_Mn2_plus_"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 200
    target 30
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 46
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 31
    target 214
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 31
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 214
    target 34
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 200
    target 35
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 172
    target 36
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 36
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 36
    target 52
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 51
    target 37
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 37
    target 213
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 37
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 146
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 39
    target 212
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_371"
      target_id "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 135
    target 40
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 142
    target 40
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 135
    target 41
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 41
    target 211
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_379"
      target_id "NMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 46
    target 42
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 42
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_398"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 142
    target 43
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 135
    target 43
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 46
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NTP(4_minus_)"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 44
    target 47
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 44
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 49
    target 47
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 187
    target 47
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_391"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 47
    target 143
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 47
    target 182
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 47
    target 197
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 47
    target 198
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 49
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 50
    target 48
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 51
    target 49
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 196
    target 49
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 51
    target 50
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 52
    target 50
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 50
    target 53
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 51
    target 54
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 51
    target 55
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 51
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 51
    target 57
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 58
    target 54
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 195
    target 54
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P78362;UNIPROT:Q96SB4"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 54
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 54
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 58
    target 55
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 55
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 55
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 58
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 191
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49840;UNIPROT:P49841"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 192
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "UNIPROT:P49841"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 56
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 56
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 58
    target 57
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 59
    target 57
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48729"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 57
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 57
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 94
    target 58
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 178
    target 58
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_326"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 101
    target 58
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 58
    target 179
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 58
    target 180
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 58
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 58
    target 182
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 58
    target 183
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 58
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 58
    target 184
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 58
    target 185
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 58
    target 186
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 58
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 58
    target 66
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 58
    target 187
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9"
      target_id "R2_391"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 62
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 63
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 64
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 65
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 66
    target 61
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 61
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "R2_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 75
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 82
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 62
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 88
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 175
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus_GalNAc"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 176
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10472"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 63
    target 177
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 172
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "S_minus_adenosyl_minus_L_minus_methionine"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 173
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q99873"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 64
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "S_minus_adenosyl_minus_L_minus_homocysteine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 74
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 75
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "nucleotide_minus_sugar"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 65
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 71
    target 66
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 72
    target 66
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 66
    target 73
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 68
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R2_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 67
    target 69
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_297"
      target_id "CQ2_plus_,_space_HCQ2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 68
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CQ,_space_HCQ"
      target_id "R2_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 77
    target 74
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_387"
      target_id "UNIPROT:P0DTC5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 74
    target 78
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 74
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 74
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 74
    target 81
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC5"
      target_id "R2_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 171
    target 77
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      target_id "R2_387"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 82
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 80
    target 83
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_338"
      target_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 165
    target 82
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_308"
      target_id "UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 82
    target 166
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 82
    target 167
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 82
    target 152
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 82
    target 168
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC4"
      target_id "R2_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 83
    target 84
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 85
    target 84
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 84
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_363"
      target_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 125
    target 85
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_340"
      target_id "UNIPROT:P0DTC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 85
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 85
    target 127
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_396"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 85
    target 122
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 85
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 85
    target 129
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 85
    target 130
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_344"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 85
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 85
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 85
    target 133
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 85
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 86
    target 87
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 88
    target 87
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 89
    target 87
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC7"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 87
    target 90
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_356"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 112
    target 88
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_400"
      target_id "UNIPROT:P0DTC3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 88
    target 113
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_392"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 88
    target 114
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_384"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 88
    target 115
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 88
    target 116
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 88
    target 117
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 88
    target 118
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 91
    target 90
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 92
    target 90
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 93
    target 90
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 90
    target 94
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 90
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_380"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 90
    target 96
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_381"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 90
    target 97
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 99
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 102
    target 91
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09958"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 91
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 99
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 103
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15393"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 92
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 99
    target 93
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 109
    target 93
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07711"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 93
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 107
    target 94
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P55072"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 94
    target 108
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 98
    target 97
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 97
    target 99
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_370"
      target_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 101
    target 98
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 99
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_404"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 99
    target 101
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 102
    target 101
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P09958"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 103
    target 101
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O15393"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 101
    target 104
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 103
    target 105
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15393"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 106
    target 105
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "TMPRSS2_space_inhibitors"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 109
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07711"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 111
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CTSL_space_inhibitors"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 124
    target 112
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      target_id "R2_400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 123
    target 116
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      target_id "R2_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 119
    target 118
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "CMP_minus_Neu5Ac"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 120
    target 118
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 118
    target 121
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_389"
      target_id "CMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 120
    target 122
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 164
    target 125
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      target_id "R2_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 161
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P26572"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 162
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16706"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 163
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10469"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 160
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 159
    target 129
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27824"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 156
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 157
    target 131
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 131
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_399"
      target_id "DOLP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 153
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYC5"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 154
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 155
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q09328"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 150
    target 133
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 133
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_320"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 135
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 136
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 137
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13724"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 134
    target 138
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_324"
      target_id "beta_minus_D_minus_glucose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 135
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 136
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 140
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 142
    target 141
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 143
    target 142
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_317"
      target_id "UNIPROT:P0DTC1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 142
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 142
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC1"
      target_id "R2_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 146
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9GZQ8"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 147
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 148
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 149
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTD2"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 150
    target 152
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "palmitoyl_minus_CoA"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 152
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_332"
      target_id "CoA_minus_SH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 170
    target 165
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      target_id "R2_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 169
    target 167
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 190
    target 178
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      target_id "R2_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 188
    target 186
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P63279;UNIPROT:P63165"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 186
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_412"
      target_id "UNIPROT:P63279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 192
    target 193
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P49841"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 194
    target 193
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "GSKi"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 197
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_306"
      target_id "UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 199
    target 198
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBF4"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 200
    target 201
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 200
    target 202
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 200
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 200
    target 204
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 209
    target 201
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17844"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 201
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_333"
      target_id "UNIPROT:P0DTD1;UNIPROT:P17844"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 207
    target 203
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P40337"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 203
    target 208
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_352"
      target_id "UNIPROT:P0DTD1;UNIPROT:P40337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 205
    target 204
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06400"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 204
    target 206
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_330"
      target_id "UNIPROT:P0DTD1;UNIPROT:P06400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
