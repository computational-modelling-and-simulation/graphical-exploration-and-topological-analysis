# generated with VANTED V2.8.2 at Fri Mar 04 10:06:58 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917572; urn:miriam:ncbiprotein:YP_009725306; NA"
      hgnc "NA"
      map_id "nsp10"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de394; a99b9; a8b3c; b19e5; glyph61"
      uniprot "NA"
    ]
    graphics [
      x 528.4547911707458
      y 371.4695131567017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_48"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph82"
      uniprot "NA"
    ]
    graphics [
      x 429.64189890303174
      y 232.34692336259357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph84"
      uniprot "NA"
    ]
    graphics [
      x 635.0922663623764
      y 488.73015447942373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; NA"
      hgnc "NA"
      map_id "nsp14"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9; glyph54; sa80"
      uniprot "NA"
    ]
    graphics [
      x 763.4897430750082
      y 457.2205601618569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436; NA"
      hgnc "NA"
      map_id "nsp12"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d0526; a4ef4; glyph62"
      uniprot "NA"
    ]
    graphics [
      x 741.6006308586686
      y 521.1198698451059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "CoV_space_poly_minus__br_merase_space_complex"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph5; glyph4"
      uniprot "NA"
    ]
    graphics [
      x 568.323450259379
      y 648.6072057326946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoV_space_poly_minus__br_merase_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph80"
      uniprot "NA"
    ]
    graphics [
      x 420.57987961238894
      y 711.2295335148026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph77"
      uniprot "NA"
    ]
    graphics [
      x 424.66848239369926
      y 645.6206979179772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32592996; urn:miriam:wikidata:Q89686805; NA"
      hgnc "NA"
      map_id "nsp9"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94; cf2d5; de7fa; glyph51; glyph42"
      uniprot "NA"
    ]
    graphics [
      x 425.65528327419196
      y 485.3046938347717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "complex"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph8; glyph6"
      uniprot "NA"
    ]
    graphics [
      x 522.4179253922954
      y 531.8708247799541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph13; glyph17"
      uniprot "NA"
    ]
    graphics [
      x 225.74167270075884
      y 621.680201912548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "Replication_space_transcription_space_complex_space_"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph3; glyph2"
      uniprot "NA"
    ]
    graphics [
      x 483.2483801606672
      y 862.6168155030716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Replication_space_transcription_space_complex_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph81"
      uniprot "NA"
    ]
    graphics [
      x 584.7568162866429
      y 1022.9417568232404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph86"
      uniprot "NA"
    ]
    graphics [
      x 631.8691906192145
      y 851.5068511904176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_36"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph71"
      uniprot "NA"
    ]
    graphics [
      x 754.6615580081128
      y 796.8814483770959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_39"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph74"
      uniprot "NA"
    ]
    graphics [
      x 694.4373387470122
      y 1017.2988312419488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_plus_)sgRNA_space_(2_minus_9)"
      name "(_plus_)sgRNA_space_(2_minus_9)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph20"
      uniprot "NA"
    ]
    graphics [
      x 748.5122920154245
      y 1092.472139412503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_plus_)sgRNA_space_(2_minus_9)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph88"
      uniprot "NA"
    ]
    graphics [
      x 906.2538507533745
      y 1042.6464304576712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_40"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph75"
      uniprot "NA"
    ]
    graphics [
      x 1006.0274010538363
      y 968.1350063210602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "viral_space_accessory"
      name "viral_space_accessory"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph19"
      uniprot "NA"
    ]
    graphics [
      x 829.3601412609789
      y 976.9999199092995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_accessory"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "structural_space_proteins"
      name "structural_space_proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph18"
      uniprot "NA"
    ]
    graphics [
      x 923.9232457461453
      y 918.1433572098304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "structural_space_proteins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph85"
      uniprot "NA"
    ]
    graphics [
      x 191.29557196826602
      y 471.27416043326286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph87"
      uniprot "NA"
    ]
    graphics [
      x 335.6974470146813
      y 560.3701774919618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph78"
      uniprot "NA"
    ]
    graphics [
      x 340.8045132648604
      y 643.5282874114332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph83"
      uniprot "NA"
    ]
    graphics [
      x 108.90386196167708
      y 472.1096075720027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_34"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph69"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 357.07886338289075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "pp1a"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph63; glyph46"
      uniprot "NA"
    ]
    graphics [
      x 222.3638694112493
      y 331.63975553405726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pp1a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_37"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph72"
      uniprot "NA"
    ]
    graphics [
      x 298.9172867477944
      y 732.1425168356293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon 1 pathway"
      full_annotation "NA; urn:miriam:pubmed:24622840;urn:miriam:ncbiprotein:YP_009724389"
      hgnc "NA"
      map_id "pp1ab"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph40; glyph53; sa184"
      uniprot "NA"
    ]
    graphics [
      x 502.54940435847703
      y 613.3973531491686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pp1ab"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph79"
      uniprot "NA"
    ]
    graphics [
      x 730.7293523529895
      y 617.3687669888166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q94648377; urn:miriam:refseq:QII57165.1; NA"
      hgnc "NA"
      map_id "nsp13"
      name "nsp13"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "c38dc; a8ee9; f1b6a; glyph34"
      uniprot "NA"
    ]
    graphics [
      x 839.3961360566441
      y 528.6374286064952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4868; C19DMap:SARS-CoV-2 RTC and transcription; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725311; NA"
      hgnc "NA"
      map_id "nsp16"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4; abf4b; c8bd9; glyph36; sa81"
      uniprot "NA"
    ]
    graphics [
      x 868.5610713716388
      y 595.8615740424394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4861; WP4868; WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725310; NA"
      hgnc "NA"
      map_id "nsp15"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205; fa46a; beaa5; c625e; glyph52"
      uniprot "NA"
    ]
    graphics [
      x 845.7184449190124
      y 668.0255287600523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_35"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph70"
      uniprot "NA"
    ]
    graphics [
      x 279.50186457635186
      y 431.72250023175627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_38"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph73"
      uniprot "NA"
    ]
    graphics [
      x 307.47545091351117
      y 476.9312957175938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph76"
      uniprot "NA"
    ]
    graphics [
      x 635.2745301654692
      y 348.8982823875439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp8_space_(I)"
      name "nsp8_space_(I)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph49"
      uniprot "NA"
    ]
    graphics [
      x 621.9090487351513
      y 204.566540269962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp8_space_(I)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp8_space_(II)"
      name "nsp8_space_(II)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph50"
      uniprot "NA"
    ]
    graphics [
      x 523.9615118501813
      y 291.10297389846187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp8_space_(II)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038963; NA"
      hgnc "NA"
      map_id "nsp7"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "db214; e64da; glyph33"
      uniprot "NA"
    ]
    graphics [
      x 572.7244058303631
      y 245.1088533700098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      name "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph16"
      uniprot "NA"
    ]
    graphics [
      x 508.3884324829417
      y 778.9249922705926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4846; WP5027; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882; urn:miriam:wikidata:Q90038952; urn:miriam:ncbiprotein:YP_009725297; NA"
      hgnc "NA"
      map_id "nsp1"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606; f9094; e6110; d2ec8; da173; glyph39"
      uniprot "NA"
    ]
    graphics [
      x 431.77526996914014
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q89006922; NA"
      hgnc "NA"
      map_id "nsp2"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6991; glyph37"
      uniprot "NA"
    ]
    graphics [
      x 365.8497889024846
      y 99.0845063159984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp3"
      name "nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed5ac; glyph45"
      uniprot "NA"
    ]
    graphics [
      x 320.3353568314719
      y 157.90017473985642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "nsp11"
      name "nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph66"
      uniprot "NA"
    ]
    graphics [
      x 528.8625415409099
      y 162.98189081208022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4846; WP5038; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q88656943; urn:miriam:ncbiprotein:YP_009725302; NA"
      hgnc "NA"
      map_id "nsp6"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7ad1; c2256; f6f18; glyph56"
      uniprot "NA"
    ]
    graphics [
      x 514.9099941333725
      y 94.50637847638063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917582; NA"
      hgnc "NA"
      map_id "nsp5"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5d9f; glyph67"
      uniprot "NA"
    ]
    graphics [
      x 447.3082584381128
      y 121.98668984992764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4846; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038956; NA"
      hgnc "NA"
      map_id "nsp4"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e019b; e5589; glyph41"
      uniprot "NA"
    ]
    graphics [
      x 347.6842990771373
      y 273.6391320696614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 48
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp10"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 27
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "pp1a"
      target_id "M15_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 2
    target 37
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp8_space_(I)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 2
    target 38
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp8_space_(II)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 2
    target 39
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 2
    target 9
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 2
    target 41
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 2
    target 42
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 2
    target 43
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 2
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 2
    target 45
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 2
    target 46
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 2
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "nsp4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp14"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp12"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_50"
      target_id "CoV_space_poly_minus__br_merase_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 30
    target 4
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 30
    target 5
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "CoV_space_poly_minus__br_merase_space_complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "CoV_space_poly_minus__br_merase_space_complex"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 12
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 7
    target 10
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 7
    target 9
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "nsp9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 7
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 7
    target 40
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp9"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 10
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "complex"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 11
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 8
    target 12
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_42"
      target_id "Replication_space_transcription_space_complex_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 36
    target 10
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_41"
      target_id "complex"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 11
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 11
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 11
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 11
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 12
    target 14
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "Replication_space_transcription_space_complex_space_"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 16
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_39"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_47"
      target_id "(_plus_)sgRNA_space_(2_minus_9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_36"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "(_plus_)sgRNA_space_(2_minus_9)"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_40"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "viral_space_accessory"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 18
    target 21
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "structural_space_proteins"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 35
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_38"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 22
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_51"
      target_id "pp1a"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 34
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_35"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 23
    target 29
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_53"
      target_id "pp1ab"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 28
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_37"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 24
    target 29
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_43"
      target_id "pp1ab"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_34"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 25
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_49"
      target_id "pp1a"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "pp1ab"
      target_id "M15_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 30
    target 32
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 30
    target 33
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "nsp15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp8_space_(I)"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 38
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp8_space_(II)"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 39
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "nsp7"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
