# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 610.9598031731856
      y 718.1237620395829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_61"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id81035407"
      uniprot "NA"
    ]
    graphics [
      x 628.2592746556077
      y 567.4463500612818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_73"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide06bd151"
      uniprot "NA"
    ]
    graphics [
      x 622.3034702618868
      y 892.5270097957125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000175197"
      hgnc "NA"
      map_id "DDIT3"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "a158c"
      uniprot "NA"
    ]
    graphics [
      x 604.2169143374516
      y 1032.3396795558774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DDIT3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_54"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id60fca41b"
      uniprot "NA"
    ]
    graphics [
      x 556.2237779932193
      y 941.260586167346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5600;urn:miriam:ncbigene:6300;urn:miriam:ensembl:ENSG00000112062;urn:miriam:ncbigene:5603"
      hgnc "NA"
      map_id "f53ff"
      name "f53ff"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f53ff"
      uniprot "NA"
    ]
    graphics [
      x 496.1198886362108
      y 816.6513006218634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f53ff"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_58"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id79ad2aef"
      uniprot "NA"
    ]
    graphics [
      x 672.9197879011027
      y 751.7628987810215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_70"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcac13c17"
      uniprot "NA"
    ]
    graphics [
      x 322.10252501108613
      y 717.2372493293245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id198c85f"
      uniprot "NA"
    ]
    graphics [
      x 416.49711747324005
      y 639.5825020614557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id44f6de9d"
      uniprot "NA"
    ]
    graphics [
      x 430.1731272363935
      y 976.1305869642508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_57"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id6fd5bed2"
      uniprot "NA"
    ]
    graphics [
      x 357.3643499740161
      y 860.9005001444398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id589b86fe"
      uniprot "NA"
    ]
    graphics [
      x 725.5946180038629
      y 1040.4791103797447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS_br_infection"
      name "SARS_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f9996"
      uniprot "NA"
    ]
    graphics [
      x 898.5516739737802
      y 1119.110352371337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_br_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000117676;urn:miriam:ensembl:ENSG00000177189;urn:miriam:ensembl:ENSG00000071242"
      hgnc "NA"
      map_id "dd506"
      name "dd506"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd506"
      uniprot "NA"
    ]
    graphics [
      x 845.4289505867092
      y 1222.6760530585439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dd506"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_77"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ideee7f970"
      uniprot "NA"
    ]
    graphics [
      x 1028.20039946452
      y 1186.1410008227788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc1c0f088"
      uniprot "NA"
    ]
    graphics [
      x 817.9662789848003
      y 1118.3795840342138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id63f652b7"
      uniprot "NA"
    ]
    graphics [
      x 739.0903019273127
      y 1324.827040647657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Protein_br_synthesis"
      name "Protein_br_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5ffe"
      uniprot "NA"
    ]
    graphics [
      x 598.4993866743187
      y 1313.6568219507674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Protein_br_synthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_47"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id392d909b"
      uniprot "NA"
    ]
    graphics [
      x 450.6931878044761
      y 1253.7651825306693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000151247"
      hgnc "NA"
      map_id "EIF4E"
      name "EIF4E"
      node_subtype "GENE"
      node_type "species"
      org_id "ce80a"
      uniprot "NA"
    ]
    graphics [
      x 385.1239139611666
      y 1117.5604846484018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF4E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cell_space_survival_space__br_and_space_proliferation"
      name "Cell_space_survival_space__br_and_space_proliferation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8558"
      uniprot "NA"
    ]
    graphics [
      x 885.1392068333602
      y 1008.2797733435096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cell_space_survival_space__br_and_space_proliferation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_60"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id80dca1f4"
      uniprot "NA"
    ]
    graphics [
      x 940.0331329585522
      y 901.2980981998417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000115966;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "b90b1"
      name "b90b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b90b1"
      uniprot "NA"
    ]
    graphics [
      x 1041.8473794249705
      y 908.2116947468176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b90b1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_64"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2a9cdb0"
      uniprot "NA"
    ]
    graphics [
      x 1164.1628512484565
      y 1013.351579699511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_48"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3d6c0762"
      uniprot "NA"
    ]
    graphics [
      x 840.4016598568419
      y 859.2333866869794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "Innate_br_immunity"
      name "Innate_br_immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc6a0"
      uniprot "NA"
    ]
    graphics [
      x 627.2421391865374
      y 830.9498464987952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Innate_br_immunity"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id559f5826"
      uniprot "NA"
    ]
    graphics [
      x 467.898883719965
      y 899.7422378388183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000170345;urn:miriam:ensembl:ENSG00000177606;urn:miriam:ncbigene:3726"
      hgnc "NA"
      map_id "d382f"
      name "d382f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d382f"
      uniprot "NA"
    ]
    graphics [
      x 337.1910689058277
      y 991.1955897929072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d382f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_44"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2520366d"
      uniprot "NA"
    ]
    graphics [
      x 522.9128292342943
      y 663.8573884311588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide8866e40"
      uniprot "NA"
    ]
    graphics [
      x 314.36213848522453
      y 1175.431858804758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad2dd"
      uniprot "NA"
    ]
    graphics [
      x 180.45213122308968
      y 1031.051221209265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ea117"
      uniprot "NA"
    ]
    graphics [
      x 454.2336823701216
      y 1114.8446597860714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1e34"
      uniprot "NA"
    ]
    graphics [
      x 202.58597440895392
      y 925.8884166500593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc72f872e"
      uniprot "NA"
    ]
    graphics [
      x 231.34478061137327
      y 1125.4119753732675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "IFI27"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "f93d3; c5ac8"
      uniprot "NA"
    ]
    graphics [
      x 187.41817332462114
      y 1249.0336480066208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFI27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "BST2"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "a24ed; b17a4"
      uniprot "NA"
    ]
    graphics [
      x 89.2223701492336
      y 888.8479038845583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BST2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "IFITM2"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acf10; f247c"
      uniprot "NA"
    ]
    graphics [
      x 516.1798391623493
      y 1233.9906316248062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "IFITM3"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f50e2; f1794"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1071.0129608022698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4877; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185885; urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "IFITM1"
      name "IFITM1"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "cf41c; b8cbc; a93c7; e997b"
      uniprot "NA"
    ]
    graphics [
      x 342.6509521854961
      y 1311.6700067022791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFITM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5602"
      hgnc "NA"
      map_id "ca580"
      name "ca580"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca580"
      uniprot "NA"
    ]
    graphics [
      x 736.264158173776
      y 361.3568827034685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ca580"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_46"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id352f2389"
      uniprot "NA"
    ]
    graphics [
      x 909.0867721040046
      y 258.8775118433854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_65"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idac31cf2f"
      uniprot "NA"
    ]
    graphics [
      x 872.4790714997137
      y 338.5260536871271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_72"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide012a8a5"
      uniprot "NA"
    ]
    graphics [
      x 786.4126831251426
      y 500.90924255288047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_51"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54d4a9a9"
      uniprot "NA"
    ]
    graphics [
      x 763.1718339464522
      y 195.09953871466791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide37bb3b6"
      uniprot "NA"
    ]
    graphics [
      x 635.4703988449079
      y 309.18438692599966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861; WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "BCL2"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "eef8e; e21a3"
      uniprot "NA"
    ]
    graphics [
      x 694.2777726747271
      y 432.66751928878796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BCL2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_50"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4b1d8b89"
      uniprot "NA"
    ]
    graphics [
      x 863.7095294960087
      y 451.1895308391017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4877; WP5038; C19DMap:JNK pathway"
      full_annotation "NA; urn:miriam:wikipathways:WP4936; urn:miriam:obo.go:GO%3A0006914"
      hgnc "NA"
      map_id "Autophagy"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3; d0d85; sa18"
      uniprot "NA"
    ]
    graphics [
      x 986.1843953649936
      y 498.6998645756764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_229E_br_IBV_space_infection"
      name "SARS,_space_229E_br_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e046c"
      uniprot "NA"
    ]
    graphics [
      x 833.1848877274975
      y 71.59941061126062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_229E_br_IBV_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided0c8de7"
      uniprot "NA"
    ]
    graphics [
      x 960.8409407060898
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000076984"
      hgnc "NA"
      map_id "MAP2K7"
      name "MAP2K7"
      node_subtype "GENE"
      node_type "species"
      org_id "ac39a"
      uniprot "NA"
    ]
    graphics [
      x 1046.5025364325606
      y 186.6872516714019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAP2K7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_45"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id264ad72f"
      uniprot "NA"
    ]
    graphics [
      x 1152.1612245875872
      y 99.6771323900955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000173327;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ensembl:ENSG00000006432"
      hgnc "NA"
      map_id "b9bb1"
      name "b9bb1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9bb1"
      uniprot "NA"
    ]
    graphics [
      x 1042.4004079428591
      y 117.25490653378029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b9bb1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000065559"
      hgnc "NA"
      map_id "MAP2K4"
      name "MAP2K4"
      node_subtype "GENE"
      node_type "species"
      org_id "bed55"
      uniprot "NA"
    ]
    graphics [
      x 779.2339015081145
      y 654.7686000775986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAP2K4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_63"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9808cc66"
      uniprot "NA"
    ]
    graphics [
      x 865.6147473798401
      y 603.5036406970457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000085511;urn:miriam:ensembl:ENSG00000095015"
      hgnc "NA"
      map_id "c8921"
      name "c8921"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c8921"
      uniprot "NA"
    ]
    graphics [
      x 715.8687009319934
      y 557.2291579033918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c8921"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_71"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idda26811d"
      uniprot "NA"
    ]
    graphics [
      x 545.4700643430799
      y 485.1930885091576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000034152;urn:miriam:ensembl:ENSG00000108984"
      hgnc "NA"
      map_id "e6b7b"
      name "e6b7b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e6b7b"
      uniprot "NA"
    ]
    graphics [
      x 389.57513994726736
      y 477.72746410939607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e6b7b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_43"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id1b1215f4"
      uniprot "NA"
    ]
    graphics [
      x 269.80992446469116
      y 390.13006420414973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_MERS,_space__br_229E_space_infection"
      name "SARS,_space_MERS,_space__br_229E_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5c80"
      uniprot "NA"
    ]
    graphics [
      x 138.75419359254056
      y 382.75978392008216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_MERS,_space__br_229E_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfb2829ab"
      uniprot "NA"
    ]
    graphics [
      x 104.05666156488849
      y 505.1004551304208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "UNIPROT:P59635;UNIPROT:P59632"
      name "e11b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e11b1"
      uniprot "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      x 189.63185192485088
      y 618.0711030347096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635;UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:wikidata:Q89458416;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
      name "afd54"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afd54"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    ]
    graphics [
      x 999.8930931889495
      y 370.63148151551377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5594"
      hgnc "NA"
      map_id "f9084"
      name "f9084"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9084"
      uniprot "NA"
    ]
    graphics [
      x 1230.740668399313
      y 1153.124785474089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f9084"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id81131143"
      uniprot "NA"
    ]
    graphics [
      x 1207.6535925942308
      y 1266.6150894195102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c30bf1f"
      uniprot "NA"
    ]
    graphics [
      x 1386.4851416540196
      y 1151.4686772543405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_67"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc07c34c7"
      uniprot "NA"
    ]
    graphics [
      x 1333.114152759177
      y 1278.8773066986291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000169032;urn:miriam:ensembl:ENSG00000126934"
      hgnc "NA"
      map_id "e5ca8"
      name "e5ca8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e5ca8"
      uniprot "NA"
    ]
    graphics [
      x 1404.8117772542337
      y 1403.4528252276323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e5ca8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_55"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id62eba7d3"
      uniprot "NA"
    ]
    graphics [
      x 1519.3937023723172
      y 1340.9841178868924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idbeaaca30"
      uniprot "NA"
    ]
    graphics [
      x 1319.9627544053542
      y 1507.9266178197718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000132155"
      hgnc "NA"
      map_id "RAF1"
      name "RAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fd989"
      uniprot "NA"
    ]
    graphics [
      x 1192.434722658672
      y 1518.6164247229308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RAF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_MERS,_space_229E_br_infection"
      name "SARS,_space_MERS,_space_229E_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bf897"
      uniprot "NA"
    ]
    graphics [
      x 1509.0965142366579
      y 1214.5461185834251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_MERS,_space_229E_br_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4877; WP4880; C19DMap:JNK pathway; C19DMap:PAMP signalling; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416; urn:miriam:uniprot:P59633; urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "UNIPROT:P59633"
      name "ee4e9; 3b; Orf3b"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "ee4e9; e6060; sa75; sa356; sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1116.7929753981834
      y 1329.771993841663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59633"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 74
    source 2
    target 1
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_61"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 3
    target 1
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_73"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 46
    target 2
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BCL2"
      target_id "W9_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 4
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "DDIT3"
      target_id "W9_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 5
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_54"
      target_id "DDIT3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 6
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 7
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_58"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 8
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_70"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 9
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_42"
      target_id "f53ff"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 6
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 6
    target 11
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 6
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f53ff"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 54
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K4"
      target_id "W9_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 62
    target 8
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59635;UNIPROT:P59632"
      target_id "W9_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 58
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "e6b7b"
      target_id "W9_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 10
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_49"
      target_id "EIF4E"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 11
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_57"
      target_id "d382f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 13
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_br_infection"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 12
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_53"
      target_id "dd506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 13
    target 15
    cd19dm [
      diagram "WP4877"
      edge_type "INHIBITION"
      source_id "SARS_br_infection"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 15
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_77"
      target_id "dd506"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 14
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "dd506"
      target_id "W9_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 14
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "dd506"
      target_id "W9_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 64
    target 15
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f9084"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 16
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_68"
      target_id "Cell_space_survival_space__br_and_space_proliferation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 17
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_56"
      target_id "Protein_br_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 19
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_47"
      target_id "Protein_br_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 20
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "EIF4E"
      target_id "W9_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 22
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_60"
      target_id "Cell_space_survival_space__br_and_space_proliferation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 23
    target 22
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b90b1"
      target_id "W9_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 24
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_64"
      target_id "b90b1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 23
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b90b1"
      target_id "W9_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 64
    target 24
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "f9084"
      target_id "W9_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 25
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_48"
      target_id "Innate_br_immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 27
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_52"
      target_id "Innate_br_immunity"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 28
    target 27
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "d382f"
      target_id "W9_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 29
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_44"
      target_id "d382f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 28
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 28
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 28
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 28
    target 33
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 28
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d382f"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 40
    target 29
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "ca580"
      target_id "W9_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 39
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM1"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 38
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM3"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 37
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFITM2"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 36
    target 33
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BST2"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 35
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "IFI27"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 41
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_46"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 42
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_65"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 43
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_72"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 44
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_51"
      target_id "ca580"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 40
    target 45
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "ca580"
      target_id "W9_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 51
    target 41
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K7"
      target_id "W9_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 63
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
      target_id "W9_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 54
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "MAP2K4"
      target_id "W9_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 49
    target 44
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_229E_br_IBV_space_infection"
      target_id "W9_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 45
    target 46
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_74"
      target_id "BCL2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 46
    target 47
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "BCL2"
      target_id "W9_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 47
    target 48
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_50"
      target_id "Autophagy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 49
    target 50
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_229E_br_IBV_space_infection"
      target_id "W9_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 50
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_76"
      target_id "MAP2K7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 52
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_45"
      target_id "MAP2K7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 53
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "b9bb1"
      target_id "W9_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 55
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_63"
      target_id "MAP2K4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 56
    target 55
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "c8921"
      target_id "W9_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 56
    target 57
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "c8921"
      target_id "W9_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 57
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_71"
      target_id "e6b7b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 59
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_43"
      target_id "e6b7b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 60
    target 59
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space__br_229E_space_infection"
      target_id "W9_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 60
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space__br_229E_space_infection"
      target_id "W9_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 61
    target 62
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_78"
      target_id "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 65
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_62"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 66
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_59"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 67
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_67"
      target_id "f9084"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 73
    target 65
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59633"
      target_id "W9_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 72
    target 66
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space_229E_br_infection"
      target_id "W9_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 68
    target 67
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "e5ca8"
      target_id "W9_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 69
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_55"
      target_id "e5ca8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 70
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_66"
      target_id "e5ca8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 72
    target 69
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_MERS,_space_229E_br_infection"
      target_id "W9_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 71
    target 70
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "RAF1"
      target_id "W9_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
