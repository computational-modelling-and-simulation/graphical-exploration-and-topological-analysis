# generated with VANTED V2.8.2 at Fri Mar 04 10:06:58 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 924.6059396155778
      y 729.0510715523224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32408336"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re168"
      uniprot "NA"
    ]
    graphics [
      x 1084.5002688051622
      y 655.739514203057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831;PUBMED:19864379;PUBMED:32432918"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_38"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re153"
      uniprot "NA"
    ]
    graphics [
      x 1078.653817425337
      y 1096.4107280455623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 749.2536103017552
      y 457.63143070540207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32432657"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_56"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re189"
      uniprot "NA"
    ]
    graphics [
      x 873.4360313057268
      y 791.0165127465452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_57"
      name "PMID:22536270"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 666.8730969223893
      y 703.4693013192856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:27965422;PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re190"
      uniprot "NA"
    ]
    graphics [
      x 973.0317430408462
      y 948.0146471471517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_46"
      name "PMID:10969042"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 621.5893915248439
      y 762.9166830630929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:24227843;PUBMED:28512108;PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1151.4618234928457
      y 699.4508402709627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_4"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 1221.4608378053786
      y 921.4064451886519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:28512108"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 690.0306128180948
      y 633.3652651515312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:25225202"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 942.1422684200472
      y 815.8802094131831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:26010093;PUBMED:26171856"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 722.4098522514721
      y 689.9244354731309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:19034303;PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_67"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re200"
      uniprot "NA"
    ]
    graphics [
      x 1096.8945018816346
      y 986.2921751330101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D006973"
      hgnc "NA"
      map_id "hypertension"
      name "hypertension"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 1000.5834180612582
      y 1119.1306911608986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hypertension"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003924"
      hgnc "NA"
      map_id "_space_Diabetes_space_mellitus,_space_type_space_II"
      name "_space_Diabetes_space_mellitus,_space_type_space_II"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1226.5317663463093
      y 1077.3340762128698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_space_Diabetes_space_mellitus,_space_type_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re201"
      uniprot "NA"
    ]
    graphics [
      x 788.6019547958675
      y 1127.5283261339484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 7
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P12821; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "NA; HGNC_SYMBOL:ACE"
      map_id "UNIPROT:P12821"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e0c12; sa29; sa146; sa28; sa100; sa199; sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 632.8407368903675
      y 1028.2127196528959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12821"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:2550696"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 670.9668347809418
      y 1224.757811313832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re193"
      uniprot "NA"
    ]
    graphics [
      x 827.1472415103275
      y 1037.9194496216132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 478.0283039412249
      y 767.311995330634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:20066004;PUBMED:32343152;PUBMED:23937567;PUBMED:24803075"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re188"
      uniprot "NA"
    ]
    graphics [
      x 511.97388523049835
      y 1210.7394575870442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re186"
      uniprot "NA"
    ]
    graphics [
      x 626.9960460278357
      y 866.8467100414007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_65"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re197"
      uniprot "NA"
    ]
    graphics [
      x 861.3643090845768
      y 1252.164868464064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 314.22314338376964
      y 944.1629481605364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_40"
      name "PMID:190881"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 554.3561758049523
      y 1009.1618836117921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 396.4361803678444
      y 637.3250211588795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_9"
      name "angiotensin_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 522.7859683978883
      y 491.31327208506974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000196549;urn:miriam:ncbigene:4311;urn:miriam:ncbigene:4311;urn:miriam:ec-code:3.4.24.11;urn:miriam:hgnc.symbol:MME;urn:miriam:hgnc.symbol:MME;urn:miriam:refseq:NM_000902;urn:miriam:uniprot:P08473;urn:miriam:uniprot:P08473;urn:miriam:hgnc:7154"
      hgnc "HGNC_SYMBOL:MME"
      map_id "UNIPROT:P08473"
      name "MME"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39; sa194"
      uniprot "UNIPROT:P08473"
    ]
    graphics [
      x 229.72783077327108
      y 491.8466320901989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08473"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_7"
      name "angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24; sa96"
      uniprot "NA"
    ]
    graphics [
      x 453.15737711399925
      y 522.4500510141633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re123"
      uniprot "NA"
    ]
    graphics [
      x 236.78936838001073
      y 582.0852675031045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 310.0551467611832
      y 682.6786806783393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:24041943"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 419.483891895204
      y 731.6033223160323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:1310484"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 304.46178387258703
      y 811.630317188528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 574.9615002598415
      y 309.17903473822355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 625.7107741243224
      y 243.48244219188382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:15767466"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 401.4502970312203
      y 332.15593710017896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 631.906576936839
      y 406.0758173960985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:29928987"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 799.625659956559
      y 500.87930894620393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 603.8200296545833
      y 576.8512899006854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P04201"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa77; sa141; sa496; sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 777.9735736408267
      y 738.4859028029091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re194"
      uniprot "NA"
    ]
    graphics [
      x 895.4602905764511
      y 966.568453661661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_42"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 888.2240840501195
      y 540.2974471111842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_85"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 988.0539177323046
      y 489.8281389841253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:27660028"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 568.3827585963218
      y 854.7505689367645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_39"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 631.997652952128
      y 661.4861441175234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:29287092"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re127"
      uniprot "NA"
    ]
    graphics [
      x 671.633243060799
      y 918.5932613555045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:12829792"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_28"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 890.9311334963293
      y 449.94002241256516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_9"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 1011.7733926164867
      y 729.1313960108602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 813.5160825900173
      y 956.3162579699317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006979"
      hgnc "NA"
      map_id "oxidative_space_stress"
      name "oxidative_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 975.7782075417916
      y 1032.0642482669857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "oxidative_space_stress"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071;PUBMED:25666589"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_52"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re184"
      uniprot "NA"
    ]
    graphics [
      x 1114.3799947525335
      y 847.9671061300453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 11
      diagram "WP4799; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P30556; urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "NA; HGNC_SYMBOL:AGTR1"
      map_id "UNIPROT:P30556"
      name "AT1R; AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab2a6; sa102; sa167; sa139; sa140; sa26; sa204; sa500; sa484; sa519; sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1145.1847154217116
      y 590.0582361727443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 1395.5550189886283
      y 629.432354364629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770;PUBMED:25124854"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re195"
      uniprot "NA"
    ]
    graphics [
      x 1395.9590568400126
      y 521.0376399374063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:19834109"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re203"
      uniprot "NA"
    ]
    graphics [
      x 1376.4845626873253
      y 726.1925366302647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_10"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 969.8195109976233
      y 764.7093351079507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_49"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 1208.3730807744505
      y 447.9557785246381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:12754187"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_50"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 943.903585335854
      y 536.8316328169594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_51"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1079.7654688040388
      y 356.41449072057674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:1338730"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re198"
      uniprot "NA"
    ]
    graphics [
      x 1221.7566228074588
      y 759.880610746204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:24530803"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 1297.3557287237763
      y 473.2456088916366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:26497614;PUBMED:17138938;PUBMED:32333398;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 835.029921799946
      y 726.0085403319871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:23884911"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_47"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re177"
      uniprot "NA"
    ]
    graphics [
      x 1113.6543988592455
      y 440.8103367702383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D013927"
      hgnc "NA"
      map_id "thrombosis"
      name "thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa172"
      uniprot "NA"
    ]
    graphics [
      x 982.2785335309675
      y 383.8627938119993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "thrombosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:obo.chebi:CHEBI%3A48432; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_II"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb; sa23; sa95; sa194"
      uniprot "NA"
    ]
    graphics [
      x 689.8444374111873
      y 826.2119979781233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80127"
      hgnc "NA"
      map_id "angiotensin_space_IV"
      name "angiotensin_space_IV"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa63"
      uniprot "NA"
    ]
    graphics [
      x 780.4626977818756
      y 845.7551916518502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_IV"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:91691124;urn:miriam:kegg.compound:C20970"
      hgnc "NA"
      map_id "angiotensin_space_A"
      name "angiotensin_space_A"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 583.3368010313279
      y 631.2410701220235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 540.918376891133
      y 803.2024320275754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 560.932410757822
      y 525.1360713853171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4799; C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P50052; urn:miriam:ensembl:ENSG00000180772;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc:338;urn:miriam:refseq:NM_000686;urn:miriam:uniprot:P50052;urn:miriam:uniprot:P50052;urn:miriam:ncbigene:186;urn:miriam:ncbigene:186"
      hgnc "NA; HGNC_SYMBOL:AGTR2"
      map_id "UNIPROT:P50052"
      name "AT2R; AGTR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee6b1; sa79; sa25"
      uniprot "UNIPROT:P50052"
    ]
    graphics [
      x 529.5529536419579
      y 207.76862724153284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50052"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:25014541"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 401.954856541278
      y 139.05117846319456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:30048754"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 500.8477279241162
      y 320.36610340901933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:24463937"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re165"
      uniprot "NA"
    ]
    graphics [
      x 567.3854905185198
      y 359.88851090339097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 759.2271433364928
      y 165.9602428932103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014661"
      hgnc "NA"
      map_id "vasoconstriction"
      name "vasoconstriction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa56"
      uniprot "NA"
    ]
    graphics [
      x 970.4869881355819
      y 236.83395344342023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vasoconstriction"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_31"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re146"
      uniprot "NA"
    ]
    graphics [
      x 944.259625494646
      y 69.60938445446357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:9493859"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_30"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 1161.2158527230672
      y 394.2885000909265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ec-code:3.4.11.3;urn:miriam:refseq:NM_005575;urn:miriam:ensembl:ENSG00000113441;urn:miriam:hgnc:6656;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:ncbigene:4012;urn:miriam:uniprot:Q9UIQ6;urn:miriam:uniprot:Q9UIQ6;urn:miriam:ncbigene:4012"
      hgnc "HGNC_SYMBOL:LNPEP"
      map_id "UNIPROT:Q9UIQ6"
      name "LNPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa156; sa152"
      uniprot "UNIPROT:Q9UIQ6"
    ]
    graphics [
      x 1261.1237316717138
      y 579.5437892638489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UIQ6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:10234025;PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_64"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re196"
      uniprot "NA"
    ]
    graphics [
      x 1473.1025557535122
      y 557.4904353982206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re151"
      uniprot "NA"
    ]
    graphics [
      x 1062.8467942831335
      y 481.695330964361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:11707427"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re122"
      uniprot "NA"
    ]
    graphics [
      x 1068.0370828570076
      y 737.505667768589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_53"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re185"
      uniprot "NA"
    ]
    graphics [
      x 1394.4592606361502
      y 811.3628141812858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:27038740"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 1300.1108480090124
      y 398.7417095835779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006954"
      hgnc "NA"
      map_id "inflammatory_space_response"
      name "inflammatory_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa61"
      uniprot "NA"
    ]
    graphics [
      x 1153.4569270632846
      y 286.9497828945765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "inflammatory_space_response"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_84"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1023.4683021861492
      y 123.11265349694463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_198923;urn:miriam:ncbigene:116512;urn:miriam:ncbigene:116512;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:uniprot:Q8TDS7;urn:miriam:uniprot:Q8TDS7;urn:miriam:hgnc:29626;urn:miriam:ensembl:ENSG00000172938"
      hgnc "HGNC_SYMBOL:MRGPRD"
      map_id "UNIPROT:Q8TDS7"
      name "MRGPRD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94; sa50"
      uniprot "UNIPROT:Q8TDS7"
    ]
    graphics [
      x 823.4695830238397
      y 67.71238278077487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDS7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 705.7895891833625
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:44192273"
      hgnc "NA"
      map_id "alamandine"
      name "alamandine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 708.1815957127925
      y 235.65319366271012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "alamandine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D009410"
      hgnc "NA"
      map_id "neurodegeneration"
      name "neurodegeneration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa190"
      uniprot "NA"
    ]
    graphics [
      x 1375.4703537201003
      y 998.487088567071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "neurodegeneration"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:30934934"
      hgnc "NA"
      map_id "angiotensin_space_3_minus_7"
      name "angiotensin_space_3_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 828.6417095597884
      y 408.51057929050995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_3_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re149"
      uniprot "NA"
    ]
    graphics [
      x 831.0825546062722
      y 619.6825279290055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re148"
      uniprot "NA"
    ]
    graphics [
      x 752.9375291653104
      y 592.5404675953129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003071"
      hgnc "NA"
      map_id "cognition"
      name "cognition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 1561.7850942365885
      y 486.1249364487432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cognition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D005355"
      hgnc "NA"
      map_id "fibrosis"
      name "fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 690.9575021800135
      y 471.503409345747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fibrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A147302"
      hgnc "NA"
      map_id "CGP42112A"
      name "CGP42112A"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 449.39650925404794
      y 243.3485890886077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CGP42112A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 523.8633299118021
      y 1087.4494970130288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A89666"
      hgnc "NA"
      map_id "angiotensin_space_III"
      name "angiotensin_space_III"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa62"
      uniprot "NA"
    ]
    graphics [
      x 361.2237234749563
      y 1119.8615900611514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_III"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:290;urn:miriam:ncbigene:290;urn:miriam:hgnc:500;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:refseq:NM_001150;urn:miriam:uniprot:P15144;urn:miriam:uniprot:P15144;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:ensembl:ENSG00000166825;urn:miriam:ec-code:3.4.11.2"
      hgnc "HGNC_SYMBOL:ANPEP"
      map_id "UNIPROT:P15144"
      name "ANPEP_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P15144"
    ]
    graphics [
      x 423.67366597262424
      y 1195.1494177472025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 379.5596639502329
      y 980.6627483813293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000138792;urn:miriam:ncbigene:2028;urn:miriam:ncbigene:2028;urn:miriam:hgnc:3355;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:refseq:NM_001379611;urn:miriam:ec-code:3.4.11.7;urn:miriam:uniprot:Q07075;urn:miriam:uniprot:Q07075"
      hgnc "HGNC_SYMBOL:ENPEP"
      map_id "UNIPROT:Q07075"
      name "ENPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162; sa64"
      uniprot "UNIPROT:Q07075"
    ]
    graphics [
      x 174.96609235494066
      y 938.2898228651575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q07075"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re191"
      uniprot "NA"
    ]
    graphics [
      x 244.68343165260535
      y 761.1055213689278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:22710644"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re128"
      uniprot "NA"
    ]
    graphics [
      x 100.37870559292026
      y 1061.4308417556686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:24337978"
      hgnc "NA"
      map_id "QGC001"
      name "QGC001"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 238.3690784287578
      y 1094.3666807436748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "QGC001"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D008297"
      hgnc "NA"
      map_id "sex,_space_male"
      name "sex,_space_male"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa193; sa44"
      uniprot "NA"
    ]
    graphics [
      x 468.58688143852606
      y 659.5269322557436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "sex,_space_male"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 375.820356852558
      y 462.94633068389453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097;PUBMED:19375596"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 904.9755398650533
      y 1046.0043689478755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:6555043"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 600.4729419581959
      y 1083.4226774850076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:22180785"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 356.21114847787373
      y 764.4305417079574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 851.8150322450767
      y 676.1863199493075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:2266130"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re131"
      uniprot "NA"
    ]
    graphics [
      x 563.7369448871866
      y 943.2157060308853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_I"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a; sa21; sa195"
      uniprot "NA"
    ]
    graphics [
      x 446.23953330923933
      y 1027.9273870466054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2097;urn:miriam:ncbigene:1215;urn:miriam:ncbigene:1215;urn:miriam:hgnc.symbol:CMA1;urn:miriam:hgnc.symbol:CMA1;urn:miriam:ensembl:ENSG00000092009;urn:miriam:refseq:NM_001836;urn:miriam:uniprot:P23946;urn:miriam:uniprot:P23946;urn:miriam:ec-code:3.4.21.39"
      hgnc "HGNC_SYMBOL:CMA1"
      map_id "UNIPROT:P23946"
      name "CMA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa122"
      uniprot "UNIPROT:P23946"
    ]
    graphics [
      x 451.4258115467398
      y 876.5960425704152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23946"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:6172448"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re129"
      uniprot "NA"
    ]
    graphics [
      x 586.4019480366817
      y 1357.9193083272626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 754.1981245689398
      y 1306.9589751888889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "UNIPROT:P01019"
      name "AGT"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "sa34; sa174; sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 834.1469373272979
      y 1444.4270845453989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01019"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "UNIPROT:P00797"
      name "REN; Prorenin"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "sa36; sa35; sa71; sa415; sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 975.4349182993556
      y 1467.246395430935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00797"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:12122115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_72"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1062.6400271473788
      y 1412.2804039349508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_82"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1166.5374452854871
      y 1485.1026154583515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17823"
      hgnc "NA"
      map_id "Calcitriol"
      name "Calcitriol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa201; sa37"
      uniprot "NA"
    ]
    graphics [
      x 1049.0801170361622
      y 1293.6429118838298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Calcitriol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:8351287"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re167"
      uniprot "NA"
    ]
    graphics [
      x 1111.946824931773
      y 1448.4197939833198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A4903"
      hgnc "NA"
      map_id "ethynylestradiol"
      name "ethynylestradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa134"
      uniprot "NA"
    ]
    graphics [
      x 1283.7375518080908
      y 1407.919057549505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ethynylestradiol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_001909;urn:miriam:ensembl:ENSG00000117984;urn:miriam:ec-code:3.4.23.5;urn:miriam:uniprot:P07339;urn:miriam:uniprot:P07339;urn:miriam:ncbigene:1509;urn:miriam:ncbigene:1509;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc:2529"
      hgnc "HGNC_SYMBOL:CTSD"
      map_id "UNIPROT:P07339"
      name "CTSD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:P07339"
    ]
    graphics [
      x 616.4299324603274
      y 1490.8930529403806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:1511;urn:miriam:uniprot:P08311;urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448; urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "UNIPROT:P08311"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137; sa26; sa381"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 525.7516761451345
      y 1479.0206099606783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_96"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa113"
      uniprot "NA"
    ]
    graphics [
      x 828.9672389549339
      y 557.1707031834318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:C000657245; urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_infection"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa165; sa207; sa499; sa480; sa481"
      uniprot "NA"
    ]
    graphics [
      x 987.357838677153
      y 614.4666545922972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:27465904"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_12"
      name "angiotensin_space_1_minus_12"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa121"
      uniprot "NA"
    ]
    graphics [
      x 154.12952315775
      y 722.7912147092815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 527.1170147711897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:22490446"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_4"
      name "angiotensin_space_1_minus_4"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 119.39491440413678
      y 581.4226970320426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:6357;urn:miriam:ec-code:3.4.21.35;urn:miriam:refseq:NM_002257;urn:miriam:uniprot:P06870;urn:miriam:uniprot:P06870;urn:miriam:ncbigene:3816;urn:miriam:ncbigene:3816;urn:miriam:hgnc.symbol:KLK1;urn:miriam:hgnc.symbol:KLK1;urn:miriam:ensembl:ENSG00000167748"
      hgnc "HGNC_SYMBOL:KLK1"
      map_id "UNIPROT:P06870"
      name "KLK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P06870"
    ]
    graphics [
      x 584.920015273028
      y 1214.025636323154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06870"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_188"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa99"
      uniprot "NA"
    ]
    graphics [
      x 845.5477756294557
      y 1177.9778082531627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38; sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 1181.2065774830255
      y 1032.4175165398792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_35"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re150"
      uniprot "NA"
    ]
    graphics [
      x 1374.9928162973865
      y 897.0161330354081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A135632"
      hgnc "NA"
      map_id "Camostat_space_mesilate"
      name "Camostat_space_mesilate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 1314.0111297790372
      y 804.6618191373357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Camostat_space_mesilate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707; urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393; urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393; urn:miriam:uniprot:O15393; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099; urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "NA; HGNC_SYMBOL:TMPRSS2"
      map_id "UNIPROT:O15393"
      name "TMPRSS2; TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_893; layout_1045; layout_2499; layout_2500; layout_3349; cf321; df9cf; sa40; sa130; sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1355.8580548820614
      y 583.2420397886094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:32275855;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:ACE2;HGNC_SYMBOL:S"
      map_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
      name "ACE2_minus_Spike_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 1436.5520326538638
      y 1038.9211677137966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_37"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 1516.8528088503608
      y 1172.8243784108815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014779"
      hgnc "NA"
      map_id "viral_space_replication_space_cycle"
      name "viral_space_replication_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 1426.202353146221
      y 1268.193250934307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "viral_space_replication_space_cycle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:10485450"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_7"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1329.1526039982746
      y 328.5341763512283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A50113"
      hgnc "NA"
      map_id "androgen"
      name "androgen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 1293.9322716268302
      y 188.50597814809737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "androgen"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A6541"
      hgnc "NA"
      map_id "Losartan"
      name "Losartan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 1410.7828801588382
      y 379.811316588817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Losartan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_140"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 1324.0931540021484
      y 674.8318593362533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "aldosterone"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195; sa509; sa503"
      uniprot "NA"
    ]
    graphics [
      x 1054.5226951420711
      y 933.7610262607315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aldosterone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:15809376;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P30556;UNIPROT:P04201"
      name "MAS1:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P30556;UNIPROT:P04201"
    ]
    graphics [
      x 1015.5099793406833
      y 867.1830207074031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556;UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070527; urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_aggregation"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa209; sa409"
      uniprot "NA"
    ]
    graphics [
      x 1506.9920059740757
      y 800.9654998877797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_aggregation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D011654"
      hgnc "NA"
      map_id "pulmonary_space_edema"
      name "pulmonary_space_edema"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa208"
      uniprot "NA"
    ]
    graphics [
      x 1532.7600228906974
      y 666.651173556791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "pulmonary_space_edema"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:146025955"
      hgnc "NA"
      map_id "AR234960"
      name "AR234960"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 501.5564189495455
      y 949.412782242587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AR234960"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80129"
      hgnc "NA"
      map_id "angiotensin_space_1_minus_5"
      name "angiotensin_space_1_minus_5"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 392.6852025346027
      y 885.3169851586855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_1_minus_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_132"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 929.7709436449825
      y 1121.0457678465596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:9358;urn:miriam:hgnc.symbol:PREP;urn:miriam:hgnc.symbol:PREP;urn:miriam:ncbigene:5550;urn:miriam:ncbigene:5550;urn:miriam:refseq:NM_002726;urn:miriam:ensembl:ENSG00000085377;urn:miriam:uniprot:P48147;urn:miriam:uniprot:P48147;urn:miriam:ec-code:3.4.21.26"
      hgnc "HGNC_SYMBOL:PREP"
      map_id "UNIPROT:P48147"
      name "PREP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "UNIPROT:P48147"
    ]
    graphics [
      x 391.2520609755011
      y 809.4083309586313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:7064;urn:miriam:ncbigene:7064;urn:miriam:hgnc.symbol:THOP1;urn:miriam:ensembl:ENSG00000172009;urn:miriam:hgnc.symbol:THOP1;urn:miriam:uniprot:P52888;urn:miriam:uniprot:P52888;urn:miriam:hgnc:11793;urn:miriam:ec-code:3.4.24.15;urn:miriam:refseq:NM_003249"
      hgnc "HGNC_SYMBOL:THOP1"
      map_id "UNIPROT:P52888"
      name "THOP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:P52888"
    ]
    graphics [
      x 543.2160182606494
      y 755.1153848810047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P52888"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16469"
      hgnc "NA"
      map_id "estradiol"
      name "estradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa169; sa200"
      uniprot "NA"
    ]
    graphics [
      x 1004.4166666497603
      y 1237.2719249279912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "estradiol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0007568"
      hgnc "NA"
      map_id "aging"
      name "aging"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 810.5291449897561
      y 900.9405485375937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aging"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D000017"
      hgnc "NA"
      map_id "ABO_space_blood_space_group_space_system"
      name "ABO_space_blood_space_group_space_system"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 452.8861146300626
      y 1335.4102510238897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ABO_space_blood_space_group_space_system"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43755"
      hgnc "NA"
      map_id "Lisinopril"
      name "Lisinopril"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 721.1980701075468
      y 1364.7902259324605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lisinopril"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:25225202;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P30556;UNIPROT:Q9BYF1"
      name "ACE2:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P30556;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 918.7628084085544
      y 907.4681220013637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729; urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "UNIPROT:P78536"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43; path_1_sa174; path_1_sa171"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 1284.0592155316663
      y 720.1410816755167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78536"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000137509;urn:miriam:hgnc.symbol:PRCP;urn:miriam:hgnc.symbol:PRCP;urn:miriam:ec-code:3.4.16.2;urn:miriam:uniprot:P42785;urn:miriam:uniprot:P42785;urn:miriam:hgnc:9344;urn:miriam:ncbigene:5547;urn:miriam:refseq:NM_005040;urn:miriam:ncbigene:5547"
      hgnc "HGNC_SYMBOL:PRCP"
      map_id "UNIPROT:P42785"
      name "PRCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa47"
      uniprot "UNIPROT:P42785"
    ]
    graphics [
      x 673.016100195839
      y 562.5117646312045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42785"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_126"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 1166.5054361931886
      y 777.773873700678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 160
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_45"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 1
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 1
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 1
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 1
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 1
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 1
    target 10
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 1
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 1
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 1
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 1
    target 14
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 159
    target 2
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_126"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 126
    target 2
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 152
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "estradiol"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 132
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 120
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Calcitriol"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 68
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_A"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 4
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_78"
      target_id "alamandine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 153
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aging"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 66
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 158
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P42785"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 6
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_57"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 153
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aging"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 15
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "hypertension"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 112
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 8
    target 28
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_46"
      target_id "angiotensin_space_1_minus_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 157
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P78536"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 53
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P30556"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 132
    target 10
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 10
    target 136
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_4"
      target_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 66
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 11
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_89"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 53
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 66
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_II"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 12
    target 156
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_93"
      target_id "UNIPROT:P30556;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 105
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "sex,_space_male"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "hypertension"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "_space_Diabetes_space_mellitus,_space_type_space_II"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "hypertension"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 18
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 18
    target 22
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 18
    target 23
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 18
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12821"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 18
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 18
    target 26
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 18
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 155
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Lisinopril"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 143
    target 20
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aldosterone"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 30
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 21
    target 148
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_71"
      target_id "angiotensin_space_1_minus_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 154
    target 22
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "ABO_space_blood_space_group_space_system"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 153
    target 23
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "aging"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 105
    target 23
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "sex,_space_male"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 152
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "estradiol"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 120
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Calcitriol"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 127
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 25
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_12"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 112
    target 26
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 26
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_40"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_9"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 29
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 27
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_48"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 28
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_9"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 29
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08473"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 29
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 29
    target 31
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 29
    target 32
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08473"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_14"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_74"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 33
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_20"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 34
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_21"
      target_id "angiotensin_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 30
    target 35
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 30
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 30
    target 37
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 30
    target 38
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 30
    target 39
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 30
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_7"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 127
    target 31
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 112
    target 32
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 112
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 151
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P52888"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 112
    target 34
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 150
    target 34
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48147"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 35
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_76"
      target_id "alamandine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 87
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 71
    target 37
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 38
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_32"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 53
    target 39
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_62"
      target_id "UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 41
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 41
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 41
    target 46
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 41
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 41
    target 48
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 41
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 41
    target 50
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M12_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 149
    target 42
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_132"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 143
    target 42
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "aldosterone"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 43
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_42"
      target_id "thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 44
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_85"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 148
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_1_minus_5"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 46
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_39"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 147
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "AR234960"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 48
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_28"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 53
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 49
    target 144
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_9"
      target_id "UNIPROT:P30556;UNIPROT:P04201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_41"
      target_id "oxidative_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_52"
      target_id "oxidative_space_stress"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 53
    target 56
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 53
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 53
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 53
    target 59
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 53
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 53
    target 61
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P30556"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 53
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 53
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 53
    target 64
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M12_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 54
    target 146
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_69"
      target_id "pulmonary_space_edema"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 55
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_63"
      target_id "cognition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 56
    target 145
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_70"
      target_id "platelet_space_aggregation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 144
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P30556;UNIPROT:P04201"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 66
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 58
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_49"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 59
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_50"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 60
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_51"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 142
    target 61
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_140"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 61
    target 143
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_66"
      target_id "aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 141
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Losartan"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 66
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 67
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_IV"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 68
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_A"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_47"
      target_id "thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 107
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_91"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 108
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_19"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 109
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_6"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 110
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_5"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 111
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_18"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 66
    target 100
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 66
    target 70
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 66
    target 69
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 66
    target 93
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M12_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 97
    target 67
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_80"
      target_id "angiotensin_space_IV"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 67
    target 82
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_IV"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 67
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_IV"
      target_id "M12_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_77"
      target_id "angiotensin_space_A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_A"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 71
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 71
    target 74
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 71
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50052"
      target_id "M12_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 96
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "CGP42112A"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 74
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_43"
      target_id "fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_86"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 77
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_31"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 78
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_30"
      target_id "vasoconstriction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 87
    target 77
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 79
    target 81
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 79
    target 83
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 79
    target 84
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UIQ6"
      target_id "M12_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 80
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_64"
      target_id "cognition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 91
    target 81
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_3_minus_7"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 83
    target 90
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_53"
      target_id "neurodegeneration"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_29"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_84"
      target_id "inflammatory_space_response"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDS7"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "alamandine"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_34"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 93
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_33"
      target_id "angiotensin_space_3_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_III"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 99
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15144"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 100
    target 98
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_79"
      target_id "angiotensin_space_III"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q07075"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07075"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q07075"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 105
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "sex,_space_male"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "QGC001"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "sex,_space_male"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 131
    target 107
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_188"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 132
    target 107
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 112
    target 108
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 130
    target 108
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P06870"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 127
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 113
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23946"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 125
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_96"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 126
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 113
    target 111
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23946"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 114
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_17"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 115
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_73"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 116
    target 114
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 123
    target 114
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07339"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 124
    target 114
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P08311"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00797"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 116
    target 121
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M12_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 120
    target 118
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Calcitriol"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 122
    target 121
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "ethynylestradiol"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_1_minus_12"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 128
    target 129
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_8"
      target_id "angiotensin_space_1_minus_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "Camostat_space_mesilate"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 135
    target 133
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O15393"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 133
    target 136
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_35"
      target_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 135
    target 139
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O15393"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M12_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_37"
      target_id "viral_space_replication_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "androgen"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
