# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4868; WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "STAT1"
      name "STAT1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9489; f2e43"
      uniprot "NA"
    ]
    graphics [
      x 1193.6370025410258
      y 533.603333173493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "STAT1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_106"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfc1498e4"
      uniprot "NA"
    ]
    graphics [
      x 1274.7640742843257
      y 458.6761355642044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4880; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59634; urn:miriam:uniprot:P59634;urn:miriam:ncbigene:1489673"
      hgnc "NA"
      map_id "UNIPROT:P59634"
      name "6; Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ac5ae; sa75"
      uniprot "UNIPROT:P59634"
    ]
    graphics [
      x 1320.9780654132078
      y 364.4593772545005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59634"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 2
    target 1
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_106"
      target_id "STAT1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 3
    target 2
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59634"
      target_id "W17_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
