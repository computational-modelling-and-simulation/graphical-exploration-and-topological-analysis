# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1101.9202754192297
      y 409.51193859889463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4883; WP4969; WP4912; WP5039; WP4853"
      full_annotation "urn:miriam:ncbigene:59272; urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272; urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "ACE2"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "bf1a9; aa820; dc981; c2d8e; a9be1; f1b6b; f3245"
      uniprot "NA"
    ]
    graphics [
      x 1136.782967644917
      y 165.10633102987794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP4723"
      hgnc "NA"
      map_id "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
      name "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ccb5e"
      uniprot "NA"
    ]
    graphics [
      x 1320.4683755136207
      y 500.13298466810517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id66f48e91"
      uniprot "NA"
    ]
    graphics [
      x 1417.5304064243355
      y 378.3558083313941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030818"
      hgnc "NA"
      map_id "Omega_minus_3"
      name "Omega_minus_3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f8b43"
      uniprot "NA"
    ]
    graphics [
      x 1451.535733301746
      y 253.52491344375457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Omega_minus_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:33170317"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5b9fb57"
      uniprot "NA"
    ]
    graphics [
      x 1102.9777247887528
      y 280.0721007024415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050322"
      hgnc "NA"
      map_id "CoA(18:3(6Z,9Z,12Z))"
      name "CoA(18:3(6Z,9Z,12Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cee96"
      uniprot "NA"
    ]
    graphics [
      x 419.9631064120941
      y 703.400217195852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA(18:3(6Z,9Z,12Z))"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd433"
      uniprot "NA"
    ]
    graphics [
      x 278.2127327587234
      y 691.1098442757552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000119673"
      hgnc "NA"
      map_id "ACOT2"
      name "ACOT2"
      node_subtype "GENE"
      node_type "species"
      org_id "ba85d"
      uniprot "NA"
    ]
    graphics [
      x 198.42504630087365
      y 602.5735061126733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACOT2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030141"
      hgnc "NA"
      map_id "gamma_minus_linolenic_space_acid"
      name "gamma_minus_linolenic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cf458"
      uniprot "NA"
    ]
    graphics [
      x 157.7721705784288
      y 694.399617244487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "gamma_minus_linolenic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01010001"
      hgnc "NA"
      map_id "palmitic_space_acid"
      name "palmitic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b2e17"
      uniprot "NA"
    ]
    graphics [
      x 809.0189978878906
      y 465.59129123936276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "palmitic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:33664446"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_54"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idbc38d6ef"
      uniprot "NA"
    ]
    graphics [
      x 829.8825778047022
      y 613.446139826342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cytokine_space_Storm"
      name "Cytokine_space_Storm"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4a45"
      uniprot "NA"
    ]
    graphics [
      x 764.8605283986501
      y 752.5172631533638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytokine_space_Storm"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050278"
      hgnc "NA"
      map_id "CoA(20:3(8Z,11Z,14Z))"
      name "CoA(20:3(8Z,11Z,14Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f1c02"
      uniprot "NA"
    ]
    graphics [
      x 713.3348288953264
      y 986.6558421814234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA(20:3(8Z,11Z,14Z))"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_1"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a616d"
      uniprot "NA"
    ]
    graphics [
      x 900.6252115991232
      y 975.1685182556565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000149485"
      hgnc "NA"
      map_id "FADS1"
      name "FADS1"
      node_subtype "GENE"
      node_type "species"
      org_id "bd87a"
      uniprot "NA"
    ]
    graphics [
      x 884.6657305898696
      y 1086.4275161408905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FADS1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050288"
      hgnc "NA"
      map_id "Arachidonoyl_minus_CoA"
      name "Arachidonoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a74ef"
      uniprot "NA"
    ]
    graphics [
      x 1056.4556288066815
      y 874.3100208909542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Arachidonoyl_minus_CoA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "stearic_space_acid"
      name "stearic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c3274"
      uniprot "NA"
    ]
    graphics [
      x 717.3112434721444
      y 484.032103548044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "stearic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:26271607"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb6a5e755"
      uniprot "NA"
    ]
    graphics [
      x 736.7494569887522
      y 621.1297625211176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030002"
      hgnc "NA"
      map_id "oleic_space_acid_br_"
      name "oleic_space_acid_br_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ecc95"
      uniprot "NA"
    ]
    graphics [
      x 627.16837311945
      y 485.4499462147455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "oleic_space_acid_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:34281182"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35e835c1"
      uniprot "NA"
    ]
    graphics [
      x 649.8067024662274
      y 638.6165864248287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "mitogen_minus_activated_space_protein_space_kinase"
      name "mitogen_minus_activated_space_protein_space_kinase"
      node_subtype "GENE"
      node_type "species"
      org_id "b0174"
      uniprot "NA"
    ]
    graphics [
      x 954.7758745619103
      y 285.38376889087004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "mitogen_minus_activated_space_protein_space_kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:29167338"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id488708d6"
      uniprot "NA"
    ]
    graphics [
      x 875.7483396131836
      y 195.9279268094806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:brenda:3.1.1.4"
      hgnc "NA"
      map_id "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
      name "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
      node_subtype "GENE"
      node_type "species"
      org_id "b7b4c"
      uniprot "NA"
    ]
    graphics [
      x 778.0007708945348
      y 250.77663935137915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMPG01050137"
      hgnc "NA"
      map_id "Omega_minus_6"
      name "Omega_minus_6"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b0824"
      uniprot "NA"
    ]
    graphics [
      x 1268.579457872234
      y 141.81506422915498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Omega_minus_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:33377319;PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id93f06a92"
      uniprot "NA"
    ]
    graphics [
      x 1161.2526912619228
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP5039"
      hgnc "NA"
      map_id "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
      name "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a661b"
      uniprot "NA"
    ]
    graphics [
      x 438.9712556160281
      y 910.2428466743322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:32469225"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbb7f8442"
      uniprot "NA"
    ]
    graphics [
      x 603.3082710751381
      y 823.0563124394203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030120"
      hgnc "NA"
      map_id "linoleic_space_acid"
      name "linoleic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b9202"
      uniprot "NA"
    ]
    graphics [
      x 726.1174877739467
      y 164.73777792944543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "linoleic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:33664446"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_39"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3263c402"
      uniprot "NA"
    ]
    graphics [
      x 936.1669741046509
      y 119.80419791278075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030001"
      hgnc "NA"
      map_id "Arachidonic_space_acid"
      name "Arachidonic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b4efb"
      uniprot "NA"
    ]
    graphics [
      x 1053.2830424727908
      y 768.3153654657106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Arachidonic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idebf627ef"
      uniprot "NA"
    ]
    graphics [
      x 915.0086921520492
      y 745.6460804093471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8ca14613"
      uniprot "NA"
    ]
    graphics [
      x 527.2758754223499
      y 187.66792297919085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:brenda:1.14.19.3"
      hgnc "NA"
      map_id "Linoleoyl_minus_CoA_br_desaturase_br_"
      name "Linoleoyl_minus_CoA_br_desaturase_br_"
      node_subtype "GENE"
      node_type "species"
      org_id "d2e0b"
      uniprot "NA"
    ]
    graphics [
      x 624.996503468797
      y 181.503930553001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Linoleoyl_minus_CoA_br_desaturase_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050343"
      hgnc "NA"
      map_id "CoA(18:2(9Z,12Z))"
      name "CoA(18:2(9Z,12Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b79f3"
      uniprot "NA"
    ]
    graphics [
      x 432.7574899916591
      y 334.63959627542704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CoA(18:2(9Z,12Z))"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:1608291"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id18e899db"
      uniprot "NA"
    ]
    graphics [
      x 904.8720392793814
      y 842.971109008007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP4846"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_and_space__br_COVID_minus_19_space_Pathway_br_Molecular_space_mechanism"
      name "SARS_minus_CoV_minus_2_space_and_space__br_COVID_minus_19_space_Pathway_br_Molecular_space_mechanism"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a93c5"
      uniprot "NA"
    ]
    graphics [
      x 134.65990633105008
      y 855.1874980971304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_and_space__br_COVID_minus_19_space_Pathway_br_Molecular_space_mechanism"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_37"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id1bbbeedc"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 935.434208762237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:pubmed:32130973"
      hgnc "NA"
      map_id "W22_33"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f71d4"
      uniprot "NA"
    ]
    graphics [
      x 166.9900495381026
      y 1014.1652783246761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5cd8db28"
      uniprot "NA"
    ]
    graphics [
      x 1198.4104089656553
      y 300.4581796153037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "Virus_space_in_space_host_space_cell_br_"
      name "Virus_space_in_space_host_space_cell_br_"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b668e"
      uniprot "NA"
    ]
    graphics [
      x 1176.6229445485924
      y 416.80821140212913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Virus_space_in_space_host_space_cell_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37739"
      hgnc "NA"
      map_id "Glycerophospholipids"
      name "Glycerophospholipids"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f34d0"
      uniprot "NA"
    ]
    graphics [
      x 601.2006600743339
      y 315.75126141723547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Glycerophospholipids"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fda8300"
      uniprot "NA"
    ]
    graphics [
      x 710.5988640505136
      y 348.7724294533224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:32469225"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8d80a48f"
      uniprot "NA"
    ]
    graphics [
      x 303.86535362663193
      y 986.7362279166673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_42"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4f2a84fe"
      uniprot "NA"
    ]
    graphics [
      x 1299.861607232774
      y 306.94548935395187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b7a91"
      uniprot "NA"
    ]
    graphics [
      x 1208.220936908141
      y 693.948291675176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aa396"
      uniprot "NA"
    ]
    graphics [
      x 696.4715421814906
      y 1124.3363999450246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030158"
      hgnc "NA"
      map_id "bishomo_minus_gamma_minus_linolenic_space_acid"
      name "bishomo_minus_gamma_minus_linolenic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e6b51"
      uniprot "NA"
    ]
    graphics [
      x 652.1474999895361
      y 1232.8683738371558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bishomo_minus_gamma_minus_linolenic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7c94a43"
      uniprot "NA"
    ]
    graphics [
      x 396.93329272659133
      y 511.791590644479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:uniprot:O95864"
      hgnc "NA"
      map_id "UNIPROT:O95864"
      name "FADS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa2ba"
      uniprot "UNIPROT:O95864"
    ]
    graphics [
      x 298.1914015506765
      y 446.7309525317184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95864"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d3b92"
      uniprot "NA"
    ]
    graphics [
      x 541.8664381137079
      y 879.0960451230437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000012660;urn:miriam:ensembl:ENSG00000197977"
      hgnc "NA"
      map_id "f1b8f"
      name "f1b8f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f1b8f"
      uniprot "NA"
    ]
    graphics [
      x 543.6910102112897
      y 996.7964748883926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f1b8f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:33505321;PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_38"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2cffd52"
      uniprot "NA"
    ]
    graphics [
      x 1323.6302374482736
      y 202.53759868115276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:32422320"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida0676778"
      uniprot "NA"
    ]
    graphics [
      x 1033.688697461415
      y 380.9655872061945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec306"
      uniprot "NA"
    ]
    graphics [
      x 1161.4992137501658
      y 839.3072541429974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 56
    source 3
    target 4
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
      target_id "W22_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 4
    target 5
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_46"
      target_id "Omega_minus_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 1
    target 6
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "W22_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 6
    target 2
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_44"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 7
    target 8
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "CoA(18:3(6Z,9Z,12Z))"
      target_id "W22_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 9
    target 8
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "ACOT2"
      target_id "W22_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 8
    target 10
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_24"
      target_id "gamma_minus_linolenic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 11
    target 12
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "palmitic_space_acid"
      target_id "W22_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 12
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_54"
      target_id "Cytokine_space_Storm"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 14
    target 15
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "CoA(20:3(8Z,11Z,14Z))"
      target_id "W22_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 16
    target 15
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "FADS1"
      target_id "W22_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 15
    target 17
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_1"
      target_id "Arachidonoyl_minus_CoA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 18
    target 19
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "stearic_space_acid"
      target_id "W22_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 19
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_52"
      target_id "Cytokine_space_Storm"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 20
    target 21
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "oleic_space_acid_br_"
      target_id "W22_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 21
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_40"
      target_id "Cytokine_space_Storm"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 22
    target 23
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "mitogen_minus_activated_space_protein_space_kinase"
      target_id "W22_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 23
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_41"
      target_id "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 25
    target 26
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Omega_minus_6"
      target_id "W22_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 26
    target 2
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_50"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 27
    target 28
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
      target_id "W22_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 28
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_53"
      target_id "Cytokine_space_Storm"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 29
    target 30
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "linoleic_space_acid"
      target_id "W22_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 30
    target 2
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_39"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 31
    target 32
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Arachidonic_space_acid"
      target_id "W22_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 32
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_55"
      target_id "Cytokine_space_Storm"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 29
    target 33
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "linoleic_space_acid"
      target_id "W22_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 34
    target 33
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "Linoleoyl_minus_CoA_br_desaturase_br_"
      target_id "W22_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 33
    target 35
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_48"
      target_id "CoA(18:2(9Z,12Z))"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 13
    target 36
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Cytokine_space_Storm"
      target_id "W22_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 36
    target 17
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_36"
      target_id "Arachidonoyl_minus_CoA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 37
    target 38
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2_space_and_space__br_COVID_minus_19_space_Pathway_br_Molecular_space_mechanism"
      target_id "W22_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 38
    target 39
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_37"
      target_id "W22_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 2
    target 40
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "ACE2"
      target_id "W22_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 40
    target 41
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_45"
      target_id "Virus_space_in_space_host_space_cell_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 42
    target 43
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Glycerophospholipids"
      target_id "W22_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 24
    target 43
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
      target_id "W22_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 43
    target 29
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "linoleic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 43
    target 11
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "palmitic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 43
    target 18
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "stearic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 43
    target 20
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "oleic_space_acid_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 39
    target 44
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_33"
      target_id "W22_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 44
    target 27
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_49"
      target_id "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 3
    target 45
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
      target_id "W22_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 45
    target 25
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_42"
      target_id "Omega_minus_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 17
    target 46
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Arachidonoyl_minus_CoA"
      target_id "W22_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 46
    target 3
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_12"
      target_id "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 14
    target 47
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "CoA(20:3(8Z,11Z,14Z))"
      target_id "W22_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 47
    target 48
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_5"
      target_id "bishomo_minus_gamma_minus_linolenic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 35
    target 49
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "CoA(18:2(9Z,12Z))"
      target_id "W22_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 50
    target 49
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95864"
      target_id "W22_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 49
    target 7
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_47"
      target_id "CoA(18:3(6Z,9Z,12Z))"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 7
    target 51
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "CoA(18:3(6Z,9Z,12Z))"
      target_id "W22_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 52
    target 51
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "f1b8f"
      target_id "W22_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 51
    target 14
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_23"
      target_id "CoA(20:3(8Z,11Z,14Z))"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 5
    target 53
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Omega_minus_3"
      target_id "W22_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 53
    target 2
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_38"
      target_id "ACE2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 41
    target 54
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Virus_space_in_space_host_space_cell_br_"
      target_id "W22_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 54
    target 22
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_51"
      target_id "mitogen_minus_activated_space_protein_space_kinase"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 17
    target 55
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "Arachidonoyl_minus_CoA"
      target_id "W22_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 55
    target 31
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_26"
      target_id "Arachidonic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
