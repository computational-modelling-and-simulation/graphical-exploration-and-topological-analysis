# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1932"
      uniprot "NA"
    ]
    graphics [
      x 157.6263429555977
      y 1459.097087746452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682009;PUBMED:22362731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_270"
      name "ZCRB1 binds 5'UTR of SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682009__layout_200"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1544.5317548707762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4; urn:miriam:reactome:R-HSA-9698376;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "UNIPROT:Q8TBF4"
      name "ZCRB1; ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand); ZCRB1:m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_201; layout_202; layout_2316; layout_3236"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 146.16978386332198
      y 1622.4899300088666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 3
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBF4"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
