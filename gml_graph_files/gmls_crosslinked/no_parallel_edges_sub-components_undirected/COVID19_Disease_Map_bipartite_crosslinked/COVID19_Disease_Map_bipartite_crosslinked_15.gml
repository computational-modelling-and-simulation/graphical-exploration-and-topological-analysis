# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q5MNZ9;urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
      name "a61b2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a61b2"
      uniprot "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
    ]
    graphics [
      x 1596.0635698360318
      y 2588.694963950067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id8d83e948"
      uniprot "NA"
    ]
    graphics [
      x 2244.831102311511
      y 2497.9217761446607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophagosome"
      name "Autophagosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e8965"
      uniprot "NA"
    ]
    graphics [
      x 1686.0635698360318
      y 2548.2650524756305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:24991833"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idceed974e"
      uniprot "NA"
    ]
    graphics [
      x 476.45915011555394
      y 1867.8041680700976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Lysozome"
      name "Lysozome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b5087"
      uniprot "NA"
    ]
    graphics [
      x 422.5
      y 1028.3546048957992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lysozome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "b2eb5"
      name "b2eb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b2eb5"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1489.1944410743417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b2eb5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autolysosome"
      name "Autolysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c2baf"
      uniprot "NA"
    ]
    graphics [
      x 1106.459150115554
      y 1982.121298750929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autolysosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 8
    source 1
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
      target_id "W7_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 2
    target 3
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_35"
      target_id "Autophagosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 3
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "Autophagosome"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 5
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "Lysozome"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 6
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "INHIBITION"
      source_id "b2eb5"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 4
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_37"
      target_id "Autolysosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
