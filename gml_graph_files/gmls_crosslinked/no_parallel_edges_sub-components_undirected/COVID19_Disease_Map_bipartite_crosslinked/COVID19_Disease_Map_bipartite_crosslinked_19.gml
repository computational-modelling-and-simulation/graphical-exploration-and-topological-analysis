# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:55922"
      hgnc "NA"
      map_id "NKRF"
      name "NKRF"
      node_subtype "GENE"
      node_type "species"
      org_id "ea9bc"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1452.615639559921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NKRF"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8e00894d"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1378.4715430068516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "acca6"
      uniprot "NA"
    ]
    graphics [
      x 1862.5
      y 1186.5210079975373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_9"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c049f"
      uniprot "NA"
    ]
    graphics [
      x 1166.459150115554
      y 1885.6792602184362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "cb292"
      uniprot "NA"
    ]
    graphics [
      x 1519.5745965099811
      y 2142.5814652116505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aff81"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 851.4275678762788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2a0ea132"
      uniprot "NA"
    ]
    graphics [
      x 1159.5745965099811
      y 2065.679260218436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd72cdda3"
      uniprot "NA"
    ]
    graphics [
      x 1379.834929654287
      y 2461.630219727918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_1"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "aa453"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1057.42270160177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3497;urn:miriam:ncbigene:3500;urn:miriam:ncbigene:3501;urn:miriam:ncbigene:3543;urn:miriam:ncbigene:3538;urn:miriam:ncbigene:3503"
      hgnc "NA"
      map_id "b7f53"
      name "b7f53"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7f53"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1177.285306990201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b7f53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:5725029;urn:miriam:ncbigene:3929;urn:miriam:ncbigene:10394;urn:miriam:ncbigene:1401;urn:miriam:ncbigene:64386"
      hgnc "NA"
      map_id "b120d"
      name "b120d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b120d"
      uniprot "NA"
    ]
    graphics [
      x 1671.00456109272
      y 2618.694963950067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b120d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:920;urn:miriam:ncbigene:926;urn:miriam:ncbigene:925;urn:miriam:ncbigene:914"
      hgnc "NA"
      map_id "cb65d"
      name "cb65d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cb65d"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1572.2786556188137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cb65d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3569"
      hgnc "NA"
      map_id "bebbf"
      name "bebbf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bebbf"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1178.6949639500672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bebbf"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:919;urn:miriam:ncbigene:28639;urn:miriam:ncbigene:28755;urn:miriam:ncbigene:916;urn:miriam:ncbigene:917"
      hgnc "NA"
      map_id "d2540"
      name "d2540"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2540"
      uniprot "NA"
    ]
    graphics [
      x 1198.0561600971253
      y 2271.0116320122006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d2540"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:7535;urn:miriam:ncbigene:3932;urn:miriam:ncbigene:2534"
      hgnc "NA"
      map_id "dbed0"
      name "dbed0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dbed0"
      uniprot "NA"
    ]
    graphics [
      x 1759.5745965099811
      y 2244.689478303303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dbed0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3122;urn:miriam:ncbigene:3123;urn:miriam:ncbigene:3126;urn:miriam:ncbigene:3127"
      hgnc "NA"
      map_id "b533f"
      name "b533f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b533f"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1420.6134979422188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b533f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikidata:Q87917572;urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32838362"
      hgnc "NA"
      map_id "b7705"
      name "b7705"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7705"
      uniprot "NA"
    ]
    graphics [
      x 2119.5745965099813
      y 2110.1189559661866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b7705"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_19"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "idea6a7587"
      uniprot "NA"
    ]
    graphics [
      x 1589.955641054751
      y 2438.694963950067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikipathways:WP4846"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_ORFs"
      name "SARS_minus_CoV_minus_2_space_ORFs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e9691"
      uniprot "NA"
    ]
    graphics [
      x 1881.841788597757
      y 1709.7323077154438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_ORFs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 20
    source 2
    target 1
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_17"
      target_id "NKRF"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 1
    target 3
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 4
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 1
    target 5
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 1
    target 6
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 1
    target 7
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 1
    target 8
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 1
    target 9
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 17
    target 2
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "b7705"
      target_id "W19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 3
    target 16
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_2"
      target_id "b533f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 4
    target 15
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_9"
      target_id "dbed0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 5
    target 14
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_10"
      target_id "d2540"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 6
    target 13
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_3"
      target_id "bebbf"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 7
    target 12
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_16"
      target_id "cb65d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 8
    target 11
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_18"
      target_id "b120d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 9
    target 10
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_1"
      target_id "b7f53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 18
    target 17
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_19"
      target_id "b7705"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 19
    target 18
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2_space_ORFs"
      target_id "W19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
