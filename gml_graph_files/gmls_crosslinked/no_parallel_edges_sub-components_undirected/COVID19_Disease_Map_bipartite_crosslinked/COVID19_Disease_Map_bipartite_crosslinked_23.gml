# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000163737;urn:miriam:ncbigene:2920;urn:miriam:ensembl:ENSG00000169429;urn:miriam:ensembl:ENSG00000169248;urn:miriam:ensembl:ENSG00000163736;urn:miriam:ensembl:ENSG00000156234;urn:miriam:ensembl:ENSG00000275302;urn:miriam:ensembl:ENSG00000108691;urn:miriam:ensembl:ENSG00000107562;urn:miriam:ensembl:ENSG00000169245;urn:miriam:ensembl:ENSG00000163735;urn:miriam:ensembl:ENSG00000163734;urn:miriam:ncbigene:6372;urn:miriam:ensembl:ENSG00000163739;urn:miriam:ensembl:ENSG00000277632;urn:miriam:ensembl:ENSG00000138755"
      hgnc "NA"
      map_id "fcec7"
      name "fcec7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fcec7"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 922.8004127166694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fcec7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32416070;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c290b"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 863.9899776768532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_131"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f692c"
      uniprot "NA"
    ]
    graphics [
      x 1517.6025631240852
      y 626.6504427592781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "Neutrophil_space_recruitment"
      name "Neutrophil_space_recruitment"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e62ae"
      uniprot "NA"
    ]
    graphics [
      x 1711.4819801735532
      y 234.68947830330308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Neutrophil_space_recruitment"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_76"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfc78"
      uniprot "NA"
    ]
    graphics [
      x 2347.3474404714884
      y 638.7189702225969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 6
    source 2
    target 1
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_59"
      target_id "fcec7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 1
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "fcec7"
      target_id "W21_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 5
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_76"
      target_id "W21_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 3
    target 4
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_131"
      target_id "Neutrophil_space_recruitment"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
