# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983;urn:miriam:ncbigene:3646;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:8894;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:1968;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8665; https://identifiers.org/complexportal:CPX-5223; urn:miriam:ncbigene:8665;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:1983;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8666;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8662"
      hgnc "NA"
      map_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b327e; dc35b; ed8b6"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1176.8972151454109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4b83f12a"
      uniprot "NA"
    ]
    graphics [
      x 2756.4591501155537
      y 1707.3279402424666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_18"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8da43876"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1342.9679496508934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id573935d6"
      uniprot "NA"
    ]
    graphics [
      x 2389.5745965099813
      y 2222.4006145847416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_13"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id2d00e6ed"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1420.2308504778182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8b1ce7b7"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 912.3830754138021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_12"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id19d08a66"
      uniprot "NA"
    ]
    graphics [
      x 1579.5745965099811
      y 2202.5814652116505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id78120f40"
      uniprot "NA"
    ]
    graphics [
      x 1849.5745965099811
      y 2014.5616833256738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb76f4fdf"
      uniprot "NA"
    ]
    graphics [
      x 2624.295511425698
      y 714.3748524910262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "Load_space_mRNA_space_and_space_start_space_translation"
      name "Load_space_mRNA_space_and_space_start_space_translation"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "dfc42"
      uniprot "NA"
    ]
    graphics [
      x 1411.5253887200693
      y 2140.2308504778184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Load_space_mRNA_space_and_space_start_space_translation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983"
      hgnc "NA"
      map_id "EIF5"
      name "EIF5"
      node_subtype "GENE"
      node_type "species"
      org_id "db339"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 810.5018723715465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1968;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:8894"
      hgnc "NA"
      map_id "Ternary_space_complex"
      name "Ternary_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d6fe1"
      uniprot "NA"
    ]
    graphics [
      x 1376.459150115554
      y 1757.295570402829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ternary_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:10209"
      hgnc "NA"
      map_id "EIF1"
      name "EIF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fea09"
      uniprot "NA"
    ]
    graphics [
      x 2239.5745965099813
      y 2178.0402426628975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1964"
      hgnc "NA"
      map_id "EIF1A"
      name "EIF1A"
      node_subtype "GENE"
      node_type "species"
      org_id "ca957"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1560.6881277329076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF1A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8665;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666"
      hgnc "NA"
      map_id "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      name "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b97c5"
      uniprot "NA"
    ]
    graphics [
      x 2389.5745965099813
      y 2177.570615365179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 16
    source 2
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_14"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 3
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_18"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 4
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_15"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 5
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_13"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 6
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_17"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 1
    target 7
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 8
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 1
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 15
    target 2
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      target_id "W4_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 14
    target 3
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF1A"
      target_id "W4_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 13
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF1"
      target_id "W4_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 12
    target 5
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "Ternary_space_complex"
      target_id "W4_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 11
    target 6
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF5"
      target_id "W4_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 7
    target 10
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_12"
      target_id "Load_space_mRNA_space_and_space_start_space_translation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
