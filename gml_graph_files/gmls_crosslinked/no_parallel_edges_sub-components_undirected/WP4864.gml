# generated with VANTED V2.8.2 at Fri Mar 04 09:59:53 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; WP4864; WP4880"
      full_annotation "urn:miriam:reactome:R-COV-9684216;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694371; urn:miriam:reactome:R-COV-9694446;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9684206; urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694439; urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P59596; urn:miriam:pubmed:16442106;urn:miriam:reactome:R-COV-9683588;urn:miriam:reactome:R-COV-9694684;urn:miriam:uniprot:P59596; urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "UNIPROT:P59596"
      name "M_space_lattice; N_minus_glycan_space_M; M; nascent_space_M"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_602; layout_601; layout_600; layout_236; layout_387; b1ff7; ea9e0"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 1286.684270574853
      y 703.9520991007814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59596"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9678108; WP4864; WP4880; C19DMap:TGFbeta signalling; C19DMap:JNK pathway; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967; urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433; urn:miriam:uniprot:P59632; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669; urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "UNIPROT:P59632"
      name "O_minus_glycosyl_space_3a_space_tetramer; 3a; 3a:membranous_space_structure; O_minus_glycosyl_space_3a; GalNAc_minus_O_minus_3a; Orf3a; SARS_space_Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_392; layout_584; layout_1543; layout_230; layout_342; layout_585; layout_349; layout_345; layout_581; b5cfb; b7423; sa65; sa77; sa147; sa3; sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 665.8054888405261
      y 1311.139038822721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59632"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 783.6855208113113
      y 773.8752422456946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "Apoptosome"
      name "Apoptosome"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ca06b"
      uniprot "NA"
    ]
    graphics [
      x 250.00873224545404
      y 878.6541979545314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_28"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id332625ef"
      uniprot "NA"
    ]
    graphics [
      x 362.1934842598038
      y 747.4621198323739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000164305;urn:miriam:ensembl:ENSG00000165806"
      hgnc "NA"
      map_id "bbd5b"
      name "bbd5b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bbd5b"
      uniprot "NA"
    ]
    graphics [
      x 505.49743633458274
      y 640.8797325000826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bbd5b"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000232810;urn:miriam:ensembl:ENSG00000117560"
      hgnc "NA"
      map_id "a042f"
      name "a042f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a042f"
      uniprot "NA"
    ]
    graphics [
      x 1123.555483063533
      y 1041.7732213024412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a042f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_27"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id26c91cfd"
      uniprot "NA"
    ]
    graphics [
      x 1048.0603430519977
      y 954.1896975427967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000168040"
      hgnc "NA"
      map_id "FADD"
      name "FADD"
      node_subtype "GENE"
      node_type "species"
      org_id "b16d4"
      uniprot "NA"
    ]
    graphics [
      x 1021.590297526395
      y 816.8543932981281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FADD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59595;urn:miriam:uniprot:Q7TFA0;urn:miriam:uniprot:P59636;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59637;urn:miriam:uniprot:P59633;urn:miriam:uniprot:P59594"
      hgnc "NA"
      map_id "UNIPROT:P59595;UNIPROT:Q7TFA0;UNIPROT:P59636;UNIPROT:P59634;UNIPROT:P59637;UNIPROT:P59633;UNIPROT:P59594"
      name "f5b62"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5b62"
      uniprot "UNIPROT:P59595;UNIPROT:Q7TFA0;UNIPROT:P59636;UNIPROT:P59634;UNIPROT:P59637;UNIPROT:P59633;UNIPROT:P59594"
    ]
    graphics [
      x 555.6861680179843
      y 854.8083367746436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59595;UNIPROT:Q7TFA0;UNIPROT:P59636;UNIPROT:P59634;UNIPROT:P59637;UNIPROT:P59633;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_40"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idad374e81"
      uniprot "NA"
    ]
    graphics [
      x 672.025534481056
      y 849.590864798475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000132906"
      hgnc "NA"
      map_id "CASP9"
      name "CASP9"
      node_subtype "GENE"
      node_type "species"
      org_id "cacde"
      uniprot "NA"
    ]
    graphics [
      x 483.0998846318423
      y 992.1301227355015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CASP9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idb82c8f11"
      uniprot "NA"
    ]
    graphics [
      x 354.78616474148595
      y 958.1287646007939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_41"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idb0a41cb7"
      uniprot "NA"
    ]
    graphics [
      x 646.7370113740394
      y 702.3605407552917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_37"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id812e8e5d"
      uniprot "NA"
    ]
    graphics [
      x 1189.878323574796
      y 651.5861238468672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ncbigene:207"
      hgnc "NA"
      map_id "AKT1"
      name "AKT1"
      node_subtype "GENE"
      node_type "species"
      org_id "bfe85"
      uniprot "NA"
    ]
    graphics [
      x 1071.4760447066833
      y 701.7379050632645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AKT1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide4442266"
      uniprot "NA"
    ]
    graphics [
      x 927.4705545996302
      y 755.1226311577792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_33"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54c6a1c1"
      uniprot "NA"
    ]
    graphics [
      x 957.6768568946095
      y 660.680395352114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000064012"
      hgnc "NA"
      map_id "CASP8"
      name "CASP8"
      node_subtype "GENE"
      node_type "species"
      org_id "f60b1"
      uniprot "NA"
    ]
    graphics [
      x 857.0146068980577
      y 526.8378928853105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CASP8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "OC43_br_infection"
      name "OC43_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5a4a"
      uniprot "NA"
    ]
    graphics [
      x 651.4307088809219
      y 779.7313757155475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "OC43_br_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_12"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c78a8"
      uniprot "NA"
    ]
    graphics [
      x 608.3055459482307
      y 932.4706605484778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637"
      hgnc "NA"
      map_id "UNIPROT:P59635;UNIPROT:P59637"
      name "dcc55"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dcc55"
      uniprot "UNIPROT:P59635;UNIPROT:P59637"
    ]
    graphics [
      x 99.68669300459476
      y 225.58512275789252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P59635;UNIPROT:P59637"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idda923b3f"
      uniprot "NA"
    ]
    graphics [
      x 188.19350635908938
      y 292.4043258515619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000171791;urn:miriam:ensembl:ENSG00000143384;urn:miriam:ensembl:ENSG00000171552"
      hgnc "NA"
      map_id "d5ccc"
      name "d5ccc"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d5ccc"
      uniprot "NA"
    ]
    graphics [
      x 297.3467957258335
      y 228.53670113311614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d5ccc"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_30"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id42dbcb94"
      uniprot "NA"
    ]
    graphics [
      x 680.7607780446207
      y 559.0214916588819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000120868"
      hgnc "NA"
      map_id "APAF1"
      name "APAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "f2fff"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 737.5560952216427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "APAF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_26"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id23a5b04e"
      uniprot "NA"
    ]
    graphics [
      x 125.45786199577219
      y 835.8865697361717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id67844ae8"
      uniprot "NA"
    ]
    graphics [
      x 603.8110079254838
      y 645.7281304214588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "OC43_space_infection"
      name "OC43_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cdc80"
      uniprot "NA"
    ]
    graphics [
      x 728.7015106751129
      y 85.45271117674872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "OC43_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_39"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9d6e5912"
      uniprot "NA"
    ]
    graphics [
      x 614.6470902123045
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000087088"
      hgnc "NA"
      map_id "BAX"
      name "BAX"
      node_subtype "GENE"
      node_type "species"
      org_id "bb36d; dbfc4"
      uniprot "NA"
    ]
    graphics [
      x 519.2173733415723
      y 139.22053932571816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BAX"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000015475"
      hgnc "NA"
      map_id "BID"
      name "BID"
      node_subtype "GENE"
      node_type "species"
      org_id "aff48"
      uniprot "NA"
    ]
    graphics [
      x 803.1898921362988
      y 387.39667174200633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BID"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:9727492"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5ea6cbdd"
      uniprot "NA"
    ]
    graphics [
      x 903.1519358735887
      y 370.1387039635888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:PRO_0000223233;urn:miriam:pubmed:9727492"
      hgnc "NA"
      map_id "UNIPROT:PRO_0000223233"
      name "tBID"
      node_subtype "GENE"
      node_type "species"
      org_id "ef970"
      uniprot "UNIPROT:PRO_0000223233"
    ]
    graphics [
      x 803.4155759408964
      y 255.3455356088195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:PRO_0000223233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_32"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4d5fb94c"
      uniprot "NA"
    ]
    graphics [
      x 662.4191465424952
      y 198.63514440278095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000188130;urn:miriam:ensembl:ENSG00000156711;urn:miriam:ensembl:ENSG00000185386;urn:miriam:ensembl:ENSG00000112062"
      hgnc "NA"
      map_id "e188e"
      name "e188e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e188e"
      uniprot "NA"
    ]
    graphics [
      x 430.79494093535527
      y 1340.1131857366872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e188e"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id44e385eb"
      uniprot "NA"
    ]
    graphics [
      x 316.23611125569664
      y 1272.2057731209356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18070"
      hgnc "NA"
      map_id "Cytochrome_space_C"
      name "Cytochrome_space_C"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f4f84"
      uniprot "NA"
    ]
    graphics [
      x 230.57993187546947
      y 1166.3788602871855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytochrome_space_C"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9d25fe73"
      uniprot "NA"
    ]
    graphics [
      x 208.16705620289866
      y 1026.5827409782846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_3"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a8baf"
      uniprot "NA"
    ]
    graphics [
      x 383.2769488216245
      y 125.57193437270575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000105327;urn:miriam:ensembl:ENSG00000002330;urn:miriam:ensembl:ENSG00000153094"
      hgnc "NA"
      map_id "b7dd0"
      name "b7dd0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7dd0"
      uniprot "NA"
    ]
    graphics [
      x 420.23577665012124
      y 367.3005759949058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b7dd0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6ca195ae"
      uniprot "NA"
    ]
    graphics [
      x 423.70900183171193
      y 238.47607144915622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_29"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3bbd5b9"
      uniprot "NA"
    ]
    graphics [
      x 556.7943417512483
      y 1364.7586302706989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 44
    source 4
    target 5
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "Apoptosome"
      target_id "W8_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 5
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_28"
      target_id "bbd5b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 7
    target 8
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "a042f"
      target_id "W8_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 8
    target 9
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_27"
      target_id "FADD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 10
    target 11
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59595;UNIPROT:Q7TFA0;UNIPROT:P59636;UNIPROT:P59634;UNIPROT:P59637;UNIPROT:P59633;UNIPROT:P59594"
      target_id "W8_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 11
    target 3
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_40"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 12
    target 13
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "CASP9"
      target_id "W8_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 13
    target 4
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_42"
      target_id "Apoptosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 6
    target 14
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "bbd5b"
      target_id "W8_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 14
    target 3
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_41"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 1
    target 15
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59596"
      target_id "W8_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 15
    target 16
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_37"
      target_id "AKT1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 16
    target 17
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "AKT1"
      target_id "W8_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 17
    target 3
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_44"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 9
    target 18
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "FADD"
      target_id "W8_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 18
    target 19
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_33"
      target_id "CASP8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 20
    target 21
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "OC43_br_infection"
      target_id "W8_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 21
    target 12
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_12"
      target_id "CASP9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 22
    target 23
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59635;UNIPROT:P59637"
      target_id "W8_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 23
    target 24
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_43"
      target_id "d5ccc"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 19
    target 25
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "CASP8"
      target_id "W8_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 25
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_30"
      target_id "bbd5b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 26
    target 27
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "APAF1"
      target_id "W8_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 27
    target 4
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_26"
      target_id "Apoptosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 20
    target 28
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "OC43_br_infection"
      target_id "W8_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 28
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_35"
      target_id "bbd5b"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 29
    target 30
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "OC43_space_infection"
      target_id "W8_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 30
    target 31
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_39"
      target_id "BAX"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 32
    target 33
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "BID"
      target_id "W8_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 19
    target 33
    cd19dm [
      diagram "WP4864"
      edge_type "CATALYSIS"
      source_id "CASP8"
      target_id "W8_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 33
    target 34
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_34"
      target_id "UNIPROT:PRO_0000223233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 34
    target 35
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:PRO_0000223233"
      target_id "W8_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 35
    target 31
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_32"
      target_id "BAX"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 36
    target 37
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "e188e"
      target_id "W8_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 37
    target 38
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_31"
      target_id "Cytochrome_space_C"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 38
    target 39
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "Cytochrome_space_C"
      target_id "W8_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 39
    target 4
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_38"
      target_id "Apoptosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 24
    target 40
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "d5ccc"
      target_id "W8_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 40
    target 31
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_3"
      target_id "BAX"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 41
    target 42
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "b7dd0"
      target_id "W8_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 24
    target 42
    cd19dm [
      diagram "WP4864"
      edge_type "INHIBITION"
      source_id "d5ccc"
      target_id "W8_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 42
    target 31
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_36"
      target_id "BAX"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 2
    target 43
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P59632"
      target_id "W8_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 43
    target 36
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_29"
      target_id "e188e"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
