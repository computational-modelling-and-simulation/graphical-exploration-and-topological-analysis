# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9694300;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683625; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729324; urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768; urn:miriam:reactome:R-COV-9684230;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729277;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9729264;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9684199;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694612; urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729335;urn:miriam:pubmed:19106108; urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694659; urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P0DTC9; urn:miriam:reactome:R-COV-9729308;urn:miriam:uniprot:P0DTC9; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC9; urn:miriam:uniprot:P0DTC9; urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9; urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "NA; HGNC_SYMBOL:N"
      map_id "UNIPROT:P0DTC9"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; N_space_tetramer; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand); N; p_minus_S188,206_minus_N; SUMO_minus_p_minus_N_space_dimer:SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S,2T_minus_N; p_minus_11S,2T_minus_N; encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA; p_minus_8S_minus_N; ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N; p_minus_11S,2T_minus_metR95,177_minus_N; nucleocapsid_br_protein; Nucleocapsid; Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_3639; layout_2352; layout_2496; layout_3571; layout_2364; layout_2341; layout_3558; layout_2343; layout_2465; layout_3542; layout_3528; layout_2353; layout_2467; layout_3574; layout_2361; layout_2362; layout_3616; layout_3529; layout_2960; ff3a1; b0f05; b6d08; b9df2; sa1685; sa1887; csa365; csa369; csa374; csa398; sa1667; csa353; csa357; csa391; csa389; csa387; csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 774.3388349073875
      y 1861.8721737867022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id254c7db4"
      uniprot "NA"
    ]
    graphics [
      x 819.0413778587194
      y 1965.0586584759421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "nucleocapsid_br_phosphoprotein"
      name "nucleocapsid_br_phosphoprotein"
      node_subtype "GENE"
      node_type "species"
      org_id "aabef"
      uniprot "NA"
    ]
    graphics [
      x 926.0772390502283
      y 1932.8097481227985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nucleocapsid_br_phosphoprotein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 2
    target 1
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_119"
      target_id "UNIPROT:P0DTC9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 3
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "nucleocapsid_br_phosphoprotein"
      target_id "W1_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
