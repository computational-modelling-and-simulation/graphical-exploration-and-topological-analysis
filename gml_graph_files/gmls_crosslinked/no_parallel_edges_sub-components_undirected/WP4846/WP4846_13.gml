# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572;urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "cd41e"
      name "cd41e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cd41e"
      uniprot "NA"
    ]
    graphics [
      x 1022.4197134594956
      y 280.6929540083239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cd41e"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_158"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd68bc555"
      uniprot "NA"
    ]
    graphics [
      x 1058.567174284771
      y 163.87725007085476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4884"
      hgnc "NA"
      map_id "Pathogenesis"
      name "Pathogenesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bc193"
      uniprot "NA"
    ]
    graphics [
      x 1016.243239264443
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pathogenesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "cd41e"
      target_id "W1_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_158"
      target_id "Pathogenesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
