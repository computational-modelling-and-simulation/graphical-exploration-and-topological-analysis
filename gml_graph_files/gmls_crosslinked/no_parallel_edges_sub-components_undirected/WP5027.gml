# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4846; WP5027; WP4868; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882; urn:miriam:wikidata:Q90038952; urn:miriam:ncbiprotein:YP_009725297; NA"
      hgnc "NA"
      map_id "nsp1"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606; f9094; e6110; d2ec8; da173; glyph39"
      uniprot "NA"
    ]
    graphics [
      x 778.6541675615614
      y 825.7710066360767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_19"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idad839e9d"
      uniprot "NA"
    ]
    graphics [
      x 886.278903763292
      y 866.2960677337288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8669"
      hgnc "NA"
      map_id "EIF3J"
      name "EIF3J"
      node_subtype "GENE"
      node_type "species"
      org_id "efe7f"
      uniprot "NA"
    ]
    graphics [
      x 827.8296685090538
      y 962.146433626642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF3J"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983;urn:miriam:ncbigene:3646;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:8894;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:1968;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8665; https://identifiers.org/complexportal:CPX-5223; urn:miriam:ncbigene:8665;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:1983;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8666;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8662"
      hgnc "NA"
      map_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b327e; dc35b; ed8b6"
      uniprot "NA"
    ]
    graphics [
      x 231.55039952819374
      y 285.63625214755524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_12"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id19d08a66"
      uniprot "NA"
    ]
    graphics [
      x 177.90566891429773
      y 379.9894602756355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "Load_space_mRNA_space_and_space_start_space_translation"
      name "Load_space_mRNA_space_and_space_start_space_translation"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "dfc42"
      uniprot "NA"
    ]
    graphics [
      x 287.56491032642083
      y 341.18749342057913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Load_space_mRNA_space_and_space_start_space_translation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8665;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666"
      hgnc "NA"
      map_id "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      name "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b97c5"
      uniprot "NA"
    ]
    graphics [
      x 252.59266330387607
      y 595.7420837149897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4b83f12a"
      uniprot "NA"
    ]
    graphics [
      x 224.04258547424342
      y 470.92689164732457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id78120f40"
      uniprot "NA"
    ]
    graphics [
      x 126.58584132803605
      y 248.01196021101526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb76f4fdf"
      uniprot "NA"
    ]
    graphics [
      x 280.28743357352005
      y 407.8932774773326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1964"
      hgnc "NA"
      map_id "EIF1A"
      name "EIF1A"
      node_subtype "GENE"
      node_type "species"
      org_id "ca957"
      uniprot "NA"
    ]
    graphics [
      x 508.0146594803194
      y 299.6173126925774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF1A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_18"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8da43876"
      uniprot "NA"
    ]
    graphics [
      x 387.36654232242324
      y 260.60407882031956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:10209"
      hgnc "NA"
      map_id "EIF1"
      name "EIF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fea09"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 501.8945919655286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id573935d6"
      uniprot "NA"
    ]
    graphics [
      x 93.37227306050448
      y 378.5610583870392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1968;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:8894"
      hgnc "NA"
      map_id "Ternary_space_complex"
      name "Ternary_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d6fe1"
      uniprot "NA"
    ]
    graphics [
      x 325.1091148786852
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ternary_space_complex"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_13"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id2d00e6ed"
      uniprot "NA"
    ]
    graphics [
      x 294.49678537812974
      y 173.77126781323273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983"
      hgnc "NA"
      map_id "EIF5"
      name "EIF5"
      node_subtype "GENE"
      node_type "species"
      org_id "db339"
      uniprot "NA"
    ]
    graphics [
      x 463.5921190863247
      y 480.48814489084225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8b1ce7b7"
      uniprot "NA"
    ]
    graphics [
      x 378.74701270651536
      y 383.5545006950552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 19
    source 1
    target 2
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "nsp1"
      target_id "W4_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 2
    target 3
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_19"
      target_id "EIF3J"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 4
    target 5
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 5
    target 6
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_12"
      target_id "Load_space_mRNA_space_and_space_start_space_translation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 7
    target 8
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      target_id "W4_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 8
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_14"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 4
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 4
    target 10
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      target_id "W4_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 11
    target 12
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF1A"
      target_id "W4_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 12
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_18"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 13
    target 14
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF1"
      target_id "W4_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 14
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_15"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 15
    target 16
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "Ternary_space_complex"
      target_id "W4_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 16
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_13"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 17
    target 18
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "EIF5"
      target_id "W4_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 18
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_17"
      target_id "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
