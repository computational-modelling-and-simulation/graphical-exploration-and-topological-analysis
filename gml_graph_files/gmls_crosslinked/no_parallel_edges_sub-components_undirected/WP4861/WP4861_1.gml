# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 31
      diagram "R-HSA-9694516; WP4846; WP4799; WP4861; WP4853; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195; urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880; urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917; urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459; urn:miriam:uniprot:P0DTC2; urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025; urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "NA; HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; fully_space_glycosylated_space_Spike_space_trimer; tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer; Man(9)_space_N_minus_glycan_space_unfolded_space_Spike; high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike; nascent_space_Spike; high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike; high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike; 14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike; trimer; surface_br_glycoprotein_space_S; b76b3; surface_br_glycoprotein; a4fdf; SARS_minus_CoV_minus_2_space_spike; OC43_space_infection; S"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2897; layout_2956; layout_2896; layout_3099; layout_3050; layout_3055; layout_2899; layout_2903; layout_2329; layout_2894; layout_2895; layout_2376; c25c7; e7798; b76b3; c8192; cc4b9; a6335; f7af7; a4fdf; cfddc; bc47f; eef69; sa1688; sa1893; sa2040; sa2178; sa1859; sa2009; sa2173; sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1134.7072775167574
      y 775.9858710297847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id839c7346"
      uniprot "NA"
    ]
    graphics [
      x 988.0494030141989
      y 858.287557202935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "iddcd631b5"
      uniprot "NA"
    ]
    graphics [
      x 1205.7226906843046
      y 640.3735673268716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:O75460; urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "NA; HGNC_SYMBOL:ERN1"
      map_id "UNIPROT:O75460"
      name "f8553; ERN1; ERN1:Unfolded_space_protein"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "f8553; b9fe8; a273e; path_0_sa1; path_0_csa2; path_0_sa617"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 1213.6673865639623
      y 490.7991017218991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75460"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc7e2ae4"
      uniprot "NA"
    ]
    graphics [
      x 1340.609108253499
      y 415.89411629669206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11f5ed11"
      uniprot "NA"
    ]
    graphics [
      x 1135.933877347042
      y 671.3149961492086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id74bb08d8"
      uniprot "NA"
    ]
    graphics [
      x 1058.4976267820305
      y 304.06913080297284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc4a4b8a1"
      uniprot "NA"
    ]
    graphics [
      x 1287.2289967027352
      y 700.2708653883311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Interferon 1 pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P45983; urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599; urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "NA; HGNC_SYMBOL:MAPK8"
      map_id "UNIPROT:P45983"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b098e; sa63; path_0_sa40; path_0_sa41"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 1296.4869912358392
      y 888.8221994618807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P45983"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc5c1"
      uniprot "NA"
    ]
    graphics [
      x 1243.068428215072
      y 1052.152390796771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; WP4864; WP4877; WP5039; C19DMap:JNK pathway; C19DMap:Apoptosis pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:wikipathways:WP354; NA; urn:miriam:wikipathways:WP254; urn:miriam:obo.go:GO%3A0006915; urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2; urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567; urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "Apoptosis"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2; d1a8d; be42e; a6ff9; sa17; sa41; path_1_sa110; path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 1111.2462605953492
      y 1156.5672796992922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida913653e"
      uniprot "NA"
    ]
    graphics [
      x 1127.3446643098723
      y 1015.8058871439462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5555a7cf"
      uniprot "NA"
    ]
    graphics [
      x 944.0691306425317
      y 1239.2204056900407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P35638"
      hgnc "NA"
      map_id "UNIPROT:P35638"
      name "CHOP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2d29"
      uniprot "UNIPROT:P35638"
    ]
    graphics [
      x 756.7865089126657
      y 1269.0228863389889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35638"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc828ca15"
      uniprot "NA"
    ]
    graphics [
      x 718.705959004748
      y 1091.2982534171283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id17eaab75"
      uniprot "NA"
    ]
    graphics [
      x 622.1975470960718
      y 1257.0148840130873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idde832765"
      uniprot "NA"
    ]
    graphics [
      x 608.3878846170487
      y 1379.8434941755086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
      name "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e0713"
      uniprot "NA"
    ]
    graphics [
      x 437.5952279545298
      y 1416.626909264806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id315b7e46"
      uniprot "NA"
    ]
    graphics [
      x 262.90806579510706
      y 1389.8354750431663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861; WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "BCL2"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "eef8e; e21a3"
      uniprot "NA"
    ]
    graphics [
      x 103.51132870082586
      y 1338.8727541178716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BCL2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:JNK pathway; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P10415; urn:miriam:hgnc.symbol:BCL2;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990; urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "NA; HGNC_SYMBOL:BCL2"
      map_id "UNIPROT:P10415"
      name "BCL2"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "c7da2; sa53; sa12; path_0_sa55; path_0_sa54; path_0_sa152"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 160.54396466908975
      y 1330.3287085156119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10415"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:O75807;urn:miriam:wikidata:Q7251492; urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645; urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645;urn:miriam:ncbigene:23645"
      hgnc "NA; HGNC_SYMBOL:PPP1R15A"
      map_id "UNIPROT:O75807"
      name "df8e0; PPP1R15A"
      node_subtype "COMPLEX; RNA; PROTEIN; GENE"
      node_type "species"
      org_id "df8e0; path_0_sa108; path_0_sa107; path_0_sa109"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 491.9259884578108
      y 1198.4802825797942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75807"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_87"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idead04e33"
      uniprot "NA"
    ]
    graphics [
      x 383.0797249637138
      y 1088.8858815314143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q53XC0"
      hgnc "NA"
      map_id "UNIPROT:Q53XC0"
      name "EIF2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6968; d4ef9"
      uniprot "UNIPROT:Q53XC0"
    ]
    graphics [
      x 330.01339060636496
      y 935.8700607893317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q53XC0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf3808898"
      uniprot "NA"
    ]
    graphics [
      x 445.4188693483213
      y 837.9622826577016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_70"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id915467c9"
      uniprot "NA"
    ]
    graphics [
      x 419.3630993144647
      y 997.158141106609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida575a860"
      uniprot "NA"
    ]
    graphics [
      x 505.7208475140569
      y 921.4769301902022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb174dd6a"
      uniprot "NA"
    ]
    graphics [
      x 167.17022147213913
      y 949.9504972987979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4861; C19DMap:PAMP signalling; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P19525; urn:miriam:uniprot:P19525;urn:miriam:wikidata:Q2819370; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610; urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "NA; HGNC_SYMBOL:EIF2AK2"
      map_id "UNIPROT:P19525"
      name "PKR; d477c; EIF2AK2:dsRNA; EIF2AK2"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "c0e50; d477c; csa96; sa454; path_0_sa287; path_0_sa210"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 126.08718766467939
      y 1109.628742276904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19525"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9BQI3"
      hgnc "NA"
      map_id "UNIPROT:Q9H492;UNIPROT:Q9BQI3"
      name "e7f9b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7f9b"
      uniprot "UNIPROT:Q9H492;UNIPROT:Q9BQI3"
    ]
    graphics [
      x 62.5
      y 902.4137952133159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H492;UNIPROT:Q9BQI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9NZJ5; urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "NA; HGNC_SYMBOL:EIF2AK3"
      map_id "UNIPROT:Q9NZJ5"
      name "fbd45; PERK; EIF2AK3:EIF2AK3; EIF2AK3"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "fbd45; a8ce8; path_0_csa21; path_0_sa2"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 191.76355670102828
      y 802.183199131427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NZJ5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idcf12fa8a"
      uniprot "NA"
    ]
    graphics [
      x 301.16831381432786
      y 706.6430703316577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_IBV_space_infection"
      name "SARS,_space_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "dc80b"
      uniprot "NA"
    ]
    graphics [
      x 457.958534555511
      y 688.0522388852253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_IBV_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc824ad90"
      uniprot "NA"
    ]
    graphics [
      x 634.9986383374884
      y 720.0946943951933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P11021;urn:miriam:uniprot:Q9NZJ5"
      hgnc "NA"
      map_id "UNIPROT:P11021;UNIPROT:Q9NZJ5"
      name "a0c95"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a0c95"
      uniprot "UNIPROT:P11021;UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 587.3189211844677
      y 614.4634325734082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11021;UNIPROT:Q9NZJ5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 8
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P11021; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:pubmed:32169481;urn:miriam:hgnc:5238;urn:miriam:pubmed:30978349;urn:miriam:refseq:NM_005347;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:pubmed:32340551;urn:miriam:ec-code:3.6.4.10; urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "NA; HGNC_SYMBOL:HSPA5"
      map_id "UNIPROT:P11021"
      name "cc525; e10e4; dadd6; HSPA5"
      node_subtype "COMPLEX; PROTEIN; RNA; GENE"
      node_type "species"
      org_id "cc525; e10e4; dadd6; path_0_sa4; path_1_sa87; sa262; path_1_sa149; path_1_sa125"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 810.2280209506404
      y 814.2534673192426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3e604b90"
      uniprot "NA"
    ]
    graphics [
      x 846.1494888037913
      y 937.7714189409342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18850;urn:miriam:uniprot:P11021; urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "NA; HGNC_SYMBOL:ATF6;HGNC_SYMBOL:HSPA5"
      map_id "UNIPROT:P18850;UNIPROT:P11021"
      name "beb8c; ATF6:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "beb8c; path_0_csa1"
      uniprot "UNIPROT:P18850;UNIPROT:P11021"
    ]
    graphics [
      x 893.96068558741
      y 1042.7963117847319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18850;UNIPROT:P11021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_58"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id334c961f"
      uniprot "NA"
    ]
    graphics [
      x 223.30402649982773
      y 1154.9657760054242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idcdf3e573"
      uniprot "NA"
    ]
    graphics [
      x 189.23244741256667
      y 1269.9277830558499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; WP5038; WP5039; C19DMap:PAMP signalling; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:wikidata:Q2819370; NA"
      hgnc "NA"
      map_id "dsRNA"
      name "dsRNA"
      node_subtype "RNA; UNKNOWN"
      node_type "species"
      org_id "a90b0; ff0ff; cb56c; sa6; sa26; sa27; sa456; sa93; sa82"
      uniprot "NA"
    ]
    graphics [
      x 198.91950957428946
      y 1454.5015649507086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dsRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_71"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ida0444a3e"
      uniprot "NA"
    ]
    graphics [
      x 213.4270799594974
      y 1588.3672530779768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "MERS_space_4a"
      name "MERS_space_4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d43bc"
      uniprot "NA"
    ]
    graphics [
      x 130.68657292930516
      y 1662.4636144789001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MERS_space_4a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4846; WP4861; WP4868; WP4880; C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "urn:miriam:wikidata:Q87917579; urn:miriam:ncbiprotein:YP_009725310; NA"
      hgnc "NA"
      map_id "nsp15"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205; fa46a; beaa5; c625e; glyph52"
      uniprot "NA"
    ]
    graphics [
      x 310.882474355624
      y 1228.838964985935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nsp15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18848; urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272; urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "NA; HGNC_SYMBOL:ATF4"
      map_id "UNIPROT:P18848"
      name "ATF4"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "e01f1; path_0_sa105; path_0_sa106; path_0_sa104"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 674.6267059618178
      y 899.791558581818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18848"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1f373365"
      uniprot "NA"
    ]
    graphics [
      x 747.5762060124413
      y 681.1330011267568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "CRE_br_cAMP_space_response_space_element"
      name "CRE_br_cAMP_space_response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "d0da6"
      uniprot "NA"
    ]
    graphics [
      x 815.9759545209254
      y 472.56920712144387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CRE_br_cAMP_space_response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5031f464"
      uniprot "NA"
    ]
    graphics [
      x 879.2905642527833
      y 327.27732349921735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "Amino_space_acid_space_synthesis_br_Antioxidant_space_response_br_Apoptosis"
      name "Amino_space_acid_space_synthesis_br_Antioxidant_space_response_br_Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c90ec"
      uniprot "NA"
    ]
    graphics [
      x 949.8139937504841
      y 218.95382357107826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Amino_space_acid_space_synthesis_br_Antioxidant_space_response_br_Apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "Global_space_protein_br_translation"
      name "Global_space_protein_br_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ed36d"
      uniprot "NA"
    ]
    graphics [
      x 518.336943202205
      y 1054.2338918380146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Global_space_protein_br_translation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "SARS,_space_OC43,_br_MHV,_space_IBV_space_infection"
      name "SARS,_space_OC43,_br_MHV,_space_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cd9a0"
      uniprot "NA"
    ]
    graphics [
      x 588.9762136366904
      y 809.1795766725708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS,_space_OC43,_br_MHV,_space_IBV_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "f61a6"
      name "f61a6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f61a6"
      uniprot "NA"
    ]
    graphics [
      x 1124.7471860606668
      y 857.0321140883773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f61a6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P17861; urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861; urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "NA; HGNC_SYMBOL:XBP1"
      map_id "UNIPROT:P17861"
      name "XBP1u; XBP1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "d3eb7; bbd1f; path_0_sa206; path_0_sa26; path_0_sa28; path_0_sa627; path_0_sa626"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 856.89440836914
      y 172.50912705850226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17861"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 16
      diagram "R-HSA-9694516; WP4846; WP4861; WP5038; C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4; urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4; urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4; urn:miriam:uniprot:P0DTC4; urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "NA; HGNC_SYMBOL:E"
      map_id "UNIPROT:P0DTC4"
      name "nascent_space_E; N_minus_glycan_space_E; Ub_minus_3xPalmC_minus_E; Ub_minus_3xPalmC_minus_E_space_pentamer; 3xPalmC_minus_E; envelope_br_protein; SARS_space_E; Envelope_space_Protein_space_E; E"
      node_subtype "PROTEIN; COMPLEX; GENE"
      node_type "species"
      org_id "layout_2335; layout_2405; layout_2410; layout_2412; layout_2407; layout_2414; aa466; fce54; c7c21; sa2062; sa2115; sa2023; sa2065; sa1687; sa1892; sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1157.3999465236107
      y 248.61668611668802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_68"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id858a8309"
      uniprot "NA"
    ]
    graphics [
      x 711.452105044109
      y 199.03629880342362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id906309ce"
      uniprot "NA"
    ]
    graphics [
      x 699.722509783343
      y 118.18729599397784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_64"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id7f19c7ea"
      uniprot "NA"
    ]
    graphics [
      x 821.1536247517482
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      name "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b2b"
      uniprot "NA"
    ]
    graphics [
      x 574.8351064668825
      y 195.78698133592934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idad83fc4d"
      uniprot "NA"
    ]
    graphics [
      x 484.23287566681387
      y 326.45921257368565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "ER_space_protein_space_chaperones_br_Lipid_space_biosynthesis_br_ER_minus_associated_space_degradation"
      name "ER_space_protein_space_chaperones_br_Lipid_space_biosynthesis_br_ER_minus_associated_space_degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c085b"
      uniprot "NA"
    ]
    graphics [
      x 463.88203088826845
      y 466.8943133996899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ER_space_protein_space_chaperones_br_Lipid_space_biosynthesis_br_ER_minus_associated_space_degradation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "f5b2d"
      name "f5b2d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5b2d"
      uniprot "NA"
    ]
    graphics [
      x 1142.2127352178466
      y 552.2579186653782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f5b2d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "Unfolded_br_protein"
      name "Unfolded_br_protein"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b00dc"
      uniprot "NA"
    ]
    graphics [
      x 1449.1228173473464
      y 480.8364602510941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Unfolded_br_protein"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P11021;urn:miriam:uniprot:O75460"
      hgnc "NA"
      map_id "UNIPROT:P11021;UNIPROT:O75460"
      name "d38f8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d38f8"
      uniprot "UNIPROT:P11021;UNIPROT:O75460"
    ]
    graphics [
      x 951.4417932919707
      y 775.3943201551933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11021;UNIPROT:O75460"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 64
    source 1
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "W3_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 1
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2"
      target_id "W3_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 63
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021;UNIPROT:O75460"
      target_id "W3_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 2
    target 36
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_67"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 4
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "W3_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 5
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_78"
      target_id "UNIPROT:O75460"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 4
    target 6
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75460"
      target_id "W3_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 4
    target 7
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 4
    target 8
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75460"
      target_id "W3_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 62
    target 5
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "Unfolded_br_protein"
      target_id "W3_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 61
    target 6
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "f5b2d"
      target_id "W3_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 6
    target 52
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_54"
      target_id "f61a6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 53
    target 7
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 54
    target 7
    cd19dm [
      diagram "WP4861"
      edge_type "INHIBITION"
      source_id "UNIPROT:P0DTC4"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 8
    target 9
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_77"
      target_id "UNIPROT:P45983"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 9
    target 10
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P45983"
      target_id "W3_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 10
    target 11
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_52"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 12
    target 11
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_73"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 13
    target 11
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_62"
      target_id "Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 52
    target 12
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "f61a6"
      target_id "W3_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 14
    target 13
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638"
      target_id "W3_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 15
    target 14
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_80"
      target_id "UNIPROT:P35638"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 14
    target 16
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638"
      target_id "W3_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 14
    target 17
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35638"
      target_id "W3_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 45
    target 15
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18848"
      target_id "W3_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 16
    target 22
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_55"
      target_id "UNIPROT:O75807"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 17
    target 18
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_86"
      target_id "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 18
    target 19
    cd19dm [
      diagram "WP4861"
      edge_type "PHYSICAL_STIMULATION"
      source_id "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
      target_id "W3_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 20
    target 19
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "BCL2"
      target_id "W3_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 19
    target 21
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_57"
      target_id "UNIPROT:P10415"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 22
    target 23
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75807"
      target_id "W3_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 23
    target 24
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_87"
      target_id "UNIPROT:Q53XC0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 25
    target 24
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_88"
      target_id "UNIPROT:Q53XC0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 24
    target 26
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q53XC0"
      target_id "W3_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 24
    target 27
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q53XC0"
      target_id "W3_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 24
    target 28
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q53XC0"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 51
    target 25
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_OC43,_br_MHV,_space_IBV_space_infection"
      target_id "W3_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 26
    target 50
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_70"
      target_id "Global_space_protein_br_translation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 27
    target 45
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_72"
      target_id "UNIPROT:P18848"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 29
    target 28
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P19525"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 30
    target 28
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9H492;UNIPROT:Q9BQI3"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 31
    target 28
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NZJ5"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 39
    target 29
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_58"
      target_id "UNIPROT:P19525"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 29
    target 40
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19525"
      target_id "W3_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 31
    target 32
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZJ5"
      target_id "W3_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 33
    target 32
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_IBV_space_infection"
      target_id "W3_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 33
    target 34
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "SARS,_space_IBV_space_infection"
      target_id "W3_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 35
    target 34
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11021;UNIPROT:Q9NZJ5"
      target_id "W3_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 34
    target 36
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_79"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 37
    target 36
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_60"
      target_id "UNIPROT:P11021"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 38
    target 37
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850;UNIPROT:P11021"
      target_id "W3_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 44
    target 39
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "nsp15"
      target_id "W3_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 41
    target 40
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "dsRNA"
      target_id "W3_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 42
    target 41
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_71"
      target_id "dsRNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 43
    target 42
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "MERS_space_4a"
      target_id "W3_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 45
    target 46
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18848"
      target_id "W3_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 46
    target 47
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_56"
      target_id "CRE_br_cAMP_space_response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 47
    target 48
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "CRE_br_cAMP_space_response_space_element"
      target_id "W3_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 48
    target 49
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_61"
      target_id "Amino_space_acid_space_synthesis_br_Antioxidant_space_response_br_Apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 53
    target 55
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "W3_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 53
    target 56
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "W3_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 53
    target 57
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17861"
      target_id "W3_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 55
    target 58
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_68"
      target_id "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 56
    target 58
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_69"
      target_id "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 58
    target 59
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      target_id "W3_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 59
    target 60
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_74"
      target_id "ER_space_protein_space_chaperones_br_Lipid_space_biosynthesis_br_ER_minus_associated_space_degradation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
