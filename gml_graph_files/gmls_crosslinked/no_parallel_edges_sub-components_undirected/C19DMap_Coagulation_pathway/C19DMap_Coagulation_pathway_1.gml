# generated with VANTED V2.8.2 at Fri Mar 04 10:06:53 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 30
      diagram "R-HSA-9678108; R-HSA-9694516; WP4846; WP4799; WP5038; WP4868; C19DMap:Renin-angiotensin pathway; C19DMap:Virus replication cycle; C19DMap:Endoplasmatic Reticulum stress; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480; urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499; urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1; urn:miriam:uniprot:Q9BYF1; urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2; urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "NA; HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:Q9BYF1"
      name "glycosylated_minus_ACE2; glycosylated_minus_ACE2:ACE2_space_inhibitors; ACE2; ACE2,_space_soluble; ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN; COMPLEX; GENE; RNA"
      node_type "species"
      org_id "layout_713; layout_2065; layout_836; layout_2067; layout_3279; layout_2491; layout_3347; layout_2484; e154d; ffb2b; d051e; a23f4; e92a9; aaf33; sa168; sa30; sa98; sa73; sa31; sa2239; sa2238; sa1462; sa1545; path_1_sa145; sa277; sa278; path_1_sa178; path_1_sa180; sa398; sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 593.2147216420171
      y 1051.1852528544782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:28116710"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_69"
      name "PMID:20689271"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re306"
      uniprot "NA"
    ]
    graphics [
      x 464.6758422717661
      y 1150.9209287386427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_68"
      name "PMID:32275855"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re305"
      uniprot "NA"
    ]
    graphics [
      x 524.3200021341802
      y 647.316625874482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_70"
      name "PMID:23392115, PMID:32336612"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re307"
      uniprot "NA"
    ]
    graphics [
      x 491.20909084476096
      y 1267.2430875589243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:16008552"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_67"
      name "PMID:32336612, PMID:16008552"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re304"
      uniprot "NA"
    ]
    graphics [
      x 797.5022870142473
      y 1179.4743689830798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:C000657245; urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_infection"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa165; sa207; sa499; sa480; sa481"
      uniprot "NA"
    ]
    graphics [
      x 1021.7459901556576
      y 1177.1974967953443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_78"
      name "PMICID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re324"
      uniprot "NA"
    ]
    graphics [
      x 1264.892553358128
      y 919.0178444110134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:22449964"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_39"
      name "PMID:22449964"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 767.228193159198
      y 1338.7488503107656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:28228446;PUBMED:6282863;PUBMED:2117226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_46"
      name "PMID:28228446, PMID: 6282863, PMID:2117226 "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re234"
      uniprot "NA"
    ]
    graphics [
      x 1394.106599038205
      y 1307.0133886562876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_86"
      name "PMCID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re334"
      uniprot "NA"
    ]
    graphics [
      x 1010.5688197616862
      y 1346.5170670660164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_85"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re333"
      uniprot "NA"
    ]
    graphics [
      x 733.1919358032725
      y 1136.1584027323022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_77"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re323"
      uniprot "NA"
    ]
    graphics [
      x 1055.678950839347
      y 1430.2733121469364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32504360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_95"
      name "PMID:32504360"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re344"
      uniprot "NA"
    ]
    graphics [
      x 960.7208405329134
      y 1158.7810067460018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32171076"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_73"
      name "PMID:32171076"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re315"
      uniprot "NA"
    ]
    graphics [
      x 1234.6361206846136
      y 844.5935308221104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_81"
      name "DOI:10.1101/2020.04.25.20077842"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re328"
      uniprot "NA"
    ]
    graphics [
      x 868.8415944053448
      y 1431.7380753223533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_128"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re396"
      uniprot "NA"
    ]
    graphics [
      x 811.4615883361737
      y 1403.219046700703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_116"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re375"
      uniprot "NA"
    ]
    graphics [
      x 664.1391479185631
      y 1295.1329056763389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:173529;PUBMED:8404594;PUBMED:32565254"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re376"
      uniprot "NA"
    ]
    graphics [
      x 398.9264619598664
      y 1305.21715135543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_87"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re336"
      uniprot "NA"
    ]
    graphics [
      x 893.0661605983046
      y 923.9892117775194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_65"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re295"
      uniprot "NA"
    ]
    graphics [
      x 926.5213504929277
      y 1468.3593019702935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_41"
      name "PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 1290.6099418347856
      y 1353.3253281682503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_79"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re325"
      uniprot "NA"
    ]
    graphics [
      x 813.8935333831018
      y 805.5062980672462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:10574983;PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_97"
      name "PMID:29096812, PMID:10574983, PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re346"
      uniprot "NA"
    ]
    graphics [
      x 1294.0640352586972
      y 762.3177742916989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32359396"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_84"
      name "PMID:32359396"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re332"
      uniprot "NA"
    ]
    graphics [
      x 1091.420750350613
      y 943.9405451326619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_74"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re319"
      uniprot "NA"
    ]
    graphics [
      x 1259.5209027155333
      y 1054.8027904729483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:8136018"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_36"
      name "PMID: 8136018"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re179"
      uniprot "NA"
    ]
    graphics [
      x 1218.533673674716
      y 1089.2044305751222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_88"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re337"
      uniprot "NA"
    ]
    graphics [
      x 950.4484325963648
      y 896.0354080445236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776;PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_57"
      name "PMID:11290788, PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 1201.801916378398
      y 756.5347627068797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_66"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re296"
      uniprot "NA"
    ]
    graphics [
      x 1088.3754042646776
      y 1081.0564738775079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:2437112"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_80"
      name "PMID:2437112, DOI:10.1101/2020.04.25.20077842"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re326"
      uniprot "NA"
    ]
    graphics [
      x 891.2105565625677
      y 860.8886827081651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4927; C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "NA; HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P00747"
      name "Plasmin; PLG; Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1440; sa1433; sa1431; sa211; sa212; sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1030.5030241519821
      y 698.419295827117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf3a protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2; urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "UNIPROT:P08697"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86; sa87; sa412"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 752.2176931782863
      y 803.4269940038729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08697"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2437112;urn:miriam:hgnc:9075;urn:miriam:mesh:D005341;urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:SERPINF2;HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P08697;UNIPROT:P00747"
      name "SERPINF2:Plasmin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      x 744.6848829483026
      y 857.184519959167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08697;UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:9012652"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_92"
      name "PMID:9012652"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re341"
      uniprot "NA"
    ]
    graphics [
      x 1221.6328047882166
      y 554.1089598135579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:3850647;PUBMED:89876;PUBMED:6539333;PUBMED:2966802"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_35"
      name "PMID: 89876, PMID:385647"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re178"
      uniprot "NA"
    ]
    graphics [
      x 883.4687525780763
      y 779.3675222544474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:27077125"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_94"
      name "PMID:27077125"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re343"
      uniprot "NA"
    ]
    graphics [
      x 745.796045656002
      y 934.6216882794348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:27077125;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_237"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa467"
      uniprot "NA"
    ]
    graphics [
      x 623.2017205134562
      y 880.8945997327677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727; urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727; urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:mesh:D015936;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "UNIPROT:P01031"
      name "C5; C5b; C5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314; sa315; sa253"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 839.351992792581
      y 1273.4364174785028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:30083158;PUBMED:12878586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_51"
      name "PMID:30083158, PMID: 12878586"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 716.0045917689406
      y 988.4454484370452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_52"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 1017.0960460224235
      y 1576.9921824230091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C6"
      map_id "UNIPROT:P13671"
      name "C6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318"
      uniprot "UNIPROT:P13671"
    ]
    graphics [
      x 1027.2495282079312
      y 1717.110886724896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D050776;urn:miriam:mesh:C050974;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C5"
      map_id "UNIPROT:P13671;UNIPROT:P01031"
      name "C5b:C6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      x 1216.6472939376463
      y 1618.7739661931348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671;UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_53"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re264"
      uniprot "NA"
    ]
    graphics [
      x 1420.0388445429935
      y 1706.513997206065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C7"
      map_id "UNIPROT:P10643"
      name "C7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P10643"
    ]
    graphics [
      x 1507.1445899356445
      y 1665.2943454511262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28630159;urn:miriam:mesh:C037453;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C6"
      map_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
      name "C5b:C6:C7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      x 1617.8510015413972
      y 1630.2498683227518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:284414"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_54"
      name "PMID:284414"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re266"
      uniprot "NA"
    ]
    graphics [
      x 1736.6780865834082
      y 1466.490284000014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:mesh:D003185;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C8B"
      map_id "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
      name "C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    ]
    graphics [
      x 1638.4111674708188
      y 1500.7025415596572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:pubmed:28630159;urn:miriam:hgnc:1354;urn:miriam:mesh:C042295;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C8B;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C6;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7"
      map_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
      name "C5b:C6:C7:C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      x 1686.0466780880438
      y 1253.6300057646035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:6796960"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_55"
      name "PMID:6796960"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 1525.1471285522957
      y 1005.3972988819235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748"
      hgnc "HGNC_SYMBOL:C9"
      map_id "UNIPROT:P02748"
      name "C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:P02748"
    ]
    graphics [
      x 1543.1067039892096
      y 1170.132959506502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02748"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8B"
      map_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      x 1192.6234293385492
      y 666.8485115255627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_64"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re292"
      uniprot "NA"
    ]
    graphics [
      x 792.7419277906495
      y 526.1359072008192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_93"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re342"
      uniprot "NA"
    ]
    graphics [
      x 899.8517127702514
      y 554.9197362247263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_72"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re309"
      uniprot "NA"
    ]
    graphics [
      x 1093.4366000223085
      y 462.30141946127344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S; urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38; sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 735.3447512426347
      y 446.4643414219941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:C8B;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C6;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      x 1195.4570266875894
      y 356.3385373890994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_71"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re308"
      uniprot "NA"
    ]
    graphics [
      x 627.9546593280014
      y 346.1344206269582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4b;urn:miriam:mesh:C032261;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5; urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4b;HGNC_SYMBOL:C4B; HGNC_SYMBOL:C4B"
      map_id "UNIPROT:P0C0L5"
      name "C4d; C4b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa387; sa365"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 609.4019206627132
      y 502.72650981315456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:21664989"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_59"
      name "PMID:22949645"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 986.6918568659918
      y 331.015978770768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_91"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re340"
      uniprot "NA"
    ]
    graphics [
      x 571.6989455243408
      y 414.08268505393255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_63"
      name "PMID: 32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 332.3653988537873
      y 813.6417315674552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_60"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re278"
      uniprot "NA"
    ]
    graphics [
      x 747.3113384010705
      y 356.6579448223638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_62"
      name "PMID:19362461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 348.974472896391
      y 585.6322065761474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.46;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2771;urn:miriam:hgnc.symbol:CFD;urn:miriam:hgnc.symbol:CFD;urn:miriam:refseq:NM_001928;urn:miriam:ensembl:ENSG00000197766;urn:miriam:uniprot:P00746;urn:miriam:uniprot:P00746;urn:miriam:ncbigene:1675;urn:miriam:ncbigene:1675"
      hgnc "HGNC_SYMBOL:CFD"
      map_id "UNIPROT:P00746"
      name "CFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P00746"
    ]
    graphics [
      x 203.4034802242728
      y 753.8636108478057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00746"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717; urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:hgnc.symbol:C2;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717; urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050679;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "UNIPROT:P06681"
      name "C2a; C2; C2b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa367; sa366; sa368"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 940.3337734909526
      y 170.265744949863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06681"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D051574;urn:miriam:taxonomy:9606;urn:miriam:brenda:3.4.21.43;urn:miriam:mesh:D050678;urn:miriam:hgnc:1324;urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5;urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C4B;HGNC_SYMBOL:C2"
      map_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      name "C2a:C4b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      x 731.5223753541567
      y 623.2407695094423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L5;UNIPROT:P06681"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:17395591;PUBMED:427127"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_50"
      name "PMID:427127, PMID:17395591"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 916.3480750300132
      y 657.230418905522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718; urn:miriam:hgnc.symbol:C3;urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718; urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:mesh:D015926;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "UNIPROT:P01024"
      name "C3b; C3; C3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa310; sa252; sa308"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 1078.472482079476
      y 727.4556156424679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01024"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D003179;urn:miriam:brenda:3.4.21.47;urn:miriam:taxonomy:9606;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:mesh:D051561;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629"
      hgnc "HGNC_SYMBOL:C3;HGNC_SYMBOL:CFB"
      map_id "UNIPROT:P01024;UNIPROT:P00751"
      name "C3b:Bb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P01024;UNIPROT:P00751"
    ]
    graphics [
      x 959.8497909170882
      y 769.1723586098353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01024;UNIPROT:P00751"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_61"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 1002.4967242635983
      y 899.0466006539914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:34.4.21.47;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:mesh:D051566;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:CFB;HGNC_SYMBOL:C3"
      map_id "UNIPROT:P00751;UNIPROT:P01024"
      name "C3b:Bb:C3b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      x 859.6332456682853
      y 1034.4810671648977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00751;UNIPROT:P01024"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:10946292"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_89"
      name "PMID:10946292"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re338"
      uniprot "NA"
    ]
    graphics [
      x 1124.1489122482608
      y 67.33970062660228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "UNIPROT:P48740"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa458; sa358"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 1307.3150112634494
      y 88.86018814502086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P48740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_58"
      name "PMID:11290788"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 1379.3522535048623
      y 220.70292496033414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "UNIPROT:O00187"
      name "MBL2; MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362; sa459; sa357"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 1270.0144860179246
      y 398.1222020705836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_90"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re339"
      uniprot "NA"
    ]
    graphics [
      x 1312.4582060892471
      y 253.25174865739052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "MASP2_space_deposition"
      name "MASP2_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa417"
      uniprot "NA"
    ]
    graphics [
      x 1240.8078572837596
      y 153.4199243191773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MASP2_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "C4d_space_deposition"
      name "C4d_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa388"
      uniprot "NA"
    ]
    graphics [
      x 189.85126969325302
      y 1117.5242640927686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C4d_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_106"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re361"
      uniprot "NA"
    ]
    graphics [
      x 210.56108663605244
      y 1392.4140809415871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D007681"
      hgnc "NA"
      map_id "septal_space_capillary_space_necrosis"
      name "septal_space_capillary_space_necrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa389"
      uniprot "NA"
    ]
    graphics [
      x 391.3643451994591
      y 1549.3032694792548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "septal_space_capillary_space_necrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "Thrombosis"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271; sa529"
      uniprot "NA"
    ]
    graphics [
      x 667.3423150666629
      y 573.9353362341303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Thrombosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_43"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re227"
      uniprot "NA"
    ]
    graphics [
      x 814.3888020879053
      y 586.3974078477371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_45"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re231"
      uniprot "NA"
    ]
    graphics [
      x 666.5648054815977
      y 780.6370409019587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_44"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re229"
      uniprot "NA"
    ]
    graphics [
      x 543.937045589288
      y 762.2588125821815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:9490235"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_75"
      name "PMID:9490235"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re320"
      uniprot "NA"
    ]
    graphics [
      x 946.0131467702835
      y 536.6079854797227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_48"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 591.3044684453901
      y 612.4247285682785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:20483636"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_49"
      name "PMID:20483636"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 795.0362712569796
      y 472.5428788436125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:26709040"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_125"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re393"
      uniprot "NA"
    ]
    graphics [
      x 411.0836080703836
      y 784.8969626200458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:20975035"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_112"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re371"
      uniprot "NA"
    ]
    graphics [
      x 520.4490479932319
      y 861.4977929337718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:3165516"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_124"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re392"
      uniprot "NA"
    ]
    graphics [
      x 497.5682853997603
      y 538.9008192078506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:3500650"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_76"
      name "PMID:3500650"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re321"
      uniprot "NA"
    ]
    graphics [
      x 1033.4955753315426
      y 635.0817364832078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "UNIPROT:P04275"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa411; sa472"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 1313.161053043866
      y 855.9627940879583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_103"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re358"
      uniprot "NA"
    ]
    graphics [
      x 1439.513227177072
      y 1051.8729188448397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:GP6;HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
      name "GP6:alpha2:beta1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 1654.8338342494628
      y 990.7738992900556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:hgnc:12726;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:GP6;HGNC_SYMBOL:VWF;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      name "GP6:alpha2beta1:VWF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      x 1211.2017872052475
      y 1188.9309000065825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:19286885"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_98"
      name "PMID:19286885"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re352"
      uniprot "NA"
    ]
    graphics [
      x 1155.9914473452468
      y 1046.6075992994536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_99"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re353"
      uniprot "NA"
    ]
    graphics [
      x 891.1148992468458
      y 1181.7803736806482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_activation"
      name "platelet_space_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa430"
      uniprot "NA"
    ]
    graphics [
      x 560.2370870642503
      y 1098.8150133375173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_activation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_101"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re356"
      uniprot "NA"
    ]
    graphics [
      x 660.1802313463174
      y 1009.2355274029496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961;PUBMED:19465929"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_100"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re355"
      uniprot "NA"
    ]
    graphics [
      x 336.2119815721158
      y 983.637020746903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "thrombus_space_formation"
      name "thrombus_space_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa441"
      uniprot "NA"
    ]
    graphics [
      x 237.88515893795102
      y 797.2896131431719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "thrombus_space_formation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570;PUBMED:21789389"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re365"
      uniprot "NA"
    ]
    graphics [
      x 139.53103210620486
      y 686.0813401011119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:16391415"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_111"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re369"
      uniprot "NA"
    ]
    graphics [
      x 215.69075929286896
      y 907.8094532097787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 11
      diagram "WP4799; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P30556; urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "NA; HGNC_SYMBOL:AGTR1"
      map_id "UNIPROT:P30556"
      name "AT1R; AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab2a6; sa102; sa167; sa139; sa140; sa26; sa204; sa500; sa484; sa519; sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 261.9495916428741
      y 1074.068127979512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:8158359"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_110"
      name "PMID:8158359"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re366"
      uniprot "NA"
    ]
    graphics [
      x 248.5093989792838
      y 1189.2713302509815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:20591974;PUBMED:8034668;PUBMED:11983698;PUBMED:2091055"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re377"
      uniprot "NA"
    ]
    graphics [
      x 367.9421123286792
      y 1207.593899007322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:11983698"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re386"
      uniprot "NA"
    ]
    graphics [
      x 194.44769195002903
      y 1212.054424179781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:21349712;PUBMED:7045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re382"
      uniprot "NA"
    ]
    graphics [
      x 333.293983975283
      y 889.1276796310202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "UNIPROT:P08235"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa506; sa516"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 449.0081459529565
      y 678.7125978164047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P08235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "aldosterone"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195; sa509; sa503"
      uniprot "NA"
    ]
    graphics [
      x 436.8910238593397
      y 1058.9347070051467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "aldosterone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_126"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re394"
      uniprot "NA"
    ]
    graphics [
      x 789.8637508346598
      y 906.5440258569118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:8202152"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_119"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re379"
      uniprot "NA"
    ]
    graphics [
      x 531.1944505175472
      y 965.5909082537071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:5932931"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re391"
      uniprot "NA"
    ]
    graphics [
      x 475.34297084144646
      y 808.4388067596647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_127"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re395"
      uniprot "NA"
    ]
    graphics [
      x 248.89153924845664
      y 1010.3422365966443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:32252108"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_130"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re399"
      uniprot "NA"
    ]
    graphics [
      x 536.072091781308
      y 1298.7401909474806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D007008;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Hypokalemia"
      name "Hypokalemia"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa531"
      uniprot "NA"
    ]
    graphics [
      x 600.8976804270192
      y 1497.1738774721457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Hypokalemia"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re398"
      uniprot "NA"
    ]
    graphics [
      x 453.9731632965096
      y 1503.8361756073814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_271"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa532"
      uniprot "NA"
    ]
    graphics [
      x 451.5731680016326
      y 1298.8712140991745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:E protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "K_plus_"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa65; sa66; sa521"
      uniprot "NA"
    ]
    graphics [
      x 319.8795899041699
      y 1495.2533781174188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "K_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "vascular_space_inflammation"
      name "vascular_space_inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa530"
      uniprot "NA"
    ]
    graphics [
      x 128.8987971223512
      y 968.100395488178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vascular_space_inflammation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_266"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 401.6880762766182
      y 678.9900560243266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 7
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P12821; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640; urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "NA; HGNC_SYMBOL:ACE"
      map_id "UNIPROT:P12821"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e0c12; sa29; sa146; sa28; sa100; sa199; sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 620.3204195914291
      y 701.4127792911197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12821"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042;PUBMED:190881"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_25"
      name "PMID:19065996"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 419.8045879469116
      y 864.1505897606912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_115"
      name "PMID:10749699"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re374"
      uniprot "NA"
    ]
    graphics [
      x 938.3561787698136
      y 608.9388547695451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hmdb:HMDB0004246; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A3165"
      hgnc "NA"
      map_id "Bradykinin"
      name "Bradykinin"
      node_subtype "SIMPLE_MOLECULE; PROTEIN"
      node_type "species"
      org_id "bc2f2; sa402"
      uniprot "NA"
    ]
    graphics [
      x 805.5973433847516
      y 731.3423536919935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bradykinin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C079000;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Bradykinin(1_minus_5)"
      name "Bradykinin(1_minus_5)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 1002.3097606818808
      y 444.1998686507094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bradykinin(1_minus_5)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "Small_space_peptide"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa183; sa171; sa178; sa397"
      uniprot "NA"
    ]
    graphics [
      x 1136.496207544953
      y 779.2033194800558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Small_space_peptide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:15746105"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_122"
      name "NA"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re390"
      uniprot "NA"
    ]
    graphics [
      x 1117.4891628890991
      y 1187.657143408827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:7391081;PUBMED:864009"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_26"
      name "PMID:7391081"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1057.5599893835501
      y 506.59699098878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:15853774;PUBMED:11551226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_24"
      name "PMID:11551226"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 1226.6373635834714
      y 979.1584138755373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159; urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "UNIPROT:P00742"
      name "F10a; F10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa176; sa177"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 1244.7549171019043
      y 1144.6707003541187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00742"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F9"
      map_id "UNIPROT:P00740"
      name "F9a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P00740"
    ]
    graphics [
      x 1148.6510578929922
      y 1127.6770659681238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:SERPINC1"
      map_id "UNIPROT:P01008"
      name "Antithrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202"
      uniprot "UNIPROT:P01008"
    ]
    graphics [
      x 1338.097314378596
      y 1095.6127723622208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01008"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "PUBMED:579490"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_34"
      name "PMID:579490"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 1480.0266842308297
      y 1178.045414911611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4927; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734; urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147; urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "NA; HGNC_SYMBOL:F2"
      map_id "UNIPROT:P00734"
      name "Thrombin; Prothrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "af109; sa181; sa498; sa203; sa182"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 1327.0640794730234
      y 1180.002859321802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28304;urn:miriam:taxonomy:9606;urn:miriam:pubmed:708377"
      hgnc "NA"
      map_id "Heparin"
      name "Heparin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 1619.553059682964
      y 1204.262392246213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Heparin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:8388351;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_47"
      name "PMID:8388351"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 1083.9349818122446
      y 1362.0850211386837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:23809134"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_38"
      name "PMID:23809134"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 1458.3887196388578
      y 985.4977697186697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106;PUBMED:8631976"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_27"
      name "PMID:21304106, PMID:8631976"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 1110.6341853858003
      y 833.7737986991723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "PUBMED:4430674;PUBMED:3818642"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_33"
      name "PMID:4430674,PMID:3818642"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 1335.7390900306382
      y 1302.674650970759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D015951;urn:miriam:pubmed:2303476;urn:miriam:mesh:D15943;urn:miriam:mesh:C022475;urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159;urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F10;HGNC_SYMBOL:F5"
      map_id "UNIPROT:P00742;UNIPROT:P12259"
      name "F5a:F10a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      x 1217.0403062555952
      y 1324.0678206349821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00742;UNIPROT:P12259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:2303476"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_32"
      name "PMID:2303476"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 1095.6869495271317
      y 1256.8116624484487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F5"
      map_id "UNIPROT:P12259"
      name "F5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201"
      uniprot "UNIPROT:P12259"
    ]
    graphics [
      x 892.2566324068546
      y 1236.6094421221655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:2322551;PUBMED:6572921;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_31"
      name "PMID:2322551, PMID:6282863, PMID:6572921"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re171"
      uniprot "NA"
    ]
    graphics [
      x 900.4635242248369
      y 1363.814143543832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3541;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F3;urn:miriam:refseq:NM_001993;urn:miriam:hgnc.symbol:F3;urn:miriam:ncbigene:2152;urn:miriam:ncbigene:2152;urn:miriam:ensembl:ENSG00000117525;urn:miriam:uniprot:P13726;urn:miriam:uniprot:P13726"
      hgnc "HGNC_SYMBOL:F3"
      map_id "UNIPROT:P13726"
      name "F5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa200"
      uniprot "UNIPROT:P13726"
    ]
    graphics [
      x 771.5143887203949
      y 1250.4527412961884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13726"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:11784;urn:miriam:taxonomy:9986;urn:miriam:pubmed:6282863;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:THBD;HGNC_SYMBOL:F2"
      map_id "UNIPROT:P07204;UNIPROT:P00734"
      name "Thrombin:Thrombomodulin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      x 1148.698183546799
      y 1445.4936273521644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07204;UNIPROT:P00734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "UNIPROT:P04070"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205; sa482"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 936.7826943912391
      y 1411.519153191457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04070"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:3124286;PUBMED:3096399;PUBMED:12091055;PUBMED:10373228"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_37"
      name "PMID:10373228, PMID:3124286"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 639.9211515781348
      y 1169.9805503364123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930; urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "UNIPROT:P00750"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1432; sa213; sa225"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 798.2374916923634
      y 1095.1100762777523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "UNIPROT:P05121"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224; sa505"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 584.8657173557733
      y 1340.2184050294134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 4
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:obo.chebi:CHEBI%3A48432; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_II"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb; sa23; sa95; sa194"
      uniprot "NA"
    ]
    graphics [
      x 402.3082759034844
      y 1137.1757591009118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "angiotensin_space_I_minus_7"
      name "angiotensin_space_I_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa400"
      uniprot "NA"
    ]
    graphics [
      x 313.5437093981251
      y 1254.9331182165251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_108"
      name "PMID:18026570"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re364"
      uniprot "NA"
    ]
    graphics [
      x 144.32725693095222
      y 1056.708478542963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "UNIPROT:P04201"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa77; sa141; sa496; sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 88.0914037016729
      y 844.716044367181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P04201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_40"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 361.4329834117175
      y 928.2591590615077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "angiotensin_space_I"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a; sa21; sa195"
      uniprot "NA"
    ]
    graphics [
      x 327.3169627244637
      y 677.5520454868674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461;PUBMED:6172448;PUBMED:30934934"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_23"
      name "PMID:10585461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 379.41271704599285
      y 373.1696185607102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029; urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "UNIPROT:P01019"
      name "AGT"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "sa34; sa174; sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 368.6210448149277
      y 231.87779483419524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01019"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15; urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "UNIPROT:P00797"
      name "REN; Prorenin"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "sa36; sa35; sa71; sa415; sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 508.50067998439107
      y 212.74571315012918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00797"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:692685"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_83"
      name "PMID:692685"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re330"
      uniprot "NA"
    ]
    graphics [
      x 692.319582352992
      y 186.97813826299614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1; urn:miriam:brenda:3.4.21.34;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:mesh:D020842;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "UNIPROT:P03952"
      name "KLKB1; Kallikrein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167; sa251"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 899.2780205823784
      y 294.1893964840735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P03952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:4627469;PUBMED:6768384"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_42"
      name "PMID:6768384, PMID:4627469"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 792.9711537037575
      y 279.95564095762774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_82"
      name "PMID:21304106"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re329"
      uniprot "NA"
    ]
    graphics [
      x 881.3950521148565
      y 367.6043399890175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "PUBMED:7944388"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_28"
      name "PMID:7944388"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 915.3787346839811
      y 75.85153276344556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889; urn:miriam:mesh:D019679;urn:miriam:hgnc.symbol:KNG1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383; HGNC_SYMBOL:KNG1"
      map_id "UNIPROT:P01042"
      name "KNG1; Kininogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169; sa250"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 813.775791230157
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01042"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:17598838;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-10087151;urn:miriam:hgnc:6371;urn:miriam:hgnc:6383;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383;HGNC_SYMBOL:KLKB1"
      map_id "UNIPROT:P01042;UNIPROT:P03952"
      name "KNG1:KLKB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      x 800.2954272131839
      y 128.0638803088941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01042;UNIPROT:P03952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:3530;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:mesh:D015956;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:brenda:3.4.21.38;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:ec-code:3.4.21.38; urn:miriam:hgnc:3530;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:hgnc.symbol:F12;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "UNIPROT:P00748"
      name "F12a; F12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165; sa170"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 971.070429087436
      y 489.9567329479345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00748"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:21199867"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_56"
      name "PMID:21199867"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 665.2360041363656
      y 1227.789248100869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "UNIPROT:P00749"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236; sa356"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 745.4584020515224
      y 1039.957031308235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00749"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:9066005"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_113"
      name "PMID:9066005"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re372"
      uniprot "NA"
    ]
    graphics [
      x 940.641427825907
      y 978.5403064605301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_248"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 1050.266891293666
      y 1116.8220657558395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:mesh:D015945;urn:miriam:hgnc.symbol:F11;urn:miriam:brenda:3.4.21.27;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128; urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "UNIPROT:P03951"
      name "F11a; F11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa173; sa172"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 1054.5288641249872
      y 1013.729395310724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P03951"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:9100000"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_29"
      name "PMID:9100000"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re169"
      uniprot "NA"
    ]
    graphics [
      x 1174.8175078782808
      y 1253.4972040933949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:RPS3AP29;urn:miriam:refseq:NG_011230;urn:miriam:ensembl:ENSG00000237818;urn:miriam:hgnc:35531;urn:miriam:ncbigene:730861"
      hgnc "HGNC_SYMBOL:RPS3AP29"
      map_id "F9"
      name "F9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa174"
      uniprot "NA"
    ]
    graphics [
      x 1224.0456127051532
      y 1414.255613156392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "UNIPROT:P50454"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226; sa227"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 1475.0579948804848
      y 811.4603242201957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50454"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD"
      hgnc "HGNC_SYMBOL:THBD"
      map_id "UNIPROT:P07204"
      name "Thrombomodulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa301"
      uniprot "UNIPROT:P07204"
    ]
    graphics [
      x 758.3452481432108
      y 1418.8402855669235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:22471307"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_30"
      name "PMID:22471307"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re170"
      uniprot "NA"
    ]
    graphics [
      x 937.6732058619123
      y 1090.4219702728747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010; urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "UNIPROT:P00451"
      name "F8; F8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa179; sa180"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 963.9476880529561
      y 1294.442077617692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00451"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:22471307;urn:miriam:intact:EBI-11621595;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3546;urn:miriam:hgnc:35531;urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010;urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F8;HGNC_SYMBOL:F9"
      map_id "UNIPROT:P00451;UNIPROT:P00740"
      name "F8:F9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      x 787.2184637216621
      y 961.2318394778331
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00451;UNIPROT:P00740"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0001974"
      hgnc "NA"
      map_id "vascular_space_remodeling"
      name "vascular_space_remodeling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa518"
      uniprot "NA"
    ]
    graphics [
      x 1039.4098298073463
      y 871.8939756919069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "vascular_space_remodeling"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:3569;urn:miriam:ncbigene:3569;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05231;urn:miriam:uniprot:P05231;urn:miriam:ensembl:ENSG00000136244;urn:miriam:hgnc:6018;urn:miriam:hgnc.symbol:IL6;urn:miriam:hgnc.symbol:IL6;urn:miriam:refseq:NM_000600"
      hgnc "HGNC_SYMBOL:IL6"
      map_id "UNIPROT:P05231"
      name "IL6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa242"
      uniprot "UNIPROT:P05231"
    ]
    graphics [
      x 498.5222901335565
      y 1022.4877214353652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:14388;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGB;HGNC_SYMBOL:FGG;HGNC_SYMBOL:GP6"
      map_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
      name "Fibrinogen:GP6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      x 894.1892115108861
      y 981.3604558040306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_107"
      name "PMID:29472360"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re363"
      uniprot "NA"
    ]
    graphics [
      x 1272.1691379246618
      y 1001.4052890030317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3662;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3661;urn:miriam:hgnc:3694;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGG;HGNC_SYMBOL:FGB"
      map_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      name "Fibrinogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 1403.161416745957
      y 1212.3695467872826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:GP6"
      map_id "UNIPROT:Q9HCN6"
      name "GP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa431"
      uniprot "UNIPROT:Q9HCN6"
    ]
    graphics [
      x 1541.5938916236728
      y 904.0528900789823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HCN6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_102"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re357"
      uniprot "NA"
    ]
    graphics [
      x 1747.6022701012419
      y 886.88641538958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-16428357;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "UNIPROT:P17301;UNIPROT:P05556"
      name "ITGA2:ITGAB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 1824.799414729443
      y 774.8224597471241
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17301;UNIPROT:P05556"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:19286885;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_223"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa440"
      uniprot "NA"
    ]
    graphics [
      x 1036.082695248881
      y 969.8122449391544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01589;urn:miriam:uniprot:P01589;urn:miriam:ncbigene:3559;urn:miriam:ncbigene:3559;urn:miriam:hgnc:6008;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:refseq:NM_000417;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:ensembl:ENSG00000134460"
      hgnc "HGNC_SYMBOL:IL2RA"
      map_id "UNIPROT:P01589"
      name "IL2RA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:P01589"
    ]
    graphics [
      x 855.8456128749485
      y 652.6874772343116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01589"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000584;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000169429;urn:miriam:hgnc:6025;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:uniprot:P10145;urn:miriam:uniprot:P10145;urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3576"
      hgnc "HGNC_SYMBOL:CXCL8"
      map_id "UNIPROT:P10145"
      name "IL8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P10145"
    ]
    graphics [
      x 715.9686673271241
      y 737.1460723300088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P10145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:1401;urn:miriam:ncbigene:1401;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2367;urn:miriam:hgnc.symbol:CRP;urn:miriam:uniprot:P02741;urn:miriam:uniprot:P02741;urn:miriam:hgnc.symbol:CRP;urn:miriam:ensembl:ENSG00000132693;urn:miriam:refseq:NM_000567"
      hgnc "HGNC_SYMBOL:CRP"
      map_id "UNIPROT:P02741"
      name "CRP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa410"
      uniprot "UNIPROT:P02741"
    ]
    graphics [
      x 1158.1837037442228
      y 617.5675627463834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02741"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Apoptosis pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810; urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:hgnc.symbol:TNF;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01375;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "UNIPROT:P01375"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3; sa238"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 817.7346619926327
      y 983.6597403050966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Orf3a protein interactions; C19DMap:NLRP3 inflammasome activation; C19DMap:Coagulation pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538; urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553; urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "UNIPROT:P01584"
      name "IL1b; proIL_minus_1B; IL_minus_1B; IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127; sa92; sa15; sa17; sa172; sa21; sa244; sa462; sa464; sa460; sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 954.9786583637967
      y 716.8827169706882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01584"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D00318"
      hgnc "NA"
      map_id "C4"
      name "C4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 1114.812037953222
      y 337.4302966296076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc:1323;urn:miriam:ensembl:ENSG00000244731;urn:miriam:uniprot:P0C0L4;urn:miriam:uniprot:P0C0L4;urn:miriam:refseq:NM_007293"
      hgnc "HGNC_SYMBOL:C4A"
      map_id "UNIPROT:P0C0L4"
      name "C4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P0C0L4"
    ]
    graphics [
      x 1094.2316821383251
      y 383.5456102643965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C0L4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "C5b_minus_9_space_deposition"
      name "C5b_minus_9_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa390"
      uniprot "NA"
    ]
    graphics [
      x 461.3829054127543
      y 426.1614038536698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "C5b_minus_9_space_deposition"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_104"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re359"
      uniprot "NA"
    ]
    graphics [
      x 262.87897573105136
      y 479.3108863571358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:10807586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_105"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re360"
      uniprot "NA"
    ]
    graphics [
      x 328.1044870838626
      y 299.82003161231694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006915"
      hgnc "NA"
      map_id "apoptosis"
      name "apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa408"
      uniprot "NA"
    ]
    graphics [
      x 273.2139789642267
      y 198.9311448489542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Renin-angiotensin pathway; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070527; urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "platelet_space_aggregation"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa209; sa409"
      uniprot "NA"
    ]
    graphics [
      x 208.39349145547487
      y 402.48910322598215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "platelet_space_aggregation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_249"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa495"
      uniprot "NA"
    ]
    graphics [
      x 1307.5850315686223
      y 648.2907852847449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32302438;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_241"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa479"
      uniprot "NA"
    ]
    graphics [
      x 918.9391596613638
      y 1129.4893132319676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_225"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa451"
      uniprot "NA"
    ]
    graphics [
      x 1024.8354529512646
      y 766.7358068279956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2504360"
      hgnc "NA"
      map_id "cytokine_space_storm"
      name "cytokine_space_storm"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa416"
      uniprot "NA"
    ]
    graphics [
      x 965.6320217974672
      y 1041.195487771164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cytokine_space_storm"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:mesh:C046193;urn:miriam:pubmed:22930518;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147;urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:F2;HGNC_SYMBOL:SERPINC1"
      map_id "UNIPROT:P00734;UNIPROT:P01008"
      name "TAT_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      x 1187.440686851365
      y 879.3400879452925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00734;UNIPROT:P01008"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_227"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa453"
      uniprot "NA"
    ]
    graphics [
      x 1179.2169052493464
      y 925.1934518096996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C465961"
      hgnc "NA"
      map_id "Fibrin_space_polymer"
      name "Fibrin_space_polymer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa424"
      uniprot "NA"
    ]
    graphics [
      x 1502.1438358740788
      y 704.3984025083844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrin_space_polymer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C036309;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "D_minus_dimer"
      name "D_minus_dimer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "NA"
    ]
    graphics [
      x 1358.6135383531341
      y 611.5910970020082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D_minus_dimer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:7577232"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_96"
      name "PMID:29096812, PMID:7577232"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re345"
      uniprot "NA"
    ]
    graphics [
      x 1645.8254247915527
      y 817.1663610384334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C011468;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "Fibrin_space_monomer"
      name "Fibrin_space_monomer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 1590.8827776320727
      y 1080.0311086224183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrin_space_monomer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D026122;urn:miriam:taxonomy:9606;urn:miriam:brenda:2.3.2.13;urn:miriam:hgnc.symbol:F13"
      hgnc "HGNC_SYMBOL:F13"
      map_id "F13a"
      name "F13a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa425"
      uniprot "NA"
    ]
    graphics [
      x 1634.0678951680713
      y 649.2089420654445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F13a"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_234"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa460"
      uniprot "NA"
    ]
    graphics [
      x 763.3632117592811
      y 680.0744687213212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_251"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa497"
      uniprot "NA"
    ]
    graphics [
      x 1381.0701624224052
      y 1467.538254340051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_231"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa457"
      uniprot "NA"
    ]
    graphics [
      x 992.4660920121248
      y 1408.1205714856142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_226"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa452"
      uniprot "NA"
    ]
    graphics [
      x 978.8311663941444
      y 830.3578543058829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_257"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa504"
      uniprot "NA"
    ]
    graphics [
      x 342.7409697623016
      y 1424.8790406008663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A3892;urn:miriam:hgnc:9201"
      hgnc "NA"
      map_id "ACTH"
      name "ACTH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 281.0588949108002
      y 1378.188261075124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACTH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_255"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa501"
      uniprot "NA"
    ]
    graphics [
      x 548.4199379763006
      y 1405.5907954048566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106;urn:miriam:doi:10.1101/2020.04.25.200"
      hgnc "NA"
      map_id "M121_240"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa478"
      uniprot "NA"
    ]
    graphics [
      x 843.9474328621164
      y 1582.440200172915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_230"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa456"
      uniprot "NA"
    ]
    graphics [
      x 1390.2463875560256
      y 828.3275826196502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_229"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa455"
      uniprot "NA"
    ]
    graphics [
      x 1024.6193835902177
      y 1267.1951149174101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_228"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa454"
      uniprot "NA"
    ]
    graphics [
      x 586.7388746717603
      y 1181.9658763151574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_235"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 1139.9056157846603
      y 1391.009187014059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:9051;urn:miriam:pubmed:22449964;urn:miriam:hgnc:8593;urn:miriam:intact:EBI-7800882;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583;urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:SERPINE1;HGNC_SYMBOL:PLAT"
      map_id "UNIPROT:P05121;UNIPROT:P00750"
      name "PLAT:SERPINE1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      x 723.4518281237446
      y 1499.2530649316168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05121;UNIPROT:P00750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_236"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa462"
      uniprot "NA"
    ]
    graphics [
      x 1405.1130540812196
      y 910.7566802060111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:taxonomy:9606;urn:miriam:hgnc:13557;urn:miriam:taxonomy:2697049;urn:miriam:pdb:6CS2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 331.55640772703083
      y 472.1435362481524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_114"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re373"
      uniprot "NA"
    ]
    graphics [
      x 159.28755407645747
      y 342.50612687798923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_viral_space_entry"
      name "SARS_minus_CoV_minus_2_space_viral_space_entry"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa485"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 257.6488722154899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_viral_space_entry"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_207"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa399"
      uniprot "NA"
    ]
    graphics [
      x 328.0056568909113
      y 1137.3399093513171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 230
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_69"
      target_id "UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 1
    target 5
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BYF1"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 229
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_207"
      target_id "M121_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 55
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 3
    target 226
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_68"
      target_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 151
    target 4
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 4
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_70"
      target_id "angiotensin_space_I_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 6
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 6
    target 11
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 6
    target 13
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 6
    target 15
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 6
    target 16
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 6
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 6
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 6
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 6
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 6
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 6
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 6
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 6
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 6
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 6
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 6
    target 27
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 6
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 6
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 6
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "SARS_minus_CoV_minus_2_space_infection"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 225
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_236"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 7
    target 51
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_78"
      target_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 150
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05121"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 149
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00750"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 8
    target 224
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_39"
      target_id "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 184
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 135
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 146
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P07204;UNIPROT:P00734"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 9
    target 210
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_46"
      target_id "Fibrin_space_monomer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 223
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_235"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 10
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_86"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 222
    target 11
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_228"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 204
    target 11
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 11
    target 181
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_85"
      target_id "UNIPROT:P05231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 147
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04070"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 221
    target 13
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_229"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 204
    target 13
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 13
    target 192
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_95"
      target_id "UNIPROT:P01375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 220
    target 14
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_230"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 14
    target 191
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_73"
      target_id "UNIPROT:P02741"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 219
    target 15
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_240"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 15
    target 176
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_81"
      target_id "UNIPROT:P07204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 16
    target 116
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_128"
      target_id "Hypokalemia"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 218
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_255"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 17
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_116"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 216
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_257"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 151
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 119
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "K_plus_"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 104
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 217
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "ACTH"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 18
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_117"
      target_id "aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 215
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_226"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 204
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 19
    target 190
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_87"
      target_id "UNIPROT:P10145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 214
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_231"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 20
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_65"
      target_id "UNIPROT:P00451"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 213
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_251"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 21
    target 184
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_41"
      target_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 212
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_234"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 22
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_79"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 207
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Fibrin_space_polymer"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 31
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 175
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P50454"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 23
    target 208
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_97"
      target_id "D_minus_dimer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 206
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_227"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 204
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 24
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_84"
      target_id "UNIPROT:P01584"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 92
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 133
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01008"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 135
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 26
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_36"
      target_id "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 203
    target 27
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_225"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 204
    target 27
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "cytokine_space_storm"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 27
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_88"
      target_id "UNIPROT:P01589"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 75
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00187"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 202
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_241"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 29
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_66"
      target_id "UNIPROT:P01008"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00747"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08697"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 30
    target 33
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_80"
      target_id "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 34
    target 31
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_92"
      target_id "UNIPROT:P00747"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 31
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00747"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 31
    target 36
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "UNIPROT:P00747"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 201
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_249"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 51
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 149
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00750"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 169
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00749"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 172
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03951"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 161
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_237"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_94"
      target_id "UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 66
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 71
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00751;UNIPROT:P01024"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_52"
      target_id "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P01031"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10643"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 43
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_53"
      target_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_54"
      target_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02748"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 49
    target 51
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_55"
      target_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 51
    target 53
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 51
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 52
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_64"
      target_id "C5b_minus_9_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 53
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_93"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 54
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_72"
      target_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594"
      target_id "M121_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_71"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "UNIPROT:P0C0L5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 58
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 58
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 58
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 58
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C0L5"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 194
    target 59
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C4"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 75
    target 59
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O00187"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 59
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "UNIPROT:P0C0L4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 60
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_91"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 61
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_63"
      target_id "C4d_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 65
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06681"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 62
    target 66
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_60"
      target_id "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00746"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 65
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06681"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P0C0L5;UNIPROT:P06681"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 69
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P01024;UNIPROT:P00751"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01024;UNIPROT:P00751"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_61"
      target_id "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P48740"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P48740"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 75
    target 74
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "UNIPROT:O00187"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00187"
      target_id "M121_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_90"
      target_id "MASP2_space_deposition"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C4d_space_deposition"
      target_id "M121_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_106"
      target_id "septal_space_capillary_space_necrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_43"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 83
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_45"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 84
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_44"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 85
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_75"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 86
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_48"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 87
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_49"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 88
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_125"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 89
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_112"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 90
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_124"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 91
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_76"
      target_id "Thrombosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 193
    target 82
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01584"
      target_id "M121_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 192
    target 83
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01375"
      target_id "M121_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 181
    target 84
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05231"
      target_id "M121_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 191
    target 85
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02741"
      target_id "M121_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 190
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P10145"
      target_id "M121_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 189
    target 87
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01589"
      target_id "M121_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 104
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 151
    target 89
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_II"
      target_id "M121_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 109
    target 90
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08235"
      target_id "M121_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04275"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_103"
      target_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 186
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_102"
      target_id "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      target_id "M121_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 95
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
      target_id "M121_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 96
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_98"
      target_id "M121_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_99"
      target_id "platelet_space_activation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 99
    target 98
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_101"
      target_id "platelet_space_activation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 98
    target 100
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "platelet_space_activation"
      target_id "M121_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 182
    target 99
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
      target_id "M121_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_100"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_109"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 103
    target 101
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_111"
      target_id "thrombus_space_formation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 154
    target 102
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M121_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 104
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 104
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30556"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 104
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30556"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 151
    target 105
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 150
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05121"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 110
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "aldosterone"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 151
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 152
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 181
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P05231"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 151
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_II"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P08235"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 110
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "aldosterone"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 110
    target 112
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 110
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "aldosterone"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 110
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 110
    target 115
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "aldosterone"
      target_id "M121_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 111
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_126"
      target_id "vascular_space_remodeling"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 121
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_266"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 113
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_123"
      target_id "UNIPROT:P12821"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 114
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_127"
      target_id "vascular_space_inflammation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_130"
      target_id "Hypokalemia"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "Hypokalemia"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 118
    target 117
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_271"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_129"
      target_id "K_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 122
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12821"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 156
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 123
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_25"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Bradykinin"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 124
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "Bradykinin(1_minus_5)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 124
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 162
    target 125
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "Bradykinin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 125
    target 170
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Bradykinin"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 125
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "Bradykinin"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 129
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 130
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "Small_space_peptide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 178
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00451"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 135
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 147
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 167
    target 129
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00748"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 161
    target 129
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00742"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 132
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00740"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 133
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01008"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 131
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00742"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 173
    target 132
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_29"
      target_id "UNIPROT:P00740"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 132
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00740"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P01008"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 135
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 136
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "Heparin"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 135
    target 137
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 135
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 135
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 135
    target 140
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00734"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 176
    target 137
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P07204"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 137
    target 146
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_47"
      target_id "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 175
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50454"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 167
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00748"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 139
    target 172
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "UNIPROT:P03951"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 141
    target 140
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00742;UNIPROT:P12259"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 142
    target 141
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_32"
      target_id "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12259"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_31"
      target_id "UNIPROT:P12259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 145
    target 144
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13726"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 146
    target 144
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P07204;UNIPROT:P00734"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 147
    target 144
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P04070"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 149
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00750"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 150
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P05121"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 151
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "angiotensin_space_II"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 152
    target 148
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 170
    target 149
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_113"
      target_id "UNIPROT:P00750"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 150
    target 168
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "UNIPROT:P05121"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 155
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_40"
      target_id "angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "angiotensin_space_I_minus_7"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P04201"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 156
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "angiotensin_space_I"
      target_id "M121_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 157
    target 156
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_23"
      target_id "angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01019"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 159
    target 157
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00797"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00797"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 161
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03952"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 162
    target 161
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "UNIPROT:P03952"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 161
    target 163
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P03952"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 161
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P03952"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 166
    target 162
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01042;UNIPROT:P03952"
      target_id "M121_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 162
    target 165
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "UNIPROT:P01042"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 167
    target 163
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00748"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 165
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01042"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 164
    target 166
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_28"
      target_id "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00749"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_248"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 172
    target 173
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P03951"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "F9"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 178
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00451"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 177
    target 179
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_30"
      target_id "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 183
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_107"
      target_id "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 184
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 185
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HCN6"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 187
    target 186
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P17301;UNIPROT:P05556"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 196
    target 197
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C5b_minus_9_space_deposition"
      target_id "M121_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 196
    target 198
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "C5b_minus_9_space_deposition"
      target_id "M121_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 197
    target 200
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_104"
      target_id "platelet_space_aggregation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 198
    target 199
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_105"
      target_id "apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 209
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_96"
      target_id "Fibrin_space_polymer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 210
    target 209
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "Fibrin_space_monomer"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 211
    target 209
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "F13a"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 226
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
      target_id "M121_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 227
    target 228
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_114"
      target_id "SARS_minus_CoV_minus_2_space_viral_space_entry"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
