# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000019186"
      hgnc "NA"
      map_id "CYP24A1"
      name "CYP24A1"
      node_subtype "GENE"
      node_type "species"
      org_id "cf2a0"
      uniprot "NA"
    ]
    graphics [
      x 692.0521125066696
      y 886.9760926971996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CYP24A1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbcf919df"
      uniprot "NA"
    ]
    graphics [
      x 737.9447088275103
      y 1000.3668702672564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000160868"
      hgnc "NA"
      map_id "CYP3A4"
      name "CYP3A4"
      node_subtype "GENE"
      node_type "species"
      org_id "f4d43; fee50"
      uniprot "NA"
    ]
    graphics [
      x 859.8869669629273
      y 1141.2886031791409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CYP3A4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940"
      hgnc "NA"
      map_id "Vitamin_space_D3"
      name "Vitamin_space_D3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac941; fef60"
      uniprot "NA"
    ]
    graphics [
      x 633.3511708937507
      y 933.3546565453155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Vitamin_space_D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbbb881c9"
      uniprot "NA"
    ]
    graphics [
      x 866.4291746861901
      y 1311.6117594664415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide42ad8d5"
      uniprot "NA"
    ]
    graphics [
      x 970.9942397853779
      y 1198.168500062574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34306"
      hgnc "NA"
      map_id "20_minus_HETE"
      name "20_minus_HETE"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a8cb6"
      uniprot "NA"
    ]
    graphics [
      x 1054.1660134601111
      y 1258.2795754773113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "20_minus_HETE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15843"
      hgnc "NA"
      map_id "Arachidonic_space__br_acid"
      name "Arachidonic_space__br_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a5ea4"
      uniprot "NA"
    ]
    graphics [
      x 864.1284460863504
      y 1432.8300717176257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Arachidonic_space__br_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 9
    source 1
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "CYP24A1"
      target_id "W13_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 3
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "CYP3A4"
      target_id "W13_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 2
    target 4
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_98"
      target_id "Vitamin_space_D3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 5
    target 3
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_97"
      target_id "CYP3A4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 3
    target 6
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "CYP3A4"
      target_id "W13_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 8
    target 5
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Arachidonic_space__br_acid"
      target_id "W13_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 6
    target 7
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_100"
      target_id "20_minus_HETE"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
