# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4799; WP4965; WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "Angiotensin_space_II"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b733a; bab13; f080e; ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 749.7672396397603
      y 398.00412558605194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fae04"
      uniprot "NA"
    ]
    graphics [
      x 525.8276330845179
      y 553.8132765725163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida74e8c"
      uniprot "NA"
    ]
    graphics [
      x 930.4987116436673
      y 258.48381033811813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id65de959d"
      uniprot "NA"
    ]
    graphics [
      x 841.7983851310912
      y 515.6880423395228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000144891; urn:miriam:pubmed:33375371;urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "AGTR1"
      name "AGTR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e034b; f99b1"
      uniprot "NA"
    ]
    graphics [
      x 716.6316650055592
      y 483.7079678824127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGTR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id99222b0a"
      uniprot "NA"
    ]
    graphics [
      x 567.485251496091
      y 488.96129672892965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:cas:52-39-1"
      hgnc "NA"
      map_id "Aldosterone"
      name "Aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e3c20"
      uniprot "NA"
    ]
    graphics [
      x 391.95771767356575
      y 482.65761189383153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Aldosterone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id672631f5"
      uniprot "NA"
    ]
    graphics [
      x 314.9188142553695
      y 620.9741071937124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4883; WP4969"
      full_annotation "urn:miriam:ncbigene:1636; urn:miriam:ncbigene:1636;urn:miriam:pubmed:15174896"
      hgnc "NA"
      map_id "ACE"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e5d6d; ed520; e130d; ab666"
      uniprot "NA"
    ]
    graphics [
      x 332.70640248920313
      y 805.7187572946341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id73c52fb1"
      uniprot "NA"
    ]
    graphics [
      x 403.1982626214939
      y 986.532831275126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id54c92813"
      uniprot "NA"
    ]
    graphics [
      x 264.91378763865794
      y 992.1907835490497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4ceb9356"
      uniprot "NA"
    ]
    graphics [
      x 169.00018891826028
      y 799.1420637308598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b1ef8"
      uniprot "NA"
    ]
    graphics [
      x 253.95089658571055
      y 692.4458765687751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_103"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfbc3672"
      uniprot "NA"
    ]
    graphics [
      x 549.3673506089247
      y 888.3427077073464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc8e789dd"
      uniprot "NA"
    ]
    graphics [
      x 189.55820267226227
      y 880.2265070094443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "Degradation"
      name "Degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5292"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 868.5571966922248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Degradation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hmdb:HMDB0004246; urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A3165"
      hgnc "NA"
      map_id "Bradykinin"
      name "Bradykinin"
      node_subtype "SIMPLE_MOLECULE; PROTEIN"
      node_type "species"
      org_id "bc2f2; sa402"
      uniprot "NA"
    ]
    graphics [
      x 792.5777605354971
      y 961.0415205669136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Bradykinin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id54a8211b"
      uniprot "NA"
    ]
    graphics [
      x 892.6235082648466
      y 797.8399304083442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id63c55d3"
      uniprot "NA"
    ]
    graphics [
      x 671.1350964987962
      y 1147.1152592159767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b7518"
      uniprot "NA"
    ]
    graphics [
      x 970.1402859869451
      y 1002.4173118326571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:624"
      hgnc "NA"
      map_id "BDKRB2"
      name "BDKRB2"
      node_subtype "GENE"
      node_type "species"
      org_id "f5820"
      uniprot "NA"
    ]
    graphics [
      x 1120.9426219724871
      y 962.2557012860386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BDKRB2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida315d709"
      uniprot "NA"
    ]
    graphics [
      x 966.9184745811776
      y 853.9284322335169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d28a2"
      uniprot "NA"
    ]
    graphics [
      x 1216.090313295736
      y 969.3422940070643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f390a"
      uniprot "NA"
    ]
    graphics [
      x 1175.4707555517662
      y 848.5572486828657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000089250"
      hgnc "NA"
      map_id "NOS1"
      name "NOS1"
      node_subtype "GENE"
      node_type "species"
      org_id "cf49a"
      uniprot "NA"
    ]
    graphics [
      x 1046.722622718381
      y 877.0366971419395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NOS1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a7811"
      uniprot "NA"
    ]
    graphics [
      x 896.0255064700968
      y 982.7701741883233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16467"
      hgnc "NA"
      map_id "L_minus_arginine"
      name "L_minus_arginine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d555e; d996c"
      uniprot "NA"
    ]
    graphics [
      x 766.9677794124378
      y 862.8771938879765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_arginine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000164867;urn:miriam:pubmed:18040024"
      hgnc "NA"
      map_id "NOS3"
      name "NOS3"
      node_subtype "GENE"
      node_type "species"
      org_id "fc11d"
      uniprot "NA"
    ]
    graphics [
      x 1056.839055714552
      y 972.0142057677942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NOS3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16480"
      hgnc "NA"
      map_id "nitric_space_oxide"
      name "nitric_space_oxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c57f1; e5fdd"
      uniprot "NA"
    ]
    graphics [
      x 767.8408843021693
      y 1172.4449912474538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nitric_space_oxide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_25"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bd07a"
      uniprot "NA"
    ]
    graphics [
      x 669.0600213857829
      y 1354.0329847395658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c1e80"
      uniprot "NA"
    ]
    graphics [
      x 713.8234069543512
      y 1081.6064505404663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:uniprot:A0A140VJE6"
      hgnc "NA"
      map_id "UNIPROT:A0A140VJE6"
      name "Guanylate_space_cyclase"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5e16"
      uniprot "UNIPROT:A0A140VJE6"
    ]
    graphics [
      x 652.4056075488887
      y 1516.9727773897494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A0A140VJE6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:10320667"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_43"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d69af"
      uniprot "NA"
    ]
    graphics [
      x 715.3776866837653
      y 1664.7578039521172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15996"
      hgnc "NA"
      map_id "Guanosine_space_triphosphate"
      name "Guanosine_space_triphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e3d3f"
      uniprot "NA"
    ]
    graphics [
      x 577.1204835255224
      y 1641.3622291761112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanosine_space_triphosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16356"
      hgnc "NA"
      map_id "Cyclic_space_guanosine_space_monophosphate"
      name "Cyclic_space_guanosine_space_monophosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ec563"
      uniprot "NA"
    ]
    graphics [
      x 882.6745635011506
      y 1705.6060585011192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cyclic_space_guanosine_space_monophosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_19"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b3d21"
      uniprot "NA"
    ]
    graphics [
      x 1060.2976976266737
      y 1693.641494385492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000185532"
      hgnc "NA"
      map_id "PRKG1"
      name "PRKG1"
      node_subtype "GENE"
      node_type "species"
      org_id "cdaee"
      uniprot "NA"
    ]
    graphics [
      x 1236.7559574377271
      y 1630.174688591664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PRKG1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_32"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c4399"
      uniprot "NA"
    ]
    graphics [
      x 1391.6628453021249
      y 1494.0805934448283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d96ba"
      uniprot "NA"
    ]
    graphics [
      x 1154.4132905062804
      y 1525.7230371211047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000067560"
      hgnc "NA"
      map_id "RHOA"
      name "RHOA"
      node_subtype "GENE"
      node_type "species"
      org_id "e0505"
      uniprot "NA"
    ]
    graphics [
      x 1171.6268166338
      y 1380.5538069486647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RHOA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_55"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e5c48"
      uniprot "NA"
    ]
    graphics [
      x 1247.7478351518141
      y 1229.3169065305253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000067900"
      hgnc "NA"
      map_id "ROCK1"
      name "ROCK1"
      node_subtype "GENE"
      node_type "species"
      org_id "a8b46"
      uniprot "NA"
    ]
    graphics [
      x 1345.904159227983
      y 1131.1601623877573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ROCK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_13"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b06b4"
      uniprot "NA"
    ]
    graphics [
      x 1457.5207894300017
      y 1180.002687903257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:brenda:3.1.3.53"
      hgnc "NA"
      map_id "MLCP"
      name "MLCP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "feb86"
      uniprot "NA"
    ]
    graphics [
      x 1480.5298579716964
      y 1304.3735498502433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MLCP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:1336455"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_45"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "d9883"
      uniprot "NA"
    ]
    graphics [
      x 1573.0601532434216
      y 1172.6366519290887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "Relaxation"
      name "Relaxation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c3988"
      uniprot "NA"
    ]
    graphics [
      x 1591.3216702832142
      y 1041.5162404437554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Relaxation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id501f9be8"
      uniprot "NA"
    ]
    graphics [
      x 639.5281043189233
      y 1000.8312919159173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id6c85e882"
      uniprot "NA"
    ]
    graphics [
      x 724.37123682614
      y 698.8621864699362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0000464"
      hgnc "NA"
      map_id "Ca_plus__plus_"
      name "Ca_plus__plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cc565"
      uniprot "NA"
    ]
    graphics [
      x 679.39374117447
      y 564.6970057008409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ca_plus__plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133068"
      hgnc "NA"
      map_id "bradykinin,_space_des_minus_arg(9)"
      name "bradykinin,_space_des_minus_arg(9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "aea90"
      uniprot "NA"
    ]
    graphics [
      x 559.6465907493937
      y 1103.3581388220557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bradykinin,_space_des_minus_arg(9)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b1f87"
      uniprot "NA"
    ]
    graphics [
      x 470.2752753677128
      y 933.2121198835474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b63af"
      uniprot "NA"
    ]
    graphics [
      x 854.7760902790643
      y 1220.6391313303882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:623"
      hgnc "NA"
      map_id "BDKRB1"
      name "BDKRB1"
      node_subtype "GENE"
      node_type "species"
      org_id "b029e"
      uniprot "NA"
    ]
    graphics [
      x 1137.2798049412893
      y 1286.7239028406493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "BDKRB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_1"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a2920"
      uniprot "NA"
    ]
    graphics [
      x 1251.6235424983283
      y 1379.2634943295836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b1b8e"
      uniprot "NA"
    ]
    graphics [
      x 1361.5383813594444
      y 1308.7231601320843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:cas:35121-78-9;urn:miriam:cas:363-24-6;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1A"
      hgnc "HGNC_SYMBOL:IL1B;HGNC_SYMBOL:IL1A"
      map_id "Inflammatory_space_mediators"
      name "Inflammatory_space_mediators"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ff4e0"
      uniprot "NA"
    ]
    graphics [
      x 1565.8082936508436
      y 1306.4184302337649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Inflammatory_space_mediators"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_61"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ef0da"
      uniprot "NA"
    ]
    graphics [
      x 1709.6149437637278
      y 1233.4578990795612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hgnc.symbol:IL1A;urn:miriam:cas:363-24-6;urn:miriam:cas:35121-78-9;urn:miriam:hgnc.symbol:IL1B"
      hgnc "HGNC_SYMBOL:IL1A;HGNC_SYMBOL:IL1B"
      map_id "f66e8"
      name "f66e8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f66e8"
      uniprot "NA"
    ]
    graphics [
      x 1776.6938082871366
      y 1113.8236840231002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f66e8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000232810;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1A;urn:miriam:hgnc.symbol:NFKB1"
      hgnc "HGNC_SYMBOL:IL1B;HGNC_SYMBOL:IL1A;HGNC_SYMBOL:NFKB1"
      map_id "f6746"
      name "f6746"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f6746"
      uniprot "NA"
    ]
    graphics [
      x 1260.812400964909
      y 1492.7293898859693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "f6746"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 7
      diagram "WP4883; WP4969; WP4912; WP5039; WP4853"
      full_annotation "urn:miriam:ncbigene:59272; urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272; urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "ACE2"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "bf1a9; aa820; dc981; c2d8e; a9be1; f1b6b; f3245"
      uniprot "NA"
    ]
    graphics [
      x 444.6129136616978
      y 747.7031219170467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b31de"
      uniprot "NA"
    ]
    graphics [
      x 438.99407430535206
      y 551.1794283694582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4965; WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0061196; urn:miriam:pubchem.compound:3081372"
      hgnc "NA"
      map_id "Angiotensin_space_I"
      name "Angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ad44a; c3eaf"
      uniprot "NA"
    ]
    graphics [
      x 529.850418375614
      y 378.3912279469606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_I"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:kegg.compound:C15851"
      hgnc "NA"
      map_id "Ang_space_1_minus_9"
      name "Ang_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b1bb7"
      uniprot "NA"
    ]
    graphics [
      x 414.1398396486151
      y 661.9681854803157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ang_space_1_minus_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11cf8705"
      uniprot "NA"
    ]
    graphics [
      x 598.5431564094674
      y 707.0931363941191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000180772; urn:miriam:pubmed:18035185;urn:miriam:ncbigene:186"
      hgnc "NA"
      map_id "AGTR2"
      name "AGTR2"
      node_subtype "GENE"
      node_type "species"
      org_id "a4260; f2fd8"
      uniprot "NA"
    ]
    graphics [
      x 796.3557627241189
      y 765.8974814578544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGTR2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e5729"
      uniprot "NA"
    ]
    graphics [
      x 674.4921987684967
      y 219.2495539481564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4883; WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720; urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "AGT"
      name "AGT"
      node_subtype "SIMPLE_MOLECULE; GENE"
      node_type "species"
      org_id "b9dff; f53bd; cfd56"
      uniprot "NA"
    ]
    graphics [
      x 775.12910220901
      y 248.13763816472135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGT"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4883; WP4969"
      full_annotation "urn:miriam:ncbigene:5972"
      hgnc "NA"
      map_id "REN"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "c91b1; f2946"
      uniprot "NA"
    ]
    graphics [
      x 871.3042546754191
      y 166.51167209655216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "REN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_81"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id1454daff"
      uniprot "NA"
    ]
    graphics [
      x 1050.238903822601
      y 202.25887302686408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940;urn:miriam:ensembl:ENSG00000111424"
      hgnc "NA"
      map_id "c2c3d"
      name "c2c3d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c2c3d"
      uniprot "NA"
    ]
    graphics [
      x 1189.356403290931
      y 252.3176590030538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c2c3d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000120054"
      hgnc "NA"
      map_id "CPN1"
      name "CPN1"
      node_subtype "GENE"
      node_type "species"
      org_id "fe018"
      uniprot "NA"
    ]
    graphics [
      x 691.1755674037869
      y 1269.2071385818303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CPN1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000164344"
      hgnc "NA"
      map_id "KLKB1"
      name "KLKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7722"
      uniprot "NA"
    ]
    graphics [
      x 980.5514806145577
      y 663.8362372188822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "KLKB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec428"
      uniprot "NA"
    ]
    graphics [
      x 1037.2054907173433
      y 534.0174841919724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:3827"
      hgnc "NA"
      map_id "KNG1"
      name "KNG1"
      node_subtype "GENE"
      node_type "species"
      org_id "b76f8"
      uniprot "NA"
    ]
    graphics [
      x 1048.9689033133363
      y 378.2661886651711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "KNG1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idfbb4ef92"
      uniprot "NA"
    ]
    graphics [
      x 1030.058699861686
      y 462.64287595112904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "Kallikrein_minus_Kinin_br_System"
      name "Kallikrein_minus_Kinin_br_System"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b4cad"
      uniprot "NA"
    ]
    graphics [
      x 1144.239517416959
      y 535.6821558920349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Kallikrein_minus_Kinin_br_System"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11c88b0d"
      uniprot "NA"
    ]
    graphics [
      x 1307.4922312765468
      y 523.373869780725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000167748"
      hgnc "NA"
      map_id "KLK1"
      name "KLK1"
      node_subtype "GENE"
      node_type "species"
      org_id "ddce0"
      uniprot "NA"
    ]
    graphics [
      x 1459.1051437290457
      y 507.50603991734715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "KLK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7951d7ac"
      uniprot "NA"
    ]
    graphics [
      x 1588.5167418744106
      y 419.6289644498705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000131187"
      hgnc "NA"
      map_id "F12"
      name "F12"
      node_subtype "GENE"
      node_type "species"
      org_id "f0b03"
      uniprot "NA"
    ]
    graphics [
      x 1637.8423939121485
      y 269.32013180202625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_104"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfe038846"
      uniprot "NA"
    ]
    graphics [
      x 1539.747568601667
      y 314.9483664463287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:32562843;PUBMED:33065209"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida91dd59d"
      uniprot "NA"
    ]
    graphics [
      x 1552.9069463709043
      y 137.36498391177759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:wikipathways:WP558"
      hgnc "NA"
      map_id "Complement_space_and_br_Coagulation_space_Cascades"
      name "Complement_space_and_br_Coagulation_space_Cascades"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d20d8"
      uniprot "NA"
    ]
    graphics [
      x 1414.4802964564046
      y 67.39245006208091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Complement_space_and_br_Coagulation_space_Cascades"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide64f6ad9"
      uniprot "NA"
    ]
    graphics [
      x 1253.8463298711038
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:5054;urn:miriam:pubmed:33375371"
      hgnc "NA"
      map_id "SERPINE1"
      name "SERPINE1"
      node_subtype "GENE"
      node_type "species"
      org_id "aac02"
      uniprot "NA"
    ]
    graphics [
      x 1089.1041585416529
      y 120.1018544587472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SERPINE1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000149131"
      hgnc "NA"
      map_id "SERPING1"
      name "SERPING1"
      node_subtype "GENE"
      node_type "species"
      org_id "c32b7"
      uniprot "NA"
    ]
    graphics [
      x 1452.321181384491
      y 391.07141194664075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SERPING1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:chemspider:110354"
      hgnc "NA"
      map_id "Ang_space_1_minus_7"
      name "Ang_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d4174"
      uniprot "NA"
    ]
    graphics [
      x 140.8293207770571
      y 638.6183714111722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ang_space_1_minus_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4969; WP4880; WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "NFKB1"
      name "NFKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "ae038; d3bcb; dc151"
      uniprot "NA"
    ]
    graphics [
      x 263.15513839503205
      y 1162.525509499134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NFKB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb55155be"
      uniprot "NA"
    ]
    graphics [
      x 343.1674028992179
      y 1299.9376602693953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000100030"
      hgnc "NA"
      map_id "MAPK1"
      name "MAPK1"
      node_subtype "GENE"
      node_type "species"
      org_id "c0de8"
      uniprot "NA"
    ]
    graphics [
      x 452.20295664683306
      y 1374.8078554481783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MAPK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 91
    source 2
    target 1
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_73"
      target_id "Angiotensin_space_II"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 1
    target 3
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W13_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 1
    target 4
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W13_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 62
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_I"
      target_id "W13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 9
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "ACE"
      target_id "W13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 3
    target 85
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_94"
      target_id "SERPINE1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 4
    target 5
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_87"
      target_id "AGTR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 5
    target 6
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "AGTR1"
      target_id "W13_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 6
    target 7
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_92"
      target_id "Aldosterone"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 7
    target 8
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Aldosterone"
      target_id "W13_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 8
    target 9
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_88"
      target_id "ACE"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 10
    target 9
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_90"
      target_id "ACE"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 11
    target 9
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_85"
      target_id "ACE"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 9
    target 12
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "ACE"
      target_id "W13_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 9
    target 13
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "ACE"
      target_id "W13_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 9
    target 14
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "ACE"
      target_id "W13_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 9
    target 15
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "ACE"
      target_id "W13_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 50
    target 10
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "bradykinin,_space_des_minus_arg(9)"
      target_id "W13_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 88
    target 11
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "NFKB1"
      target_id "W13_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 12
    target 16
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_82"
      target_id "Degradation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 63
    target 13
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Ang_space_1_minus_9"
      target_id "W13_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 13
    target 87
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_16"
      target_id "Ang_space_1_minus_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 14
    target 17
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_103"
      target_id "Bradykinin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 15
    target 16
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_99"
      target_id "Degradation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 18
    target 17
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_84"
      target_id "Bradykinin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 17
    target 19
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Bradykinin"
      target_id "W13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 17
    target 20
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Bradykinin"
      target_id "W13_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 72
    target 18
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "KLKB1"
      target_id "W13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 71
    target 19
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "CPN1"
      target_id "W13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 19
    target 50
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_86"
      target_id "bradykinin,_space_des_minus_arg(9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 20
    target 21
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_22"
      target_id "BDKRB2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 22
    target 21
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_93"
      target_id "BDKRB2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 21
    target 23
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "BDKRB2"
      target_id "W13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 21
    target 24
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "BDKRB2"
      target_id "W13_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 65
    target 22
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "AGTR2"
      target_id "W13_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 23
    target 28
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_40"
      target_id "NOS3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 24
    target 25
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_66"
      target_id "NOS1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 25
    target 26
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "NOS1"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 27
    target 26
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "L_minus_arginine"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 28
    target 26
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "NOS3"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 26
    target 29
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_3"
      target_id "nitric_space_oxide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 47
    target 27
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_83"
      target_id "L_minus_arginine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 27
    target 48
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "L_minus_arginine"
      target_id "W13_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 29
    target 30
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "nitric_space_oxide"
      target_id "W13_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 29
    target 31
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "nitric_space_oxide"
      target_id "W13_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 30
    target 32
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_25"
      target_id "UNIPROT:A0A140VJE6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 32
    target 33
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "UNIPROT:A0A140VJE6"
      target_id "W13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 34
    target 33
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Guanosine_space_triphosphate"
      target_id "W13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 33
    target 35
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_43"
      target_id "Cyclic_space_guanosine_space_monophosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 35
    target 36
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Cyclic_space_guanosine_space_monophosphate"
      target_id "W13_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 36
    target 37
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_19"
      target_id "PRKG1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 37
    target 38
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "PRKG1"
      target_id "W13_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 37
    target 39
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "PRKG1"
      target_id "W13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 38
    target 44
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_32"
      target_id "MLCP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 39
    target 40
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_44"
      target_id "RHOA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 40
    target 41
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "RHOA"
      target_id "W13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 41
    target 42
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_55"
      target_id "ROCK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 42
    target 43
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "ROCK1"
      target_id "W13_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 43
    target 44
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_13"
      target_id "MLCP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 44
    target 45
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "MLCP"
      target_id "W13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 45
    target 46
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_45"
      target_id "Relaxation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 50
    target 47
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "bradykinin,_space_des_minus_arg(9)"
      target_id "W13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 48
    target 49
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_89"
      target_id "Ca_plus__plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 51
    target 50
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_17"
      target_id "bradykinin,_space_des_minus_arg(9)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 50
    target 52
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "bradykinin,_space_des_minus_arg(9)"
      target_id "W13_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 60
    target 51
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "ACE2"
      target_id "W13_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 52
    target 53
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_21"
      target_id "BDKRB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 54
    target 53
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_1"
      target_id "BDKRB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 53
    target 55
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "BDKRB1"
      target_id "W13_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 59
    target 54
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "f6746"
      target_id "W13_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 55
    target 56
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_14"
      target_id "Inflammatory_space_mediators"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 56
    target 57
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Inflammatory_space_mediators"
      target_id "W13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 57
    target 58
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_61"
      target_id "f66e8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 60
    target 61
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "ACE2"
      target_id "W13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 62
    target 61
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_I"
      target_id "W13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 61
    target 63
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_18"
      target_id "Ang_space_1_minus_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 66
    target 62
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_54"
      target_id "Angiotensin_space_I"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 63
    target 64
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Ang_space_1_minus_9"
      target_id "W13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 64
    target 65
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_80"
      target_id "AGTR2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 67
    target 66
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "AGT"
      target_id "W13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 68
    target 66
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "REN"
      target_id "W13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 69
    target 68
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_81"
      target_id "REN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 70
    target 69
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "c2c3d"
      target_id "W13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 73
    target 72
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_58"
      target_id "KLKB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 74
    target 73
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "KNG1"
      target_id "W13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 75
    target 74
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_102"
      target_id "KNG1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 76
    target 75
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "Kallikrein_minus_Kinin_br_System"
      target_id "W13_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 77
    target 76
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_79"
      target_id "Kallikrein_minus_Kinin_br_System"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 78
    target 77
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "KLK1"
      target_id "W13_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 79
    target 78
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_91"
      target_id "KLK1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 80
    target 79
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "F12"
      target_id "W13_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 81
    target 80
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_104"
      target_id "F12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 80
    target 82
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "F12"
      target_id "W13_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 86
    target 81
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "SERPING1"
      target_id "W13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 82
    target 83
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_95"
      target_id "Complement_space_and_br_Coagulation_space_Cascades"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 84
    target 83
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_101"
      target_id "Complement_space_and_br_Coagulation_space_Cascades"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 85
    target 84
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "SERPINE1"
      target_id "W13_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 89
    target 88
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_96"
      target_id "NFKB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 90
    target 89
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "MAPK1"
      target_id "W13_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
