# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 264.52243306582284
      y 689.2234163534777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Interferon 1 pathway; C19DMap:E protein interactions; C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E; urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "UNIPROT:E"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131; sa13; sa32; sa22; sa19; sa83; sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 714.6375599065566
      y 777.4542268344758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "UNIPROT:O60885"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12; sa16"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 1153.3294757447143
      y 924.1680202802078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60885"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 923.4112217093852
      y 882.6958393284127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:E protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29101"
      hgnc "NA"
      map_id "Na_plus_"
      name "Na_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa68; sa67; sa336; sa337"
      uniprot "NA"
    ]
    graphics [
      x 141.5673102321682
      y 468.39789905834687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Na_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 153.25812642600852
      y 602.3299577671057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481;urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476"
      hgnc "HGNC_SYMBOL:ATP1B1;HGNC_SYMBOL:ATP1A1"
      map_id "UNIPROT:P05026;UNIPROT:P05023"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P05026;UNIPROT:P05023"
    ]
    graphics [
      x 228.15198451167123
      y 746.5189097114085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05026;UNIPROT:P05023"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:ASIC1"
      map_id "UNIPROT:P78348"
      name "ASIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27; sa25"
      uniprot "UNIPROT:P78348"
    ]
    graphics [
      x 378.560263666659
      y 918.7645841819267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P78348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_16"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 276.93877242790984
      y 829.3012891111005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "UNIPROT:Q9BUF7"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29; sa34"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 1484.966945648644
      y 187.5486690790421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BUF7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1490.6439778939598
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:PALS1;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5;HGNC_SYMBOL:PALS1"
      map_id "UNIPROT:E;UNIPROT:Q8N3R9"
      name "E_minus_PALS1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      x 872.9970175524752
      y 300.54154108604985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:E;UNIPROT:Q8N3R9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_27"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 967.0794619900079
      y 154.8608414197161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Maintenance_space_of_space_tight_space_junction"
      name "Maintenance_space_of_space_tight_space_junction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa74"
      uniprot "NA"
    ]
    graphics [
      x 1100.0839982635857
      y 66.2374078314133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Maintenance_space_of_space_tight_space_junction"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CDK9;HGNC_SYMBOL:CCNT1"
      map_id "UNIPROT:P50750;UNIPROT:O60563"
      name "P_minus_TEFb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      x 1353.6834007748157
      y 456.66784645888464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50750;UNIPROT:O60563"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_22"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 1289.1353486486046
      y 286.5051073914008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      name "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 1221.9735384267096
      y 158.17228295347664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "UNIPROT:P20226"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18; sa17"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1192.9497115655936
      y 570.0436548174176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P20226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1126.4400926678977
      y 690.6931296306816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "UNIPROT:P25440"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21; sa20"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1007.118256196025
      y 792.5960394278665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25440"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_10"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 910.7510085330407
      y 534.0658006306479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "UNIPROT:Q8N3R9"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30; sa35"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 1159.5271035049163
      y 402.14905758000407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N3R9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_28"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 365.33923885088535
      y 722.7379002741291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Activity_space_of_space_sodium_space_channels"
      name "Activity_space_of_space_sodium_space_channels"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa73"
      uniprot "NA"
    ]
    graphics [
      x 531.9957554048535
      y 767.8758873538987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Activity_space_of_space_sodium_space_channels"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "UNIPROT:P50750"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10; sa15"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 1437.5872503535595
      y 800.4800855084222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P50750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1325.4575895159605
      y 913.4160562210911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 525.0585334943368
      y 878.6043749949649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033"
      hgnc "HGNC_SYMBOL:STOML3"
      map_id "UNIPROT:Q8TAV4"
      name "STOML3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:Q8TAV4"
    ]
    graphics [
      x 540.9650382592963
      y 1006.8205367768821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TAV4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133115;urn:miriam:ncbigene:161003;urn:miriam:ncbigene:161003;urn:miriam:hgnc:19420;urn:miriam:hgnc.symbol:STOML3;urn:miriam:uniprot:Q8TAV4;urn:miriam:uniprot:Q8TAV4;urn:miriam:hgnc.symbol:STOML3;urn:miriam:refseq:NM_001144033;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:hgnc.symbol:ASIC1;urn:miriam:ensembl:ENSG00000110881;urn:miriam:uniprot:P78348;urn:miriam:uniprot:P78348;urn:miriam:ncbigene:41;urn:miriam:ncbigene:41;urn:miriam:refseq:NM_020039;urn:miriam:hgnc:100"
      hgnc "HGNC_SYMBOL:STOML3;HGNC_SYMBOL:ASIC1"
      map_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
      name "ASIC1_space_trimer:H_plus_:STOML3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q8TAV4;UNIPROT:P78348"
    ]
    graphics [
      x 452.9235611807546
      y 733.8277727587079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TAV4;UNIPROT:P78348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "UNIPROT:O60563"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11; sa14"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 1358.8754126518988
      y 744.9265842702487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60563"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1263.4152355544409
      y 840.9943086784192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 1148.387739846317
      y 289.8351181617855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14; urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504; urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504; urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14; HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H4C1; HGNC_SYMBOL:H4C1; HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1"
      map_id "UNIPROT:P62805"
      name "H4C14; H4_minus_16; H4C1; H4C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9; sa8; sa2; sa5"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1017.1166392632736
      y 420.44876112954637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62805"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 988.5676410820196
      y 549.6497966379732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464"
      hgnc "HGNC_SYMBOL:H3C15"
      map_id "UNIPROT:Q71DI3"
      name "H3C15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q71DI3"
    ]
    graphics [
      x 1073.634517248584
      y 477.1639400563219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q71DI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1"
      hgnc "HGNC_SYMBOL:H3C1"
      map_id "UNIPROT:P68431"
      name "H3C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P68431"
    ]
    graphics [
      x 844.9165135246625
      y 517.7062337853118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P68431"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349"
      hgnc "HGNC_SYMBOL:H2BC21"
      map_id "UNIPROT:Q16778"
      name "H2BC21"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q16778"
    ]
    graphics [
      x 947.2933905955142
      y 414.69429796124376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16778"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:interpro:IPR002119"
      hgnc "NA"
      map_id "H2A"
      name "H2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 890.92929673387
      y 456.58602920754413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4-16"
      map_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      x 1015.342538709446
      y 714.5595608396504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1378.672186994626
      y 296.1765596390821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "UNIPROT:Q8NI35"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33; sa28"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 1495.2597120704777
      y 365.71769989348314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NI35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9;urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:PATJ;HGNC_SYMBOL:MPP5;HGNC_SYMBOL:CRB3"
      map_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
      name "CRB3:PALS1:PATJ_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      x 1363.738000422577
      y 161.17051488883942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1259.0777518795098
      y 69.47288647904253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 1432.104298599444
      y 474.65766868388596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 850.1187329731473
      y 804.3113015134327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:pubchem.compound:46907787"
      hgnc "NA"
      map_id "JQ_minus_1"
      name "JQ_minus_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 800.5469730484621
      y 705.1204634475578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "JQ_minus_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1044.541164046646
      y 901.4122986537046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119"
      hgnc "HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H4C14"
      map_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      x 1029.6980884196018
      y 1045.9732103257006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:18406326"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_24"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1011.8267468472256
      y 1171.2691038348262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Chromatin_space_organization"
      name "Chromatin_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 1110.9196278398458
      y 1219.8190413634056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Chromatin_space_organization"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_21"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1427.3900733883588
      y 636.9961235752373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:799;urn:miriam:uniprot:P05023;urn:miriam:uniprot:P05023;urn:miriam:ensembl:ENSG00000163399;urn:miriam:refseq:NM_001160233;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:hgnc.symbol:ATP1A1;urn:miriam:ec-code:7.2.2.13;urn:miriam:ncbigene:476;urn:miriam:ncbigene:476;urn:miriam:uniprot:P05026;urn:miriam:uniprot:P05026;urn:miriam:hgnc:804;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:ensembl:ENSG00000143153;urn:miriam:hgnc.symbol:ATP1B1;urn:miriam:refseq:NM_001677;urn:miriam:ncbigene:481;urn:miriam:ncbigene:481"
      hgnc "HGNC_SYMBOL:ATP1A1;HGNC_SYMBOL:ATP1B1"
      map_id "UNIPROT:P05023;UNIPROT:P05026"
      name "ATP1A:ATP1B:FXYDs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P05023;UNIPROT:P05026"
    ]
    graphics [
      x 523.4014260522779
      y 692.6298083594057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05023;UNIPROT:P05026"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 454.4528664316085
      y 803.7335750548773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:E protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "K_plus_"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa65; sa66; sa521"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 838.4414492886289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "K_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 90.56389004797768
      y 736.9250070081882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 601.3046782522458
      y 903.5603566800562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:21524776"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_30"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 481.13442256778296
      y 620.6257044772124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 763.6185121681408
      y 205.27509163454036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_58"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 650.1664893591421
      y 158.75370701533333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 60
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60885"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Na_plus_"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78348"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 1
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BUF7"
      target_id "M17_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E;UNIPROT:Q8N3R9"
      target_id "M17_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_27"
      target_id "Maintenance_space_of_space_tight_space_junction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750;UNIPROT:O60563"
      target_id "M17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_22"
      target_id "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P20226"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P25440"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 2
    target 21
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 21
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_10"
      target_id "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 7
    target 23
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_28"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 3
    target 26
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 8
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P78348"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 2
    target 27
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 27
    target 29
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_17"
      target_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60563"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 3
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 22
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62805"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q71DI3"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 36
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P68431"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 37
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16778"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 38
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "H2A"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 34
    target 39
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_32"
      target_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 22
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N3R9"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 10
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BUF7"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_25"
      target_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
      target_id "M17_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 43
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_26"
      target_id "Maintenance_space_of_space_tight_space_junction"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NI35"
      target_id "M17_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 20
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25440"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 2
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "JQ_minus_1"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 39
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 3
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O60885"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 20
    target 47
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P25440"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_23"
      target_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
      target_id "M17_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_24"
      target_id "Chromatin_space_organization"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 25
    target 51
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P50750"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 30
    target 51
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60563"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 51
    target 15
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_21"
      target_id "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05023;UNIPROT:P05026"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 2
    target 53
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "UNIPROT:E"
      target_id "M17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 53
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_15"
      target_id "UNIPROT:P05026;UNIPROT:P05023"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "K_plus_"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 7
    target 55
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "TRIGGER"
      source_id "UNIPROT:P05026;UNIPROT:P05023"
      target_id "M17_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 28
    target 56
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4"
      target_id "M17_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 56
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_29"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 29
    target 57
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TAV4;UNIPROT:P78348"
      target_id "M17_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 57
    target 24
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_30"
      target_id "Activity_space_of_space_sodium_space_channels"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 12
    target 58
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:E;UNIPROT:Q8N3R9"
      target_id "M17_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_11"
      target_id "M17_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
