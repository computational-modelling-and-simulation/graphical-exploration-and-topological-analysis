# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 1680.4035052184795
      y 1498.0441647210725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 1554.9027503086734
      y 1486.4130126116775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4860; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:wikidata:Q25100575; urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "NA"
      map_id "Pevonedistat"
      name "Pevonedistat"
      node_subtype "SIMPLE_MOLECULE; DRUG"
      node_type "species"
      org_id "dda75; sa54"
      uniprot "NA"
    ]
    graphics [
      x 1586.8942231409728
      y 912.6487825602609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92; sa55"
      uniprot "NA"
    ]
    graphics [
      x 1073.742606099223
      y 741.6146438821193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387; urn:miriam:uniprot:P62877;urn:miriam:refseq:NM_014248;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:P62877"
      name "RBX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa11; sa12; sa20; sa4"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 907.4541952714661
      y 1928.7875944284692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083; urn:miriam:obo.chebi:CHEBI%3A456215; urn:miriam:obo.chebi:CHEBI%3A16027"
      hgnc "NA"
      map_id "AMP"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa301; sa97; sa224; sa45; sa216; sa288"
      uniprot "NA"
    ]
    graphics [
      x 1524.4697895593379
      y 1430.3811801991578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370; urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOB"
      map_id "UNIPROT:Q15370"
      name "ELOB"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa17; sa9; sa1"
      uniprot "UNIPROT:Q15370"
    ]
    graphics [
      x 220.7422783187617
      y 835.6227091131361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_51"
      name "deg_EloB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB"
      uniprot "NA"
    ]
    graphics [
      x 242.65286938417114
      y 919.7337493154713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_133"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 367.50991391351295
      y 899.2885125984817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 1114.9467779206004
      y 557.7985396219943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_75"
      name "diss_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1024.7644764397228
      y 546.4444041332032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2; urn:miriam:interpro:IPR000608"
      hgnc "HGNC_SYMBOL:UBE2; NA"
      map_id "E2"
      name "E2"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa23; sa42; sa7; sa15; sa89"
      uniprot "NA"
    ]
    graphics [
      x 1197.9077639681948
      y 579.8262970085963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 905.8304141067068
      y 693.9267718480448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593; urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593"
      hgnc "HGNC_SYMBOL:NAE1"
      map_id "UNIPROT:Q13564"
      name "NAE1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa176; sa174; sa179"
      uniprot "UNIPROT:Q13564"
    ]
    graphics [
      x 1881.7379790999325
      y 1511.5178665909957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13564"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_12"
      name "bind_NAE1_UBA3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE1_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1741.1429469449545
      y 1504.7956332388717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470; urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:UBA3"
      map_id "UNIPROT:Q8TBC4"
      name "UBA3"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa177; sa180; sa175"
      uniprot "UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1690.0012153799435
      y 1693.376452884639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593; urn:miriam:hgnc.symbol:NAE1;urn:miriam:obo.chebi:CHEBI%3A145535;urn:miriam:hgnc.symbol:UBA3;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:obo.chebi:CHEBI%3A145535"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3"
      map_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      name "NAE; NAE:Pevonedistat"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9; csa12"
      uniprot "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      x 1600.9797355937812
      y 1291.5923028877633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    ]
    graphics [
      x 1315.4503912979121
      y 1286.9166957595646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_78"
      name "diss_E3:NEDD8_E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_NEDD8_E3"
      uniprot "NA"
    ]
    graphics [
      x 1433.5874975638656
      y 1206.356388211618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "CSN5"
      name "CSN5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "NA"
    ]
    graphics [
      x 1735.5232018915226
      y 1198.303523596295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CSN5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      x 1164.0373677935047
      y 1190.6138923253156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 1368.2060699107562
      y 494.898711066852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_200"
      name "ubiquit_subs:E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1481.4553531643905
      y 386.9047509015379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    ]
    graphics [
      x 1584.2747058665664
      y 337.5014472465981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_64"
      name "deg_UBA3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1574.976310655125
      y 1840.5972492488556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_120"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1473.9786030963774
      y 1937.4866120646604
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_56"
      name "deg_NAE1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1642.2496373324832
      y 1464.0341322230272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_117"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 1430.2707807568631
      y 1505.9819317492377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725; urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M"
      map_id "UNIPROT:P61081"
      name "UBE2M"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa102; sa255; sa256"
      uniprot "UNIPROT:P61081"
    ]
    graphics [
      x 1016.3815399889538
      y 771.2759725978423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_66"
      name "deg_Ubc12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 907.92162480783
      y 606.8806798036925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_131"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 819.1814609469998
      y 492.75078072351255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re99"
      uniprot "NA"
    ]
    graphics [
      x 1420.2512028526726
      y 618.8357270943945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_140"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 1558.3737489509476
      y 732.6957279370093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:0E2:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    ]
    graphics [
      x 973.7717521655765
      y 390.4411070155743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_74"
      name "diss_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1144.2565266299675
      y 375.9826004355882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      x 1250.8276093731643
      y 508.92258051762053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBA"
      hgnc "HGNC_SYMBOL:UBA"
      map_id "E1"
      name "E1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa22; sa14; sa6; sa41"
      uniprot "NA"
    ]
    graphics [
      x 1610.7332730423802
      y 1030.4347414221013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_47"
      name "deg_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1"
      uniprot "NA"
    ]
    graphics [
      x 1474.9390916844475
      y 989.4103512882604
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_150"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 1391.926094025027
      y 872.5193203558263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_3"
      name "bind_E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1055.8484811600292
      y 626.9191703864707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "ubit_underscore_traget"
      name "ubit_underscore_traget"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162; sa171; sa105"
      uniprot "NA"
    ]
    graphics [
      x 1169.116192139682
      y 1477.5892066218926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubit_underscore_traget"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_72"
      name "deubiquit_subs:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deubiquit_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 957.4542955417409
      y 1765.0242174526152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:DUB"
      hgnc "HGNC_SYMBOL:DUB"
      map_id "DUB"
      name "DUB"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa24; sa16; sa8"
      uniprot "NA"
    ]
    graphics [
      x 694.1812189175039
      y 2016.7994519213257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DUB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648; urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC"
      map_id "UNIPROT:Q15369"
      name "ELOC"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa18; sa10; sa2"
      uniprot "UNIPROT:Q15369"
    ]
    graphics [
      x 398.54496172686015
      y 1219.3106666200174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_53"
      name "deg_EloC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC"
      uniprot "NA"
    ]
    graphics [
      x 574.0586050090933
      y 1247.9941577974396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_141"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 717.5350797515487
      y 1272.6080517175028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:ZYg11B;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ZYg11B;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:Orf10:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 1268.7871598966972
      y 1042.3103128170433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_81"
      name "diss_E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1231.0369259668569
      y 1245.251538840562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      name "Cul2_space_ubiquitin_space_ligase:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 1332.280705179898
      y 1107.0100889749383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB"
      map_id "UNIPROT:Q15369;UNIPROT:Q15370"
      name "ELOB:ELOC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 441.7308879008385
      y 978.3969774154078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_86"
      name "diss_EloB:EloC"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 262.34894042254143
      y 1027.8215184272976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_48"
      name "deg_E1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1699.093007776018
      y 1128.9272191905225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_149"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 1590.1928904104084
      y 1138.0702036430168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
    ]
    graphics [
      x 1407.2230210029732
      y 1412.907584866216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_82"
      name "diss_E3:Orf10:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 1319.0815785455084
      y 1465.179600969321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ncbiprotein:BCD58762;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8:Orf10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      x 1455.8600791832628
      y 1338.8244187483199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_2"
      name "bind_E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1103.7177765435795
      y 439.97897238576616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559; urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:ensembl:ENSG00000129559"
      hgnc "HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843"
      name "NEDD8"
      node_subtype "PROTEIN; GENE; RNA"
      node_type "species"
      org_id "sa118; sa178; sa173"
      uniprot "UNIPROT:Q15843"
    ]
    graphics [
      x 979.1514539443642
      y 1468.8775664897978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_57"
      name "deg_NEDD8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 900.335747638667
      y 1621.2885216989816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_116"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa182"
      uniprot "NA"
    ]
    graphics [
      x 777.8959863381474
      y 1676.367627959978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_197"
      name "ubiquit:E2_E3:Orf10:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1322.3204479755882
      y 403.53465236671263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_46"
      name "deg_DUB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 505.64437886781127
      y 2042.91903508593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_152"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa39"
      uniprot "NA"
    ]
    graphics [
      x 358.5675874382772
      y 1988.1294169984726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_87"
      name "diss_NAE"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1772.7726584510874
      y 1450.422050048991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_11"
      name "bind_NAE_Pevonedistat"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1662.9794737962154
      y 1067.5715899803388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      name "Orf10_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa278"
      uniprot "NA"
    ]
    graphics [
      x 914.1045830782158
      y 280.5352431168035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Orf10_space_(_plus_)ss_space_sgmRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_60"
      name "deg_Orf10_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 861.0777181119779
      y 140.0774742151748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_135"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 771.2889736840946
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820; urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820"
      hgnc "HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q9C0D3"
      name "ZYG11B"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa19; sa11; sa3"
      uniprot "UNIPROT:Q9C0D3"
    ]
    graphics [
      x 472.7395411976279
      y 1068.465944390333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9C0D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_68"
      name "deg_Zyg11B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 342.35134331894653
      y 1175.7466680911434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_144"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 327.9428805333506
      y 1299.7529555596507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_177"
      name "transcr_NEDD8"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 873.6535515363615
      y 1527.1491786156548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_194"
      name "transl_Zyg11B"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 667.8519612957614
      y 1091.039352335353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_184"
      name "transl_E1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E1"
      uniprot "NA"
    ]
    graphics [
      x 1651.0923246032335
      y 1169.7500565019689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_172"
      name "transcr_E1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E1"
      uniprot "NA"
    ]
    graphics [
      x 1745.3440700515534
      y 1063.7193230474056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2_minus_Ub:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    ]
    graphics [
      x 767.5794087525106
      y 628.7741900968664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_71"
      name "deubiquit_E2:E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 971.9224901498326
      y 631.8364708014619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094; urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:CUL2"
      map_id "UNIPROT:Q13617"
      name "CUL2"
      node_subtype "RNA; PROTEIN; GENE"
      node_type "species"
      org_id "sa13; sa21; sa5"
      uniprot "UNIPROT:Q13617"
    ]
    graphics [
      x 652.5269460565515
      y 1769.3556284201118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_44"
      name "deg_Cul2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 511.40569780480416
      y 1676.171056662397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_147"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 402.72562925183786
      y 1599.4946994680136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_193"
      name "transl_Ubc12"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 899.6174239267419
      y 767.7440102001747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_45"
      name "deg_DUB"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_DUB"
      uniprot "NA"
    ]
    graphics [
      x 551.034732563676
      y 2103.2935675473573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_154"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 434.48527346614674
      y 2010.7438117353352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_43"
      name "deg_Cul2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 464.4407142504661
      y 1791.3444183022252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_148"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 340.1809428168806
      y 1751.466810879256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_173"
      name "transcr_E2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_E2"
      uniprot "NA"
    ]
    graphics [
      x 1313.0818341397471
      y 578.7052141563396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_10"
      name "bind_EloB_EloC"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_EloB_EloC"
      uniprot "NA"
    ]
    graphics [
      x 323.3742327933911
      y 1002.2151062506042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_176"
      name "transcr_NAE1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 1988.7844282667825
      y 1583.0014658750993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_79"
      name "diss_E3:Orf10"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1162.9326754973413
      y 1004.9416833702344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_8"
      name "bind_Elo_Zyg11B"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 589.0238045842632
      y 1060.5379991598948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ZYG11B"
      map_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      name "Zyg11B:EloBC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      x 785.615258995067
      y 1155.6294145900151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_52"
      name "deg_EloB_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloB_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 88.05713696418184
      y 789.6576622212069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_128"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 902.7245196327775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_70"
      name "deubiquit_E2:E3:Orf10:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "deubiquit_E2_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1248.0152587387297
      y 422.8762701627628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_196"
      name "ubiquit_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E2"
      uniprot "NA"
    ]
    graphics [
      x 1436.602122554825
      y 782.7908226138336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:UBE2;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:UBE2;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8;HGNC_SYMBOL:UBE2"
      map_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
      name "Cul2_space_ubiquitin_space_ligase:N8:E2:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    ]
    graphics [
      x 791.6639750203593
      y 793.9131265973067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_76"
      name "diss_E2:E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E2_E3_subs_ub"
      uniprot "NA"
    ]
    graphics [
      x 991.4550900002673
      y 826.9059552870124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
      name "Cul2_space_ubiquitin_space_ligase:N8:substrate_minus_Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    ]
    graphics [
      x 1029.4050630277109
      y 1055.9506776537455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_59"
      name "deg_Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 845.3239860717792
      y 658.6652940627138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_134"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 696.2232251627244
      y 597.4769653639526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_183"
      name "transl_DUB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_DUB"
      uniprot "NA"
    ]
    graphics [
      x 665.535241929636
      y 2140.1778676231443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_170"
      name "transcr_Cul2"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 544.8828042180061
      y 1761.965293484947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_174"
      name "transcr_EloB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloB"
      uniprot "NA"
    ]
    graphics [
      x 412.94516369120083
      y 822.7294332466927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_85"
      name "diss_Elo:Zyg11B"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Elo_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 603.7148932389712
      y 996.1068053107989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_49"
      name "deg_E2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2"
      uniprot "NA"
    ]
    graphics [
      x 1336.1144883617658
      y 701.0535302129382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_191"
      name "transl_Rbx1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 1023.0305194202573
      y 1967.8869789528255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_67"
      name "deg_Ubc12_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Ubc12_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1157.921642551883
      y 795.5534618356608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_132"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa258"
      uniprot "NA"
    ]
    graphics [
      x 1282.5164435147026
      y 815.2086676285162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_9"
      name "bind_Elo:Zyg11B_Cul2:Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Elo_Zyg11B_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 988.5702494185437
      y 1280.2251617665627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:P62877"
      name "Rbx1:Cul2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      x 941.9380736019214
      y 1551.481991022659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:P62877"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_54"
      name "deg_EloC_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_EloC_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 580.7220735565533
      y 1337.0847858894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_138"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 721.6829491320591
      y 1372.6191038682246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0000502"
      hgnc "NA"
      map_id "26S_minus_proteasom"
      name "26S_minus_proteasom"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "NA"
    ]
    graphics [
      x 1422.601772317049
      y 1886.75845755243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "26S_minus_proteasom"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_41"
      name "deg_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1540.6549002967643
      y 2015.5717250860584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_136"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa268"
      uniprot "NA"
    ]
    graphics [
      x 1549.0746164845339
      y 2138.869623798476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_199"
      name "ubiquit_subs:E3"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_subs_E3"
      uniprot "NA"
    ]
    graphics [
      x 671.2497900611586
      y 712.3952941249789
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_69"
      name "deg_Zyg11B_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Zyg11B_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 315.68759313400926
      y 1103.0827237753956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_142"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 213.37073647883392
      y 1171.0465082087428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_1"
      name "bind_Cul2_Rbx1"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 814.7329513130921
      y 1748.4396037627912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_187"
      name "transl_EloC"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloC"
      uniprot "NA"
    ]
    graphics [
      x 500.5056762123954
      y 1292.938913312274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:hgnc.symbol:RBX1;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:uniprot:Q9C0D3;urn:miriam:uniprot:Q9C0D3;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:hgnc.symbol:ZYG11B;urn:miriam:ncbigene:79699;urn:miriam:ensembl:ENSG00000162378;urn:miriam:refseq:NM_024646;urn:miriam:hgnc:25820;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370"
      hgnc "HGNC_SYMBOL:ELOC;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ZYG11B;HGNC_SYMBOL:RBX1"
      map_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      name "Cul2_space_ubiquitin_space_ligase:substrate"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      x 1014.5064950228831
      y 1169.6930350875205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_84"
      name "diss_E3:subs"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1070.4838993429148
      y 1334.2790449882857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_182"
      name "transl_Cul2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Cul2"
      uniprot "NA"
    ]
    graphics [
      x 575.7643779427492
      y 1857.2457605888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc.symbol:NAE1;urn:miriam:hgnc:621;urn:miriam:ncbigene:8883;urn:miriam:ncbigene:8883;urn:miriam:uniprot:Q13564;urn:miriam:uniprot:Q13564;urn:miriam:refseq:NM_003905;urn:miriam:ensembl:ENSG00000159593;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:ensembl:ENSG00000144744;urn:miriam:hgnc.symbol:UBA3;urn:miriam:hgnc.symbol:UBA3;urn:miriam:uniprot:Q8TBC4;urn:miriam:uniprot:Q8TBC4;urn:miriam:ec-code:6.2.1.64;urn:miriam:refseq:NM_198195;urn:miriam:ncbigene:9039;urn:miriam:ncbigene:9039;urn:miriam:hgnc:12470"
      hgnc "HGNC_SYMBOL:NAE1;HGNC_SYMBOL:UBA3;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
      name "NAE:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    ]
    graphics [
      x 1132.5578424711248
      y 1138.8994443911856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_15"
      name "bind_Ubc12_NAE:NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Ubc12_NAE_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1051.2759886009696
      y 966.4710641970994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "urn:miriam:hgnc.symbol:UBE2M;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:hgnc:7732;urn:miriam:ncbigene:4738;urn:miriam:ncbigene:4738;urn:miriam:refseq:NM_006156;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q15843;urn:miriam:hgnc.symbol:NEDD8;urn:miriam:ensembl:ENSG00000129559;urn:miriam:hgnc:12491;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:uniprot:P61081;urn:miriam:uniprot:P61081;urn:miriam:hgnc.symbol:UBE2M;urn:miriam:ec-code:2.3.2.34;urn:miriam:ncbigene:9040;urn:miriam:refseq:NM_003969;urn:miriam:ncbigene:9040;urn:miriam:ensembl:ENSG00000130725"
      hgnc "HGNC_SYMBOL:UBE2M;HGNC_SYMBOL:NEDD8"
      map_id "UNIPROT:Q15843;UNIPROT:P61081"
      name "UBE2M:NEDD8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:Q15843;UNIPROT:P61081"
    ]
    graphics [
      x 1127.034717798555
      y 893.1932851560061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15843;UNIPROT:P61081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_58"
      name "deg_NEDD8_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NEDD8_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 786.214359595858
      y 1509.8060435574344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_115"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa181"
      uniprot "NA"
    ]
    graphics [
      x 651.4837522915336
      y 1524.1023081305595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_168"
      name "syn_26S_proteasom"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_26S_proteasom"
      uniprot "NA"
    ]
    graphics [
      x 1455.9943114041748
      y 2046.5322726067873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_179"
      name "transcr_UBA3"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1798.0943343010986
      y 1768.8987755135363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_88"
      name "diss_NAE:Pevonedistat"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_NAE_Pevonedistat"
      uniprot "NA"
    ]
    graphics [
      x 1555.052949392631
      y 1077.4772480564384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_198"
      name "ubiquit:E2_E3:subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "ubiquit_E2_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 908.9412576420057
      y 541.6347325972739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_13"
      name "bind_NEDD8_NAE"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_NEDD8_NAE"
      uniprot "NA"
    ]
    graphics [
      x 1252.840422340521
      y 1348.5232861349784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_5"
      name "bind_E3:Orf10:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 1222.7052848167395
      y 767.7748360138562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_185"
      name "transl_E2"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_E2"
      uniprot "NA"
    ]
    graphics [
      x 1186.2784619388808
      y 457.0332353589472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_14"
      name "bind_Orf10_E3"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_Orf10_E3"
      uniprot "NA"
    ]
    graphics [
      x 1218.0449006044444
      y 967.2295273708265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_77"
      name "diss_E3"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3"
      uniprot "NA"
    ]
    graphics [
      x 937.6606631739405
      y 1315.734489870707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_188"
      name "transl_NAE1"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 2006.205097252002
      y 1481.2429680900104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_137"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa269"
      uniprot "NA"
    ]
    graphics [
      x 1981.1099215208042
      y 1030.8571246776035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_169"
      name "syn_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "syn_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 1853.089781514443
      y 1047.4557151015513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_180"
      name "transcr_Ubc12"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Ubc12"
      uniprot "NA"
    ]
    graphics [
      x 901.1424380819393
      y 843.5781651457496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 1643.0013670332833
      y 891.663131949485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_83"
      name "diss_E3:subs:ub"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_E3_sub_ub"
      uniprot "NA"
    ]
    graphics [
      x 1132.5925491303276
      y 1263.0006326099046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_171"
      name "transcr_DUB"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_DUB"
      uniprot "NA"
    ]
    graphics [
      x 585.3546413685895
      y 1994.8989850078792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_4"
      name "bind_E3:Orf10_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_Orf10_subs"
      uniprot "NA"
    ]
    graphics [
      x 1286.4376168441831
      y 1222.0574212902188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_7"
      name "bind_E3:subs_NEDD8"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 960.0021691158895
      y 926.6483598572826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_80"
      name "diss_E3:Orf10:NEDD8_E3:Orf10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "diss_E3_Orf10_NEDD8_E3_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 1524.1674056777283
      y 1206.6575138190049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_50"
      name "deg_E2_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_E2_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1159.1975305177561
      y 693.3158143499576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_151"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa37"
      uniprot "NA"
    ]
    graphics [
      x 1074.3893232039422
      y 837.9183677573835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_178"
      name "transcr_Rbx1"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 972.4785318976158
      y 2036.6217558942806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_181"
      name "transcr_Zyg11B"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_Zyg11B"
      uniprot "NA"
    ]
    graphics [
      x 572.431961745317
      y 1138.945241148937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_175"
      name "transcr_EloC"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "transcr_EloC"
      uniprot "NA"
    ]
    graphics [
      x 466.91208578349676
      y 1159.0087788529002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_186"
      name "transl_EloB"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_EloB"
      uniprot "NA"
    ]
    graphics [
      x 310.6192559703529
      y 753.5434741439217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_73"
      name "diss_Cul2:Rbx1"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "diss_Cul2_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 865.5171493307486
      y 1775.6741579630816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_63"
      name "deg_substrate:ub"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_substrate_ub"
      uniprot "NA"
    ]
    graphics [
      x 1284.8333986834502
      y 1703.5445069245782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_104"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1233.536585028266
      y 1806.6513299495487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_42"
      name "deg_CSN5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_CSN5"
      uniprot "NA"
    ]
    graphics [
      x 1910.3251348508434
      y 1149.1672175423153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_189"
      name "transl_NEDD8"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_NEDD8"
      uniprot "NA"
    ]
    graphics [
      x 856.907258873976
      y 1440.106216697607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_62"
      name "deg_Rbx1_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 918.1629394139286
      y 2100.801697853492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_145"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 819.405926709265
      y 2067.0247782922283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_6"
      name "bind_E3_subs"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "bind_E3_subs"
      uniprot "NA"
    ]
    graphics [
      x 1141.904050002114
      y 1329.2882022505214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_55"
      name "deg_NAE1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_NAE1"
      uniprot "NA"
    ]
    graphics [
      x 1884.6850507751974
      y 1643.6438024467045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_118"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 1760.1900675840739
      y 1613.0971320863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_192"
      name "transl_UBA3"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_UBA3"
      uniprot "NA"
    ]
    graphics [
      x 1720.046741634309
      y 1817.0721810606497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_61"
      name "deg_Rbx1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_Rbx1"
      uniprot "NA"
    ]
    graphics [
      x 811.3046847203523
      y 1984.7997919347245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_146"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 718.4724816242964
      y 1924.3851681605584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_65"
      name "deg_UBA3_mRNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "deg_UBA3_mRNA"
      uniprot "NA"
    ]
    graphics [
      x 1513.818978214341
      y 1748.1857371961053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_119"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa185"
      uniprot "NA"
    ]
    graphics [
      x 1389.5019216193245
      y 1805.4585022505212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_190"
      name "transl_Orf10"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "transl_Orf10"
      uniprot "NA"
    ]
    graphics [
      x 982.1452450011611
      y 475.16659219983194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M119_195"
      name "ubiquit_E1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ubiquit_E1"
      uniprot "NA"
    ]
    graphics [
      x 1638.474063566849
      y 1356.0186457015666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M119_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf10 Cul2 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "Ub"
      name "Ub"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa43"
      uniprot "NA"
    ]
    graphics [
      x 1615.1458900888156
      y 1510.082858276307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 172
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_51"
      target_id "M119_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_75"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_12"
      target_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "CSN5"
      target_id "M119_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_78"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_200"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 16
    target 25
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_64"
      target_id "M119_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 14
    target 27
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_56"
      target_id "M119_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_66"
      target_id "M119_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 12
    target 32
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_90"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
      target_id "M119_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 35
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_74"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_47"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 13
    target 40
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 12
    target 40
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 40
    target 10
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_3"
      target_id "UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "DUB"
      target_id "M119_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_53"
      target_id "M119_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 48
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_81"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 51
    target 7
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 51
    target 44
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_86"
      target_id "UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 37
    target 52
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_48"
      target_id "M119_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q9C0D3"
      target_id "M119_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 55
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_82"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 36
    target 57
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 12
    target 57
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 57
    target 34
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_2"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15370;UNIPROT:Q15843;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_57"
      target_id "M119_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 36
    target 61
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 12
    target 61
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 61
    target 22
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_197"
      target_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 43
    target 62
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_46"
      target_id "M119_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 17
    target 64
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 64
    target 14
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "UNIPROT:Q13564"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 64
    target 16
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_87"
      target_id "UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 17
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 3
    target 65
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Pevonedistat"
      target_id "M119_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      target_id "M119_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_60"
      target_id "M119_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_68"
      target_id "M119_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 58
    target 72
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 69
    target 73
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 37
    target 74
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 37
    target 75
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      target_id "M119_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 77
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 77
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_71"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_44"
      target_id "M119_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 29
    target 81
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 43
    target 82
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_45"
      target_id "M119_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 78
    target 84
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_43"
      target_id "M119_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 12
    target 86
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 7
    target 87
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 44
    target 87
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 87
    target 50
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_10"
      target_id "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 14
    target 88
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 49
    target 89
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M119_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 89
    target 4
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "Orf10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 89
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_79"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 50
    target 90
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 69
    target 90
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_8"
      target_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 7
    target 92
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_52"
      target_id "M119_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 22
    target 94
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 94
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 94
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_70"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 12
    target 95
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 37
    target 95
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
      target_id "M119_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 97
    target 12
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_76"
      target_id "E2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 4
    target 99
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M119_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_59"
      target_id "M119_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 43
    target 101
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 78
    target 102
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 7
    target 103
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 91
    target 104
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 104
    target 50
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 104
    target 69
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_85"
      target_id "UNIPROT:Q9C0D3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 12
    target 105
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 105
    target 39
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_49"
      target_id "M119_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 5
    target 106
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 29
    target 107
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 107
    target 108
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_67"
      target_id "M119_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 91
    target 109
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 109
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_9"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 44
    target 111
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_54"
      target_id "M119_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "26S_minus_proteasom"
      target_id "M119_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_41"
      target_id "M119_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 76
    target 116
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
      target_id "M119_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 116
    target 96
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_199"
      target_id "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:P62877;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 69
    target 117
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_69"
      target_id "M119_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 5
    target 119
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 78
    target 119
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 119
    target 110
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_1"
      target_id "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 44
    target 120
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 122
    target 21
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 122
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_84"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 78
    target 123
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617"
      target_id "M119_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 29
    target 125
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_15"
      target_id "UNIPROT:Q15843;UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 58
    target 127
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_58"
      target_id "M119_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 115
    target 129
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_136"
      target_id "M119_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 129
    target 113
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_168"
      target_id "26S_minus_proteasom"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 16
    target 130
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 17
    target 131
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 131
    target 3
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_88"
      target_id "Pevonedistat"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 13
    target 132
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 12
    target 132
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 132
    target 76
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_198"
      target_id "UNIPROT:Q15369;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15843;UNIPROT:Q15370;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 58
    target 133
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 17
    target 133
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4;UNIPROT:Q13564"
      target_id "M119_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 133
    target 124
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_13"
      target_id "UNIPROT:Q13564;UNIPROT:Q15843;UNIPROT:Q8TBC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 47
    target 134
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 126
    target 134
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:P61081"
      target_id "M119_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 134
    target 36
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15843"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 134
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_5"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 12
    target 135
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 21
    target 136
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 4
    target 136
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10"
      target_id "M119_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 136
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_14"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 21
    target 137
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 137
    target 110
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "UNIPROT:Q13617;UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 137
    target 91
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_77"
      target_id "UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 14
    target 138
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "M119_137"
      target_id "M119_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 140
    target 20
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_169"
      target_id "CSN5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 29
    target 141
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61081"
      target_id "M119_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 37
    target 142
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 142
    target 33
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_89"
      target_id "M119_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 98
    target 143
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q15370;UNIPROT:P62877"
      target_id "M119_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 143
    target 18
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 143
    target 41
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_83"
      target_id "ubit_underscore_traget"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 43
    target 144
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "DUB"
      target_id "M119_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 49
    target 145
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 41
    target 145
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 145
    target 47
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_4"
      target_id "UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q9C0D3;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 126
    target 146
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843;UNIPROT:P61081"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 121
    target 146
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
      target_id "M119_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 146
    target 13
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15370;UNIPROT:Q13617;UNIPROT:Q15843;UNIPROT:Q15369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 146
    target 29
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_7"
      target_id "UNIPROT:P61081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 56
    target 147
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:Q13617;UNIPROT:P62877;UNIPROT:Q15843;UNIPROT:Q15369"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 20
    target 147
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "CSN5"
      target_id "M119_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 147
    target 49
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_80"
      target_id "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q9C0D3;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 12
    target 148
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E2"
      target_id "M119_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_50"
      target_id "M119_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 5
    target 150
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 69
    target 151
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9C0D3"
      target_id "M119_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 44
    target 152
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369"
      target_id "M119_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 7
    target 153
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15370"
      target_id "M119_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 110
    target 154
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13617;UNIPROT:P62877"
      target_id "M119_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 154
    target 78
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "UNIPROT:Q13617"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 154
    target 5
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_73"
      target_id "UNIPROT:P62877"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 41
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 113
    target 155
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CATALYSIS"
      source_id "26S_minus_proteasom"
      target_id "M119_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_63"
      target_id "M119_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 20
    target 157
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "CSN5"
      target_id "M119_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 157
    target 139
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_42"
      target_id "M119_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 58
    target 158
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15843"
      target_id "M119_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 5
    target 159
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_62"
      target_id "M119_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 41
    target 161
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ubit_underscore_traget"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 21
    target 161
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15369;UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q9C0D3;UNIPROT:P62877"
      target_id "M119_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 161
    target 121
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_6"
      target_id "UNIPROT:Q13617;UNIPROT:Q9C0D3;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q15370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 14
    target 162
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13564"
      target_id "M119_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_55"
      target_id "M119_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 16
    target 164
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 5
    target 165
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62877"
      target_id "M119_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_61"
      target_id "M119_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 16
    target 167
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TBC4"
      target_id "M119_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_65"
      target_id "M119_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 66
    target 169
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Orf10_space_(_plus_)ss_space_sgmRNA"
      target_id "M119_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 169
    target 4
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_190"
      target_id "Orf10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 37
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "E1"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "Ub"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 2
    target 170
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M119_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 170
    target 6
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 170
    target 1
    cd19dm [
      diagram "C19DMap:Orf10 Cul2 pathway"
      edge_type "PRODUCTION"
      source_id "M119_195"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
