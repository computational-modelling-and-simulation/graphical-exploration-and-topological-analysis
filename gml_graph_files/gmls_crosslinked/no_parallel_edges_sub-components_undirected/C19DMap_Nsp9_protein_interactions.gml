# generated with VANTED V2.8.2 at Fri Mar 04 09:59:53 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 21
      diagram "R-HSA-9678108; C19DMap:Nsp9 protein interactions; C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694642;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678281; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684355;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647;urn:miriam:uniprot:P0C6X7; urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678280; urn:miriam:reactome:R-COV-9684874;urn:miriam:reactome:R-COV-9694600;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682244;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682196;urn:miriam:reactome:R-COV-9694504;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9684861;urn:miriam:reactome:R-COV-9694695;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682242;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425;urn:miriam:uniprot:P0C6X7; urn:miriam:reactome:R-COV-9694372;urn:miriam:reactome:R-COV-9682216;urn:miriam:uniprot:P0C6X7; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7; urn:miriam:doi:10.1101/2020.05.18.102467;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0C6X7"
      name "pp1ab_minus_nsp13; nsp15_space_hexamer; pp1ab_minus_nsp14; pp1ab; pp1ab_minus_nsp16; pp1ab_minus_nsp12; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; pp1ab_minus_nsp15; pp1ab_minus_nsp9; pp1ab_minus_nsp7; pp1ab_minus_nsp5; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp8; pp1ab_minus_nsp6; pp1ab_minus_nsp10; Nsp14; Nsp16; inhibited_space_ribosomal_space_40S_space_SU; Nsp1; inhibited_space_80S_space_ribosome"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_32; layout_117; layout_29; layout_5; layout_35; layout_34; layout_727; layout_30; layout_33; layout_37; layout_28; layout_27; layout_31; layout_2066; layout_38; sa1378; sa1426; sa1374; csa15; sa70; csa16"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 416.67369649879436
      y 1430.69796678796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 1613.051046009204
      y 922.8188771151994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 45
      diagram "R-HSA-9694516; WP4846; WP5038; C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061; urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216; urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695; urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647; urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504; urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150; urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600; urn:miriam:uniprot:P0DTD1; urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-; urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "NA; HGNC_SYMBOL:rep"
      map_id "UNIPROT:P0DTD1"
      name "pp1ab_minus_nsp13; pp1ab; pp1ab_minus_nsp14; pp1ab_minus_nsp8; pp1ab_minus_nsp9; pp1ab_minus_nsp12; pp1ab_minus_nsp6; pp1ab_minus_nsp15; pp1ab_minus_nsp7; pp1ab_minus_nsp10; pp1ab_minus_nsp1_minus_4; pp1ab_minus_nsp16; pp1ab_minus_nsp5; nsp15_space_hexamer; N_minus_glycan_space_pp1ab_minus_nsp3_minus_4; orf1ab; nsp2; nsp7; Nsp13; pp1ab_space_Nsp3_minus_16; pp1ab_space_nsp6_minus_16; pp1a_space_Nsp3_minus_11; Nsp9; Nuclear_space_Pore_space_comp; Nsp8; Nsp7; Nsp12; Nsp10; homodimer; Nsp7812; RNArecognition; NspComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "layout_2195; layout_2763; layout_2192; layout_2893; layout_2196; layout_2575; layout_2199; layout_2193; layout_2848; layout_2201; layout_2190; layout_2198; layout_2191; layout_2245; layout_2218; cda8f; d244b; c185d; aaa58; d2766; sa167; sa309; sa2244; sa2229; sa1790; sa2221; sa2240; sa1423; csa21; sa997; a7c94; sa1421; sa1184; sa1428; sa1257; sa1424; sa1430; csa83; sa1183; csa63; csa64; csa84; sa1422; sa1429; csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1005.3190644596558
      y 938.7124470317054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4927; C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "NA; HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P00747"
      name "Plasmin; PLG; Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1440; sa1433; sa1431; sa211; sa212; sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1697.4337306416994
      y 1495.0332573042324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908; urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153; urn:miriam:obo.chebi:CHEBI%3A57945"
      hgnc "NA"
      map_id "NADH"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2; sa322; sa207; sa79; sa1264; sa286; sa44"
      uniprot "NA"
    ]
    graphics [
      x 992.711374003408
      y 379.149361760588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:TGFbeta signalling; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA; urn:miriam:pubmed:17016423;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "UNIPROT:P61586"
      name "RHOA; RGcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1; sa38; sa1305; csa101"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 1748.8883795153524
      y 751.9757188013004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61586"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619; urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "GDP"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa225; sa1274; path_0_sa100; path_0_sa103"
      uniprot "NA"
    ]
    graphics [
      x 2035.8279315642287
      y 427.80306169909807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:pubmed:18506437;urn:miriam:pubchem.compound:667490;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:pubmed:17139284;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4; urn:miriam:pubmed:17496727;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "UNIPROT:P12268"
      name "IMPDH2; IMPDH2:Nsp14; IMercomp; IRcomp; IMcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa209; csa8; sa1395; csa125; csa126; csa124"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 953.7902595995465
      y 1638.2524563922266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:pubmed:17355872;urn:miriam:pubchem.compound:5361;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "UNIPROT:Q9NXA8"
      name "SIRT5; SIRT5:Nsp14; SScomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa403; csa6; sa1408; csa128"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 247.834005012312
      y 1500.7578416363685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NXA8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:5799033;urn:miriam:pubchem.compound:446541;urn:miriam:obo.chebi:CHEBI%3A168396; urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "Mycophenolic_space_acid"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa213; sa1427; sa1398"
      uniprot "NA"
    ]
    graphics [
      x 534.6123498379413
      y 827.2884084668527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mycophenolic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A63580;urn:miriam:pubmed:22555152;urn:miriam:pubchem.compound:37542; urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "NA"
      map_id "Ribavirin"
      name "Ribavirin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa214; sa1405"
      uniprot "NA"
    ]
    graphics [
      x 1420.345609496907
      y 1726.008035494534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ribavirin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NEMF"
      map_id "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
      name "pathogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa85"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
    ]
    graphics [
      x 982.0717478604608
      y 1576.728831420363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_193"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10172"
      uniprot "NA"
    ]
    graphics [
      x 1151.7863122659965
      y 1611.0826598378108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "Neutrophil_underscore_activation"
      name "Neutrophil_underscore_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1278"
      uniprot "NA"
    ]
    graphics [
      x 1120.00164884309
      y 1522.372312652419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Neutrophil_underscore_activation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:GTF2F2"
      map_id "UNIPROT:P13984"
      name "GTF2F2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa981"
      uniprot "UNIPROT:P13984"
    ]
    graphics [
      x 1269.931969197126
      y 1244.6420333907688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P13984"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:3860504;PUBMED:16878124;PUBMED:26496610;PUBMED:8662660;PUBMED:26344197;PUBMED:17643375;PUBMED:11278533"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10082"
      uniprot "NA"
    ]
    graphics [
      x 1626.3071883226298
      y 1232.6269830662081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947"
      hgnc "HGNC_SYMBOL:GTF2B"
      map_id "UNIPROT:Q00403"
      name "GTF2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1058"
      uniprot "UNIPROT:Q00403"
    ]
    graphics [
      x 1750.7192288637293
      y 1306.2236197502102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00403"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "UNIPROT:P30876;UNIPROT:P24928"
      name "POLR2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1000"
      uniprot "UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 1755.4503363888448
      y 1223.8574607669618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30876;UNIPROT:P24928"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188"
      hgnc "HGNC_SYMBOL:POLR2B"
      map_id "UNIPROT:P30876"
      name "POLR2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1055"
      uniprot "UNIPROT:P30876"
    ]
    graphics [
      x 1650.8393319840657
      y 1379.5169738950635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30876"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434"
      hgnc "HGNC_SYMBOL:POLR2E"
      map_id "UNIPROT:P19388"
      name "POLR2E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1056"
      uniprot "UNIPROT:P19388"
    ]
    graphics [
      x 1716.3624170900068
      y 1139.6421648372707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002"
      hgnc "HGNC_SYMBOL:POLR2G"
      map_id "UNIPROT:P62487"
      name "POLR2G"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1057"
      uniprot "UNIPROT:P62487"
    ]
    graphics [
      x 1623.9402788850348
      y 1076.9469733690837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P62487"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:26344197;urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434;urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002;urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342;urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188;urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2E;HGNC_SYMBOL:POLR2G;HGNC_SYMBOL:GTF2B;HGNC_SYMBOL:GTF2F2;HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
      name "gtfrnapoly"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 1588.2084326971026
      y 1115.8327319229245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_141"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10117"
      uniprot "NA"
    ]
    graphics [
      x 1190.8347906666759
      y 972.8304339987484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32296183;urn:miriam:pubmed:9049309"
      hgnc "NA"
      map_id "Nuclear_space_Pore"
      name "Nuclear_space_Pore"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "NA"
    ]
    graphics [
      x 1341.6010120055298
      y 944.5735465384893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nuclear_space_Pore"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516"
      hgnc "HGNC_SYMBOL:FBLN5"
      map_id "UNIPROT:Q9UBX5"
      name "FBLN5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1138"
      uniprot "UNIPROT:Q9UBX5"
    ]
    graphics [
      x 1574.9932056796388
      y 1333.2265294250687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBX5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:14745449"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_126"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10102"
      uniprot "NA"
    ]
    graphics [
      x 1824.005440378351
      y 1336.397187066389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:LOXL1"
      map_id "UNIPROT:Q08397"
      name "LOXL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1127"
      uniprot "UNIPROT:Q08397"
    ]
    graphics [
      x 1936.2432014779697
      y 1222.067718682269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q08397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:14745449;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:LOXL1"
      map_id "UNIPROT:Q9UBX5;UNIPROT:Q08397"
      name "LOXcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:Q08397"
    ]
    graphics [
      x 1884.3221003176386
      y 1183.8201597854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBX5;UNIPROT:Q08397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1; urn:miriam:pubmed:20724158;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1;urn:miriam:pubchem.compound:5090; urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1; urn:miriam:uniprot:P35354;urn:miriam:obo.chebi:CHEBI%3A41423;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "UNIPROT:P35354"
      name "PTGS2; PTGScomp; PTCS2:celecoxib"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1153; csa54; sa315; csa34"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 2069.9661831360872
      y 1020.5593323708541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:20724158;PUBMED:10555907;PUBMED:10693877;PUBMED:10643177;PUBMED:11398914;PUBMED:20166930;PUBMED:10580458;PUBMED:11752352;PUBMED:10566562"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_154"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10131"
      uniprot "NA"
    ]
    graphics [
      x 2179.302076014892
      y 1160.2731399470451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5090"
      hgnc "NA"
      map_id "Rofecoxib"
      name "Rofecoxib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1154; sa1143"
      uniprot "NA"
    ]
    graphics [
      x 2151.218239441775
      y 1329.5281615922665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Rofecoxib"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:ZNF503"
      map_id "UNIPROT:Q96F45"
      name "ZNF503"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa964"
      uniprot "UNIPROT:Q96F45"
    ]
    graphics [
      x 902.4862167980102
      y 671.0611418754366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96F45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:27705803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_142"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10118"
      uniprot "NA"
    ]
    graphics [
      x 690.1412091175841
      y 579.2116217270433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:DCAF7"
      map_id "UNIPROT:P61962"
      name "DCAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa983"
      uniprot "UNIPROT:P61962"
    ]
    graphics [
      x 488.36682573161045
      y 751.0615852754831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61962"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:27705803;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:ZNF503;HGNC_SYMBOL:DCAF7"
      map_id "UNIPROT:Q96F45;UNIPROT:P61962"
      name "dcafznf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q96F45;UNIPROT:P61962"
    ]
    graphics [
      x 723.9658794221284
      y 439.2840882511142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96F45;UNIPROT:P61962"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17678888;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401; urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:uniprot:P05305;urn:miriam:ensembl:ENSG00000078401; urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401; urn:miriam:pubmed:28514442;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401; urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "UNIPROT:P05305"
      name "EDN1_minus_homo; EDN1"
      node_subtype "COMPLEX; GENE; RNA; PROTEIN"
      node_type "species"
      org_id "csa38; sa1007; sa1006; sa1074; sa1003; csa36; csa39"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 590.892808229051
      y 1004.132861596218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:7509919"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_132"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10108"
      uniprot "NA"
    ]
    graphics [
      x 1038.804178786432
      y 1193.2120840984257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA; urn:miriam:pubmed:17472992;urn:miriam:pubchem.compound:643975;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA; urn:miriam:pubmed:20811346;urn:miriam:pubchem.compound:6918493;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA; urn:miriam:pubmed:10727528;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:2244; urn:miriam:pubmed:24261583;urn:miriam:pubmed:22862294;urn:miriam:pubmed:22458347;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:16004692"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "UNIPROT:P25101"
      name "EDNRA; EDNRASitaComp; EDNRAmbComp; EDNRAcetComp; EDNRMacComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1116; csa59; csa60; csa62; csa61"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1398.9010492360064
      y 1398.4391362941922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:7509919;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDNRA;HGNC_SYMBOL:EDN1"
      map_id "UNIPROT:P25101;UNIPROT:P05305"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:P25101;UNIPROT:P05305"
    ]
    graphics [
      x 1146.3490571023767
      y 1214.048651416352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P25101;UNIPROT:P05305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_211"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10190"
      uniprot "NA"
    ]
    graphics [
      x 791.2765289366863
      y 973.0257024254342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091; urn:miriam:pubmed:17194211;urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091"
      hgnc "HGNC_SYMBOL:SEPSECS"
      map_id "UNIPROT:Q9HD40"
      name "SEPSECS; SPcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1337; csa109"
      uniprot "UNIPROT:Q9HD40"
    ]
    graphics [
      x 709.2866596426563
      y 1066.9369562417542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HD40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SEPSECS;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
      name "SEPSECScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa108"
      uniprot "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
    ]
    graphics [
      x 653.4847916072856
      y 880.7800736831277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22; urn:miriam:pubmed:16753178;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22;urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "HGNC_SYMBOL:NARS2"
      map_id "UNIPROT:Q96I59"
      name "NARS2; NLcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1349; csa113"
      uniprot "UNIPROT:Q96I59"
    ]
    graphics [
      x 1001.8883937572716
      y 559.9825953081374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96I59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:12874385;PUBMED:16753178"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_216"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10195"
      uniprot "NA"
    ]
    graphics [
      x 1099.8746171224582
      y 435.71564873867703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "NA"
      map_id "L_minus_Asparagine"
      name "L_minus_Asparagine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1353"
      uniprot "NA"
    ]
    graphics [
      x 1205.1593140856107
      y 515.9357719514048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Asparagine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10125"
      uniprot "NA"
    ]
    graphics [
      x 1348.8044592200101
      y 1307.5394849847573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBN1"
      map_id "UNIPROT:P35555"
      name "FBN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa965"
      uniprot "UNIPROT:P35555"
    ]
    graphics [
      x 1114.8377486515649
      y 1270.17253590051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35555"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:FBN1;HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
      name "Fibrillincomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1163.003014742067
      y 1304.8700766389338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_217"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10196"
      uniprot "NA"
    ]
    graphics [
      x 748.3659413838843
      y 700.948221837414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:NGDN"
      map_id "UNIPROT:Q8NEJ9"
      name "NGDN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1358"
      uniprot "UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 632.977584953589
      y 582.9985126196541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8NEJ9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NGDN"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
      name "NGDNcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa114"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 591.6796240214263
      y 630.4658209937595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:MIB1"
      map_id "UNIPROT:Q86YT6"
      name "MIB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa977"
      uniprot "UNIPROT:Q86YT6"
    ]
    graphics [
      x 731.3999248729276
      y 1580.5612529481625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86YT6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:21985982"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_128"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10104"
      uniprot "NA"
    ]
    graphics [
      x 761.1267080863892
      y 1852.11741074025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1"
      hgnc "HGNC_SYMBOL:DLL1"
      map_id "UNIPROT:O00548"
      name "DLL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1135"
      uniprot "UNIPROT:O00548"
    ]
    graphics [
      x 908.6225288334792
      y 1830.1553070249633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00548"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:21985982;urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:DLL1;HGNC_SYMBOL:MIB1"
      map_id "UNIPROT:O00548;UNIPROT:Q86YT6"
      name "MIBcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "UNIPROT:O00548;UNIPROT:Q86YT6"
    ]
    graphics [
      x 776.7310620242955
      y 1985.5439881944133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00548;UNIPROT:Q86YT6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:27880803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10061"
      uniprot "NA"
    ]
    graphics [
      x 414.4527405113803
      y 908.5065524251245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7"
      hgnc "HGNC_SYMBOL:SPART"
      map_id "UNIPROT:Q8N0X7"
      name "SPART"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa970"
      uniprot "UNIPROT:Q8N0X7"
    ]
    graphics [
      x 1784.9847452921404
      y 978.2896958900203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N0X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:19765186"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_130"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10106"
      uniprot "NA"
    ]
    graphics [
      x 2067.051586532394
      y 1094.7329631725092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846; urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:pubmed:10592235;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846;urn:miriam:pubchem.compound:643975"
      hgnc "HGNC_SYMBOL:AIFM1"
      map_id "UNIPROT:O95831"
      name "AIFM1; AIFMFlaComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1071; csa58"
      uniprot "UNIPROT:O95831"
    ]
    graphics [
      x 2226.89237448178
      y 1235.482274869568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95831"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19765186;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:AIFM1"
      map_id "UNIPROT:Q8N0X7;UNIPROT:O95831"
      name "SPARTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa46"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:O95831"
    ]
    graphics [
      x 2153.29040387642
      y 978.7691805323684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N0X7;UNIPROT:O95831"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_214"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10193"
      uniprot "NA"
    ]
    graphics [
      x 686.5008410248204
      y 1115.0411731934837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000174780;urn:miriam:hgnc:11303;urn:miriam:refseq:NM_001267722;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:uniprot:O76094"
      hgnc "HGNC_SYMBOL:SRP72"
      map_id "UNIPROT:O76094"
      name "SRP72"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1346"
      uniprot "UNIPROT:O76094"
    ]
    graphics [
      x 566.4262365763849
      y 1260.3558614480203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O76094"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000174780;urn:miriam:hgnc:11303;urn:miriam:refseq:NM_001267722;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:uniprot:O76094;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SRP72;HGNC_SYMBOL:rep"
      map_id "UNIPROT:O76094;UNIPROT:P0DTD1"
      name "SRP72comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa111"
      uniprot "UNIPROT:O76094;UNIPROT:P0DTD1"
    ]
    graphics [
      x 522.2870271492088
      y 1242.1986784178237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O76094;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2"
      hgnc "HGNC_SYMBOL:COPS2"
      map_id "UNIPROT:P61201"
      name "COPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1090"
      uniprot "UNIPROT:P61201"
    ]
    graphics [
      x 550.6056896431867
      y 1148.8816625958314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:10903862;PUBMED:26186194;PUBMED:23408908;PUBMED:11967155;PUBMED:26344197;PUBMED:27173435;PUBMED:16045761;PUBMED:22939629;PUBMED:26496610;PUBMED:21145461;PUBMED:27833851;PUBMED:18850735;PUBMED:26976604;PUBMED:9707402;PUBMED:28514442;PUBMED:16624822;PUBMED:19295130;PUBMED:19615732;PUBMED:22863883;PUBMED:12628923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_124"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10091"
      uniprot "NA"
    ]
    graphics [
      x 547.6133794207652
      y 1324.8172357940848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A"
      map_id "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
      name "COPS4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1089"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
    ]
    graphics [
      x 414.2921373145157
      y 1331.2828791667546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS5"
      map_id "UNIPROT:Q92905"
      name "COPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1087"
      uniprot "UNIPROT:Q92905"
    ]
    graphics [
      x 487.8681618426707
      y 1179.1866106659168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92905"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:COPS6"
      map_id "UNIPROT:Q7L5N1"
      name "COPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1086"
      uniprot "UNIPROT:Q7L5N1"
    ]
    graphics [
      x 427.80569923206235
      y 1080.8757947653291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7L5N1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS7A"
      map_id "UNIPROT:Q9UBW8"
      name "COPS7A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1088"
      uniprot "UNIPROT:Q9UBW8"
    ]
    graphics [
      x 407.2050609440307
      y 1262.069881666073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9UBW8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS8"
      map_id "UNIPROT:Q99627"
      name "COPS8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1091"
      uniprot "UNIPROT:Q99627"
    ]
    graphics [
      x 590.7205616481194
      y 1222.3313147585336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99627"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16045761;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS5;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS8"
      map_id "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
    ]
    graphics [
      x 858.39958849059
      y 1530.369414154668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_168"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10145"
      uniprot "NA"
    ]
    graphics [
      x 1142.5408216627532
      y 1141.4656316566395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902; urn:miriam:pubmed:17016423;urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:BCKDK"
      map_id "UNIPROT:O14874"
      name "BCKDK; ADPcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1198; csa67"
      uniprot "UNIPROT:O14874"
    ]
    graphics [
      x 1335.8088991743357
      y 1085.2565676462698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14874"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:BCKDK"
      map_id "UNIPROT:P0DTD1;UNIPROT:O14874"
      name "s389"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa66"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O14874"
    ]
    graphics [
      x 1101.097488746288
      y 1373.8739266692387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:O14874"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr"
      hgnc "HGNC_SYMBOL:vpr"
      map_id "UNIPROT:I2A5W5"
      name "Vpr"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1085"
      uniprot "UNIPROT:I2A5W5"
    ]
    graphics [
      x 1300.9589278420353
      y 1773.5148902799033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:I2A5W5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:22190034;PUBMED:12237292"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_135"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10111"
      uniprot "NA"
    ]
    graphics [
      x 1099.2062516683366
      y 1728.1372803314162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:22190034;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS8;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS4;HGNC_SYMBOL:vpr;HGNC_SYMBOL:COPS5"
      map_id "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
    ]
    graphics [
      x 1273.3337857726415
      y 1713.1633796380283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:18506437;PUBMED:11752352"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_229"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10208"
      uniprot "NA"
    ]
    graphics [
      x 1140.663251787245
      y 1691.1926106768742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:667490"
      hgnc "NA"
      map_id "Mercaptopurine"
      name "Mercaptopurine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1402"
      uniprot "NA"
    ]
    graphics [
      x 1286.064280504676
      y 1635.6968867351034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mercaptopurine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_143"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10119"
      uniprot "NA"
    ]
    graphics [
      x 734.6469862988787
      y 1213.4445985286882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:MIB1;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
      name "mibcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
    ]
    graphics [
      x 499.049035728034
      y 1135.1280735282912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312; urn:miriam:pubmed:17016423;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:3870203; urn:miriam:pubmed:20196537;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:34755; urn:miriam:pubmed:1170911;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:66414; urn:miriam:pubmed:12876237;urn:miriam:pubchem.compound:5281081;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312; urn:miriam:pubmed:10592235;urn:miriam:pubchem.compound:6914595;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312; urn:miriam:pubmed:20502133;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:4659569"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "UNIPROT:P21964"
      name "COMT; DCcomp; ACcomp; MCcomp; NCcomp; DNCcomp; TCcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1277; csa94; csa95; csa97; csa93; csa96; csa92"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1761.7597503164836
      y 420.47908146082807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21964"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_197"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10176"
      uniprot "NA"
    ]
    graphics [
      x 1741.5384980522567
      y 496.6923423836161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:3870203"
      hgnc "NA"
      map_id "3,5_minus_Dinitrocatechol"
      name "3,5_minus_Dinitrocatechol"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1287"
      uniprot "NA"
    ]
    graphics [
      x 1802.335863978837
      y 582.8151501093671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "3,5_minus_Dinitrocatechol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410; urn:miriam:pubmed:19703035;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.substance:5734; urn:miriam:pubmed:20185318;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.compound:5281855; urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:3639;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410; urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:70876165;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410; urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:2343;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "UNIPROT:O43570"
      name "CA12; ZonisamideComp; EAcomp; HCTcomp; HFTcomp; BZcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1228; csa77; csa78; csa79; csa80; csa81"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1681.689198831728
      y 1322.2122302395412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43570"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:17582922;PUBMED:18537527;PUBMED:17762320;PUBMED:19703035;PUBMED:18782051"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_179"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10157"
      uniprot "NA"
    ]
    graphics [
      x 1552.775460991842
      y 1456.4322354336691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.substance:5734"
      hgnc "NA"
      map_id "Zonisamide"
      name "Zonisamide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1231"
      uniprot "NA"
    ]
    graphics [
      x 1420.8575127036297
      y 1502.3461024534763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Zonisamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_127"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10103"
      uniprot "NA"
    ]
    graphics [
      x 1414.473421318905
      y 702.7158910319699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2B"
      map_id "UNIPROT:Q9NZL9"
      name "MAT2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa962"
      uniprot "UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1702.5561697773676
      y 549.2021685322004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NZL9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MAT2B"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
      name "mat2bcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1529.8582568011202
      y 624.3403032578465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_231"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10210"
      uniprot "NA"
    ]
    graphics [
      x 328.8613157267922
      y 1366.3131337966418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:SIRT5;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
      name "SIRT5comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa127"
      uniprot "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 517.9191988505966
      y 1198.68235877485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_225"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10204"
      uniprot "NA"
    ]
    graphics [
      x 989.5731591771042
      y 1141.7435867717902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:AP2A2"
      map_id "UNIPROT:O94973"
      name "AP2A2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1392"
      uniprot "UNIPROT:O94973"
    ]
    graphics [
      x 1035.0044638505874
      y 1272.356803135616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O94973"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:AP2A2"
      map_id "UNIPROT:P0DTD1;UNIPROT:O94973"
      name "AP2A2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa122"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O94973"
    ]
    graphics [
      x 976.106919599141
      y 1291.0089300234476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:O94973"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_146"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10122"
      uniprot "NA"
    ]
    graphics [
      x 979.4520075223709
      y 692.2168082319827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:EIF4H"
      map_id "UNIPROT:Q15056"
      name "EIF4H"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa985"
      uniprot "UNIPROT:Q15056"
    ]
    graphics [
      x 844.6845879994381
      y 592.0011206298623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15056"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:EIF4H"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q15056"
      name "eifcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q15056"
    ]
    graphics [
      x 945.275556536494
      y 555.1536082500565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q15056"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_188"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10167"
      uniprot "NA"
    ]
    graphics [
      x 753.2585872386372
      y 837.7159821163154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2; urn:miriam:pubmed:17016423;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2;urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238; urn:miriam:pubmed:17341833;urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2"
      hgnc "HGNC_SYMBOL:CYB5R3"
      map_id "UNIPROT:P00387"
      name "CYB5R3; FADcomp; NADHcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1260; csa88; csa87"
      uniprot "UNIPROT:P00387"
    ]
    graphics [
      x 852.3625723921322
      y 693.1349124899284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:CYB5R3"
      map_id "UNIPROT:P0DTD1;UNIPROT:P00387"
      name "CYB5R3comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa86"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P00387"
    ]
    graphics [
      x 596.8060258095769
      y 855.0971295760648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P00387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_190"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10169"
      uniprot "NA"
    ]
    graphics [
      x 985.4969166583797
      y 755.1592810078427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "NA"
      map_id "FAD"
      name "FAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1269; sa1389"
      uniprot "NA"
    ]
    graphics [
      x 1121.4476763365235
      y 767.8008543251655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:24778252;PUBMED:32353859;PUBMED:29845934;PUBMED:18483487;PUBMED:26725010;PUBMED:17643375"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_218"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10197"
      uniprot "NA"
    ]
    graphics [
      x 837.5425174880565
      y 1455.2602853697047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0"
      hgnc "HGNC_SYMBOL:MEPCE"
      map_id "UNIPROT:Q7L2J0"
      name "MEPCE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1361"
      uniprot "UNIPROT:Q7L2J0"
    ]
    graphics [
      x 767.056995033158
      y 1646.780048047922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7L2J0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:LARP7"
      map_id "UNIPROT:Q4G0J3"
      name "LARP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1364"
      uniprot "UNIPROT:Q4G0J3"
    ]
    graphics [
      x 843.6083542056381
      y 1620.895561058771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q4G0J3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:29845934;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:MEPCE;HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP7"
      map_id "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
      name "MEPCEcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa115"
      uniprot "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
    ]
    graphics [
      x 678.1768207729665
      y 1570.9446477998472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_185"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10164"
      uniprot "NA"
    ]
    graphics [
      x 935.1609752153763
      y 891.6932088570675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_235"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re10214"
      uniprot "NA"
    ]
    graphics [
      x 413.67443335612904
      y 734.2844199396188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:17472992;PUBMED:10961375;PUBMED:11849873;PUBMED:11704565;PUBMED:11752352;PUBMED:11728166;PUBMED:11447307"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_159"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10136"
      uniprot "NA"
    ]
    graphics [
      x 1492.8840824575232
      y 1213.2139745546056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "Sitaxentan"
      name "Sitaxentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1169"
      uniprot "NA"
    ]
    graphics [
      x 1550.6650687180054
      y 1056.641343530121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Sitaxentan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:FBN1"
      map_id "UNIPROT:P98095;UNIPROT:P35555"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P98095;UNIPROT:P35555"
    ]
    graphics [
      x 1562.7634223861992
      y 1566.4578035039535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P98095;UNIPROT:P35555"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:10544250;PUBMED:10825173"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10128"
      uniprot "NA"
    ]
    graphics [
      x 1695.9491580878273
      y 1602.9744043404426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501; urn:miriam:pubmed:17620346;urn:miriam:pubmed:16679386;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:pubchem.compound:5090"
      hgnc "HGNC_SYMBOL:ELN"
      map_id "UNIPROT:P15502"
      name "ELN; RofecoxibComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1139; csa52"
      uniprot "UNIPROT:P15502"
    ]
    graphics [
      x 1842.5027313067035
      y 1524.444063842809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15502"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:pubmed:10544250;urn:miriam:pubmed:10825173;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199"
      hgnc "HGNC_SYMBOL:FBN1;HGNC_SYMBOL:ELN;HGNC_SYMBOL:FBLN2"
      map_id "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
    ]
    graphics [
      x 1613.7496981303952
      y 1678.5633354220718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404"
      hgnc "HGNC_SYMBOL:EXOSC2"
      map_id "UNIPROT:Q13868"
      name "EXOSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1322"
      uniprot "UNIPROT:Q13868"
    ]
    graphics [
      x 973.6286251981644
      y 320.26738414866804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13868"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_208"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10187"
      uniprot "NA"
    ]
    graphics [
      x 1066.409834988141
      y 357.97868508375893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010"
      hgnc "HGNC_SYMBOL:EXOSC3"
      map_id "UNIPROT:Q9NQT5"
      name "EXOSC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1333"
      uniprot "UNIPROT:Q9NQT5"
    ]
    graphics [
      x 1026.3114974294595
      y 188.32989312710083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NQT5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915"
      hgnc "HGNC_SYMBOL:EXOSC5"
      map_id "UNIPROT:Q9NQT4"
      name "EXOSC5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1335"
      uniprot "UNIPROT:Q9NQT4"
    ]
    graphics [
      x 1163.048720240054
      y 285.195701126935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NQT4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26"
      hgnc "HGNC_SYMBOL:EXOSC8"
      map_id "UNIPROT:Q96B26"
      name "EXOSC8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1356"
      uniprot "UNIPROT:Q96B26"
    ]
    graphics [
      x 993.4420818250283
      y 248.3161172601234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96B26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:28514442;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404;urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26;urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:EXOSC3;HGNC_SYMBOL:EXOSC2;HGNC_SYMBOL:EXOSC8;HGNC_SYMBOL:EXOSC5;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
      name "EXOCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa105"
      uniprot "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1093.9589557429351
      y 204.10102224900572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:MRPS5"
      map_id "UNIPROT:P82675"
      name "MRPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1325"
      uniprot "UNIPROT:P82675"
    ]
    graphics [
      x 461.4884351159719
      y 448.506404209279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P82675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_209"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10188"
      uniprot "NA"
    ]
    graphics [
      x 601.2014410127081
      y 517.8179843293881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399"
      hgnc "HGNC_SYMBOL:MRPS2"
      map_id "UNIPROT:Q9Y399"
      name "MRPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1328"
      uniprot "UNIPROT:Q9Y399"
    ]
    graphics [
      x 499.5423869728504
      y 390.92213108094325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS5"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
      name "MRPScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa106"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
    ]
    graphics [
      x 564.6563502154164
      y 365.773475010669
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_204"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10183"
      uniprot "NA"
    ]
    graphics [
      x 1977.6510655485472
      y 620.8189559040406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:25544563"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10105"
      uniprot "NA"
    ]
    graphics [
      x 1664.5710603815855
      y 360.1016137494498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:PCSK1"
      map_id "UNIPROT:P29120"
      name "PCSK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1132"
      uniprot "UNIPROT:P29120"
    ]
    graphics [
      x 1529.0734305827905
      y 346.59194880773623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:25544563;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:MAT2B;HGNC_SYMBOL:PCSK1"
      map_id "UNIPROT:Q9NZL9;UNIPROT:P29120"
      name "NEC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa49"
      uniprot "UNIPROT:Q9NZL9;UNIPROT:P29120"
    ]
    graphics [
      x 1769.638182995053
      y 296.5668811474386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NZL9;UNIPROT:P29120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_194"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10173"
      uniprot "NA"
    ]
    graphics [
      x 1439.2300919599143
      y 614.1754144476625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:COMT;HGNC_SYMBOL:rep"
      map_id "UNIPROT:P21964;UNIPROT:P0DTD1"
      name "COMT"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:P21964;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1358.3189212645432
      y 533.6174023703688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21964;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_173"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10150"
      uniprot "NA"
    ]
    graphics [
      x 934.8203968830337
      y 817.6815604108675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:LARP4B"
      map_id "UNIPROT:Q92615"
      name "LARP4B_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1213"
      uniprot "UNIPROT:Q92615"
    ]
    graphics [
      x 809.4505021054168
      y 746.6882158000957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q92615"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP4B"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q92615"
      name "LARPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa71"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q92615"
    ]
    graphics [
      x 913.7929470786332
      y 727.1023526037542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q92615"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_203"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10182"
      uniprot "NA"
    ]
    graphics [
      x 1477.0805725652979
      y 865.2101816902193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:RHOA;HGNC_SYMBOL:rep"
      map_id "UNIPROT:P61586;UNIPROT:P0DTD1"
      name "RHOA7comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "UNIPROT:P61586;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1621.5150359512788
      y 965.6741176900028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61586;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785; urn:miriam:pubmed:10592235;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619;urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785"
      hgnc "HGNC_SYMBOL:RAB7A"
      map_id "UNIPROT:P51149"
      name "RAB7A; RGcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1300; csa99"
      uniprot "UNIPROT:P51149"
    ]
    graphics [
      x 1603.0308567592822
      y 475.49008135591805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_202"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10181"
      uniprot "NA"
    ]
    graphics [
      x 1847.7676761044154
      y 382.3237165126758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:8940009"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10134"
      uniprot "NA"
    ]
    graphics [
      x 1688.3366225130821
      y 163.4818482275764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:env"
      map_id "UNIPROT:A0A517FIL8"
      name "ENV"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1163"
      uniprot "UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 1841.333724808795
      y 159.66622532010433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A0A517FIL8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8940009;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122;urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:PCSK1;HGNC_SYMBOL:env"
      map_id "UNIPROT:P29120;UNIPROT:A0A517FIL8"
      name "NECENVComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P29120;UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 1753.244568499294
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P29120;UNIPROT:A0A517FIL8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:17620346;PUBMED:16679386"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_163"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10140"
      uniprot "NA"
    ]
    graphics [
      x 2016.713479331907
      y 1451.4924443803052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_234"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10213"
      uniprot "NA"
    ]
    graphics [
      x 167.06293126420587
      y 1323.8519656535861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:PEG10"
      map_id "UNIPROT:Q86TG7"
      name "PEG10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1418"
      uniprot "UNIPROT:Q86TG7"
    ]
    graphics [
      x 218.4068288222395
      y 1168.7502611763184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q86TG7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:PEG10"
      map_id "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
      name "PEG10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa130"
      uniprot "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
    ]
    graphics [
      x 62.5
      y 1229.0170238140563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505; urn:miriam:pubmed:16530182;urn:miriam:pubchem.compound:6323481;urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505"
      hgnc "HGNC_SYMBOL:SCARB1"
      map_id "UNIPROT:Q8WTV0"
      name "SCARB1; lipidcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1225; csa74"
      uniprot "UNIPROT:Q8WTV0"
    ]
    graphics [
      x 983.4169867023454
      y 1228.2353223588884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WTV0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:16530182;PUBMED:16371234;PUBMED:15541376;PUBMED:14594995;PUBMED:15791597"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_176"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10154"
      uniprot "NA"
    ]
    graphics [
      x 1037.0357464080616
      y 1443.3816730768447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6323481"
      hgnc "NA"
      map_id "Phosphatidyl_space_serine"
      name "Phosphatidyl_space_serine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1222"
      uniprot "NA"
    ]
    graphics [
      x 1058.4814100489432
      y 1590.361784501888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Phosphatidyl_space_serine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:17355872"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_232"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10211"
      uniprot "NA"
    ]
    graphics [
      x 353.90835404987126
      y 1606.500790168268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5361"
      hgnc "NA"
      map_id "Suramin"
      name "Suramin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1412"
      uniprot "NA"
    ]
    graphics [
      x 431.9823850234259
      y 1726.0025422643857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Suramin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:UBQLN4;HGNC_SYMBOL:EDN1"
      map_id "UNIPROT:Q9NRR5;UNIPROT:P05305"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:Q9NRR5;UNIPROT:P05305"
    ]
    graphics [
      x 1156.0148571639772
      y 400.7805142625707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NRR5;UNIPROT:P05305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_151"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10127"
      uniprot "NA"
    ]
    graphics [
      x 1299.3778688756192
      y 266.32449169959716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:UBQLN4"
      map_id "UNIPROT:P05305;UNIPROT:Q9NRR5"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:P05305;UNIPROT:Q9NRR5"
    ]
    graphics [
      x 1438.4149777168855
      y 240.90455910067897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05305;UNIPROT:Q9NRR5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_164"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10141"
      uniprot "NA"
    ]
    graphics [
      x 885.8044437835196
      y 945.0597879403844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_220"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10199"
      uniprot "NA"
    ]
    graphics [
      x 1396.4696851725605
      y 1152.8069047328925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:SRP54"
      map_id "UNIPROT:P61011"
      name "SRP54"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1369"
      uniprot "UNIPROT:P61011"
    ]
    graphics [
      x 1529.1405323995114
      y 1155.437599209904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61011"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP19"
      map_id "UNIPROT:P09132"
      name "SRP19"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1372"
      uniprot "UNIPROT:P09132"
    ]
    graphics [
      x 1563.8190949791351
      y 1259.3818031590733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP54;HGNC_SYMBOL:rep;HGNC_SYMBOL:SRP19"
      map_id "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
      name "SRP54comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa117"
      uniprot "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
    ]
    graphics [
      x 1575.3984499127278
      y 1179.2984039761561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_210"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10189"
      uniprot "NA"
    ]
    graphics [
      x 837.6083645895926
      y 1292.1801082583165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138"
      hgnc "HGNC_SYMBOL:AATF"
      map_id "UNIPROT:Q9NY61"
      name "AATF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1330"
      uniprot "UNIPROT:Q9NY61"
    ]
    graphics [
      x 685.0999430348966
      y 1378.4830855618943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NY61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AATF;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
      name "AATFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa107"
      uniprot "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
    ]
    graphics [
      x 757.658278505721
      y 1459.563777739328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_226"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10205"
      uniprot "NA"
    ]
    graphics [
      x 678.6840194150114
      y 1650.1133435977067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:IMPDH2"
      map_id "UNIPROT:P0C6X7;UNIPROT:P12268"
      name "INPDH2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa123"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P12268"
    ]
    graphics [
      x 733.4516126311903
      y 1771.4792243095976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0C6X7;UNIPROT:P12268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "PUBMED:15568807"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_137"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10113"
      uniprot "NA"
    ]
    graphics [
      x 526.5698683948344
      y 919.1873363533786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911; urn:miriam:pubmed:12023972;urn:miriam:pubmed:12660248;urn:miriam:pubmed:11596649;urn:miriam:pubchem.compound:34755;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911"
      hgnc "HGNC_SYMBOL:MAT2A"
      map_id "UNIPROT:P31153"
      name "MAT2A; SAdComp2"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1122; csa55"
      uniprot "UNIPROT:P31153"
    ]
    graphics [
      x 1965.3601681174414
      y 420.53833918389205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:12023972;PUBMED:12660248;PUBMED:11596649"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_155"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10132"
      uniprot "NA"
    ]
    graphics [
      x 1826.4087838736737
      y 481.1566294056816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:34755"
      hgnc "NA"
      map_id "S_minus_Adenosylmethionine"
      name "S_minus_Adenosylmethionine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1146"
      uniprot "NA"
    ]
    graphics [
      x 1914.5983565578067
      y 589.7151428907014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_Adenosylmethionine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_166"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10143"
      uniprot "NA"
    ]
    graphics [
      x 633.368720277973
      y 1108.227420436348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1126/science.abc1560"
      hgnc "NA"
      map_id "virus_underscore_replication"
      name "virus_underscore_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1194"
      uniprot "NA"
    ]
    graphics [
      x 439.9399155123108
      y 1136.627455123524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "virus_underscore_replication"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_233"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10212"
      uniprot "NA"
    ]
    graphics [
      x 242.7999290259387
      y 1111.526049812094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044"
      hgnc "HGNC_SYMBOL:ZNF250"
      map_id "UNIPROT:P15622"
      name "ZNF250"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1415"
      uniprot "UNIPROT:P15622"
    ]
    graphics [
      x 102.3089137724669
      y 1071.012523065699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15622"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:ZNF250;HGNC_SYMBOL:rep"
      map_id "UNIPROT:P15622;UNIPROT:P0C6X7"
      name "ZNF250comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa129"
      uniprot "UNIPROT:P15622;UNIPROT:P0C6X7"
    ]
    graphics [
      x 361.6489179095481
      y 902.9403604412578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15622;UNIPROT:P0C6X7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281078"
      hgnc "NA"
      map_id "Mycophenolate_space_mofetil"
      name "Mycophenolate_space_mofetil"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1401"
      uniprot "NA"
    ]
    graphics [
      x 646.0060901299767
      y 656.4518291854047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mycophenolate_space_mofetil"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:15570183"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_228"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10207"
      uniprot "NA"
    ]
    graphics [
      x 485.0109215040625
      y 656.2767222863447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429; urn:miriam:pubmed:12060674;urn:miriam:pubmed:11301045;urn:miriam:pubmed:12660248;urn:miriam:pubmed:12631701;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubchem.compound:34755"
      hgnc "HGNC_SYMBOL:MAT1A"
      map_id "UNIPROT:Q00266"
      name "MAT1A; SAdComp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1123; csa53"
      uniprot "UNIPROT:Q00266"
    ]
    graphics [
      x 2102.339990792967
      y 705.7969332397078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "PUBMED:12060674;PUBMED:11301045;PUBMED:12660248;PUBMED:12631701"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_153"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10130"
      uniprot "NA"
    ]
    graphics [
      x 1993.0797775753917
      y 690.5296985653748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_174"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10151"
      uniprot "NA"
    ]
    graphics [
      x 1108.061331277357
      y 1001.1985250110104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:ZC3H7A"
      map_id "UNIPROT:Q8IWR0"
      name "ZC3H7A_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1216"
      uniprot "UNIPROT:Q8IWR0"
    ]
    graphics [
      x 1131.6015251613624
      y 909.3750148529758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IWR0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZC3H7A"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
      name "ZC3H7Acomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa72"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
    ]
    graphics [
      x 1067.271603974699
      y 1095.3460011894056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_221"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10200"
      uniprot "NA"
    ]
    graphics [
      x 1176.3613145309666
      y 846.8305447239329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564"
      hgnc "HGNC_SYMBOL:AP2M1"
      map_id "UNIPROT:Q96CW1"
      name "AP2M1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1380"
      uniprot "UNIPROT:Q96CW1"
    ]
    graphics [
      x 1307.240672891124
      y 837.7940084593132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96CW1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AP2M1;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
      name "AP2M1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa118"
      uniprot "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1272.8778440095045
      y 945.0253283867673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10062"
      uniprot "NA"
    ]
    graphics [
      x 459.0211386136276
      y 1017.9034353756774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_206"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10185"
      uniprot "NA"
    ]
    graphics [
      x 799.9595229094684
      y 1358.1433722864308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:CCDC86"
      map_id "UNIPROT:Q9H6F5"
      name "CCDC86"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1315"
      uniprot "UNIPROT:Q9H6F5"
    ]
    graphics [
      x 634.7180942296089
      y 1448.536321442013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9H6F5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:CCDC86"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
      name "CCDCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
    ]
    graphics [
      x 686.2399024960015
      y 1488.547444811741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:32405421"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_165"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10142"
      uniprot "NA"
    ]
    graphics [
      x 557.4134847698541
      y 702.5531884853028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:8673700"
      hgnc "NA"
      map_id "RdRpassembled"
      name "RdRpassembled"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1189"
      uniprot "NA"
    ]
    graphics [
      x 374.01896451333755
      y 674.3053680854327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RdRpassembled"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:121304016"
      hgnc "NA"
      map_id "remdesivir_space_"
      name "remdesivir_space_"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1188"
      uniprot "NA"
    ]
    graphics [
      x 402.7975246755094
      y 613.389199462561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "remdesivir_space_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:492405"
      hgnc "NA"
      map_id "favipiravir"
      name "favipiravir"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1435"
      uniprot "NA"
    ]
    graphics [
      x 453.82819080869206
      y 571.1877897331426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "favipiravir"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_230"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10209"
      uniprot "NA"
    ]
    graphics [
      x 1203.6935525920385
      y 1758.055170251716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "PUBMED:17496727"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_227"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10206"
      uniprot "NA"
    ]
    graphics [
      x 785.72118032151
      y 1197.1768686354735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_144"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10120"
      uniprot "NA"
    ]
    graphics [
      x 1067.4059989382786
      y 828.1943468857085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZNF503"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q96F45"
      name "znfcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96F45"
    ]
    graphics [
      x 1195.1368135885527
      y 899.7044330609151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q96F45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:8021;urn:miriam:ncbigene:8021;urn:miriam:uniprot:P35658;urn:miriam:hgnc.symbol:NUP214;urn:miriam:hgnc.symbol:NUP214;urn:miriam:ensembl:ENSG00000126883;urn:miriam:hgnc:8064;urn:miriam:refseq:NM_005085"
      hgnc "HGNC_SYMBOL:NUP214"
      map_id "UNIPROT:P35658"
      name "NUP214"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1045"
      uniprot "UNIPROT:P35658"
    ]
    graphics [
      x 1214.26530070695
      y 1891.2381875301203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35658"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:9049309"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_140"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10116"
      uniprot "NA"
    ]
    graphics [
      x 1147.427228778452
      y 1790.9892476054065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000108559;urn:miriam:refseq:NM_002532;urn:miriam:ncbigene:4927;urn:miriam:ncbigene:4927;urn:miriam:uniprot:Q99567;urn:miriam:hgnc.symbol:NUP88;urn:miriam:hgnc.symbol:NUP88;urn:miriam:pubmed:30543681;urn:miriam:hgnc:8067"
      hgnc "HGNC_SYMBOL:NUP88"
      map_id "UNIPROT:Q99567"
      name "NUP88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1046"
      uniprot "UNIPROT:Q99567"
    ]
    graphics [
      x 1029.5232019986481
      y 1792.0865081116065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99567"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:9049309;urn:miriam:ensembl:ENSG00000108559;urn:miriam:refseq:NM_002532;urn:miriam:ncbigene:4927;urn:miriam:ncbigene:4927;urn:miriam:uniprot:Q99567;urn:miriam:hgnc.symbol:NUP88;urn:miriam:hgnc.symbol:NUP88;urn:miriam:pubmed:30543681;urn:miriam:hgnc:8067;urn:miriam:ncbigene:8021;urn:miriam:ncbigene:8021;urn:miriam:uniprot:P35658;urn:miriam:hgnc.symbol:NUP214;urn:miriam:hgnc.symbol:NUP214;urn:miriam:ensembl:ENSG00000126883;urn:miriam:hgnc:8064;urn:miriam:refseq:NM_005085"
      hgnc "HGNC_SYMBOL:NUP88;HGNC_SYMBOL:NUP214"
      map_id "UNIPROT:Q99567;UNIPROT:P35658"
      name "nup2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q99567;UNIPROT:P35658"
    ]
    graphics [
      x 1185.9566269795714
      y 1457.9482845450339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99567;UNIPROT:P35658"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_145"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10121"
      uniprot "NA"
    ]
    graphics [
      x 601.5018772456951
      y 924.9205273913296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:DCAF7"
      map_id "UNIPROT:P0DTD1;UNIPROT:P61962"
      name "dcafcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P61962"
    ]
    graphics [
      x 403.8394017694452
      y 993.916573016139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P61962"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_167"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10144"
      uniprot "NA"
    ]
    graphics [
      x 1226.6825867862212
      y 1090.5445980739157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:SBNO1"
      map_id "UNIPROT:A3KN83"
      name "SBNO1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1195"
      uniprot "UNIPROT:A3KN83"
    ]
    graphics [
      x 1319.2993211299456
      y 1178.1285121673511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A3KN83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:SBNO1"
      map_id "UNIPROT:P0DTD1;UNIPROT:A3KN83"
      name "SBNOcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa65"
      uniprot "UNIPROT:P0DTD1;UNIPROT:A3KN83"
    ]
    graphics [
      x 1335.960799497848
      y 1229.0962471034143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:A3KN83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_172"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10149"
      uniprot "NA"
    ]
    graphics [
      x 872.599792600377
      y 531.2210260889713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:RBM41"
      map_id "UNIPROT:Q96IZ5"
      name "RBM41"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1210"
      uniprot "UNIPROT:Q96IZ5"
    ]
    graphics [
      x 860.858353224388
      y 361.44044223655214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96IZ5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:RBM41"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
      name "RBMcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa70"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
    ]
    graphics [
      x 794.2884191293736
      y 373.8849306073986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:18486144;PUBMED:20196537"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_198"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10177"
      uniprot "NA"
    ]
    graphics [
      x 1868.3350170726458
      y 257.6995176195135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:34755"
      hgnc "NA"
      map_id "Ademetionine"
      name "Ademetionine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1290"
      uniprot "NA"
    ]
    graphics [
      x 1801.4650975856002
      y 136.57940932327108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ademetionine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_148"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10124"
      uniprot "NA"
    ]
    graphics [
      x 724.6675154204096
      y 1153.6449296678832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638"
      hgnc "HGNC_SYMBOL:NEK9"
      map_id "UNIPROT:Q8TD19"
      name "NEK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa979"
      uniprot "UNIPROT:Q8TD19"
    ]
    graphics [
      x 488.78176702918563
      y 1410.6491400864797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TD19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:NEK9;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
      name "nek9comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
    ]
    graphics [
      x 661.4074773349626
      y 1229.4678985537494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_156"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10133"
      uniprot "NA"
    ]
    graphics [
      x 1317.5657925128962
      y 599.9997822155082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308"
      hgnc "HGNC_SYMBOL:INS"
      map_id "UNIPROT:P01308"
      name "Insulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1160"
      uniprot "UNIPROT:P01308"
    ]
    graphics [
      x 1251.472015851369
      y 741.4138333957306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:INS;HGNC_SYMBOL:PCSK1"
      map_id "UNIPROT:P01308;UNIPROT:P29120"
      name "NECINsComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:P01308;UNIPROT:P29120"
    ]
    graphics [
      x 1192.7807005939032
      y 729.7855307908152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P01308;UNIPROT:P29120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_133"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10109"
      uniprot "NA"
    ]
    graphics [
      x 948.0491705512061
      y 627.9158336093362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:UBQLN4"
      map_id "UNIPROT:Q9NRR5"
      name "UBQLN4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1108"
      uniprot "UNIPROT:Q9NRR5"
    ]
    graphics [
      x 1062.5613597575307
      y 514.3021675035966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NRR5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:20811346;PUBMED:19601701;PUBMED:19389876;PUBMED:19920913"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_160"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10137"
      uniprot "NA"
    ]
    graphics [
      x 1233.1141920124612
      y 1408.6040634048595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6918493"
      hgnc "NA"
      map_id "Ambrisentan"
      name "Ambrisentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1172"
      uniprot "NA"
    ]
    graphics [
      x 1096.1249184455219
      y 1342.3714557773196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ambrisentan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:1170911"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_200"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10179"
      uniprot "NA"
    ]
    graphics [
      x 1707.6473434778995
      y 255.60190154254633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:66414"
      hgnc "NA"
      map_id "2_minus_Methoxyestradiol"
      name "2_minus_Methoxyestradiol"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1296"
      uniprot "NA"
    ]
    graphics [
      x 1619.2325730848788
      y 161.03304839718044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2_minus_Methoxyestradiol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_178"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10156"
      uniprot "NA"
    ]
    graphics [
      x 1404.765213114501
      y 1077.1793210243031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:CA12;HGNC_SYMBOL:rep"
      map_id "UNIPROT:O43570;UNIPROT:P0DTD1"
      name "CA12comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa76"
      uniprot "UNIPROT:O43570;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1475.2949675774091
      y 976.0412964553117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43570;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898; urn:miriam:pubmed:10592235;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619;urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898"
      hgnc "HGNC_SYMBOL:RALA"
      map_id "UNIPROT:P11233"
      name "RALA; GDPcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1270; csa90"
      uniprot "UNIPROT:P11233"
    ]
    graphics [
      x 1658.1294156470601
      y 650.1531872084845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_192"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10171"
      uniprot "NA"
    ]
    graphics [
      x 1893.1258852295637
      y 485.3271126508941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q7Z3B4;urn:miriam:refseq:NM_001278603;urn:miriam:ensembl:ENSG00000138750;urn:miriam:hgnc:17359;urn:miriam:hgnc.symbol:NUP54;urn:miriam:hgnc.symbol:NUP54;urn:miriam:ncbigene:53371;urn:miriam:ncbigene:53371"
      hgnc "HGNC_SYMBOL:NUP54"
      map_id "UNIPROT:Q7Z3B4"
      name "NUP54"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1043"
      uniprot "UNIPROT:Q7Z3B4"
    ]
    graphics [
      x 1296.4931809073985
      y 183.4674835743316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q7Z3B4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      annotation "PUBMED:12196509"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10081"
      uniprot "NA"
    ]
    graphics [
      x 1317.0613803148651
      y 341.9295768818607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9BVL2;urn:miriam:ensembl:ENSG00000139496;urn:miriam:hgnc.symbol:NUP58;urn:miriam:hgnc.symbol:NUP58;urn:miriam:ncbigene:9818;urn:miriam:ncbigene:9818;urn:miriam:refseq:NM_001008564;urn:miriam:hgnc:20261"
      hgnc "HGNC_SYMBOL:NUP58"
      map_id "UNIPROT:Q9BVL2"
      name "NUP58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1044"
      uniprot "UNIPROT:Q9BVL2"
    ]
    graphics [
      x 1191.3944946289544
      y 231.20008113316555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BVL2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000213024;urn:miriam:refseq:NM_153719;urn:miriam:uniprot:P37198;urn:miriam:ncbigene:23636;urn:miriam:ncbigene:23636;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc:8066"
      hgnc "HGNC_SYMBOL:NUP62"
      map_id "UNIPROT:P37198"
      name "NUP62"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1042"
      uniprot "UNIPROT:P37198"
    ]
    graphics [
      x 1371.581022638454
      y 199.61090061767527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P37198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12196509;urn:miriam:uniprot:Q9BVL2;urn:miriam:ensembl:ENSG00000139496;urn:miriam:hgnc.symbol:NUP58;urn:miriam:hgnc.symbol:NUP58;urn:miriam:ncbigene:9818;urn:miriam:ncbigene:9818;urn:miriam:refseq:NM_001008564;urn:miriam:hgnc:20261;urn:miriam:uniprot:Q7Z3B4;urn:miriam:refseq:NM_001278603;urn:miriam:ensembl:ENSG00000138750;urn:miriam:hgnc:17359;urn:miriam:hgnc.symbol:NUP54;urn:miriam:hgnc.symbol:NUP54;urn:miriam:ncbigene:53371;urn:miriam:ncbigene:53371;urn:miriam:ensembl:ENSG00000213024;urn:miriam:refseq:NM_153719;urn:miriam:uniprot:P37198;urn:miriam:ncbigene:23636;urn:miriam:ncbigene:23636;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc:8066"
      hgnc "HGNC_SYMBOL:NUP58;HGNC_SYMBOL:NUP54;HGNC_SYMBOL:NUP62"
      map_id "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
      name "nup1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
    ]
    graphics [
      x 1294.9469573094289
      y 662.4806848977265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_184"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10163"
      uniprot "NA"
    ]
    graphics [
      x 1055.1856405098133
      y 774.1644344840104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_004457;urn:miriam:ec-code:6.2.1.3;urn:miriam:ncbigene:2181;urn:miriam:ncbigene:2181;urn:miriam:uniprot:O95573;urn:miriam:uniprot:O95573;urn:miriam:ec-code:6.2.1.15;urn:miriam:ensembl:ENSG00000123983;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc:3570"
      hgnc "HGNC_SYMBOL:ACSL3"
      map_id "UNIPROT:O95573"
      name "ACSL3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1247"
      uniprot "UNIPROT:O95573"
    ]
    graphics [
      x 1099.7479428177921
      y 667.6997009229732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95573"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:refseq:NM_004457;urn:miriam:ec-code:6.2.1.3;urn:miriam:ncbigene:2181;urn:miriam:ncbigene:2181;urn:miriam:uniprot:O95573;urn:miriam:uniprot:O95573;urn:miriam:ec-code:6.2.1.15;urn:miriam:ensembl:ENSG00000123983;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc:3570"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ACSL3"
      map_id "UNIPROT:P0DTD1;UNIPROT:O95573"
      name "ACSLcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa82"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O95573"
    ]
    graphics [
      x 1161.5458313401841
      y 665.7721214226995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:O95573"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:8934526"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_138"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10114"
      uniprot "NA"
    ]
    graphics [
      x 927.1262359999524
      y 1287.6994620583555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat"
      map_id "UNIPROT:A6MI22;UNIPROT:P17735"
      name "TAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1063"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735"
    ]
    graphics [
      x 735.6018281429311
      y 1311.9972506380138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A6MI22;UNIPROT:P17735"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8934526;urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat;HGNC_SYMBOL:GTF2F2"
      map_id "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
      name "TAT_minus_HIV"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
    ]
    graphics [
      x 779.7771456444389
      y 1323.5620838681293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_215"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10194"
      uniprot "NA"
    ]
    graphics [
      x 865.0586392315951
      y 773.6202432650455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NARS2"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q96I59"
      name "NARS2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa112"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96I59"
    ]
    graphics [
      x 712.9771363631503
      y 752.1130675533204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q96I59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "PUBMED:10727528"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_162"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10139"
      uniprot "NA"
    ]
    graphics [
      x 1339.7676298249216
      y 1428.6800555585908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2244"
      hgnc "NA"
      map_id "Acetylsalicylic_space_acid"
      name "Acetylsalicylic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1180"
      uniprot "NA"
    ]
    graphics [
      x 1278.2367697328436
      y 1360.0736255402078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Acetylsalicylic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_186"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10165"
      uniprot "NA"
    ]
    graphics [
      x 809.0258790536977
      y 1115.3061657256278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232"
      hgnc "NA"
      map_id "ssRNAoligo"
      name "ssRNAoligo"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1253"
      uniprot "NA"
    ]
    graphics [
      x 666.09749461774
      y 1283.5502252850556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ssRNAoligo"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_158"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10135"
      uniprot "NA"
    ]
    graphics [
      x 2245.7786101845504
      y 1394.9084591943697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "Flavin_space_adenine_space_dinucleotide"
      name "Flavin_space_adenine_space_dinucleotide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1166"
      uniprot "NA"
    ]
    graphics [
      x 2129.264700587998
      y 1493.6242409831425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Flavin_space_adenine_space_dinucleotide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "PUBMED:21516116;PUBMED:12671891;PUBMED:26186194;PUBMED:28514442;PUBMED:27548429;PUBMED:25416956;PUBMED:22863883"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_125"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10097"
      uniprot "NA"
    ]
    graphics [
      x 2045.5681268515602
      y 530.68262320038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12671891;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2A;HGNC_SYMBOL:MAT1A;HGNC_SYMBOL:MAT2B"
      map_id "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
      name "MAT"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 2110.869650871241
      y 632.608240865638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_223"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10202"
      uniprot "NA"
    ]
    graphics [
      x 937.7692307429309
      y 1094.616464210892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER; urn:miriam:pubmed:10592235;urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER;urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "HGNC_SYMBOL:GFER"
      map_id "UNIPROT:P55789"
      name "GFER; FGCOMP"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1386; csa121"
      uniprot "UNIPROT:P55789"
    ]
    graphics [
      x 1023.1330845705023
      y 1018.9149696783893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55789"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:GFER"
      map_id "UNIPROT:P0DTD1;UNIPROT:P55789"
      name "GFERcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa120"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P55789"
    ]
    graphics [
      x 876.342162380846
      y 1249.8811404859798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P55789"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_170"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10147"
      uniprot "NA"
    ]
    graphics [
      x 803.1241408013502
      y 818.0772651287239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149"
      hgnc "HGNC_SYMBOL:ZNF318"
      map_id "UNIPROT:Q5VUA4"
      name "ZNF318"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1204"
      uniprot "UNIPROT:Q5VUA4"
    ]
    graphics [
      x 651.9127809054175
      y 737.9519488931601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5VUA4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:ZNF318;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
      name "ZNFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa68"
      uniprot "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 773.8598420819917
      y 909.5736428671061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_175"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10152"
      uniprot "NA"
    ]
    graphics [
      x 858.9589638198435
      y 1061.1059462421615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:MYCBP2"
      map_id "UNIPROT:O75592"
      name "MYCBP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1219"
      uniprot "UNIPROT:O75592"
    ]
    graphics [
      x 840.8312230482122
      y 1200.3485306164343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75592"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MYCBP2"
      map_id "UNIPROT:P0DTD1;UNIPROT:O75592"
      name "MYCBPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa73"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O75592"
    ]
    graphics [
      x 901.9352011857937
      y 1171.281023655665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:O75592"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_134"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10110"
      uniprot "NA"
    ]
    graphics [
      x 447.9993853975144
      y 872.3229976544199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:COPS6"
      map_id "UNIPROT:P05305;UNIPROT:Q7L5N1"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P05305;UNIPROT:Q7L5N1"
    ]
    graphics [
      x 595.1894899834532
      y 806.9727893413442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05305;UNIPROT:Q7L5N1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:17341833;PUBMED:17040106;PUBMED:17401193;PUBMED:17082011;PUBMED:16814740"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_189"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10168"
      uniprot "NA"
    ]
    graphics [
      x 939.0436648049847
      y 481.0395713224432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_177"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10155"
      uniprot "NA"
    ]
    graphics [
      x 957.3084955034767
      y 1020.8179800144527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SCARB1;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q8WTV0;UNIPROT:P0DTD1"
      name "SCARB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa75"
      uniprot "UNIPROT:Q8WTV0;UNIPROT:P0DTD1"
    ]
    graphics [
      x 879.8729952716108
      y 874.4469820837435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WTV0;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_201"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10180"
      uniprot "NA"
    ]
    graphics [
      x 1363.882279617536
      y 631.91847764639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:RAB7A;HGNC_SYMBOL:rep"
      map_id "UNIPROT:P51149;UNIPROT:P0DTD1"
      name "RAB7comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa98"
      uniprot "UNIPROT:P51149;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1360.009019431436
      y 472.5678152348614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51149;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914"
      hgnc "HGNC_SYMBOL:FOCAD"
      map_id "UNIPROT:Q5VW36"
      name "FOCAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1318"
      uniprot "UNIPROT:Q5VW36"
    ]
    graphics [
      x 800.5936765228828
      y 487.4204488838618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5VW36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_207"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10186"
      uniprot "NA"
    ]
    graphics [
      x 892.1891375762351
      y 610.8844650393985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:FOCAD"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
      name "FOCADcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa104"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
    ]
    graphics [
      x 865.4700610392575
      y 449.6459170847495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:20185318"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_180"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10158"
      uniprot "NA"
    ]
    graphics [
      x 1524.784175438043
      y 1294.4367645130508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281855"
      hgnc "NA"
      map_id "Ellagic_space_Acid"
      name "Ellagic_space_Acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1234"
      uniprot "NA"
    ]
    graphics [
      x 1475.5241402419117
      y 1154.6860818067507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ellagic_space_Acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      annotation "PUBMED:5006793"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_236"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10215"
      uniprot "NA"
    ]
    graphics [
      x 1845.0303043135616
      y 1665.761158275046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930; urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "UNIPROT:P00750"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1432; sa213; sa225"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1946.447645677446
      y 1598.767066133506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00750"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:4413"
      hgnc "NA"
      map_id "Nafamostat"
      name "Nafamostat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1434"
      uniprot "NA"
    ]
    graphics [
      x 1966.2302769715386
      y 1674.009687631035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nafamostat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_150"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10126"
      uniprot "NA"
    ]
    graphics [
      x 1161.526421927747
      y 1074.9920455265967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:GTF2F2"
      map_id "UNIPROT:P0DTD1;UNIPROT:P13984"
      name "gtf2f2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P13984"
    ]
    graphics [
      x 1217.2057948257689
      y 1182.9344278949634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P13984"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_181"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10159"
      uniprot "NA"
    ]
    graphics [
      x 1538.6483481959267
      y 1386.0901225239372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:3639"
      hgnc "NA"
      map_id "Hydrochlorothiazide"
      name "Hydrochlorothiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1237"
      uniprot "NA"
    ]
    graphics [
      x 1442.2007903529054
      y 1267.8349125944842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Hydrochlorothiazide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      annotation "PUBMED:17353931;PUBMED:26186194;PUBMED:20873783;PUBMED:12101123;PUBMED:28514442;PUBMED:12840024;PUBMED:20562859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10084"
      uniprot "NA"
    ]
    graphics [
      x 283.5631077999951
      y 1585.7477054010706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK7"
      map_id "UNIPROT:Q8TDX7"
      name "NEK7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1067"
      uniprot "UNIPROT:Q8TDX7"
    ]
    graphics [
      x 315.31793765128714
      y 1701.717110311539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDX7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408"
      hgnc "HGNC_SYMBOL:NEK6"
      map_id "UNIPROT:Q9HC98"
      name "NEK6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1066"
      uniprot "UNIPROT:Q9HC98"
    ]
    graphics [
      x 386.5323544542615
      y 1664.9054666014154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HC98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:28514442;urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK6;HGNC_SYMBOL:NEK9;HGNC_SYMBOL:NEK7"
      map_id "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
      name "NEKs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
    ]
    graphics [
      x 419.25172961176247
      y 1522.2458529735527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      annotation "PUBMED:17678888;PUBMED:16741042;PUBMED:20837776;PUBMED:11603923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10089"
      uniprot "NA"
    ]
    graphics [
      x 662.7973465075968
      y 932.7958537633191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10090"
      uniprot "NA"
    ]
    graphics [
      x 690.6978909042136
      y 983.1575101827478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "PUBMED:17194211;PUBMED:11481605"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_212"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10191"
      uniprot "NA"
    ]
    graphics [
      x 716.6901471332153
      y 1258.4650027097982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051"
      hgnc "NA"
      map_id "Pyridoxal_space_phosphate"
      name "Pyridoxal_space_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1340"
      uniprot "NA"
    ]
    graphics [
      x 733.6486191246479
      y 1406.791784806294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pyridoxal_space_phosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_219"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10198"
      uniprot "NA"
    ]
    graphics [
      x 1056.4643719820488
      y 701.357627905129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206"
      hgnc "HGNC_SYMBOL:DDX10"
      map_id "UNIPROT:Q13206"
      name "DDX10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1366"
      uniprot "UNIPROT:Q13206"
    ]
    graphics [
      x 1147.0996904155859
      y 592.3364918155764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:DDX10;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q13206;UNIPROT:P0DTD1"
      name "DDX10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa116"
      uniprot "UNIPROT:Q13206;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1084.4780348959537
      y 574.7881888114889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13206;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_191"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10170"
      uniprot "NA"
    ]
    graphics [
      x 1396.0864932062796
      y 822.2097342568522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:RALA"
      map_id "UNIPROT:P0DTD1;UNIPROT:P11233"
      name "RALAcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa89"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P11233"
    ]
    graphics [
      x 1513.3400843035417
      y 790.2710416068212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:P11233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_139"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10115"
      uniprot "NA"
    ]
    graphics [
      x 1281.2626626188032
      y 1039.135388438978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_147"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10123"
      uniprot "NA"
    ]
    graphics [
      x 1436.70932928896
      y 922.12037516632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:rep"
      map_id "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
      name "spartcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1570.836167265115
      y 852.9478251892901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      annotation "PUBMED:11873938;PUBMED:12538800;PUBMED:11939936;PUBMED:11440283;PUBMED:10439935;PUBMED:10882160;PUBMED:10981253;PUBMED:11752352;PUBMED:12876237;PUBMED:9808337"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_196"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10175"
      uniprot "NA"
    ]
    graphics [
      x 1959.3571868078611
      y 562.2549557089999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281081"
      hgnc "NA"
      map_id "Entacapone"
      name "Entacapone"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1284"
      uniprot "NA"
    ]
    graphics [
      x 2009.6791477336305
      y 734.7912111513195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Entacapone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_222"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10201"
      uniprot "NA"
    ]
    graphics [
      x 771.3940418956964
      y 611.8335581175463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:ERGIC1"
      map_id "UNIPROT:Q969X5"
      name "ERGIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1383"
      uniprot "UNIPROT:Q969X5"
    ]
    graphics [
      x 680.3787290434877
      y 424.12264221308374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q969X5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ERGIC1"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q969X5"
      name "ERGIC1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa119"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q969X5"
    ]
    graphics [
      x 677.2401431298886
      y 495.0914110353369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q969X5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      annotation "PUBMED:14962394"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_131"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10107"
      uniprot "NA"
    ]
    graphics [
      x 901.9261940407979
      y 1011.3131427417011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:BAG6"
      map_id "UNIPROT:P46379"
      name "BAG6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1077"
      uniprot "UNIPROT:P46379"
    ]
    graphics [
      x 261.40947697855836
      y 1287.047442281276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P46379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_136"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10112"
      uniprot "NA"
    ]
    graphics [
      x 320.9872688910435
      y 1135.5968364690298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:BAG6"
      map_id "UNIPROT:P05305;UNIPROT:P46379"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P05305;UNIPROT:P46379"
    ]
    graphics [
      x 229.93964848339556
      y 1240.46433263238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P05305;UNIPROT:P46379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_205"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10184"
      uniprot "NA"
    ]
    graphics [
      x 1239.3492383476384
      y 817.8436131509582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:MPHOSPH10"
      map_id "UNIPROT:O00566"
      name "MPHOSPH10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1312"
      uniprot "UNIPROT:O00566"
    ]
    graphics [
      x 1389.3551231264612
      y 768.8278960114543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O00566"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MPHOSPH10"
      map_id "UNIPROT:P0DTD1;UNIPROT:O00566"
      name "MPHOSPHcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa102"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O00566"
    ]
    graphics [
      x 1331.094811422602
      y 729.9338814177862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:O00566"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_224"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10203"
      uniprot "NA"
    ]
    graphics [
      x 1063.711594791463
      y 893.7251086115072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_182"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10161"
      uniprot "NA"
    ]
    graphics [
      x 1832.3096049929618
      y 1229.314324540467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:70876165"
      hgnc "NA"
      map_id "Hydroflumethiazide"
      name "Hydroflumethiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1241"
      uniprot "NA"
    ]
    graphics [
      x 1825.832711520794
      y 1076.6213138214496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Hydroflumethiazide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_213"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10192"
      uniprot "NA"
    ]
    graphics [
      x 1208.9290457616407
      y 1284.67855740231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:NOL10"
      map_id "UNIPROT:Q9BSC4"
      name "NOL10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1343"
      uniprot "UNIPROT:Q9BSC4"
    ]
    graphics [
      x 1242.4918093111366
      y 1472.5032759899887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BSC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NOL10"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
      name "NOL10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa110"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
    ]
    graphics [
      x 1286.5065780159532
      y 1436.6609467178464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_183"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10162"
      uniprot "NA"
    ]
    graphics [
      x 1864.1083499107126
      y 1411.6656490525365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2343"
      hgnc "NA"
      map_id "Benzthiazide"
      name "Benzthiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1244"
      uniprot "NA"
    ]
    graphics [
      x 1986.7915799485543
      y 1377.3579852057278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Benzthiazide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_169"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10146"
      uniprot "NA"
    ]
    graphics [
      x 1502.595624993969
      y 1021.3254843559899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10087"
      uniprot "NA"
    ]
    graphics [
      x 479.0032668256906
      y 963.174633825073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_171"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10148"
      uniprot "NA"
    ]
    graphics [
      x 979.359555278962
      y 1349.3402504707835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:TCF12"
      map_id "UNIPROT:Q99081"
      name "TCF12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1207"
      uniprot "UNIPROT:Q99081"
    ]
    graphics [
      x 979.8973213813048
      y 1482.392758533164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q99081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:TCF12"
      map_id "UNIPROT:P0DTD1;UNIPROT:Q99081"
      name "TCFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa69"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q99081"
    ]
    graphics [
      x 933.3404063795388
      y 1515.1413665835462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTD1;UNIPROT:Q99081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_187"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10166"
      uniprot "NA"
    ]
    graphics [
      x 840.3689833093898
      y 1392.508767450273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:NEMF"
      map_id "UNIPROT:O60524"
      name "NEMF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1376"
      uniprot "UNIPROT:O60524"
    ]
    graphics [
      x 795.3707787907346
      y 1539.4313330614664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O60524"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_199"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10178"
      uniprot "NA"
    ]
    graphics [
      x 1866.283331277516
      y 533.2311545283974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6914595"
      hgnc "NA"
      map_id "(3,4_minus_DIHYDROXY_minus_2_minus_NITROPHENYL)(PHENYL)METHANONE"
      name "(3,4_minus_DIHYDROXY_minus_2_minus_NITROPHENYL)(PHENYL)METHANONE"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1293"
      uniprot "NA"
    ]
    graphics [
      x 1898.5294502427514
      y 660.9330578095763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(3,4_minus_DIHYDROXY_minus_2_minus_NITROPHENYL)(PHENYL)METHANONE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      annotation "PUBMED:24261583;PUBMED:22862294;PUBMED:22458347"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_161"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10138"
      uniprot "NA"
    ]
    graphics [
      x 1477.5847322659731
      y 1615.3450169488183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:16004692"
      hgnc "NA"
      map_id "Macitentan"
      name "Macitentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1177"
      uniprot "NA"
    ]
    graphics [
      x 1534.8368636304986
      y 1753.8372879362173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Macitentan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      annotation "PUBMED:11873938;PUBMED:17063156;PUBMED:12538800;PUBMED:20502133;PUBMED:10882160;PUBMED:18046910;PUBMED:9917075;PUBMED:19503773;PUBMED:11752352;PUBMED:15697329"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_195"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10174"
      uniprot "NA"
    ]
    graphics [
      x 1798.695936457757
      y 659.6758486964518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:4659569"
      hgnc "NA"
      map_id "Tolcapone"
      name "Tolcapone"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1281"
      uniprot "NA"
    ]
    graphics [
      x 1818.4006051370227
      y 840.6310251664999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Tolcapone"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 323
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
      target_id "M115_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_193"
      target_id "Neutrophil_underscore_activation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13984"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00403"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 18
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30876;UNIPROT:P24928"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 19
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P30876"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 20
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P19388"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 21
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P62487"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_119"
      target_id "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 3
    target 23
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Nuclear_space_Pore"
      target_id "M115_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UBX5"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q08397"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 4
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_126"
      target_id "UNIPROT:Q9UBX5;UNIPROT:Q08397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35354"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Rofecoxib"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96F45"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61962"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 33
    target 35
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_142"
      target_id "UNIPROT:Q96F45;UNIPROT:P61962"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 38
    target 37
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25101"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 37
    target 39
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_132"
      target_id "UNIPROT:P25101;UNIPROT:P05305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 3
    target 40
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HD40"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_211"
      target_id "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96I59"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Asparagine"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 3
    target 46
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35555"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 25
    target 46
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UBX5"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 4
    target 46
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_149"
      target_id "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 3
    target 49
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8NEJ9"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 49
    target 51
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_217"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86YT6"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00548"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_128"
      target_id "UNIPROT:O00548;UNIPROT:Q86YT6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 36
    target 56
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 34
    target 56
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P61962"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 57
    target 58
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N0X7"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95831"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 58
    target 60
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_130"
      target_id "UNIPROT:Q8N0X7;UNIPROT:O95831"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 3
    target 61
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O76094"
      target_id "M115_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 61
    target 63
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_214"
      target_id "UNIPROT:O76094;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61201"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 67
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92905"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 68
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7L5N1"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 69
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9UBW8"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 70
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99627"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 65
    target 71
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_124"
      target_id "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 3
    target 72
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14874"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_168"
      target_id "UNIPROT:P0DTD1;UNIPROT:O14874"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:I2A5W5"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 71
    target 76
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_135"
      target_id "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 8
    target 78
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Mercaptopurine"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 3
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 52
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86YT6"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_143"
      target_id "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "3,5_minus_Dinitrocatechol"
      target_id "M115_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Zonisamide"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 3
    target 88
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZL9"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 88
    target 90
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_127"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 1
    target 91
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 9
    target 91
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NXA8"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_231"
      target_id "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 3
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O94973"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_225"
      target_id "UNIPROT:P0DTD1;UNIPROT:O94973"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 3
    target 96
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q15056"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 96
    target 98
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_146"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q15056"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 3
    target 99
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 100
    target 99
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00387"
      target_id "M115_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 99
    target 101
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_188"
      target_id "UNIPROT:P0DTD1;UNIPROT:P00387"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 100
    target 102
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00387"
      target_id "M115_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "FAD"
      target_id "M115_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 3
    target 104
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7L2J0"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 106
    target 104
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q4G0J3"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 104
    target 107
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_218"
      target_id "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 3
    target 108
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 10
    target 109
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Mycophenolic_space_acid"
      target_id "M115_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 38
    target 110
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25101"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Sitaxentan"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P98095;UNIPROT:P35555"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P15502"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 4
    target 113
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 113
    target 115
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_152"
      target_id "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13868"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 3
    target 117
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 118
    target 117
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NQT5"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 119
    target 117
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NQT4"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 120
    target 117
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96B26"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 117
    target 121
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_208"
      target_id "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P82675"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 3
    target 123
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9Y399"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 123
    target 125
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_209"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 6
    target 126
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61586"
      target_id "M115_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 7
    target 126
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M115_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 89
    target 127
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZL9"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29120"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_129"
      target_id "UNIPROT:Q9NZL9;UNIPROT:P29120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 3
    target 130
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 82
    target 130
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_194"
      target_id "UNIPROT:P21964;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 3
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 133
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q92615"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 132
    target 134
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_173"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q92615"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 3
    target 135
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 6
    target 135
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61586"
      target_id "M115_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_203"
      target_id "UNIPROT:P61586;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51149"
      target_id "M115_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 7
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M115_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 128
    target 139
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29120"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:A0A517FIL8"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 139
    target 141
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_157"
      target_id "UNIPROT:P29120;UNIPROT:A0A517FIL8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 114
    target 142
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P15502"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 31
    target 142
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Rofecoxib"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 1
    target 143
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q86TG7"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 143
    target 145
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_234"
      target_id "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8WTV0"
      target_id "M115_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Phosphatidyl_space_serine"
      target_id "M115_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 9
    target 149
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NXA8"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 150
    target 149
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Suramin"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NRR5;UNIPROT:P05305"
      target_id "M115_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_151"
      target_id "UNIPROT:P05305;UNIPROT:Q9NRR5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 3
    target 154
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 3
    target 155
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 156
    target 155
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61011"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 157
    target 155
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P09132"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 155
    target 158
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_220"
      target_id "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 3
    target 159
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 160
    target 159
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NY61"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 159
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_210"
      target_id "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 1
    target 162
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 8
    target 162
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_226"
      target_id "UNIPROT:P0C6X7;UNIPROT:P12268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 36
    target 164
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P31153"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 167
    target 166
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "S_minus_Adenosylmethionine"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 3
    target 168
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 168
    target 169
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_166"
      target_id "virus_underscore_replication"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 1
    target 170
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P15622"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 170
    target 172
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_233"
      target_id "UNIPROT:P15622;UNIPROT:P0C6X7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 173
    target 174
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Mycophenolate_space_mofetil"
      target_id "M115_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 174
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_228"
      target_id "Mycophenolic_space_acid"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00266"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 167
    target 176
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "S_minus_Adenosylmethionine"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 3
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 178
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8IWR0"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 177
    target 179
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_174"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 3
    target 180
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96CW1"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 180
    target 182
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_221"
      target_id "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 36
    target 183
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 3
    target 184
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 185
    target 184
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9H6F5"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 184
    target 186
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_206"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 3
    target 187
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 188
    target 187
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "RdRpassembled"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 189
    target 187
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "remdesivir_space_"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 190
    target 187
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "favipiravir"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 8
    target 191
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 11
    target 191
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Ribavirin"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 8
    target 192
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 10
    target 192
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Mycophenolic_space_acid"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 3
    target 193
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 32
    target 193
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96F45"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 193
    target 194
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_144"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q96F45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35658"
      target_id "M115_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99567"
      target_id "M115_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 196
    target 198
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_140"
      target_id "UNIPROT:Q99567;UNIPROT:P35658"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 3
    target 199
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 34
    target 199
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P61962"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 199
    target 200
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_145"
      target_id "UNIPROT:P0DTD1;UNIPROT:P61962"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 3
    target 201
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 202
    target 201
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:A3KN83"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_167"
      target_id "UNIPROT:P0DTD1;UNIPROT:A3KN83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 3
    target 204
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 205
    target 204
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96IZ5"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 204
    target 206
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_172"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 82
    target 207
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Ademetionine"
      target_id "M115_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 3
    target 209
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 210
    target 209
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TD19"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 209
    target 211
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_148"
      target_id "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 128
    target 212
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P29120"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 213
    target 212
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P01308"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 212
    target 214
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_156"
      target_id "UNIPROT:P01308;UNIPROT:P29120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 36
    target 215
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 216
    target 215
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NRR5"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 215
    target 151
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_133"
      target_id "UNIPROT:Q9NRR5;UNIPROT:P05305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 38
    target 217
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25101"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 218
    target 217
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Ambrisentan"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 82
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 220
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "2_minus_Methoxyestradiol"
      target_id "M115_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 3
    target 221
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 85
    target 221
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_178"
      target_id "UNIPROT:O43570;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11233"
      target_id "M115_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 7
    target 224
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M115_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 225
    target 226
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7Z3B4"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 227
    target 226
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BVL2"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 228
    target 226
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P37198"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 226
    target 229
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_118"
      target_id "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 3
    target 230
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 231
    target 230
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95573"
      target_id "M115_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 230
    target 232
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_184"
      target_id "UNIPROT:P0DTD1;UNIPROT:O95573"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 15
    target 233
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13984"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 234
    target 233
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:A6MI22;UNIPROT:P17735"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 233
    target 235
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_138"
      target_id "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 3
    target 236
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 43
    target 236
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q96I59"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_215"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q96I59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 38
    target 238
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25101"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 239
    target 238
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Acetylsalicylic_space_acid"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 3
    target 240
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 241
    target 240
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "ssRNAoligo"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 59
    target 242
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O95831"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 243
    target 242
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Flavin_space_adenine_space_dinucleotide"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 175
    target 244
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00266"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 165
    target 244
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P31153"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 89
    target 244
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NZL9"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 244
    target 245
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_125"
      target_id "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 3
    target 246
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 247
    target 246
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55789"
      target_id "M115_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 246
    target 248
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_223"
      target_id "UNIPROT:P0DTD1;UNIPROT:P55789"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 3
    target 249
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 250
    target 249
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q5VUA4"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 249
    target 251
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_170"
      target_id "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 3
    target 252
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 253
    target 252
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O75592"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 252
    target 254
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_175"
      target_id "UNIPROT:P0DTD1;UNIPROT:O75592"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 36
    target 255
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 68
    target 255
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q7L5N1"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 255
    target 256
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_134"
      target_id "UNIPROT:P05305;UNIPROT:Q7L5N1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 100
    target 257
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00387"
      target_id "M115_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 5
    target 257
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "NADH"
      target_id "M115_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 3
    target 258
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 146
    target 258
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8WTV0"
      target_id "M115_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 258
    target 259
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_177"
      target_id "UNIPROT:Q8WTV0;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 3
    target 260
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 137
    target 260
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P51149"
      target_id "M115_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 260
    target 261
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_201"
      target_id "UNIPROT:P51149;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 262
    target 263
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q5VW36"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 3
    target 263
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 263
    target 264
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_207"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 85
    target 265
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 266
    target 265
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Ellagic_space_Acid"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 4
    target 267
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P00747"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 268
    target 267
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "UNIPROT:P00750"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 269
    target 267
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "Nafamostat"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 3
    target 270
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 15
    target 270
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P13984"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 270
    target 271
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_150"
      target_id "UNIPROT:P0DTD1;UNIPROT:P13984"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 85
    target 272
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 273
    target 272
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Hydrochlorothiazide"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 210
    target 274
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TD19"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 275
    target 274
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDX7"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 276
    target 274
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HC98"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 274
    target 277
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_120"
      target_id "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 36
    target 278
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 36
    target 279
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 41
    target 280
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9HD40"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 281
    target 280
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Pyridoxal_space_phosphate"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 3
    target 282
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 283
    target 282
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q13206"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 282
    target 284
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_219"
      target_id "UNIPROT:Q13206;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 3
    target 285
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 223
    target 285
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P11233"
      target_id "M115_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 285
    target 286
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_191"
      target_id "UNIPROT:P0DTD1;UNIPROT:P11233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 198
    target 287
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99567;UNIPROT:P35658"
      target_id "M115_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 229
    target 287
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
      target_id "M115_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 287
    target 24
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_139"
      target_id "Nuclear_space_Pore"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 3
    target 288
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 57
    target 288
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8N0X7"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 288
    target 289
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_147"
      target_id "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 82
    target 290
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 291
    target 290
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Entacapone"
      target_id "M115_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 3
    target 292
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 293
    target 292
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q969X5"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 292
    target 294
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_222"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q969X5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 3
    target 295
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 296
    target 297
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P46379"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 36
    target 297
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 297
    target 298
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_136"
      target_id "UNIPROT:P05305;UNIPROT:P46379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 3
    target 299
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 300
    target 299
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O00566"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 299
    target 301
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_205"
      target_id "UNIPROT:P0DTD1;UNIPROT:O00566"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 247
    target 302
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P55789"
      target_id "M115_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 103
    target 302
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "FAD"
      target_id "M115_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 85
    target 303
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 304
    target 303
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Hydroflumethiazide"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 3
    target 305
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 306
    target 305
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9BSC4"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 305
    target 307
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_213"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 85
    target 308
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O43570"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 309
    target 308
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Benzthiazide"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 73
    target 310
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O14874"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 2
    target 310
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "ADP"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 36
    target 311
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P05305"
      target_id "M115_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 3
    target 312
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 313
    target 312
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q99081"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 312
    target 314
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_171"
      target_id "UNIPROT:P0DTD1;UNIPROT:Q99081"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 3
    target 315
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0DTD1"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 1
    target 315
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P0C6X7"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 316
    target 315
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:O60524"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 315
    target 12
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_187"
      target_id "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 82
    target 317
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 318
    target 317
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "(3,4_minus_DIHYDROXY_minus_2_minus_NITROPHENYL)(PHENYL)METHANONE"
      target_id "M115_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 38
    target 319
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P25101"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 320
    target 319
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Macitentan"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 82
    target 321
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P21964"
      target_id "M115_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 322
    target 321
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "Tolcapone"
      target_id "M115_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
