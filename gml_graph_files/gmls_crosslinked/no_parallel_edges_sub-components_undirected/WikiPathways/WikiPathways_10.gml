# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 9
      diagram "WP4861; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P18850; urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "NA; HGNC_SYMBOL:ATF6"
      map_id "UNIPROT:P18850"
      name "ATF6; ATF6_minus_p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bfd13; b27d0; b1cbb; f77a3; path_0_sa3; path_0_sa65; path_0_sa62; path_0_sa64; path_0_sa57"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1534.0991110010664
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P18850"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8122cdf4"
      uniprot "NA"
    ]
    graphics [
      x 1353.754086247907
      y 842.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3b399cfb"
      uniprot "NA"
    ]
    graphics [
      x 1715.6073124686477
      y 332.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbbefaff4"
      uniprot "NA"
    ]
    graphics [
      x 1405.095293077272
      y 422.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:11163209"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7f4a3b95"
      uniprot "NA"
    ]
    graphics [
      x 1314.069064279899
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "MVH_space_infection"
      name "MVH_space_infection"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c7e0a"
      uniprot "NA"
    ]
    graphics [
      x 1283.6403060410005
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MVH_space_infection"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:O43462;urn:miriam:uniprot:Q14703"
      hgnc "NA"
      map_id "UNIPROT:O43462;UNIPROT:Q14703"
      name "cec30"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cec30"
      uniprot "UNIPROT:O43462;UNIPROT:Q14703"
    ]
    graphics [
      x 1280.3731363999382
      y 2012.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O43462;UNIPROT:Q14703"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "ERSE_minus_II_br_ER_space_stress_space__br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      name "ERSE_minus_II_br_ER_space_stress_space__br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b5637"
      uniprot "NA"
    ]
    graphics [
      x 1535.6009635950331
      y 1232.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ERSE_minus_II_br_ER_space_stress_space__br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idcbd96fe9"
      uniprot "NA"
    ]
    graphics [
      x 1661.918563889751
      y 2155.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:ensembl:ENSG00000175197;urn:miriam:ensembl:ENSG00000100219"
      hgnc "NA"
      map_id "c9bf8"
      name "c9bf8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c9bf8"
      uniprot "NA"
    ]
    graphics [
      x 1626.3289195177035
      y 2077.835760147579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c9bf8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P17861;urn:miriam:uniprot:P35638"
      hgnc "NA"
      map_id "UNIPROT:P17861;UNIPROT:P35638"
      name "ee104"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ee104"
      uniprot "UNIPROT:P17861;UNIPROT:P35638"
    ]
    graphics [
      x 1788.555614925871
      y 2455.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P17861;UNIPROT:P35638"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 12
    source 1
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "W3_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 1
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "W3_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 1
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "W3_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 1
    target 5
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P18850"
      target_id "W3_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 4
    target 8
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_76"
      target_id "ERSE_minus_II_br_ER_space_stress_space__br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 6
    target 5
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "MVH_space_infection"
      target_id "W3_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 7
    target 5
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O43462;UNIPROT:Q14703"
      target_id "W3_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 8
    target 9
    cd19dm [
      diagram "WP4861"
      edge_type "PHYSICAL_STIMULATION"
      source_id "ERSE_minus_II_br_ER_space_stress_space__br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      target_id "W3_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 10
    target 9
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "c9bf8"
      target_id "W3_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 9
    target 11
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_81"
      target_id "UNIPROT:P17861;UNIPROT:P35638"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
