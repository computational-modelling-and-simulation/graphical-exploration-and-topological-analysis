# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000110713;urn:miriam:ensembl:ENSG00000101146"
      hgnc "NA"
      map_id "ef9ac"
      name "ef9ac"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ef9ac"
      uniprot "NA"
    ]
    graphics [
      x 1180.1958284362465
      y 3812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ef9ac"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32529952"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_16"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ab09f"
      uniprot "NA"
    ]
    graphics [
      x 1080.895420615159
      y 3992.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q89226299"
      hgnc "NA"
      map_id "UNIPROT:Q89226299"
      name "ORF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6ec9"
      uniprot "UNIPROT:Q89226299"
    ]
    graphics [
      x 1021.3311657046356
      y 3812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q89226299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "ef9ac"
      target_id "W21_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_16"
      target_id "UNIPROT:Q89226299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
