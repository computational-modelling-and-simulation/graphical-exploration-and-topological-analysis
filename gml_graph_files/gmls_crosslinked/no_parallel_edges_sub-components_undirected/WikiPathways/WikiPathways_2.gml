# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740572; urn:miriam:wikidata:Q89226299"
      hgnc "NA"
      map_id "ORF6"
      name "ORF6"
      node_subtype "GENE; PROTEIN"
      node_type "species"
      org_id "fc02d; bce0b"
      uniprot "NA"
    ]
    graphics [
      x 2018.528374273356
      y 2005.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ORF6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_144"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida427ca0"
      uniprot "NA"
    ]
    graphics [
      x 2181.38164667478
      y 1795.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 3
    source 1
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "ORF6"
      target_id "W1_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
