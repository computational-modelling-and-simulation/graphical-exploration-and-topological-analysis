# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "Substrate"
      name "Substrate"
      node_subtype "GENE"
      node_type "species"
      org_id "d1f64"
      uniprot "NA"
    ]
    graphics [
      x 1336.5328171465035
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Substrate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id92c39589"
      uniprot "NA"
    ]
    graphics [
      x 1297.1372451892958
      y 1172.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:wikipathways:WP183"
      hgnc "NA"
      map_id "Proteasome_space_degradation"
      name "Proteasome_space_degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5bba"
      uniprot "NA"
    ]
    graphics [
      x 1427.8022995613371
      y 992.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Proteasome_space_degradation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "Substrate"
      target_id "W5_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_5"
      target_id "Proteasome_space_degradation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
