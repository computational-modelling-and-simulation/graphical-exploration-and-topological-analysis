# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q9ULJ8;urn:miriam:uniprot:Q13522;urn:miriam:uniprot:Q12972;urn:miriam:uniprot:Q96I34;urn:miriam:uniprot:Q6GQY8;urn:miriam:uniprot:P36873;urn:miriam:uniprot:Q96A00;urn:miriam:uniprot:Q5SWA1;urn:miriam:uniprot:Q96QC0;urn:miriam:uniprot:O75807;urn:miriam:uniprot:Q16821;urn:miriam:uniprot:D3DTX6;urn:miriam:uniprot:P62136;urn:miriam:uniprot:Q8WVI7;urn:miriam:uniprot:Q9UQK1;urn:miriam:uniprot:O14974;urn:miriam:uniprot:Q96KQ4;urn:miriam:uniprot:Q6ZSY5;urn:miriam:uniprot:Q8TAE6;urn:miriam:uniprot:Q96T49;urn:miriam:uniprot:Q9H7J1;urn:miriam:uniprot:Q9BZL4;urn:miriam:uniprot:Q9NXH3;urn:miriam:uniprot:Q15435;urn:miriam:uniprot:Q9UD71;urn:miriam:uniprot:P62140;urn:miriam:uniprot:O95685;urn:miriam:uniprot:Q5SRK2;urn:miriam:uniprot:Q96C90;urn:miriam:uniprot:B7ZBB8;urn:miriam:uniprot:Q86XI6;urn:miriam:uniprot:P41236"
      hgnc "NA"
      map_id "UNIPROT:Q9ULJ8;UNIPROT:Q13522;UNIPROT:Q12972;UNIPROT:Q96I34;UNIPROT:Q6GQY8;UNIPROT:P36873;UNIPROT:Q96A00;UNIPROT:Q5SWA1;UNIPROT:Q96QC0;UNIPROT:O75807;UNIPROT:Q16821;UNIPROT:D3DTX6;UNIPROT:P62136;UNIPROT:Q8WVI7;UNIPROT:Q9UQK1;UNIPROT:O14974;UNIPROT:Q96KQ4;UNIPROT:Q6ZSY5;UNIPROT:Q8TAE6;UNIPROT:Q96T49;UNIPROT:Q9H7J1;UNIPROT:Q9BZL4;UNIPROT:Q9NXH3;UNIPROT:Q15435;UNIPROT:Q9UD71;UNIPROT:P62140;UNIPROT:O95685;UNIPROT:Q5SRK2;UNIPROT:Q96C90;UNIPROT:B7ZBB8;UNIPROT:Q86XI6;UNIPROT:P41236"
      name "PP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "de11e"
      uniprot "UNIPROT:Q9ULJ8;UNIPROT:Q13522;UNIPROT:Q12972;UNIPROT:Q96I34;UNIPROT:Q6GQY8;UNIPROT:P36873;UNIPROT:Q96A00;UNIPROT:Q5SWA1;UNIPROT:Q96QC0;UNIPROT:O75807;UNIPROT:Q16821;UNIPROT:D3DTX6;UNIPROT:P62136;UNIPROT:Q8WVI7;UNIPROT:Q9UQK1;UNIPROT:O14974;UNIPROT:Q96KQ4;UNIPROT:Q6ZSY5;UNIPROT:Q8TAE6;UNIPROT:Q96T49;UNIPROT:Q9H7J1;UNIPROT:Q9BZL4;UNIPROT:Q9NXH3;UNIPROT:Q15435;UNIPROT:Q9UD71;UNIPROT:P62140;UNIPROT:O95685;UNIPROT:Q5SRK2;UNIPROT:Q96C90;UNIPROT:B7ZBB8;UNIPROT:Q86XI6;UNIPROT:P41236"
    ]
    graphics [
      x 821.4027163088865
      y 512.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9ULJ8;UNIPROT:Q13522;UNIPROT:Q12972;UNIPROT:Q96I34;UNIPROT:Q6GQY8;UNIPROT:P36873;UNIPROT:Q96A00;UNIPROT:Q5SWA1;UNIPROT:Q96QC0;UNIPROT:O75807;UNIPROT:Q16821;UNIPROT:D3DTX6;UNIPROT:P62136;UNIPROT:Q8WVI7;UNIPROT:Q9UQK1;UNIPROT:O14974;UNIPROT:Q96KQ4;UNIPROT:Q6ZSY5;UNIPROT:Q8TAE6;UNIPROT:Q96T49;UNIPROT:Q9H7J1;UNIPROT:Q9BZL4;UNIPROT:Q9NXH3;UNIPROT:Q15435;UNIPROT:Q9UD71;UNIPROT:P62140;UNIPROT:O95685;UNIPROT:Q5SRK2;UNIPROT:Q96C90;UNIPROT:B7ZBB8;UNIPROT:Q86XI6;UNIPROT:P41236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_84"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idda829af2"
      uniprot "NA"
    ]
    graphics [
      x 913.6110938886792
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:wikidata:Q7251492"
      hgnc "NA"
      map_id "PP1"
      name "PP1"
      node_subtype "GENE"
      node_type "species"
      org_id "f8679"
      uniprot "NA"
    ]
    graphics [
      x 1120.971204452082
      y 152.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9ULJ8;UNIPROT:Q13522;UNIPROT:Q12972;UNIPROT:Q96I34;UNIPROT:Q6GQY8;UNIPROT:P36873;UNIPROT:Q96A00;UNIPROT:Q5SWA1;UNIPROT:Q96QC0;UNIPROT:O75807;UNIPROT:Q16821;UNIPROT:D3DTX6;UNIPROT:P62136;UNIPROT:Q8WVI7;UNIPROT:Q9UQK1;UNIPROT:O14974;UNIPROT:Q96KQ4;UNIPROT:Q6ZSY5;UNIPROT:Q8TAE6;UNIPROT:Q96T49;UNIPROT:Q9H7J1;UNIPROT:Q9BZL4;UNIPROT:Q9NXH3;UNIPROT:Q15435;UNIPROT:Q9UD71;UNIPROT:P62140;UNIPROT:O95685;UNIPROT:Q5SRK2;UNIPROT:Q96C90;UNIPROT:B7ZBB8;UNIPROT:Q86XI6;UNIPROT:P41236"
      target_id "W3_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_84"
      target_id "PP1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
