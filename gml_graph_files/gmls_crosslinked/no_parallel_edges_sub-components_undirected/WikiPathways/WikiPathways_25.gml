# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:55922"
      hgnc "NA"
      map_id "NKRF"
      name "NKRF"
      node_subtype "GENE"
      node_type "species"
      org_id "ea9bc"
      uniprot "NA"
    ]
    graphics [
      x 745.85590081091
      y 2043.636410062999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NKRF"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8e00894d"
      uniprot "NA"
    ]
    graphics [
      x 1332.0776284369183
      y 2274.7754434484614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "acca6"
      uniprot "NA"
    ]
    graphics [
      x 300.6049916048904
      y 2102.2888198336295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_9"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c049f"
      uniprot "NA"
    ]
    graphics [
      x 361.5964887191898
      y 1709.4348912494797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "cb292"
      uniprot "NA"
    ]
    graphics [
      x 387.2132939769831
      y 2022.3532296893236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aff81"
      uniprot "NA"
    ]
    graphics [
      x 541.9548299158315
      y 1499.4348912494797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2a0ea132"
      uniprot "NA"
    ]
    graphics [
      x 722.8292057828955
      y 2249.4348912494797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd72cdda3"
      uniprot "NA"
    ]
    graphics [
      x 776.6110456707686
      y 2189.4348912494797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_1"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "aa453"
      uniprot "NA"
    ]
    graphics [
      x 1518.9795155965996
      y 2335.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3497;urn:miriam:ncbigene:3500;urn:miriam:ncbigene:3501;urn:miriam:ncbigene:3543;urn:miriam:ncbigene:3538;urn:miriam:ncbigene:3503"
      hgnc "NA"
      map_id "b7f53"
      name "b7f53"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7f53"
      uniprot "NA"
    ]
    graphics [
      x 1783.3215832438855
      y 2575.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b7f53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:5725029;urn:miriam:ncbigene:3929;urn:miriam:ncbigene:10394;urn:miriam:ncbigene:1401;urn:miriam:ncbigene:64386"
      hgnc "NA"
      map_id "b120d"
      name "b120d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b120d"
      uniprot "NA"
    ]
    graphics [
      x 755.0481811515674
      y 1993.8241014676514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b120d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:920;urn:miriam:ncbigene:926;urn:miriam:ncbigene:925;urn:miriam:ncbigene:914"
      hgnc "NA"
      map_id "cb65d"
      name "cb65d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cb65d"
      uniprot "NA"
    ]
    graphics [
      x 780.2985271960788
      y 2311.0605799811333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cb65d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3569"
      hgnc "NA"
      map_id "bebbf"
      name "bebbf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bebbf"
      uniprot "NA"
    ]
    graphics [
      x 394.470398721357
      y 1529.4348912494797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bebbf"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:919;urn:miriam:ncbigene:28639;urn:miriam:ncbigene:28755;urn:miriam:ncbigene:916;urn:miriam:ncbigene:917"
      hgnc "NA"
      map_id "d2540"
      name "d2540"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2540"
      uniprot "NA"
    ]
    graphics [
      x 354.69644975728806
      y 2131.2020019521906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d2540"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:7535;urn:miriam:ncbigene:3932;urn:miriam:ncbigene:2534"
      hgnc "NA"
      map_id "dbed0"
      name "dbed0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dbed0"
      uniprot "NA"
    ]
    graphics [
      x 276.90231687814696
      y 1683.5013718089153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dbed0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:ncbigene:3122;urn:miriam:ncbigene:3123;urn:miriam:ncbigene:3126;urn:miriam:ncbigene:3127"
      hgnc "NA"
      map_id "b533f"
      name "b533f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b533f"
      uniprot "NA"
    ]
    graphics [
      x 114.14518842828033
      y 2281.922713432092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b533f"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikidata:Q87917572;urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32838362"
      hgnc "NA"
      map_id "b7705"
      name "b7705"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7705"
      uniprot "NA"
    ]
    graphics [
      x 1860.9580888176192
      y 2635.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "b7705"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4884"
      full_annotation "NA"
      hgnc "NA"
      map_id "W19_19"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "idea6a7587"
      uniprot "NA"
    ]
    graphics [
      x 2271.42249498986
      y 2767.4090121585305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4884"
      full_annotation "urn:miriam:wikipathways:WP4846"
      hgnc "NA"
      map_id "SARS_minus_CoV_minus_2_space_ORFs"
      name "SARS_minus_CoV_minus_2_space_ORFs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e9691"
      uniprot "NA"
    ]
    graphics [
      x 2372.551108933128
      y 2933.0404562759377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SARS_minus_CoV_minus_2_space_ORFs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 20
    source 2
    target 1
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_17"
      target_id "NKRF"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 1
    target 3
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 4
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 1
    target 5
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 1
    target 6
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 1
    target 7
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 1
    target 8
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 1
    target 9
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "NKRF"
      target_id "W19_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 17
    target 2
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "b7705"
      target_id "W19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 3
    target 16
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_2"
      target_id "b533f"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 4
    target 15
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_9"
      target_id "dbed0"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 5
    target 14
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_10"
      target_id "d2540"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 6
    target 13
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_3"
      target_id "bebbf"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 7
    target 12
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_16"
      target_id "cb65d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 8
    target 11
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_18"
      target_id "b120d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 9
    target 10
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_1"
      target_id "b7f53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 18
    target 17
    cd19dm [
      diagram "WP4884"
      edge_type "PRODUCTION"
      source_id "W19_19"
      target_id "b7705"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 19
    target 18
    cd19dm [
      diagram "WP4884"
      edge_type "CONSPUMPTION"
      source_id "SARS_minus_CoV_minus_2_space_ORFs"
      target_id "W19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
