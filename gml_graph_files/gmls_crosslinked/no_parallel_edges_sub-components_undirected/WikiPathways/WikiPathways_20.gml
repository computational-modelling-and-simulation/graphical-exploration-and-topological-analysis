# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q14457;urn:miriam:uniprot:Q8NEB9;urn:miriam:uniprot:Q99570"
      hgnc "NA"
      map_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
      name "ac27b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac27b"
      uniprot "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    ]
    graphics [
      x 1831.4685745165507
      y 2245.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4f4ebe11"
      uniprot "NA"
    ]
    graphics [
      x 2304.1820190102335
      y 2141.6764183507603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3266321"
      uniprot "NA"
    ]
    graphics [
      x 1316.8240205025372
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "UNIPROT:Q9HBF4"
      name "ZFYVE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f5ed9"
      uniprot "UNIPROT:Q9HBF4"
    ]
    graphics [
      x 1148.4094729689914
      y 3002.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HBF4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8TDY2;urn:miriam:uniprot:O75385;urn:miriam:uniprot:O75143;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      name "autophagy_br_initiation_br_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ce113"
      uniprot "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 2107.5891630254036
      y 2425.691913497371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "d2646"
      name "d2646"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2646"
      uniprot "NA"
    ]
    graphics [
      x 2502.719725334995
      y 2091.321544147862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d2646"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbfaf590f"
      uniprot "NA"
    ]
    graphics [
      x 1927.2438800398588
      y 3302.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:P42345"
      hgnc "NA"
      map_id "UNIPROT:P42345"
      name "MTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "dc0f7; a9b05"
      uniprot "UNIPROT:P42345"
    ]
    graphics [
      x 1569.2542598578516
      y 3212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P42345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:ensembl:ENSG00000175224;urn:miriam:uniprot:O75385;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "UNIPROT:O75385;UNIPROT:Q8IYT8"
      name "fccb6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fccb6"
      uniprot "UNIPROT:O75385;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 2005.1704376196121
      y 3572.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75385;UNIPROT:Q8IYT8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5a4fd155"
      uniprot "NA"
    ]
    graphics [
      x 1426.389365022803
      y 2702.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 11
    source 2
    target 1
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_32"
      target_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 1
    target 3
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
      target_id "W7_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 5
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 6
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "PHYSICAL_STIMULATION"
      source_id "d2646"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 3
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_31"
      target_id "UNIPROT:Q9HBF4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 5
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 8
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P42345"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 7
    target 9
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_36"
      target_id "UNIPROT:O75385;UNIPROT:Q8IYT8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 8
    target 10
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P42345"
      target_id "W7_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
