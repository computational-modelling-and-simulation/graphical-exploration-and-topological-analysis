# generated with VANTED V2.8.2 at Fri Mar 04 10:06:58 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000267690"
      hgnc "NA"
      map_id "LDLRAD4_minus_AS1"
      name "LDLRAD4_minus_AS1"
      node_subtype "RNA"
      node_type "species"
      org_id "c9042"
      uniprot "NA"
    ]
    graphics [
      x 411.8008579924856
      y 955.2993008226617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LDLRAD4_minus_AS1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32111819"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfb4f20fd"
      uniprot "NA"
    ]
    graphics [
      x 528.4324699952213
      y 1020.6090646819068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "LDLRAD4"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "e9bfd; ca96b; d98b8"
      uniprot "NA"
    ]
    graphics [
      x 674.5445714959428
      y 1127.497165636914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LDLRAD4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:31602316"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6fa109c9"
      uniprot "NA"
    ]
    graphics [
      x 502.1224618418753
      y 1155.326428427184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:19430479"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_25"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idef60a322"
      uniprot "NA"
    ]
    graphics [
      x 563.1707166203142
      y 1092.3675920684873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:31819986"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idfdf4dec2"
      uniprot "NA"
    ]
    graphics [
      x 817.1255699887959
      y 980.1909928561944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf54d419"
      uniprot "NA"
    ]
    graphics [
      x 712.137927747098
      y 1028.2575784153332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7947a72f"
      uniprot "NA"
    ]
    graphics [
      x 626.0282585828254
      y 1015.12028963999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:28888937"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6cc9fb0d"
      uniprot "NA"
    ]
    graphics [
      x 794.7906332676663
      y 1064.209500207643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "NEDD4"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c276a; c628c"
      uniprot "NA"
    ]
    graphics [
      x 731.7621106983335
      y 896.4535951031949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NEDD4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP3874"
      hgnc "NA"
      map_id "TGF_minus_B_space_signaling_br_pathway"
      name "TGF_minus_B_space_signaling_br_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bab41"
      uniprot "NA"
    ]
    graphics [
      x 892.849753329964
      y 1129.5488919256168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TGF_minus_B_space_signaling_br_pathway"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id71ecd0f3"
      uniprot "NA"
    ]
    graphics [
      x 628.5443549056718
      y 794.6499961556827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fe61d16"
      uniprot "NA"
    ]
    graphics [
      x 744.9435360395162
      y 778.9173964223535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP4217"
      hgnc "NA"
      map_id "Ebola_space_virus_br_pathway_space_in_space_host"
      name "Ebola_space_virus_br_pathway_space_in_space_host"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b8756"
      uniprot "NA"
    ]
    graphics [
      x 550.9493985048821
      y 703.2835167849086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ebola_space_virus_br_pathway_space_in_space_host"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:mirbase.mature:MIMAT0000075"
      hgnc "NA"
      map_id "hsa_minus_miR_minus_20a_minus_5p"
      name "hsa_minus_miR_minus_20a_minus_5p"
      node_subtype "RNA"
      node_type "species"
      org_id "f25b2"
      uniprot "NA"
    ]
    graphics [
      x 901.4385131520376
      y 837.4380694833405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hsa_minus_miR_minus_20a_minus_5p"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3b8d16d5"
      uniprot "NA"
    ]
    graphics [
      x 926.8355268800426
      y 688.4474309628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000085978"
      hgnc "NA"
      map_id "ATG16L1"
      name "ATG16L1"
      node_subtype "GENE"
      node_type "species"
      org_id "f314c"
      uniprot "NA"
    ]
    graphics [
      x 898.9758746642385
      y 549.7166330042955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG16L1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_19"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id694a53fa"
      uniprot "NA"
    ]
    graphics [
      x 822.8330354532754
      y 439.60966448957254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophagosome_br_formation"
      name "Autophagosome_br_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5486"
      uniprot "NA"
    ]
    graphics [
      x 707.6619019054629
      y 392.7477107464141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagosome_br_formation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Blood_br_pressure"
      name "Blood_br_pressure"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e3ffd"
      uniprot "NA"
    ]
    graphics [
      x 565.4744690074338
      y 1209.0370391549648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Blood_br_pressure"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "Neurodevelopmental_br_disorders"
      name "Neurodevelopmental_br_disorders"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1533"
      uniprot "NA"
    ]
    graphics [
      x 397.001561057413
      y 1073.7292175935363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Neurodevelopmental_br_disorders"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 22
    source 1
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4_minus_AS1"
      target_id "W23_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 2
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_27"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 4
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_21"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 5
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_25"
      target_id "LDLRAD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 3
    target 6
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 3
    target 7
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 3
    target 8
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 3
    target 9
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "LDLRAD4"
      target_id "W23_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 21
    target 4
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Neurodevelopmental_br_disorders"
      target_id "W23_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 20
    target 5
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Blood_br_pressure"
      target_id "W23_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 6
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_28"
      target_id "hsa_minus_miR_minus_20a_minus_5p"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 9
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "NEDD4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 9
    target 11
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "TGF_minus_B_space_signaling_br_pathway"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 10
    target 12
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "NEDD4"
      target_id "W23_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 10
    target 13
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "NEDD4"
      target_id "W23_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 12
    target 14
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_22"
      target_id "Ebola_space_virus_br_pathway_space_in_space_host"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 15
    target 16
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "hsa_minus_miR_minus_20a_minus_5p"
      target_id "W23_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 16
    target 17
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_17"
      target_id "ATG16L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 18
    target 17
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_19"
      target_id "ATG16L1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 19
    target 18
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "Autophagosome_br_formation"
      target_id "W23_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
