# generated with VANTED V2.8.2 at Fri Mar 04 09:59:55 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 1474.2122112596576
      y 827.5961846225423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 1025.5463239962858
      y 820.4461849169841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 10
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438; urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A15996; urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "GTP"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183; layout_424; layout_154; layout_2300; layout_2281; layout_2432; sa229; sa82; path_0_sa102; path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 1048.371043577976
      y 671.6695091300847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 1364.0096894503054
      y 1002.4825841057745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 1371.7669753190924
      y 437.50593785600773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 1266.9774510322397
      y 402.69045279368487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078; urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659; urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "UDP"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319; layout_2389; sa22; sa35"
      uniprot "NA"
    ]
    graphics [
      x 185.93646304756305
      y 514.4083114480445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474; urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "NADPH"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382; sa395; sa30; sa295; sa153; sa25; sa33; sa24; sa100; sa356; sa379"
      uniprot "NA"
    ]
    graphics [
      x 745.7959476302879
      y 660.837154594026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908; urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153; urn:miriam:obo.chebi:CHEBI%3A57945"
      hgnc "NA"
      map_id "NADH"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2; sa322; sa207; sa79; sa1264; sa286; sa44"
      uniprot "NA"
    ]
    graphics [
      x 908.6104659009085
      y 609.0029848906343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Interferon 1 pathway; C19DMap:Virus replication cycle; C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "Nsp14"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318; sa2206; sa35; sa120; sa410"
      uniprot "NA"
    ]
    graphics [
      x 777.9976780933245
      y 1370.053912455997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nsp14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:60961;urn:miriam:obo.chebi:CHEBI%3A16335"
      hgnc "NA"
      map_id "Adenosine"
      name "Adenosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa333"
      uniprot "NA"
    ]
    graphics [
      x 1427.2758467978729
      y 955.9804016689342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_80"
      name "adenosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 1631.8985377791207
      y 1149.6959290298532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "UNIPROT:P00491"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa348; sa358; sa320; sa258; sa113"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 1538.231252047446
      y 1317.7004345718415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00491"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:190;urn:miriam:obo.chebi:CHEBI%3A16708"
      hgnc "NA"
      map_id "Adenine"
      name "Adenine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa346"
      uniprot "NA"
    ]
    graphics [
      x 1791.9000350429797
      y 1096.4881599062878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
      name "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa318; sa271; sa112"
      uniprot "NA"
    ]
    graphics [
      x 1604.5510772416842
      y 1263.714063373867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619; urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "GDP"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa225; sa1274; path_0_sa100; path_0_sa103"
      uniprot "NA"
    ]
    graphics [
      x 956.6455695093645
      y 666.1117096487598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:4543472"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_54"
      name "GDP reductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 709.4226426648199
      y 728.3096920530658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.substance:223441017;urn:miriam:pubchem.substance:3635;urn:miriam:obo.chebi:CHEBI%3A15033"
      hgnc "NA"
      map_id "Thioredoxin"
      name "Thioredoxin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa242; sa376"
      uniprot "NA"
    ]
    graphics [
      x 653.2888010770932
      y 821.2657465695823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Thioredoxin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001034;urn:miriam:ncbigene:6241;urn:miriam:ncbigene:6241;urn:miriam:hgnc:10452;urn:miriam:ec-code:1.17.4.1;urn:miriam:hgnc.symbol:RRM2;urn:miriam:hgnc.symbol:RRM2;urn:miriam:ensembl:ENSG00000171848;urn:miriam:uniprot:P31350;urn:miriam:uniprot:P31350;urn:miriam:ensembl:ENSG00000167325;urn:miriam:hgnc.symbol:RRM1;urn:miriam:uniprot:P23921;urn:miriam:uniprot:P23921;urn:miriam:hgnc.symbol:RRM1;urn:miriam:ncbigene:6240;urn:miriam:refseq:NM_001033;urn:miriam:ncbigene:6240;urn:miriam:hgnc:10451;urn:miriam:ec-code:1.17.4.1;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477"
      hgnc "HGNC_SYMBOL:RRM2;HGNC_SYMBOL:RRM1;HGNC_SYMBOL:RRM2B"
      map_id "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
      name "ribonucleoside_space_reductase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3; csa4"
      uniprot "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
    ]
    graphics [
      x 606.3208439355217
      y 705.9785576466861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28862;urn:miriam:pubchem.compound:135398595"
      hgnc "NA"
      map_id "dGDP"
      name "dGDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 831.4490766743229
      y 443.94320637815963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dGDP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18191;urn:miriam:pubchem.substance:11533266;urn:miriam:pubchem.substance:3636"
      hgnc "NA"
      map_id "Thioredoxin_space_disulfide"
      name "Thioredoxin_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa241; sa374"
      uniprot "NA"
    ]
    graphics [
      x 581.2130759159581
      y 773.707996565134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Thioredoxin_space_disulfide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:GLA;urn:miriam:hgnc.symbol:GLA;urn:miriam:ec-code:3.2.1.22;urn:miriam:ensembl:ENSG00000102393;urn:miriam:ec-code:3.2.1.47;urn:miriam:ncbigene:2717;urn:miriam:ncbigene:2717;urn:miriam:uniprot:P06280;urn:miriam:uniprot:P06280;urn:miriam:refseq:NM_000169;urn:miriam:hgnc:4296; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:hgnc.symbol:GLA;urn:miriam:hgnc.symbol:GLA;urn:miriam:ec-code:3.2.1.22;urn:miriam:ensembl:ENSG00000102393;urn:miriam:ec-code:3.2.1.47;urn:miriam:ncbigene:2717;urn:miriam:ncbigene:2717;urn:miriam:uniprot:P06280;urn:miriam:uniprot:P06280;urn:miriam:refseq:NM_000169;urn:miriam:hgnc:4296"
      hgnc "HGNC_SYMBOL:GLA"
      map_id "UNIPROT:P06280"
      name "GLA; GLA:Nsp14"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa55; csa7"
      uniprot "UNIPROT:P06280"
    ]
    graphics [
      x 590.8405481348411
      y 847.2173574777339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P06280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 685.9246355705786
      y 1154.4156833175907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16813;urn:miriam:pubchem.compound:11850"
      hgnc "NA"
      map_id "Galacitol"
      name "Galacitol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 922.5275025085346
      y 817.0581221209216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Galacitol"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:30201105"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_76"
      name "galactitol:NAD+ 1-oxidoreductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 842.3410561800739
      y 690.2870679687964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009;urn:miriam:pubchem.compound:5886"
      hgnc "NA"
      map_id "NADP"
      name "NADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29; sa296; sa106"
      uniprot "NA"
    ]
    graphics [
      x 1003.5374966465978
      y 539.1819052744123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AKR1B1;urn:miriam:hgnc.symbol:AKR1B1;urn:miriam:ncbigene:231;urn:miriam:ncbigene:231;urn:miriam:hgnc:381;urn:miriam:ensembl:ENSG00000085662;urn:miriam:refseq:NM_001628;urn:miriam:ec-code:1.1.1.300;urn:miriam:uniprot:P15121;urn:miriam:uniprot:P15121;urn:miriam:ec-code:1.1.1.372;urn:miriam:ec-code:1.1.1.54;urn:miriam:ec-code:1.1.1.21"
      hgnc "HGNC_SYMBOL:AKR1B1"
      map_id "UNIPROT:P15121"
      name "AKR1B1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:P15121"
    ]
    graphics [
      x 864.878770550226
      y 863.8180967597579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P15121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439353;urn:miriam:obo.chebi:CHEBI%3A27667"
      hgnc "NA"
      map_id "D_minus_Galactose"
      name "D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 515.113642266411
      y 599.0369382686155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D_minus_Galactose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 34
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "H"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31; sa103; sa281; sa282; sa134; sa133; sa178; sa298; sa297; sa288; sa129; sa360; sa341; sa254; sa327; sa302; sa336; sa325; sa164; sa266; sa190; sa183; sa169; sa286; sa206; sa221; sa222; sa188; sa187; sa83; sa123; sa78; sa367; sa89"
      uniprot "NA"
    ]
    graphics [
      x 1241.887547657256
      y 748.7746445396722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:8629;urn:miriam:obo.chebi:CHEBI%3A46229"
      hgnc "NA"
      map_id "UDP_minus__alpha__minus_D_minus_Glucose"
      name "UDP_minus__alpha__minus_D_minus_Glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10"
      uniprot "NA"
    ]
    graphics [
      x 532.8048799833178
      y 859.4886678100578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus__alpha__minus_D_minus_Glucose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:31827638"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_48"
      name "UDP-galactose-4-epimerase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 259.8575874210379
      y 707.9641403203267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:5.1.3.2;urn:miriam:hgnc.symbol:GALE;urn:miriam:uniprot:Q14376;urn:miriam:uniprot:Q14376;urn:miriam:hgnc.symbol:GALE;urn:miriam:hgnc:4116;urn:miriam:ncbigene:2582;urn:miriam:ncbigene:2582;urn:miriam:ec-code:5.1.3.7;urn:miriam:refseq:NM_000403;urn:miriam:ensembl:ENSG00000117308"
      hgnc "HGNC_SYMBOL:GALE"
      map_id "UNIPROT:Q14376"
      name "GALE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:Q14376"
    ]
    graphics [
      x 121.87676287018405
      y 746.1443139292527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A66914;urn:miriam:pubchem.compound:18068"
      hgnc "NA"
      map_id "UDP_minus__alpha__minus_D_minus_Galactose"
      name "UDP_minus__alpha__minus_D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa8"
      uniprot "NA"
    ]
    graphics [
      x 306.1075607907886
      y 566.9520183034602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP_minus__alpha__minus_D_minus_Galactose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:13363863"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_55"
      name "ATP:dGDP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 1034.402893137607
      y 217.31534594966183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000103024;urn:miriam:hgnc:7851;urn:miriam:hgnc.symbol:NME3;urn:miriam:hgnc.symbol:NME3;urn:miriam:uniprot:Q13232;urn:miriam:uniprot:Q13232;urn:miriam:ec-code:2.7.4.6;urn:miriam:refseq:NM_002513;urn:miriam:ncbigene:4832;urn:miriam:ncbigene:4832"
      hgnc "HGNC_SYMBOL:NME3"
      map_id "UNIPROT:Q13232"
      name "NME3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa232; sa380"
      uniprot "UNIPROT:Q13232"
    ]
    graphics [
      x 1051.4326975936528
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7850;urn:miriam:uniprot:P22392;urn:miriam:uniprot:P22392;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:refseq:NM_002512;urn:miriam:ensembl:ENSG00000243678;urn:miriam:ec-code:2.7.13.3;urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME2;HGNC_SYMBOL:NME1"
      map_id "UNIPROT:P22392;UNIPROT:P15531"
      name "Nucleoside_space_diphosphate_space_kinase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2; csa5"
      uniprot "UNIPROT:P22392;UNIPROT:P15531"
    ]
    graphics [
      x 953.2464905496754
      y 204.77863904053265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P22392;UNIPROT:P15531"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:29922;urn:miriam:ncbigene:29922;urn:miriam:ensembl:ENSG00000143156;urn:miriam:uniprot:Q9Y5B8;urn:miriam:uniprot:Q9Y5B8;urn:miriam:refseq:NM_013330;urn:miriam:hgnc.symbol:NME7;urn:miriam:hgnc.symbol:NME7;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:20461"
      hgnc "HGNC_SYMBOL:NME7"
      map_id "UNIPROT:Q9Y5B8"
      name "NME7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa237; sa383"
      uniprot "UNIPROT:Q9Y5B8"
    ]
    graphics [
      x 909.8302728200435
      y 145.1269239395167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y5B8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:20567;urn:miriam:hgnc.symbol:NME6;urn:miriam:hgnc.symbol:NME6;urn:miriam:refseq:NM_005793;urn:miriam:ec-code:2.7.4.6;urn:miriam:ensembl:ENSG00000172113;urn:miriam:ncbigene:10201;urn:miriam:ncbigene:10201;urn:miriam:uniprot:O75414;urn:miriam:uniprot:O75414"
      hgnc "HGNC_SYMBOL:NME6"
      map_id "UNIPROT:O75414"
      name "NME6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236; sa382"
      uniprot "UNIPROT:O75414"
    ]
    graphics [
      x 888.4598441870997
      y 255.49576685163345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7853;urn:miriam:hgnc.symbol:NME5;urn:miriam:hgnc.symbol:NME5;urn:miriam:ncbigene:8382;urn:miriam:ncbigene:8382;urn:miriam:refseq:NM_003551;urn:miriam:ensembl:ENSG00000112981;urn:miriam:uniprot:P56597;urn:miriam:uniprot:P56597"
      hgnc "HGNC_SYMBOL:NME5"
      map_id "UNIPROT:P56597"
      name "NME5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa235; sa381"
      uniprot "UNIPROT:P56597"
    ]
    graphics [
      x 986.1639421234156
      y 88.14665534086055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P56597"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398599;urn:miriam:obo.chebi:CHEBI%3A16497"
      hgnc "NA"
      map_id "dGTP"
      name "dGTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa245"
      uniprot "NA"
    ]
    graphics [
      x 1071.4467829346186
      y 420.8662418991983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dGTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:937;urn:miriam:obo.chebi:CHEBI%3A32544"
      hgnc "NA"
      map_id "Nicotinate"
      name "Nicotinate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 1498.5187354649288
      y 383.07916326479335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nicotinate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1591.144437065644
      y 388.9588248782141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:pubchem.compound:936"
      hgnc "NA"
      map_id "Nicotinamide"
      name "Nicotinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1428.0745909640216
      y 573.9873268424501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nicotinamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927;urn:miriam:pubchem.compound:439924"
      hgnc "NA"
      map_id "N_minus_Ribosyl_minus_nicotinamide"
      name "N_minus_Ribosyl_minus_nicotinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa68"
      uniprot "NA"
    ]
    graphics [
      x 1571.636362082938
      y 798.8094767265087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N_minus_Ribosyl_minus_nicotinamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:14907738"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_24"
      name "ATP:N-ribosylnicotinamide 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1482.172514655624
      y 573.4380456652984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "NRK1"
      name "NRK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa104"
      uniprot "NA"
    ]
    graphics [
      x 1627.0488210346991
      y 616.5805327679582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NRK1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:14180;urn:miriam:obo.chebi:CHEBI%3A16171"
      hgnc "NA"
      map_id "Nicotinamide_space_D_minus_ribonucleotide"
      name "Nicotinamide_space_D_minus_ribonucleotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 1579.1339033522943
      y 690.7295551762293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nicotinamide_space_D_minus_ribonucleotide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A78679;urn:miriam:pubchem.compound:439167"
      hgnc "NA"
      map_id "D_minus_Ribose_space_5P"
      name "D_minus_Ribose_space_5P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa146"
      uniprot "NA"
    ]
    graphics [
      x 2006.1484950365596
      y 753.7092072046363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D_minus_Ribose_space_5P"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:4306285"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_38"
      name "ribose-phosphate pyrophosphokinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 1817.3401981508882
      y 681.2174087159993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001204402;urn:miriam:hgnc:9462;urn:miriam:ensembl:ENSG00000147224;urn:miriam:ncbigene:5631;urn:miriam:ncbigene:5631;urn:miriam:ec-code:2.7.6.1;urn:miriam:hgnc.symbol:PRPS1;urn:miriam:uniprot:P60891;urn:miriam:uniprot:P60891;urn:miriam:hgnc.symbol:PRPS1"
      hgnc "HGNC_SYMBOL:PRPS1"
      map_id "UNIPROT:P60891"
      name "PRPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:P60891"
    ]
    graphics [
      x 1810.429935962517
      y 796.2455284709864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P60891"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P11908;urn:miriam:uniprot:P11908;urn:miriam:ensembl:ENSG00000101911;urn:miriam:ec-code:2.7.6.1;urn:miriam:ncbigene:5634;urn:miriam:ncbigene:5634;urn:miriam:hgnc:9465;urn:miriam:hgnc.symbol:PRPS2;urn:miriam:refseq:NM_002765;urn:miriam:hgnc.symbol:PRPS2"
      hgnc "HGNC_SYMBOL:PRPS2"
      map_id "UNIPROT:P11908"
      name "PRPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa149"
      uniprot "UNIPROT:P11908"
    ]
    graphics [
      x 1941.4878732671264
      y 587.9814868718075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P11908"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:PRPS1L1;urn:miriam:hgnc.symbol:PRPS1L1;urn:miriam:ensembl:ENSG00000229937;urn:miriam:ncbigene:221823;urn:miriam:ncbigene:221823;urn:miriam:refseq:NM_175886;urn:miriam:uniprot:P21108;urn:miriam:uniprot:P21108;urn:miriam:ec-code:2.7.6.1;urn:miriam:hgnc:9463"
      hgnc "HGNC_SYMBOL:PRPS1L1"
      map_id "UNIPROT:P21108"
      name "PRPS1L1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P21108"
    ]
    graphics [
      x 1931.396116330593
      y 739.8013439462693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111;urn:miriam:pubchem.compound:7339"
      hgnc "NA"
      map_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      name "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa110; sa351; sa269; sa330"
      uniprot "NA"
    ]
    graphics [
      x 1647.9971760053309
      y 925.3131772162667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083; urn:miriam:obo.chebi:CHEBI%3A456215; urn:miriam:obo.chebi:CHEBI%3A16027"
      hgnc "NA"
      map_id "AMP"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa301; sa97; sa224; sa45; sa216; sa288"
      uniprot "NA"
    ]
    graphics [
      x 1512.1333604181257
      y 669.2429527791938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398638;urn:miriam:obo.chebi:CHEBI%3A17368"
      hgnc "NA"
      map_id "Hypoxanthine"
      name "Hypoxanthine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa317"
      uniprot "NA"
    ]
    graphics [
      x 1239.021891425494
      y 1077.2468480549514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Hypoxanthine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_83"
      name "Deoxyinosine:orthophosphate ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 1348.913211391231
      y 1304.3246818793255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439287;urn:miriam:obo.chebi:CHEBI%3A28542"
      hgnc "NA"
      map_id "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
      name "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa357; sa260"
      uniprot "NA"
    ]
    graphics [
      x 1219.7557129403185
      y 1378.8163049327327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28997;urn:miriam:pubchem.compound:135398593"
      hgnc "NA"
      map_id "Deoxyinosine"
      name "Deoxyinosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 1157.4975556438244
      y 1219.3956527787552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyinosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:35398641;urn:miriam:obo.chebi:CHEBI%3A17596"
      hgnc "NA"
      map_id "Inosine"
      name "Inosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa313"
      uniprot "NA"
    ]
    graphics [
      x 1061.7396166691065
      y 1107.0508814769887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Inosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_72"
      name "inosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1345.3314437715126
      y 1194.2995534903853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "_alpha_D_minus_Ribose_space_1P"
      name "_alpha_D_minus_Ribose_space_1P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa145"
      uniprot "NA"
    ]
    graphics [
      x 1771.8965839438831
      y 682.6515230171094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha_D_minus_Ribose_space_1P"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:4992818"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_36"
      name "phosphodeoxyribomutase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 1973.6649666020232
      y 658.0966410887703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_018290;urn:miriam:uniprot:Q96G03;urn:miriam:uniprot:Q96G03;urn:miriam:ec-code:5.4.2.7;urn:miriam:hgnc:8906;urn:miriam:ensembl:ENSG00000169299;urn:miriam:ec-code:5.4.2.2;urn:miriam:ncbigene:55276;urn:miriam:ncbigene:55276;urn:miriam:hgnc.symbol:PGM2;urn:miriam:hgnc.symbol:PGM2"
      hgnc "HGNC_SYMBOL:PGM2"
      map_id "UNIPROT:Q96G03"
      name "PGM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:Q96G03"
    ]
    graphics [
      x 2109.123124236809
      y 637.5719518489277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96G03"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_79"
      name "adenosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 1323.3933148406813
      y 877.5724876530466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813; urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:P21589;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "UNIPROT:P21589"
      name "NT5E; NT5E:Zn2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa343; sa100; sa316; sa275; csa35"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1296.2715240821487
      y 1041.5905124983851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21589"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:14953432"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_64"
      name "GTP diphosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1120.3923319174469
      y 908.8612540361669
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:954;urn:miriam:ncbigene:954;urn:miriam:hgnc:3364;urn:miriam:ec-code:3.6.1.-;urn:miriam:uniprot:Q9Y5L3;urn:miriam:uniprot:Q9Y5L3;urn:miriam:hgnc.symbol:ENTPD2;urn:miriam:hgnc.symbol:ENTPD2;urn:miriam:refseq:NM_203468;urn:miriam:ensembl:ENSG00000054179"
      hgnc "HGNC_SYMBOL:ENTPD2"
      map_id "UNIPROT:Q9Y5L3"
      name "ENTPD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa277"
      uniprot "UNIPROT:Q9Y5L3"
    ]
    graphics [
      x 993.9403049206484
      y 1070.3873002039772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y5L3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398631;urn:miriam:obo.chebi:CHEBI%3A17345"
      hgnc "NA"
      map_id "GMP"
      name "GMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa215"
      uniprot "NA"
    ]
    graphics [
      x 1085.8119409667786
      y 974.8047222780505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1066;urn:miriam:obo.chebi:CHEBI%3A16675"
      hgnc "NA"
      map_id "Quinolinate"
      name "Quinolinate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa131"
      uniprot "NA"
    ]
    graphics [
      x 1779.2956350941636
      y 578.8723320677125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Quinolinate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:5320648;PUBMED:14165928"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_33"
      name "nicotinate-nucleotide pyrophosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 1663.1798431639704
      y 682.4377627101971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298; urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298"
      hgnc "HGNC_SYMBOL:QPRT"
      map_id "UNIPROT:Q15274"
      name "QPRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132; sa207"
      uniprot "UNIPROT:Q15274"
    ]
    graphics [
      x 1828.6330672854758
      y 617.6826422450422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15763;urn:miriam:pubchem.compound:121992"
      hgnc "NA"
      map_id "Nicotinate_space_D_minus_ribonucleotide"
      name "Nicotinate_space_D_minus_ribonucleotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa72"
      uniprot "NA"
    ]
    graphics [
      x 1725.0861452872202
      y 714.7825559769855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Nicotinate_space_D_minus_ribonucleotide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526;urn:miriam:pubchem.compound:280; urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "CO2"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136; sa186; sa26; sa70; sa122; sa40; sa201"
      uniprot "NA"
    ]
    graphics [
      x 1698.9468086468
      y 447.51171764638406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CO2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:129652037"
      hgnc "NA"
      map_id "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
      name "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 1317.9325030098385
      y 936.9126480066716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:13416226"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_42"
      name "5'-Phosphoribosylformylglycinamide:L-glutamine amido-ligase "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1249.9062883188574
      y 658.3872057965597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050;urn:miriam:pubchem.compound:5961"
      hgnc "NA"
      map_id "L_minus_Glutamine"
      name "L_minus_Glutamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa173; sa218; sa85; sa155"
      uniprot "NA"
    ]
    graphics [
      x 1270.5060482301099
      y 540.0069499608123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Glutamine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:8863;urn:miriam:refseq:NM_012393;urn:miriam:ncbigene:5198;urn:miriam:ncbigene:5198;urn:miriam:ensembl:ENSG00000178921;urn:miriam:uniprot:O15067;urn:miriam:uniprot:O15067;urn:miriam:ec-code:6.3.5.3;urn:miriam:hgnc.symbol:PFAS;urn:miriam:hgnc.symbol:PFAS"
      hgnc "HGNC_SYMBOL:PFAS"
      map_id "UNIPROT:O15067"
      name "PFAS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:O15067"
    ]
    graphics [
      x 1110.6529203811624
      y 573.0255284755183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15067"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18413;urn:miriam:pubchem.compound:5462266"
      hgnc "NA"
      map_id "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
      name "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1335.9989813770808
      y 734.2670710881708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015;urn:miriam:pubchem.compound:33032"
      hgnc "NA"
      map_id "L_minus_Glutamate"
      name "L_minus_Glutamate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa177; sa220; sa86; sa154"
      uniprot "NA"
    ]
    graphics [
      x 1324.7604205972748
      y 546.1987003030183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Glutamate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_25"
      name "nicotinamide ribonucleotide phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1394.0262913832933
      y 879.4322584375684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398640;urn:miriam:obo.chebi:CHEBI%3A17202"
      hgnc "NA"
      map_id "IMP"
      name "IMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 1018.9795633645002
      y 1021.8069649996542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_66"
      name "GMP reductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 885.2786011505095
      y 803.1020038978007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "Ammonium"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa294; sa362; sa329; sa307; sa334"
      uniprot "NA"
    ]
    graphics [
      x 943.1606009479999
      y 1038.5998396900534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ammonium"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:2766;urn:miriam:ncbigene:2766;urn:miriam:refseq:NM_006877;urn:miriam:hgnc:4376;urn:miriam:ensembl:ENSG00000137198;urn:miriam:ec-code:1.7.1.7;urn:miriam:uniprot:P36959;urn:miriam:uniprot:P36959;urn:miriam:hgnc.symbol:GMPR;urn:miriam:hgnc.symbol:GMPR"
      hgnc "HGNC_SYMBOL:GMPR"
      map_id "UNIPROT:P36959"
      name "GMPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa299"
      uniprot "UNIPROT:P36959"
    ]
    graphics [
      x 733.0346619703182
      y 873.3986948141584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P36959"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4377;urn:miriam:hgnc.symbol:GMPR2;urn:miriam:hgnc.symbol:GMPR2;urn:miriam:ec-code:1.7.1.7;urn:miriam:refseq:NM_016576;urn:miriam:ensembl:ENSG00000100938;urn:miriam:ncbigene:51292;urn:miriam:uniprot:Q9P2T1;urn:miriam:uniprot:Q9P2T1;urn:miriam:ncbigene:51292"
      hgnc "HGNC_SYMBOL:GMPR2"
      map_id "UNIPROT:Q9P2T1"
      name "GMPR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa300"
      uniprot "UNIPROT:Q9P2T1"
    ]
    graphics [
      x 764.2438204269409
      y 929.5930593141902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9P2T1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:pubmed:18506437;urn:miriam:pubchem.compound:667490;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035; urn:miriam:pubmed:17139284;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4; urn:miriam:pubmed:17496727;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "UNIPROT:P12268"
      name "IMPDH2; IMPDH2:Nsp14; IMercomp; IRcomp; IMcomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa209; csa8; sa1395; csa125; csa126; csa124"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1069.160676794034
      y 1200.3034770099105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P12268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_10"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 1311.8918038928578
      y 1261.883866849526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubmed:1969416"
      hgnc "NA"
      map_id "Guanine_space_nucleotide_space_synthesis"
      name "Guanine_space_nucleotide_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa411"
      uniprot "NA"
    ]
    graphics [
      x 1480.5422567727287
      y 1280.3464461412634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanine_space_nucleotide_space_synthesis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_65"
      name "GDP phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 941.8880395402781
      y 964.1485491487068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:14573;urn:miriam:ec-code:3.6.1.6;urn:miriam:ensembl:ENSG00000197217;urn:miriam:hgnc.symbol:ENTPD4;urn:miriam:hgnc.symbol:ENTPD4;urn:miriam:ec-code:3.6.1.15;urn:miriam:ncbigene:9583;urn:miriam:ncbigene:9583;urn:miriam:ec-code:3.6.1.42;urn:miriam:uniprot:Q9Y227;urn:miriam:uniprot:Q9Y227;urn:miriam:refseq:NM_004901"
      hgnc "HGNC_SYMBOL:ENTPD4"
      map_id "UNIPROT:Q9Y227"
      name "ENTPD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa290"
      uniprot "UNIPROT:Q9Y227"
    ]
    graphics [
      x 764.2221568035551
      y 1044.9865718083258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:O75356;urn:miriam:uniprot:O75356;urn:miriam:ensembl:ENSG00000187097;urn:miriam:ncbigene:957;urn:miriam:ncbigene:957;urn:miriam:hgnc:3367;urn:miriam:ec-code:3.6.1.6;urn:miriam:hgnc.symbol:ENTPD5;urn:miriam:hgnc.symbol:ENTPD5;urn:miriam:ec-code:3.6.1.42;urn:miriam:refseq:NM_001249"
      hgnc "HGNC_SYMBOL:ENTPD5"
      map_id "UNIPROT:O75356"
      name "ENTPD5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa291"
      uniprot "UNIPROT:O75356"
    ]
    graphics [
      x 842.9006174891774
      y 1108.289416233001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:955;urn:miriam:ncbigene:955;urn:miriam:uniprot:O75354;urn:miriam:uniprot:O75354;urn:miriam:hgnc.symbol:ENTPD6;urn:miriam:hgnc.symbol:ENTPD6;urn:miriam:ec-code:3.6.1.6;urn:miriam:hgnc:3368;urn:miriam:refseq:NM_001114089;urn:miriam:ensembl:ENSG00000197586"
      hgnc "HGNC_SYMBOL:ENTPD6"
      map_id "UNIPROT:O75354"
      name "ENTPD6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa292"
      uniprot "UNIPROT:O75354"
    ]
    graphics [
      x 750.4095815785904
      y 994.4970864610592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O75354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:19721;urn:miriam:hgnc.symbol:CANT1;urn:miriam:hgnc.symbol:CANT1;urn:miriam:ncbigene:124583;urn:miriam:ncbigene:124583;urn:miriam:ec-code:3.6.1.6;urn:miriam:ensembl:ENSG00000171302;urn:miriam:refseq:NM_138793;urn:miriam:uniprot:Q8WVQ1;urn:miriam:uniprot:Q8WVQ1"
      hgnc "HGNC_SYMBOL:CANT1"
      map_id "UNIPROT:Q8WVQ1"
      name "CANT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa293"
      uniprot "UNIPROT:Q8WVQ1"
    ]
    graphics [
      x 724.586652009543
      y 1087.080162512271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8WVQ1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:13363863"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_53"
      name "ATP:GDP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 1053.4923083129113
      y 314.5882530317909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_81"
      name "Adenine phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 1755.2843224214698
      y 924.084027946835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000485;urn:miriam:hgnc:626;urn:miriam:uniprot:P07741;urn:miriam:uniprot:P07741;urn:miriam:ensembl:ENSG00000198931;urn:miriam:ncbigene:353;urn:miriam:ncbigene:353;urn:miriam:hgnc.symbol:APRT;urn:miriam:hgnc.symbol:APRT;urn:miriam:ec-code:2.4.2.7"
      hgnc "HGNC_SYMBOL:APRT"
      map_id "UNIPROT:P07741"
      name "APRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa350"
      uniprot "UNIPROT:P07741"
    ]
    graphics [
      x 1891.8635661995595
      y 979.7619138479108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07741"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398597;urn:miriam:obo.chebi:CHEBI%3A16192"
      hgnc "NA"
      map_id "dGMP"
      name "dGMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa248"
      uniprot "NA"
    ]
    graphics [
      x 1109.802329656056
      y 667.900523716655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dGMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:14253449"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_56"
      name "ATP:dGMP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 1097.5719958028938
      y 467.67574479318176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4693;urn:miriam:uniprot:Q16774;urn:miriam:uniprot:Q16774;urn:miriam:ncbigene:2987;urn:miriam:ncbigene:2987;urn:miriam:ensembl:ENSG00000143774;urn:miriam:hgnc.symbol:GUK1;urn:miriam:hgnc.symbol:GUK1;urn:miriam:pubmed:8663313;urn:miriam:refseq:NM_000858;urn:miriam:ec-code:2.7.4.8"
      hgnc "HGNC_SYMBOL:GUK1"
      map_id "UNIPROT:Q16774"
      name "GUK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:Q16774"
    ]
    graphics [
      x 963.8164334417575
      y 511.66412740221693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16774"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5892;urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "NAD"
      name "NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa70; sa323; sa205"
      uniprot "NA"
    ]
    graphics [
      x 1149.5485091564083
      y 512.5572777327131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:13428775"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_23"
      name "NAD+ phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1323.7906399122078
      y 664.1501280016238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:3356;urn:miriam:ec-code:3.1.4.1;urn:miriam:uniprot:P22413;urn:miriam:uniprot:P22413;urn:miriam:ec-code:3.6.1.9;urn:miriam:ncbigene:5167;urn:miriam:ncbigene:5167;urn:miriam:ensembl:ENSG00000197594;urn:miriam:hgnc.symbol:ENPP1;urn:miriam:refseq:NM_006208;urn:miriam:hgnc.symbol:ENPP1"
      hgnc "HGNC_SYMBOL:ENPP1"
      map_id "UNIPROT:P22413"
      name "ENPP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94; sa262"
      uniprot "UNIPROT:P22413"
    ]
    graphics [
      x 1270.860725322892
      y 810.6507969993172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P22413"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.4.1;urn:miriam:hgnc:3358;urn:miriam:ensembl:ENSG00000154269;urn:miriam:ec-code:3.6.1.9;urn:miriam:refseq:NM_005021;urn:miriam:ncbigene:5169;urn:miriam:uniprot:O14638;urn:miriam:uniprot:O14638;urn:miriam:ncbigene:5169;urn:miriam:hgnc.symbol:ENPP3;urn:miriam:hgnc.symbol:ENPP3"
      hgnc "HGNC_SYMBOL:ENPP3"
      map_id "UNIPROT:O14638"
      name "ENPP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa95; sa261"
      uniprot "UNIPROT:O14638"
    ]
    graphics [
      x 1327.0486617333265
      y 800.7448114030586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O14638"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:13717628"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_32"
      name "ATP:nicotinamide-nucleotide adenylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1629.1867325491671
      y 540.700133795362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000173614;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:17877;urn:miriam:hgnc.symbol:NMNAT1;urn:miriam:hgnc.symbol:NMNAT1;urn:miriam:ec-code:2.7.7.1;urn:miriam:refseq:NM_001297778;urn:miriam:pubmed:12359228;urn:miriam:ncbigene:64802;urn:miriam:ncbigene:64802;urn:miriam:uniprot:Q9HAN9;urn:miriam:uniprot:Q9HAN9"
      hgnc "HGNC_SYMBOL:NMNAT1"
      map_id "UNIPROT:Q9HAN9"
      name "NMNAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q9HAN9"
    ]
    graphics [
      x 1653.2643386039554
      y 461.4828739909261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HAN9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:pubmed:12359228;urn:miriam:ensembl:ENSG00000157064; urn:miriam:uniprot:Q9BZQ4;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:ensembl:ENSG00000157064"
      hgnc "HGNC_SYMBOL:NMNAT2"
      map_id "UNIPROT:Q9BZQ4"
      name "NMNAT2; NMNAT2:Mg2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa91; csa16"
      uniprot "UNIPROT:Q9BZQ4"
    ]
    graphics [
      x 1746.973893489108
      y 416.3122898433719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BZQ4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565;urn:miriam:pubmed:17402747; urn:miriam:uniprot:Q96T66;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "HGNC_SYMBOL:NMNAT3"
      map_id "UNIPROT:Q96T66"
      name "NMNAT3; NMNAT3:Mg2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa93; csa3"
      uniprot "UNIPROT:Q96T66"
    ]
    graphics [
      x 1751.0717265173337
      y 490.87110760816176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96T66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135421870;urn:miriam:obo.chebi:CHEBI%3A18304"
      hgnc "NA"
      map_id "Deamino_minus_NAD"
      name "Deamino_minus_NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa71"
      uniprot "NA"
    ]
    graphics [
      x 1444.6558358104385
      y 405.9051735143038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deamino_minus_NAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:13730;urn:miriam:obo.chebi:CHEBI%3A17256"
      hgnc "NA"
      map_id "Deoxyadenosine"
      name "Deoxyadenosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa359"
      uniprot "NA"
    ]
    graphics [
      x 1155.035787922767
      y 707.3071667226676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyadenosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:14927650"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_84"
      name "Deoxyadenosine aminohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 1038.4650287813363
      y 952.8800457141608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000022;urn:miriam:ensembl:ENSG00000196839;urn:miriam:hgnc.symbol:ADA;urn:miriam:hgnc.symbol:ADA;urn:miriam:ec-code:3.5.4.4;urn:miriam:hgnc:186;urn:miriam:ncbigene:100;urn:miriam:ncbigene:100;urn:miriam:uniprot:P00813;urn:miriam:uniprot:P00813"
      hgnc "HGNC_SYMBOL:ADA"
      map_id "UNIPROT:P00813"
      name "ADA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363; sa337"
      uniprot "UNIPROT:P00813"
    ]
    graphics [
      x 1016.6724324695284
      y 1147.672500299971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00813"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:11947697"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_16"
      name "beta-1,4-galactosyltransferase 1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 152.65363369764907
      y 646.3509195718206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:79025;urn:miriam:obo.chebi:CHEBI%3A28102"
      hgnc "NA"
      map_id "_alpha__minus_D_minus_Glucose"
      name "_alpha__minus_D_minus_Glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21; sa26"
      uniprot "NA"
    ]
    graphics [
      x 271.02770666157096
      y 811.3519324268217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha__minus_D_minus_Glucose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_002289;urn:miriam:ensembl:ENSG00000167531;urn:miriam:hgnc.symbol:LALBA;urn:miriam:hgnc.symbol:LALBA;urn:miriam:hgnc:6480;urn:miriam:ncbigene:3906;urn:miriam:uniprot:P00709;urn:miriam:uniprot:P00709;urn:miriam:ncbigene:3906;urn:miriam:ec-code:2.4.1.90;urn:miriam:uniprot:P15291;urn:miriam:uniprot:P15291;urn:miriam:ec-code:2.4.1.275;urn:miriam:hgnc.symbol:B4GALT1;urn:miriam:ensembl:ENSG00000086062;urn:miriam:hgnc.symbol:B4GALT1;urn:miriam:ncbigene:2683;urn:miriam:ncbigene:2683;urn:miriam:refseq:NM_001497;urn:miriam:hgnc:924;urn:miriam:ec-code:2.4.1.-;urn:miriam:ec-code:2.4.1.22;urn:miriam:ec-code:2.4.1.38"
      hgnc "HGNC_SYMBOL:LALBA;HGNC_SYMBOL:B4GALT1"
      map_id "UNIPROT:P00709;UNIPROT:P15291"
      name "lactose_space_synthetase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P00709;UNIPROT:P15291"
    ]
    graphics [
      x 62.5
      y 554.8703976030795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00709;UNIPROT:P15291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:440995;urn:miriam:obo.chebi:CHEBI%3A17716"
      hgnc "NA"
      map_id "Lactose"
      name "Lactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 293.3384573451913
      y 760.5430331613962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lactose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16174;urn:miriam:pubchem.compound:188966"
      hgnc "NA"
      map_id "dADP"
      name "dADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa369"
      uniprot "NA"
    ]
    graphics [
      x 984.6543601159572
      y 301.9756750209147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:13211603"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_88"
      name "ATP:dADP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1088.6470893956168
      y 156.8107368838381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16284;urn:miriam:pubchem.compound:15993"
      hgnc "NA"
      map_id "dATP"
      name "dATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 895.6713307944865
      y 186.41802006057083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:14832298"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_78"
      name "Adenosine kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 1377.0785612340956
      y 572.9290259101889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P55263;urn:miriam:uniprot:P55263;urn:miriam:hgnc:257;urn:miriam:refseq:NM_006721;urn:miriam:ncbigene:132;urn:miriam:ncbigene:132;urn:miriam:ensembl:ENSG00000156110;urn:miriam:ec-code:2.7.1.20;urn:miriam:hgnc.symbol:ADK;urn:miriam:hgnc.symbol:ADK"
      hgnc "HGNC_SYMBOL:ADK"
      map_id "UNIPROT:P55263"
      name "ADK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa342"
      uniprot "UNIPROT:P55263"
    ]
    graphics [
      x 1470.255465826358
      y 630.7436438484367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P55263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17172;urn:miriam:pubchem.compound:135398592"
      hgnc "NA"
      map_id "Deoxyguanosine"
      name "Deoxyguanosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa251"
      uniprot "NA"
    ]
    graphics [
      x 1210.8878291676883
      y 878.5795146721244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyguanosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:6260206"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_57"
      name "ATP:deoxyguanosine 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 1269.5858336433262
      y 590.4837285762387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:ensembl:ENSG00000156136;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:ec-code:2.7.1.74;urn:miriam:hgnc:2704;urn:miriam:uniprot:P27707;urn:miriam:uniprot:P27707;urn:miriam:refseq:NM_000788"
      hgnc "HGNC_SYMBOL:DCK"
      map_id "UNIPROT:P27707"
      name "DCK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255; sa368"
      uniprot "UNIPROT:P27707"
    ]
    graphics [
      x 1341.5245679763848
      y 377.81016367230245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P27707"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398634;urn:miriam:obo.chebi:CHEBI%3A16235"
      hgnc "NA"
      map_id "Guanine"
      name "Guanine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa256"
      uniprot "NA"
    ]
    graphics [
      x 1251.1066728598094
      y 1274.135259343657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:16578130"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_74"
      name "Guanine deaminase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 1110.6357907225824
      y 1092.5316317996967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001242505;urn:miriam:hgnc:4212;urn:miriam:uniprot:Q9Y2T3;urn:miriam:uniprot:Q9Y2T3;urn:miriam:ensembl:ENSG00000119125;urn:miriam:hgnc.symbol:GDA;urn:miriam:ncbigene:9615;urn:miriam:hgnc.symbol:GDA;urn:miriam:ncbigene:9615;urn:miriam:ec-code:3.5.4.3"
      hgnc "HGNC_SYMBOL:GDA"
      map_id "UNIPROT:Q9Y2T3"
      name "GDA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa326"
      uniprot "UNIPROT:Q9Y2T3"
    ]
    graphics [
      x 919.1509859765924
      y 1143.6892620298458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y2T3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1188;urn:miriam:obo.chebi:CHEBI%3A15318"
      hgnc "NA"
      map_id "Xanthine"
      name "Xanthine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa309"
      uniprot "NA"
    ]
    graphics [
      x 1236.8413353653973
      y 1123.8388381096988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Xanthine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:123912;urn:miriam:obo.chebi:CHEBI%3A17973"
      hgnc "NA"
      map_id "_alpha__minus_D_minus_Galactose_minus_1P"
      name "_alpha__minus_D_minus_Galactose_minus_1P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 801.78585617284
      y 347.88974544578525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha__minus_D_minus_Galactose_minus_1P"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:13260264"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_28"
      name "galactose-1-phosphate uridylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 583.9285584550812
      y 562.6048460349687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000155;urn:miriam:ensembl:ENSG00000213930;urn:miriam:hgnc:4135;urn:miriam:ec-code:2.7.7.12;urn:miriam:hgnc.symbol:GALT;urn:miriam:hgnc.symbol:GALT;urn:miriam:ncbigene:2592;urn:miriam:ncbigene:2592;urn:miriam:uniprot:P07902;urn:miriam:uniprot:P07902"
      hgnc "HGNC_SYMBOL:GALT"
      map_id "UNIPROT:P07902"
      name "GALT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P07902"
    ]
    graphics [
      x 545.7248470926188
      y 452.4279893291828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P07902"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439165;urn:miriam:obo.chebi:CHEBI%3A29042"
      hgnc "NA"
      map_id "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
      name "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa11"
      uniprot "NA"
    ]
    graphics [
      x 699.135775957609
      y 802.0274211784707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_58"
      name "Deoxyguanosine phosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1287.9454702839964
      y 1193.0979607743354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_60"
      name "GMP:diphosphate 5-phospho-alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1466.317688546719
      y 1093.1546746874503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:5157;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:ec-code:2.4.2.8;urn:miriam:refseq:NM_000194;urn:miriam:ncbigene:3251;urn:miriam:ncbigene:3251;urn:miriam:ensembl:ENSG00000165704;urn:miriam:uniprot:P00492;urn:miriam:uniprot:P00492"
      hgnc "HGNC_SYMBOL:HPRT1"
      map_id "UNIPROT:P00492"
      name "HPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa267; sa332"
      uniprot "UNIPROT:P00492"
    ]
    graphics [
      x 1435.62248870557
      y 1223.9591745994808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00492"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267; urn:miriam:pubmed:17355872;urn:miriam:pubchem.compound:5361;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "UNIPROT:Q9NXA8"
      name "SIRT5; SIRT5:Nsp14; SScomp"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa403; csa6; sa1408; csa128"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 804.0783120793164
      y 1059.9938778814503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NXA8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_11"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 619.3049945395672
      y 1230.7992137657218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "Urea_space_cycle"
      name "Urea_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa412"
      uniprot "NA"
    ]
    graphics [
      x 552.8466340429222
      y 1358.193999877134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Urea_space_cycle"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18319;urn:miriam:pubchem.compound:160666"
      hgnc "NA"
      map_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa189"
      uniprot "NA"
    ]
    graphics [
      x 1543.6929595380802
      y 1141.3837841962268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:13366975"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_46"
      name "adenoylsuccinate lyase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1483.298696487521
      y 1502.9708983472585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:ADSL;urn:miriam:hgnc.symbol:ADSL;urn:miriam:ncbigene:158;urn:miriam:ncbigene:158;urn:miriam:ec-code:4.3.2.2;urn:miriam:hgnc:291;urn:miriam:uniprot:P30566;urn:miriam:uniprot:P30566;urn:miriam:refseq:NM_000026;urn:miriam:ensembl:ENSG00000239900"
      hgnc "HGNC_SYMBOL:ADSL"
      map_id "UNIPROT:P30566"
      name "ADSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa197"
      uniprot "UNIPROT:P30566"
    ]
    graphics [
      x 1298.2425872162876
      y 1528.8736432692078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P30566"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18406;urn:miriam:pubchem.compound:65110"
      hgnc "NA"
      map_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 1358.9340006398793
      y 1648.2860158303554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5460307;urn:miriam:obo.chebi:CHEBI%3A29806"
      hgnc "NA"
      map_id "Fumarate"
      name "Fumarate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 1432.2549177156359
      y 1626.5558331073096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fumarate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:13463019"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_68"
      name "AMP deaminase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1000.2925016624295
      y 880.3989115638375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "AMDP2"
      name "AMDP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa305"
      uniprot "NA"
    ]
    graphics [
      x 831.7392111337166
      y 917.2075282991952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMDP2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:468;urn:miriam:ncbigene:270;urn:miriam:refseq:NM_000036;urn:miriam:ncbigene:270;urn:miriam:ec-code:3.5.4.6;urn:miriam:ensembl:ENSG00000116748;urn:miriam:uniprot:P23109;urn:miriam:uniprot:P23109;urn:miriam:hgnc.symbol:AMPD1;urn:miriam:hgnc.symbol:AMPD1"
      hgnc "HGNC_SYMBOL:AMPD1"
      map_id "UNIPROT:P23109"
      name "AMPD1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P23109"
    ]
    graphics [
      x 876.7500592061415
      y 953.723696290617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P23109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AMPD3;urn:miriam:hgnc.symbol:AMPD3;urn:miriam:refseq:NM_000480;urn:miriam:ec-code:3.5.4.6;urn:miriam:hgnc:470;urn:miriam:uniprot:Q01432;urn:miriam:uniprot:Q01432;urn:miriam:ensembl:ENSG00000133805;urn:miriam:ncbigene:272;urn:miriam:ncbigene:272"
      hgnc "HGNC_SYMBOL:AMPD3"
      map_id "UNIPROT:Q01432"
      name "AMPD3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:Q01432"
    ]
    graphics [
      x 820.0440161200715
      y 860.4848705336356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q01432"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:30816613"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_37"
      name "UDP glucose pyrophosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 861.6791618924827
      y 1009.2896485884196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:UGP2;urn:miriam:ncbigene:7360;urn:miriam:uniprot:Q16851;urn:miriam:uniprot:Q16851;urn:miriam:hgnc.symbol:UGP2;urn:miriam:ncbigene:7360;urn:miriam:hgnc:12527;urn:miriam:ec-code:2.7.7.9;urn:miriam:ensembl:ENSG00000169764;urn:miriam:refseq:NM_006759"
      hgnc "HGNC_SYMBOL:UGP2"
      map_id "UNIPROT:Q16851"
      name "UGP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:Q16851"
    ]
    graphics [
      x 779.6102133189235
      y 1169.14060727477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16851"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubchem.compound:6133;urn:miriam:obo.chebi:CHEBI%3A15713; urn:miriam:obo.chebi:CHEBI%3A15713"
      hgnc "NA"
      map_id "UTP"
      name "UTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa13; sa40"
      uniprot "NA"
    ]
    graphics [
      x 843.037858925152
      y 1186.298900752878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UTP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:14927650"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_77"
      name "Adenosine aminohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 1163.5402829284412
      y 1044.0910694676522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439531;urn:miriam:obo.chebi:CHEBI%3A17164"
      hgnc "NA"
      map_id "Stachyose"
      name "Stachyose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa53"
      uniprot "NA"
    ]
    graphics [
      x 377.7460749065831
      y 834.9486285133822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Stachyose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_17"
      name "Stachyose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 509.48221907143954
      y 771.905160711022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:176077;urn:miriam:pubmed:10866822;urn:miriam:obo.chebi:CHEBI%3A135923"
      hgnc "NA"
      map_id "Migalastat"
      name "Migalastat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 414.26208566822754
      y 662.695041158366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Migalastat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16634;urn:miriam:pubchem.compound:439242"
      hgnc "NA"
      map_id "Raffinose"
      name "Raffinose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 441.91615799108536
      y 606.9346454555191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Raffinose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:4543472"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_89"
      name "2'-Deoxyadenosine 5'-diphosphate:oxidized-thioredoxin 2'-oxidoreductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 803.5818991400115
      y 584.1295737175494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439357;urn:miriam:obo.chebi:CHEBI%3A28061"
      hgnc "NA"
      map_id "_alpha__minus_D_minus_Galactose"
      name "_alpha__minus_D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 791.1256319503879
      y 189.18605628008606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "_alpha__minus_D_minus_Galactose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:14596685"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_18"
      name "galactokinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1087.2908762986003
      y 245.83946428605088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000154;urn:miriam:ensembl:ENSG00000108479;urn:miriam:hgnc.symbol:GALK1;urn:miriam:hgnc.symbol:GALK1;urn:miriam:hgnc:4118;urn:miriam:ncbigene:2584;urn:miriam:ncbigene:2584;urn:miriam:ec-code:2.7.1.6;urn:miriam:uniprot:P51570;urn:miriam:uniprot:P51570"
      hgnc "HGNC_SYMBOL:GALK1"
      map_id "UNIPROT:P51570"
      name "GALK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P51570"
    ]
    graphics [
      x 1136.2617470429393
      y 87.13198731751152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P51570"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:18569334"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_73"
      name "inosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1097.2906510519012
      y 789.7159968209955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:XDH;urn:miriam:hgnc.symbol:XDH;urn:miriam:uniprot:P47989;urn:miriam:uniprot:P47989;urn:miriam:ensembl:ENSG00000158125;urn:miriam:ncbigene:7498;urn:miriam:ncbigene:7498;urn:miriam:ec-code:1.17.1.4;urn:miriam:ec-code:1.17.3.2;urn:miriam:refseq:NM_000379;urn:miriam:hgnc:12805"
      hgnc "HGNC_SYMBOL:XDH"
      map_id "UNIPROT:P47989"
      name "XDH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P47989"
    ]
    graphics [
      x 946.5687149554846
      y 745.6673842285662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P47989"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37737;urn:miriam:pubchem.compound:439905"
      hgnc "NA"
      map_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
      name "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa153"
      uniprot "NA"
    ]
    graphics [
      x 1480.4993821013256
      y 889.9168009098371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:13563520"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_40"
      name "5-Phospho-D-ribosylamine:glycine ligase (ADP-forming)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1436.4741295724311
      y 786.1040537825342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:750;urn:miriam:obo.chebi:CHEBI%3A15428"
      hgnc "NA"
      map_id "Glycine"
      name "Glycine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1573.7060646743503
      y 891.9991076552659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Glycine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000819;urn:miriam:ec-code:2.1.2.2;urn:miriam:uniprot:P22102;urn:miriam:uniprot:P22102;urn:miriam:ec-code:6.3.3.1;urn:miriam:ncbigene:2618;urn:miriam:ncbigene:2618;urn:miriam:ec-code:6.3.4.13;urn:miriam:hgnc:4163;urn:miriam:hgnc.symbol:GART;urn:miriam:ensembl:ENSG00000159131;urn:miriam:hgnc.symbol:GART"
      hgnc "HGNC_SYMBOL:GART"
      map_id "UNIPROT:P22102"
      name "GART"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa160"
      uniprot "UNIPROT:P22102"
    ]
    graphics [
      x 1505.381178264488
      y 944.7646866016548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P22102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:129630972;urn:miriam:obo.chebi:CHEBI%3A143788"
      hgnc "NA"
      map_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
      name "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 1512.706572466714
      y 1014.569421911388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:15026423;PUBMED:9778377"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_9"
      name "galactose mutarotase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 585.1296177588597
      y 309.60155080062145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_138801;urn:miriam:hgnc:24063;urn:miriam:ec-code:5.1.3.3;urn:miriam:uniprot:Q96C23;urn:miriam:uniprot:Q96C23;urn:miriam:ensembl:ENSG00000143891;urn:miriam:ncbigene:130589;urn:miriam:ncbigene:130589;urn:miriam:hgnc.symbol:GALM;urn:miriam:hgnc.symbol:GALM"
      hgnc "HGNC_SYMBOL:GALM"
      map_id "UNIPROT:Q96C23"
      name "GALM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q96C23"
    ]
    graphics [
      x 661.3712921785927
      y 191.1242213592393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96C23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:4310599"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_59"
      name "2'-Deoxyguanosine 5'-triphosphate diphosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1182.444891378658
      y 760.3792986268847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:3704;urn:miriam:ncbigene:3704;urn:miriam:refseq:NM_033453;urn:miriam:ec-code:3.6.1.9;urn:miriam:ensembl:ENSG00000125877;urn:miriam:hgnc:6176;urn:miriam:hgnc.symbol:ITPA;urn:miriam:hgnc.symbol:ITPA;urn:miriam:uniprot:Q9BY32;urn:miriam:uniprot:Q9BY32"
      hgnc "HGNC_SYMBOL:ITPA"
      map_id "UNIPROT:Q9BY32"
      name "ITPA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa264"
      uniprot "UNIPROT:Q9BY32"
    ]
    graphics [
      x 1176.68980249572
      y 907.4372760062448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BY32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398635;urn:miriam:obo.chebi:CHEBI%3A16750"
      hgnc "NA"
      map_id "Guanosine"
      name "Guanosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 1276.402749918052
      y 1341.8777412437366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_61"
      name "guanosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1436.8114500079434
      y 1341.1439128146076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:4324895"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_34"
      name "nicotinate phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 1511.5451043906778
      y 722.5355287493576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "NAPRT1"
      name "NAPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 1625.9895246150536
      y 819.1054832006474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAPRT1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:13416279"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_27"
      name "nicotinamide phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 1671.4399937602548
      y 768.0066055110375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835; urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835"
      hgnc "HGNC_SYMBOL:NAMPT"
      map_id "UNIPROT:P43490"
      name "NAMPT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa108; sa206"
      uniprot "UNIPROT:P43490"
    ]
    graphics [
      x 1686.9759210744178
      y 871.1189133906166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P43490"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28413;urn:miriam:pubchem.compound:165388"
      hgnc "NA"
      map_id "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
      name "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 1580.2286903211264
      y 583.7249077785656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:3036807"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_45"
      name "phosphoribosylaminoimidazole-succinocarboxamide synthase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1456.7641365618483
      y 725.387425838747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17053;urn:miriam:pubchem.compound:5960"
      hgnc "NA"
      map_id "L_minus_Aspartate"
      name "L_minus_Aspartate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1531.6941804450312
      y 845.6110215811317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Aspartate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P22234;urn:miriam:uniprot:P22234;urn:miriam:hgnc:8587;urn:miriam:ncbigene:10606;urn:miriam:ncbigene:10606;urn:miriam:refseq:NM_006452;urn:miriam:ensembl:ENSG00000128050;urn:miriam:hgnc.symbol:PAICS;urn:miriam:hgnc.symbol:PAICS;urn:miriam:ec-code:6.3.2.6;urn:miriam:ec-code:4.1.1.21"
      hgnc "HGNC_SYMBOL:PAICS"
      map_id "UNIPROT:P22234"
      name "PAICS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa185"
      uniprot "UNIPROT:P22234"
    ]
    graphics [
      x 1519.3244741155086
      y 540.825094243346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P22234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:4307347"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_52"
      name "Guanylate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1132.6724111606695
      y 612.1151378549691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405929"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_43"
      name "2-(Formamido)-N1-(5-phosphoribosyl)acetamidine cyclo-ligase "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1412.8660090504088
      y 678.2510507216261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A138560;urn:miriam:pubchem.compound:161500"
      hgnc "NA"
      map_id "Aminoimidazole_space_ribotide"
      name "Aminoimidazole_space_ribotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa179"
      uniprot "NA"
    ]
    graphics [
      x 1459.2657005836263
      y 486.93289543898754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Aminoimidazole_space_ribotide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:2183217"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_41"
      name "10-formyltetrahydrofolate:5'-phosphoribosylglycinamide formyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1399.5971497881224
      y 1146.4878402350155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135450591;urn:miriam:obo.chebi:CHEBI%3A15637"
      hgnc "NA"
      map_id "10_minus_Formyltetrahydrofolate"
      name "10_minus_Formyltetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa167; sa201"
      uniprot "NA"
    ]
    graphics [
      x 1309.285737655056
      y 1425.2721397066675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "10_minus_Formyltetrahydrofolate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135444742;urn:miriam:obo.chebi:CHEBI%3A67016"
      hgnc "NA"
      map_id "Tetrahydrofolate"
      name "Tetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa168; sa200"
      uniprot "NA"
    ]
    graphics [
      x 1373.0148221373938
      y 1434.9089702665847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Tetrahydrofolate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:13672969"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_47"
      name "phosphoribosylaminoimidazolecarboxamide formyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1215.8034765678692
      y 1589.2368765056435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000138363;urn:miriam:uniprot:P31939;urn:miriam:uniprot:P31939;urn:miriam:refseq:NM_004044;urn:miriam:ec-code:3.5.4.10;urn:miriam:hgnc:794;urn:miriam:hgnc.symbol:ATIC;urn:miriam:ncbigene:471;urn:miriam:hgnc.symbol:ATIC;urn:miriam:ncbigene:471;urn:miriam:ec-code:2.1.2.3"
      hgnc "HGNC_SYMBOL:ATIC"
      map_id "UNIPROT:P31939"
      name "ATIC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa199"
      uniprot "UNIPROT:P31939"
    ]
    graphics [
      x 1100.0630764144423
      y 1468.959409665711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31939"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18381;urn:miriam:pubchem.compound:166760"
      hgnc "NA"
      map_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa198"
      uniprot "NA"
    ]
    graphics [
      x 1046.868450394277
      y 1507.7148568565306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "PUBMED:16756498"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_30"
      name "NAD-dependent deacetylase sirtuin-5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1007.5622394245868
      y 624.9093027217543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.substance:5094"
      hgnc "NA"
      map_id "Histone_space_N6_minus_acetyl_minus_L_minus_lysine"
      name "Histone_space_N6_minus_acetyl_minus_L_minus_lysine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa115"
      uniprot "NA"
    ]
    graphics [
      x 1001.0997438745255
      y 470.60809097520234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Histone_space_N6_minus_acetyl_minus_L_minus_lysine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:72193709;urn:miriam:obo.chebi:CHEBI%3A76279"
      hgnc "NA"
      map_id "O_minus_Acetyl_minus_ADP_minus_ribose"
      name "O_minus_Acetyl_minus_ADP_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa117"
      uniprot "NA"
    ]
    graphics [
      x 872.3991042497207
      y 541.1462003436532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "O_minus_Acetyl_minus_ADP_minus_ribose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A5738;urn:miriam:pubchem.substance:5447;urn:miriam:pubchem.substance:223439948"
      hgnc "NA"
      map_id "Histone_minus_L_minus_lysine"
      name "Histone_minus_L_minus_lysine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa116"
      uniprot "NA"
    ]
    graphics [
      x 916.2271786225605
      y 482.37375475415814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Histone_minus_L_minus_lysine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:14444527"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_71"
      name "inosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1132.0615333836793
      y 1001.1902293055573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:17291528"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_63"
      name "GTP phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1087.0726298987554
      y 864.316085735778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:28204;urn:miriam:refseq:NM_032324;urn:miriam:ec-code:3.6.1.15;urn:miriam:uniprot:Q9BSD7;urn:miriam:uniprot:Q9BSD7;urn:miriam:ensembl:ENSG00000135778;urn:miriam:hgnc.symbol:NTPCR;urn:miriam:hgnc.symbol:NTPCR;urn:miriam:ncbigene:84284;urn:miriam:ncbigene:84284"
      hgnc "HGNC_SYMBOL:NTPCR"
      map_id "UNIPROT:Q9BSD7"
      name "NTPCR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa283"
      uniprot "UNIPROT:Q9BSD7"
    ]
    graphics [
      x 1184.4588353273277
      y 979.325254387911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BSD7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:5799033;PUBMED:22555152"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 998.076749040385
      y 768.6575637807152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000106348;urn:miriam:ncbigene:3614;urn:miriam:refseq:NM_000883;urn:miriam:ncbigene:3614;urn:miriam:hgnc.symbol:IMPDH1;urn:miriam:hgnc.symbol:IMPDH1;urn:miriam:ec-code:1.1.1.205;urn:miriam:hgnc:6052;urn:miriam:uniprot:P20839;urn:miriam:uniprot:P20839"
      hgnc "HGNC_SYMBOL:IMPDH1"
      map_id "UNIPROT:P20839"
      name "IMPDH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "UNIPROT:P20839"
    ]
    graphics [
      x 806.3533889104153
      y 655.9374662948244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P20839"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:5799033;urn:miriam:pubchem.compound:446541;urn:miriam:obo.chebi:CHEBI%3A168396; urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "Mycophenolic_space_acid"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa213; sa1427; sa1398"
      uniprot "NA"
    ]
    graphics [
      x 826.7580224595919
      y 787.3226566919205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mycophenolic_space_acid"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:53241;urn:miriam:pubmed:10878288"
      hgnc "NA"
      map_id "Merimepodib"
      name "Merimepodib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 897.6453320331256
      y 690.3400759864634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Merimepodib"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A63580;urn:miriam:pubmed:22555152;urn:miriam:pubchem.compound:37542; urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "NA"
      map_id "Ribavirin"
      name "Ribavirin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa214; sa1405"
      uniprot "NA"
    ]
    graphics [
      x 801.8395389719293
      y 742.0493323227449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Ribavirin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15652;urn:miriam:pubchem.compound:73323"
      hgnc "NA"
      map_id "XMP"
      name "XMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa204"
      uniprot "NA"
    ]
    graphics [
      x 1158.1638257535208
      y 821.7433944173732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "XMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_62"
      name "guanosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1174.174894836971
      y 1137.4861462472625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "PUBMED:13563458"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_51"
      name "GMP synthase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 1280.9659663821008
      y 707.8298887552163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_003875;urn:miriam:pubmed:8089153;urn:miriam:hgnc:4378;urn:miriam:uniprot:P49915;urn:miriam:uniprot:P49915;urn:miriam:ensembl:ENSG00000163655;urn:miriam:hgnc.symbol:GMPS;urn:miriam:hgnc.symbol:GMPS;urn:miriam:ec-code:6.3.5.2;urn:miriam:ncbigene:8833;urn:miriam:ncbigene:8833"
      hgnc "HGNC_SYMBOL:GMPS"
      map_id "UNIPROT:P49915"
      name "GMPS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa216"
      uniprot "UNIPROT:P49915"
    ]
    graphics [
      x 1192.1899304789445
      y 635.6485947792451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P49915"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:9500840"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_44"
      name "phosphoribosylaminoimidazole carboxylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1545.2472305283095
      y 444.43596804670904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 720.7950153614674
      y 1265.7372639902921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:13717627"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_21"
      name "NAD synthetase 1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 1225.1023060013952
      y 466.8064795758393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:hgnc:29832;urn:miriam:uniprot:Q6IA69;urn:miriam:uniprot:Q6IA69;urn:miriam:pubmed:12547821;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161; urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:uniprot:Q6IA69;urn:miriam:hgnc:29832;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161"
      hgnc "HGNC_SYMBOL:NADSYN1"
      map_id "UNIPROT:Q6IA69"
      name "NADSYN1; NADSYN1_space_hexamer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84; sa208"
      uniprot "UNIPROT:Q6IA69"
    ]
    graphics [
      x 1226.1594787160882
      y 304.0589257453043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6IA69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_75"
      name "hypoxanthine-guanine phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1367.5216502324213
      y 1068.5052431934332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:13549414"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_82"
      name "Adenlyate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1501.9613183219976
      y 263.768792085407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AK5;urn:miriam:hgnc.symbol:AK5;urn:miriam:ncbigene:26289;urn:miriam:ncbigene:26289;urn:miriam:ensembl:ENSG00000154027;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:hgnc:365;urn:miriam:uniprot:Q9Y6K8;urn:miriam:uniprot:Q9Y6K8;urn:miriam:refseq:NM_174858"
      hgnc "HGNC_SYMBOL:AK5"
      map_id "UNIPROT:Q9Y6K8"
      name "AK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa389; sa370"
      uniprot "UNIPROT:Q9Y6K8"
    ]
    graphics [
      x 1415.7434177940588
      y 117.04347368176968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y6K8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q96M32;urn:miriam:uniprot:Q96M32;urn:miriam:hgnc:20091;urn:miriam:hgnc.symbol:AK7;urn:miriam:hgnc.symbol:AK7;urn:miriam:ncbigene:122481;urn:miriam:ncbigene:122481;urn:miriam:ensembl:ENSG00000140057;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:refseq:NM_001350888"
      hgnc "HGNC_SYMBOL:AK7"
      map_id "UNIPROT:Q96M32"
      name "AK7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:Q96M32"
    ]
    graphics [
      x 1640.152746968501
      y 182.42975044101115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96M32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P00568;urn:miriam:uniprot:P00568;urn:miriam:refseq:NM_000476;urn:miriam:ensembl:ENSG00000106992;urn:miriam:ncbigene:203;urn:miriam:ncbigene:203;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:361;urn:miriam:ec-code:2.7.4.3;urn:miriam:hgnc.symbol:AK1;urn:miriam:hgnc.symbol:AK1"
      hgnc "HGNC_SYMBOL:AK1"
      map_id "UNIPROT:P00568"
      name "AK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa392"
      uniprot "UNIPROT:P00568"
    ]
    graphics [
      x 1551.6026903675893
      y 120.61919748093453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00568"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:158067;urn:miriam:ncbigene:158067;urn:miriam:hgnc.symbol:AK8;urn:miriam:ensembl:ENSG00000165695;urn:miriam:hgnc.symbol:AK8;urn:miriam:hgnc:26526;urn:miriam:refseq:NM_152572;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:uniprot:Q96MA6;urn:miriam:uniprot:Q96MA6"
      hgnc "HGNC_SYMBOL:AK8"
      map_id "UNIPROT:Q96MA6"
      name "AK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa390"
      uniprot "UNIPROT:Q96MA6"
    ]
    graphics [
      x 1615.8490872271173
      y 226.3751250298002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96MA6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_20"
      name "NAD(P) transhydrogenase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 883.1725878725507
      y 422.1441453021386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7863;urn:miriam:ncbigene:23530;urn:miriam:ncbigene:23530;urn:miriam:ensembl:ENSG00000112992;urn:miriam:hgnc.symbol:NNT;urn:miriam:refseq:NM_182977;urn:miriam:hgnc.symbol:NNT;urn:miriam:uniprot:Q13423;urn:miriam:uniprot:Q13423;urn:miriam:ec-code:7.1.1.1"
      hgnc "HGNC_SYMBOL:NNT"
      map_id "UNIPROT:Q13423"
      name "NNT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:Q13423"
    ]
    graphics [
      x 737.4228180274308
      y 348.8465918658226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q13423"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:5822067"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_67"
      name "Lactose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 457.80782895731966
      y 850.6982029993605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4298;urn:miriam:ec-code:3.2.1.23;urn:miriam:hgnc.symbol:GLB1;urn:miriam:hgnc.symbol:GLB1;urn:miriam:refseq:NM_000404;urn:miriam:ensembl:ENSG00000170266;urn:miriam:uniprot:P16278;urn:miriam:uniprot:P16278;urn:miriam:ncbigene:2720;urn:miriam:ncbigene:2720"
      hgnc "HGNC_SYMBOL:GLB1"
      map_id "UNIPROT:P16278"
      name "GLB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:P16278"
    ]
    graphics [
      x 383.95872911997606
      y 977.6058822682726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P16278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_002299;urn:miriam:ensembl:ENSG00000115850;urn:miriam:hgnc:6530;urn:miriam:hgnc.symbol:LCT;urn:miriam:hgnc.symbol:LCT;urn:miriam:ec-code:3.2.1.108;urn:miriam:ncbigene:3938;urn:miriam:ncbigene:3938;urn:miriam:uniprot:P09848;urn:miriam:uniprot:P09848;urn:miriam:ec-code:3.2.1.62"
      hgnc "HGNC_SYMBOL:LCT"
      map_id "UNIPROT:P09848"
      name "LCT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa57"
      uniprot "UNIPROT:P09848"
    ]
    graphics [
      x 323.4238360159512
      y 925.7078111438725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09848"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:13502325"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_49"
      name "IMP cyclohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1011.396193239932
      y 1268.044049105263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "PUBMED:11866528"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_31"
      name "NAD+ glycohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1199.494730489053
      y 558.5077735090704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:pubmed:16690024;urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907;urn:miriam:uniprot:P28907; urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907"
      hgnc "HGNC_SYMBOL:CD38"
      map_id "UNIPROT:P28907"
      name "CD38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa121; sa144; sa265"
      uniprot "UNIPROT:P28907"
    ]
    graphics [
      x 1201.3188013493273
      y 357.69219368866186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P28907"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:33576;urn:miriam:obo.chebi:CHEBI%3A16960"
      hgnc "NA"
      map_id "ADP_minus_D_minus_ribose"
      name "ADP_minus_D_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 1059.6416398927063
      y 528.7924151258525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP_minus_D_minus_ribose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_69"
      name "XMP 5'-nucleotidase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1249.0460305844583
      y 948.2692126617484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:64959;urn:miriam:obo.chebi:CHEBI%3A18107"
      hgnc "NA"
      map_id "Xanthosine"
      name "Xanthosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa308"
      uniprot "NA"
    ]
    graphics [
      x 1421.7826345990366
      y 1066.0367000951483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Xanthosine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17713;urn:miriam:pubchem.compound:12599"
      hgnc "NA"
      map_id "dAMP"
      name "dAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa364"
      uniprot "NA"
    ]
    graphics [
      x 1375.2095482954867
      y 252.67955415296876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dAMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:5862227"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_86"
      name "adenylate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1264.2016836710695
      y 211.33238157695632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_15"
      name "Raffinose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 595.948230460735
      y 638.8158146183911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17992;urn:miriam:pubchem.compound:5988"
      hgnc "NA"
      map_id "Sucrose"
      name "Sucrose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 488.4324738835028
      y 480.9953136426972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Sucrose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:11829748"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_26"
      name "2'-phospho-ADP-ribosyl cyclase/2'-phospho-cyclic-ADP-ribose transferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1313.4366157555617
      y 313.2366617160593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:71768143;urn:miriam:obo.chebi:CHEBI%3A75967"
      hgnc "NA"
      map_id "nicotinate_minus_adenine_space_dinucleotide_space_phosphate"
      name "nicotinate_minus_adenine_space_dinucleotide_space_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa107"
      uniprot "NA"
    ]
    graphics [
      x 1290.5894482052372
      y 149.57561507995172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinate_minus_adenine_space_dinucleotide_space_phosphate"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28053;urn:miriam:pubchem.compound:440658"
      hgnc "NA"
      map_id "Melibiose"
      name "Melibiose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 362.956672665224
      y 705.4618532777789
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Melibiose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:16661511;PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_87"
      name "melibiose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 504.1904755447197
      y 715.6104663095459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:11594753"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_19"
      name "NAD kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1163.0460040153253
      y 410.2162116130337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130; urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:O95544;urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130;urn:miriam:obo.chebi:CHEBI%3A29105; urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK"
      hgnc "HGNC_SYMBOL:NADK"
      map_id "UNIPROT:O95544"
      name "NADK; NADK:Zn2_plus__space_tetramer; APOA1BP"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa75; csa17; sa329"
      uniprot "UNIPROT:O95544"
    ]
    graphics [
      x 1167.7843222340903
      y 253.25720991673836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95544"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:5667299"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_85"
      name "ATP:deoxyadenosine 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 1303.4186940210868
      y 467.9684714664338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 948.2814361854703
      y 1370.256444736076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "PUBMED:13684981"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_22"
      name "nicotinamide-nucleotide adenylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1555.1109920347208
      y 505.73123421784715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_29"
      name "N-Ribosylnicotinamide:orthophosphate ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1594.7090359387553
      y 1026.7507744509776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:14235537"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 1386.1959981622626
      y 760.1249058605175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q06203;urn:miriam:uniprot:Q06203;urn:miriam:hgnc:9238;urn:miriam:hgnc.symbol:PPAT;urn:miriam:hgnc.symbol:PPAT;urn:miriam:ncbigene:5471;urn:miriam:ensembl:ENSG00000128059;urn:miriam:ncbigene:5471;urn:miriam:refseq:NM_002703;urn:miriam:ec-code:2.4.2.14"
      hgnc "HGNC_SYMBOL:PPAT"
      map_id "UNIPROT:Q06203"
      name "PPAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa158"
      uniprot "UNIPROT:Q06203"
    ]
    graphics [
      x 1377.4016890166602
      y 631.8452442940052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q06203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_70"
      name "Xanthosine phosphorlyase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1486.7569839288412
      y 1193.5735564944873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 243
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Adenosine"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 4
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 12
    target 14
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_80"
      target_id "Adenine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 12
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_80"
      target_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Thioredoxin"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 19
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 17
    target 20
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "dGDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 17
    target 21
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "Thioredoxin_space_disulfide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 17
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P06280"
      target_id "M112_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 10
    target 23
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nsp14"
      target_id "M112_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Galacitol"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NADP"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P15121"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 25
    target 28
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 25
    target 8
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "NADPH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 25
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus__alpha__minus_D_minus_Glucose"
      target_id "M112_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14376"
      target_id "M112_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_48"
      target_id "UDP_minus__alpha__minus_D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 20
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "dGDP"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 5
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13232"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 36
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22392;UNIPROT:P15531"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 37
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5B8"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 38
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75414"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 39
    target 34
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P56597"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 34
    target 40
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_55"
      target_id "dGTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 34
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_55"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinate"
      target_id "M112_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_35"
      target_id "Nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "N_minus_Ribosyl_minus_nicotinamide"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 5
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "NRK1"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "Nicotinamide_space_D_minus_ribonucleotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 45
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 45
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "D_minus_Ribose_space_5P"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 5
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P60891"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P11908"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 52
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21108"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 49
    target 53
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_38"
      target_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 49
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_38"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Hypoxanthine"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 13
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_83"
      target_id "Deoxyinosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 56
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_83"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Inosine"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 4
    target 60
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 13
    target 60
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 60
    target 55
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_72"
      target_id "Hypoxanthine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 60
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_72"
      target_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "_alpha_D_minus_Ribose_space_1P"
      target_id "M112_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96G03"
      target_id "M112_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 62
    target 48
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_36"
      target_id "D_minus_Ribose_space_5P"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 54
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "AMP"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 2
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 64
    target 11
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_79"
      target_id "Adenosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 64
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_79"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 3
    target 66
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 2
    target 66
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5L3"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 66
    target 68
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "GMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 66
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 66
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Quinolinate"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 53
    target 70
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 29
    target 70
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q15274"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 70
    target 72
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "Nicotinate_space_D_minus_ribonucleotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 70
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 70
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "CO2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Glutamine"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 5
    target 75
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 2
    target 75
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 77
    target 75
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15067"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 75
    target 78
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 75
    target 79
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "L_minus_Glutamate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 75
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 75
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 75
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 47
    target 80
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinamide_space_D_minus_ribonucleotide"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 2
    target 80
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 65
    target 80
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 80
    target 44
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_25"
      target_id "N_minus_Ribosyl_minus_nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 80
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_25"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "IMP"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Ammonium"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 26
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NADP"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 84
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P36959"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 85
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9P2T1"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 82
    target 68
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "GMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 82
    target 8
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "NADPH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 82
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M112_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_10"
      target_id "Guanine_space_nucleotide_space_synthesis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 16
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 2
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 67
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5L3"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 90
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y227"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 91
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75356"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 92
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75354"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 93
    target 89
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8WVQ1"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 89
    target 68
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "GMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 89
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 89
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 16
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GDP"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 5
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 35
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13232"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 36
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22392;UNIPROT:P15531"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 39
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P56597"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 38
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75414"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 37
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5B8"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 94
    target 3
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_53"
      target_id "GTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 94
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_53"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 14
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Adenine"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 53
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07741"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 95
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_81"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 95
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_81"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "dGMP"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 5
    target 98
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 99
    target 98
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16774"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 98
    target 20
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_56"
      target_id "dGDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 98
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_56"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 2
    target 101
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22413"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 103
    target 101
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O14638"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 101
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_23"
      target_id "Nicotinamide_space_D_minus_ribonucleotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 101
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_23"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 72
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinate_space_D_minus_ribonucleotide"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 29
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 5
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9HAN9"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 106
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BZQ4"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 107
    target 104
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96T66"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 104
    target 108
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_32"
      target_id "Deamino_minus_NAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 104
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_32"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 109
    target 110
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Deoxyadenosine"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 2
    target 110
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 29
    target 110
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00813"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 110
    target 58
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_84"
      target_id "Deoxyinosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 110
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_84"
      target_id "Ammonium"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 33
    target 112
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus__alpha__minus_D_minus_Galactose"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "_alpha__minus_D_minus_Glucose"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 114
    target 112
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00709;UNIPROT:P15291"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 112
    target 115
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_16"
      target_id "Lactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 112
    target 7
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_16"
      target_id "UDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "dADP"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 5
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 36
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22392;UNIPROT:P15531"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 39
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P56597"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 35
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13232"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 38
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O75414"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 37
    target 117
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5B8"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_88"
      target_id "dATP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 117
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_88"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 11
    target 119
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Adenosine"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 5
    target 119
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 120
    target 119
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P55263"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 119
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 119
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 119
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Deoxyguanosine"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 5
    target 122
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27707"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 122
    target 97
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "dGMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 122
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 122
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Guanine"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 2
    target 125
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 29
    target 125
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y2T3"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 125
    target 127
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_74"
      target_id "Xanthine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 125
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_74"
      target_id "Ammonium"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 128
    target 129
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "_alpha__minus_D_minus_Galactose_minus_1P"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 30
    target 129
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus__alpha__minus_D_minus_Glucose"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P07902"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 129
    target 33
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_28"
      target_id "UDP_minus__alpha__minus_D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 129
    target 131
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_28"
      target_id "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 121
    target 132
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Deoxyguanosine"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 4
    target 132
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 13
    target 132
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 132
    target 124
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_58"
      target_id "Guanine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 132
    target 57
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_58"
      target_id "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 124
    target 133
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Guanine"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 53
    target 133
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00492"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 133
    target 68
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_60"
      target_id "GMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 133
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_60"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NXA8"
      target_id "M112_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_11"
      target_id "Urea_space_cycle"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
      target_id "M112_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P30566"
      target_id "M112_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 139
    target 141
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_46"
      target_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 139
    target 142
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_46"
      target_id "Fumarate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 54
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "AMP"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 29
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 2
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "AMDP2"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 145
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P23109"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 146
    target 143
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q01432"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 143
    target 81
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_68"
      target_id "IMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 143
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_68"
      target_id "Ammonium"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 30
    target 147
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UDP_minus__alpha__minus_D_minus_Glucose"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 1
    target 147
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "PPi"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16851"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 147
    target 131
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_37"
      target_id "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 147
    target 149
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_37"
      target_id "UTP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 11
    target 150
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Adenosine"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 2
    target 150
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 29
    target 150
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 111
    target 150
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00813"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 150
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_77"
      target_id "Inosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 150
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_77"
      target_id "Ammonium"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Stachyose"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 2
    target 152
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 22
    target 152
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P06280"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 153
    target 152
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Migalastat"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 152
    target 154
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_17"
      target_id "Raffinose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 152
    target 28
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_17"
      target_id "D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 6
    target 155
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ADP"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 18
    target 155
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Thioredoxin"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 19
    target 155
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 155
    target 116
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "dADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 155
    target 21
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "Thioredoxin_space_disulfide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 155
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 156
    target 157
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "_alpha__minus_D_minus_Galactose"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 5
    target 157
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P51570"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 157
    target 128
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_18"
      target_id "_alpha__minus_D_minus_Galactose_minus_1P"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 157
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_18"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 55
    target 159
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Hypoxanthine"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 100
    target 159
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 2
    target 159
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 160
    target 159
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P47989"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 159
    target 127
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "Xanthine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 159
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 159
    target 9
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "NADH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 5
    target 162
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Glycine"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 164
    target 162
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22102"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 162
    target 165
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 162
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 162
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 162
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 28
    target 166
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "D_minus_Galactose"
      target_id "M112_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 167
    target 166
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96C23"
      target_id "M112_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 166
    target 156
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_9"
      target_id "_alpha__minus_D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 40
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "dGTP"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 2
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 102
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22413"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 103
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O14638"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BY32"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 168
    target 97
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "dGMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 168
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 168
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Guanosine"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 4
    target 171
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 13
    target 171
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 171
    target 124
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_61"
      target_id "Guanine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 171
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_61"
      target_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 41
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinate"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 53
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 2
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 5
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 173
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "NAPRT1"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 172
    target 72
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "Nicotinate_space_D_minus_ribonucleotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 172
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 172
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 172
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 43
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinamide"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 53
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P43490"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 174
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_27"
      target_id "Nicotinamide_space_D_minus_ribonucleotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 174
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_27"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 176
    target 177
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 5
    target 177
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 178
    target 177
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Aspartate"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 179
    target 177
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22234"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 177
    target 138
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 177
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 177
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 177
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 68
    target 180
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GMP"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 5
    target 180
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 99
    target 180
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16774"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 180
    target 16
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_52"
      target_id "GDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 180
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_52"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 78
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 5
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 164
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22102"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 181
    target 182
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "Aminoimidazole_space_ribotide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 181
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 181
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 181
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 165
    target 183
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 184
    target 183
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "10_minus_Formyltetrahydrofolate"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 164
    target 183
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22102"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 183
    target 74
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 183
    target 185
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "Tetrahydrofolate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 183
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 141
    target 186
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 184
    target 186
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "10_minus_Formyltetrahydrofolate"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 187
    target 186
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31939"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 186
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_47"
      target_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 186
    target 185
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_47"
      target_id "Tetrahydrofolate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 100
    target 189
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 190
    target 189
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Histone_space_N6_minus_acetyl_minus_L_minus_lysine"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 2
    target 189
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 135
    target 189
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NXA8"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 189
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "Nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 189
    target 191
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "O_minus_Acetyl_minus_ADP_minus_ribose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 189
    target 192
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "Histone_minus_L_minus_lysine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 81
    target 193
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "IMP"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 2
    target 193
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 65
    target 193
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 193
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_71"
      target_id "Inosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 193
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_71"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 3
    target 194
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GTP"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 2
    target 194
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 67
    target 194
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y5L3"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 195
    target 194
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BSD7"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 194
    target 16
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "GDP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 194
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 194
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 81
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "IMP"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 100
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 2
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P20839"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 86
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P12268"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 198
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Mycophenolic_space_acid"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 199
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Merimepodib"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 200
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Ribavirin"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 196
    target 201
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "XMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 196
    target 9
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "NADH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 196
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 68
    target 202
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "GMP"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 2
    target 202
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 65
    target 202
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 202
    target 170
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_62"
      target_id "Guanosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 202
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_62"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "XMP"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 2
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 5
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 76
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Glutamine"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 204
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P49915"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 203
    target 68
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "GMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 203
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 203
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 203
    target 79
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "L_minus_Glutamate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 203
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 182
    target 205
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Aminoimidazole_space_ribotide"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 73
    target 205
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "CO2"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 179
    target 205
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P22234"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 205
    target 176
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_44"
      target_id "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 205
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_44"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 135
    target 206
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q9NXA8"
      target_id "M112_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 10
    target 206
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nsp14"
      target_id "M112_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 108
    target 207
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Deamino_minus_NAD"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 5
    target 207
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 76
    target 207
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Glutamine"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 2
    target 207
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6IA69"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 207
    target 100
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "NAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 207
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 207
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 207
    target 79
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "L_minus_Glutamate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 55
    target 209
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Hypoxanthine"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 53
    target 209
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 134
    target 209
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00492"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 209
    target 81
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_75"
      target_id "IMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 209
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_75"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 54
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "AMP"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 5
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 211
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y6K8"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 212
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96M32"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 213
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00568"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 214
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96MA6"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 210
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_82"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 100
    target 215
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 8
    target 215
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 216
    target 215
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q13423"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 215
    target 9
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_20"
      target_id "NADH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 215
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_20"
      target_id "NADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 115
    target 217
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Lactose"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 2
    target 217
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 218
    target 217
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P16278"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 219
    target 217
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09848"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 217
    target 28
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_67"
      target_id "D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 217
    target 113
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_67"
      target_id "_alpha__minus_D_minus_Glucose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 188
    target 220
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
      target_id "M112_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 187
    target 220
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P31939"
      target_id "M112_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 220
    target 81
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_49"
      target_id "IMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 220
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_49"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 100
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 2
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 222
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28907"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 221
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "Nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 221
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 221
    target 223
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "ADP_minus_D_minus_ribose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 201
    target 224
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "XMP"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 2
    target 224
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 65
    target 224
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 224
    target 225
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_69"
      target_id "Xanthosine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 224
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_69"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 226
    target 227
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "dAMP"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 5
    target 227
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 211
    target 227
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y6K8"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 227
    target 116
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_86"
      target_id "dADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 227
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_86"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 154
    target 228
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Raffinose"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 2
    target 228
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 22
    target 228
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P06280"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 153
    target 228
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Migalastat"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 228
    target 28
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_15"
      target_id "D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 228
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_15"
      target_id "Sucrose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 41
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinate"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 26
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NADP"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 222
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28907"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 230
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_26"
      target_id "Nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 230
    target 231
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_26"
      target_id "nicotinate_minus_adenine_space_dinucleotide_space_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 232
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Melibiose"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 2
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 22
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P06280"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 153
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "Migalastat"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 233
    target 113
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_87"
      target_id "_alpha__minus_D_minus_Glucose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 233
    target 28
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_87"
      target_id "D_minus_Galactose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 100
    target 234
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "NAD"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 5
    target 234
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 235
    target 234
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95544"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 234
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "NADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 234
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 234
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 109
    target 236
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Deoxyadenosine"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 5
    target 236
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 123
    target 236
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P27707"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 236
    target 226
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "dAMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 236
    target 6
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 236
    target 29
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "H"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 86
    target 237
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P12268"
      target_id "M112_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 10
    target 237
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nsp14"
      target_id "M112_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 47
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Nicotinamide_space_D_minus_ribonucleotide"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 5
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 29
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 106
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BZQ4"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 105
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9HAN9"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 107
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96T66"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 238
    target 100
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_22"
      target_id "NAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 238
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_22"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 44
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "N_minus_Ribosyl_minus_nicotinamide"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 4
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 13
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 239
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_29"
      target_id "Nicotinamide"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 239
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_29"
      target_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 53
    target 240
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 2
    target 240
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 76
    target 240
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Glutamine"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 241
    target 240
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q06203"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 240
    target 161
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 240
    target 79
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "L_minus_Glutamate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 240
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 225
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Xanthosine"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 4
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "Pi"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 13
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00491"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 242
    target 127
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_70"
      target_id "Xanthine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 242
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_70"
      target_id "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
