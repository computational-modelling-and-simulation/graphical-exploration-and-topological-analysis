# generated with VANTED V2.8.2 at Fri Mar 04 10:06:59 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4883; WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720; urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "AGT"
      name "AGT"
      node_subtype "SIMPLE_MOLECULE; GENE"
      node_type "species"
      org_id "b9dff; f53bd; cfd56"
      uniprot "NA"
    ]
    graphics [
      x 605.0515442130395
      y 168.68865134411197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AGT"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id695320d0"
      uniprot "NA"
    ]
    graphics [
      x 543.7453232237277
      y 271.69298179635894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4883; WP4969"
      full_annotation "urn:miriam:ncbigene:5972"
      hgnc "NA"
      map_id "REN"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "c91b1; f2946"
      uniprot "NA"
    ]
    graphics [
      x 661.5416468785927
      y 265.07978839801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "REN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2718"
      hgnc "NA"
      map_id "Angiotensin_space_1"
      name "Angiotensin_space_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d391a"
      uniprot "NA"
    ]
    graphics [
      x 432.60367917373503
      y 337.9613588126312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id45cf6f5e"
      uniprot "NA"
    ]
    graphics [
      x 315.28627983652945
      y 396.19987831013606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4883; WP4969"
      full_annotation "urn:miriam:ncbigene:1636; urn:miriam:ncbigene:1636;urn:miriam:pubmed:15174896"
      hgnc "NA"
      map_id "ACE"
      name "ACE"
      node_subtype "PROTEIN; GENE"
      node_type "species"
      org_id "e5d6d; ed520; e130d; ab666"
      uniprot "NA"
    ]
    graphics [
      x 342.8023797414909
      y 506.22264840653935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2719"
      hgnc "NA"
      map_id "Angiotensin_space_2"
      name "Angiotensin_space_2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d23b7"
      uniprot "NA"
    ]
    graphics [
      x 192.602542029877
      y 434.5023851148737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7114ee4c"
      uniprot "NA"
    ]
    graphics [
      x 76.03584108775567
      y 398.86487906010444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "AT1R"
      name "AT1R"
      node_subtype "GENE"
      node_type "species"
      org_id "f6bb2"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 276.40586899331834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AT1R"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id93eee6dc"
      uniprot "NA"
    ]
    graphics [
      x 123.7793344738875
      y 165.7378662059083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
      name "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0920"
      uniprot "NA"
    ]
    graphics [
      x 220.09029284382632
      y 83.23066678151417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb0d71735"
      uniprot "NA"
    ]
    graphics [
      x 343.4343235834736
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "Tissue_space_injury"
      name "Tissue_space_injury"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfb7f"
      uniprot "NA"
    ]
    graphics [
      x 444.17442338729745
      y 132.6185342566896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Tissue_space_injury"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 1
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "AGT"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 3
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "REN"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 2
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_31"
      target_id "Angiotensin_space_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 4
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_1"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 6
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "ACE"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 5
    target 7
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_29"
      target_id "Angiotensin_space_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 7
    target 8
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_2"
      target_id "W11_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 8
    target 9
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_32"
      target_id "AT1R"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 9
    target 10
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "AT1R"
      target_id "W11_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 10
    target 11
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_35"
      target_id "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 11
    target 12
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
      target_id "W11_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 12
    target 13
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_37"
      target_id "Tissue_space_injury"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
