# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 36
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294; urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A18361; urn:miriam:obo.chebi:CHEBI%3A35782; urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "PPi"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139; layout_407; layout_745; layout_649; layout_1268; layout_1274; layout_637; layout_145; layout_160; layout_2308; layout_2303; layout_2263; layout_2285; layout_3764; layout_2425; layout_2267; layout_2294; layout_2271; layout_2441; sa135; sa349; sa130; sa268; sa18; sa265; sa142; sa109; sa223; sa331; sa90; sa157; sa28; sa46; sa211; sa193; sa192"
      uniprot "NA"
    ]
    graphics [
      x 882.3728365948974
      y 322.4443605650208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 52
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp4 and Nsp6 protein interactions; C19DMap:E protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597; urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057; urn:miriam:obo.chebi:CHEBI%3A29235; urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "H_plus_"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_49; layout_275; layout_2028; layout_391; layout_254; layout_442; layout_318; layout_68; layout_2211; layout_2369; layout_3569; layout_3547; layout_3602; layout_2226; layout_2388; layout_2348; layout_2421; layout_3554; layout_2934; layout_3624; sa371; sa335; sa370; sa18; sa715; sa20; sa377; sa649; sa644; sa373; sa19; sa234; sa233; sa26; sa57; sa67; sa157; sa137; sa212; sa104; sa235; sa252; sa219; sa25; sa251; sa319; sa98; sa287; sa43; sa190; sa381; sa195"
      uniprot "NA"
    ]
    graphics [
      x 896.9664772004649
      y 561.360279064961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 73
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519; urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962; urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "H2O"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088; layout_147; layout_418; layout_40; layout_9; layout_18; layout_1997; layout_16; layout_277; layout_156; layout_579; layout_3385; layout_2283; layout_2430; layout_2727; layout_2764; layout_2203; layout_2215; layout_3095; layout_2273; layout_2725; layout_2898; sa243; sa344; sa278; sa172; sa98; sa287; sa96; sa361; sa328; sa303; sa335; sa54; sa375; sa324; sa263; sa140; sa119; sa315; sa284; sa208; sa272; sa219; sa87; sa25; sa203; sa122; sa50; sa34; sa156; sa18; sa8; sa157; sa26; sa158; sa106; sa211; sa103; sa206; sa261; sa255; sa21; sa318; sa33; sa205; sa27; sa46; sa10; sa353; sa36; sa194; sa257"
      uniprot "NA"
    ]
    graphics [
      x 938.1918482155585
      y 826.9359142863714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372; urn:miriam:obo.chebi:CHEBI%3A18367; urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "Pi"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "layout_181; layout_426; layout_134; layout_161; layout_2301; layout_2286; layout_2434; layout_2258; sa32; sa347; sa356; sa319; sa345; sa279; sa280; sa175; sa99; sa289; sa259; sa165; sa270; sa143; sa193; sa181; sa314; sa285; sa273; sa311; sa111; sa312; sa15; sa14; sa205; sa262; sa105"
      uniprot "NA"
    ]
    graphics [
      x 993.9714515459724
      y 725.3368549521149
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pi"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 46
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Orf10 Cul2 pathway; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592; urn:miriam:obo.chebi:CHEBI%3A15422; urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957; urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "ATP"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431; layout_131; layout_248; layout_193; layout_2307; layout_3777; layout_3773; layout_3779; layout_3775; layout_2255; layout_2439; sa33; sa246; sa101; sa150; sa174; sa230; sa249; sa128; sa387; sa338; sa252; sa6; sa161; sa139; sa191; sa227; sa180; sa217; sa81; sa354; sa371; sa76; sa365; sa88; sa9; sa44; sa203; sa239; sa229; sa218; sa107; sa293; sa204; sa103; sa94"
      uniprot "NA"
    ]
    graphics [
      x 819.4178465611817
      y 362.4731455447168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 35
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:Nsp9 protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370; urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761; urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "ADP"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133; layout_249; layout_3790; layout_3774; layout_3780; layout_3776; layout_2257; sa30; sa247; sa102; sa176; sa231; sa250; sa386; sa339; sa253; sa352; sa7; sa163; sa141; sa192; sa228; sa182; sa82; sa388; sa372; sa77; sa366; sa13; sa1201; sa204; sa217; sa240; sa226; sa99"
      uniprot "NA"
    ]
    graphics [
      x 633.4511468492483
      y 319.68958632198803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 7
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Electron Transport Chain disruption; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360; urn:miriam:obo.chebi:CHEBI%3A15846; urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "NAD_plus_"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439; layout_2443; sa3; sa222; sa256; sa108; sa45"
      uniprot "NA"
    ]
    graphics [
      x 795.2693027083077
      y 451.5134513355599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 4
      diagram "R-HSA-9678108; R-HSA-9694516; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277; urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "NAM"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_716; layout_2444; sa221; sa253"
      uniprot "NA"
    ]
    graphics [
      x 1055.4473923740609
      y 498.19415751724034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 17
      diagram "R-HSA-9694516; WP4846; WP5039; C19DMap:Virus replication cycle; C19DMap:Orf3a protein interactions; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958; urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139; urn:miriam:uniprot:P0DTC3; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049; urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "UNIPROT:P0DTC3"
      name "O_minus_glycosyl_space_3a_space_tetramer; O_minus_glycosyl_space_3a; 3a; 3a:membranous_space_structure; GalNAc_minus_O_minus_3a; ORF3a; Orf3a"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "layout_2458; layout_2461; layout_2399; layout_2403; layout_2332; layout_2392; layout_2459; layout_2455; layout_2395; cb0cc; ac4ba; ed8aa; sa1873; sa2247; sa1; sa169; sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1154.3989322200175
      y 1823.0666307871822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P0DTC3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 6
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA; urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "e_minus_"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa645; sa650; sa651; sa242; sa195; sa320"
      uniprot "NA"
    ]
    graphics [
      x 1107.0980913534127
      y 459.62877927066745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e_minus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "Fe2_plus_"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa367; sa365; sa375; sa163; sa55; sa162; sa28; sa135; sa238; sa240; sa357"
      uniprot "NA"
    ]
    graphics [
      x 1022.6594262109661
      y 1267.5165112543777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fe2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 11
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474; urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "NADPH"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382; sa395; sa30; sa295; sa153; sa25; sa33; sa24; sa100; sa356; sa379"
      uniprot "NA"
    ]
    graphics [
      x 903.6989357769879
      y 932.6925053904806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADPH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Electron Transport Chain disruption; C19DMap:Nsp14 and metabolism; C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908; urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153; urn:miriam:obo.chebi:CHEBI%3A57945"
      hgnc "NA"
      map_id "NADH"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2; sa322; sa207; sa79; sa1264; sa286; sa44"
      uniprot "NA"
    ]
    graphics [
      x 1150.22806125036
      y 816.321184356312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:E protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29101"
      hgnc "NA"
      map_id "Na_plus_"
      name "Na_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa68; sa67; sa336; sa337"
      uniprot "NA"
    ]
    graphics [
      x 1538.7930394340437
      y 1980.2538913460908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Na_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Orf10 Cul2 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083; urn:miriam:obo.chebi:CHEBI%3A456215; urn:miriam:obo.chebi:CHEBI%3A16027"
      hgnc "NA"
      map_id "AMP"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa301; sa97; sa224; sa45; sa216; sa288"
      uniprot "NA"
    ]
    graphics [
      x 988.6207249560003
      y 645.0289608837898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813; urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:P21589;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "UNIPROT:P21589"
      name "NT5E; NT5E:Zn2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa343; sa100; sa316; sa275; csa35"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1132.2378647674814
      y 708.1902078725423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P21589"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298; urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298"
      hgnc "HGNC_SYMBOL:QPRT"
      map_id "UNIPROT:Q15274"
      name "QPRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132; sa207"
      uniprot "UNIPROT:Q15274"
    ]
    graphics [
      x 1349.3879154972146
      y 562.9491082177552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q15274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 7
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Pyrimidine deprivation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526;urn:miriam:pubchem.compound:280; urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "CO2"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136; sa186; sa26; sa70; sa122; sa40; sa201"
      uniprot "NA"
    ]
    graphics [
      x 1385.573975716925
      y 721.2512525303766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CO2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:pubmed:12359228;urn:miriam:ensembl:ENSG00000157064; urn:miriam:uniprot:Q9BZQ4;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:ensembl:ENSG00000157064"
      hgnc "HGNC_SYMBOL:NMNAT2"
      map_id "UNIPROT:Q9BZQ4"
      name "NMNAT2; NMNAT2:Mg2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa91; csa16"
      uniprot "UNIPROT:Q9BZQ4"
    ]
    graphics [
      x 1044.041126883392
      y 77.36315959734895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BZQ4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565;urn:miriam:pubmed:17402747; urn:miriam:uniprot:Q96T66;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "HGNC_SYMBOL:NMNAT3"
      map_id "UNIPROT:Q96T66"
      name "NMNAT3; NMNAT3:Mg2_plus_"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa93; csa3"
      uniprot "UNIPROT:Q96T66"
    ]
    graphics [
      x 1207.1864543666109
      y 499.89205664685886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q96T66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835; urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835"
      hgnc "HGNC_SYMBOL:NAMPT"
      map_id "UNIPROT:P43490"
      name "NAMPT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa108; sa206"
      uniprot "UNIPROT:P43490"
    ]
    graphics [
      x 973.8485245845891
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P43490"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:hgnc:29832;urn:miriam:uniprot:Q6IA69;urn:miriam:uniprot:Q6IA69;urn:miriam:pubmed:12547821;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161; urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:uniprot:Q6IA69;urn:miriam:hgnc:29832;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161"
      hgnc "HGNC_SYMBOL:NADSYN1"
      map_id "UNIPROT:Q6IA69"
      name "NADSYN1; NADSYN1_space_hexamer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84; sa208"
      uniprot "UNIPROT:Q6IA69"
    ]
    graphics [
      x 789.3581835783883
      y 631.4200417864249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6IA69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:pubmed:16690024;urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907;urn:miriam:uniprot:P28907; urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907"
      hgnc "HGNC_SYMBOL:CD38"
      map_id "UNIPROT:P28907"
      name "CD38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa121; sa144; sa265"
      uniprot "UNIPROT:P28907"
    ]
    graphics [
      x 978.0609699515694
      y 362.2142266184244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P28907"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Nsp14 and metabolism; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130; urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:O95544;urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130;urn:miriam:obo.chebi:CHEBI%3A29105; urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK"
      hgnc "HGNC_SYMBOL:NADK"
      map_id "UNIPROT:O95544"
      name "NADK; NADK:Zn2_plus__space_tetramer; APOA1BP"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa75; csa17; sa329"
      uniprot "UNIPROT:O95544"
    ]
    graphics [
      x 461.6804122546796
      y 404.1555576901983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O95544"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "PRPP"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29; sa210; sa187; sa202"
      uniprot "NA"
    ]
    graphics [
      x 1135.522414743279
      y 385.19194827050603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PRPP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050; urn:miriam:obo.chebi:CHEBI%3A58359"
      hgnc "NA"
      map_id "L_minus_Gln"
      name "L_minus_Gln"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1; sa197"
      uniprot "NA"
    ]
    graphics [
      x 713.3286561034599
      y 535.6312203845664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Gln"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Pyrimidine deprivation; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015; urn:miriam:obo.chebi:CHEBI%3A29985"
      hgnc "NA"
      map_id "L_minus_Glu"
      name "L_minus_Glu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10; sa212"
      uniprot "NA"
    ]
    graphics [
      x 681.7333850828865
      y 598.3525322778472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Glu"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Nsp9 protein interactions; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1; urn:miriam:pubmed:20724158;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1;urn:miriam:pubchem.compound:5090; urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1; urn:miriam:uniprot:P35354;urn:miriam:obo.chebi:CHEBI%3A41423;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "UNIPROT:P35354"
      name "PTGS2; PTGScomp; PTCS2:celecoxib"
      node_subtype "PROTEIN; COMPLEX"
      node_type "species"
      org_id "sa1153; csa54; sa315; csa34"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 1160.938822760022
      y 470.21681757630853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "CO"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158; sa31; sa362"
      uniprot "NA"
    ]
    graphics [
      x 1335.3672766661548
      y 1275.055638739513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 5
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "Heme"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152; sa23; sa21; sa197; sa355"
      uniprot "NA"
    ]
    graphics [
      x 1160.075935936758
      y 1303.927110365089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Heme"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 13
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "O2"
      name "O2"
      node_subtype "SIMPLE_MOLECULE; ION"
      node_type "species"
      org_id "sa154; sa24; sa121; sa124; sa156; sa136; sa213; sa22; sa250; sa4; sa358; sa39; sa383"
      uniprot "NA"
    ]
    graphics [
      x 837.957227396391
      y 1219.4091305077882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "O2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 9
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18; urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601; urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "UNIPROT:P09601"
      name "HMOX1"
      node_subtype "PROTEIN; RNA; GENE"
      node_type "species"
      org_id "sa161; sa168; sa50; sa29; sa10; sa341; sa351; sa352; sa340"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1091.7789308860906
      y 1520.0348039640676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P09601"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "Biliverdin"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156; sa22; sa361"
      uniprot "NA"
    ]
    graphics [
      x 1161.2388735683903
      y 1228.271664363997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Biliverdin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 8
      diagram "C19DMap:NLRP3 inflammasome activation; C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "NADP_plus_"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155; sa27; sa34; sa241; sa23; sa104; sa354; sa380"
      uniprot "NA"
    ]
    graphics [
      x 786.4966883505009
      y 847.5897478054073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NADP_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "H2O2"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa123; sa125; sa254"
      uniprot "NA"
    ]
    graphics [
      x 544.2836491994469
      y 683.7836627093988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "H2O2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:HMOX1 pathway; C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:7795;urn:miriam:refseq:NM_001077494;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:ensembl:ENSG00000077150;urn:miriam:uniprot:Q00653;urn:miriam:uniprot:Q00653;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:RELA"
      map_id "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101; csa36; csa38"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1607.2423039687092
      y 1335.5153727740965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927"
      hgnc "NA"
      map_id "NR"
      name "NR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 720.718321754836
      y 160.71559910698193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NR"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 788.5103338893751
      y 277.56500366073146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:17871;urn:miriam:ec-code:2.7.1.173;urn:miriam:refseq:NM_014446;urn:miriam:ensembl:ENSG00000077009;urn:miriam:ec-code:2.7.1.22;urn:miriam:ncbigene:27231;urn:miriam:ncbigene:27231;urn:miriam:uniprot:Q9NPI5;urn:miriam:refseq:NM_170678;urn:miriam:hgnc.symbol:NMRK2;urn:miriam:hgnc.symbol:NMRK2"
      hgnc "HGNC_SYMBOL:NMRK2"
      map_id "UNIPROT:Q9NPI5"
      name "NMRK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa237; sa215"
      uniprot "UNIPROT:Q9NPI5"
    ]
    graphics [
      x 645.8250986871576
      y 223.33537168411397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NPI5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A14649"
      hgnc "NA"
      map_id "NMN"
      name "NMN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa223; sa260"
      uniprot "NA"
    ]
    graphics [
      x 896.8765112692839
      y 391.8891963360222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NMN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1296.4465115638272
      y 714.1389830685481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "AdoMet"
      name "AdoMet"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa232"
      uniprot "NA"
    ]
    graphics [
      x 1441.472978085423
      y 753.107987802751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AdoMet"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000166741;urn:miriam:ec-code:2.1.1.1;urn:miriam:hgnc:7861;urn:miriam:refseq:NM_006169;urn:miriam:ncbigene:4837;urn:miriam:ncbigene:4837;urn:miriam:hgnc.symbol:NNMT;urn:miriam:hgnc.symbol:NNMT;urn:miriam:uniprot:P40261"
      hgnc "HGNC_SYMBOL:NNMT"
      map_id "UNIPROT:P40261"
      name "NNMT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa214"
      uniprot "UNIPROT:P40261"
    ]
    graphics [
      x 1378.1987338409604
      y 853.7559077238143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P40261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16797"
      hgnc "NA"
      map_id "MNA"
      name "MNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 1271.9468603715343
      y 578.9015047197546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680"
      hgnc "NA"
      map_id "AdoHcy"
      name "AdoHcy"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa231"
      uniprot "NA"
    ]
    graphics [
      x 1351.2380368264573
      y 812.0780213781775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AdoHcy"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 601.8560000944155
      y 519.870985506908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 1082.0892864124844
      y 640.2085431344274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927"
      hgnc "NA"
      map_id "NRNAM"
      name "NRNAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa263"
      uniprot "NA"
    ]
    graphics [
      x 1277.0174294845065
      y 660.4608155172139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NRNAM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 908.2151949554122
      y 199.32575833859983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019"
      hgnc "NA"
      map_id "PPi(3_minus_)"
      name "PPi(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa228; sa292; sa203"
      uniprot "NA"
    ]
    graphics [
      x 1083.5479222797637
      y 144.2901423385607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi(3_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 966.5995697868202
      y 433.9701316921686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:refseq:NM_004334;urn:miriam:ensembl:ENSG00000109743;urn:miriam:ec-code:3.2.2.6;urn:miriam:ncbigene:683;urn:miriam:ncbigene:683;urn:miriam:hgnc:1118;urn:miriam:uniprot:Q10588;urn:miriam:hgnc.symbol:BST1;urn:miriam:hgnc.symbol:BST1"
      hgnc "HGNC_SYMBOL:BST1"
      map_id "UNIPROT:Q10588"
      name "BST1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa264"
      uniprot "UNIPROT:Q10588"
    ]
    graphics [
      x 1056.194359917329
      y 299.9442791107467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q10588"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57967"
      hgnc "NA"
      map_id "ADP_minus_ribose"
      name "ADP_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa258"
      uniprot "NA"
    ]
    graphics [
      x 935.893445783535
      y 303.7259185456978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ADP_minus_ribose"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58527"
      hgnc "NA"
      map_id "NAR"
      name "NAR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa224"
      uniprot "NA"
    ]
    graphics [
      x 668.0631879594616
      y 112.31013080906109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAR"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 733.5113155640151
      y 252.0693154969283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000106733;urn:miriam:ec-code:2.7.1.173;urn:miriam:uniprot:Q9NWW6;urn:miriam:ncbigene:54981;urn:miriam:refseq:NM_017881;urn:miriam:ncbigene:54981;urn:miriam:ec-code:2.7.1.22;urn:miriam:hgnc:26057;urn:miriam:hgnc.symbol:NMRK1;urn:miriam:hgnc.symbol:NMRK1"
      hgnc "HGNC_SYMBOL:NMRK1"
      map_id "UNIPROT:Q9NWW6"
      name "NMRK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236"
      uniprot "UNIPROT:Q9NWW6"
    ]
    graphics [
      x 625.0324670128202
      y 379.37062919102755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9NWW6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15763"
      hgnc "NA"
      map_id "NAMN"
      name "NAMN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa199; sa291; sa95"
      uniprot "NA"
    ]
    graphics [
      x 1005.500700754468
      y 258.6376376692956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAMN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16946"
      hgnc "NA"
      map_id "L_minus_Kynurenine"
      name "L_minus_Kynurenine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa17"
      uniprot "NA"
    ]
    graphics [
      x 483.255742335565
      y 1245.4139745893772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Kynurenine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 744.3150066123246
      y 985.9342869447929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:KMO;urn:miriam:hgnc.symbol:KMO;urn:miriam:uniprot:O15229;urn:miriam:ncbigene:8564;urn:miriam:ncbigene:8564;urn:miriam:ec-code:1.14.13.9;urn:miriam:hgnc:6381;urn:miriam:ensembl:ENSG00000117009;urn:miriam:refseq:NM_003679"
      hgnc "HGNC_SYMBOL:KMO"
      map_id "UNIPROT:O15229"
      name "KMO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa377"
      uniprot "UNIPROT:O15229"
    ]
    graphics [
      x 701.0252629673928
      y 1108.4332367541888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:O15229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17380"
      hgnc "NA"
      map_id "3HKYN"
      name "3HKYN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 555.8206808769833
      y 1194.3542440200538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "3HKYN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15793"
      hgnc "NA"
      map_id "3HAA"
      name "3HAA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 636.3439372943067
      y 1249.2426972004625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "3HAA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:12186837"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_28"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 412.1955053031043
      y 1197.9268358413883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070231"
      hgnc "NA"
      map_id "T_minus_cell_space_apoptosis"
      name "T_minus_cell_space_apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa375"
      uniprot "NA"
    ]
    graphics [
      x 268.218419709895
      y 1056.288021767777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "T_minus_cell_space_apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90174;urn:miriam:obo.chebi:CHEBI%3A90171"
      hgnc "NA"
      map_id "dh_minus_beta_minus_NAD"
      name "dh_minus_beta_minus_NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa249"
      uniprot "NA"
    ]
    graphics [
      x 526.3461625820353
      y 747.9733984863046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "dh_minus_beta_minus_NAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 682.5360292524139
      y 744.195603594666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q5VYX0;urn:miriam:obo.chebi:CHEBI%3A16238;urn:miriam:ncbigene:55328;urn:miriam:ncbigene:55328;urn:miriam:hgnc.symbol:RNLS;urn:miriam:hgnc.symbol:RNLS;urn:miriam:ec-code:1.6.3.5;urn:miriam:hgnc:25641;urn:miriam:refseq:NM_018363;urn:miriam:uniprot:Q5VYX0;urn:miriam:uniprot:Q5VYX0;urn:miriam:ensembl:ENSG00000184719;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "HGNC_SYMBOL:RNLS"
      map_id "UNIPROT:Q5VYX0"
      name "RNLS:FAD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q5VYX0"
    ]
    graphics [
      x 551.2866772207562
      y 812.5233759849937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q5VYX0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27647"
      hgnc "NA"
      map_id "PGG2"
      name "PGG2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa321"
      uniprot "NA"
    ]
    graphics [
      x 1191.440753318941
      y 554.3817857403166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PGG2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1053.57563497755
      y 570.2797350691256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15554"
      hgnc "NA"
      map_id "PGH2"
      name "PGH2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa317"
      uniprot "NA"
    ]
    graphics [
      x 761.0772980528551
      y 507.6888468839111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PGH2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:17476692"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_26"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 419.2506348925992
      y 1037.8107843697308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "CA"
      name "CA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 300.6249597457348
      y 867.8932322824536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 657.6483457170738
      y 515.5280541889006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:133686;urn:miriam:ncbigene:133686;urn:miriam:refseq:NM_153013;urn:miriam:ensembl:ENSG00000152620;urn:miriam:hgnc:26404;urn:miriam:hgnc.symbol:NADK2;urn:miriam:hgnc.symbol:NADK2;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:Q4G0N4"
      hgnc "HGNC_SYMBOL:NADK2"
      map_id "UNIPROT:Q4G0N4"
      name "NADK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa324"
      uniprot "UNIPROT:Q4G0N4"
    ]
    graphics [
      x 638.075189763077
      y 661.9959070351546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q4G0N4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:23123095"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_35"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 796.0941571930554
      y 1171.5819968537453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0045066"
      hgnc "NA"
      map_id "Regulatory_space_T_minus_cell_space_generation"
      name "Regulatory_space_T_minus_cell_space_generation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 969.6109048860268
      y 1090.9120490880446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Regulatory_space_T_minus_cell_space_generation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 1139.1743348180619
      y 277.93684843443555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:Q9HAN9;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565"
      hgnc "HGNC_SYMBOL:NMNAT3"
      map_id "UNIPROT:Q9HAN9;UNIPROT:Q96T66"
      name "NMNAT1:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:Q9HAN9;UNIPROT:Q96T66"
    ]
    graphics [
      x 1243.2660106690978
      y 447.76699939730156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9HAN9;UNIPROT:Q96T66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 3
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18304"
      hgnc "NA"
      map_id "NAAD"
      name "NAAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa290; sa200; sa97"
      uniprot "NA"
    ]
    graphics [
      x 1058.570130314342
      y 355.2519698422791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAAD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:17476692"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_29"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 183.61840427367224
      y 920.093342973449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 595.0651959848486
      y 1069.643529011982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q16719;urn:miriam:hgnc:6469;urn:miriam:ensembl:ENSG00000115919;urn:miriam:ncbigene:8942;urn:miriam:refseq:NM_001032998;urn:miriam:ncbigene:8942;urn:miriam:hgnc.symbol:KYNU;urn:miriam:hgnc.symbol:KYNU;urn:miriam:ec-code:3.7.1.3"
      hgnc "HGNC_SYMBOL:KYNU"
      map_id "UNIPROT:Q16719"
      name "KYNU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:Q16719"
    ]
    graphics [
      x 474.6873022668474
      y 1071.0298124780365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16719"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57972"
      hgnc "NA"
      map_id "L_minus_Ala"
      name "L_minus_Ala"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 347.4911856101983
      y 1234.750028138484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Ala"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A32544"
      hgnc "NA"
      map_id "NCA"
      name "NCA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa271; sa189"
      uniprot "NA"
    ]
    graphics [
      x 836.356180868858
      y 563.4471503600887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NCA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 830.6658785814941
      y 740.8128002761243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:SLC22A13;urn:miriam:refseq:NM_004256;urn:miriam:hgnc.symbol:SLC22A13;urn:miriam:ensembl:ENSG00000172940;urn:miriam:uniprot:Q9Y226;urn:miriam:hgnc:8494;urn:miriam:ncbigene:9390;urn:miriam:ncbigene:9390"
      hgnc "HGNC_SYMBOL:SLC22A13"
      map_id "UNIPROT:Q9Y226"
      name "SLC22A13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa268"
      uniprot "UNIPROT:Q9Y226"
    ]
    graphics [
      x 850.9352317960681
      y 859.0469695836077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9Y226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 855.5959680179748
      y 490.46673132549535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 761.242776415945
      y 687.7545214381494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133202"
      hgnc "NA"
      map_id "(ADP_minus_D_minus_ribosyl)(n)_minus_acceptor"
      name "(ADP_minus_D_minus_ribosyl)(n)_minus_acceptor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa233"
      uniprot "NA"
    ]
    graphics [
      x 720.3976379072632
      y 796.8669512671014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(ADP_minus_D_minus_ribosyl)(n)_minus_acceptor"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q53GL7;urn:miriam:ncbigene:54956;urn:miriam:ec-code:2.4.2.-;urn:miriam:uniprot:Q8N5Y8;urn:miriam:ncbigene:84875;urn:miriam:ncbigene:83666;urn:miriam:hgnc.symbol:PARP9;urn:miriam:hgnc.symbol:PARP10;urn:miriam:hgnc.symbol:PARP16;urn:miriam:uniprot:Q8IXQ6"
      hgnc "HGNC_SYMBOL:PARP9;HGNC_SYMBOL:PARP10;HGNC_SYMBOL:PARP16"
      map_id "UNIPROT:Q53GL7;UNIPROT:Q8N5Y8;UNIPROT:Q8IXQ6"
      name "PARPs"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q8N5Y8;UNIPROT:Q8IXQ6"
    ]
    graphics [
      x 636.7345345925461
      y 804.572329422085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q53GL7;UNIPROT:Q8N5Y8;UNIPROT:Q8IXQ6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133203"
      hgnc "NA"
      map_id "(ADP_minus_D_minus_ribosyl)(n_plus_1)_minus_acceptor"
      name "(ADP_minus_D_minus_ribosyl)(n_plus_1)_minus_acceptor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa234"
      uniprot "NA"
    ]
    graphics [
      x 693.2245727875941
      y 852.9036440772893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "(ADP_minus_D_minus_ribosyl)(n_plus_1)_minus_acceptor"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 206.5800610733137
      y 1226.9444740640367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15361"
      hgnc "NA"
      map_id "PYR"
      name "PYR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 133.39777454037062
      y 1329.3581735544244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PYR"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q16773;urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:uniprot:Q16773;urn:miriam:uniprot:Q16773;urn:miriam:hgnc:1564;urn:miriam:refseq:NM_001122671;urn:miriam:ec-code:4.4.1.13;urn:miriam:ncbigene:883;urn:miriam:ncbigene:883;urn:miriam:ensembl:ENSG00000171097;urn:miriam:ec-code:2.6.1.7;urn:miriam:hgnc.symbol:KYAT1;urn:miriam:hgnc.symbol:KYAT1;urn:miriam:ec-code:2.6.1.64"
      hgnc "HGNC_SYMBOL:KYAT1"
      map_id "UNIPROT:Q16773"
      name "PXLP_minus_KYAT1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16773"
    ]
    graphics [
      x 155.05109465326666
      y 1099.876812508675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16773"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43580"
      hgnc "NA"
      map_id "I3PROPA"
      name "I3PROPA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1173.6859389575002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "I3PROPA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58095"
      hgnc "NA"
      map_id "L_minus_Phe"
      name "L_minus_Phe"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 119.85314907001361
      y 1057.6049032253693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Phe"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55528"
      hgnc "NA"
      map_id "I3LACT"
      name "I3LACT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 197.27643241892156
      y 1137.7605846196325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "I3LACT"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17442"
      hgnc "NA"
      map_id "AP_minus_DOBu"
      name "AP_minus_DOBu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa12"
      uniprot "NA"
    ]
    graphics [
      x 380.96635906087243
      y 1104.1151879874335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AP_minus_DOBu"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57912"
      hgnc "NA"
      map_id "L_minus_Tryptophan"
      name "L_minus_Tryptophan"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2; sa1"
      uniprot "NA"
    ]
    graphics [
      x 759.9343581435763
      y 1677.396797330386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Tryptophan"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 862.1043308276605
      y 1698.4222492398135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:SLC36A4;urn:miriam:hgnc.symbol:SLC36A4;urn:miriam:refseq:NM_001286139;urn:miriam:uniprot:Q6YBV0;urn:miriam:ensembl:ENSG00000180773;urn:miriam:ncbigene:120103;urn:miriam:ncbigene:120103;urn:miriam:hgnc:19660"
      hgnc "HGNC_SYMBOL:SLC36A4"
      map_id "UNIPROT:Q6YBV0"
      name "SLC36A4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:Q6YBV0"
    ]
    graphics [
      x 928.2596716647804
      y 1593.6618348008433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6YBV0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:30338242;PUBMED:32292563;PUBMED:29531094;PUBMED:28523098"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 853.101937664917
      y 1493.569078958044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 4
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P14902;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:hgnc:6059;urn:miriam:hgnc.symbol:IDO1;urn:miriam:uniprot:P14902;urn:miriam:uniprot:P14902;urn:miriam:hgnc.symbol:IDO1;urn:miriam:ensembl:ENSG00000131203;urn:miriam:refseq:NM_002164;urn:miriam:ncbigene:3620;urn:miriam:ncbigene:3620;urn:miriam:ec-code:1.13.11.52; urn:miriam:uniprot:P14902;urn:miriam:hgnc.symbol:IDO1;urn:miriam:ncbigene:3620;urn:miriam:ec-code:1.13.11.52; urn:miriam:hgnc:6059;urn:miriam:hgnc.symbol:IDO1;urn:miriam:uniprot:P14902;urn:miriam:ensembl:ENSG00000131203;urn:miriam:refseq:NM_002164;urn:miriam:ncbigene:3620"
      hgnc "HGNC_SYMBOL:IDO1"
      map_id "UNIPROT:P14902"
      name "IDO1; apo_minus_IDO1"
      node_subtype "COMPLEX; PROTEIN; GENE; RNA"
      node_type "species"
      org_id "csa39; sa5; sa58; sa65"
      uniprot "UNIPROT:P14902"
    ]
    graphics [
      x 1067.3833370743803
      y 1461.5312343217554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P14902"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "Epacadostat"
      name "Epacadostat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 783.2891926049916
      y 1401.5811382476404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Epacadostat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18377"
      hgnc "NA"
      map_id "NFK"
      name "NFK"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa11"
      uniprot "NA"
    ]
    graphics [
      x 702.6992760614855
      y 1441.4240404090556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NFK"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:23123095"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 523.5845323447314
      y 1118.5281154678853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0035744"
      hgnc "NA"
      map_id "Th1_space_cell_space_cytokine_space_production"
      name "Th1_space_cell_space_cytokine_space_production"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa374"
      uniprot "NA"
    ]
    graphics [
      x 492.5025376509083
      y 972.823598465665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Th1_space_cell_space_cytokine_space_production"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A64084"
      hgnc "NA"
      map_id "S_minus_NADPHX"
      name "S_minus_NADPHX"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa101"
      uniprot "NA"
    ]
    graphics [
      x 718.1705325380183
      y 587.2291606845822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "S_minus_NADPHX"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 843.1530421587601
      y 620.6654335727033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAXD;urn:miriam:uniprot:Q8IW45;urn:miriam:ec-code:4.2.1.93;urn:miriam:ncbigene:55739"
      hgnc "HGNC_SYMBOL:NAXD"
      map_id "UNIPROT:Q8IW45"
      name "CARKD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa106"
      uniprot "UNIPROT:Q8IW45"
    ]
    graphics [
      x 920.5666319400074
      y 750.3350517501701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8IW45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 1172.7826710267543
      y 1622.2739866033505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29044"
      hgnc "NA"
      map_id "ACS"
      name "ACS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa38"
      uniprot "NA"
    ]
    graphics [
      x 1239.7305161989555
      y 1060.3742609781257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ACS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 1429.0771895340447
      y 921.9290878221277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:refseq:NM_001307983;urn:miriam:hgnc.symbol:ACMSD;urn:miriam:hgnc.symbol:ACMSD;urn:miriam:ncbigene:130013;urn:miriam:ncbigene:130013;urn:miriam:hgnc:19288;urn:miriam:ensembl:ENSG00000153086;urn:miriam:uniprot:Q8TDX5;urn:miriam:ec-code:4.1.1.45"
      hgnc "HGNC_SYMBOL:ACMSD"
      map_id "UNIPROT:Q8TDX5"
      name "ACMSD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa37"
      uniprot "UNIPROT:Q8TDX5"
    ]
    graphics [
      x 1565.0552704738323
      y 928.9010459720371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8TDX5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15745"
      hgnc "NA"
      map_id "2AMA"
      name "2AMA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 1281.1373226317628
      y 806.8483438513128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2AMA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 641.7800659175182
      y 978.4656119518767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18344"
      hgnc "NA"
      map_id "KYNA"
      name "KYNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 740.3847091758319
      y 1075.0713764934203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "KYNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 1043.0377934736794
      y 809.8677342137496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:refseq:NM_031438;urn:miriam:ncbigene:83594;urn:miriam:ncbigene:83594;urn:miriam:uniprot:Q9BQG2;urn:miriam:ec-code:3.6.1.-;urn:miriam:ensembl:ENSG00000112874;urn:miriam:ec-code:3.6.1.22;urn:miriam:hgnc:18826;urn:miriam:hgnc.symbol:NUDT12;urn:miriam:hgnc.symbol:NUDT12"
      hgnc "HGNC_SYMBOL:NUDT12"
      map_id "UNIPROT:Q9BQG2"
      name "NUDT12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa289"
      uniprot "UNIPROT:Q9BQG2"
    ]
    graphics [
      x 1124.2360808325252
      y 922.7997212859514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q9BQG2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A74452"
      hgnc "NA"
      map_id "NMNH"
      name "NMNH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa285"
      uniprot "NA"
    ]
    graphics [
      x 1014.1154766457817
      y 953.5778833630596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NMNH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_25"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 817.1161209091954
      y 1812.29683796784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A1902483"
      hgnc "NA"
      map_id "Effector_space_T_minus_cell_space_apoptosis"
      name "Effector_space_T_minus_cell_space_apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa371"
      uniprot "NA"
    ]
    graphics [
      x 933.7102075780747
      y 1749.9937572696545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Effector_space_T_minus_cell_space_apoptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32292563;PUBMED:29531094;PUBMED:28523098"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_24"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 1236.9658120242077
      y 1417.8374419694587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1057.5766720656914
      y 705.969846623278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16886"
      hgnc "NA"
      map_id "2AM"
      name "2AM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1132.5847928672615
      y 603.2117853612838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2AM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A64085"
      hgnc "NA"
      map_id "R_minus_NADPHX"
      name "R_minus_NADPHX"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa102"
      uniprot "NA"
    ]
    graphics [
      x 694.7911623426455
      y 473.01582963657694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R_minus_NADPHX"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 553.6115584542988
      y 477.55295706490745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:4843; urn:miriam:ncbigene:4348"
      hgnc "NA"
      map_id "iNOS"
      name "iNOS"
      node_subtype "GENE; RNA"
      node_type "species"
      org_id "sa57; sa59"
      uniprot "NA"
    ]
    graphics [
      x 1498.3550417157908
      y 1220.4262186157284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "iNOS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:9126284"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1450.9724763680001
      y 1378.8004987143443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q14609"
      hgnc "NA"
      map_id "UNIPROT:Q14609"
      name "IFN_minus_G"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa62"
      uniprot "UNIPROT:Q14609"
    ]
    graphics [
      x 1273.2783278969425
      y 1491.0968645384569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q14609"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:29531094"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 716.7086213723001
      y 1227.1874925581028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:20910;urn:miriam:refseq:NM_001145526;urn:miriam:ec-code:3.5.1.9;urn:miriam:uniprot:Q63HM1;urn:miriam:hgnc.symbol:AFMID;urn:miriam:ncbigene:125061;urn:miriam:ensembl:ENSG00000183077;urn:miriam:hgnc.symbol:AFMID;urn:miriam:ncbigene:125061"
      hgnc "HGNC_SYMBOL:AFMID"
      map_id "UNIPROT:Q63HM1"
      name "AFMID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q63HM1"
    ]
    graphics [
      x 689.5487915083598
      y 1351.4024537158732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q63HM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30751"
      hgnc "NA"
      map_id "HCOOH"
      name "HCOOH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 619.4501762285936
      y 1333.143324013881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HCOOH"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 745.2751759125135
      y 1526.9114565453474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:IDO2;urn:miriam:hgnc.symbol:IDO2;urn:miriam:ensembl:ENSG00000188676;urn:miriam:ec-code:1.13.11.-;urn:miriam:uniprot:Q6ZQW0;urn:miriam:hgnc:27269;urn:miriam:ncbigene:169355;urn:miriam:ncbigene:169355;urn:miriam:refseq:NM_194294"
      hgnc "HGNC_SYMBOL:IDO2"
      map_id "UNIPROT:Q6ZQW0"
      name "IDO2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q6ZQW0"
    ]
    graphics [
      x 630.2457095320982
      y 1601.2842634844187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6ZQW0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 1071.4597360439934
      y 1151.5447158602074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1192.0990620194352
      y 919.0255764850006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16675"
      hgnc "NA"
      map_id "QUIN"
      name "QUIN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1225.342557709705
      y 773.4675179074902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "QUIN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "monocarboxylates_space_transported_space_by_space_SLC5A8"
      name "monocarboxylates_space_transported_space_by_space_SLC5A8"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa338; sa334"
      uniprot "NA"
    ]
    graphics [
      x 1448.255258282477
      y 1972.6888654096415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "monocarboxylates_space_transported_space_by_space_SLC5A8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 1482.3969853466774
      y 2089.916863654078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q8N695;urn:miriam:ensembl:ENSG00000256870;urn:miriam:hgnc:19119;urn:miriam:hgnc.symbol:SLC5A8;urn:miriam:hgnc.symbol:SLC5A8;urn:miriam:refseq:NM_145913;urn:miriam:ncbigene:160728;urn:miriam:ncbigene:160728"
      hgnc "HGNC_SYMBOL:SLC5A8"
      map_id "UNIPROT:Q8N695"
      name "SLC5A8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269"
      uniprot "UNIPROT:Q8N695"
    ]
    graphics [
      x 1371.5219408854916
      y 2023.2958820652098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q8N695"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 803.2221717837594
      y 203.08501915041984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 940.7009173946752
      y 1227.3054450150848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P46952;urn:miriam:ncbigene:23498;urn:miriam:ncbigene:23498;urn:miriam:hgnc:4796;urn:miriam:ensembl:ENSG00000162882;urn:miriam:refseq:NM_012205;urn:miriam:hgnc.symbol:HAAO;urn:miriam:hgnc.symbol:HAAO;urn:miriam:ec-code:1.13.11.6"
      hgnc "HGNC_SYMBOL:HAAO"
      map_id "UNIPROT:P46952"
      name "HAAO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:P46952"
    ]
    graphics [
      x 914.754567860975
      y 1346.6988731333258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P46952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 923.2209817202414
      y 485.514515749546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1036.0704824489599
      y 201.89720628434088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 1122.2952076099598
      y 1701.1145939296287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:12706494;PUBMED:11287117"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 975.0171861704312
      y 1432.6673453953451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16480"
      hgnc "NA"
      map_id "NO"
      name "NO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa64"
      uniprot "NA"
    ]
    graphics [
      x 1061.0747292671394
      y 1322.9408382947404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NO"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 2
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:348;urn:miriam:hgnc.symbol:AHR;urn:miriam:hgnc.symbol:AHR;urn:miriam:ensembl:ENSG00000106546;urn:miriam:uniprot:P35869;urn:miriam:refseq:NM_001621;urn:miriam:ncbigene:196;urn:miriam:ncbigene:196;urn:miriam:obo.chebi:CHEBI%3A16946; urn:miriam:hgnc:348;urn:miriam:hgnc.symbol:AHR;urn:miriam:hgnc.symbol:AHR;urn:miriam:ensembl:ENSG00000106546;urn:miriam:uniprot:P35869;urn:miriam:refseq:NM_001621;urn:miriam:ncbigene:196;urn:miriam:ncbigene:196"
      hgnc "HGNC_SYMBOL:AHR"
      map_id "UNIPROT:P35869"
      name "AHR_slash_L_minus_KYN; AHR"
      node_subtype "COMPLEX; PROTEIN"
      node_type "species"
      org_id "csa2; sa26"
      uniprot "UNIPROT:P35869"
    ]
    graphics [
      x 871.0660903322891
      y 1021.9498485769982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35869"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:28673995;PUBMED:23123095;PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_36"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1058.404358233635
      y 968.0158301099376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1177.9501795807082
      y 340.428330925557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019"
      hgnc "NA"
      map_id "PPi_space_(3_minus_)"
      name "PPi_space_(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 1319.8623976853005
      y 363.70874012306103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPi_space_(3_minus_)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 987.9186694152379
      y 484.85030524503554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:6.3.4.21;urn:miriam:uniprot:Q6XQN6;urn:miriam:ncbigene:93100;urn:miriam:hgnc.symbol:NAPRT"
      hgnc "HGNC_SYMBOL:NAPRT"
      map_id "UNIPROT:Q6XQN6"
      name "NAPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa188"
      uniprot "UNIPROT:Q6XQN6"
    ]
    graphics [
      x 892.0507005567213
      y 647.6565326390203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6XQN6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "Mg2_plus_"
      name "Mg2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa191"
      uniprot "NA"
    ]
    graphics [
      x 938.9107290249524
      y 677.8442419635812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Mg2_plus_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1379.2967502387528
      y 1129.4534007809664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NOS2;urn:miriam:uniprot:P35228;urn:miriam:ncbigene:4843;urn:miriam:ec-code:1.14.13.39"
      hgnc "HGNC_SYMBOL:NOS2"
      map_id "UNIPROT:P35228"
      name "iNOS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa60"
      uniprot "UNIPROT:P35228"
    ]
    graphics [
      x 1182.0239913332955
      y 1102.7320644232054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P35228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1168.7800112675518
      y 146.62881244149025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16467"
      hgnc "NA"
      map_id "L_minus_Arginine"
      name "L_minus_Arginine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa378"
      uniprot "NA"
    ]
    graphics [
      x 1006.1044356318924
      y 1142.4717631284589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "L_minus_Arginine"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:11463332"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 955.9460327095018
      y 1023.8785006757969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16349"
      hgnc "NA"
      map_id "Citrulline"
      name "Citrulline"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382"
      uniprot "NA"
    ]
    graphics [
      x 897.4134638444086
      y 1140.9935349868156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Citrulline"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 1497.53474452014
      y 1315.8441721221225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1189.1080812746195
      y 614.5514541787841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:17948274"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_30"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 502.28545127179285
      y 1361.8172183439951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0097029"
      hgnc "NA"
      map_id "Dendritic_space_cell_space_maturation"
      name "Dendritic_space_cell_space_maturation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa376"
      uniprot "NA"
    ]
    graphics [
      x 427.29287635219686
      y 1471.6877722983752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Dendritic_space_cell_space_maturation"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 280.76562903171043
      y 1359.4382112779977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q6YP21;urn:miriam:ncbigene:56267;urn:miriam:ec-code:4.4.1.13;urn:miriam:hgnc.symbol:KYAT3;urn:miriam:ec-code:2.6.1.7;urn:miriam:ec-code:2.6.1.63"
      hgnc "HGNC_SYMBOL:KYAT3"
      map_id "UNIPROT:Q6YP21"
      name "CCBL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:Q6YP21"
    ]
    graphics [
      x 191.61782221441524
      y 1455.2132343526857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q6YP21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re101"
      uniprot "NA"
    ]
    graphics [
      x 466.2143811518597
      y 503.1524305409089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:5.3.99.4;urn:miriam:ec-code:4.2.1.152;urn:miriam:ensembl:ENSG00000124212;urn:miriam:hgnc:9603;urn:miriam:ncbigene:5740;urn:miriam:ncbigene:5740;urn:miriam:refseq:NM_000961;urn:miriam:hgnc.symbol:PTGIS;urn:miriam:hgnc.symbol:PTGIS;urn:miriam:uniprot:Q16647"
      hgnc "HGNC_SYMBOL:PTGIS"
      map_id "UNIPROT:Q16647"
      name "PTGIS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa339"
      uniprot "UNIPROT:Q16647"
    ]
    graphics [
      x 349.06466424973246
      y 568.2927238149327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:Q16647"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15552"
      hgnc "NA"
      map_id "PGI2"
      name "PGI2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa316"
      uniprot "NA"
    ]
    graphics [
      x 335.47945131261883
      y 475.4118550597884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PGI2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:9126284;PUBMED:23476103"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 1170.330535195384
      y 1487.5254273218925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:28673995;PUBMED:23123095;PUBMED:29531094;PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_33"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 647.9699137771801
      y 1141.4270538811122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 970.0707337355658
      y 582.3544582244185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1189.9602664495478
      y 1371.2540921463717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 726.3311838887154
      y 333.48195534406864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 177
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NR"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 5
    target 38
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NPI5"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "NMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 38
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 38
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 8
    target 41
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAM"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "AdoMet"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 43
    target 41
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P40261"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_61"
      target_id "MNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 41
    target 45
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_61"
      target_id "AdoHcy"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 7
    target 46
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 5
    target 46
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 24
    target 46
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95544"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 46
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_63"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 46
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_63"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 40
    target 47
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NMN"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 3
    target 47
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 16
    target 47
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_69"
      target_id "NRNAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 47
    target 4
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_69"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 40
    target 49
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NMN"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 5
    target 49
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 19
    target 49
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BZQ4"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 49
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_54"
      target_id "NAD_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_54"
      target_id "PPi(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 7
    target 51
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 3
    target 51
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q10588"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 51
    target 53
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "ADP_minus_ribose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 51
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 51
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAR"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 5
    target 55
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NWW6"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "NAMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 55
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 55
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Kynurenine"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 31
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 12
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 2
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O15229"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "3HKYN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 59
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 59
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 61
    target 63
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HKYN"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 58
    target 63
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Kynurenine"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_28"
      target_id "T_minus_cell_space_apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "dh_minus_beta_minus_NAD"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 31
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 2
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q5VYX0"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 66
    target 35
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_67"
      target_id "H2O2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 66
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_67"
      target_id "NAD_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PGG2"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 10
    target 69
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "e_minus_"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 2
    target 69
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 28
    target 69
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35354"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 44
    target 69
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "MNA"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_15"
      target_id "PGH2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 69
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_15"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 62
    target 71
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_26"
      target_id "CA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 7
    target 73
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 5
    target 73
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q4G0N4"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 73
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 73
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 73
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 62
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_35"
      target_id "Regulatory_space_T_minus_cell_space_generation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 57
    target 77
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAMN"
      target_id "M123_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 5
    target 77
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9HAN9;UNIPROT:Q96T66"
      target_id "M123_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 77
    target 79
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_73"
      target_id "NAAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 77
    target 50
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_73"
      target_id "PPi(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 72
    target 80
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "CA"
      target_id "M123_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 80
    target 64
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_29"
      target_id "T_minus_cell_space_apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 61
    target 81
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HKYN"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 3
    target 81
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16719"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 81
    target 62
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_71"
      target_id "3HAA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 81
    target 83
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_71"
      target_id "L_minus_Ala"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NCA"
      target_id "M123_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9Y226"
      target_id "M123_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 79
    target 87
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAAD"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 5
    target 87
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 26
    target 87
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Gln"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 3
    target 87
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 22
    target 87
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6IA69"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 87
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "NAD_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 87
    target 27
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "L_minus_Glu"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 87
    target 15
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "AMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 87
    target 1
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 7
    target 88
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "(ADP_minus_D_minus_ribosyl)(n)_minus_acceptor"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q53GL7;UNIPROT:Q8N5Y8;UNIPROT:Q8IXQ6"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 88
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_59"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 88
    target 91
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_59"
      target_id "(ADP_minus_D_minus_ribosyl)(n_plus_1)_minus_acceptor"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 58
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Kynurenine"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PYR"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 94
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16773"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 95
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "I3PROPA"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 96
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "L_minus_Phe"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 97
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "I3LACT"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 92
    target 98
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_48"
      target_id "AP_minus_DOBu"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 92
    target 83
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_48"
      target_id "L_minus_Ala"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Tryptophan"
      target_id "M123_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 101
    target 100
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6YBV0"
      target_id "M123_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 99
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Tryptophan"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 31
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P14902"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 104
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "Epacadostat"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 102
    target 105
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_42"
      target_id "NFK"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 62
    target 106
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_27"
      target_id "Th1_space_cell_space_cytokine_space_production"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "S_minus_NADPHX"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 5
    target 109
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8IW45"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 109
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "NADPH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 109
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 109
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 109
    target 4
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 32
    target 111
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P09601"
      target_id "M123_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ACS"
      target_id "M123_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8TDX5"
      target_id "M123_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 113
    target 115
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_23"
      target_id "2AMA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 113
    target 18
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_23"
      target_id "CO2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 98
    target 116
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "AP_minus_DOBu"
      target_id "M123_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_56"
      target_id "KYNA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 116
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_56"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 13
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NADH"
      target_id "M123_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 15
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "AMP"
      target_id "M123_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 3
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 119
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BQG2"
      target_id "M123_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 118
    target 120
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_72"
      target_id "NMNH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 118
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_72"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 99
    target 121
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Tryptophan"
      target_id "M123_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_25"
      target_id "Effector_space_T_minus_cell_space_apoptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 30
    target 123
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "Heme"
      target_id "M123_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 103
    target 123
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14902"
      target_id "M123_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 115
    target 124
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "2AMA"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 3
    target 124
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 7
    target 124
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "2AM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 124
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "NADH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 124
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "R_minus_NADPHX"
      target_id "M123_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 24
    target 127
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:O95544"
      target_id "M123_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 127
    target 108
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_43"
      target_id "S_minus_NADPHX"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 128
    target 129
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "iNOS"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14609"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 36
    target 129
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 29
    target 129
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "CO"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 105
    target 131
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NFK"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 3
    target 131
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 132
    target 131
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q63HM1"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 131
    target 58
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_44"
      target_id "L_minus_Kynurenine"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 131
    target 133
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_44"
      target_id "HCOOH"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 99
    target 134
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Tryptophan"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 31
    target 134
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 135
    target 134
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6ZQW0"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 134
    target 105
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_34"
      target_id "NFK"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 30
    target 136
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "Heme"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 31
    target 136
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 12
    target 136
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 32
    target 136
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P09601"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 136
    target 33
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "Biliverdin"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 136
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 136
    target 11
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "Fe2_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 136
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 136
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "CO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 112
    target 137
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ACS"
      target_id "M123_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_32"
      target_id "QUIN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 137
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_32"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "monocarboxylates_space_transported_space_by_space_SLC5A8"
      target_id "M123_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 14
    target 140
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "Na_plus_"
      target_id "M123_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 141
    target 140
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q8N695"
      target_id "M123_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 54
    target 142
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAR"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 5
    target 142
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 39
    target 142
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NPI5"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 142
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "NAMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 142
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 142
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 62
    target 143
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 31
    target 143
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P46952"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 143
    target 112
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_14"
      target_id "ACS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 7
    target 145
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 3
    target 145
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 23
    target 145
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P28907"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 145
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "NAM"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 145
    target 53
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "ADP_minus_ribose"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 145
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 8
    target 146
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAM"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 25
    target 146
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PRPP"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 21
    target 146
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P43490"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 146
    target 1
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_60"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 146
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_60"
      target_id "NAMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 32
    target 147
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P09601"
      target_id "M123_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 9
    target 147
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "MODULATION"
      source_id "UNIPROT:P0DTC3"
      target_id "M123_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 32
    target 148
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P09601"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 130
    target 148
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14609"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 149
    target 148
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "NO"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 62
    target 148
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "3HAA"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35869"
      target_id "M123_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 151
    target 76
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_36"
      target_id "Regulatory_space_T_minus_cell_space_generation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 57
    target 152
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAMN"
      target_id "M123_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 5
    target 152
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 20
    target 152
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q96T66"
      target_id "M123_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 152
    target 79
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_47"
      target_id "NAAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_47"
      target_id "PPi_space_(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 84
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NCA"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 25
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PRPP"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 2
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6XQN6"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 156
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "Mg2_plus_"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 154
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_58"
      target_id "NAMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 154
    target 1
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_58"
      target_id "PPi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 128
    target 157
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "iNOS"
      target_id "M123_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_40"
      target_id "UNIPROT:P35228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 57
    target 159
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAMN"
      target_id "M123_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 19
    target 159
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9BZQ4"
      target_id "M123_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 159
    target 79
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_50"
      target_id "NAAD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 159
    target 50
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_50"
      target_id "PPi(3_minus_)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 160
    target 161
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Arginine"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 12
    target 161
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NADPH"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 31
    target 161
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "O2"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 158
    target 161
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P35228"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "Citrulline"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 161
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "NADP_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 161
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 161
    target 149
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "NO"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 36
    target 163
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
      target_id "M123_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 29
    target 163
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "CO"
      target_id "M123_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 138
    target 164
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "QUIN"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 25
    target 164
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PRPP"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 2
    target 164
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H_plus_"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 17
    target 164
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q15274"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 164
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "NAMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 164
    target 18
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "CO2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 164
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "H2O"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 61
    target 165
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HKYN"
      target_id "M123_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 62
    target 165
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "3HAA"
      target_id "M123_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_30"
      target_id "Dendritic_space_cell_space_maturation"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 58
    target 167
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Kynurenine"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 93
    target 167
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PYR"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 168
    target 167
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q6YP21"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 167
    target 83
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_49"
      target_id "L_minus_Ala"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 70
    target 169
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "PGH2"
      target_id "M123_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 170
    target 169
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q16647"
      target_id "M123_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 169
    target 171
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_16"
      target_id "PGI2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 103
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14902"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 149
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "NO"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 130
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q14609"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 58
    target 173
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "L_minus_Kynurenine"
      target_id "M123_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 150
    target 173
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P35869"
      target_id "M123_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 7
    target 174
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NAD_plus_"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 3
    target 174
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "H2O"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 16
    target 174
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P21589"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 174
    target 40
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_68"
      target_id "NMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 174
    target 4
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_68"
      target_id "Pi"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 103
    target 175
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P14902"
      target_id "M123_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 37
    target 176
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "NR"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 5
    target 176
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "ATP"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 56
    target 176
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "UNIPROT:Q9NWW6"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 176
    target 40
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "NMN"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 176
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "ADP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 176
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "H_plus_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
