# generated with VANTED V2.8.2 at Fri Mar 04 10:07:00 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q381899"
      hgnc "NA"
      map_id "Fibrinogen"
      name "Fibrinogen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d0b3d; f3c75"
      uniprot "NA"
    ]
    graphics [
      x 267.21552243843746
      y 451.0340702611441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrinogen"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c4da9"
      uniprot "NA"
    ]
    graphics [
      x 442.82960994128916
      y 377.5097931570846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad0b7"
      uniprot "NA"
    ]
    graphics [
      x 286.02119402562886
      y 563.9073841576327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_50"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e100c"
      uniprot "NA"
    ]
    graphics [
      x 173.23453630451849
      y 375.52821782378464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:2856554"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d83d6"
      uniprot "NA"
    ]
    graphics [
      x 162.2686770917271
      y 607.7680051933141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 5
      diagram "WP4927; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734; urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147; urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "NA; HGNC_SYMBOL:F2"
      map_id "UNIPROT:P00734"
      name "Thrombin; Prothrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "af109; sa181; sa498; sa203; sa182"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 86.18928751900353
      y 683.872458331803
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00734"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q27205"
      hgnc "NA"
      map_id "d483c"
      name "d483c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d483c"
      uniprot "NA"
    ]
    graphics [
      x 141.31998608420594
      y 797.3700455507854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d483c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:21742792;PUBMED:27363989"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3d56"
      uniprot "NA"
    ]
    graphics [
      x 170.16355316355026
      y 975.3745133615369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P00488;urn:miriam:pubmed:27363989"
      hgnc "NA"
      map_id "UNIPROT:P00488"
      name "Factor_space_XIII_space_A_space_chain"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1095"
      uniprot "UNIPROT:P00488"
    ]
    graphics [
      x 62.5
      y 1012.8502310235547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00488"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q27205"
      hgnc "NA"
      map_id "a8861"
      name "a8861"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a8861"
      uniprot "NA"
    ]
    graphics [
      x 250.7375805055716
      y 1130.0830791907547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a8861"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:2143188"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f5786"
      uniprot "NA"
    ]
    graphics [
      x 375.0325689025082
      y 1206.9865215263662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:2143188"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d27d4"
      uniprot "NA"
    ]
    graphics [
      x 282.85271585803093
      y 1268.5055995377409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 6
      diagram "WP4927; C19DMap:Nsp9 protein interactions; C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747; urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "NA; HGNC_SYMBOL:PLG"
      map_id "UNIPROT:P00747"
      name "Plasmin; PLG; Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1440; sa1433; sa1431; sa211; sa212; sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 386.88564305245745
      y 1333.4501295133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P00747"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q198526;urn:miriam:pubmed:32344011;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "D_minus_Dimer"
      name "D_minus_Dimer"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fc137"
      uniprot "NA"
    ]
    graphics [
      x 196.27097704219494
      y 1336.0734544035467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D_minus_Dimer"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q2162109"
      hgnc "NA"
      map_id "Fibrin_space_degradation_space__br_products_space_(FDPs)"
      name "Fibrin_space_degradation_space__br_products_space_(FDPs)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b02e2"
      uniprot "NA"
    ]
    graphics [
      x 492.9740854807325
      y 1238.6147421193855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Fibrin_space_degradation_space__br_products_space_(FDPs)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "E"
      name "E"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c1ef1"
      uniprot "NA"
    ]
    graphics [
      x 317.5799092774771
      y 683.7536725763403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "E"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c3f7f"
      uniprot "NA"
    ]
    graphics [
      x 411.98182941596747
      y 760.2885574433244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_48"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ddc79"
      uniprot "NA"
    ]
    graphics [
      x 293.7872332842084
      y 803.8485380404493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "D"
      name "D"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f6688; b0fab"
      uniprot "NA"
    ]
    graphics [
      x 386.58932036637
      y 874.8286740331112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "D"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
      name "d9474; afcd3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d9474; afcd3"
      uniprot "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      x 654.536483084129
      y 324.2922582273563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c203e"
      uniprot "NA"
    ]
    graphics [
      x 757.8123061997371
      y 503.2641584867546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_68"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f9232"
      uniprot "NA"
    ]
    graphics [
      x 573.994181001393
      y 232.26371992683397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_44"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d91d8"
      uniprot "NA"
    ]
    graphics [
      x 812.0699061724323
      y 226.50837003047423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_39"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d69c7"
      uniprot "NA"
    ]
    graphics [
      x 520.9524587041276
      y 284.43410965877143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
      name "a870c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a870c"
      uniprot "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      x 571.1568070497087
      y 380.6762056389591
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_5"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a51f3"
      uniprot "NA"
    ]
    graphics [
      x 653.6397391298235
      y 502.9468161989689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
      name "bbecc"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bbecc"
      uniprot "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
    ]
    graphics [
      x 794.6474359304283
      y 584.4635763224883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eabf8"
      uniprot "NA"
    ]
    graphics [
      x 888.6890678179159
      y 507.91779735833325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_23"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf206"
      uniprot "NA"
    ]
    graphics [
      x 973.9959994379506
      y 593.7530730404897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "UNIPROT:P02671"
      name "FGA_space_(A_alpha_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f7650; f5a20; d7d71; a38e8"
      uniprot "UNIPROT:P02671"
    ]
    graphics [
      x 1190.0734720622854
      y 577.8584524898479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_10"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "acd27"
      uniprot "NA"
    ]
    graphics [
      x 1190.9661524813343
      y 687.3723323935427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5e482a09"
      uniprot "NA"
    ]
    graphics [
      x 1367.524585834553
      y 636.5755674738253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_24"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf421"
      uniprot "NA"
    ]
    graphics [
      x 1131.6152809436835
      y 467.15364377626247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d7615"
      uniprot "NA"
    ]
    graphics [
      x 1092.0867217543355
      y 611.3051389244639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_6"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a6e53"
      uniprot "NA"
    ]
    graphics [
      x 1024.9932996373504
      y 671.4297650358694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc381d3da"
      uniprot "NA"
    ]
    graphics [
      x 1284.926016958449
      y 670.3281749105018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:pubchem.compound:462382;urn:miriam:obo.chebi:CHEBI%3A6426"
      hgnc "NA"
      map_id "a4d07"
      name "a4d07"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a4d07"
      uniprot "NA"
    ]
    graphics [
      x 1304.4320433469013
      y 783.8050324713308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "a4d07"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2243"
      hgnc "NA"
      map_id "FGA"
      name "FGA"
      node_subtype "GENE"
      node_type "species"
      org_id "a4aba; ef261"
      uniprot "NA"
    ]
    graphics [
      x 1220.946010834255
      y 788.239999126141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FGA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "UNIPROT:P02671;UNIPROT:P02679"
      name "c579f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c579f"
      uniprot "UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      x 878.5302249221297
      y 700.2527190660649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02671;UNIPROT:P02679"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_54"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eb5be"
      uniprot "NA"
    ]
    graphics [
      x 1002.3585887509466
      y 858.4682822629663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "UNIPROT:P02679"
      name "FGG_space_(_gamma_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c5467; c1685; efb81; f949f"
      uniprot "UNIPROT:P02679"
    ]
    graphics [
      x 1175.1014049703072
      y 967.1673028792193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02679"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b068e"
      uniprot "NA"
    ]
    graphics [
      x 1351.5082754634386
      y 896.7596861558824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a9600"
      uniprot "NA"
    ]
    graphics [
      x 1106.216169328499
      y 1105.2193630150314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_46"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "db5f6"
      uniprot "NA"
    ]
    graphics [
      x 1063.1282337160033
      y 1002.6630113336037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_59"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "efe48"
      uniprot "NA"
    ]
    graphics [
      x 1088.5982147740017
      y 765.0532921931756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "faf78"
      uniprot "NA"
    ]
    graphics [
      x 1197.2464305948029
      y 1122.4929689684154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b8553"
      uniprot "NA"
    ]
    graphics [
      x 1270.9664495965678
      y 1113.051638027201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:pubchem.compound:462382"
      hgnc "NA"
      map_id "MG132"
      name "MG132"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e9c4f"
      uniprot "NA"
    ]
    graphics [
      x 1276.4262750483763
      y 1228.205547742186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "MG132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2266"
      hgnc "NA"
      map_id "FGG"
      name "FGG"
      node_subtype "GENE"
      node_type "species"
      org_id "b543a; b0c94"
      uniprot "NA"
    ]
    graphics [
      x 1157.1544655305659
      y 1222.1967959294348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FGG"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "UNIPROT:P02679;UNIPROT:P02675"
      name "bb70e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bb70e"
      uniprot "UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 1019.9414837999404
      y 530.6528735226597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02679;UNIPROT:P02675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_58"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "efc91"
      uniprot "NA"
    ]
    graphics [
      x 988.904910461073
      y 339.85164863532134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "UNIPROT:P02675"
      name "FGB_space_(B_beta_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d8eb7; bd1cb; cab41"
      uniprot "UNIPROT:P02675"
    ]
    graphics [
      x 989.6908378650387
      y 182.9034729022846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02675"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f47dc"
      uniprot "NA"
    ]
    graphics [
      x 1111.0283615746184
      y 212.75900685601357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_60"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f02cd"
      uniprot "NA"
    ]
    graphics [
      x 989.561301618887
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e202f"
      uniprot "NA"
    ]
    graphics [
      x 1174.7668547792946
      y 182.55521694235978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A52722;urn:miriam:pubchem.compound:462382"
      hgnc "NA"
      map_id "ef073"
      name "ef073"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ef073"
      uniprot "NA"
    ]
    graphics [
      x 1163.0138832130551
      y 316.55838460174067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ef073"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2244"
      hgnc "NA"
      map_id "FGB"
      name "FGB"
      node_subtype "GENE"
      node_type "species"
      org_id "dd369; fef9d"
      uniprot "NA"
    ]
    graphics [
      x 1235.4521456435564
      y 296.0777972749049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FGB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "UNIPROT:P02679;UNIPROT:P02671"
      name "dea90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dea90"
      uniprot "UNIPROT:P02679;UNIPROT:P02671"
    ]
    graphics [
      x 1423.1074799827832
      y 743.852600583735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P02679;UNIPROT:P02671"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A31206;urn:miriam:obo.chebi:CHEBI%3A6426;urn:miriam:obo.chebi:CHEBI%3A3638"
      hgnc "NA"
      map_id "c43f2"
      name "c43f2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c43f2"
      uniprot "NA"
    ]
    graphics [
      x 1438.4389693162277
      y 800.4943266133164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c43f2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbiprotein:NP_000499.1"
      hgnc "NA"
      map_id "FGA_space_(A_alpha_E)"
      name "FGA_space_(A_alpha_E)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d3c11"
      uniprot "NA"
    ]
    graphics [
      x 1021.4709034597663
      y 424.2016991047804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FGA_space_(A_alpha_E)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 61
    source 2
    target 1
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_31"
      target_id "Fibrinogen"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 1
    target 3
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "Fibrinogen"
      target_id "W14_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 1
    target 4
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "Fibrinogen"
      target_id "W14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 1
    target 5
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "Fibrinogen"
      target_id "W14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 20
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
      target_id "W14_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 3
    target 16
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_11"
      target_id "E"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 6
    target 5
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00734"
      target_id "W14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 5
    target 7
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_42"
      target_id "d483c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 7
    target 8
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "d483c"
      target_id "W14_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 9
    target 8
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00488"
      target_id "W14_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 8
    target 10
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_2"
      target_id "a8861"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 10
    target 11
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "a8861"
      target_id "W14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 10
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "a8861"
      target_id "W14_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 13
    target 11
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "W14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 11
    target 15
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_64"
      target_id "Fibrin_space_degradation_space__br_products_space_(FDPs)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 13
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "UNIPROT:P00747"
      target_id "W14_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 12
    target 14
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_36"
      target_id "D_minus_Dimer"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 17
    target 16
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_29"
      target_id "E"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 16
    target 18
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "E"
      target_id "W14_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 19
    target 17
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "D"
      target_id "W14_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 18
    target 19
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_48"
      target_id "D"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 21
    target 20
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_28"
      target_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 22
    target 20
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_68"
      target_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 23
    target 20
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_44"
      target_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 20
    target 24
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
      target_id "W14_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 39
    target 21
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671;UNIPROT:P02679"
      target_id "W14_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 25
    target 22
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
      target_id "W14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 52
    target 23
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02675"
      target_id "W14_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 24
    target 25
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_39"
      target_id "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 26
    target 25
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_5"
      target_id "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 27
    target 26
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
      target_id "W14_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 28
    target 27
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_53"
      target_id "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 29
    target 27
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_23"
      target_id "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 50
    target 28
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02675"
      target_id "W14_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 30
    target 29
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671"
      target_id "W14_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 31
    target 30
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_10"
      target_id "UNIPROT:P02671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 32
    target 30
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_73"
      target_id "UNIPROT:P02671"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 30
    target 33
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671"
      target_id "W14_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 30
    target 34
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671"
      target_id "W14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 30
    target 35
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671"
      target_id "W14_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 30
    target 36
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02671"
      target_id "W14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 38
    target 31
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "FGA"
      target_id "W14_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 58
    target 32
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02671"
      target_id "W14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 59
    target 32
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "c43f2"
      target_id "W14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 33
    target 60
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_24"
      target_id "FGA_space_(A_alpha_E)"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 35
    target 39
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_6"
      target_id "UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 37
    target 36
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "a4d07"
      target_id "W14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 36
    target 38
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_74"
      target_id "FGA"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 40
    target 39
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_54"
      target_id "UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 41
    target 40
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679"
      target_id "W14_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 42
    target 41
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_15"
      target_id "UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 43
    target 41
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_9"
      target_id "UNIPROT:P02679"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 41
    target 44
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679"
      target_id "W14_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 41
    target 45
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679"
      target_id "W14_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 41
    target 46
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679"
      target_id "W14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 41
    target 47
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679"
      target_id "W14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 58
    target 42
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02679;UNIPROT:P02671"
      target_id "W14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 59
    target 42
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "c43f2"
      target_id "W14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 49
    target 43
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "FGG"
      target_id "W14_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 45
    target 50
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_59"
      target_id "UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 48
    target 46
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "MG132"
      target_id "W14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 46
    target 49
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_70"
      target_id "FGG"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 48
    target 47
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "MG132"
      target_id "W14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 47
    target 49
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_19"
      target_id "FGG"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 51
    target 50
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_58"
      target_id "UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 52
    target 51
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02675"
      target_id "W14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 53
    target 52
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_63"
      target_id "UNIPROT:P02675"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 52
    target 54
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02675"
      target_id "W14_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 52
    target 55
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P02675"
      target_id "W14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 57
    target 53
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "FGB"
      target_id "W14_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 56
    target 55
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "ef073"
      target_id "W14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 55
    target 57
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_51"
      target_id "FGB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
