# generated with VANTED V2.8.2 at Fri Mar 04 09:59:54 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 4
      diagram "WP4799; WP4965; WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719; urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "Angiotensin_space_II"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b733a; bab13; f080e; ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 143.50137962781236
      y 880.6701483002785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Angiotensin_space_II"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      count 3
      diagram "WP5038; WP4961; C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580; urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA"
      map_id "cGAMP"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ba77f; bd7f8; sa83"
      uniprot "NA"
    ]
    graphics [
      x 211.63635315155432
      y 1209.170462416751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cGAMP"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 2
      diagram "WP5038; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000160703"
      hgnc "NA"
      map_id "NLRX1"
      name "NLRX1"
      node_subtype "GENE"
      node_type "species"
      org_id "c2498; ab61d"
      uniprot "NA"
    ]
    graphics [
      x 402.8199184352915
      y 699.7225363622008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRX1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 8
      diagram "WP5038; WP4912; WP4868; WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "IRF3"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899; c429c; f0053; e7683; f0259; e9185; db31f; e366a"
      uniprot "NA"
    ]
    graphics [
      x 790.3560971309844
      y 1013.9198839724352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IRF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4969; WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940"
      hgnc "NA"
      map_id "Vitamin_space_D3"
      name "Vitamin_space_D3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac941; fef60"
      uniprot "NA"
    ]
    graphics [
      x 314.58339534526215
      y 1399.6240132396806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Vitamin_space_D3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4876; WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "IL1B"
      name "IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "b860a; c9a82"
      uniprot "NA"
    ]
    graphics [
      x 838.9674935986893
      y 595.0555665143795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IL1B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000184584"
      hgnc "NA"
      map_id "STING1"
      name "STING1"
      node_subtype "GENE"
      node_type "species"
      org_id "c14f2"
      uniprot "NA"
    ]
    graphics [
      x 393.3150361802563
      y 944.996732713715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "STING1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0e13"
      uniprot "NA"
    ]
    graphics [
      x 515.8823084860218
      y 838.3951210991122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000162711"
      hgnc "NA"
      map_id "NLRP3"
      name "NLRP3"
      node_subtype "GENE"
      node_type "species"
      org_id "f72d3"
      uniprot "NA"
    ]
    graphics [
      x 616.0101242290714
      y 727.384466572001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NLRP3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      name "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      node_subtype "GENE"
      node_type "species"
      org_id "fa31c"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1160.0549492101827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fc1c4"
      uniprot "NA"
    ]
    graphics [
      x 97.42016935141226
      y 1271.9974343313452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000164430"
      hgnc "NA"
      map_id "CGAS"
      name "CGAS"
      node_subtype "GENE"
      node_type "species"
      org_id "a9c5b"
      uniprot "NA"
    ]
    graphics [
      x 231.08928742292227
      y 1292.2489707958025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CGAS"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000150995"
      hgnc "NA"
      map_id "ITPR1"
      name "ITPR1"
      node_subtype "GENE"
      node_type "species"
      org_id "ea20e"
      uniprot "NA"
    ]
    graphics [
      x 222.04694125873874
      y 656.025177785523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ITPR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idab07d001"
      uniprot "NA"
    ]
    graphics [
      x 221.88069586009624
      y 518.6459363102417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Calcium_space_release"
      name "Calcium_space_release"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8293"
      uniprot "NA"
    ]
    graphics [
      x 317.9459475008067
      y 429.47179573072003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Calcium_space_release"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cc8bf"
      uniprot "NA"
    ]
    graphics [
      x 452.9653445010475
      y 434.5635552932725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000104518"
      hgnc "NA"
      map_id "GSDMD"
      name "GSDMD"
      node_subtype "GENE"
      node_type "species"
      org_id "dcc65"
      uniprot "NA"
    ]
    graphics [
      x 568.160565306048
      y 382.67418364683243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSDMD"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e393d"
      uniprot "NA"
    ]
    graphics [
      x 543.9461518238061
      y 1088.6091315578735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "fb896"
      name "fb896"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fb896"
      uniprot "NA"
    ]
    graphics [
      x 696.9234625582498
      y 1221.1702311807073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fb896"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca979"
      uniprot "NA"
    ]
    graphics [
      x 398.5237838788553
      y 817.0290081136029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf594d3e0"
      uniprot "NA"
    ]
    graphics [
      x 715.925181992167
      y 932.613224085103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "e7a9d"
      name "e7a9d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7a9d"
      uniprot "NA"
    ]
    graphics [
      x 1305.7098155137844
      y 1298.1313281859577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e7a9d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_7"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b3b9f"
      uniprot "NA"
    ]
    graphics [
      x 1367.0553045186498
      y 1148.0249089603262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000162924;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "c329d"
      name "c329d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c329d"
      uniprot "NA"
    ]
    graphics [
      x 1399.943843387287
      y 1009.410972477928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c329d"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "c72d7"
      name "c72d7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c72d7"
      uniprot "NA"
    ]
    graphics [
      x 1217.2077926430493
      y 1203.020060754697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c72d7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cb0f5"
      uniprot "NA"
    ]
    graphics [
      x 1168.1537106131493
      y 1301.5904221187243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000269335;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ensembl:ENSG00000213341"
      hgnc "NA"
      map_id "fef8c"
      name "fef8c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fef8c"
      uniprot "NA"
    ]
    graphics [
      x 1009.0953640596199
      y 1317.059621090728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "fef8c"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8097"
      uniprot "NA"
    ]
    graphics [
      x 280.2676403277041
      y 1068.4126383301955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf6aa73a2"
      uniprot "NA"
    ]
    graphics [
      x 256.21618363828645
      y 921.451327196155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d60dd"
      uniprot "NA"
    ]
    graphics [
      x 870.0031994480564
      y 920.7019917237701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4961; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "IFNB1"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "e699d; dcfff"
      uniprot "NA"
    ]
    graphics [
      x 912.3659105781744
      y 817.5681547477741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IFNB1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id9ea5ec2d"
      uniprot "NA"
    ]
    graphics [
      x 708.646875757588
      y 1107.3519572955556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc9fd"
      uniprot "NA"
    ]
    graphics [
      x 1437.6265301127023
      y 888.201078691221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Cytokines"
      name "Cytokines"
      node_subtype "GENE"
      node_type "species"
      org_id "b9d10"
      uniprot "NA"
    ]
    graphics [
      x 1548.6784924710119
      y 872.2316594976832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Cytokines"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Pyroptosis"
      name "Pyroptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a367e"
      uniprot "NA"
    ]
    graphics [
      x 623.152945773805
      y 181.4835995256658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Pyroptosis"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f3a2a"
      uniprot "NA"
    ]
    graphics [
      x 637.2641674531284
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000117525"
      hgnc "NA"
      map_id "F3"
      name "F3"
      node_subtype "GENE"
      node_type "species"
      org_id "ae651"
      uniprot "NA"
    ]
    graphics [
      x 742.6735424917722
      y 104.66571278076538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "F3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d99fa"
      uniprot "NA"
    ]
    graphics [
      x 1194.9696372660678
      y 1399.6775735725041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232"
      hgnc "NA"
      map_id "d1991"
      name "d1991"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1991"
      uniprot "NA"
    ]
    graphics [
      x 1096.4427984009026
      y 1499.932466883027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "d1991"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "Intravenous_br_immunoglobulins"
      name "Intravenous_br_immunoglobulins"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa4ea"
      uniprot "NA"
    ]
    graphics [
      x 1117.1364870665523
      y 930.7869319377655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Intravenous_br_immunoglobulins"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_45"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id231333be"
      uniprot "NA"
    ]
    graphics [
      x 1059.3715091981721
      y 1035.3788032838206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000143226"
      hgnc "NA"
      map_id "FCGR2A"
      name "FCGR2A"
      node_subtype "GENE"
      node_type "species"
      org_id "f7129"
      uniprot "NA"
    ]
    graphics [
      x 959.0654339832618
      y 1120.9194886344558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "FCGR2A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c76d4"
      uniprot "NA"
    ]
    graphics [
      x 722.1406093149933
      y 638.8410395687799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8f3d31d3"
      uniprot "NA"
    ]
    graphics [
      x 194.73752457570424
      y 1377.0268816680282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id394406f9"
      uniprot "NA"
    ]
    graphics [
      x 652.9621927522154
      y 295.89704581929584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcfcc5c05"
      uniprot "NA"
    ]
    graphics [
      x 834.5022463646361
      y 1183.8263117694228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15365"
      hgnc "NA"
      map_id "Aspirin"
      name "Aspirin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "feddd"
      uniprot "NA"
    ]
    graphics [
      x 398.2573133403903
      y 1104.3562199248533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Aspirin"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcde3e513"
      uniprot "NA"
    ]
    graphics [
      x 345.9693831315616
      y 1218.9349177327103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f375a"
      uniprot "NA"
    ]
    graphics [
      x 979.0977814042624
      y 1559.999557501034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_6"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "b2dd0"
      uniprot "NA"
    ]
    graphics [
      x 859.8269921587398
      y 1562.813541127653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d73de"
      uniprot "NA"
    ]
    graphics [
      x 287.2349048951049
      y 796.4335101842363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc321"
      uniprot "NA"
    ]
    graphics [
      x 846.85955451271
      y 1302.4245699405046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 53
    source 7
    target 8
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 8
    target 9
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_25"
      target_id "NLRP3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 10
    target 11
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      target_id "W20_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 11
    target 12
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_40"
      target_id "CGAS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 13
    target 14
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "ITPR1"
      target_id "W20_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 14
    target 15
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_49"
      target_id "Calcium_space_release"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 15
    target 16
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Calcium_space_release"
      target_id "W20_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 16
    target 17
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_18"
      target_id "GSDMD"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 7
    target 18
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 18
    target 19
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_26"
      target_id "fb896"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 3
    target 20
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "NLRX1"
      target_id "W20_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 20
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_16"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 4
    target 21
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W20_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 22
    target 23
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "e7a9d"
      target_id "W20_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 23
    target 24
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_7"
      target_id "c329d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 25
    target 26
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "c72d7"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 27
    target 26
    cd19dm [
      diagram "WP4961"
      edge_type "PHYSICAL_STIMULATION"
      source_id "fef8c"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 26
    target 22
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_17"
      target_id "e7a9d"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 2
    target 28
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "cGAMP"
      target_id "W20_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 28
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_37"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 1
    target 29
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Angiotensin_space_II"
      target_id "W20_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 29
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_53"
      target_id "STING1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 4
    target 30
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "IRF3"
      target_id "W20_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 30
    target 31
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_20"
      target_id "IFNB1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 19
    target 32
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "fb896"
      target_id "W20_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 32
    target 4
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_48"
      target_id "IRF3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 24
    target 33
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "c329d"
      target_id "W20_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 33
    target 34
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_41"
      target_id "Cytokines"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 35
    target 36
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Pyroptosis"
      target_id "W20_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 36
    target 37
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_34"
      target_id "F3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 22
    target 38
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "e7a9d"
      target_id "W20_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 38
    target 39
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_23"
      target_id "d1991"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 40
    target 41
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Intravenous_br_immunoglobulins"
      target_id "W20_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 41
    target 42
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_45"
      target_id "FCGR2A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 9
    target 43
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "NLRP3"
      target_id "W20_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 43
    target 6
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_14"
      target_id "IL1B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 12
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "CGAS"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 5
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "INHIBITION"
      source_id "Vitamin_space_D3"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 44
    target 2
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_47"
      target_id "cGAMP"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 17
    target 45
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "GSDMD"
      target_id "W20_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 45
    target 35
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_46"
      target_id "Pyroptosis"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 42
    target 46
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "FCGR2A"
      target_id "W20_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 46
    target 19
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_51"
      target_id "fb896"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 47
    target 48
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "Aspirin"
      target_id "W20_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 48
    target 12
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_50"
      target_id "CGAS"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 39
    target 49
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "d1991"
      target_id "W20_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 49
    target 50
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_33"
      target_id "W20_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 7
    target 51
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "STING1"
      target_id "W20_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 51
    target 13
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_21"
      target_id "ITPR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 19
    target 52
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "fb896"
      target_id "W20_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 52
    target 27
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_9"
      target_id "fef8c"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
