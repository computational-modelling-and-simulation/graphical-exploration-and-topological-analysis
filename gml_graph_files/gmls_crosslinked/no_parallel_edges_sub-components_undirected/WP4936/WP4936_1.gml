# generated with VANTED V2.8.2 at Fri Mar 04 10:06:57 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000204673"
      hgnc "NA"
      map_id "AKT1S1"
      name "AKT1S1"
      node_subtype "GENE"
      node_type "species"
      org_id "c9d11"
      uniprot "NA"
    ]
    graphics [
      x 1467.8519606674047
      y 1169.5882966757968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AKT1S1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_57"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id28c533ea"
      uniprot "NA"
    ]
    graphics [
      x 1608.0773048458677
      y 1244.6945685419228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id63264543"
      uniprot "NA"
    ]
    graphics [
      x 1313.292151683862
      y 1117.6666445839915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:2475;urn:miriam:ncbiprotein:64223;urn:miriam:ncbiprotein:57521;urn:miriam:ncbiprotein:64798"
      hgnc "NA"
      map_id "c5e75"
      name "c5e75"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c5e75"
      uniprot "NA"
    ]
    graphics [
      x 1127.565010334151
      y 1078.9087380201586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "c5e75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35a4115e"
      uniprot "NA"
    ]
    graphics [
      x 1093.1926104360941
      y 898.0313108344483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_60"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3a40ef1e"
      uniprot "NA"
    ]
    graphics [
      x 1196.7328798816334
      y 1192.2380903870774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id853c2e26"
      uniprot "NA"
    ]
    graphics [
      x 1021.7231149189938
      y 1066.1867569228928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_73"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide513164c"
      uniprot "NA"
    ]
    graphics [
      x 892.736927814692
      y 1114.982332956243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7a84a2"
      uniprot "NA"
    ]
    graphics [
      x 1238.6091756782553
      y 1019.5701054214214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_71"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idd277ed15"
      uniprot "NA"
    ]
    graphics [
      x 1060.9162999897057
      y 1208.5644759054749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000023287;urn:miriam:ncbiprotein:9776;urn:miriam:ncbiprotein:8408"
      hgnc "NA"
      map_id "cf768"
      name "cf768"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cf768"
      uniprot "NA"
    ]
    graphics [
      x 961.2317760072593
      y 1305.4569954004655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "cf768"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id949da026"
      uniprot "NA"
    ]
    graphics [
      x 820.4693710906581
      y 1390.6952651886322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "Phagophore"
      name "Phagophore"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a1420"
      uniprot "NA"
    ]
    graphics [
      x 771.7495020941238
      y 1479.7253724167053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Phagophore"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000116209"
      hgnc "NA"
      map_id "TMEM59"
      name "TMEM59"
      node_subtype "GENE"
      node_type "species"
      org_id "e10fa"
      uniprot "NA"
    ]
    graphics [
      x 728.5649346580029
      y 1331.7774833083583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TMEM59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000101844"
      hgnc "NA"
      map_id "ATG4A"
      name "ATG4A"
      node_subtype "GENE"
      node_type "species"
      org_id "bc0e6"
      uniprot "NA"
    ]
    graphics [
      x 824.1719398555919
      y 1533.640693571782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG4A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:9140;urn:miriam:ncbiprotein:55054;urn:miriam:ncbiprotein:9474"
      hgnc "NA"
      map_id "ddc93"
      name "ddc93"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ddc93"
      uniprot "NA"
    ]
    graphics [
      x 697.2700843302425
      y 1485.4328277574327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ddc93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10533"
      hgnc "NA"
      map_id "ATG7"
      name "ATG7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8149"
      uniprot "NA"
    ]
    graphics [
      x 847.2191101329038
      y 1286.6225494616392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:64422"
      hgnc "NA"
      map_id "ATG3"
      name "ATG3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bfd6f"
      uniprot "NA"
    ]
    graphics [
      x 916.1437037071872
      y 1470.3452614503692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10133"
      hgnc "NA"
      map_id "OPTN"
      name "OPTN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b3adb"
      uniprot "NA"
    ]
    graphics [
      x 935.2912495189812
      y 1399.702009505384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "OPTN"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000066739"
      hgnc "NA"
      map_id "ATG2B"
      name "ATG2B"
      node_subtype "GENE"
      node_type "species"
      org_id "a3222"
      uniprot "NA"
    ]
    graphics [
      x 885.079674781324
      y 1526.1630701114518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG2B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8878"
      hgnc "NA"
      map_id "SQSTM1"
      name "SQSTM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acae9"
      uniprot "NA"
    ]
    graphics [
      x 703.5856534873619
      y 1416.2943183416564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "SQSTM1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6194"
      hgnc "NA"
      map_id "RPS6"
      name "RPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1a5f"
      uniprot "NA"
    ]
    graphics [
      x 629.7494435813073
      y 1383.3105993123036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RPS6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000158528"
      hgnc "NA"
      map_id "PPP1R9A"
      name "PPP1R9A"
      node_subtype "GENE"
      node_type "species"
      org_id "ad6e9"
      uniprot "NA"
    ]
    graphics [
      x 1037.9108285195096
      y 1484.1432135061355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PPP1R9A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10241"
      hgnc "NA"
      map_id "CALCOCO2"
      name "CALCOCO2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bbbe9"
      uniprot "NA"
    ]
    graphics [
      x 754.4825817992524
      y 1541.7337450199257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "CALCOCO2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophagosome_slash__br_Late_space_endosome"
      name "Autophagosome_slash__br_Late_space_endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa8e9"
      uniprot "NA"
    ]
    graphics [
      x 666.0141564805085
      y 1244.4647427613413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophagosome_slash__br_Late_space_endosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:11345"
      hgnc "NA"
      map_id "GABARAPL2"
      name "GABARAPL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc090"
      uniprot "NA"
    ]
    graphics [
      x 784.9383110250682
      y 1160.124958692589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GABARAPL2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "db1e2"
      uniprot "NA"
    ]
    graphics [
      x 745.8536611715995
      y 939.8006625613951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "ORF3"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "ec84a; cbf03; c28d5"
      uniprot "NA"
    ]
    graphics [
      x 724.426428800387
      y 751.5317670514279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ORF3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_3"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a2165"
      uniprot "NA"
    ]
    graphics [
      x 586.1814356512688
      y 699.3304969079547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_69"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id9545f48f"
      uniprot "NA"
    ]
    graphics [
      x 871.482645539439
      y 703.7485858812723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf121"
      uniprot "NA"
    ]
    graphics [
      x 705.7001736787769
      y 613.6369871375958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8887"
      hgnc "NA"
      map_id "TAX1BP1"
      name "TAX1BP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a579d"
      uniprot "NA"
    ]
    graphics [
      x 696.0506767529396
      y 496.4552505716782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TAX1BP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6009"
      hgnc "NA"
      map_id "RHEB"
      name "RHEB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d10c6"
      uniprot "NA"
    ]
    graphics [
      x 1026.2300404746084
      y 717.6238839922642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "RHEB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id67a2f315"
      uniprot "NA"
    ]
    graphics [
      x 1126.415451879201
      y 607.9997645328347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "GSK3"
      name "GSK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ec6c1"
      uniprot "NA"
    ]
    graphics [
      x 1258.4260205408398
      y 576.3241605007967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GSK3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id788d6f1c"
      uniprot "NA"
    ]
    graphics [
      x 1358.1679960339995
      y 658.1038729053772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "TSC2"
      name "TSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed654"
      uniprot "NA"
    ]
    graphics [
      x 1263.1930091447566
      y 730.3701817240187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "TSC2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide9784478"
      uniprot "NA"
    ]
    graphics [
      x 1141.0447949178279
      y 796.4949074630905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 3
      diagram "WP4936; C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P31749; urn:miriam:ncbigene:207;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:391;urn:miriam:hgnc.symbol:AKT1;urn:miriam:refseq:NM_005163;urn:miriam:uniprot:P31749;urn:miriam:ensembl:ENSG00000142208"
      hgnc "NA; HGNC_SYMBOL:AKT1"
      map_id "UNIPROT:P31749"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c34e3; sa29; sa28"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 1018.6184721908754
      y 844.643930777043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UNIPROT:P31749"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:27072;urn:miriam:ncbiprotein:55823;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ensembl:ENSG00000104142;urn:miriam:ncbiprotein:64601;urn:miriam:ensembl:ENSG00000139719"
      hgnc "NA"
      map_id "HOPS_space_COMPLEX_br_"
      name "HOPS_space_COMPLEX_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f7bb6"
      uniprot "NA"
    ]
    graphics [
      x 455.74877088374564
      y 695.9280759180015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "HOPS_space_COMPLEX_br_"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id180e5540"
      uniprot "NA"
    ]
    graphics [
      x 416.49140900278746
      y 814.7086596039172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000118640"
      hgnc "NA"
      map_id "VAMP8"
      name "VAMP8"
      node_subtype "GENE"
      node_type "species"
      org_id "e1e54"
      uniprot "NA"
    ]
    graphics [
      x 460.8664548983529
      y 943.0476989237679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "VAMP8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id20e40bd3"
      uniprot "NA"
    ]
    graphics [
      x 549.0482203092844
      y 1069.2737911447618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "Lysosome"
      name "Lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8183"
      uniprot "NA"
    ]
    graphics [
      x 665.3511942318024
      y 1001.1875474049925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lysosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "Autophago_br_lysosome"
      name "Autophago_br_lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d106b"
      uniprot "NA"
    ]
    graphics [
      x 664.0778199097721
      y 1073.9358028503498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Autophago_br_lysosome"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_50"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f725b"
      uniprot "NA"
    ]
    graphics [
      x 1224.3508004346022
      y 1523.64447138692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302;urn:miriam:pubmed:33845483"
      hgnc "NA"
      map_id "NSP6"
      name "NSP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6a2a"
      uniprot "NA"
    ]
    graphics [
      x 1401.9812079387366
      y 1498.108401813366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NSP6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fbe32"
      uniprot "NA"
    ]
    graphics [
      x 1521.0167672017153
      y 1348.5489474973526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e048e"
      uniprot "NA"
    ]
    graphics [
      x 1418.518107453792
      y 1395.8828090024042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000188554"
      hgnc "NA"
      map_id "NBR1"
      name "NBR1"
      node_subtype "GENE"
      node_type "species"
      org_id "d05df"
      uniprot "NA"
    ]
    graphics [
      x 1364.2135175565588
      y 1295.704066157395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NBR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:79065"
      hgnc "NA"
      map_id "ATG9A"
      name "ATG9A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6873"
      uniprot "NA"
    ]
    graphics [
      x 1579.5491710009699
      y 1165.9793790324122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATG9A"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_61"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id47e9ee53"
      uniprot "NA"
    ]
    graphics [
      x 1628.147910058168
      y 1027.3449464521088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "e44d3"
      name "e44d3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e44d3"
      uniprot "NA"
    ]
    graphics [
      x 1711.9073697400008
      y 945.1029959247161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "e44d3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8825c1d0"
      uniprot "NA"
    ]
    graphics [
      x 553.5618260612496
      y 1281.900462740357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000108443"
      hgnc "NA"
      map_id "P70S6K"
      name "P70S6K"
      node_subtype "GENE"
      node_type "species"
      org_id "bfbce"
      uniprot "NA"
    ]
    graphics [
      x 656.7658090000878
      y 1165.6243847087117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "P70S6K"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id259f64e"
      uniprot "NA"
    ]
    graphics [
      x 491.6841981638491
      y 1169.9223978134205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000063046"
      hgnc "NA"
      map_id "EIF4B"
      name "EIF4B"
      node_subtype "GENE"
      node_type "species"
      org_id "c8253"
      uniprot "NA"
    ]
    graphics [
      x 377.9409567397332
      y 1120.0755018629015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "EIF4B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 2
      diagram "WP4936; WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000155506"
      hgnc "NA"
      map_id "LARP1"
      name "LARP1"
      node_subtype "GENE"
      node_type "species"
      org_id "a13c2; b7808"
      uniprot "NA"
    ]
    graphics [
      x 1259.1408949248046
      y 914.9121628748863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LARP1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000149357"
      hgnc "NA"
      map_id "LAMTOR1"
      name "LAMTOR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e6980"
      uniprot "NA"
    ]
    graphics [
      x 962.0976570715584
      y 975.8042274193275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "LAMTOR1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5563"
      hgnc "NA"
      map_id "AMPK"
      name "AMPK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a5318"
      uniprot "NA"
    ]
    graphics [
      x 1220.5089092190924
      y 1306.9409348895126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMPK"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5315"
      hgnc "NA"
      map_id "PKM"
      name "PKM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a628b"
      uniprot "NA"
    ]
    graphics [
      x 1725.5335477794174
      y 1271.7111925131517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PKM"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 62
    source 2
    target 1
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_57"
      target_id "AKT1S1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 1
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "AKT1S1"
      target_id "W6_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 61
    target 2
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "PKM"
      target_id "W6_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 3
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_62"
      target_id "c5e75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 5
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_58"
      target_id "c5e75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 6
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_60"
      target_id "c5e75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 7
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_66"
      target_id "c5e75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 4
    target 8
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "c5e75"
      target_id "W6_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 4
    target 9
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "c5e75"
      target_id "W6_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 4
    target 10
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "c5e75"
      target_id "W6_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 33
    target 5
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "RHEB"
      target_id "W6_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 60
    target 6
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "AMPK"
      target_id "W6_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 59
    target 7
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "LAMTOR1"
      target_id "W6_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 8
    target 55
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_73"
      target_id "P70S6K"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 9
    target 58
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_65"
      target_id "LARP1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 10
    target 11
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_71"
      target_id "cf768"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 11
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "cf768"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 13
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "Phagophore"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 14
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "TMEM59"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 15
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ATG4A"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 16
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ddc93"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 17
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ATG7"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 18
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ATG3"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 19
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "OPTN"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 20
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "ATG2B"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 21
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "SQSTM1"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 22
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "INHIBITION"
      source_id "RPS6"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 23
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "PPP1R9A"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 24
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "CALCOCO2"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 12
    target 25
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "Autophagosome_slash__br_Late_space_endosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 12
    target 26
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "GABARAPL2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 54
    target 22
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_67"
      target_id "RPS6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 46
    target 23
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_50"
      target_id "PPP1R9A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 25
    target 43
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "Autophagosome_slash__br_Late_space_endosome"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 27
    target 26
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_36"
      target_id "GABARAPL2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 28
    target 27
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ORF3"
      target_id "W6_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 28
    target 29
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ORF3"
      target_id "W6_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 28
    target 30
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ORF3"
      target_id "W6_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 28
    target 31
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ORF3"
      target_id "W6_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 29
    target 40
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_3"
      target_id "HOPS_space_COMPLEX_br_"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 30
    target 33
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_69"
      target_id "RHEB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 31
    target 32
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_20"
      target_id "TAX1BP1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 34
    target 33
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_63"
      target_id "RHEB"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 35
    target 34
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "GSK3"
      target_id "W6_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 35
    target 36
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "GSK3"
      target_id "W6_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 36
    target 37
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_64"
      target_id "TSC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 38
    target 37
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_74"
      target_id "TSC2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 39
    target 38
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "UNIPROT:P31749"
      target_id "W6_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 40
    target 41
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "HOPS_space_COMPLEX_br_"
      target_id "W6_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 41
    target 42
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_54"
      target_id "VAMP8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 42
    target 43
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "VAMP8"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 44
    target 43
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "Lysosome"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 43
    target 45
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_55"
      target_id "Autophago_br_lysosome"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 47
    target 46
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "NSP6"
      target_id "W6_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 47
    target 48
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "NSP6"
      target_id "W6_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 47
    target 49
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "NSP6"
      target_id "W6_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 48
    target 51
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_53"
      target_id "ATG9A"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 49
    target 50
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_38"
      target_id "NBR1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 51
    target 52
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "ATG9A"
      target_id "W6_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 52
    target 53
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_61"
      target_id "e44d3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 55
    target 54
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "P70S6K"
      target_id "W6_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 55
    target 56
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "P70S6K"
      target_id "W6_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 56
    target 57
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_56"
      target_id "EIF4B"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
